package com.af.admin.constants;

public class AfHibernateQueries {

	private AfHibernateQueries(){}

	/***********************************************************************************************************/
	/******************************** Menu related queries ********************************************/
	/***********************************************************************************************************/

	public static final String AF_ADMIN_MENU="select aam from AfAdminMenu aam where aam.status=1";
	public static final String AF_ADMIN_MENU_RELATION="select aamr from AfAdminMenuRelation aamr where aamr.status=1";


	public static final String AF_ADMIN_MENUS="select new com.af.admin.beans.AfAdminMenusBean(me.menuId,me.title,me.url,mr.parentMenuId)" 
			+"from AfAdminMenu me ,AfAdminMenuRelation mr where me.menuId=mr.menuId and me.status=1 order by mr.parentMenuId, mr.menuId";


	/***********************************************************************************************************/
	/******************************** User Management related queries ********************************************/
	/***********************************************************************************************************/

	public static final String AF_GET_ACTIVE_USER_BY_EMAIL="select user from AfAdminUser user where user.userName=:userName";
	public static final String Af_GET_ACTIVE_USER_BY_EMAIL_AND_PASSWORD="select user from AfAdminUser user where user.userName=:userName and user.password=:password and user.userStatus=1";
	public static final String AF_ADMIN_USER_BY_NAME="select user from AfAdminUser user where user.userName=:userName";

	public static final String AF_ROLE_PERMISSIONS_QUERY="select afpv.permissionName from AfAdminRolePermissions afrp ,AfPermissionValues afpv"
			+ " where afrp.afPermissionValues.permissionId=afpv.permissionId and afrp.afAdminRoles.roleId=:userRole and afrp.permissionStatus=1";


	public static final String AF_ADMIN_USER = "select aau from AfAdminUser aau where aau.status=1";
	public static final String AF_ADMIN_ROLES = "select aar from AfAdminRoles aar where aar.status=1";
	public static final String AF_ADMIN_ROLE_BY_NAME = "select aar from AfAdminRoles aar where aar.roleName=:roleName";
	public static final String AF_PERMISSION_VALUES_QUERY = "select pv from AfPermissionValues pv where pv.status=1";

	public static final String AF_ENUM_VALUES = "select aev from AfEnumValues aev where aev.typeName=:typeName";

	/***********************************************************************************************************/
	/******************************** Banner Management related queries ********************************************/
	/***********************************************************************************************************/


	public static final String AF_BANNER = "select ab from AfBanner ab where ab.status=1 order by ab.bannerSections, ab.bannerPosition"; 


	public static final String AF_PARTNERS = "select ap from AfPartner ap where ap.partnerStatus=1 and ap.status=1";

	public static final String GET_ALL_PARTNERS = "select ap from AfPartner ap where ap.status=1 order by ap.partnerName";

	public static final String AF_PARTNERS_BY_DEFAULT = "select ap from AfPartner ap where ap.defaultPartner=1 and ap.status=1";

	public static final String AF_PARTNERS_BY_PARTNER_ID="select partners from AfPartner partners where partners.partnerId=:partnerId";

	public static final String AF_PARTNERS_BY_PARTNER_NAME="select partners from AfPartner partners where partners.partnerName=:partnerName";

	public static final String AF_CATEGORY = "select aac from AfCategory aac where aac.status=1 order by aac.categoryId";

	public static final String AF_CATEGORY_BY_CATEGORY_NO = "select aac from AfCategory aac where aac.catNo=:catNo";

	public static final String AF_CATEGORY_LIST_BASED_PART_NO = "select aacl from AfCategory aacl where aacl.status=1";

	public static final String AF_CATEGORIES_BY_CATEGORY_ID="select category from AfCategory category where category.categoryId=:categoryId";

	public static final String AF_CATEGORY_WHERE_PARTNER_CATEGORY_IS_NOT_EMPTY="from AfPartnerCategory partnercategory where partnercategory.status=1 ";

	public static final String AF_PARTNER_CATEGORY="from AfPartnerCategory partnercategory where partnercategory.status=1";

	public static final String AF_PARTNERCOMMISSION_BY_PRT_NO = "select apc from AfPartnerCommission apc where apc.afPartner.prtNo=:partNo and apc.status=1";
	
	public static final String AF_PARTNERCOMMISSION = "from AfPartnerCommission apc where apc.status=1 and apc.afPartner.status=1";
	
	public static final String AF_PARTNER_CATEGORIES="from AfPartnerCategory partnercategory where partnercategory.prtCatNo=:prtCatNo";

	public static final String AF_PARTNER_CATEGORY_BY_DISPLAY_ORDER="select afpc from AfPartnerCategory afpc where afpc.partnerCategoryPosition=:categoryDisplayOrder and afpc.partnerCategoryStatus=1 and afpc.status=1";


	public static final String AF_PARTNER_CATEGORY_BY_PRTNO = "select afpc from AfPartnerCategory afpc where afpc.afPartner.prtNo=:prtNo and afpc.status=1";

	public static final String AF_RECOMMENDED_PRODUCT = "select afrp from AfRecommendedProduct afrp where afrp.status=1 order by afrp.rpPosition";

	public static final String AF_RECOMMENDED_PRODUCT_CHECK="select afrp from AfRecommendedProduct afrp where  afrp.afProduct.masterProductId=:prtPrdNo and afrp.status=1";

	public static final String AF_RECOMMENDED_PRODUCT_POSITION_CHECK="select afrp from AfRecommendedProduct afrp where afrp.rpPosition=:prtPrdPos and afrp.status=1";

	public static final String AF_PARTNER_PRODUCT_CHECK="select afpc from AfPartnerCommission afpc where afpc.afPartner.prtNo=:prtNo and afpc.afPartnerProduct.prtPrdNo=:prtPrdNo and afpc.afPartnerCategory.prtCatNo=:prtCatNo and afpc.status=1";

	public static final String AF_RECOMMENDED_CATEGORY = "select afrc from AfRecommendedCategory afrc where afrc.status=1 order by afrc.rcPosition";

	public static final String AF_RECOMMENDED_CATEGORY_CHECK="select afrc from AfRecommendedCategory afrc where afrc.afPartner.prtNo=:prtNo and afrc.afPartnerCategory.prtCatNo=:prtCatNo and afrc.status=1";

	public static final String AF_RECOMMENDED_CATEGORY_POSITION_CHECK="select afrc from AfRecommendedCategory afrc where afrc.rcPosition=:recommendedCategoryPosition and afrc.status=1";

	public static final String AF_PARTNER_PRODUCT = "select afpp from AfPartnerProduct afpp where afpp.afPartnerCategory.prtCatNo=:prtCatNo and afpp.status=1";

	public static final String AF_COMMISION_EXISTENCE_STARTDATE="select count(*) from AfPartnerCommission afpc where afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo=:partCatNo and afpc.afPartnerProduct.prtPrdNo=:partPdctNo and afpc.activationDate >:fromDate";

	public static final String AF_COMMISION_EXISTENCE_STARTDATE_WITHOUT_PRODUCT="select count(*) from AfPartnerCommission afpc where afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo=:partCatNo and afpc.afPartnerProduct.prtPrdNo IS null and afpc.activationDate>:fromDate";

	public static final String AF_COMMISION_EXISTENCE_STARTDATE_WITH_PRT_NO="select count(*) from AfPartnerCommission afpc where afpc.afPartner.prtNo=:partNo and afpc.activationDate>:fromDate and afpc.afPartnerCategory.prtCatNo is null and afpc.afPartnerProduct.prtPrdNo is null and afpc.productBrand is null and afpc.categoryPath is null";
	public static final String AF_COMMISION_EXISTENCE_STARTDATE_WITH_PRT_CAT_NO="select count(*) from AfPartnerCommission afpc where afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo=:partCatNo and afpc.activationDate>:fromDate and afpc.afPartnerProduct.prtPrdNo is null and afpc.productBrand is null and afpc.categoryPath is null";
	public static final String AF_COMMISION_EXISTENCE_STARTDATE_WITH_PRODUCT="select count(*) from AfPartnerCommission afpc where afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo=:partCatNo and afpc.afPartnerProduct.prtPrdNo=:partPrdNo and afpc.activationDate>:fromDate and afpc.productBrand is null and afpc.categoryPath is null";
	public static final String AF_COMMISION_EXISTENCE_STARTDATE_WITH_BRAND="select count(*) from AfPartnerCommission afpc where afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo=:partCatNo and afpc.productBrand=:brandName and afpc.activationDate>:fromDate and afpc.afPartnerProduct.prtPrdNo is null and afpc.categoryPath is null";
	public static final String AF_COMMISION_EXISTENCE_STARTDATE_WITH_BRAND_CAT_PATH="select count(*) from AfPartnerCommission afpc where afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo=:partCatNo and afpc.productBrand=:brandName and afpc.activationDate>:fromDate and afpc.afPartnerProduct.prtPrdNo is null and afpc.categoryPath=:catPath";
	public static final String AF_COMMISION_EXISTENCE_STARTDATE_WITH_PRT_CAT_AND_CAT_PATH="select count(*) from AfPartnerCommission afpc where afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo=:partCatNo and afpc.activationDate>:fromDate and afpc.afPartnerProduct.prtPrdNo is null and afpc.categoryPath=:catPath and afpc.productBrand is null";
	public static final String AF_COMMISION_EXISTENCE_STARTDATE_WITH_PRT_AND_CAT_PATH="select count(*) from AfPartnerCommission afpc where afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo is null and afpc.activationDate>:fromDate and afpc.afPartnerProduct.prtPrdNo is null and afpc.categoryPath=:catPath and afpc.productBrand is null";

	public static final String AF_COMMISION_EXISTENCE_EDIT_STARTDATE_WITH_PRT_NO="select MAX(afpc.deactivationDate) from AfPartnerCommission afpc where afpc.prtCommNo!=:partComNo and afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo is null and afpc.afPartnerProduct.prtPrdNo is null and afpc.productBrand is null and afpc.categoryPath is null and afpc.deactivationDate<:startDateHidden";
	public static final String AF_COMMISION_EXISTENCE_EDIT_STARTDATE_WITH_PRT_CAT_NO="select MAX(afpc.deactivationDate) from AfPartnerCommission afpc where afpc.prtCommNo!=:partComNo and afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo=:partCatNo and afpc.afPartnerProduct.prtPrdNo is null and afpc.productBrand is null and afpc.categoryPath is null and afpc.deactivationDate<:startDateHidden";
	public static final String AF_COMMISION_EXISTENCE_EDIT_STARTDATE_WITH_PRODUCT="select MAX(afpc.deactivationDate) from AfPartnerCommission afpc where afpc.prtCommNo!=:partComNo and afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo=:partCatNo and afpc.afPartnerProduct.prtPrdNo=:partPrdNo and afpc.productBrand is null and afpc.categoryPath is null and afpc.deactivationDate<:startDateHidden";
	public static final String AF_COMMISION_EXISTENCE_EDIT_STARTDATE_WITH_BRAND="select MAX(afpc.deactivationDate) from AfPartnerCommission afpc where afpc.prtCommNo!=:partComNo and afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo=:partCatNo and afpc.productBrand=:brandName and afpc.afPartnerProduct.prtPrdNo is null and afpc.categoryPath is null and afpc.deactivationDate<:startDateHidden";
	public static final String AF_COMMISION_EXISTENCE_EDIT_STARTDATE_WITH_BRAND_AND_CAT_PATH="select MAX(afpc.deactivationDate) from AfPartnerCommission afpc where afpc.prtCommNo!=:partComNo and afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo=:partCatNo and afpc.productBrand=:brandName and afpc.afPartnerProduct.prtPrdNo is null and afpc.categoryPath=:catPath and afpc.deactivationDate<:startDateHidden";
	public static final String AF_COMMISION_EXISTENCE_EDIT_STARTDATE_WITH_PRT_CAT_AND_CAT_PATH="select MAX(afpc.deactivationDate) from AfPartnerCommission afpc where afpc.prtCommNo!=:partComNo and afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo=:partCatNo and afpc.productBrand is null and afpc.afPartnerProduct.prtPrdNo is null and afpc.categoryPath=:catPath and afpc.deactivationDate<:startDateHidden";
	public static final String AF_COMMISION_EXISTENCE_EDIT_STARTDATE_WITH_PRT_AND_CAT_PATH="select MAX(afpc.deactivationDate) from AfPartnerCommission afpc where afpc.prtCommNo!=:partComNo and afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo is null and afpc.productBrand is null and afpc.afPartnerProduct.prtPrdNo is null and afpc.categoryPath=:catPath and afpc.deactivationDate<:startDateHidden";
	

	public static final String AF_COMMISION_EXISTENCE_STARTDATE_ENDDATE="select count(*) from AfPartnerCommission afpc where afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo=:partCatNo and afpc.afPartnerProduct.prtPrdNo=:partPdctNo and afpc.deactivationDate>:newtodate";

	public static final String AF_COMMISION_EXISTENCE_STARTDATE_ENDDATE_WITHOUT_PRODUCT="select count(*) from AfPartnerCommission afpc where afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo=:partCatNo and afpc.afPartnerProduct.prtPrdNo IS null and afpc.deactivationDate > :newtodate";


	public static final String AF_COMMISION_EXISTENCE="select max(afpc.prtCommNo) from AfPartnerCommission afpc where afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo=:partCatNo and afpc.afPartnerProduct.prtPrdNo=:partPdctNo";

	public static final String AF_COMMISION_EXISTENCE_WITHOUT_PRODUCT="select max(afpc.prtCommNo) from AfPartnerCommission afpc where afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo=:partCatNo and afpc.afPartnerProduct.prtPrdNo IS null";

//	public static final String AF_GET_TRANSACTION_ID = "from AfTransaction aft where aft.status=1";
	
	public static final String AF_GET_TRANSACTION_ID = "select aft from AfTransaction aft  where aft.status=1 and aft.jpNumber=:jpNumber ";
//	public static final String AF_GET_TRANSACTION_ID1 = "select aft from AfTransaction aft where aft.status=1 and aft.jpNumber=:jpNumber and aft.createdTime=:startdate between aft.createdTime=:enddate ";
	
	public static final String AF_TRANSACTION_DETAILS = "select aftd.partnerId as partnerId,aftd.transactionDate as transactionDate,aftd.partnerItemId as partnerItemId, "
			+ " aftd.milesPostingDate as milesPostingDate,aftd.partnerProductId as partnerProductId,aftd.productStatus as productStatus,"
			+ " aftd.price as price, aftd.jpmilesToCredit as jpmilesToCredit, aftd.expectedDateOfCredit as expectedDateOfCredit  from AfTransactionDetails aftd,AfTransaction aft where aftd.afTransaction.transactionId=aft.transactionId and aftd.afTransaction.transactionId=:transactionId and aftd.status=1";
	
	public static final String AF_PARTNER_PRODUCT_NAME="from AfPartnerProduct afpproduct where afpproduct.productId=:productId";


	public static final String AF_OFFERS_DEALS = "select afpo from AfPartnerOffer afpo where afpo.status=1 and (afpo.deactivationDate is not null and afpo.deactivationDate>=Now()) order by afpo.offerPosition desc";
	public static final String AF_CHECK_OFFER_NAME="select afod from AfOffersDeals afod where afod.afPartner.prtNo=:prtNo and afod.afPartnerOffer.prtOffrNo=:offerId and afod.status=1";
	public static final String AF_CHECK_OFFER_SORT_ORDER="select afpo from AfPartnerOffer afpo where afpo.offerPosition=:offersDealsSortOrder and afpo.status=1";
	public static final String AF_SEARCHED_OFFER_BY_CATEGORY="select afod from AfOffersDeals afod where afod.afPartner.prtNo=:prtNo and afod.afPartnerCategory.prtCatNo=:partCatNo and afod.status=1";
	public static final String AF_OFFER_NAMES_BY_CATEGORY = "select afop from AfPartnerOffer afop where afop.afPartnerCategory.prtCatNo=:prtCatNo and afop.afPartner.prtNo=:prtNo and afop.status=1";
	public static final String AF_PARTNER_OFFER_DETAILS = "select afpo from AfPartnerOffer afpo where afpo.afPartner.prtNo=:prtNo and afpo.status=1";
	public static final String AF_PARTNER_OFFER_BY_PRTOFFRNO = "select afpo from AfPartnerOffer afpo where afpo.prtOffrNo=:prtOffrNo and afpo.afPartner.prtNo=:prtNo and afpo.status=1";
	public static final String AF_PARTNER_OFFER_BY_PRTCATNO = "select afpo from AfPartnerOffer afpo where afpo.afPartnerCategory.prtCatNo=:prtCatNo and afpo.afPartner.prtNo=:prtNo and afpo.status=1";
	public static final String AF_PARTNER_OFFER_BY_PRT_NO_AND_CAT_NO = "select afpo from AfPartnerOffer afpo where afpo.afCategory.catNo=:catNo and afpo.afPartner.prtNo=:prtNo and afpo.status=1";
	public static final String AF_PARTNER_CATEGORY_NO_BY_NAME = "select afpc from AfPartnerCategory afpc where afpc.afPartner.prtNo=:prtNo  and afpc.partnerCategoryName=:catName and afpc.status=1";

	public static final String AF_PARTNER_OFFER_BY_PRTNO = "select afpo from AfPartnerOffer afpo where afpo.afPartner.prtNo=:prtNo and afpo.status=1 order by afpo.offerPosition desc";
	public static final String AF_PARTNER_OFFER_BY_NAME = "select afpo from AfPartnerOffer afpo where afpo.status=1";

	//With offer name
	public static final String AF_PARTNER_OFFER_BY_PRTNO_OFFRNAME = "select afpo from AfPartnerOffer afpo where afpo.afPartner.prtNo=:prtNo and afpo.offerName=:searchByValue and afpo.status=1 order by -afpo.offerPosition desc";
	public static final String AF_PARTNER_OFFER_BY_OFFRNAME = "select afpo from AfPartnerOffer afpo where afpo.offerName=:searchByValue and afpo.status=1 order by -afpo.offerPosition desc";

	public static final String AF_PARTNER_OFFER_BY_PRTNO_NAME = "select afpo from AfPartnerOffer afpo where afpo.afPartner.prtNo=:prtNo and  afpo.status=1 order by -afpo.offerPosition desc";
	
	//With Description
	public static final String AF_PARTNER_OFFER_BY_PRTNO_DESC = "select afpo from AfPartnerOffer afpo where afpo.afPartner.prtNo=:prtNo and afpo.description=:searchByValue and afpo.status=1 order by -afpo.offerPosition desc";
	public static final String AF_PARTNER_OFFER_BY_DESC = "select afpo from AfPartnerOffer afpo where afpo.description=:searchByValue and afpo.status=1 order by -afpo.offerPosition desc";

	//With Category name
	public static final String AF_PARTNER_OFFER_BY_PRTNO_CATNAME = "select afpo from AfPartnerOffer afpo where afpo.afPartner.prtNo=:prtNo and afpo.afCategory.catNo=:catNo and afpo.status=1 order by -afpo.offerPosition desc";
	public static final String AF_PARTNER_OFFER_BY_CATNAME = "select afpo from AfPartnerOffer afpo where afpo.afCategory.catNo=:catNo and afpo.status=1 order by -afpo.offerPosition desc";

	public static final String  AF_GET_ROLE_PERMISSIONS_BY_ID="select afarp from AfAdminRolePermissions iparp where afarp.afAdminRoles=:roleId";

	public static final String AF_RECOMMENDED_CATEGORY_BY_NO="select afrc from AfRecommendedCategory afrc where afrc.afPartner.prtNo=:prtNo and afrc.status=1";

	public static final String GET_PRT_CAT_NO_PRICE_RANGE_FILTER = "select afpr from AfPriceRangeFilter afpr where afpr.prtCatNo=:prt_cat_no and afpr.status=1";

	/************ Reports start *******************/

	public static final String GET_BUSINESS_REPORT = "select new com.af.admin.beans.BuisnessReportsBean(tr.jpNumber,tr.source,tr.medium,tr.campaign,tr.memberOrGuest,td.transactionId,td.partnerItemId,td.transactionDate,td.partnerId,td.partnerId,td.partnerId,td.price,td.ratePerMile,td.commissionRate,td.memberShare,td.jpmilesPurchase,td.expectedDateOfCredit,td.jpmilesCancel,td.jpmilesToCredit,td.productStatus,td.partnerProductId)"
			+"from AfTransactionDetails td,AfTransaction tr where td.afTransaction.txnNo = tr.txnNo and";

	public static final String GET_NEW_PRODUCTS_REPORT_BRANDS_BY_PRODUCTS = "select afpp.productBrand from AfPartnerProduct afpp, AfPartner afp"
			+ " where afpp.status=1 and afpp.productStatus=1 and (date(afpp.createdTime) between :toDate and :fromDate or"
			+ " date(afpp.modifiedTime) between :toDate and :fromDate) and afpp.afPartner.prtNo=afp.prtNo";
	
	public static final String GET_NEW_PRODUCTS_REPORT_PARTNERS_BY_PRODUCTS = "select distinct afp.partnerName, afp.prtNo from AfPartnerProduct afpp, AfPartner afp"
			+ " where afpp.status = 1 and afpp.productStatus = 1 and afpp.afPartner.prtNo = afp.prtNo";
	
	public static final String GET_NEW_PRODUCTS_REPORT = "select afpp from AfPartnerProduct afpp where afpp.status=1 and afpp.productStatus=1"
			+ " and afpp.productBrand=:productBrand and afpp.afPartner.prtNo=:prtNo and (date(afpp.createdTime) between :toDate and :fromDate or"
			+ " date(afpp.modifiedTime) between :toDate and :fromDate)";
	
	public static final String GET_MAPPED_PRODUCTS_REPORT = "select distinct afp from AfProduct afp, AfPartnerProduct afpp"
			+ " where afp.productStatus=1 and afp.status=1 and afp.partnerCount > 1";
	
	public static final String GET_MAPPED_PRODUCTS_REPORT_JOIN = " and afp.masterProductId = afpp.afProduct.masterProductId";
	
	
	/************ Reports end *******************/
	
	//Phase 2 Query starts..................
	public static final String AF_LOGO_PARTNER_DISPLAY_ORDER_CHECK="select afp from AfPartner afp where afp.partnerPosition=:prtPos and afp.offLinePartner=0 and afp.readPartner=0 and afp.talkPartner=0 and afp.status=1";
	
	public static final String AF_OFFLINE_PARTNER_DISPLAY_ORDER_CHECK="select afp from AfPartner afp where afp.partnerPosition=:prtPos and afp.status=1 and afp.offLinePartner=1";

	public static final String AF_LOGO_PARTNER_TILE_POS_CHECK="select afp from AfPartner afp where afp.logoPartnerPosition=:prtPos and afp.logoPartner=1 and afp.status=1";
	
	public static final String AF_OFFLINE_PARTNER_TILE_POS_CHECK="select afp from AfPartner afp where afp.logoPartnerPosition=:prtPos and afp.offLinePartner=1 and afp.status=1";

	public static final String AF_HOST_PARTNERS = "select ap from AfPartner ap where ap.status=1 and ap.hostPartner=1 order by ap.partnerPosition";

	public static final String AF_LOGO_PARTNERS = "select ap from AfPartner ap where ap.status=1 and ap.logoPartner=1 order by ap.partnerPosition";
	
	public static final String AF_OFFLINE_PARTNERS = "select ap from AfPartner ap where ap.status=1 and ap.offLinePartner=1 order by ap.partnerPosition";

	public static final String AF_READ_PARTNERS = "select ap from AfPartner ap where ap.status=1 and ap.readPartner=1 order by ap.partnerPosition";
	
	public static final String AF_TALK_PARTNERS = "select ap from AfPartner ap where ap.status=1 and ap.talkPartner=1 order by ap.partnerPosition";
	
	public static final String AF_DEFAULT_PARTNERS = "select ap from AfPartner ap where ap.status=1 and ap.defaultPartner=1 order by ap.partnerPosition";

	public static final String AF_CATEGORY_MAX_CAT_NO = "select max(afcc.catNo) from AfCategory afcc";

	public static final String AF_PRICE_RANGE_FILTER_BY_CAT_NO = "select afpr from AfPriceRangeFilter afpr where afpr.catNo=:condition and afpr.status=1";

	public static final String GET_AF_PRICE_RANGE_FILTER_BY_CAT_NO = "select afpr from AfPriceRangeFilter afpr where afpr.catNo=:condition and afpr.status=1";

	public static final String GET_MAX_CATEGORY_ID = "select max(categoryId) from AfCategory where length(categoryId)=3";
	
	public static final String GET_MAX_SUB_CATEGORY_ID = "select max(substring(categoryId, -3)) from AfCategory where categoryId like :condition";
	
	public static final String GET_CATEGORY_COUNT_BY_CAT_NAME = "select count(*) from AfCategory where lower(categoryName)=:condition and status=1";

	public static final String GET_CATEGORY_BY_CAT_ID = "select afcc from AfCategory afcc where afcc.categoryId like :condition order by afcc.categoryId";

	public static final String AF_PARTNERS_BY_DEFAULT_AND_HOST = "select ap from AfPartner ap where (ap.defaultPartner=1 or ap.hostPartner=1) and ap.status=1";

	public static final String AF_CATEGORY_DISPLAY_ORDER_CHECK = "select count(*) from AfCategory where gqcDisplayOrder=:displayOrder and status=1";

	public static final String AF_PARTNER_KEYWORDS_FILE_NAME = "select afk from AfPartnerKeyword afk where afk.status=1 and afk.afPartner.prtNo=:logoPrtNo";

	public static final String AF_PARTNER_DELETE_KEYWORD="delete AfPartnerKeyword afk where afk.afPartner.prtNo=:prtNo";

	public static final String AF_LOGO_PARTNER_SAVE_CONFIG=" update AfPartner afp set afp.logoPartnerTilePosition=:tileNo where afp.logoPartner=1 and afp.status=1";

	public static final String AF_RECOMMENDED_CATEGORY_BY_CATNO="select afrc from AfRecommendedCategory afrc where afrc.afCategory.catNo=:rcCatno and afrc.status=1";

	public static final String AF_OFFERS_DEALS_BY_PRT_NO = "select afpo from AfPartnerOffer afpo where afpo.status=1 and afpo.afPartner.prtNo=:prtNo";


	public static final String AF_GET_CONTENT_MANAGMENT = "select ac from AfEmailManagement ac where ac.status=1";
	public static final String AF_GET_CONTENT_BY_TYPE = "select ac from AfEmailManagement ac where ac.contentType=:contentType and ac.status=1";

	public static final String GET_PARTNER_CATEGORY_BY_NO = "select afpc from AfPartnerCategory afpc where afpc.afPartner.prtNo=:prtNo and afpc.status=1"; 


	public static final String GET_CAT_NO_COUNT_FROM_AF_PARTNER_OFFER = "select count(*) from AfPartnerOffer where afCategory.catNo=:catNo and status=1";
	public static final String GET_CAT_NO_COUNT_FROM_AF_PARTNER_KEYWORD = "select count(*) from AfPartnerKeyword where afCategory.catNo=:catNo and status=1";
	public static final String GET_CAT_NO_COUNT_FROM_AF_RECOMMENDED_CATEGORY = "select count(*) from AfRecommendedCategory where afCategory.catNo=:catNo and status=1";
	public static final String GET_CAT_NO_COUNT_FROM_AF_PARTNER_CATEGORY = "select count(*) from AfPartnerCategory where afCategory.catNo=:catNo and status=1";

	public static final String GET_AF_CATEGORY_BY_CATEGORY_ID = "select afc from AfCategory afc where categoryId=:categoryID and status=1";

	public static final String AF_COMMISION_EXISTENCE_WITH_PRT_NO="select afpc.prtCommNo from AfPartnerCommission afpc where afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo is null and afpc.afPartnerProduct.prtPrdNo is null and afpc.productBrand is null and afpc.categoryPath is null";

	public static final String AF_COMMISION_EXISTENCE_WITH_PRT_NO_AND_PRT_CAT_NO="select afpc.prtCommNo from AfPartnerCommission afpc where afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo=:prtCatNo and afpc.afPartnerProduct.prtPrdNo is null and afpc.productBrand is null and afpc.categoryPath is null";
	
	public static final String AF_COMMISION_EXISTENCE_WITH_PRT_NO_AND_PRT_CAT_NO_AND_CAT_PATH="select afpc.prtCommNo from AfPartnerCommission afpc where afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo=:prtCatNo and afpc.afPartnerProduct.prtPrdNo is null and afpc.productBrand is null and afpc.categoryPath=:catpath";

	public static final String AF_COMMISION_EXISTENCE_WITH_PRT_NO_AND_PRT_CAT_NO_AND_PRT_PRD_NO="select afpc.prtCommNo from AfPartnerCommission afpc where afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo=:prtCatNo and afpc.afPartnerProduct.prtPrdNo=:prtPrdNo and afpc.productBrand is null and afpc.categoryPath is null";
	
	public static final String AF_COMMISION_EXISTENCE_WITH_PRT_NO_AND_PRT_CAT_NO_AND_BRAND_NAME="select afpc.prtCommNo from AfPartnerCommission afpc where afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo=:prtCatNo and afpc.afPartnerProduct.prtPrdNo is null and afpc.productBrand=:productBrand and afpc.categoryPath is null";
	public static final String AF_COMMISION_EXISTENCE_WITH_PRT_NO_AND_PRT_CAT_NO_AND_CATPATH="select afpc.prtCommNo from AfPartnerCommission afpc where afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo=:prtCatNo and afpc.afPartnerProduct.prtPrdNo is null and afpc.productBrand is null  and afpc.categoryPath is null and afpc.categoryPath=:catpath";
	public static final String AF_COMMISION_EXISTENCE_WITH_PRT_NO_AND_PRT_CAT_NO_AND_BRAND_NAME_AND_CAT_PATH="select afpc.prtCommNo from AfPartnerCommission afpc where afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo=:prtCatNo and afpc.afPartnerProduct.prtPrdNo is null and afpc.productBrand=:productBrand and afpc.categoryPath=:catpath";

	
	public static final String AF_COMMISION_EXISTENCE_ENDDATE_WITH_PRT_NO="select count(*) from AfPartnerCommission afpc where afpc.afPartner.prtNo=:partNo and afpc.deactivationDate>:newtodate and afpc.afPartnerCategory.prtCatNo is null and afpc.afPartnerProduct.prtPrdNo is null and afpc.productBrand is null and afpc.categoryPath is null";
	public static final String AF_COMMISION_EXISTENCE_ENDDATE_WITH_PRT_CAT_NO="select count(*) from AfPartnerCommission afpc where afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo=:partCatNo and afpc.deactivationDate>:newtodate and afpc.afPartnerProduct.prtPrdNo is null and afpc.productBrand is null and afpc.categoryPath is null";
	public static final String AF_COMMISION_EXISTENCE_ENDDATE_WITH_PRODUCT="select count(*) from AfPartnerCommission afpc where afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo=:partCatNo and afpc.afPartnerProduct.prtPrdNo=:partPrdNo and afpc.deactivationDate>:newtodate and afpc.productBrand is null and afpc.categoryPath is null";
	public static final String AF_COMMISION_EXISTENCE_ENDDATE_WITH_BRAND="select count(*) from AfPartnerCommission afpc where afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo=:partCatNo and afpc.productBrand=:brandName and afpc.deactivationDate>:newtodate and afpc.afPartnerProduct.prtPrdNo is null and afpc.categoryPath is null";
	public static final String AF_COMMISION_EXISTENCE_ENDDATE_WITH_BRAND_AND_CAT_PATH="select count(*) from AfPartnerCommission afpc where afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo=:partCatNo and afpc.productBrand=:brandName and afpc.deactivationDate>:newtodate and afpc.afPartnerProduct.prtPrdNo is null and afpc.categoryPath=:catPath";
	public static final String AF_COMMISION_EXISTENCE_ENDDATE_WITH_PRT_CAT_AND_CAT_PATH="select count(*) from AfPartnerCommission afpc where afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo=:partCatNo and afpc.productBrand is null and afpc.deactivationDate>:newtodate and afpc.afPartnerProduct.prtPrdNo is null and afpc.categoryPath=:catPath";
	public static final String AF_COMMISION_EXISTENCE_ENDDATE_WITH_PRT_AND_CAT_PATH="select count(*) from AfPartnerCommission afpc where afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo is null and afpc.productBrand is null and afpc.deactivationDate>:newtodate and afpc.afPartnerProduct.prtPrdNo is null and afpc.categoryPath=:catPath";

	public static final String AF_COMMISION_EXISTENCE_EDIT_ENDDATE_WITH_PRT_NO="select MIN(afpc.activationDate) from AfPartnerCommission afpc where afpc.prtCommNo!=:partComNo and afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo is null and afpc.afPartnerProduct.prtPrdNo is null and afpc.productBrand is null and afpc.categoryPath is null and afpc.activationDate>:endDateHidden";
	public static final String AF_COMMISION_EXISTENCE_EDIT_ENDDATE_WITH_PRT_CAT_NO="select MIN(afpc.activationDate) from AfPartnerCommission afpc where afpc.prtCommNo!=:partComNo and afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo=:partCatNo and afpc.afPartnerProduct.prtPrdNo is null and afpc.productBrand is null and afpc.categoryPath is null and afpc.activationDate>:endDateHidden";
	public static final String AF_COMMISION_EXISTENCE_EDIT_ENDDATE_WITH_PRODUCT="select MIN(afpc.activationDate) from AfPartnerCommission afpc where afpc.prtCommNo!=:partComNo and afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo=:partCatNo and afpc.afPartnerProduct.prtPrdNo=:partPrdNo  and afpc.productBrand is null and afpc.categoryPath is null and afpc.activationDate>:endDateHidden";
	public static final String AF_COMMISION_EXISTENCE_EDIT_ENDDATE_WITH_BRAND="select MIN(afpc.activationDate) from AfPartnerCommission afpc where afpc.prtCommNo!=:partComNo and afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo=:partCatNo and afpc.productBrand=:brandName and afpc.afPartnerProduct.prtPrdNo is null and afpc.categoryPath is null and afpc.activationDate>:endDateHidden";
	public static final String AF_COMMISION_EXISTENCE_EDIT_ENDDATE_WITH_BRAND_AND_CATPATH="select MIN(afpc.activationDate) from AfPartnerCommission afpc where afpc.prtCommNo!=:partComNo and afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo=:partCatNo and afpc.productBrand=:brandName and afpc.afPartnerProduct.prtPrdNo is null and afpc.categoryPath=:catPath and afpc.activationDate>:endDateHidden";
	public static final String AF_COMMISION_EXISTENCE_EDIT_ENDDATE_WITH_PRT_CAT_AND_CATPATH="select MIN(afpc.activationDate) from AfPartnerCommission afpc where afpc.prtCommNo!=:partComNo and afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo=:partCatNo and afpc.productBrand is null and afpc.afPartnerProduct.prtPrdNo is null and afpc.categoryPath=:catPath and afpc.activationDate>:endDateHidden";
	public static final String AF_COMMISION_EXISTENCE_EDIT_ENDDATE_WITH_PRT_AND_CATPATH="select MIN(afpc.activationDate) from AfPartnerCommission afpc where afpc.prtCommNo!=:partComNo and afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo is null and afpc.productBrand is null and afpc.afPartnerProduct.prtPrdNo is null and afpc.categoryPath=:catPath and afpc.activationDate>:endDateHidden";

	public static final String GET_LOGO_PARTNERS_TILE_NUMBER_SEARCH_RESULT="select afp.logoPartnerTilePosition from AfPartner afp where afp.logoPartner=1 and afp.status=1";

	public static final String AF_PRODUCT_BY_CAT_NO = "select afp from AfProduct afp where afp.afCategory.catNo=:catNo and afp.masterProductId in(select afpp.afProduct.masterProductId from AfPartnerProduct afpp where afpp.productStatus=1 and afpp.status=1 ) and  afp.productStatus=1 and afp.status=1";
	
	public static final String GET_ALL_SUB_CATEGORIES_BY_CAT_ID = "select afc from AfCategory afc where afc.categoryId like :condition and afc.status=1 order by afc.categoryId";

	public static final String AF_COMMISION_EXISTENCE_EDIT_END_DATE_WITH_PRT_NO="select afpc.activationDate from AfPartnerCommission afpc where afpc.prtCommNo!=:partComNo and afpc.afPartner.prtNo=:partNo and afpc.afPartnerCategory.prtCatNo is null and afpc.afPartnerProduct.prtPrdNo is null and afpc.productBrand is null and afpc.activationDate>:endDate";

	public static final String IS_CAT_NO_MAPPED_TO_OTHER_TABLES = "select EXISTS (select CAT_NO from AF_RECOMMENDED_CATEGORY where CAT_NO = ? and status=1) OR EXISTS (select CAT_NO from AF_PARTNER_CATEGORY where CAT_NO = ? and status=1) OR EXISTS (select CAT_NO from AF_PARTNER_OFFER where CAT_NO = ? and status=1) OR EXISTS (select CAT_NO from AF_PARTNER_KEYWORD where CAT_NO = ? and status=1)";
	
	public static final String AF_BANNER_BY_BANNER_ID="select banner from AfBanner banner where banner.bannerId=:bannerId and banner.status=1";
	
	public static final String GET_CATEGORY_BY_CAT_NAME = "select afc from AfCategory afc where afc.categoryName=:categoryName";
	public static final String GET_PARTNER_BY_PRT_NAME = "select afp from AfPartner afp where afp.partnerName=:partnerName";
	
	public static final String GET_BEST_SELLING_CATEGORIES = "select afc.categoryName from AfCategory afc where afc.bestSellingCategory=1";
	
	public static final String AF_JPMILE_SAVE_CONFIG = "Update AfEnumValues set value=:jpMile where typeName=:typeName";
	
	//Get SEO Data
	public static final String GET_SEO_DATA = "select afs from AfSeo afs where afs.seoPageId is not null and status=1";
	public static final String GET_SEO_DATA_BY_PAGE_ID = "select afs from AfSeo afs where afs.seoPageId=:pageId and status=1";
	
	public static final String GET_AF_PARTNER_OFFER_BY_COND = "select afpo from AfPartnerOffer afpo where afpo.status=1 ";
	
	public static final String AF_PARTNERS_BY_ACTIVITY_ID="select partners from AfPartner partners where partners.activityId=:activityId and partners.status=1";
	
	public static final String AF_OFFERS_DEALS_SL_NO="select max(prtOffrNo) from AfPartnerOffer where status=1";
	
	public static final String AF_SECTIONS_ALL="select section from AfSections section where section.sectionType=:sectionType and section.status=1";
	
	public static final String AF_SECTIONS_BY_SECTION="select section from AfSections section where section.sectId=:sectId and section.status=1";
	
	public static final String AF_SECTIONS = "select as from AfSections as where as.sectionType=1 and as.sectionStatus=1 and as.status=1";
	
	public static final String AF_SECTIONS_BY_SECTION_TITLE="select section from AfSections section where section.sectionTitle=:sectionTitle and section.status=1";
	
	public static final String AF_SECTION_DISPLAY_ORDER_CHECK="select afs from AfSections afs where afs.sectionDispOrder=:sectionDispOrder and afs.status=1";

	public static final String AF_SECTION_ITEM="select si from AfSectionItems si where si.afSections.sectId=:sectId and si.status=1";
	
	public static final String AF_SECTION_ITEM_BY_ITEM_ID="select si from AfSectionItems si where si.itemId=:sectionItemId and si.status=1";
	
	public static final String AF_ENUM_VALUES_BY_ID = "select aev from AfEnumValues aev where aev.typeName=:typeName and aev.intCode=:intCode";

	public static final String AF_SECTION_ITEMS_DISPLAY_ORDER="select afsi from AfSectionItems afsi where afsi.displayOrder=:displayOrder and afsi.afSections.sectId=:sectionId and afsi.status=1";
	
	public static final String AF_SECTION_ITEMS_DUPLICATE_RECORD_PRDT="select afsi from AfSectionItems afsi where afsi.afSections.sectId=:sectionId and afsi.itemType=:itemType and afsi.afPartner.prtNo=:prtNo and afsi.afPartnerProduct.prtPrdNo=:prtPrdctNo and afsi.afCategory.catNo is null and afsi.afPartnerOffer.prtOffrNo is null and afsi.status=1";

	public static final String AF_SECTION_ITEMS_DUPLICATE_RECORD_CAT="select afsi from AfSectionItems afsi where afsi.afSections.sectId=:sectionId and afsi.itemType=:itemType and afsi.afCategory.catNo=:catNo and afsi.afPartner.prtNo is null and afsi.afPartnerProduct.prtPrdNo is null and afsi.afPartnerOffer.prtOffrNo is null and afsi.status=1";

	public static final String AF_SECTION_ITEMS_DUPLICATE_RECORD_PRT="select afsi from AfSectionItems afsi where afsi.afSections.sectId=:sectionId and afsi.itemType=:itemType and afsi.afPartner.prtNo=:prtNo and afsi.afCategory.catNo is null and afsi.afPartnerProduct.prtPrdNo is null and afsi.afPartnerOffer.prtOffrNo is null and afsi.status=1";

	public static final String AF_SECTION_ITEMS_DUPLICATE_RECORD_OFFERS_DEALS="select afsi from AfSectionItems afsi where afsi.afSections.sectId=:sectionId and afsi.itemType=:itemType and afsi.afPartner.prtNo=:prtNo and afsi.afCategory.catNo=:catNo and afsi.afPartnerOffer.prtOffrNo=:prtOfferNo and afsi.afPartnerProduct.prtPrdNo is null and afsi.status=1";
	
//	public static final String GET_ALL_PARTNERS_FOR_WEBSERVICE = "select ap from AfPartner ap where ap.readPartner !=1 and ap.talkPartner !=1  order by ap.prtNo";
	public static final String GET_ALL_PARTNERS_FOR_WEBSERVICE = "select ap from AfPartner ap order by ap.prtNo";
	
	//public static final String AF_PRT_COMMISSION_WS = "select afpc from AfPartnerCommission afpc where afpc.afPartner.prtNo is not null and afpc.afPartnerCategory.prtCatNo is null and afpc.afPartnerProduct.prtPrdNo is null and afpc.productBrand is null and afpc.categoryPath is null";
	
//	public static final String AF_PRT_COMMISSION_WS = "select afpc from AfPartnerCommission afpc where afpc.afPartner.prtNo is not null and afpc.afPartnerCategory.prtCatNo is null and afpc.afPartnerProduct.prtPrdNo is null and afpc.productBrand is null and afpc.categoryPath is null and afpc.afPartner.readPartner=0 and afpc.afPartner.talkPartner=0";
	
	public static final String AF_PRT_COMMISSION_WS = "select afpc from AfPartnerCommission afpc where afpc.afPartner.prtNo is not null and afpc.afPartnerCategory.prtCatNo is null and afpc.afPartnerProduct.prtPrdNo is null and afpc.productBrand is null and afpc.categoryPath is null";
	
	public static final String AF_HOME_SECTIONS = "select ahps from AfHomePageSection ahps order by sectionPosition";
	
	public static final String UPDATE_AF_HOME_PAGE_SECTIONS_BY_NAME="update AfHomePageSection set sectionName=:sectionName, modifiedTime=Now(), modifiedBy=1, status=:status, sectionPosition=:orderValue where sectionName=:exitValue";
	
//	public static final String GET_BUSINESS_REPORT_FOR_TENTATIVE = "SELECT tr.JP_NUMBER,tr.SOURCE,tr.MEDIUM,tr.CAMPAIGN,tr.MEMBER_OR_GUEST,pr1.TRANSACTION_ID,pr1.ORDER_ID,pr1.FORMATTED_ORDER_DATE,pr1.PARTNER_ID,td.PRICE,td.RATE_PER_MILE,td.COMMISSION_RATE,td.MEMBER_SHARE,td.JPMILES_PURCHASE,td.EXPECTED_DATE_OF_CREDIT,td.JPMILES_CANCEL,td.JPMILES_TO_CREDIT,"
//			+ "td.PRODUCT_STATUS,td.PARTNER_PRODUCT_ID,tr.TRANSACTION_TYPE,pr1.COMM_AMOUNT,pr1.PRT_NO,pr1.CATEGORY FROM PARTNER_REPORT_IN pr1 JOIN AF_TRANSACTION_DETAILS td ON td.PARTNER_ITEM_ID = pr1.ORDER_ID AND td.PRT_NO = pr1.PRT_NO AND td.PRODUCT_STATUS = 1 LEFT JOIN AF_TRANSACTION tr ON tr.TXN_NO = td.TXN_NO "
//			+ "LEFT JOIN AF_PARTNER_PRODUCT prod ON td.PARTNER_PRODUCT_ID = prod.PRODUCT_ID AND td.PRT_NO = prod.PRT_NO LEFT JOIN AF_PARTNER_CATEGORY cat ON prod.PRT_CAT_NO = cat.PRT_CAT_NO AND td.PRT_NO = cat.PRT_NO WHERE ";
//		
//	public static final String GET_BUSINESS_REPORT_FOR_PROCESSED = "SELECT tr.JP_NUMBER,tr.SOURCE,tr.MEDIUM,tr.CAMPAIGN,tr.MEMBER_OR_GUEST,pr1.TRANSACTION_ID,pr1.ORDER_ID,pr1.FORMATTED_ORDER_DATE,pr1.PARTNER_ID,td.PRICE,td.RATE_PER_MILE,td.COMMISSION_RATE,td.MEMBER_SHARE,td.JPMILES_PURCHASE,td.EXPECTED_DATE_OF_CREDIT,td.JPMILES_CANCEL,td.JPMILES_TO_CREDIT,"
//			+ "td.PRODUCT_STATUS,td.PARTNER_PRODUCT_ID,tr.TRANSACTION_TYPE,pr1.COMM_AMOUNT,pr1.PRT_NO,pr1.CATEGORY  FROM PARTNER_REPORT_IN pr1 JOIN AF_TRANSACTION_DETAILS td ON td.PARTNER_ITEM_ID = pr1.ORDER_ID AND td.PRT_NO = pr1.PRT_NO AND td.PRODUCT_STATUS = 2 LEFT JOIN AF_TRANSACTION tr ON tr.TXN_NO = td.TXN_NO LEFT JOIN AF_PARTNER_PRODUCT "
//			+ "prod ON td.PARTNER_PRODUCT_ID = prod.PRODUCT_ID AND td.PRT_NO = prod.PRT_NO LEFT JOIN AF_PARTNER_CATEGORY AS cat ON prod.PRT_CAT_NO = cat.PRT_CAT_NO AND td.PRT_NO = cat.PRT_NO WHERE ";
//		
//	public static final String GET_BUSINESS_REPORT_FOR_FAILED = "SELECT tr.JP_NUMBER,tr.SOURCE,tr.MEDIUM,tr.CAMPAIGN,tr.MEMBER_OR_GUEST,pr1.TRANSACTION_ID,pr1.ORDER_ID,pr1.FORMATTED_ORDER_DATE,pr1.PARTNER_ID,td.PRICE,td.RATE_PER_MILE,td.COMMISSION_RATE,td.MEMBER_SHARE,td.JPMILES_PURCHASE,td.EXPECTED_DATE_OF_CREDIT,td.JPMILES_CANCEL,td.JPMILES_TO_CREDIT,"
//			+ "td.PRODUCT_STATUS,td.PARTNER_PRODUCT_ID,tr.TRANSACTION_TYPE,pr1.COMM_AMOUNT,pr1.PRT_NO,pr1.CATEGORY FROM PARTNER_REPORT_IN pr1 JOIN AF_TRANSACTION_DETAILS td ON td.PARTNER_ITEM_ID = pr1.ORDER_ID AND td.PRT_NO = pr1.PRT_NO AND td.PRODUCT_STATUS = 4 LEFT JOIN AF_TRANSACTION tr ON tr.TXN_NO = td.TXN_NO LEFT JOIN AF_PARTNER_PRODUCT prod ON "
//			+ "td.PARTNER_PRODUCT_ID = prod.PRODUCT_ID AND td.PRT_NO = prod.PRT_NO LEFT JOIN AF_PARTNER_CATEGORY AS cat ON prod.PRT_CAT_NO = cat.PRT_CAT_NO AND td.PRT_NO = cat.PRT_NO WHERE ";

	
	public static final String GET_BUSINESS_REPORT_FOR_TENTATIVE = "SELECT tr.JP_NUMBER,tr.SOURCE,tr.MEDIUM,tr.CAMPAIGN,tr.MEMBER_OR_GUEST,pr1.TRANSACTION_ID,pr1.ORDER_ID,pr1.FORMATTED_ORDER_DATE,pr1.PARTNER_ID,td.PRICE,td.RATE_PER_MILE,td.COMMISSION_RATE,td.MEMBER_SHARE,td.JPMILES_PURCHASE,td.EXPECTED_DATE_OF_CREDIT,td.JPMILES_CANCEL,td.JPMILES_TO_CREDIT,"
			+ "td.PRODUCT_STATUS,td.PARTNER_PRODUCT_ID,tr.TRANSACTION_TYPE,pr1.COMM_AMOUNT,pr1.PRT_NO,pr1.CATEGORY ,td.BONUS_JP_MILES,pr1.PARTNER_ORDER_NUMBER FROM PARTNER_REPORT_IN pr1 JOIN AF_TRANSACTION_DETAILS td ON td.PARTNER_ITEM_ID = pr1.ORDER_ID AND td.PRT_NO = pr1.PRT_NO AND td.PRODUCT_STATUS = 1 LEFT JOIN AF_TRANSACTION tr ON tr.TXN_NO = td.TXN_NO "
			+ "LEFT JOIN AF_PARTNER_PRODUCT prod ON td.PARTNER_PRODUCT_ID = prod.PRODUCT_ID AND td.PRT_NO = prod.PRT_NO LEFT JOIN AF_PARTNER_CATEGORY cat ON prod.PRT_CAT_NO = cat.PRT_CAT_NO AND td.PRT_NO = cat.PRT_NO WHERE ";
	public static final String GET_BUSINESS_REPORT_FOR_PROCESSED = "SELECT tr.JP_NUMBER,tr.SOURCE,tr.MEDIUM,tr.CAMPAIGN,tr.MEMBER_OR_GUEST,pr1.TRANSACTION_ID,pr1.ORDER_ID,pr1.FORMATTED_ORDER_DATE,pr1.PARTNER_ID,td.PRICE,td.RATE_PER_MILE,td.COMMISSION_RATE,td.MEMBER_SHARE,td.JPMILES_PURCHASE,td.EXPECTED_DATE_OF_CREDIT,td.JPMILES_CANCEL,td.JPMILES_TO_CREDIT,"
			+ "td.PRODUCT_STATUS,td.PARTNER_PRODUCT_ID,tr.TRANSACTION_TYPE,pr1.COMM_AMOUNT,pr1.PRT_NO,pr1.CATEGORY,td.BONUS_JP_MILES,pr1.PARTNER_ORDER_NUMBER  FROM PARTNER_REPORT_IN pr1 JOIN AF_TRANSACTION_DETAILS td ON td.PARTNER_ITEM_ID = pr1.ORDER_ID AND td.PRT_NO = pr1.PRT_NO AND td.PRODUCT_STATUS = 2 LEFT JOIN AF_TRANSACTION tr ON tr.TXN_NO = td.TXN_NO LEFT JOIN AF_PARTNER_PRODUCT "
			+ "prod ON td.PARTNER_PRODUCT_ID = prod.PRODUCT_ID AND td.PRT_NO = prod.PRT_NO LEFT JOIN AF_PARTNER_CATEGORY AS cat ON prod.PRT_CAT_NO = cat.PRT_CAT_NO AND td.PRT_NO = cat.PRT_NO WHERE ";
	
	
	public static final String GET_BUSINESS_REPORT_FOR_FAILED = "SELECT tr.JP_NUMBER,tr.SOURCE,tr.MEDIUM,tr.CAMPAIGN,tr.MEMBER_OR_GUEST,pr1.TRANSACTION_ID,pr1.ORDER_ID,pr1.FORMATTED_ORDER_DATE,pr1.PARTNER_ID,td.PRICE,td.RATE_PER_MILE,td.COMMISSION_RATE,td.MEMBER_SHARE,td.JPMILES_PURCHASE,td.EXPECTED_DATE_OF_CREDIT,td.JPMILES_CANCEL,td.JPMILES_TO_CREDIT,"
			+ "td.PRODUCT_STATUS,td.PARTNER_PRODUCT_ID,tr.TRANSACTION_TYPE,pr1.COMM_AMOUNT,pr1.PRT_NO,pr1.CATEGORY ,td.BONUS_JP_MILES,pr1.PARTNER_ORDER_NUMBER FROM PARTNER_REPORT_IN pr1 JOIN AF_TRANSACTION_DETAILS td ON td.PARTNER_ITEM_ID = pr1.ORDER_ID AND td.PRT_NO = pr1.PRT_NO AND td.PRODUCT_STATUS = 4 LEFT JOIN AF_TRANSACTION tr ON tr.TXN_NO = td.TXN_NO LEFT JOIN AF_PARTNER_PRODUCT prod ON "
			+ "td.PARTNER_PRODUCT_ID = prod.PRODUCT_ID AND td.PRT_NO = prod.PRT_NO LEFT JOIN AF_PARTNER_CATEGORY AS cat ON prod.PRT_CAT_NO = cat.PRT_CAT_NO AND td.PRT_NO = cat.PRT_NO WHERE ";
	
	
	
	
	public static final String GET_DAILY_MILES_STATUS_REPORT = "select new com.af.admin.beans.BuisnessReportsBean(tr.jpNumber,td.partnerId,td.partnerItemId,td.transactionDate,td.expectedDateOfCredit,td.transactionId,td.jpmilesPurchase,td.jpmileStatus,td.partnerProductId,tr.transactionType)"
			+"from AfTransactionDetails td,AfTransaction tr where td.afTransaction.txnNo = tr.txnNo and td.productStatus=2 and ";
	
	public static final String GET_ENUM_VALUE_BY_TYPENAME="select aev from AfEnumValues aev where aev.typeName=:typeName";
	
	public static final String UPDATE_ENUM_VALUE_BY_TYPENAME = "Update AfEnumValues aev set aev.value=:value where aev.typeName='SELLER_TEXT'";
	
	public static final String AF_GET_CRIS_REQUEST="select acr.crisRequest from AfCrisMilesRequestResponse acr where acr.orderId=:orderId order by acr.orderId desc";
	
	public static final String AF_GET_CRIS_RESPONSE="select acr.crisResponse from AfCrisMilesRequestResponse acr where acr.orderId=:orderId order by acr.orderId desc";

	public static final String AF_SECTION_ITEMS_DUPLICATE_RECORD_BRANDS="select afsi from AfSectionItems afsi where afsi.afSections.sectId=:sectionId and afsi.itemType=:itemType and afsi.brandName=:brandName and afsi.status=1";

	public static final String AF_CUSTOM_SECTION_BY_TITLE ="select asn from AfSections asn where asn.sectionTitle=:title";
	
	/*public static final String GET_BUSINESS_REPORT_FOR_ALL="select tr.JP_NUMBER,tr.SOURCE,tr.MEDIUM,tr.CAMPAIGN,tr.MEMBER_OR_GUEST,pr1.TRANSACTION_ID,pr1.ORDER_ID,pr1.FORMATTED_ORDER_DATE,"+
			"pr1.PARTNER_ID,td.PRICE,td.RATE_PER_MILE,td.COMMISSION_RATE,td.MEMBER_SHARE,td.JPMILES_PURCHASE,"+
			"td.EXPECTED_DATE_OF_CREDIT,td.JPMILES_CANCEL,td.JPMILES_TO_CREDIT,td.PRODUCT_STATUS,td.PARTNER_PRODUCT_ID,tr.TRANSACTION_TYPE,FORMAT(FLOOR(pr1.COMM_AMOUNT),0) from PARTNER_REPORT_IN  pr1 "+
			"left join AF_TRANSACTION_DETAILS td on td.PARTNER_ITEM_ID = pr1.ORDER_ID and td.PRT_NO = pr1.PRT_NO left join AF_TRANSACTION tr on tr.TRANSACTION_ID = pr1.TRANSACTION_ID left join AF_PARTNER_PRODUCT prod on td.PARTNER_PRODUCT_ID = prod.PRODUCT_ID and td.PRT_NO = prod.PRT_NO left join AF_PARTNER_CATEGORY as cat on prod.PRT_CAT_NO = cat.PRT_CAT_NO where pr1.ORDER_STATUS in (select PARTNER_STATUS from PARTNER_ORDER_STATUS where PRT_NO =:prtNo) and ";
	*/
	public static final String GET_TRANSACTION_RANGE_FILTER=" and ((:lowerValue is null and :upperValue is null and afpc.lowerValue is null and afpc.upperValue is null) or "+
														"(:lowerValue is null and afpc.lowerValue is null) or "+
														"(afpc.lowerValue is null and afpc.upperValue is null) or"+
														"(:lowerValue is null and :upperValue is null) or "+
														"(:upperValue is null and afpc.upperValue is null) or "+
														"(:lowerValue is null and :upperValue !=null and afpc.lowerValue !=null and :upperValue>afpc.lowerValue) or "+
														"(:upperValue is null and :lowerValue !=null and afpc.lowerValue !=null and :lowerValue<afpc.upperValue) or "+
														"(:lowerValue !=null and afpc.lowerValue !=null and afpc.upperValue !=null and :lowerValue between afpc.lowerValue and afpc.upperValue) or "+
														"(:upperValue !=null and afpc.lowerValue !=null and afpc.upperValue !=null and :upperValue between afpc.lowerValue and afpc.upperValue) or (:lowerValue !=null and :upperValue !=null and afpc.lowerValue !=null and :lowerValue<=afpc.lowerValue and :upperValue >=afpc.lowerValue))";


	public static final String AF_READ_PARTNER_DISPLAY_ORDER_CHECK="select afp from AfPartner afp where afp.partnerPosition=:prtPos and afp.status=1 and afp.readPartner=1";
	
	public static final String AF_TALK_PARTNER_DISPLAY_ORDER_CHECK="select afp from AfPartner afp where afp.partnerPosition=:prtPos and afp.status=1 and afp.talkPartner=1";
	
	/*public static final String GET_BUSINESS_REPORT_FOR_ALL_INVALID="select tr.JP_NUMBER,tr.SOURCE,tr.MEDIUM,tr.CAMPAIGN,tr.MEMBER_OR_GUEST,pr1.TRANSACTION_ID,pr1.ORDER_ID,pr1.FORMATTED_ORDER_DATE,"+
			"pr1.PARTNER_ID,td.PRICE,td.RATE_PER_MILE,td.COMMISSION_RATE,td.MEMBER_SHARE,td.JPMILES_PURCHASE,"+
			"td.EXPECTED_DATE_OF_CREDIT,td.JPMILES_CANCEL,td.JPMILES_TO_CREDIT,td.PRODUCT_STATUS,td.PARTNER_PRODUCT_ID,tr.TRANSACTION_TYPE,FORMAT(FLOOR(pr1.COMM_AMOUNT),0),pr1.PRT_NO from PARTNER_REPORT_IN  pr1 "+
			"left join AF_TRANSACTION_DETAILS td on td.PARTNER_ITEM_ID = pr1.ORDER_ID and td.PRT_NO = pr1.PRT_NO left join AF_TRANSACTION tr on tr.TRANSACTION_ID = pr1.TRANSACTION_ID left join AF_PARTNER_PRODUCT prod on td.PARTNER_PRODUCT_ID = prod.PRODUCT_ID and td.PRT_NO = prod.PRT_NO left join AF_PARTNER_CATEGORY as cat on prod.PRT_CAT_NO = cat.PRT_CAT_NO where td. PRODUCT_STATUS is null and  pr1.ORDER_STATUS in (select PARTNER_STATUS from PARTNER_ORDER_STATUS where PRT_NO =:prtNo) and ";

	public static final String GET_BUSINESS_REPORT_FOR_ALL_VALID="select tr.JP_NUMBER,tr.SOURCE,tr.MEDIUM,tr.CAMPAIGN,tr.MEMBER_OR_GUEST,pr1.TRANSACTION_ID,pr1.ORDER_ID,pr1.FORMATTED_ORDER_DATE,"+
			"pr1.PARTNER_ID,td.PRICE,td.RATE_PER_MILE,td.COMMISSION_RATE,td.MEMBER_SHARE,td.JPMILES_PURCHASE,"+
			"td.EXPECTED_DATE_OF_CREDIT,td.JPMILES_CANCEL,td.JPMILES_TO_CREDIT,td.PRODUCT_STATUS,td.PARTNER_PRODUCT_ID,tr.TRANSACTION_TYPE,FORMAT(FLOOR(pr1.COMM_AMOUNT),0),pr1.PRT_NO from PARTNER_REPORT_IN  pr1 "+
			"left join AF_TRANSACTION_DETAILS td on td.PARTNER_ITEM_ID = pr1.ORDER_ID and td.PRT_NO = pr1.PRT_NO left join AF_TRANSACTION tr on tr.TRANSACTION_ID = pr1.TRANSACTION_ID left join AF_PARTNER_PRODUCT prod on td.PARTNER_PRODUCT_ID = prod.PRODUCT_ID and td.PRT_NO = prod.PRT_NO left join AF_PARTNER_CATEGORY as cat on prod.PRT_CAT_NO = cat.PRT_CAT_NO where td. PRODUCT_STATUS is not null and  pr1.ORDER_STATUS in (select PARTNER_STATUS from PARTNER_ORDER_STATUS where PRT_NO =:prtNo) and ";
			
*/
//	public static final String GET_BUSINESS_REPORT_FOR_ALL_VALID="SELECT tr.JP_NUMBER,tr.SOURCE,tr.MEDIUM,tr.CAMPAIGN,tr.MEMBER_OR_GUEST,td.TRANSACTION_ID,td.PARTNER_ITEM_ID AS ORDER_ID,td.TRANSACTION_DATE,td.PARTNER_ID,td.PRICE,td.RATE_PER_MILE,td.COMMISSION_RATE,td.MEMBER_SHARE,td.JPMILES_PURCHASE,td.EXPECTED_DATE_OF_CREDIT,"+
//			"td.JPMILES_CANCEL,td.JPMILES_TO_CREDIT,td.PRODUCT_STATUS,td.PARTNER_PRODUCT_ID,tr.TRANSACTION_TYPE,pr1.COMM_AMOUNT,pr1.PRT_NO,pr1.CATEGORY, td.BONUS_JP_MILES  "+
//			"FROM PARTNER_REPORT_IN pr1 JOIN AF_TRANSACTION_DETAILS td ON td.PARTNER_ITEM_ID = pr1.ORDER_ID AND td.PRT_NO = pr1.PRT_NO and pr1.ID = td.TRACKING_ID LEFT JOIN "+
//			"AF_TRANSACTION tr ON tr.TXN_NO = td.TXN_NO LEFT JOIN AF_PARTNER_PRODUCT prod ON td.PARTNER_PRODUCT_ID = prod.PRODUCT_ID AND td.PRT_NO = "+
//			"prod.PRT_NO LEFT JOIN AF_PARTNER_CATEGORY AS cat ON prod.PRT_CAT_NO = cat.PRT_CAT_NO AND td.PRT_NO = cat.PRT_NO WHERE ";
//	
//	public static final String GET_BUSINESS_REPORT_FOR_ALL_INVALID="SELECT tr.JP_NUMBER,tr.SOURCE,tr.MEDIUM,tr.CAMPAIGN,tr.MEMBER_OR_GUEST,pr1.TRANSACTION_ID,pr1.ORDER_ID,pr1.FORMATTED_ORDER_DATE,pr1.PARTNER_ID,pr1.NET_PRICE,'' as RATE_PER_MILE,'' as COMMISSION_RATE,'' as MEMBER_SHARE,'' as JPMILES_PURCHASE,'' as EXPECTED_DATE_OF_CREDIT,"+
//			"'' as JPMILES_CANCEL,'' as JPMILES_TO_CREDIT,'' as PRODUCT_STATUS,'' as PARTNER_PRODUCT_ID,tr.TRANSACTION_TYPE,pr1.COMM_AMOUNT,pr1.PRT_NO,pr1.CATEGORY "+
//			"FROM PARTNER_REPORT_IN pr1 LEFT JOIN AF_TRANSACTION tr ON tr.TRANSACTION_ID = pr1.TRANSACTION_ID "+
//			"WHERE NOT EXISTS (SELECT 1 FROM AF_TRANSACTION_DETAILS td WHERE td.PARTNER_ITEM_ID = pr1.ORDER_ID AND td.PRT_NO = pr1.PRT_NO) AND ";

	public static final String GET_BUSINESS_REPORT_FOR_ALL_VALID="SELECT tr.JP_NUMBER,tr.SOURCE,tr.MEDIUM,tr.CAMPAIGN,tr.MEMBER_OR_GUEST,td.TRANSACTION_ID,td.PARTNER_ITEM_ID AS ORDER_ID,td.TRANSACTION_DATE,td.PARTNER_ID,td.PRICE,td.RATE_PER_MILE,td.COMMISSION_RATE,td.MEMBER_SHARE,td.JPMILES_PURCHASE,td.EXPECTED_DATE_OF_CREDIT,"+
			"td.JPMILES_CANCEL,td.JPMILES_TO_CREDIT,td.PRODUCT_STATUS,td.PARTNER_PRODUCT_ID,tr.TRANSACTION_TYPE,pr1.COMM_AMOUNT,pr1.PRT_NO,pr1.CATEGORY, td.BONUS_JP_MILES,pr1.PARTNER_ORDER_NUMBER "+
			"FROM PARTNER_REPORT_IN pr1 JOIN AF_TRANSACTION_DETAILS td ON td.PARTNER_ITEM_ID = pr1.ORDER_ID AND td.PRT_NO = pr1.PRT_NO and pr1.ID = td.TRACKING_ID LEFT JOIN "+
			"AF_TRANSACTION tr ON tr.TXN_NO = td.TXN_NO LEFT JOIN AF_PARTNER_PRODUCT prod ON td.PARTNER_PRODUCT_ID = prod.PRODUCT_ID AND td.PRT_NO = "+
			"prod.PRT_NO LEFT JOIN AF_PARTNER_CATEGORY AS cat ON prod.PRT_CAT_NO = cat.PRT_CAT_NO AND td.PRT_NO = cat.PRT_NO WHERE ";
	
//	public static final String GET_BUSINESS_REPORT_FOR_ALL_INVALID="SELECT tr.JP_NUMBER,tr.SOURCE,tr.MEDIUM,tr.CAMPAIGN,tr.MEMBER_OR_GUEST,pr1.TRANSACTION_ID,pr1.ORDER_ID,pr1.FORMATTED_ORDER_DATE,pr1.PARTNER_ID,pr1.NET_PRICE,'' as RATE_PER_MILE,'' as COMMISSION_RATE,'' as MEMBER_SHARE,'' as JPMILES_PURCHASE,'' as EXPECTED_DATE_OF_CREDIT,"+
//			"'' as JPMILES_CANCEL,'' as JPMILES_TO_CREDIT,'' as PRODUCT_STATUS,'' as PARTNER_PRODUCT_ID,tr.TRANSACTION_TYPE,pr1.COMM_AMOUNT,pr1.PRT_NO,pr1.CATEGORY ,'',pr1.PARTNER_ORDER_NUMBER"+
//			"FROM PARTNER_REPORT_IN pr1 LEFT JOIN AF_TRANSACTION tr ON tr.TRANSACTION_ID = pr1.TRANSACTION_ID "+
//			"WHERE NOT EXISTS (SELECT 1 FROM AF_TRANSACTION_DETAILS td WHERE td.PARTNER_ITEM_ID = pr1.ORDER_ID AND td.PRT_NO = pr1.PRT_NO) AND ";
//	
	
	public static final String GET_BUSINESS_REPORT_FOR_ALL_INVALID="SELECT tr.JP_NUMBER,tr.SOURCE,tr.MEDIUM,tr.CAMPAIGN,tr.MEMBER_OR_GUEST,pr1.TRANSACTION_ID,pr1.ORDER_ID,pr1.FORMATTED_ORDER_DATE,pr1.PARTNER_ID,pr1.NET_PRICE,'' as RATE_PER_MILE,'' as COMMISSION_RATE,'' as MEMBER_SHARE,'' as JPMILES_PURCHASE,'' as EXPECTED_DATE_OF_CREDIT,"+
			"'' as JPMILES_CANCEL,'' as JPMILES_TO_CREDIT,'' as PRODUCT_STATUS,'' as PARTNER_PRODUCT_ID,tr.TRANSACTION_TYPE,pr1.COMM_AMOUNT,pr1.PRT_NO,pr1.CATEGORY ,'' as BONUS_JP_MILES,pr1.PARTNER_ORDER_NUMBER"+
			"  FROM PARTNER_REPORT_IN pr1 LEFT JOIN AF_TRANSACTION tr ON tr.TRANSACTION_ID = pr1.TRANSACTION_ID "+
			"WHERE NOT EXISTS (SELECT 1 FROM AF_TRANSACTION_DETAILS td WHERE td.PARTNER_ITEM_ID = pr1.ORDER_ID AND td.PRT_NO = pr1.PRT_NO) AND ";
        
        
        public static final String GET_USER_ACT_LOG_WITH_START_AND_END_DATE="select ual from AfUserActivityLog ual where ual.userActCreatedDateTime >=:fromDate and ual.userActCreatedDateTime<=:toDate order by ual.userActId desc";
	
	public static final String GET_USER_ACT_LOG_WITH_START_DATE_ONLY="select ual from AfUserActivityLog ual where ual.userActCreatedDateTime >=:fromDate order by ual.userActId desc";
	
	public static final String GET_USER_ACT_LOG_WITH_END_DATE_ONLY="select ual from AfUserActivityLog ual where ual.userActCreatedDateTime<=:toDate order by ual.userActId desc";
	
	public static final String GET_PRT_CATEGORY_BY_CAT_NAME = "select afpc from AfPartnerCategory afpc where afpc.partnerCategoryName=:afPartnerCategory";

	public static final String GET_CATEGORY_COUNT_BY_PART_CAT_NAME = "select count(*) from AfPartnerCategory afpc where lower(afpc.partnerCategoryName)=:condition and afpc.afPartner.prtNo=:prtNo and afpc.status=1";

	public static final String GET_CATEGORY_COUNT_BY_PART_CAT_ID = "select count(*) from AfPartnerCategory where lower(partnerCategoryId)=:condition and status=1";

	public static final String AF_CATEGORY_WHERE_PARTNER_CATEGORY_IS_NOT_EMPTY_EXCEPT_SEARCH="from AfPartnerCategory partnercategory where partnercategory.status=1 ";

	public static final String GET_ALL_PARTNERS_EXCEPT_SEARCH = "select ap from AfPartner ap where ap.status=1 and ap.defaultPartner=0 order by ap.prtNo";
 
	public static final String AF_READ_PARTNER_TILE_POS_CHECK="select afp from AfPartner afp where afp.logoPartnerPosition=:prtPos and afp.readPartner=1 and afp.status=1";

	public static final String AF_TALK_PARTNER_TILE_POS_CHECK="select afp from AfPartner afp where afp.logoPartnerPosition=:prtPos and afp.talkPartner=1 and afp.status=1";
	
	public static final String AF_LOGO_PARTNER_TILE_POS_CHECKS="select afp from AfPartner afp where afp.logoPartnerPosition=:prtPos and afp.status=1";

	public static final String AF_TRANSACTION_DETAILS_WITH_ORDER_ID_AND_TRANS_ID = "select tx from AfTransactionDetails tx where tx.partnerItemId=:affOrderId and tx.transactionId=:affTransId";
	
	public static final String AF_TRANSACTION__WITH_TRANSACTION_ID = "select aftx from AfTransaction aftx where aftx.transactionId=:tranxId";

	public static final String GET_FAILED_TRANSACTION_STATUS_REPORT = "select new com.af.admin.beans.BuisnessReportsBean(tr.jpNumber,td.partnerId,td.partnerItemId,td.transactionDate,td.expectedDateOfCredit,td.transactionId,td.jpmilesPurchase,td.jpmileStatus,td.partnerProductId,tr.transactionType)"
			+"from AfTransactionDetails td,AfTransaction tr where td.afTransaction.txnNo = tr.txnNo and td.productStatus=2 and td.jpmileStatus=4 and";
	
	public static final String AF_GET_OREDER_COUNT = "select count(*) from AfCrisMilesRequestResponse acr where acr.orderId=:orderId" ;

	public static final String AF_PRT_COMMISSION_BY_NAME = "select apc from AfPartnerCommission apc where apc.status=1 and apc.afPartner.status=1 and apc.afPartner.partnerName=:partnerName";

	public static final String AF_PRT_NO = "select ap.prtNo from AfPartner ap where ap.partnerName=:partnerName";

	public static final String AF_PRT_CATEGORY_NO = "select af.prtCatNo from AfPartnerCategory af where af.partnerCategoryName=:prtCategoryName and af.afPartner.prtNo=:prtNo";
	
	public static final String AF_PRT_PRODUCT_NO = "select distinct ap.prtPrdNo from AfPartnerProduct ap where ap.productName=:prtProductName and ap.afPartner.prtNo=:prtNo and ap.afPartnerCategory.prtCatNo=:prtCat";
	
	public static final String AF_PARTNERCOMMISSIONUPLOAD = "select afpc from AfPartnerCommission afpc where afpc.status=1 and afpc.afPartner.status=1";
	
	public static final String AF_PRT_CATEGORY_PATH = "select ap.categoryPath from AfPartnerProduct ap where ap.categoryPath=:prtCategoryPath and ap.afPartner.prtNo=:prtNo and ap.afPartnerCategory.prtCatNo=:prtCat";

	public static final String GET_REPORT_FOR_ALL_VALID_WITH_ZERO_JP_MILES="SELECT tr.JP_NUMBER,tr.SOURCE,tr.MEDIUM,tr.CAMPAIGN,tr.MEMBER_OR_GUEST,td.TRANSACTION_ID,td.PARTNER_ITEM_ID AS ORDER_ID,td.TRANSACTION_DATE,td.PARTNER_ID,td.PRICE,td.RATE_PER_MILE,td.COMMISSION_RATE,td.MEMBER_SHARE,td.JPMILES_PURCHASE,td.EXPECTED_DATE_OF_CREDIT,"+
			"td.JPMILES_CANCEL,td.JPMILES_TO_CREDIT,td.PRODUCT_STATUS,td.PARTNER_PRODUCT_ID,tr.TRANSACTION_TYPE,pr1.COMM_AMOUNT,pr1.PRT_NO,pr1.CATEGORY,td.EMAIL_STATUS,tr.SMS_STATUS,td.BONUS_JP_MILES "+
			"FROM PARTNER_REPORT_IN pr1 JOIN AF_TRANSACTION_DETAILS td ON td.PARTNER_ITEM_ID = pr1.ORDER_ID AND td.PRT_NO = pr1.PRT_NO and pr1.ID = td.TRACKING_ID LEFT JOIN "+
			"AF_TRANSACTION tr ON tr.TXN_NO = td.TXN_NO LEFT JOIN AF_PARTNER_PRODUCT prod ON td.PARTNER_PRODUCT_ID = prod.PRODUCT_ID AND td.PRT_NO = "+
			"prod.PRT_NO LEFT JOIN AF_PARTNER_CATEGORY AS cat ON prod.PRT_CAT_NO = cat.PRT_CAT_NO AND td.PRT_NO = cat.PRT_NO WHERE (tr.JP_NUMBER !='NA' or tr.JP_NUMBER !='') and";
	
	public static final String GET_REPORT_FOR_ALL_INVALID_WITH_ZERO_JP_MILES="SELECT tr.JP_NUMBER,tr.SOURCE,tr.MEDIUM,tr.CAMPAIGN,tr.MEMBER_OR_GUEST,pr1.TRANSACTION_ID,pr1.ORDER_ID,pr1.FORMATTED_ORDER_DATE,pr1.PARTNER_ID,pr1.NET_PRICE,'' as RATE_PER_MILE,'' as COMMISSION_RATE,'' as MEMBER_SHARE,'' as JPMILES_PURCHASE,'' as EXPECTED_DATE_OF_CREDIT,"+
			"'' as JPMILES_CANCEL,'' as JPMILES_TO_CREDIT,'' as PRODUCT_STATUS,'' as PARTNER_PRODUCT_ID,tr.TRANSACTION_TYPE,pr1.COMM_AMOUNT,pr1.PRT_NO,pr1.CATEGORY,tr.SMS_STATUS "+
			"FROM PARTNER_REPORT_IN pr1 LEFT JOIN AF_TRANSACTION tr ON tr.TRANSACTION_ID = pr1.TRANSACTION_ID "+
			"WHERE NOT EXISTS (SELECT 1 FROM AF_TRANSACTION_DETAILS td WHERE td.PARTNER_ITEM_ID = pr1.ORDER_ID AND td.PRT_NO = pr1.PRT_NO) AND ";


	public static final String GET_REPORT_FOR_TENTATIVE_WITH_ZERO_JP_MILES = "SELECT tr.JP_NUMBER,tr.SOURCE,tr.MEDIUM,tr.CAMPAIGN,tr.MEMBER_OR_GUEST,pr1.TRANSACTION_ID,pr1.ORDER_ID,pr1.FORMATTED_ORDER_DATE,pr1.PARTNER_ID,td.PRICE,td.RATE_PER_MILE,td.COMMISSION_RATE,td.MEMBER_SHARE,td.JPMILES_PURCHASE,td.EXPECTED_DATE_OF_CREDIT,td.JPMILES_CANCEL,td.JPMILES_TO_CREDIT,"
			+ "td.PRODUCT_STATUS,td.PARTNER_PRODUCT_ID,tr.TRANSACTION_TYPE,pr1.COMM_AMOUNT,pr1.PRT_NO,pr1.CATEGORY,td.EMAIL_STATUS,tr.SMS_STATUS,td.BONUS_JP_MILES FROM PARTNER_REPORT_IN pr1 JOIN AF_TRANSACTION_DETAILS td ON td.PARTNER_ITEM_ID = pr1.ORDER_ID AND td.PRT_NO = pr1.PRT_NO AND td.PRODUCT_STATUS = 1 LEFT JOIN AF_TRANSACTION tr ON tr.TXN_NO = td.TXN_NO "
			+ "LEFT JOIN AF_PARTNER_PRODUCT prod ON td.PARTNER_PRODUCT_ID = prod.PRODUCT_ID AND td.PRT_NO = prod.PRT_NO LEFT JOIN AF_PARTNER_CATEGORY cat ON prod.PRT_CAT_NO = cat.PRT_CAT_NO AND td.PRT_NO = cat.PRT_NO WHERE ";
		
	public static final String GET_REPORT_FOR_PROCESSED_WITH_ZERO_JP_MILES = "SELECT tr.JP_NUMBER,tr.SOURCE,tr.MEDIUM,tr.CAMPAIGN,tr.MEMBER_OR_GUEST,pr1.TRANSACTION_ID,pr1.ORDER_ID,pr1.FORMATTED_ORDER_DATE,pr1.PARTNER_ID,td.PRICE,td.RATE_PER_MILE,td.COMMISSION_RATE,td.MEMBER_SHARE,td.JPMILES_PURCHASE,td.EXPECTED_DATE_OF_CREDIT,td.JPMILES_CANCEL,td.JPMILES_TO_CREDIT,"
			+ "td.PRODUCT_STATUS,td.PARTNER_PRODUCT_ID,tr.TRANSACTION_TYPE,pr1.COMM_AMOUNT,pr1.PRT_NO,pr1.CATEGORY,td.EMAIL_STATUS,tr.SMS_STATUS,td.BONUS_JP_MILES  FROM PARTNER_REPORT_IN pr1 JOIN AF_TRANSACTION_DETAILS td ON td.PARTNER_ITEM_ID = pr1.ORDER_ID AND td.PRT_NO = pr1.PRT_NO AND td.PRODUCT_STATUS = 2 LEFT JOIN AF_TRANSACTION tr ON tr.TXN_NO = td.TXN_NO LEFT JOIN AF_PARTNER_PRODUCT "
			+ "prod ON td.PARTNER_PRODUCT_ID = prod.PRODUCT_ID AND td.PRT_NO = prod.PRT_NO LEFT JOIN AF_PARTNER_CATEGORY AS cat ON prod.PRT_CAT_NO = cat.PRT_CAT_NO AND td.PRT_NO = cat.PRT_NO WHERE ";
		
	public static final String GET_REPORT_FOR_FAILED_WITH_ZERO_JP_MILES = "SELECT tr.JP_NUMBER,tr.SOURCE,tr.MEDIUM,tr.CAMPAIGN,tr.MEMBER_OR_GUEST,pr1.TRANSACTION_ID,pr1.ORDER_ID,pr1.FORMATTED_ORDER_DATE,pr1.PARTNER_ID,td.PRICE,td.RATE_PER_MILE,td.COMMISSION_RATE,td.MEMBER_SHARE,td.JPMILES_PURCHASE,td.EXPECTED_DATE_OF_CREDIT,td.JPMILES_CANCEL,td.JPMILES_TO_CREDIT,"
			+ "td.PRODUCT_STATUS,td.PARTNER_PRODUCT_ID,tr.TRANSACTION_TYPE,pr1.COMM_AMOUNT,pr1.PRT_NO,pr1.CATEGORY,td.EMAIL_STATUS,tr.SMS_STATUS,td.BONUS_JP_MILES  FROM PARTNER_REPORT_IN pr1 JOIN AF_TRANSACTION_DETAILS td ON td.PARTNER_ITEM_ID = pr1.ORDER_ID AND td.PRT_NO = pr1.PRT_NO AND td.PRODUCT_STATUS = 4 LEFT JOIN AF_TRANSACTION tr ON tr.TXN_NO = td.TXN_NO LEFT JOIN AF_PARTNER_PRODUCT prod ON "
			+ "td.PARTNER_PRODUCT_ID = prod.PRODUCT_ID AND td.PRT_NO = prod.PRT_NO LEFT JOIN AF_PARTNER_CATEGORY AS cat ON prod.PRT_CAT_NO = cat.PRT_CAT_NO AND td.PRT_NO = cat.PRT_NO WHERE ";


	public static final String AF_COMMISION_NO_FROM_PRT_NO_AND_PRT_CAT_NO_UP_LOW_VAL="select afpc from AfPartnerCommission afpc where afpc.afPartner.prtNo=:prtNo and afpc.afPartnerCategory.prtCatNo=:prtCat";
	
	public static final String GET_NOTIFICATIONS = "select en from AfNotification en where en.status=1 order by en.notificationId";
	
	public static final String AF_NOTIFICATIONS_BY_NOTIFICATIONS_ID="select noti from AfNotification noti where noti.notificationId=:notificationId and noti.status=1";
	
	public static final String GET_INVALID_REPORT_FOR_ALL_INVALID="SELECT tr.JP_NUMBER,pr1.TRANSACTION_ID,pr1.ORDER_ID,pr1.FORMATTED_ORDER_DATE,pr1.PARTNER_ID,pr1.MESSAGE,pr1.PRODUCT_ID,pr1.PRT_NO,pr1.CATEGORY ,pr1.ID "+"FROM PARTNER_REPORT_IN pr1 LEFT JOIN AF_TRANSACTION tr ON tr.TRANSACTION_ID = pr1.TRANSACTION_ID "+
			                                                      "WHERE NOT EXISTS (SELECT 1 FROM AF_TRANSACTION_DETAILS td WHERE td.PARTNER_ITEM_ID = pr1.ORDER_ID AND td.PRT_NO = pr1.PRT_NO) AND ";
	
	public static final String AF_PARTNER_REPORT_WITH_ORDER_ID_AND_TRANS_ID = "select tx from PartnerReportIn tx where tx.orderId=:orderid and tx.transactionId=:transactionId";
	
	public static final String AF_OFFERS_DEALS_lINK = "select afpo from AfOfferDealLink afpo  order by afpo.id desc";
	
	public static final String AF_OFFERS_DEALS_BY_LMD_ID = "select afpo from AfOfferDealLink afpo where  afpo.lmdId=:lmdId";
	
	public static final String AF_AMAZON_PARTNER_CATEGORY="from AfAmazonPartnerCategory aapc where aapc.status=1 order by aapc.amzCatNo desc ";

	
	public static final String AF_MAX_VALUE_OF_COLUMN = "select max(apo.offerPosition) from AfPartnerOffer apo";
	
	public static final String AF_PARTNER_OFFER_BY_LMD_ID = "select afpo from AfPartnerOffer afpo where afpo.status=1 and afpo.lmdId=:lmdId ";
	
	public static final String AF_GET_PENDING_LINK_OFFER="select afpdl from AfOfferDealLink afpdl where afpdl.lmdId not in(select afpo.lmdId from AfPartnerOffer afpo where afpo.lmdId is not null ) order by afpdl.id desc ";
	
	public static final String AF_GET_APPROVED_LINK_OFFER="select afpdl from AfOfferDealLink afpdl where afpdl.lmdId in(select afpo.lmdId from AfPartnerOffer afpo where afpo.lmdId is not null ) order by afpdl.id desc ";

	public static final String  GET_OFFER_DEAL_REPORT_LIST ="SELECT  aodl.LMD_ID, aodl.STORE,aodl.OFFER_TEXT,aodl.TITLE,aodl.DESCRIPTION,aodl.CATEGORIES,"
	 		           + "  aodl.START_DATE,aodl.END_DATE,aodl.STATUS FROM AF_OFFER_DEAL_LINK aodl ";
	
	public static final String AF_BANNER_BY_UTM_PARAMETER="select banner from AfBanner banner where banner.utmSource=:source and banner.utmMedium=:medium and banner.utmCampaign=:campaign and banner.status=1";
	
	public static final String AF_PARTNERS_ORDER_STATUS_ALL = "select pos from PartnerOrderStatus pos where  pos.status=1 order by pos.id desc";
	
	public static final String AF_PARTNERS_ORDER_STATUS_DUPLICATE="select pos from PartnerOrderStatus pos where pos.prtNo=:prtNo  and  pos.partnerStatus=:partnerStatus and  pos.jpplStatus=:jpplStatus and  pos.status=1";
	
	
	public static final String AF_PARTNERS_ORDER_STATUS = "select pos from PartnerOrderStatus pos where pos.id in "+
				"(select min(po.id) from PartnerOrderStatus po where  po.status=1 group by po.partnerStatus )";
	
	public static final String GET_JPMILESTATUS_REPORT_FOR_TENTATIVE = "SELECT tr.JP_NUMBER,tr.SOURCE,tr.MEDIUM,tr.CAMPAIGN,tr.MEMBER_OR_GUEST,pr1.TRANSACTION_ID,pr1.ORDER_ID,pr1.FORMATTED_ORDER_DATE,pr1.PARTNER_ID,td.PRICE,td.RATE_PER_MILE,td.COMMISSION_RATE,td.MEMBER_SHARE,td.JPMILES_PURCHASE,td.EXPECTED_DATE_OF_CREDIT,td.JPMILES_CANCEL,td.JPMILES_TO_CREDIT,"
			+ "td.PRODUCT_STATUS,td.PARTNER_PRODUCT_ID,tr.TRANSACTION_TYPE,pr1.COMM_AMOUNT,pr1.PRT_NO,pr1.CATEGORY,td.JPMILE_STATUS ,tr.SMS_STATUS FROM PARTNER_REPORT_IN pr1 JOIN AF_TRANSACTION_DETAILS td ON td.PARTNER_ITEM_ID = pr1.ORDER_ID AND td.PRT_NO = pr1.PRT_NO AND td.PRODUCT_STATUS = 1 LEFT JOIN AF_TRANSACTION tr ON tr.TXN_NO = td.TXN_NO "
			+ "LEFT JOIN AF_PARTNER_PRODUCT prod ON td.PARTNER_PRODUCT_ID = prod.PRODUCT_ID AND td.PRT_NO = prod.PRT_NO LEFT JOIN AF_PARTNER_CATEGORY cat ON prod.PRT_CAT_NO = cat.PRT_CAT_NO AND td.PRT_NO = cat.PRT_NO WHERE ";
	
	public static final String GET_JPMILESTATUS_REPORT_FOR_PROCESSED = "SELECT tr.JP_NUMBER,tr.SOURCE,tr.MEDIUM,tr.CAMPAIGN,tr.MEMBER_OR_GUEST,pr1.TRANSACTION_ID,pr1.ORDER_ID,pr1.FORMATTED_ORDER_DATE,pr1.PARTNER_ID,td.PRICE,td.RATE_PER_MILE,td.COMMISSION_RATE,td.MEMBER_SHARE,td.JPMILES_PURCHASE,td.EXPECTED_DATE_OF_CREDIT,td.JPMILES_CANCEL,td.JPMILES_TO_CREDIT,"
			+ "td.PRODUCT_STATUS,td.PARTNER_PRODUCT_ID,tr.TRANSACTION_TYPE,pr1.COMM_AMOUNT,pr1.PRT_NO,pr1.CATEGORY,td.JPMILE_STATUS ,tr.SMS_STATUS FROM PARTNER_REPORT_IN pr1 JOIN AF_TRANSACTION_DETAILS td ON td.PARTNER_ITEM_ID = pr1.ORDER_ID AND td.PRT_NO = pr1.PRT_NO AND td.PRODUCT_STATUS = 2 LEFT JOIN AF_TRANSACTION tr ON tr.TXN_NO = td.TXN_NO LEFT JOIN AF_PARTNER_PRODUCT "
			+ "prod ON td.PARTNER_PRODUCT_ID = prod.PRODUCT_ID AND td.PRT_NO = prod.PRT_NO LEFT JOIN AF_PARTNER_CATEGORY AS cat ON prod.PRT_CAT_NO = cat.PRT_CAT_NO AND td.PRT_NO = cat.PRT_NO WHERE ";
	
	public static final String GET_JPMILESTATUS_REPORT_FOR_FAILED = "SELECT tr.JP_NUMBER,tr.SOURCE,tr.MEDIUM,tr.CAMPAIGN,tr.MEMBER_OR_GUEST,pr1.TRANSACTION_ID,pr1.ORDER_ID,pr1.FORMATTED_ORDER_DATE,pr1.PARTNER_ID,td.PRICE,td.RATE_PER_MILE,td.COMMISSION_RATE,td.MEMBER_SHARE,td.JPMILES_PURCHASE,td.EXPECTED_DATE_OF_CREDIT,td.JPMILES_CANCEL,td.JPMILES_TO_CREDIT,"
			+ "td.PRODUCT_STATUS,td.PARTNER_PRODUCT_ID,tr.TRANSACTION_TYPE,pr1.COMM_AMOUNT,pr1.PRT_NO,pr1.CATEGORY,td.JPMILE_STATUS ,tr.SMS_STATUS FROM PARTNER_REPORT_IN pr1 JOIN AF_TRANSACTION_DETAILS td ON td.PARTNER_ITEM_ID = pr1.ORDER_ID AND td.PRT_NO = pr1.PRT_NO AND td.PRODUCT_STATUS = 4 LEFT JOIN AF_TRANSACTION tr ON tr.TXN_NO = td.TXN_NO LEFT JOIN AF_PARTNER_PRODUCT prod ON "
			+ "td.PARTNER_PRODUCT_ID = prod.PRODUCT_ID AND td.PRT_NO = prod.PRT_NO LEFT JOIN AF_PARTNER_CATEGORY AS cat ON prod.PRT_CAT_NO = cat.PRT_CAT_NO AND td.PRT_NO = cat.PRT_NO WHERE ";
	
	public static final String GET_JPMILESTATUS_REPORT_FOR_ALL_VALID="SELECT tr.JP_NUMBER,tr.SOURCE,tr.MEDIUM,tr.CAMPAIGN,tr.MEMBER_OR_GUEST,td.TRANSACTION_ID,td.PARTNER_ITEM_ID AS ORDER_ID,td.TRANSACTION_DATE,td.PARTNER_ID,td.PRICE,td.RATE_PER_MILE,td.COMMISSION_RATE,td.MEMBER_SHARE,td.JPMILES_PURCHASE,td.EXPECTED_DATE_OF_CREDIT,"+
			"td.JPMILES_CANCEL,td.JPMILES_TO_CREDIT,td.PRODUCT_STATUS,td.PARTNER_PRODUCT_ID,tr.TRANSACTION_TYPE,pr1.COMM_AMOUNT,pr1.PRT_NO,pr1.CATEGORY, td.BONUS_JP_MILES ,td.JPMILE_STATUS,tr.SMS_STATUS  "+
			"FROM PARTNER_REPORT_IN pr1 JOIN AF_TRANSACTION_DETAILS td ON td.PARTNER_ITEM_ID = pr1.ORDER_ID AND td.PRT_NO = pr1.PRT_NO and pr1.ID = td.TRACKING_ID LEFT JOIN "+
			"AF_TRANSACTION tr ON tr.TXN_NO = td.TXN_NO LEFT JOIN AF_PARTNER_PRODUCT prod ON td.PARTNER_PRODUCT_ID = prod.PRODUCT_ID AND td.PRT_NO = "+
			"prod.PRT_NO LEFT JOIN AF_PARTNER_CATEGORY AS cat ON prod.PRT_CAT_NO = cat.PRT_CAT_NO AND td.PRT_NO = cat.PRT_NO WHERE ";
	
	public static final String GET_JPMILESTATUS_REPORT_FOR_ALL_INVALID="SELECT tr.JP_NUMBER,tr.SOURCE,tr.MEDIUM,tr.CAMPAIGN,tr.MEMBER_OR_GUEST,pr1.TRANSACTION_ID,pr1.ORDER_ID,pr1.FORMATTED_ORDER_DATE,pr1.PARTNER_ID,pr1.NET_PRICE,'' as RATE_PER_MILE,'' as COMMISSION_RATE,'' as MEMBER_SHARE,'' as JPMILES_PURCHASE,'' as EXPECTED_DATE_OF_CREDIT,"+
			"'' as JPMILES_CANCEL,'' as JPMILES_TO_CREDIT,'' as PRODUCT_STATUS,'' as PARTNER_PRODUCT_ID,tr.TRANSACTION_TYPE,pr1.COMM_AMOUNT,pr1.PRT_NO,pr1.CATEGORY,td.JPMILE_STATUS "+
			"FROM PARTNER_REPORT_IN pr1 LEFT JOIN AF_TRANSACTION tr ON tr.TRANSACTION_ID = pr1.TRANSACTION_ID "+
			"WHERE NOT EXISTS (SELECT 1 FROM AF_TRANSACTION_DETAILS td WHERE td.PARTNER_ITEM_ID = pr1.ORDER_ID AND td.PRT_NO = pr1.PRT_NO) AND ";
	
	
	public static final String AF_TRANSACTION_DETAILS_STATUS_UPDATE ="select at  from AfTransactionDetails at  where at.transactionId=:transactionId  and  at.partnerItemId=:partnerItemId and at.status=1 ";
	
	public static final String AF_TRANSACTION_SMS_STATUS ="select tr  from AfTransaction tr  where tr.transactionId=:transactionId ";
	
	public static final String GET_TOTAL_COMMISSION_COUNT = "select count(apc) from AfPartnerCommission apc where apc.status=1 and apc.afPartner.status=1";

	public static final String GET_TOTAL_OFFERSDEALS_COUNT = "select count(afpo) from AfPartnerOffer afpo where afpo.status=1 and (afpo.deactivationDate is not null and afpo.deactivationDate>=Now()) ";
	
	public static final String GET_TOTAL_AF_BANNER_COUNT = "select count(ab) from AfBanner ab where ab.status=1 order by ab.bannerSections, ab.bannerPosition";
	
	public static final String  GET_COMMISSION_REPORT_LIST ="SELECT  ap.PARTNER_NAME, pc.PARTNER_CATEGORY_NAME,app.PRODUCT_NAME,apc.COMMISSION_TYPE,apc.TRANSACTION_STATUS,apc.COMMISSION_RATE,"
			+ "apc.MEMBER_SHARE,apc.LOWER_VALUE,apc.UPPER_VALUE,apc.ACTIVATION_DATE,  apc.DEACTIVATION_DATE,"
			+ "apc.COMMISSION_STATUS FROM AF_PARTNER_COMMISSION apc  JOIN AF_PARTNER ap ON apc.PRT_NO = ap.PRT_NO LEFT JOIN AF_PARTNER_CATEGORY pc  ON apc.PRT_CAT_NO = pc.PRT_CAT_NO LEFT JOIN AF_PARTNER_PRODUCT app  ON apc.PRT_PRD_NO = app.PRT_PRD_NO ";
	
	
	public static final String AF_HOST_PARTNERS_COUNT = "select count(ap) from AfPartner ap where ap.status=1 and ap.hostPartner=1 order by ap.partnerPosition";

	public static final String AF_LOGO_PARTNERS_COUNT = "select count(ap) from AfPartner ap where ap.status=1 and ap.logoPartner=1 order by ap.partnerPosition";
	
	public static final String AF_OFFLINE_PARTNERS_COUNT = "select count(ap) from AfPartner ap where ap.status=1 and ap.offLinePartner=1 order by ap.partnerPosition";

	public static final String AF_READ_PARTNERS_COUNT = "select count(ap) from AfPartner ap where ap.status=1 and ap.readPartner=1 order by ap.partnerPosition";
	
	public static final String AF_TALK_PARTNERS_COUNT = "select count(ap) from AfPartner ap where ap.status=1 and ap.talkPartner=1 order by ap.partnerPosition";
	
	public static final String AF_DEFAULT_PARTNERS_COUNT = "select count(ap) from AfPartner ap where ap.status=1 and ap.defaultPartner=1 order by ap.partnerPosition";
	
	public static final String AF_PARTNERS_COUNT = "select count(ap) from AfPartner ap where ap.partnerStatus=1 and ap.status=1";
	
	public static final String AF_PARTNERS_BY_AGGREGATE_ID="select partners from AfPartner partners where partners.aggregateId=:aggregateId and partners.status=1";
	
	public static final String AF_PARTNER_MAPPING="select apm from AfPartnerMapping apm where apm.status=1";
	
	public static final String GET_PARTNER_MAPPING_COUNT_BY_NAME = "select afp from AfPartnerMapping afp  where (afp.partnerMappingName=:condition and afp.afPartner.prtNo=:partNumber) or (afp.partnerMappingName!=:condition and afp.afPartner.prtNo=:partNumber) and afp.status=1";
	
	public static final String AF_PARTNER_MAPPING_NAME="select afp from AfPartnerMapping afp where afp.partnerMappingName=:store";
	
	
	public static final String AF_OFFERS_DEALS_lINK_STORE_NAME = "select distinct(afpo.store) from AfOfferDealLink afpo ";
	
	public static final String GET_BUSINESS_REPORT_FOR_JP_NUMBER_MISSING="SELECT tr.JP_NUMBER,tr.SOURCE,tr.MEDIUM,tr.CAMPAIGN,tr.MEMBER_OR_GUEST,pr1.TRANSACTION_ID,pr1.ORDER_ID,pr1.FORMATTED_ORDER_DATE,pr1.PARTNER_ID,pr1.NET_PRICE,'' as RATE_PER_MILE,'' as COMMISSION_RATE,'' as MEMBER_SHARE,'' as JPMILES_PURCHASE,'' as EXPECTED_DATE_OF_CREDIT,\n" + 
			"	'' as JPMILES_CANCEL,'' as JPMILES_TO_CREDIT,'' as PRODUCT_STATUS,'' as PARTNER_PRODUCT_ID,tr.TRANSACTION_TYPE,pr1.COMM_AMOUNT,pr1.PRT_NO,pr1.CATEGORY,pr1.PARTNER_ORDER_NUMBER \n" + 
			"			FROM PARTNER_REPORT_IN pr1 LEFT JOIN AF_TRANSACTION tr ON tr.TRANSACTION_ID = pr1.TRANSACTION_ID \n" + 
			"			WHERE NOT EXISTS (SELECT 1 FROM AF_TRANSACTION_DETAILS td WHERE td.PARTNER_ITEM_ID = pr1.ORDER_ID AND td.PRT_NO = pr1.PRT_NO) and (pr1.TRANSACTION_ID!='NA' and pr1.TRANSACTION_ID!='') AND  (tr.JP_NUMBER='NA' or tr.JP_NUMBER ='' or tr.JP_NUMBER is null) AND  ";

	public static final String GET_BUSINESS_REPORT_FOR_REFERENCE_NUMBER_MISSING="SELECT tr.JP_NUMBER,tr.SOURCE,tr.MEDIUM,tr.CAMPAIGN,tr.MEMBER_OR_GUEST,pr1.TRANSACTION_ID,pr1.ORDER_ID,pr1.FORMATTED_ORDER_DATE,pr1.PARTNER_ID,pr1.NET_PRICE,'' as RATE_PER_MILE,'' as COMMISSION_RATE,'' as MEMBER_SHARE,'' as JPMILES_PURCHASE,'' as EXPECTED_DATE_OF_CREDIT,\n" + 
			"	'' as JPMILES_CANCEL,'' as JPMILES_TO_CREDIT,'' as PRODUCT_STATUS,'' as PARTNER_PRODUCT_ID,tr.TRANSACTION_TYPE,pr1.COMM_AMOUNT,pr1.PRT_NO,pr1.CATEGORY,pr1.PARTNER_ORDER_NUMBER \n" + 
			"			FROM PARTNER_REPORT_IN pr1 LEFT JOIN AF_TRANSACTION tr ON tr.TRANSACTION_ID = pr1.TRANSACTION_ID \n" + 
			"			WHERE NOT EXISTS (SELECT 1 FROM AF_TRANSACTION_DETAILS td WHERE td.PARTNER_ITEM_ID = pr1.ORDER_ID AND td.PRT_NO = pr1.PRT_NO) and (pr1.TRANSACTION_ID='NA' or pr1.TRANSACTION_ID='' or pr1.TRANSACTION_ID is null)  and (tr.JP_NUMBER!='NA' and tr.JP_NUMBER !='') AND ";

	public static final String GET_BUSINESS_REPORT_FOR_JP_AND_REFERENCE_NUMBER_MISSING="SELECT tr.JP_NUMBER,tr.SOURCE,tr.MEDIUM,tr.CAMPAIGN,tr.MEMBER_OR_GUEST,pr1.TRANSACTION_ID,pr1.ORDER_ID,pr1.FORMATTED_ORDER_DATE,pr1.PARTNER_ID,pr1.NET_PRICE,'' as RATE_PER_MILE,'' as COMMISSION_RATE,'' as MEMBER_SHARE,'' as JPMILES_PURCHASE,'' as EXPECTED_DATE_OF_CREDIT,\n" + 
			"	'' as JPMILES_CANCEL,'' as JPMILES_TO_CREDIT,'' as PRODUCT_STATUS,'' as PARTNER_PRODUCT_ID,tr.TRANSACTION_TYPE,pr1.COMM_AMOUNT,pr1.PRT_NO,pr1.CATEGORY,pr1.PARTNER_ORDER_NUMBER \n" + 
			"			FROM PARTNER_REPORT_IN pr1 LEFT JOIN AF_TRANSACTION tr ON tr.TRANSACTION_ID = pr1.TRANSACTION_ID \n" + 
			"			WHERE NOT EXISTS (SELECT 1 FROM AF_TRANSACTION_DETAILS td WHERE td.PARTNER_ITEM_ID = pr1.ORDER_ID AND td.PRT_NO = pr1.PRT_NO) and (pr1.TRANSACTION_ID='NA' or pr1.TRANSACTION_ID='' or pr1.TRANSACTION_ID is null)  and (tr.JP_NUMBER='NA' or tr.JP_NUMBER='' or tr.JP_NUMBER is null) AND ";

	public static final String GET_BUSINESS_REPORT_FOR_OTHERS="SELECT tr.JP_NUMBER,tr.SOURCE,tr.MEDIUM,tr.CAMPAIGN,tr.MEMBER_OR_GUEST,pr1.TRANSACTION_ID,pr1.ORDER_ID,pr1.FORMATTED_ORDER_DATE,pr1.PARTNER_ID,td.PRICE,td.RATE_PER_MILE,td.COMMISSION_RATE,td.MEMBER_SHARE,td.JPMILES_PURCHASE,td.EXPECTED_DATE_OF_CREDIT,td.JPMILES_CANCEL,td.JPMILES_TO_CREDIT,td.PRODUCT_STATUS,td.PARTNER_PRODUCT_ID,tr.TRANSACTION_TYPE,pr1.COMM_AMOUNT,pr1.PRT_NO,pr1.CATEGORY,pr1.PARTNER_ORDER_NUMBER FROM PARTNER_REPORT_IN pr1 " + 
					" JOIN AF_TRANSACTION_DETAILS td ON td.PARTNER_ITEM_ID = pr1.ORDER_ID AND td.PRT_NO = pr1.PRT_NO AND td.PRODUCT_STATUS!=2 and td.PRODUCT_STATUS!=1 " + 
					" LEFT JOIN AF_TRANSACTION tr ON tr.TXN_NO = td.TXN_NO " + 
					" LEFT JOIN AF_PARTNER_PRODUCT prod ON td.PARTNER_PRODUCT_ID = prod.PRODUCT_ID AND td.PRT_NO = prod.PRT_NO " + 
					" LEFT JOIN AF_PARTNER_CATEGORY AS cat ON prod.PRT_CAT_NO = cat.PRT_CAT_NO AND td.PRT_NO = cat.PRT_NO " + 
					" WHERE (pr1.TRANSACTION_ID != 'NA' and pr1.TRANSACTION_ID !='') and (tr.JP_NUMBER != 'NA' and tr.JP_NUMBER !='')  and " + 
					"((pr1.CATEGORY='NA' or pr1.CATEGORY='' or pr1.CATEGORY is null ) or  (pr1.PRODUCT_ID = '' or pr1.PRODUCT_ID = 'NA' or pr1.PRODUCT_ID is null))  AND ";
	
	public static final String  GET_REPORT_FOR_JP_NUMBER_MISSING_WITH_ZERO_JP_MILES="SELECT tr.JP_NUMBER,tr.SOURCE,tr.MEDIUM,tr.CAMPAIGN,tr.MEMBER_OR_GUEST,pr1.TRANSACTION_ID,pr1.ORDER_ID,pr1.FORMATTED_ORDER_DATE,pr1.PARTNER_ID,td.PRICE,td.RATE_PER_MILE,td.COMMISSION_RATE,td.MEMBER_SHARE,td.JPMILES_PURCHASE,td.EXPECTED_DATE_OF_CREDIT,td.JPMILES_CANCEL,td.JPMILES_TO_CREDIT,\n" + 
			"td.PRODUCT_STATUS,td.PARTNER_PRODUCT_ID,tr.TRANSACTION_TYPE,pr1.COMM_AMOUNT,pr1.PRT_NO,pr1.CATEGORY,td.EMAIL_STATUS,tr.SMS_STATUS,td.BONUS_JP_MILES  FROM PARTNER_REPORT_IN pr1 JOIN AF_TRANSACTION_DETAILS td ON td.PARTNER_ITEM_ID = pr1.ORDER_ID AND td.PRT_NO = pr1.PRT_NO AND td.PRODUCT_STATUS!=2 and td.PRODUCT_STATUS!=1 LEFT JOIN AF_TRANSACTION tr ON tr.TXN_NO = td.TXN_NO LEFT JOIN AF_PARTNER_PRODUCT\n" + 
			"prod ON td.PARTNER_PRODUCT_ID = prod.PRODUCT_ID AND td.PRT_NO = prod.PRT_NO LEFT JOIN AF_PARTNER_CATEGORY AS cat ON prod.PRT_CAT_NO = cat.PRT_CAT_NO AND td.PRT_NO = cat.PRT_NO WHERE  (tr.JP_NUMBER='NA' or tr.JP_NUMBER='') AND pr1.TRANSACTION_ID !=''and pr1.TRANSACTION_ID !='NA' AND";

			public static final String GET_REPORT_FOR_REFERENCE_NUMBER_MISSING_WITH_ZERO_JP_MILES="SELECT tr.JP_NUMBER,tr.SOURCE,tr.MEDIUM,tr.CAMPAIGN,tr.MEMBER_OR_GUEST,pr1.TRANSACTION_ID,pr1.ORDER_ID,pr1.FORMATTED_ORDER_DATE,pr1.PARTNER_ID,td.PRICE,td.RATE_PER_MILE,td.COMMISSION_RATE,td.MEMBER_SHARE,td.JPMILES_PURCHASE,td.EXPECTED_DATE_OF_CREDIT,td.JPMILES_CANCEL,td.JPMILES_TO_CREDIT,\n" + 
					"td.PRODUCT_STATUS,td.PARTNER_PRODUCT_ID,tr.TRANSACTION_TYPE,pr1.COMM_AMOUNT,pr1.PRT_NO,pr1.CATEGORY,td.EMAIL_STATUS,tr.SMS_STATUS,td.BONUS_JP_MILES  FROM PARTNER_REPORT_IN pr1 JOIN AF_TRANSACTION_DETAILS td ON td.PARTNER_ITEM_ID = pr1.ORDER_ID AND td.PRT_NO = pr1.PRT_NO AND td.PRODUCT_STATUS!=2 and td.PRODUCT_STATUS!=1 LEFT JOIN AF_TRANSACTION tr ON tr.TXN_NO = td.TXN_NO LEFT JOIN AF_PARTNER_PRODUCT \n" + 
					"prod ON td.PARTNER_PRODUCT_ID = prod.PRODUCT_ID AND td.PRT_NO = prod.PRT_NO LEFT JOIN AF_PARTNER_CATEGORY AS cat ON prod.PRT_CAT_NO = cat.PRT_CAT_NO AND td.PRT_NO = cat.PRT_NO WHERE  (pr1.TRANSACTION_ID='NA' or pr1.TRANSACTION_ID='') AND (tr.JP_NUMBER !='NA' and tr.JP_NUMBER !='') AND";

			public static final String GET_REPORT_FOR_JP_AND_REFERENCE_NUMBER_MISSING_WITH_ZERO_JP_MILES="SELECT tr.JP_NUMBER,tr.SOURCE,tr.MEDIUM,tr.CAMPAIGN,tr.MEMBER_OR_GUEST,pr1.TRANSACTION_ID,pr1.ORDER_ID,pr1.FORMATTED_ORDER_DATE,pr1.PARTNER_ID,td.PRICE,td.RATE_PER_MILE,td.COMMISSION_RATE,td.MEMBER_SHARE,td.JPMILES_PURCHASE,td.EXPECTED_DATE_OF_CREDIT,td.JPMILES_CANCEL,td.JPMILES_TO_CREDIT,\n" + 
					"td.PRODUCT_STATUS,td.PARTNER_PRODUCT_ID,tr.TRANSACTION_TYPE,pr1.COMM_AMOUNT,pr1.PRT_NO,pr1.CATEGORY,td.EMAIL_STATUS,tr.SMS_STATUS,td.BONUS_JP_MILES  FROM PARTNER_REPORT_IN pr1 JOIN AF_TRANSACTION_DETAILS td ON td.PARTNER_ITEM_ID = pr1.ORDER_ID AND td.PRT_NO = pr1.PRT_NO AND td.PRODUCT_STATUS!=2 and td.PRODUCT_STATUS!=1 LEFT JOIN AF_TRANSACTION tr ON tr.TXN_NO = td.TXN_NO LEFT JOIN AF_PARTNER_PRODUCT \n" + 
					"prod ON td.PARTNER_PRODUCT_ID = prod.PRODUCT_ID AND td.PRT_NO = prod.PRT_NO LEFT JOIN AF_PARTNER_CATEGORY AS cat ON prod.PRT_CAT_NO = cat.PRT_CAT_NO AND td.PRT_NO = cat.PRT_NO WHERE (pr1.TRANSACTION_ID='NA' or pr1.TRANSACTION_ID='') AND (tr.JP_NUMBER='NA' or tr.JP_NUMBER='') AND ";





			public static final String GET_REPORT_REPORT_FOR_OTHERS_WITH_ZERO_JP_MILES="SELECT tr.JP_NUMBER,tr.SOURCE,tr.MEDIUM,tr.CAMPAIGN,tr.MEMBER_OR_GUEST,pr1.TRANSACTION_ID,pr1.ORDER_ID,pr1.FORMATTED_ORDER_DATE,pr1.PARTNER_ID,td.PRICE,td.RATE_PER_MILE,td.COMMISSION_RATE,td.MEMBER_SHARE,td.JPMILES_PURCHASE,td.EXPECTED_DATE_OF_CREDIT,td.JPMILES_CANCEL,td.JPMILES_TO_CREDIT,\n" + 
					"td.PRODUCT_STATUS,td.PARTNER_PRODUCT_ID,tr.TRANSACTION_TYPE,pr1.COMM_AMOUNT,pr1.PRT_NO,pr1.CATEGORY,td.EMAIL_STATUS,tr.SMS_STATUS,td.BONUS_JP_MILES  FROM PARTNER_REPORT_IN pr1 JOIN AF_TRANSACTION_DETAILS td ON td.PARTNER_ITEM_ID = pr1.ORDER_ID AND td.PRT_NO = pr1.PRT_NO AND td.PRODUCT_STATUS!=2 and td.PRODUCT_STATUS!=1 LEFT JOIN AF_TRANSACTION tr ON tr.TXN_NO = td.TXN_NO LEFT JOIN AF_PARTNER_PRODUCT \n" + 
					"prod ON td.PARTNER_PRODUCT_ID = prod.PRODUCT_ID AND td.PRT_NO = prod.PRT_NO LEFT JOIN AF_PARTNER_CATEGORY AS cat ON prod.PRT_CAT_NO = cat.PRT_CAT_NO AND td.PRT_NO = cat.PRT_NO WHERE  (pr1.TRANSACTION_ID != '' and pr1.TRANSACTION_ID != 'NA') and (tr.JP_NUMBER!='NA' and tr.JP_NUMBER!='') and " + 
					"		((pr1.CATEGORY='NA' or pr1.CATEGORY='' or pr1.CATEGORY is null ) or  (pr1.PRODUCT_ID = '' or pr1.PRODUCT_ID = 'NA' or pr1.PRODUCT_ID is null))\n" + 
					"			AND  ";
	

}