package com.af.admin.constants;

public class AfConstants {

	private AfConstants(){}
	
	public static final String STATUS_IN_ACTIVE="In Active";
	public static final String STATUS_ACTIVE="Active";

	public static final String FAILURE="failure";
	public static final String SUCCESS="success";

	public static final String SUCCESS_MESSAGE="message";
	public static final String ERROR_MESSAGE="errormessage";

	public static final String STATUS = "STATUS";

	public static final String STATUS_CLASS_GREEN="green";
	public static final String STATUS_CLASS_RED="red";

	public static final Byte USER_STATUS_IN_ACTIVE=0;
	public static final Byte ROLE_STATUS_IN_ACTIVE=0;

	public static final Byte STATUS_IN_ACTIVE_VALUE=0;
	public static final Byte STATUS_ACTIVE_VALUE=1;

	public static final Byte CHECKBOX_STATUS_CHECKED=1;
	public static final Byte CHECKBOX_STATUS_UNCHECKED=0;

	public static final Integer PERMISSION_TYPE_VIEW=0;
	public static final Integer PERMISSION_TYPE_ADD=1;
	public static final Integer PERMISSION_TYPE_EDIT=2;
	public static final Integer PERMISSION_TYPE_DELETE=3; 
	
	public static final String JP_NUMBER_MISSING="JP Number Missing";
    public static final String REFERENCE_NUMBER_MISSING="Reference Number Missing";
    public static final String JP_AND_REFERENCE_NUMBER_MISSING="JP And Reference Number Missing";
    public static final String OTHERS="Others";


	public static final Byte PERMISSION_ACTIONS_VIEW=0;
	public static final Byte PERMISSION_ACTIONS_ADD=1;
	public static final Byte PERMISSION_ACTIONS_EDIT=2;
	public static final Byte PERMISSION_ACTIONS_DELETE=3;
	public static final Byte PERMISSION_ACTIONS_EXPORT=4;

	public static final String BANNER_SECTIONS_C="Carousel Banners";
	public static final String BANNER_SECTIONS_S="Static Banners";

	public static final String TRANSACTION_STATUS_REQUEST="Requested";
	public static final String TRANSACTION_STATUS_CONFIRM="Confirmed";
	public static final String TRANSACTION_STATUS_CANCEL="Cancelled";

	public static final String CONTENT_TYPE_HOME_0="Home Content";
	public static final String CONTENT_TYPE_EMAIL_1="Email Content ";

	public static final String ITEM_TYPE_PRODUCT = "1";
	public static final String ITEM_TYPE_CATEGORY = "2";
	public static final String ITEM_TYPE_PARTNER = "3";
	public static final String ITEM_TYPE_OFFERS_AND_DEALS = "4";
	public static final String ITEM_TYPE_BRAND = "5";
	public static final String ITEM_TYPE_CUSTOM = "6";
	
	public static final String ACTIVITY_OLD_KEY="oldFields";
	public static final String ACTIVITY_NEW_KEY="newFields";
	
	public static final String DATE_FORMAT="dd/MM/yyyy";
	public static final String FROM_DATE="fromDate";
	public static final String TO_DATE="toDate";
	public static final String TENTATIVE="Tentative";
	public static final String PROCESSED="Processed";
	public static final String DISAPPROVED="Disapproved";
	public static final String FAILED="Failed";
	public static final String CHROME_EXTENSION="Chrome Extension";
	public static final String SHOP_SITE="Shop site";
	public static final String INVALID="invalid";
	public static final String MEMBER_OR_GUEST="MEMBER_OR_GUEST";
	//public static final Integer AMAZON_PRT_NO=23;//production
	public static final Integer AMAZON_PRT_NO=21;//payara
//	public static final Integer AMAZON_PRT_NO=48;//gingerace
	public static final String EMAIL_SMS_ERROR= "EmailOrSmsFAilure";
	
	public static final String ORDER_TRANS_ERROR = "orderAndTransError";

}
