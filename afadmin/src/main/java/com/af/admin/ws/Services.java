package com.af.admin.ws;

import java.util.Date;
import java.util.List;

public interface Services {
		
	public  String autoSuggProductInfoService(int prtNo, List<Integer> catNo, String keyword);	
	public  String ProductSearchInfoService(String catNo, String mstrPrdNo,List<String> brandSeected,String startPrice,String endPrice, String mstrPrdName,String firstResult,String order,String priceListStr,List<Integer> catNos);
	public  String PartnerProductSearchInfoService(String prtCatNo, String rpName);
	public  String brandSearchInfoService(String rpName);

}
