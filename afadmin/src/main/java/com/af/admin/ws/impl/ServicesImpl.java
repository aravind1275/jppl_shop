package com.af.admin.ws.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


import com.af.admin.service.SendGetRequest;
import com.af.admin.utils.Type2Signature;
import com.af.admin.ws.Services;


@Component
public class ServicesImpl implements Services {
	private Logger logger = LogManager.getLogger(ServicesImpl.class.getName());
	private Type2Signature type2Signature = null;
	
	@Autowired
	SendGetRequest sendGetRequest;
	
	@Value("${application.solr.endpoint}")
	private String end_point;
	
	@Value("${application.solr.request.uri}")
	private String request_uri;
	
	@Value("${application.solr.request.uri_pps}")
	private String request_uri_pps;
	
		
	@Override
	public synchronized String autoSuggProductInfoService(int prtNo, List<Integer> catNos, String keyword){
		String url="", response="", key="";
		try{
			key = System.currentTimeMillis()+"";
			type2Signature = new Type2Signature();
			Map<String,String[]> mapPrdInfo=new HashMap<String, String[]>();
			String selectedCatNos="";
			mapPrdInfo.put("fl", new String[]{"MASTER_PRODUCT_ID,PRODUCT_NAME,PRODUCT_PRICE,JP_MILES"});
			mapPrdInfo.put("facet", new String[]{"on"});
			mapPrdInfo.put("facet.field", new String[]{"CAT_NO"});
			mapPrdInfo.put("facet.mincount", new String[]{"1"});
			mapPrdInfo.put("q.op", new String[]{"AND"});
			mapPrdInfo.put("indent", new String[]{"on"});
			mapPrdInfo.put("omitHeader", new String[]{"true"});
			mapPrdInfo.put("wt", new String[]{"json"});
			mapPrdInfo.put("rows", new String[]{"10"});
			mapPrdInfo.put("start", new String[]{"0"});
			mapPrdInfo.put("sort", new String[]{"PRODUCT_PRICE DESC"});
			mapPrdInfo.put("q", new String[]{"PRODUCT_NAME:"+keyword.trim()+"*"});
			
			if(catNos.size()>0){			
				int i=1,len=0;
				len = catNos.size();
				for(Integer catNo_:catNos){
					if(i==len)
						selectedCatNos += catNo_.toString();
					else
						selectedCatNos += catNo_.toString() + " OR ";
					i++;
				}		
				mapPrdInfo.put("fq",new String[]{"CAT_NO:("+selectedCatNos+")"});				
			}			
			else 
				mapPrdInfo.put("fq", new String[]{"CAT_NO:(*)"});
	        url=type2Signature.solrsign(mapPrdInfo,end_point,request_uri);
	     
	        logger.info("/autoSuggProductInfoService -- method.GET "+ key +" request:\n"+url);	       
	        response=sendGetRequest.sendGet(url);
	        logger.info("/autoSuggProductInfoService -- method.GET "+ key +" response:\n"+response.length());	
	        
	        return response;	        
		}catch(Exception e){
			//e.printStackTrace();
			logger.error("/autoSuggProductInfoService Exception - e...:" , e);
			return "1";
		}
	}
	
	
	@Override
	public synchronized String ProductSearchInfoService(String catNo, String mstrPrdNo,List<String> brandSeected,String startPrice,String endPrice,String mstrPrdName,String firstResult,String order,String priceListStr,List<Integer> catNos){
		String url="", response="";
		float startPrice_=0,endPrice_=0,x=0.01f;long count=0L,default_endPrice=19999999L;
		String selectedPriceList="";
		Map<String,String[]> mapPrdInfo=new HashMap<String, String[]>();
		List<String> qList = new ArrayList<String>();
		List<String> fqList = new ArrayList<String>();
		String q[],fq[],selectedBrands="",selectedCatNos="";
		String orderType[]={};
		String sortType="",sortvalue="",key="";		
		try{
			type2Signature = new Type2Signature();
			key = System.currentTimeMillis()+"";
			
			mapPrdInfo.put("fl", new String[]{"*, detail:[subquery]"});
			mapPrdInfo.put("detail.q", new String[]{"{!terms f=MASTER_PRODUCT_ID v=$row.MASTER_PRODUCT_ID}"});
			mapPrdInfo.put("detail.fl", new String[]{"*"});
			mapPrdInfo.put("detail.fq", new String[]{"DOCTYPE:2"});
			mapPrdInfo.put("facet", new String[]{"on"});
			mapPrdInfo.put("facet.field", new String[]{"PRODUCT_BRAND","CAT_NO"});
			mapPrdInfo.put("facet.mincount", new String[]{"1"});
			mapPrdInfo.put("q.op", new String[]{"AND"});
			mapPrdInfo.put("indent", new String[]{"off"});
			mapPrdInfo.put("omitHeader", new String[]{"true"});
			mapPrdInfo.put("wt", new String[]{"json"});
			mapPrdInfo.put("rows", new String[]{"12"});
			mapPrdInfo.put("start", new String[]{"0"});
			logger.info("/ProductSearchInfoService order:"+order);
			if(order.length()>0 && order.contains("_"))
			{
				orderType=order.split("_");
				sortType=orderType[0];
				sortvalue=orderType[1];
			}
			else{
				sortType="price";
				sortvalue="desc";
			}
			if(sortType.equalsIgnoreCase("price"))
				mapPrdInfo.put("sort", new String[]{"PRODUCT_PRICE "+sortvalue});			
			else if(sortType.equalsIgnoreCase("jpmile"))
				mapPrdInfo.put("sort", new String[]{"JP_MILES "+sortvalue});
			
			logger.info("/ProductSearchInfoService firstResult:"+firstResult);
			if(Integer.parseInt(firstResult)>0)
				mapPrdInfo.put("start", new String[]{(Integer.parseInt(firstResult)+1)+""});
			
			logger.info("/ProductSearchInfoService mstrPrdName:"+mstrPrdName);
			
			if(null!=mstrPrdNo && mstrPrdNo.length()>0){
				fqList.add("MASTER_PRODUCT_ID:"+mstrPrdNo+"");
				qList.add("PRODUCT_NAME:*");
			}
			else 
				if(mstrPrdName.length()>0)
					qList.add("PRODUCT_NAME:"+mstrPrdName.replaceAll(" ", "\\\\ "));
				else
					qList.add("PRODUCT_NAME:*");
			
			q = new String[qList.size()];				
			mapPrdInfo.put("q", qList.toArray(q));
			
			logger.info("/ProductSearchInfoService catNo:"+catNo);
			if(catNos.size()>0){			
				int i=1,len=0;
				len = catNos.size();
				for(Integer catNo_:catNos){
					if(i==len)
						selectedCatNos += catNo_.toString();
					else
						selectedCatNos += catNo_.toString() + " OR ";
					i++;
				}		
				fqList.add("CAT_NO:("+selectedCatNos+")");				
			}else
				fqList.add("CAT_NO:(*)");
			
			logger.info("/ProductSearchInfoService brandSeected:"+brandSeected);
			if(brandSeected.size()>0){
				int i=1,len=0;
				len = brandSeected.size();
				for(String brand:brandSeected){					
					if(i==len)
						selectedBrands += brand.replaceAll(" ", "\\\\ ");
					else
						selectedBrands += brand.replaceAll(" ", "\\\\ ") + " OR ";
					i++;
				}									
				fqList.add("PRODUCT_BRAND:("+selectedBrands+")");
			}
			
			logger.info("/ProductSearchInfoService priceListStr:"+priceListStr);
			if(!priceListStr.isEmpty()){
			 String[] price_list_tokens= priceListStr.split("#",-1);
			logger.info("/ProductSearchInfoService  price_list_tokens:"+price_list_tokens);
			 String[] price_tokens = null;
			 logger.info("/ProductSearchInfoService price_list_tokens.length:"+price_list_tokens.length);
			 if(price_list_tokens.length==1){
				 logger.info("/ProductSearchInfoService price_list_tokens.length==1 price_list_tokens[0]:"+price_list_tokens[0]);
				   price_tokens= price_list_tokens[0].split("-");
				   startPrice_=Float.parseFloat(price_tokens[0]);
				   endPrice_=Float.parseFloat(price_tokens[1]);
				   if(endPrice_<1)
					   endPrice_=default_endPrice;
				   selectedPriceList += "["+startPrice_+" TO "+endPrice_+"]";
			 }else{
				 logger.info("/ProductSearchInfoService price_list_tokens.length>1 price_list_tokens[0]:"+price_list_tokens[0]);				 
				 int i=1,len=0;
				 len = price_list_tokens.length;
			   for (String token : price_list_tokens) {
				   price_tokens= token.split("-");
				   startPrice_=Float.parseFloat(price_tokens[0]);				   
				   endPrice_=Float.parseFloat(price_tokens[1]);
				   if(endPrice_<1)
					   endPrice_=default_endPrice;
			
				   if(i==len)
					   selectedPriceList += "["+startPrice_+" TO "+endPrice_+"]";
				   else
					   selectedPriceList += "["+startPrice_+" TO "+endPrice_+"] OR ";
				   i++;
			   } 	
			 }
			 logger.info("/ProductSearchInfoService selectedPriceList:"+selectedPriceList);
			 fqList.add("PRODUCT_PRICE:("+selectedPriceList+")");
			 
			}
			fq = new String[fqList.size()];				
			mapPrdInfo.put("fq", fqList.toArray(fq));	
				
	        url=type2Signature.solrsign(mapPrdInfo,end_point,request_uri);
	     
	        logger.info("/ProductSearchInfoService -- method.GET "+ key +" request:\n"+url);	       
	        response=sendGetRequest.sendGet(url);
	        logger.info("/ProductSearchInfoService -- method.GET "+ key +" response:\n"+response);	
	        
	        return response;	        
		}catch(Exception e){
			logger.error("/ProductSearchInfoService Exception - e1...:" , e);
			e.printStackTrace();
			return "1";
		}
	}
	
	@Override
	public synchronized String PartnerProductSearchInfoService(String prtCatNo, String rpName){
		String url="", response="", key="";
		try{
			key = System.currentTimeMillis()+"";
			type2Signature = new Type2Signature();
			Map<String,String[]> mapPrdInfo=new HashMap<String, String[]>();
			String selectedCatNos="";
			mapPrdInfo.put("fl", new String[]{"PRT_PRD_NO,PRT_CAT_NO,PRODUCT_NAME"});				
			mapPrdInfo.put("indent", new String[]{"on"});
			mapPrdInfo.put("omitHeader", new String[]{"true"});
			mapPrdInfo.put("wt", new String[]{"json"});						
			mapPrdInfo.put("q", new String[]{"PRT_CAT_NO:"+prtCatNo.trim()});
			mapPrdInfo.put("fq", new String[]{"PRODUCT_NAME:\""+rpName.trim()+"\""});
						
	        url=type2Signature.solrsign(mapPrdInfo,end_point,request_uri_pps);
	         
	        logger.info("/PartnerProductSearchInfoService -- method.GET "+ key +" request:\n"+url);	       
	        response=sendGetRequest.sendGet(url);
	        logger.info("/PartnerProductSearchInfoService -- method.GET "+ key +" response:\n"+response+" response-length:\n"+response.length());
	         
	        return response;	        
		}catch(Exception e){
			//e.printStackTrace();
			logger.error("/PartnerProductSearchInfoService Exception - e...:" +e);
			return "1";
		}
	}
	
	@Override
	public synchronized String brandSearchInfoService(String rpName){
		String url="", response="", key="";
		try{
			key = System.currentTimeMillis()+"";
			type2Signature = new Type2Signature();
			Map<String,String[]> mapPrdInfo=new HashMap<String, String[]>();
			mapPrdInfo.put("facet.field", new String[]{"PRODUCT_BRAND"});				
			mapPrdInfo.put("facet.prefix", new String[]{rpName});
			mapPrdInfo.put("facet.limit", new String[]{"1000"});
			mapPrdInfo.put("rows", new String[]{"0"});
			mapPrdInfo.put("wt", new String[]{"json"});						
			mapPrdInfo.put("facet", new String[]{"on"});
			mapPrdInfo.put("indent", new String[]{"on"});
			mapPrdInfo.put("q", new String[]{"*:*"});
			
	        url=type2Signature.solrsign(mapPrdInfo,end_point,request_uri_pps);
	         
	        logger.info("/brandSearchInfoService -- method.GET "+ key +" request:\n"+url);	       
	        response=sendGetRequest.sendGet(url);
	        logger.info("/brandSearchInfoService -- method.GET "+ key +" response:\n"+response+" response-length:\n"+response.length());
	         
	        return response;	        
		}catch(Exception e){
			//e.printStackTrace();
			logger.error("/brandSearchInfoService Exception - e...:" + e);
			return "1";
		}
	}
	
}
