package com.af.admin.interceptor;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.af.admin.beans.AdminMenu;
import com.af.admin.beans.AdminUserBean;
import com.af.admin.service.CommonService;

public class AdminAccessInterceptor extends HandlerInterceptorAdapter {
	private Logger logger = LogManager.getLogger(AdminAccessInterceptor.class
			.getName());

	@Autowired
	CommonService commonService;

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		String requestUri = request.getRequestURI();
		// String redirect_url = request.getContextPath()+"/admin/login";
		String redirect_url = request.getContextPath();

		// for not required to check the user is exist or not
		if (requestUri.endsWith("login") || requestUri.endsWith("adminForgotPassword")||requestUri.endsWith("allOffersDeals")|| requestUri.endsWith("serverPagination") || requestUri.endsWith("getAllPrtWebservice")||requestUri.endsWith("getJPNumber"))
			return true;
		try {
			List<AdminMenu> adminMenus = new ArrayList<AdminMenu>();
			Map<String, List<AdminMenu>> submenuMap = new LinkedHashMap<String, List<AdminMenu>>();

			HttpSession session = request.getSession();
			AdminUserBean adminUserBean = (AdminUserBean) session
					.getAttribute("activeUser");

			if (null != adminUserBean) {
				if (null != adminUserBean.getUserId()) {
					session.setAttribute("userId", adminUserBean.getUserId());
					commonService.getMenus(adminMenus, submenuMap, adminUserBean.getUserRole());
					request.setAttribute("menus", adminMenus);
					request.setAttribute("subMenus", submenuMap);
					String menuUrl;
					for (AdminMenu adminMenu : adminMenus) {
						if (adminMenu.getUrl() != null) {
							if (!adminMenu.getUrl().isEmpty()
									&& request.getRequestURI().indexOf(adminMenu.getUrl()) != -1) {
								request.setAttribute("parentMenuId", adminMenu.getParentMenuId());
								request.setAttribute("menuId", adminMenu.getMenuId());
							}
						}

						if (!submenuMap.isEmpty()
								&& submenuMap.containsKey(adminMenu.getMenuId())
								&& !submenuMap.get(adminMenu.getMenuId()).isEmpty()) {
							for (AdminMenu adminMenu1 : submenuMap.get(adminMenu.getMenuId())) {
								if (adminMenu1.getUrl() != null) {
									menuUrl = (adminMenu1.getUrl().split("\\?"))[0];
								} else {
									menuUrl = adminMenu1.getUrl();
								}
								if (request.getRequestURI().indexOf(menuUrl) != -1) {
									request.setAttribute("parentMenuId", adminMenu1.getParentMenuId());
									request.setAttribute("menuId", adminMenu1.getMenuId());
								}
							}
						}
					}
				}
			}// if user is exist
			if (null == adminUserBean) {
				response.sendRedirect(redirect_url);
			}// if user is not exist
		} catch (Exception e) {
			logger.error("@@@@ Exception in AdminAccessInterceptor at preHandle() :", e);
		}
		return true;
	}

}