
package com.af.admin.pojo.search;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "PRODUCT_NAME",
    "PRT_PRD_NO",
    "PRT_CAT_NO",
    "PRODUCT_BRAND"
})
public class Doc {

    @JsonProperty("PRODUCT_NAME")
    private String pRODUCTNAME;
    @JsonProperty("PRT_PRD_NO")
    private String pRTPRDNO;
    @JsonProperty("PRT_CAT_NO")
    private long pRTCATNO;
    @JsonProperty("PRODUCT_BRAND")
    private String pRODUCTBRAND;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("PRODUCT_NAME")
    public String getPRODUCTNAME() {
        return pRODUCTNAME;
    }

    @JsonProperty("PRODUCT_NAME")
    public void setPRODUCTNAME(String pRODUCTNAME) {
        this.pRODUCTNAME = pRODUCTNAME;
    }

    @JsonProperty("PRT_PRD_NO")
    public String getPRTPRDNO() {
        return pRTPRDNO;
    }

    @JsonProperty("PRT_PRD_NO")
    public void setPRTPRDNO(String pRTPRDNO) {
        this.pRTPRDNO = pRTPRDNO;
    }

    @JsonProperty("PRT_CAT_NO")
    public long getPRTCATNO() {
        return pRTCATNO;
    }

    @JsonProperty("PRT_CAT_NO")
    public void setPRTCATNO(long pRTCATNO) {
        this.pRTCATNO = pRTCATNO;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
    @JsonProperty("PRODUCT_BRAND")
    public String getPRODUCTBRAND() {
        return pRODUCTBRAND;
    }

    @JsonProperty("PRODUCT_BRAND")
    public void setPRODUCTBRAND(String pRODUCTBRAND) {
        this.pRODUCTBRAND = pRODUCTBRAND;
    }
}
