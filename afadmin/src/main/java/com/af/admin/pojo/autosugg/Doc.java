
package com.af.admin.pojo.autosugg;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "JP_MILES",
    "PRODUCT_PRICE",
    "PRODUCT_NAME",
    "MASTER_PRODUCT_ID"
})
public class Doc {

    @JsonProperty("JP_MILES")
    private Double jPMILES;
    @JsonProperty("PRODUCT_PRICE")
    private Double pRODUCTPRICE;
    @JsonProperty("PRODUCT_NAME")
    private String pRODUCTNAME;
    @JsonProperty("MASTER_PRODUCT_ID")
    private Long mASTERPRODUCTID;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("JP_MILES")
    public Double getJPMILES() {
        return jPMILES;
    }

    @JsonProperty("JP_MILES")
    public void setJPMILES(Double jPMILES) {
        this.jPMILES = jPMILES;
    }

    @JsonProperty("PRODUCT_PRICE")
    public Double getPRODUCTPRICE() {
        return pRODUCTPRICE;
    }

    @JsonProperty("PRODUCT_PRICE")
    public void setPRODUCTPRICE(Double pRODUCTPRICE) {
        this.pRODUCTPRICE = pRODUCTPRICE;
    }

    @JsonProperty("PRODUCT_NAME")
    public String getPRODUCTNAME() {
        return pRODUCTNAME;
    }

    @JsonProperty("PRODUCT_NAME")
    public void setPRODUCTNAME(String pRODUCTNAME) {
        this.pRODUCTNAME = pRODUCTNAME;
    }

    @JsonProperty("MASTER_PRODUCT_ID")
    public Long getMASTERPRODUCTID() {
        return mASTERPRODUCTID;
    }

    @JsonProperty("MASTER_PRODUCT_ID")
    public void setMASTERPRODUCTID(Long mASTERPRODUCTID) {
        this.mASTERPRODUCTID = mASTERPRODUCTID;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
