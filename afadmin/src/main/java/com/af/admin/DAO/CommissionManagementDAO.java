package com.af.admin.DAO;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.af.admin.entity.AfPartnerCategory;
import com.af.admin.entity.AfPartnerCommission;
import com.af.admin.entity.AfPartnerProduct;




public interface CommissionManagementDAO {


	public List<AfPartnerCommission> getPartnerCommissionList(String partNo);
	public List<AfPartnerCommission> getPartnerCommissionList();

	public AfPartnerCategory getAfPartnerCategories(String prtCatNo);
	public List<AfPartnerCommission> getPartnerCommissions(Map pcmap);

	public AfPartnerProduct getAfPartnerProducts(String prtProductNo);
	public AfPartnerCommission getPartnerCommission(Integer prtCommNo);
	public String updateCommission(AfPartnerCommission afPartnerCommission);

	public String addCommission(AfPartnerCommission afpartnercommission);

	public Long checkExistingDetailsStartDate(Integer partNo,String partCatNo,String activationDate);
	public Long checkExistingDetailsStartDate(Integer partNo,String partCatNo,String partPrDctNo,String activationDate);
	public Long checkExistingDetailsEndDate(Integer partNo,String partCatNo,String partPrDctNo,String commissionrate,String memberShare,String activationDate,String deactivationDate);
	public Long checkExistingDetailsEndDate(Integer partNo,String partCatNo,String commissionrate,String memberShare,String activationDate,String deactivationDate);

	public Long checkExistingDetailsStartDate(String partNo, String partCatNo, String partPrdNo, String activationDate, String brandName, String catPath,Float lowerValue,Float upperValue);
	public Long checkExistingDetailsEditStartDate(String activationDateHidden, String partComNo, String partNo, String partCatNo, String partPrdNo, String activationDate, String brandName, String catPath);

	public Integer checkExistingDetails(Integer partNo,String partCatNo,String partPrDctNo);
	public Integer checkExistingDetails(Integer partNo,String partCatNo);

	public List<Integer> checkExistingCommDetails(Integer partNo,String partCatNo,String partPrdNo,String prtBrand,String catPath,Float lowerValue,Float upperValue);

	public Long checkExistingDetailsEndDate(String partNo,String partCatNo,String partPrdNo,String prtBrand,String activationDate,String deactivationDate,String catPath,Float lowerValue,Float upperValue);
	public Long checkExistingDetailsEditEndDate(String partComNo, String partNo,String partCatNo,String partPrdNo,String prtBrand,String activationDate,String deactivationDate, String deactivationDateHidden, String catPath,Float lowerValue,Float upperValue);

	public List<AfPartnerCommission> getPartnerCommissionList(String fromDate, String toDate, String partNo);
	public List<AfPartnerCommission> getPartnerCommissionListUpload(String fromDate, String toDate, String partNo,Integer prtCatNo,Integer prtPrdNo, String path, Float upVal, Float lowVal);

	public String saveJPMileConfig(String jpMile, String typename);
	
	public List<AfPartnerCommission> getPartnerCommissionWebservice();
	public List<AfPartnerCommission> getPartnerCommissionListByName(String partnerName);
	public Integer getpartnerNo(String partnerName);
	public Integer getAfPartnerCategoryName(String prtCategoryName, Integer prtNo);
	public Integer getAfPartnerProductNo(String prtProductName, Integer prtNo, Integer prtCat);
	public List<AfPartnerCommission> getPartnerComNumWithPrtNoPrtCatNoLowUp(Integer prtNo, Integer prtCat, Float lowVal, Float UpVal);
	public String getAFcategoryPath(String path, Integer prtNo, Integer prtCat);
	public List<AfPartnerCommission> getPartnerCommissionList(String fromDate, String toDate, String partnerNo,int start, int length);
	public Long getPartnersCommissionPaginationCount(String fromDate, String toDate, String partnerNo);
	public List<Object[]> getCommissionExportCsvList();
}
