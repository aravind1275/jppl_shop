package com.af.admin.DAO.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.af.admin.DAO.PartnerCategoryDAO;
import com.af.admin.DAO.PartnerProductDAO;
import com.af.admin.entity.AfPartnerCategory;
import com.af.admin.entity.AfPartnerProduct;

@Repository
public class PartnerCategoryDAOimpl implements PartnerCategoryDAO {
	private Logger logger = LogManager.getLogger(PartnerCategoryDAOimpl.class.getName());
	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public AfPartnerCategory getPartnerCategoryByPrtCatNO(String prtCatNo) {
		Session session = sessionFactory.getCurrentSession();
		AfPartnerCategory afPartnerCategory=null;
		try{			
			afPartnerCategory=(AfPartnerCategory)session.get(AfPartnerCategory.class, new Integer(prtCatNo));
		}catch(Exception e){
			logger.error("@@@@ Exception in PartnerCategoryDAOimpl at getPartnerCategoryByPrtCatNO() :", e);	
		}
		return afPartnerCategory;
	}
}
