package com.af.admin.DAO;

import java.util.List;

import com.af.admin.entity.PartnerOrderStatus;

public interface PartnerOrderStatusDAO {

	public String savePartnerOrderStatus(PartnerOrderStatus partnerOrderStatus);

	public List<PartnerOrderStatus> getPartnersStatus(String type);

	public boolean checkPartnerOrderStatusExist(String prtNo, String partnerStatus, String jpplStatus);

	public PartnerOrderStatus getPartnerOrderStatusid(Integer id);

	public String updatePartnerOrderStatus(PartnerOrderStatus partnerOrderStatus);

}
