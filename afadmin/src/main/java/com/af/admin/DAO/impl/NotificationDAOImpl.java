package com.af.admin.DAO.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.af.admin.DAO.NotificationDAO;
import com.af.admin.constants.AfHibernateQueries;
import com.af.admin.entity.AfBanner;
import com.af.admin.entity.AfNotification;

@Repository
public class NotificationDAOImpl implements NotificationDAO{
 private Logger logger = LogManager.getLogger(NotificationDAOImpl.class.getName());
	
	@Autowired
	SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AfNotification> getNotification() {
		List<AfNotification> notificationList = null;
		Query query = null;
		
		try{
			query = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.GET_NOTIFICATIONS);
			notificationList = query.list();
		}
		catch(Exception e){
			logger.error("@@@@ Exception in NotificationDAOImpl at getNotificationList() ", e);
		}
		
		return notificationList;	
		}

	@Override
	public String createNotification(AfNotification afNotification) {
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(afNotification);
			
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			logger.error("@@@@ Exception in NotificationDAOImpl at createNotification() :", e);	
			return "failure";
		}
		return "success";
	}

	@Override
	public String deleteNotification(Integer notificationNo) {
		String deleteStatus="";
		AfNotification afNotification = (AfNotification) sessionFactory.getCurrentSession().get(AfNotification.class, notificationNo);
		if(null!=afNotification){
			afNotification.setStatus((byte) 0);
			String NotificationName=afNotification.getNotificationName();
			NotificationName=NotificationName+"_#deleted"+"_"+notificationNo;
			afNotification.setNotificationName(NotificationName);
		}
		try{
			sessionFactory.getCurrentSession().merge(afNotification);
			deleteStatus="success";
		}catch(Exception e){
			e.printStackTrace();
			deleteStatus="failure";
			logger.error("@@@@ Exception in NotificationDAOImpl at deleteNotification() :", e);	
			return deleteStatus;
		}	
		return deleteStatus;
	}

	@Override
	public String updateNotification(AfNotification afNotification) {
try {
			
			sessionFactory.getCurrentSession().merge(afNotification);
		} catch (ConstraintViolationException e) {
			logger.error("@@@@ Exception in NotificationDAOImpl at updateNotification() :", e);	
			return "failure";
		}catch (Exception e) {
			logger.error("@@@@ Exception in NotificationDAOImpl at updateNotification() :", e);	
			return "failure";
		}
		return "success";
	}

	@Override
	public AfNotification getNotificationid(Integer notificationNo) {
			Session session = sessionFactory.getCurrentSession();
			AfNotification afNotification=null;
			try{
				afNotification=(AfNotification)session.get(AfNotification.class, notificationNo);
			}catch(Exception e){
				logger.error("@@@@ Exception in NotificationDAOImpl at getNotificationById() :", e);	
			}
			
		return afNotification;
	}

	@Override
	public boolean checkNotificationID(String notificationId) {
		List<AfNotification> notificationList=new ArrayList<AfNotification>();
		boolean flag=false;
		Query query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_NOTIFICATIONS_BY_NOTIFICATIONS_ID);
		query.setString("notificationId", notificationId);
		try{
			notificationList=query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in NotificationDAOImpl at checkNotificationID() :", e);
		}
		if(notificationList.size()>0) 
			flag= true;
		else
			flag =false;
		return flag;
	}

}
