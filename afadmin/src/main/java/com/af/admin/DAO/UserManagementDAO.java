package com.af.admin.DAO;

import java.util.List;

import com.af.admin.entity.AfAdminRolePermissions;
import com.af.admin.entity.AfAdminRoles;
import com.af.admin.entity.AfAdminUser;



public interface UserManagementDAO {
	
	public AfAdminUser getUserDetailsByUserName(String userName);
	public String updateUser(AfAdminUser ipAdminUser);
	public AfAdminUser getAdminUserById(Integer userId);
	public String createUser(AfAdminUser adminUser);
	public boolean checkUserName(String userName);
	public String deleteUser(Integer userId);
	public List<AfAdminUser> getAdminUsers();
	public boolean checkRoleName(String roleName);
	public String deleteRole(Integer roleId);
	
	public String createRole(AfAdminRoles ipAdminRoles);
	
	public String updateRole(AfAdminRoles ipAdminRoles);
	
	public AfAdminRoles getRoleById(String roleId);

	public String[] getRolePermissionsById(String roleId);
	
	public String createAdminRolePermission(AfAdminRolePermissions afAdminRolePermissions);
	public AfAdminRolePermissions getpermissionRoleById(String string);


}
