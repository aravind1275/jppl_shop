package com.af.admin.DAO.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.af.admin.DAO.TransactionDetailsDAO;
import com.af.admin.constants.AfConstants;
import com.af.admin.constants.AfHibernateQueries;
import com.af.admin.entity.AfPartnerProduct;
import com.af.admin.entity.AfTransaction;
import com.af.admin.entity.AfTransactionDetails;
import com.af.admin.entity.PartnerReportIn;
import com.af.admin.utils.ServiceProviderUtil;

@Repository     
public class TransactionDetailsDAOImpl implements TransactionDetailsDAO{
	private Logger logger = LogManager.getLogger(TransactionDetailsDAOImpl.class.getName());
	@Autowired  
	SessionFactory sessionFactory;
	
	
	
	
	
	@Override
    public List<AfTransaction> getTransactionId(String jpNumber,String fromDate,String toDate) {
        logger.debug("inside getTransactionId "+jpNumber);
        StringBuffer queryBuffer = null;
        List<AfTransaction> afTransactionList=new ArrayList<AfTransaction>();
        Query query =null;
        int maxResults=7000;
      boolean isDateYes = false;
		Date startDate = null;
        Date endDate = null;
        SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd");
        
            try{
               queryBuffer = new StringBuffer(AfHibernateQueries.AF_GET_TRANSACTION_ID);
        
		if( (fromDate != null) && !("".equals(fromDate)) )
		{
            System.out.println("inside from date");
		isDateYes = true;
		startDate = ServiceProviderUtil.getInstance().getDateFromString(fromDate, "dd/MM/yyyy");
		endDate = ServiceProviderUtil.getInstance().getDateFromString(toDate, "dd/MM/yyyy");
		fromDate = dateFormater.format(startDate);
		toDate =dateFormater.format(endDate);
		queryBuffer.append(" and date(aft.createdTime) between '" + fromDate + "' and '" + toDate + "' order by aft.createdTime desc");                       
	    }
                
                
		
		query = sessionFactory.getCurrentSession().createQuery(queryBuffer.toString());
		 query.setString("jpNumber", jpNumber);
		
                 afTransactionList =query.list();
        
        
        
        
        
        
        
        
//        
////        StringBuffer queryBuffer = null;
//        try{
//           
//             
//                
//                if( (fromDate != null) && !("".equals(fromDate)) )
//		{
//                System.out.println("inside from date");
//		isDateYes = true;
//		startDate = ServiceProviderUtil.getInstance().getDateFromString(fromDate, "dd/MM/yyyy");
//		endDate = ServiceProviderUtil.getInstance().getDateFromString(toDate, "dd/MM/yyyy");
//		fromDate = dateFormater.format(startDate);
//		toDate =dateFormater.format(endDate);
//                query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_GET_TRANSACTION_ID1);
//                query.setString("jpNumber", jpNumber);	
//                query.setString("startdate", ""+startDate);	
//                query.setString("enddate", ""+endDate);	
//                query.setMaxResults(maxResults);/
//                
//                
////		queryBuffer.append(" and date(aftd.transactionDate) between '" + fromDate + "' and '" + toDate + "' order by aftd.transactionDate desc");                       
//	    }else{
//                 query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_GET_TRANSACTION_ID);
//		query.setString("jpNumber", jpNumber);	
//                query.setMaxResults(maxResults);
//                
//                }
//		
//                
//            
//               
//		System.out.println("query"+query);
//        
//            System.out.println("Reddy Quert "+query);
//            afTransactionList =query.list();
        }catch(Exception e){
            logger.error("@@@@ Exception in TransactionDetailsDAOImpl at getTransactionId() :", e);
            e.printStackTrace();
        }
        return afTransactionList;
    }
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AfTransactionDetails> getTransactionDetails(String transactionId) {
		logger.debug("inside getTransactionDetails() "+transactionId);
		List<AfTransactionDetails> afTransactionDetailsList = new ArrayList<AfTransactionDetails>();
		
		Query query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_TRANSACTION_DETAILS);
		query.setString("transactionId", transactionId);
		
		try{  
			afTransactionDetailsList = query.setResultTransformer(Transformers.aliasToBean(AfTransactionDetails.class)).list();
			logger.debug("afTransactionDetailsList size "+afTransactionDetailsList.size());
		}catch(Exception e){
			logger.error("@@@@ Exception in TransactionDetailsDAOImpl at getTransactionDetails() :", e);
		}
              //  System.out.println("transaction date in dao is..........."+AfTransactionDetails);
		return afTransactionDetailsList;
	}
	
	
	@Override
	public List<AfPartnerProduct> getProductNameById(String productId) {
		
		logger.debug("inside getProductNameById() "+productId);
		List<AfPartnerProduct> ppList=new ArrayList<AfPartnerProduct>();
		Session session = sessionFactory.getCurrentSession();  
		Query query = session.createQuery(AfHibernateQueries.AF_PARTNER_PRODUCT_NAME);
		try{
			query.setParameter("productId", productId);
			ppList = query.list();

		}catch(Exception e){
			logger.error("@@@@ Exception in TransactionDetailsDAOImpl at getProductNameById() :", e);
		}
		return ppList;
	}

	@Override
	public List<AfTransactionDetails> getTransactiondetails(String affOrderId, String affTransId) {
		
		List<AfTransactionDetails> aftransDetails = new ArrayList<AfTransactionDetails>();
		Session session = sessionFactory.getCurrentSession();  

		Query query =session.createQuery(AfHibernateQueries.AF_TRANSACTION_DETAILS_WITH_ORDER_ID_AND_TRANS_ID);
		query.setParameter("affOrderId", affOrderId);
		query.setParameter("affTransId", affTransId);
		try {
			aftransDetails = query.list();
		} catch (Exception e) {
			logger.error("@@@@ Exception in TransactionDetailsDAOImpl at getTransactiondetails() :", e);
		}
		return aftransDetails;
	}

	@Override
	public List<AfTransaction> getTransactionByTransactionId(String tranxId) {
		
		List<AfTransaction> aftransList = new ArrayList<AfTransaction>();
		Query query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_TRANSACTION__WITH_TRANSACTION_ID);
		query.setParameter("tranxId", tranxId);
		try {
			aftransList =query.list();
		} catch (Exception e) {
			logger.error("@@@@ Exception in TransactionDetailsDAOImpl at getTransactionByTransactionId() :", e);
		}
		
		return aftransList;
	}

	@Override
	public String updateTrarnsactionDetails(AfTransactionDetails aftransactionDetails) {
		try{
		sessionFactory.getCurrentSession().merge(aftransactionDetails);
	} catch (ConstraintViolationException e) {
		logger.error("@@@@ Exception in TransactionDetailsDAOImpl at updateTrarnsactionDetails() :", e);	
		return AfConstants.FAILURE;
	}catch (Exception ex) {
		logger.error("@@@@ Exception in TransactionDetailsDAOImpl at updateTrarnsactionDetails() :", ex);	
		return AfConstants.FAILURE;
	}
	return AfConstants.SUCCESS;		
	}

	@Override
	public String updateTransaction(AfTransaction transaction) {
		try{
			sessionFactory.getCurrentSession().merge(transaction);
		} catch (ConstraintViolationException e) {
			logger.error("@@@@ Exception in TransactionDetailsDAOImpl at updateTransaction() :", e);	
			return AfConstants.FAILURE;
		}catch (Exception ex) {
			logger.error("@@@@ Exception in TransactionDetailsDAOImpl at updateTransaction() :", ex);	
			return AfConstants.FAILURE;
		}
		return AfConstants.SUCCESS;		
			
		
	}

	@Override
	public String updateTransaction(PartnerReportIn report) {
		
		try{
			sessionFactory.getCurrentSession().merge(report);
		} catch (ConstraintViolationException e) {
			logger.error("@@@@ Exception in TransactionDetailsDAOImpl at updateTransaction() :", e);	
			return AfConstants.FAILURE;
		}catch (Exception ex) {
			logger.error("@@@@ Exception in TransactionDetailsDAOImpl at updateTransaction() :", ex);	
			return AfConstants.FAILURE;
		}
		return AfConstants.SUCCESS;		
	}

	@Override
	public List<PartnerReportIn> getPartnerReportIn(String orderid, String transactionId) {
		List<PartnerReportIn> aftransDetails = new ArrayList<PartnerReportIn>();
		Session session = sessionFactory.getCurrentSession();  

		Query query =session.createQuery(AfHibernateQueries.AF_PARTNER_REPORT_WITH_ORDER_ID_AND_TRANS_ID);
		query.setParameter("orderid", orderid);
		query.setParameter("transactionId", transactionId);
		try {
			aftransDetails = query.list();
		} catch (Exception e) {
			logger.error("@@@@ Exception in TransactionDetailsDAOImpl at getPartnerReportIn() :", e);
		}
		return aftransDetails;
	}

	@Override
	public List<Object[]> getTransactionReports(String jpNumber, String fromDate, String toDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<AfTransactionDetails> getDateTransactionDetails(String transactionId, String fromDate, String toDate) {
		logger.debug("inside getTransactionDetails() Date "+transactionId);
		System.out.println("inside getTransactionDetails() Date "+transactionId);
		List<AfTransactionDetails> afTransactionDetailsList = new ArrayList<AfTransactionDetails>();
		List<Object[]> results =null;
		Query query=null;
//      String fromDate="";
//      String toDate="";
		boolean isDateYes = false;
		Date startDate = null;
        Date endDate = null;
        SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd");
        StringBuffer queryBuffer = null;
		
        queryBuffer =new StringBuffer(AfHibernateQueries.AF_TRANSACTION_DETAILS);
        
		
		if( (fromDate != null) && !("".equals(fromDate)) )
		{
            System.out.println("inside from date");
		isDateYes = true;
		startDate = ServiceProviderUtil.getInstance().getDateFromString(fromDate, "dd/MM/yyyy");
		endDate = ServiceProviderUtil.getInstance().getDateFromString(toDate, "dd/MM/yyyy");
		fromDate = dateFormater.format(startDate);
		toDate =dateFormater.format(endDate);
		queryBuffer.append(" and date(aftd.transactionDate) between '" + fromDate + "' and '" + toDate + "' order by aftd.transactionDate desc");                       
	    }else{
                
                
                queryBuffer.append(" and aftd.status=1 order by aftd.transactionDate desc");
                }
		
		query = sessionFactory.getCurrentSession().createQuery(queryBuffer.toString());
		 query.setString("transactionId", transactionId);
		 
		System.out.println("query--->"+query);
		 
		try{  
			afTransactionDetailsList = query.setResultTransformer(Transformers.aliasToBean(AfTransactionDetails.class)).list();
			logger.debug("afTransactionDetailsList date size "+afTransactionDetailsList.size());
			System.out.println("afTransactionDetailsList date size "+afTransactionDetailsList.size());
		}catch(Exception e){
			logger.error("@@@@ Exception in TransactionDetailsDateDAOImpl at getTransactionDetails() :", e);
			e.printStackTrace();
		}
		return afTransactionDetailsList;
	}

	
}