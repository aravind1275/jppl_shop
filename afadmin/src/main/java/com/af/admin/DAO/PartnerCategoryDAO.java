package com.af.admin.DAO;

import com.af.admin.entity.AfPartnerCategory;

public interface PartnerCategoryDAO {
	
	public AfPartnerCategory getPartnerCategoryByPrtCatNO(String prtCatNo);
	
}
