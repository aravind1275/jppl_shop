package com.af.admin.DAO;

import java.util.List;
import java.util.Map;

import com.af.admin.entity.AfRecommendedProduct;

public interface RecommendedProductDAO {

	public List<AfRecommendedProduct> getRecommendedProducts();
	public String createRecommendedProduct(AfRecommendedProduct afRecommendedProduct);
	public List<AfRecommendedProduct> getRecommendedProduct(Map rpmap);
	public List<AfRecommendedProduct> checkRecommendedProductPosition(String prtPrdPos);
	public AfRecommendedProduct getRecommendedProductByNo(String rpNo);
	public String updateRecommendedProduct(AfRecommendedProduct afRecommendedProduct);
	public String deleteRecommendedProduct(Integer bnrNo);
}
