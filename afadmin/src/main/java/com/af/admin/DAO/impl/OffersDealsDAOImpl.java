package com.af.admin.DAO.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.af.admin.DAO.OffersDealsDAO;
import com.af.admin.constants.AfHibernateQueries;
import com.af.admin.entity.AfOfferDealLink;
import com.af.admin.entity.AfPartnerOffer;

@Repository
public class OffersDealsDAOImpl implements OffersDealsDAO{
	
	private static Logger logger = LogManager.getLogger(OffersDealsDAOImpl.class.getName());

	@Autowired
	SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AfPartnerOffer> getOffersDealsList() {
		List<AfPartnerOffer> afPartnerOfferList=new ArrayList<>();
		Query query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_OFFERS_DEALS);
		try{
			afPartnerOfferList =query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in OffersDealsDAOImpl at getOffersDealsList() :", e);
		}
		return afPartnerOfferList;
		
	}
	
	@Override
	public List<AfPartnerOffer> getSearchByNameList(String prtNo) {
		List<AfPartnerOffer> poList=new ArrayList<AfPartnerOffer>();
		Query query = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_PARTNER_OFFER_DETAILS);		
		try{
			query.setParameter("prtNo", Integer.parseInt(prtNo));
			poList = query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in OffersDealsDAOImpl at getSearchByNameList() :", e);
		}
		return poList;
	}
	
	@Override
	public List<AfPartnerOffer> getOfferByNameList(String searchByName) {
		List<AfPartnerOffer> poList=new ArrayList<AfPartnerOffer>();
		
		try{
			Query query = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_PARTNER_OFFER_BY_NAME);
			poList = query.list();
		}
		catch(Exception e){
			logger.error("@@@@ Exception in OffersDealsDAOImpl at getOfferByNameList() :", e);
		}
		return poList;
	}
	
	@Override
	public List<AfPartnerOffer> getSearchByNameList(String prtNo, String prtOffrNo) {
		List<AfPartnerOffer> poList=new ArrayList<AfPartnerOffer>();
		Query query = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_PARTNER_OFFER_BY_PRTOFFRNO);		
		try{
			query.setParameter("prtNo", Integer.parseInt(prtNo));
			query.setParameter("prtOffrNo", Integer.parseInt(prtOffrNo));
			poList = query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in OffersDealsDAOImpl at getSearchByNameList() :", e);
		}
		return poList;
	}
	
	@Override
	public List<AfPartnerOffer> getSearchByNamePCOList(String prtNo, String prtCatNo) {
		List<AfPartnerOffer> poList=new ArrayList<AfPartnerOffer>();
		Query query = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_PARTNER_OFFER_BY_PRTCATNO);		
		try{
			query.setParameter("prtNo", Integer.parseInt(prtNo));
			query.setParameter("prtCatNo", Integer.parseInt(prtCatNo));
			poList = query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in OffersDealsDAOImpl at getSearchByNamePCOList() :", e);
		}
		return poList;
	}
	
	@Override
	public List<AfPartnerOffer> getOfferTitle(String prtNo, String catNo){
		List<AfPartnerOffer> poList=new ArrayList<AfPartnerOffer>();
		Query query = null;		
		try{
			query = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_PARTNER_OFFER_BY_PRT_NO_AND_CAT_NO);
			query.setParameter("prtNo", Integer.parseInt(prtNo));
			query.setParameter("catNo", Integer.parseInt(catNo));
			poList = query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in OffersDealsDAOImpl at getOfferTitle() :", e);
		}
		return poList;
	}
	
	@Override
	public String defineOffersDeals(AfPartnerOffer afPartnerOffer) {
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(afPartnerOffer);
		} catch (ConstraintViolationException e) {
			logger.error("@@@@ Exception in OffersDealsDAOImpl at defineOffersDeals() :", e);
			return "failure";
		}
		return "success";
	}
	
	@Override
	public AfPartnerOffer getPartnerOfferByNO(String prtOffrNo) {
		
		AfPartnerOffer afPartnerOffer = null;
		try{
			afPartnerOffer=(AfPartnerOffer)sessionFactory.getCurrentSession().get(AfPartnerOffer.class, new Integer(prtOffrNo));
		}catch(Exception e){
			logger.error("@@@@ Exception in OffersDealsDAOImpl at getPartnerOfferByNO() :", e);
		}
		return afPartnerOffer;
	}	
	
	@Override
	public String updateOfferDealsDetails(AfPartnerOffer afPartnerOffer) {
			try {
				sessionFactory.getCurrentSession().merge(afPartnerOffer);
			} catch (ConstraintViolationException e) {
				logger.error("@@@@ Exception in OffersDealsDAOImpl at updateOfferDealsDetails() :", e);
				return "failure";
			}catch (Exception e) {
				logger.error("@@@@ Exception in OffersDealsDAOImpl at updateOfferDealsDetails() :", e);
				return "failure";
			}
			return "success";
	}
	
	@Override
	public String deleteSelectedOffer(Integer prtOffrNo) {
			String deleteStatus="";
			AfPartnerOffer afPartnerOffer = (AfPartnerOffer) sessionFactory.getCurrentSession().get(AfPartnerOffer.class, prtOffrNo);
			if(null!=afPartnerOffer){
				afPartnerOffer.setStatus((byte) 0);			
			}
			try{
				sessionFactory.getCurrentSession().merge(afPartnerOffer);
				deleteStatus="success";
			}catch(Exception e){
				logger.error("@@@@ Exception in OffersDealsDAOImpl at deleteSelectedOffer() :", e);
				return "failure";
			}	
			return deleteStatus;
		}

	@Override
	public List<AfPartnerOffer> checkOffersDealSortOrder(String offersDealsSortOrder) {
		List<AfPartnerOffer> afRecommendedCategory=new ArrayList<AfPartnerOffer>();
		Session session = sessionFactory.getCurrentSession();
		try{
			Query query = session.createQuery(AfHibernateQueries.AF_CHECK_OFFER_SORT_ORDER);
			query.setParameter("offersDealsSortOrder", Byte.parseByte(offersDealsSortOrder));
			afRecommendedCategory=query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in OffersDealsDAOImpl at checkOffersDealSortOrder() :", e);	
		}
		return afRecommendedCategory;
	}
	

	@Override
	public List<AfPartnerOffer> getSearchByPrtNoOfferList(String prtNo) {
		List<AfPartnerOffer> poList=new ArrayList<AfPartnerOffer>();
		
		try{
			Query query = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_PARTNER_OFFER_BY_PRTNO);
			query.setParameter("prtNo", Integer.parseInt(prtNo));
			poList = query.list();
                        System.out.println("-----------query   "+query);
		}
		catch(Exception e){
			logger.error("@@@@ Exception in OffersDealsDAOImpl at getSearchByPrtNoOfferList() :", e);
		}
		
		return poList;
	}
	
	@Override
	public List<AfPartnerOffer> getSearchByNameOfferList(String prtNo, String searchByName, String searchByValue) {
		List<AfPartnerOffer> poList=new ArrayList<AfPartnerOffer>();
		Query query;
		
		try{
			if(!prtNo.equals("") && !searchByValue.equals("")){
				query = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_PARTNER_OFFER_BY_PRTNO_OFFRNAME);
				query.setParameter("prtNo", Integer.parseInt(prtNo));
				query.setParameter("searchByValue", searchByValue);
			}
			else if(!prtNo.equals("")){
				query = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_PARTNER_OFFER_BY_PRTNO_NAME);
				query.setParameter("prtNo", Integer.parseInt(prtNo));
				/*query.setParameter("searchByValue", searchByValue);*/
			}
			else{
				query = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_PARTNER_OFFER_BY_OFFRNAME);
				query.setParameter("searchByValue", searchByValue);
			}
			
			poList = query.list();
		}
		catch(Exception e){
			logger.error("@@@@ Exception in OffersDealsDAOImpl at getSearchByNameOfferList() :", e);
		}
		
		return poList;
	}

	@Override
	public List<AfPartnerOffer> getSearchByDescOfferList(String prtNo, String searchByName, String searchByValue) {
		List<AfPartnerOffer> poList=new ArrayList<AfPartnerOffer>();
		Query query;
		
		try{
			if(!prtNo.equals("") && !searchByValue.equals("")){
				query = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_PARTNER_OFFER_BY_PRTNO_DESC);
				query.setParameter("prtNo", Integer.parseInt(prtNo));
				query.setParameter("searchByValue", searchByValue);
			}
			else{
				query = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_PARTNER_OFFER_BY_DESC);
				query.setParameter("searchByValue", searchByValue);
			}
			
			poList = query.list();
		}
		catch(Exception e){
			logger.error("@@@@ Exception in OffersDealsDAOImpl at getSearchByDescOfferList() :", e);
		}
		
		return poList;
	}
	
	@Override
	public List<AfPartnerOffer> getSearchByCatOfferList(String prtNo, String searchByName, String catNo) {
		List<AfPartnerOffer> poList=new ArrayList<AfPartnerOffer>();
		Query query;
		try{
	
			if(!prtNo.equals("") && !catNo.equals("")){
				query = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_PARTNER_OFFER_BY_PRTNO_CATNAME);
				query.setParameter("prtNo", Integer.parseInt(prtNo));
				query.setParameter("catNo", Integer.parseInt(catNo));
			}
			else{
				query = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_PARTNER_OFFER_BY_CATNAME);
				query.setParameter("catNo", Integer.parseInt(catNo));
			}
			
			poList = query.list();
		}
		catch(Exception e){
			logger.error("@@@@ Exception in OffersDealsDAOImpl at getSearchByCatOfferList() :", e);
		}
		
		return poList;
	}
	
	@Override
	public List<AfPartnerOffer> getOffersDealsListByPrtNo(Integer prtNo) {
		List<AfPartnerOffer> afPartnerOfferList=new ArrayList<AfPartnerOffer>();
		Query query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_OFFERS_DEALS_BY_PRT_NO);
		query.setInteger("prtNo",prtNo);
		try{
			query.setInteger("prtNo",prtNo);
			afPartnerOfferList =query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in OffersDealsDAOImpl at getOffersDealsListByPrtNo() :", e);
		}
		return afPartnerOfferList;
		
	}
	
	@Override
	public Integer getMaxOffandDealSl() {
		Integer maxCount = 0;
		try {
			maxCount = (Integer) sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_OFFERS_DEALS_SL_NO).uniqueResult();
		} catch(Exception e) {
			logger.error("@@@@ Exception in OffersDealsDAOImpl at getMaxOffandDealSl() :", e);
		}
		return maxCount;
	}

	@Override
	public String offersDealsLink(AfOfferDealLink offerLink) {
		String status="";
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(offerLink);
			status="success";
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("@@@@ Exception in OffersDealsDAOImpl at offersDealsLink() :", e);
			return "failure";
		}
		return status;
	}

	@Override
	public List<AfOfferDealLink> getOffersDealList() {
		List<AfOfferDealLink> afOfferDealLink=new ArrayList<>();
		try{
			Query query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_OFFERS_DEALS_lINK);
			/*afOfferDealLink =query.setMaxResults(10).list();*/
			afOfferDealLink =query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in OffersDealsDAOImpl at getOffersDealList() :", e);
		}
		return afOfferDealLink;
	}

	@Override
	public AfOfferDealLink getOfferLmdId(String lmdId) {
		
		AfOfferDealLink afOfferDealLinkl=new AfOfferDealLink();
		Query query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_OFFERS_DEALS_BY_LMD_ID);
		
		try{
			query.setString("lmdId",lmdId);
			afOfferDealLinkl = (AfOfferDealLink)query.list().get(0);
		}catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in OffersDealsDAOImpl at getOfferLmdId() :", e);
		}
		return afOfferDealLinkl;
	}

	@Override
	public AfOfferDealLink getOfferdealLmdId(String lmdId) {
		AfOfferDealLink afOfferDealLink = null;
		try{
			afOfferDealLink=(AfOfferDealLink)sessionFactory.getCurrentSession().get(AfOfferDealLink.class, new Integer(lmdId));
		}catch(Exception e){
			logger.error("@@@@ Exception in OffersDealsDAOImpl at getOfferdealLmdId() :", e);
		}
		return afOfferDealLink;
	}

	@Override
	public String offersDealsLink(AfPartnerOffer afPartnerOffer) {
		String status="";
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(afPartnerOffer);
			status="success";
		} catch (Exception e) {
			e.printStackTrace();
			
			logger.error("@@@@ Exception in OffersDealsDAOImpl at offersDealsLink() :", e);
			return "failure";
		}
		return status;
	}

	@Override
	public AfOfferDealLink getOfferdealId(Integer id) {
		AfOfferDealLink afOfferDealLink = null;
		try{
			afOfferDealLink=(AfOfferDealLink)sessionFactory.getCurrentSession().get(AfOfferDealLink.class, id);
		}catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in OffersDealsDAOImpl at getOfferdealId() :", e);
		}
		return afOfferDealLink;
	}

	@Override
	public Integer getMaxValueOfColumn(String tableName, String columnName) {
		Integer value = 0;
		Query query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_MAX_VALUE_OF_COLUMN);
		
		try{
			//query.setString("afPartnerOffer",tableName);
			///query.setString("offerPosition",columnName);
		 value  = (Integer) query.list().get(0);
		}catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in OffersDealsDAOImpl at getOfferLmdId() :", e);
		}
		return value;
	}
	
	
	
	@Override
	public Long getOfferLmdIdCheckDupl(String lmdId) {
		AfPartnerOffer afPartnerOffer=new AfPartnerOffer();
		long count=0;
		try{
			Query query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_PARTNER_OFFER_BY_LMD_ID);
			query.setString("lmdId",lmdId);
			count = query.list().size();
		}catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in OffersDealsDAOImpl at getOfferLmdId() :", e);
		}
		return count;
	}

	@Override
	public List<Object[]> getExportOffersDealList() {
		Session session = null;
		Query query = null;
		List<Object[]> results =null;
		StringBuffer queryBuffer = null;
		try{
			session = sessionFactory.openSession();
			queryBuffer = new StringBuffer(AfHibernateQueries.GET_OFFER_DEAL_REPORT_LIST);
			query = session.createSQLQuery(queryBuffer.toString());
			results = query.list();
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in OffersDealsDAOImpl at getExportOffersDealList() :", e);
		}
		finally {
			session.close();
		}
		return results;
	}

	/*@Override
	public List<AfPartnerOffer> getPartnerOfferLmdId() {
		List<AfPartnerOffer >afPartnerOffer=new ArrayList<>();
		try{
			Query query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_PARTNER_OFFER_BY_LMD_ID);
			afPartnerOffer=query.list();
		}catch(Exception e){
			
			logger.error("@@@@ Exception in OffersDealsDAOImpl at getOfferLmdId() :", e);
		}
		return afPartnerOffer;
	
	}*/

	@Override
	public List<AfOfferDealLink> getPendingLinkOffers() {
		List<AfOfferDealLink >afOfferDealLink=new ArrayList<>();
		try{
			Query query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_GET_PENDING_LINK_OFFER);
			afOfferDealLink=query.list();
		}catch(Exception e){
			
			logger.error("@@@@ Exception in OffersDealsDAOImpl at getOfferLmdId() :", e);
		}
		return afOfferDealLink;
	}

	@Override
	public List<AfOfferDealLink> approvedOfferDealLink() {
		List<AfOfferDealLink >afOfferDealLink=new ArrayList<>();
		try{
			Query query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_GET_APPROVED_LINK_OFFER);
			afOfferDealLink=query.list();
		}catch(Exception e){
			
			logger.error("@@@@ Exception in OffersDealsDAOImpl at getOfferLmdId() :", e);
		}
		return afOfferDealLink;
	}

	@Override
	public List<AfPartnerOffer> getOffersDealsList(int length, int start) {
		List<AfPartnerOffer> afPartnerOfferList=new ArrayList<>();
		Query query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_OFFERS_DEALS);
		try{
			query.setFirstResult(start);
			afPartnerOfferList =	query.setMaxResults(length).list();
			//afPartnerOfferList =query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in OffersDealsDAOImpl at getOffersDealsList() :", e);
		}
		return afPartnerOfferList;
	}

	@Override
	public Long getPartnersCommissionPaginationCount() {
		Long totalCount=0L;
		Query query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.GET_TOTAL_OFFERSDEALS_COUNT);
		try{
			totalCount = (Long)query.uniqueResult();
		}catch(Exception e){
			logger.error("@@@@ Exception in OffersDealsDAOImpl at getPartnersCommissionPaginationCount() :", e);
		}
		return totalCount;
	}

	@Override
	public List<String> getStoreList() {
		List<String> afOfferDealLink=new ArrayList<>();
		try{
			Query query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_OFFERS_DEALS_lINK_STORE_NAME);
			afOfferDealLink =query.list();
		}catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in OffersDealsDAOImpl at getStoreList() :", e);
		}
		return afOfferDealLink;
	}

}
