package com.af.admin.DAO;

import java.util.List;
import java.util.Map;

import com.af.admin.entity.AfAdminUser;
import com.af.admin.entity.AfBanner;
import com.af.admin.entity.AfRecommendedCategory;
import com.af.admin.entity.AfRecommendedProduct;

public interface RecommendedCategoryDAO {

	public List<AfRecommendedCategory> getRecommendedCategories();
	public List<AfRecommendedCategory> getRecommendedCategories(String prtNo);
	public String createRecommendedCategory(AfRecommendedCategory afRecommendedCategory);
	public List<AfRecommendedCategory> getRecommendedCategory(Map rcmap);
	public List<AfRecommendedCategory> checkRecommendedCategoryPosition(String recommendedCategoryPosition);
	public AfRecommendedCategory getRecommendedCategoryByNo(String rcNo);
	public String updateRecommendedCategory(AfRecommendedCategory afRecommendedCategory);
	public String deleteRecommendedCategory(Integer bnrNo);
	
	public List<AfRecommendedCategory> checkRcCatNo(Integer rcCatNo);
	
}
