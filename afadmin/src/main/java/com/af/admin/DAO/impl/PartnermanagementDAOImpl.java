package com.af.admin.DAO.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.af.admin.DAO.PartnerManagementDAO;
import com.af.admin.constants.AfConstants;
import com.af.admin.constants.AfHibernateQueries;
import com.af.admin.entity.AfAmazonPartnerCategory;
import com.af.admin.entity.AfNotification;
import com.af.admin.entity.AfPartner;
import com.af.admin.entity.AfPartnerCategory;
import com.af.admin.entity.AfPartnerKeyword;
import com.af.admin.entity.AfPartnerMapping;


@Repository
public class PartnermanagementDAOImpl implements PartnerManagementDAO{
	@Autowired
	SessionFactory sessionFactory;
	
	private Logger logger = LogManager.getLogger(PartnermanagementDAOImpl.class.getName());

	
	
	@Override
	public String updatePartner(AfPartner afpartner) {
		String status = AfConstants.FAILURE;
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(afpartner);
			status = AfConstants.SUCCESS;
		}catch (Exception e) {
			status = AfConstants.FAILURE;
			e.printStackTrace();
			logger.error("@@@@ Exception in PartnerManagementDAOImpl at updatePartner :", e);

		}
		return status;
	}


	@Override
	public boolean checkPartnerID(String partnerID) {
		AfPartner afPartner=null;
		boolean flag=false;
		Query query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_PARTNERS_BY_PARTNER_ID);
		query.setString("partnerId", partnerID);
		try{
			afPartner=(AfPartner)query.uniqueResult() ;
			logger.debug(">>>>>>>>>..AfPartner"+afPartner);
		}catch(Exception e){
			logger.error("@@@@ Exception in PartnerManagementDaoImpl at checkPartnerID() :", e);
		}
		if(afPartner!=null) 
			flag= true;
		else
			flag =false;
		return flag;
	}

	@Override
	public AfPartner getPartnerById(Integer prtNo) {
		Session session = sessionFactory.getCurrentSession();
		AfPartner afPartner=null;
		try{
			afPartner=(AfPartner)session.get(AfPartner.class, prtNo);
		}catch(Exception e){
			logger.error("@@@@ Exception in PartnerManagementDaoImpl at getPartnerById() :", e);	
		}
		return afPartner;
	}

	@Override
	public List<AfPartner> getPartners(String partnerType) {

		List<AfPartner> partnerList=new ArrayList<AfPartner>();
		Session session = sessionFactory.getCurrentSession();
		Query query=null;
		if(partnerType.equalsIgnoreCase("host"))
			query= session.createQuery(AfHibernateQueries.AF_HOST_PARTNERS);
		else if(partnerType.equalsIgnoreCase("logo"))
			query= session.createQuery(AfHibernateQueries.AF_LOGO_PARTNERS);
		else if(partnerType.equalsIgnoreCase("default"))
			query= session.createQuery(AfHibernateQueries.AF_DEFAULT_PARTNERS);
		else if(partnerType.equalsIgnoreCase("offline"))
			query= session.createQuery(AfHibernateQueries.AF_OFFLINE_PARTNERS);
		else if(partnerType.equalsIgnoreCase("read"))
			query= session.createQuery(AfHibernateQueries.AF_READ_PARTNERS);
		else if(partnerType.equalsIgnoreCase("talk"))
			query= session.createQuery(AfHibernateQueries.AF_TALK_PARTNERS);	
		else
			query= session.createQuery(AfHibernateQueries.AF_PARTNERS);
		try{
			partnerList =query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in PartnerManagementDaoImpl at getPartners() :", e);	
		}
		return partnerList;
	}
	
	@Override
	public List<AfPartner> getPartnersByLimit(String partnerType, String currPage, String pageLen) {

		List<AfPartner> partnerList=new ArrayList<AfPartner>();
		Session session = sessionFactory.getCurrentSession();
		int curPage = Integer.parseInt(currPage);
		int pageLength = Integer.parseInt(pageLen);
		String queryString = "";
		Query query=null;
		if(partnerType.equalsIgnoreCase("host"))
			queryString = AfHibernateQueries.AF_HOST_PARTNERS;
		else if(partnerType.equalsIgnoreCase("logo"))
				queryString = AfHibernateQueries.AF_LOGO_PARTNERS;
		else if(partnerType.equalsIgnoreCase("default"))
			queryString = AfHibernateQueries.AF_DEFAULT_PARTNERS;
		else if(partnerType.equalsIgnoreCase("offline"))
			queryString = AfHibernateQueries.AF_OFFLINE_PARTNERS;			
		else if(partnerType.equalsIgnoreCase("read"))
			queryString = AfHibernateQueries.AF_READ_PARTNERS;
		else if(partnerType.equalsIgnoreCase("talk"))
			queryString = AfHibernateQueries.AF_TALK_PARTNERS;			
		else
			queryString = AfHibernateQueries.AF_TALK_PARTNERS;

		
			
		try{
			query = session.createQuery(queryString).setFirstResult((curPage-1)*pageLength).setMaxResults(pageLength);
			partnerList =query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in PartnerManagementDaoImpl at getPartners() :", e);	
		}
		return partnerList;
	}

	@Override
	public List<AfPartner> getPartners() {

		List<AfPartner> partnerList=new ArrayList<>();
		Session session = sessionFactory.getCurrentSession();
		Query query=session.createQuery(AfHibernateQueries.AF_PARTNERS);
		try{
			partnerList =query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in PartnerManagementDaoImpl at getPartners() :", e);	
		}
		return partnerList;
	}
	@Override
	public List<AfPartner> getLogoPartners() {

		List<AfPartner> partnerList=new ArrayList<>();
		Session session = sessionFactory.getCurrentSession();
		Query query=session.createQuery(AfHibernateQueries.AF_LOGO_PARTNERS);
		try{
			partnerList =query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in PartnerManagementDaoImpl at getLogoPartners() :", e);	
		}
		return partnerList;
	}

	@Override
	public List<AfPartner> getOfflinePartner() {
		List<AfPartner> partnerList = new ArrayList<>();
		Session session  = sessionFactory.getCurrentSession();
		Query query = session.createQuery(AfHibernateQueries.AF_OFFLINE_PARTNERS);
	    try {
			partnerList = query.list();
		} catch (Exception e) {
        logger.error("@@@@ Exception in PartnerManagementDaoImpl at getOfflinePartner() :", e);
		}
		return partnerList;
	}


	@Override
	public List<AfPartner> getHostPartner() {
		List<AfPartner> partnerList = new ArrayList<>();
		Session session  = sessionFactory.getCurrentSession();
		Query query = session.createQuery(AfHibernateQueries.AF_HOST_PARTNERS);
	    try {
			partnerList = query.list();
		} catch (Exception e) {
        logger.error("@@@@ Exception in PartnerManagementDaoImpl at getHostPartner() :", e);
		}
		return partnerList;
	}
	
	
	@Override
	public List<AfPartner> getTalkPartner() {
		List<AfPartner> partnerList = new ArrayList<>();
		Session session  = sessionFactory.getCurrentSession();
		Query query = session.createQuery(AfHibernateQueries.AF_TALK_PARTNERS);
	    try {
			partnerList = query.list();
		} catch (Exception e) {
        logger.error("@@@@ Exception in PartnerManagementDaoImpl at getTalkPartner() :", e);
		}
		return partnerList;
	}
	
	@Override
	public List<AfPartner> getReadPartner() {
		List<AfPartner> partnerList = new ArrayList<>();
		Session session  = sessionFactory.getCurrentSession();
		Query query = session.createQuery(AfHibernateQueries.AF_READ_PARTNERS);
	    try {
			partnerList = query.list();
		} catch (Exception e) {
        logger.error("@@@@ Exception in PartnerManagementDaoImpl at getOfflinePartner() :", e);
		}
		return partnerList;
	}

	
	@Override
	public String createPartner(AfPartner afpartner) {
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(afpartner);
		} catch (ConstraintViolationException e) {
			logger.error("@@@@ Exception in PartnermanagementDAOImpl at createPartner() :", e);	
			return AfConstants.FAILURE;
		}
		return AfConstants.SUCCESS;
	}

	@Override
	public List<AfPartner> getPartnerByDefault() {
		List<AfPartner> partnerList=new ArrayList<>();
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(AfHibernateQueries.AF_PARTNERS_BY_DEFAULT);
		try{
			partnerList =query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in PartnerManagementDaoImpl at getPartnerByDefault() :", e);	
		}
		return partnerList;
	}

	@Override
	public AfPartner getPartnerByPartnerNO(String partnerNo) {
		Session session = sessionFactory.getCurrentSession();
		AfPartner afpartner=null;
		try{

			afpartner=(AfPartner)session.get(AfPartner.class, new Integer(partnerNo));
		}catch(Exception e){
			logger.error("@@@@ Exception in PartnerManagementDAOImpl at getPartnerByPartnerNO() :", e);	
		}
		return afpartner;
	}

	@Override
	public String deletePartner(Integer prtNo) {
		String deleteStatus;
		AfPartner afPartner = (AfPartner) sessionFactory.getCurrentSession().get(AfPartner.class, prtNo);
		if(null!=afPartner){
			afPartner.setStatus((byte) 0);
			String partnerName=afPartner.getPartnerName();
			partnerName=partnerName+"_#deleted"+"_"+prtNo;
			afPartner.setPartnerName(partnerName);

			String partnerId = afPartner.getPartnerId();
			partnerId = partnerId+"_#deleted"+"_"+prtNo;
			afPartner.setPartnerId(partnerId);
		}
		try{
			sessionFactory.getCurrentSession().merge(afPartner);
			deleteStatus="success";
		}catch(Exception e){
			deleteStatus="failure";
			logger.error("@@@@ Exception in PartnerManagementDAOimpl at deletePartner() :", e);	
		}	
		return deleteStatus;
	}

	@Override

	public AfPartner getPartnerByPartnerName(String partnerName) {

		AfPartner afpartner=null;
		try{	
			Query query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_PARTNERS_BY_PARTNER_NAME);
			query.setString("partnerName", partnerName);
			afpartner=(AfPartner)query.uniqueResult() ;
		}catch(Exception e){
			logger.error("@@@@ Exception in PartnerManagementDaoImpl at getPartnerByPartnerName() :", e);
		}

		return afpartner;
	}


	@Override
	public AfPartnerCategory getPartnerCategoryByPartnerCategoryNo(Integer partnercategoryNo) {


		Session session = sessionFactory.getCurrentSession();
		AfPartnerCategory afpartnercategory=null;
		try{

			afpartnercategory=(AfPartnerCategory)session.get(AfPartnerCategory.class,partnercategoryNo);
		}catch(Exception e){
			logger.error("@@@@ Exception in PartnerManagementDAOImpl getPartnerCategoryByPartnerCategoryNo() :", e);	
		}
		return afpartnercategory;

	}


	@SuppressWarnings("unchecked")
	@Override
	public List<AfPartner> checkLogoPrtDisplayOrder(String prtPos, String prtType) {
		List<AfPartner> afPartners=new ArrayList<>();
		Session session = sessionFactory.getCurrentSession();
		Query query=null;
		try{
			if(prtType.equalsIgnoreCase("offline")){
				query = session.createQuery(AfHibernateQueries.AF_OFFLINE_PARTNER_DISPLAY_ORDER_CHECK);
			}
			else if(prtType.equalsIgnoreCase("read")){
				query = session.createQuery(AfHibernateQueries.AF_READ_PARTNER_DISPLAY_ORDER_CHECK);
			}
			else if(prtType.equalsIgnoreCase("talk")){
				query = session.createQuery(AfHibernateQueries.AF_TALK_PARTNER_DISPLAY_ORDER_CHECK);
			}else{
				query = session.createQuery(AfHibernateQueries.AF_LOGO_PARTNER_DISPLAY_ORDER_CHECK);
			}
			query.setParameter("prtPos", Byte.parseByte(prtPos));
			afPartners=query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in PartnerManagementDAOImpl at checkLogoPrtDisplayOrder() :", e);	
		}
		return afPartners;
	}


	@Override
	public List<AfPartner> checkLogoPrtTilePosition(String prtPos, String prtType) {
		List<AfPartner> afPartners=new ArrayList<>();
		Session session = sessionFactory.getCurrentSession();
		Query query=null;
		try{
			
			query = session.createQuery(AfHibernateQueries.AF_LOGO_PARTNER_TILE_POS_CHECKS);
			
			query.setParameter("prtPos", Byte.parseByte(prtPos));
			afPartners=query.list();
		}catch(Exception e){
			
			logger.error("@@@@ Exception in PartnerManagementDAOImpl at checkLogoPrtTilePosition() :", e);	
		}
		return afPartners;
	}


	@Override
	public List<AfPartnerKeyword> getKeywordFileName(String logoprtNo) {
		List<AfPartnerKeyword> afPartnerKeywords=null;
		Session session = sessionFactory.getCurrentSession();
		try{
			Query query = session.createQuery(AfHibernateQueries.AF_PARTNER_KEYWORDS_FILE_NAME);
			query.setParameter("logoPrtNo",Integer.parseInt(logoprtNo));
			afPartnerKeywords=query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in PartnerManagementDAOImpl at getKeywordFileName() :", e);	
		}
		return afPartnerKeywords;
	}

	
	
	@Override
	public String saveLogoPartnerConfig(String tileNo){
		String status="failure";
		int updatedEntityCount = 0;
		Session session = sessionFactory.getCurrentSession();


		try{
			Query query = session.createQuery(AfHibernateQueries.AF_LOGO_PARTNER_SAVE_CONFIG);
			query.setParameter("tileNo",Byte.parseByte(tileNo));
			updatedEntityCount = query.executeUpdate();
		}
		catch(Exception e){
			updatedEntityCount = -1;
			logger.error("@@@@ Exception in PartnerManagementDAOImpl at saveLogoPartnerConfig() :", e);
		}

		if(updatedEntityCount >= 0){
			status = "success";
		}

		return status;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AfPartner> getDefaultAndHostPartners() {
		List<AfPartner> partnerList = new ArrayList<>();
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(AfHibernateQueries.AF_PARTNERS_BY_DEFAULT_AND_HOST);
		try {
			partnerList =query.list();
		} catch(Exception e) {
			logger.error("@@@@ Exception in PartnerManagementDaoImpl getDefaultAndHostPartners() :", e);	
		}
		return partnerList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AfPartner> getAllPartners() {
		List<AfPartner> partnerList = new ArrayList<>();
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(AfHibernateQueries.GET_ALL_PARTNERS);
		try {
			partnerList = query.list();
		} catch(Exception e) {
			logger.error("@@@@ Exception in PartnerManagementDaoImpl at getAllPartners() :", e);	
		}
		return partnerList;
	}

	

	@SuppressWarnings("unchecked")
	@Override
	public List<AfPartner> getAllPartnersExceptSearch() {
		List<AfPartner> partnerlist = new ArrayList<>();
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(AfHibernateQueries.GET_ALL_PARTNERS_EXCEPT_SEARCH);
        try {
			partnerlist = query.list();
		} catch (Exception e) {
			logger.error("@@@@ Exception in PartnerManagementDaoImpl at getAllPartnersExceptSearch() :", e);	
		}	
	return partnerlist;
	}
	
	
	

	@Override
	public String getLgoPrtTileSearchresult() {
		Byte tileNo=0;
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(AfHibernateQueries.GET_LOGO_PARTNERS_TILE_NUMBER_SEARCH_RESULT);
		try {
			tileNo = (Byte)query.uniqueResult();
			if(tileNo!=null)
				return tileNo.toString();
		} catch(Exception e) {
			logger.error("@@@@ Exception in PartnerManagementDaoImpl at getLgoPrtTileSearchresult() :", e);	
		}
		return tileNo.toString();
	}

	@Override
	public AfPartner getPartnerByName(String partnerName) {
		AfPartner afPartner = null;
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			query = session.createQuery(AfHibernateQueries.GET_PARTNER_BY_PRT_NAME);
			query.setParameter("partnerName", partnerName);
			
			afPartner = (AfPartner) query.uniqueResult();
		} catch (Exception e) {
			logger.error("@@@@ Exception in PartnerManagementDaoImpl at getPartnerByName() :", e);
		}
		return afPartner;
	}
	
	@Override
	public boolean checkActivityID(String activityID) {
		AfPartner afPartner=null;
		boolean flag=false;
		Query query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_PARTNERS_BY_ACTIVITY_ID);
		query.setString("activityId", activityID);
		try{
			afPartner=(AfPartner)query.uniqueResult() ;
			logger.debug(">>>>>>>>>..AfPartner"+afPartner);
		}catch(Exception e){
			logger.error("@@@@ Exception in PartnerManagementDaoImpl at checkActivityID() :", e);
		}
		if(afPartner!=null) 
			flag= true;
		else
			flag =false;
		return flag;
	}
	
	@Override
	public List<AfPartner> getLogoPartners(String prtType) {

		List<AfPartner> partnerList=new ArrayList<>();
		Session session = sessionFactory.getCurrentSession();
		Query query=null;
		if(prtType.equalsIgnoreCase("logo")){
			query=session.createQuery(AfHibernateQueries.AF_LOGO_PARTNERS);
		}else if(prtType.equalsIgnoreCase("offline")){
			query=session.createQuery(AfHibernateQueries.AF_OFFLINE_PARTNERS);
		}else if(prtType.equalsIgnoreCase("read")){
			query=session.createQuery(AfHibernateQueries.AF_READ_PARTNERS);
		}else if(prtType.equalsIgnoreCase("talk")){
			query=session.createQuery(AfHibernateQueries.AF_TALK_PARTNERS);
		}
		try{
			partnerList =query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in PartnerManagementDaoImpl at getLogoPartners() :", e);	
		}
		return partnerList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AfPartner> getAllPrtWebservice(){
		List<AfPartner> partnerList = null;
		Query query=null;
		
		try{
			query=sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.GET_ALL_PARTNERS_FOR_WEBSERVICE);
			partnerList =query.list();
		}
		catch(Exception e){
			logger.error("@@@@ Exception in PartnerManagementDaoImpl at getAllPrtWebservice() :", e);	
		}

		return partnerList;
	}


	@Override
	public AfAmazonPartnerCategory getCategoryByPartner(String partNo) {
		Session session = sessionFactory.getCurrentSession();
		AfAmazonPartnerCategory afpartner=null;
		try{

			afpartner=(AfAmazonPartnerCategory)session.get(AfPartner.class, new Integer(partNo));
		}catch(Exception e){
			logger.error("@@@@ Exception in PartnerManagementDAOImpl at getCategoryByPartner() :", e);	
		}
		return afpartner;
	}


	@Override
	public List<AfPartner> getPartners(int length, int start, String partnerType) {
		List<AfPartner> partnerList=new ArrayList<AfPartner>();
		Session session = sessionFactory.getCurrentSession();
		Query query=null;
		if(partnerType.equalsIgnoreCase("host"))
			query= session.createQuery(AfHibernateQueries.AF_HOST_PARTNERS);
		else if(partnerType.equalsIgnoreCase("logo"))
			query= session.createQuery(AfHibernateQueries.AF_LOGO_PARTNERS);
		else if(partnerType.equalsIgnoreCase("default"))
			query= session.createQuery(AfHibernateQueries.AF_DEFAULT_PARTNERS);
		else if(partnerType.equalsIgnoreCase("offline"))
			query= session.createQuery(AfHibernateQueries.AF_OFFLINE_PARTNERS);
		else if(partnerType.equalsIgnoreCase("read"))
			query= session.createQuery(AfHibernateQueries.AF_READ_PARTNERS);
		else if(partnerType.equalsIgnoreCase("talk"))
			query= session.createQuery(AfHibernateQueries.AF_TALK_PARTNERS);	
		else
			query= session.createQuery(AfHibernateQueries.AF_PARTNERS);
		try{
			//partnerList =query.list();
			query.setFirstResult(start);
			partnerList =	query.setMaxResults(length).list();
		}catch(Exception e){
			logger.error("@@@@ Exception in PartnerManagementDaoImpl at getPartners() :", e);	
		}
		return partnerList;
	}


	@Override
	public Long getPartnerListPaginationCount(String partnerType) {
		List<AfPartner> partnerList=new ArrayList<AfPartner>();
		Session session = sessionFactory.getCurrentSession();
		Query query=null;
		Long totalCount =0L; 
		if(partnerType.equalsIgnoreCase("host"))
			query= session.createQuery(AfHibernateQueries.AF_HOST_PARTNERS_COUNT);
		else if(partnerType.equalsIgnoreCase("logo"))
			query= session.createQuery(AfHibernateQueries.AF_LOGO_PARTNERS_COUNT);
		else if(partnerType.equalsIgnoreCase("default"))
			query= session.createQuery(AfHibernateQueries.AF_DEFAULT_PARTNERS_COUNT);
		else if(partnerType.equalsIgnoreCase("offline"))
			query= session.createQuery(AfHibernateQueries.AF_OFFLINE_PARTNERS_COUNT);
		else if(partnerType.equalsIgnoreCase("read"))
			query= session.createQuery(AfHibernateQueries.AF_READ_PARTNERS_COUNT);
		else if(partnerType.equalsIgnoreCase("talk"))
			query= session.createQuery(AfHibernateQueries.AF_TALK_PARTNERS_COUNT);	
		else
			query= session.createQuery(AfHibernateQueries.AF_PARTNERS_COUNT);
		try{
			/*partnerList =query.list();*/
			totalCount = (Long)query.uniqueResult();
		}catch(Exception e){
			logger.error("@@@@ Exception in PartnerManagementDaoImpl at getPartners() :", e);	
		}
		return totalCount;	
		}


	@Override
	public boolean checkAggregateID(String aggregateid) {
		AfPartner afPartner=null;
		boolean flag=false;
		Query query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_PARTNERS_BY_AGGREGATE_ID);
		query.setString("aggregateId", aggregateid);
		try{
			afPartner=(AfPartner)query.uniqueResult() ;
			logger.debug(">>>>>>>>>..AfPartner"+afPartner);
		}catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in PartnerManagementDaoImpl at checkAggregateID() :", e);
		}
		if(afPartner!=null) 
			flag= true;
		else
			flag =false;
		return flag;
	}


	@Override
	public String createPartnerMapping(AfPartnerMapping afPartnerMapping) {
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(afPartnerMapping);
		} catch (ConstraintViolationException e) {
			logger.error("@@@@ Exception in PartnerManagementDaoImpl at createPartnerMapping() :", e);	
			return AfConstants.FAILURE;
		}
		return AfConstants.SUCCESS;
	}


	@Override
	public List<AfPartnerMapping> getpartnerMappingDetails() {
		List<AfPartnerMapping> afPartnerMappingList=new ArrayList<>();
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(AfHibernateQueries.AF_PARTNER_MAPPING);
		try{
			afPartnerMappingList = query.list();

		}catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in PartnerManagementDaoImpl at getpartnerMappingDetails() :", e);	
		}
		return afPartnerMappingList;
	}


	@Override
	public AfPartnerMapping getPartnerMappingDetailsById(Integer id) {
		Session session = sessionFactory.getCurrentSession();
		AfPartnerMapping afPartnerMapping = null;
		try {
			afPartnerMapping = (AfPartnerMapping) session.get(AfPartnerMapping.class, new Integer(id));
		} catch (Exception e) {
			logger.error("@@@@ Exception in CategoryManagementDAOImpl at getAfPartnerCategories() :", e);
		}
		return afPartnerMapping;
	}


	@Override
	public String updatePartnerMapping(AfPartnerMapping afPartnerMapping) {
		String status = "success";
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(afPartnerMapping);
			
		} catch (Exception e) {
			logger.error("@@@@ Exception in PartnerManagementDaoImpl at updatePartnerMapping() :", e);
			status = "failure";
			return status;
		}
		return status;
	}


	@Override
	public String deletePartnerMapping(Integer id) {
		String deleteStatus="";
		AfPartnerMapping afPartnerMapping = (AfPartnerMapping) sessionFactory.getCurrentSession().get(AfPartnerMapping.class, id);
		if(null!=afPartnerMapping){
			afPartnerMapping.setStatus((byte) 0);
			String partnerMappingName=afPartnerMapping.getPartnerMappingName();
			partnerMappingName=partnerMappingName+"_#deleted"+"_"+id;
			afPartnerMapping.setPartnerMappingName(partnerMappingName);
		}
		try{
			sessionFactory.getCurrentSession().merge(afPartnerMapping);
			deleteStatus="success";
		}catch(Exception e){
			e.printStackTrace();
			deleteStatus="failure";
			logger.error("@@@@ Exception in PartnerManagementDaoImpl at deletePartnerMapping() :", e);	
			return deleteStatus;
		}	
		return deleteStatus;
	}


	@Override
	public boolean MappingPartnerNameExist(String patName,String partNumber) {
		AfPartnerMapping afPartnerMapping=null;
		boolean flag = false;
		Query query  = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.GET_PARTNER_MAPPING_COUNT_BY_NAME);
		query.setString("condition", patName);
		query.setString("partNumber", partNumber);
		query.setMaxResults(1);
		//query.setParameter("prtNo",prtNo);
		try {
			afPartnerMapping = (AfPartnerMapping) query.uniqueResult() ;
		} catch(Exception e) {
			e.printStackTrace();
			logger.error("@@@@ Exception in PartnerManagementDaoImpl at MappingPartnerNameExist() :", e);
		}
		if(afPartnerMapping !=null)
			flag = true;
		else
			flag = false;
		return flag;
	}


	@Override
	public List<AfPartnerMapping> getPartnerMappingName(String store) {
		List<AfPartnerMapping> afPartnerMappingList=new ArrayList<>();
		try{
			Session session = sessionFactory.getCurrentSession();
			Query query = session.createQuery(AfHibernateQueries.AF_PARTNER_MAPPING_NAME);
			query.setParameter("store", store);
			afPartnerMappingList=query.list();
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in PartnerManagementDaoImpl at getPartnerMappingName() :", e);	
		}
		return afPartnerMappingList;
	}


}

