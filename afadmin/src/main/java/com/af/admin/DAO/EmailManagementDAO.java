package com.af.admin.DAO;

import java.util.List;

import com.af.admin.entity.AfEmailManagement;

public interface EmailManagementDAO {

	void addContent(AfEmailManagement afEmailManagement);

	public List<AfEmailManagement> getAllEmail();

	public AfEmailManagement getContentById(int emId);

	public String updateContent(AfEmailManagement afEmailManagement);

	public String emailTypeExist(String emailType);

}
