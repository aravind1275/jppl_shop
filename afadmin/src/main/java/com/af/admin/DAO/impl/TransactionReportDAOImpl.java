package com.af.admin.DAO.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.af.admin.DAO.TransactionReportDAO;
import com.af.admin.constants.AfHibernateQueries;
import com.af.admin.entity.AfPartnerProduct;
import com.af.admin.entity.AfTransaction;

@Repository     
public class TransactionReportDAOImpl implements TransactionReportDAO{

	private Logger logger = LogManager.getLogger(TransactionReportDAOImpl.class.getName());
	
	@Autowired  
	SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AfTransaction> getTransactionDetails(String inputMemberId) {
		
		List<AfTransaction> transactionList=new ArrayList<AfTransaction>();
		Query query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_TRANSACTION_DETAILS);
		query.setString("inputMemberId", inputMemberId);
		
		try{
			transactionList =query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in TransactionReportDAOImpl at getTransactionDetails() :", e);
		}
		return transactionList;
	}
	
	@Override
	public List<AfPartnerProduct> getProductNameById(String productId) {
		List<AfPartnerProduct> ppList=new ArrayList<AfPartnerProduct>();
		Session session = sessionFactory.getCurrentSession();  
		Query query = session.createQuery(AfHibernateQueries.AF_PARTNER_PRODUCT_NAME);		
		try{
			query.setParameter("productId", productId);
			ppList = query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in TransactionReportDAOImpl at getProductNameById() :", e);
		}
		return ppList;
	}
}
