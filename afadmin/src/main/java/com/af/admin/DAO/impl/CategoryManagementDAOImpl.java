package com.af.admin.DAO.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.af.admin.DAO.CategoryManagementDAO;
import com.af.admin.constants.AfConstants;
import com.af.admin.constants.AfHibernateQueries;
import com.af.admin.entity.AfAmazonPartnerCategory;
import com.af.admin.entity.AfCategory;
import com.af.admin.entity.AfPartner;
import com.af.admin.entity.AfPartnerCategory;
import com.af.admin.entity.AfPriceRangeFilter;




@Repository
public class CategoryManagementDAOImpl implements CategoryManagementDAO{
	
	@Autowired
	SessionFactory sessionFactory;



	private Logger logger = LogManager.getLogger(CategoryManagementDAOImpl.class.getName());


	@Override
	public List<AfCategory> getCategoryList() {
		List<AfCategory> categoryList=new ArrayList<AfCategory>();
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(AfHibernateQueries.AF_CATEGORY);
		try{
			categoryList = query.list();

		}catch(Exception e){
			logger.error("@@@@ Exception in CategoryManagementDAOImpl at getCategoryList() :", e);	
		}
		return categoryList;
	}


	@Override
	public String createCategory(AfCategory afcategoryBean) {
		String status;
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(afcategoryBean);
			status = "success";
		} catch (ConstraintViolationException e) {
			status = "failure";
			logger.error("@@@@ Exception in CategoryManagementDAOImpl at createCategory() :", e);	
			return status;
		}
		return status;
	}
	@Override
	public String updateCategory(AfCategory afcategory) {
		try {

			sessionFactory.getCurrentSession().merge(afcategory);
		} catch (ConstraintViolationException e) {
			logger.error("@@@@ Exception in CategoryManagementDAOImpl at updateCategory() :", e);	
			return "failure";
		}catch (Exception e) {
			logger.error("@@@@ Exception in CategoryManagementDAOImpl at updateCategory :", e);	
			return "failure";
		}
		return "success";
	}

	@Override
	public AfCategory getCategoryByCategoryNo(String catNo) {
		AfCategory afcategory = null;
		
		try{
			Session session = sessionFactory.getCurrentSession();
			afcategory = (AfCategory)session.get(AfCategory.class, new Integer(catNo));
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in CategoryManagementDAOImpl at getCategoryByCategoryNo() :", e);	
		}
		
		return afcategory;
	}

	@Override
	public boolean checkcategoryIdExist(String categoryId) {
		AfCategory afcategory=null;
		boolean flag=false;
		Query query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_CATEGORIES_BY_CATEGORY_ID);
		query.setString("categoryId", categoryId);
		try{
			afcategory=(AfCategory)query.uniqueResult() ;
		}catch(Exception e){
			logger.error("@@@@ Exception in CatgeoryMAnagementDaoImpl at checkcategoryIdExist() :", e);
		}
		if(afcategory!=null) 
			flag= true;
		else
			flag =false;
		return flag;
	}


	@Override
	public String deleteCategory(Integer catNo) {

		String deleteStatus="";
		AfCategory afCategory = (AfCategory) sessionFactory.getCurrentSession().get(AfCategory.class,catNo);
		if(null!=afCategory){
			afCategory.setStatus((byte) 0);

			String categoryId=afCategory.getCategoryId();
			categoryId=categoryId+"_#deleted"+"_"+catNo;
			afCategory.setCategoryId(categoryId);

		}
		try{
			sessionFactory.getCurrentSession().merge(afCategory);
			deleteStatus="success";
		}catch(Exception e){
			deleteStatus="failure";
			logger.error("@@@@ Exception in CategoryManagementDAOimpl at deleteCategory() :", e);	
		}	
		return deleteStatus;
	}


	@Override
	public void mapCategories(AfPartnerCategory afpartnercategory) {
		try {

			sessionFactory.getCurrentSession().saveOrUpdate(afpartnercategory);
		} catch (ConstraintViolationException e) {
			logger.error("@@@@ Exception in CategoryManagmentDAOImpl at mapCategories() :", e);	

		}catch (Exception e) {
			logger.error("@@@@ Exception in CategoryManagementDAOImpl at mapCategories :", e);	

		}

	}


	@Override
	public List<AfPartnerCategory> getmappingDetails() {
		List<AfPartnerCategory> categoryList=new ArrayList<AfPartnerCategory>();
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(AfHibernateQueries.AF_CATEGORY_WHERE_PARTNER_CATEGORY_IS_NOT_EMPTY);
		try{
			categoryList = query.list();

		}catch(Exception e){
			logger.error("@@@@ Exception in CategoryManagementDAOImpl at getmappingDetails() :", e);	
		}
		return categoryList;
	}

	
	@Override
	public List<AfPartnerCategory> getmappingDetailsExceptSearch() {
		List<AfPartnerCategory> categoryList=new ArrayList<AfPartnerCategory>();
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(AfHibernateQueries.AF_CATEGORY_WHERE_PARTNER_CATEGORY_IS_NOT_EMPTY);
		try{
			categoryList = query.list();

		}catch(Exception e){
			logger.error("@@@@ Exception in CategoryManagementDAOImpl at getmappingDetails() :", e);	
		}
		return categoryList;
	}


	
	
	

	@Override
	public String deactivateMappedCategory(Integer partnercatNo) {
		String deleteStatus="";
		AfPartnerCategory afCategory = (AfPartnerCategory) sessionFactory.getCurrentSession().get(AfPartnerCategory.class,partnercatNo);
		if(null!=afCategory){
			afCategory.setPartnerCategoryStatus((byte)0);
		}
		try{
			sessionFactory.getCurrentSession().merge(afCategory);
			deleteStatus="success";
		}catch(Exception e){
			deleteStatus="failure";

			logger.error("@@@@ Exception in CategoryManagementDAOimpl at deactivateMappedCategory() :", e);	
			return deleteStatus;
		}	
		return deleteStatus;
	}

	@Override
	public String updatePartnerCategory(AfPartnerCategory afPartnerCategory) {
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(afPartnerCategory);

		} catch (ConstraintViolationException e) {
			logger.error("@@@@ Exception in CategoryManagementDAOImpl at updatePartnerCategory() :", e);	
			return "failure";
		}catch (Exception e) {
			logger.error("@@@@ Exception in CategoryManagementDAOImpl at updatePartnerCategory() :", e);	
			return "failure";
		}
		return "success";
	}
	
	@Override
	public boolean checkForPartnerCatNo(int prt_cat_no) {
		boolean status = false;
		List<AfPriceRangeFilter> result = null;
		try{
			Query query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.GET_PRT_CAT_NO_PRICE_RANGE_FILTER);
			query.setParameter("prt_cat_no", prt_cat_no);
			result = query.list();
			
			if(!result.isEmpty() && null != result){
				status =  true;
			}else{
				status = false;
			}
			
		}catch(Exception e){
			logger.error("@@@@ Exception in CategoryManagementDAOImpl at checkForPartnerCatNo() :", e);
			return false;
		}
		
		return status;
	}
	
	@Override
	public List<AfPriceRangeFilter> getPriceRangeFilterList(int prt_cat_no) {
		List<AfPriceRangeFilter> result = null;
		try{
		
			Query query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.GET_PRT_CAT_NO_PRICE_RANGE_FILTER);
			query.setParameter("prt_cat_no", prt_cat_no);
			result = query.list();

		}catch(Exception e){
			logger.error("@@@@ Exception in CategoryManagementDAOImpl at getPriceRangeFilterList() :", e);
		}
		
		return result;
	}
	
	@Override
	public String deactivatePriceRangeFilter(AfPriceRangeFilter afPriceRangeFilter) {
		String deleteStatus="";
		try{
			sessionFactory.getCurrentSession().merge(afPriceRangeFilter);
			deleteStatus="success";
		}catch(Exception e){
			deleteStatus="failure";
			logger.error("@@@@ Exception in CategoryManagementDAOimpl at deactivatePriceRangeFilter() :", e);	
			return deleteStatus;
		}	
		return deleteStatus;
	}
	
	@Override
	public void createPriceRangeFilter(List<AfPriceRangeFilter> afPriceRangeFilter) {
		try {
			int count=0;
			Session session=sessionFactory.getCurrentSession();
			for(AfPriceRangeFilter afRangeFilter:afPriceRangeFilter){
				session.saveOrUpdate(afRangeFilter);
	            if (count%100 == 0) {
	                session.flush();
	               session.clear();
	            }
	            count++;
	        }
			
		} catch (ConstraintViolationException e) {
			logger.error("@@@@ ConstraintViolationException in CategoryManagementDAOImpl at createPriceRangeFilter() :", e);	
		} catch (Exception e) {
			logger.error("@@@@ Exception in CategoryManagementDAOImpl at createPriceRangeFilter() :", e);	
		}
	}

	@Override
	public AfPartnerCategory getPartnerCategoryByPartnerCategoryNo(String partnerCatNo) {

		AfPartnerCategory afcategory=new AfPartnerCategory();
		Session session = sessionFactory.getCurrentSession();
		afcategory = (AfPartnerCategory)session.get(AfPartnerCategory.class, new Integer(partnerCatNo));
		return afcategory;

	}

	@Override
	public boolean checkCategoryPosition(String categoryDisplayOrder) {
		List<AfPartnerCategory> categoryList=new ArrayList<AfPartnerCategory>();
		boolean checkFlag=false;

		try{
			Query query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_PARTNER_CATEGORY_BY_DISPLAY_ORDER);
			query.setParameter("categoryDisplayOrder", Byte.parseByte(categoryDisplayOrder));
			categoryList = query.list();
		}
		catch(Exception e){
			logger.error("@@@@ Exception in CatgeoryMAnagementDaoImpl at checkCategoryPosition() :", e);
		}

		if(categoryList!=null && categoryList.size() > 0) {
			checkFlag= true;
		}
		else{
			checkFlag =false;
		}

		return checkFlag;
	}

	@Override
	public Long getLastCategoryCatNo(String queryString) {
		Integer count = 0;
		try {
			Query query = sessionFactory.getCurrentSession().createQuery(queryString);
			count = (Integer) query.uniqueResult();
		} catch (Exception e) {
			logger.error("@@@@ Exception in CategoryManagementDAOImpl at getLastCategoryCatNo() :", e);	
		}
		
		return count.longValue();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean checkInPriceRangeFilter(int condition, String queryString) {
		boolean status = false;
		List<AfPriceRangeFilter> result = null;
		try {
			Query query = sessionFactory.getCurrentSession().createQuery(queryString);
			query.setParameter("condition", condition);
			result = query.list();
			if( (!result.isEmpty()) && (null != result) ) {
				status =  true;
			} else {
				status = false;
			}
		} catch(Exception e) {
			logger.error("@@@@ Exception in CategoryManagementDAOImpl at checkInPriceRangeFilter :", e);
			return false;
		}
		return status;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AfPriceRangeFilter> getPriceRangeFilterList(int condition, String queryString) {
		List<AfPriceRangeFilter> result = null;
		try {
			Query query = sessionFactory.getCurrentSession().createQuery(queryString);
			query.setParameter("condition", condition);
			result = query.list();
		} catch(Exception e) {
			logger.error("@@@@ Exception in CategoryManagementDAOImpl at getPriceRangeFilterList :", e);
		}
		return result;
	}

	@Override
	public Long getCategoryRowCount(String queryString) {
		String result = "0";
		try {
			Query query = sessionFactory.getCurrentSession().createQuery(queryString);
			result = (String) query.uniqueResult();
			if(result == null) {
				result = "0";
			}
		} catch(Exception e) {
			logger.error("@@@@ Exception in CategoryManagementDAOImpl at getCategoryRowCount :", e);
		}
		return Long.parseLong(result);
	}
	
	@Override
	public Long getCategoryRowCount(String queryString, String condition) {
		String result = "0";
		try {
			Query query = sessionFactory.getCurrentSession().createQuery(queryString);
			query.setParameter("condition", condition);
			result = (String) query.uniqueResult();
			if(result == null) {
				result = "0";
			}
		} catch(Exception e) {
			logger.error("@@@@ Exception in CategoryManagementDAOImpl at getCategoryRowCount :", e);
		}
		return Long.parseLong(result);
	}
	
	@Override
	public boolean checkcategoryNameExist(String categoryName) {
		Long result = 0L;
		boolean flag = false;
		Query query  = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.GET_CATEGORY_COUNT_BY_CAT_NAME);
		query.setString("condition", categoryName);
		try {
			result = (Long) query.uniqueResult() ;
		} catch(Exception e) {
			logger.error("@@@@ Exception in CatgeoryManagementDaoImpl at checkcategoryNameExist() :", e);
		}
		if(result > 0)
			flag = true;
		else
			flag = false;
		return flag;
	}
	
	
	@Override
	public boolean checkpartnercategoryNameExist(String partcategoryName,Integer prtNo) {
	
		Long result = 0L;
		boolean flag = false;
		Query query  = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.GET_CATEGORY_COUNT_BY_PART_CAT_NAME);
		query.setString("condition", partcategoryName);
		query.setParameter("prtNo",prtNo);
		try {
			result = (Long) query.uniqueResult() ;
		} catch(Exception e) {
			logger.error("@@@@ Exception in CatgeoryManagementDaoImpl at checkcategoryNameExist() :", e);
		}
		if(result > 0)
			flag = true;
		else
			flag = false;
		return flag;
	}
	
	
	@Override
	public boolean checkpartnercategoryIdExist(String partcategoryId) {
		
		Long result = 0L;
		boolean flag = false;
		Query query  = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.GET_CATEGORY_COUNT_BY_PART_CAT_ID);
		query.setString("condition", partcategoryId);
		try {
			result = (Long) query.uniqueResult() ;
		} catch(Exception e) {
			logger.error("@@@@ Exception in CatgeoryManagementDaoImpl at checkpartnercategoryIdExist() :", e);
		}
		if(result > 0)
			flag = true;
		else
			flag = false;
		return flag;
		
	}
	
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AfCategory> getCategoryList(String queryString, String condition) {
		List<AfCategory> afCategoryList = new ArrayList<AfCategory>();
		Query query  = sessionFactory.getCurrentSession().createQuery(queryString);
		query.setString("condition", condition);
		try {
			afCategoryList = query.list();
		} catch(Exception e) {
			logger.error("@@@@ Exception in CatgeoryManagementDaoImpl at getCategoryList() :", e);
		}
		return afCategoryList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AfPartnerCategory> getPartnerCategories(Integer prtNo){
		List<AfPartnerCategory> afPrtCategory = null;
		
		Query query  = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.GET_PARTNER_CATEGORY_BY_NO);
		query.setParameter("prtNo", prtNo);
		
		try {
			afPrtCategory = query.list();
		}
		catch(Exception e) {
			logger.error("@@@@ Exception in CatgeoryManagementDaoImpl at getPartnerCategories() :", e);
		}
		return afPrtCategory;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public boolean checkCategoryNoExistInOtherTable(String queryString, String catNo) {
		List list = new ArrayList();
		boolean flag = false;
		try {
			
			Session session=sessionFactory.getCurrentSession();
			SQLQuery sqlQuery = session.createSQLQuery(queryString);
			sqlQuery.setString(0, catNo);
			sqlQuery.setString(1, catNo);
			sqlQuery.setString(2, catNo);
			sqlQuery.setString(3, catNo);
			list = sqlQuery.list();
		} catch(Exception e) {
			logger.error("@@@@ Exception in CatgeoryMAnagementDaoImpl at checkCategoryNoExistInOtherTable() :", e);
		}
		if( (list.size() > 0) && (Integer.parseInt(String.valueOf(list.get(0))) > 0) )
			flag = true;
		else
			flag = false;
		return flag;
	}
	
	@Override
	public AfCategory getCategoryByCategoryID(String categoryID) {
		AfCategory afCategory = null;
		Query query  = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.GET_AF_CATEGORY_BY_CATEGORY_ID);
		query.setString("categoryID", categoryID);
		try {
			afCategory = (AfCategory)query.uniqueResult();
		} catch(Exception e) {
			logger.error("@@@@ Exception in CatgeoryMAnagementDaoImpl at getCategoryByCategoryID() :", e);
		}
		return afCategory;
	}

	@Override
	public boolean checkCategoryDispOrd(String displayOrder){
		Long count = 0L;
		boolean checkFlag = false;
		
		Session session = sessionFactory.getCurrentSession();
		
		try{
			Query query = session.createQuery(AfHibernateQueries.AF_CATEGORY_DISPLAY_ORDER_CHECK);
			query.setParameter("displayOrder", Byte.parseByte(displayOrder));
			count = (Long)query.uniqueResult();

			if(count > 0){
				checkFlag = true;
			}
		}
		catch(Exception ex){
			logger.error("@@@@ Exception in CatgeoryMAnagementDaoImpl at checkCategoryDispOrd() :", ex);
		}
		
		return checkFlag;
	}

	@Override
	public AfCategory getCategoryByCategoryName(String categoryName) {
		AfCategory afCategory = null;
		Session session = null;
		Query query = null;
		try {
			session = sessionFactory.getCurrentSession();
			query = session.createQuery(AfHibernateQueries.GET_CATEGORY_BY_CAT_NAME);
			query.setParameter("categoryName", categoryName);
			afCategory = (AfCategory) query.uniqueResult();
		} catch (Exception ex) {
			logger.error("@@@@ Exception in CatgeoryMAnagementDaoImpl at getCategoryByCategoryName() :", ex);
		}
		return afCategory;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getBestSellingCategories(){
		List<String> bestSellingCategories = null;
		Session session = null;
		Query query = null;
		
		try{
			session = sessionFactory.getCurrentSession();
			query = session.createQuery(AfHibernateQueries.GET_BEST_SELLING_CATEGORIES);
			bestSellingCategories = (List<String>) query.list();
		}
		catch(Exception e){
			logger.error("@@@@ Exception in CatgeoryMAnagementDaoImpl at getBestSellingCategories() :", e);
		}
		
		return bestSellingCategories;
	}


	@Override
	public String getPrtCatByCatName(String categoryName,Integer prtNo) {
		String afPartnerCategory = "NA";
		Session session = null;
		Query query = null;
		try {
			session = sessionFactory.getCurrentSession();
			query = session.createQuery(AfHibernateQueries.GET_PRT_CATEGORY_BY_CAT_NAME);
			query.setParameter("afPartnerCategory", categoryName);
			query.setParameter("prtNo", prtNo);
			query.setMaxResults(1);
			afPartnerCategory = (String) query.uniqueResult();
		} catch (Exception ex) {
			logger.error("@@@@ Exception in CatgeoryMAnagementDaoImpl at getPrtCatByCatName() :", ex);
			return "NA";
		}
		return afPartnerCategory;
	}


	@Override
	public String createPartnercategories(AfPartnerCategory afPartCat) {
		
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(afPartCat);
		} catch (ConstraintViolationException e) {
			logger.error("@@@@ Exception in PartnermanagementDAOImpl at createPartnercategories() :", e);	
			return AfConstants.FAILURE;
		}
		return AfConstants.SUCCESS;
	}


	@Override
	public AfPartnerCategory deleteCategoryvalues(Integer catNo) {
		AfPartnerCategory afCategory = (AfPartnerCategory) sessionFactory.getCurrentSession().get(AfPartnerCategory.class,catNo);
		return afCategory;
	}


	@Override
	public List<AfAmazonPartnerCategory> getAmazonmappingDetails() {
		List<AfAmazonPartnerCategory> categoryList=new ArrayList<>();
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(AfHibernateQueries.AF_AMAZON_PARTNER_CATEGORY);
		try{
			categoryList = query.list();

		}catch(Exception e){
			logger.error("@@@@ Exception in CategoryManagementDAOImpl at getmappingDetails() :", e);	
		}
		return categoryList;
	}


	@Override
	public AfPartnerCategory getAfPartnerCategories(Integer prtCatNo) {
		Session session = sessionFactory.getCurrentSession();
		AfPartnerCategory afPartnercategory = null;
		try {
			afPartnercategory = (AfPartnerCategory) session.get(AfPartnerCategory.class, new Integer(prtCatNo));
		} catch (Exception e) {
			logger.error("@@@@ Exception in CategoryManagementDAOImpl at getAfPartnerCategories() :", e);
		}
		return afPartnercategory;
	}


	@Override
	public String CreateAmazonPartner(AfAmazonPartnerCategory afAmazonPartnerCategory) {
		
		String status = "success";
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(afAmazonPartnerCategory);
			
		} catch (Exception e) {
			logger.error("@@@@ Exception in CategoryManagementDAOImpl at CreateAmazonPartner() :", e);
			status = "failure";
			return status;
		}
		return status;
	}
	
	@Override
	public AfAmazonPartnerCategory getAmazonmappingDetailsById(Integer prtCatNo) {
		Session session = sessionFactory.getCurrentSession();
		AfAmazonPartnerCategory afPartnercategory = null;
		try {
			afPartnercategory = (AfAmazonPartnerCategory) session.get(AfAmazonPartnerCategory.class, new Integer(prtCatNo));
		} catch (Exception e) {
			logger.error("@@@@ Exception in CategoryManagementDAOImpl at getAfPartnerCategories() :", e);
		}
		return afPartnercategory;
	}



	
}