package com.af.admin.DAO.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.af.admin.DAO.UserManagementDAO;
import com.af.admin.constants.AfHibernateQueries;
import com.af.admin.entity.AfAdminRolePermissions;
import com.af.admin.entity.AfAdminRoles;
import com.af.admin.entity.AfAdminUser;


@Repository
public class UserManagementDAOImpl implements UserManagementDAO{
	@Autowired
	SessionFactory sessionFactory;
	private Logger logger = LogManager.getLogger(UserManagementDAOImpl.class.getName());
	@Override
	public AfAdminUser getUserDetailsByUserName(String userName) {

		Query query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_GET_ACTIVE_USER_BY_EMAIL);
		query.setString("userName", null!=userName?userName.trim():"");
		AfAdminUser afAdminUser=null;
		try{
			afAdminUser=(AfAdminUser)query.uniqueResult();
		if(null!=afAdminUser)
			logger.info("UserManagementDAOImpl getUserDetailsByUserName() afAdminUser:"+afAdminUser.getUserName());
		}catch(Exception e){
			logger.error("@@@@ Exception in UserManagementDAOImpl at getUserDetailsByUserName() :", e);
		}
		return afAdminUser;
	
	}

	@Override
	public String updateUser(AfAdminUser afAdminUser) {
		try {
			
			sessionFactory.getCurrentSession().merge(afAdminUser);
		} catch (ConstraintViolationException e) {
			logger.error("@@@@ Exception in UserManagementDAOImpl updateUser() :"+e);	
			return "failure";
		}catch (Exception e) {
			logger.error("@@@@ Exception in UserManagementDAOImpl at updateUser() :", e);	
			return "failure";
		}
		return "success";
	}

	@Override
	public AfAdminUser getAdminUserById(Integer userId) {
		Session session = sessionFactory.getCurrentSession();
		AfAdminUser adminUser=null;
		try{
			adminUser=(AfAdminUser)session.get(AfAdminUser.class, userId);
		}catch(Exception e){
			logger.error("@@@@ Exception in UserManagementDAOImpl at getAdminUserById() :", e);
		}
		return adminUser;
	}

	@Override
	public String createUser(AfAdminUser adminUser) {
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(adminUser);
			
		} catch (ConstraintViolationException e) {
			logger.error("@@@@ Exception in UserManagementDAOImpl at createUser() :", e);	
			return "failure";
		}
		return "success";
	}

	@Override
	public boolean checkUserName(String userName) {
	AfAdminUser adminUser=null;
	boolean flag=false;
	Query query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_ADMIN_USER_BY_NAME);
	query.setString("userName", userName);
	try{
		adminUser=(AfAdminUser)query.uniqueResult() ;
		logger.debug(">>>>>>>>>..afAdminUser"+adminUser);
	}catch(Exception e){
		logger.error("@@@@ Exception in UserManagementDAOImpl at checkUserName() :", e);
	}
	if(adminUser!=null) 
		flag= true;
	else
		flag =false;
	return flag;
	}

	@Override
	public String deleteUser(Integer userId) {
		String deleteStatus="";
		AfAdminUser adminUser = (AfAdminUser) sessionFactory.getCurrentSession().get(AfAdminUser.class, userId);
		if(null!=adminUser){
			adminUser.setStatus((byte) 0);
			String userName=adminUser.getUserName();
			userName=userName+"_#deleted"+"_"+userId;
			adminUser.setUserName(userName);
		}
		try{
			sessionFactory.getCurrentSession().merge(adminUser);
			deleteStatus="User  Deleted Successfully";
		}catch(Exception e){
			deleteStatus="User not Deleted Exception is Occure";
			logger.error("@@@@ Exception in UserManagementDAOImpl at deleteUser() :", e);
		}	
		return deleteStatus;
	}


	@Override
	public List<AfAdminUser> getAdminUsers() {
		List<AfAdminUser> userList=new ArrayList<AfAdminUser>();
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(AfHibernateQueries.AF_ADMIN_USER);
		try{
			userList = query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in UserManagementDAOImpl at getAdminUsers() :", e);
		}
		return userList;
	}

	@Override
	public boolean checkRoleName(String roleName) {
		AfAdminRoles roles=null;
		boolean flag=false;
		Query query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_ADMIN_ROLE_BY_NAME);
		query.setString("roleName", roleName);
		try{
			roles=(AfAdminRoles)query.uniqueResult();
		}catch(Exception e){
			logger.error("@@@@ Exception in UserManagementDAOImpl at checkRoleName() :", e);
		}
		if(roles!=null) 
			flag= true;
		else
			flag =false;
		return flag;
	}

	@Override
	public String deleteRole(Integer roleId) {
		String deleteStatus="";
		AfAdminRoles afAdminRoles = (AfAdminRoles) sessionFactory.getCurrentSession().get(AfAdminRoles.class, roleId);
		if(null!=afAdminRoles){
			afAdminRoles.setStatus((byte) 0);
			String roleName=afAdminRoles.getRoleName();
			roleName=roleName+"_#deleted"+"_"+roleId;
			afAdminRoles.setRoleName(roleName);
			try{
				sessionFactory.getCurrentSession().merge(afAdminRoles);
				deleteStatus="success";
			}catch(Exception e){
				deleteStatus="failure";
				logger.error("@@@@ Exception in UserManagementDAOImpl at deleteRole() :", e);	
			}
	}
		return deleteStatus;
	}

	@Override
	public String createRole(AfAdminRoles afAdminRoles) {
		String status;
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(afAdminRoles);
			status="success";
		} catch (ConstraintViolationException e) {
			
			logger.error("@@@@ Exception in UserManagementDAOImpl at createRole() :", e);
			return "failure";
		}catch(Exception e){
			logger.error("@@@@ Exception in UserManagementDAOImpl at createRole() :", e);
			return "failure";
		}
		return status;
	}
	
	@Override
	public String updateRole(AfAdminRoles afAdminRoles) {
		String Status;
	try {
			
			sessionFactory.getCurrentSession().merge(afAdminRoles);
			Status = "success";
		} catch (ConstraintViolationException e) {
			logger.error("@@@@ Exception in UserManagementDAOImpl at updateRole() :", e);
			return "failure";
		}catch (Exception e) {
			logger.error("@@@@ Exception in UserManagementDAOImpl at updateRole() :", e);
			return "failure";
		}
		return Status;
	}
	

	@Override
	public AfAdminRoles getRoleById(String roleId) {
		Session session = sessionFactory.getCurrentSession();
		AfAdminRoles afAdminRoles =null;
		roleId=(null!=roleId)?roleId.trim():"";
		if(roleId.length()>0){
			try{
				afAdminRoles=(AfAdminRoles)session.get(AfAdminRoles.class,new Integer(roleId));
			}catch(Exception e){
				logger.error("@@@@ Exception in UserManagementDAOImpl at getRoleById() :", e);
			}
		}//if
		return afAdminRoles;
	}


	@Override
	public AfAdminRolePermissions getpermissionRoleById(String roleId) {
		Session session = sessionFactory.getCurrentSession();
		AfAdminRolePermissions afAdminRolePermissions =null;
		roleId=(null!=roleId)?roleId.trim():"";
		if(roleId.length()>0){
			try{
				afAdminRolePermissions=(AfAdminRolePermissions)session.get(AfAdminRolePermissions.class,new Integer(roleId));
			}catch(Exception e){
				logger.error("@@@@ Exception in UserManagementDAOImpl at getRoleById() :", e);
			}
		}//if
		return afAdminRolePermissions;
	}
	@Override
	public  String[] getRolePermissionsById(String roleId) {
		Session session = sessionFactory.getCurrentSession();
		AfAdminRoles afAdminRoles =null;
		List<AfAdminRolePermissions> afAdminRolesList=null;
		String[] getRolePermission=null;
		roleId=(null!=roleId)?roleId.trim():"";
		if(roleId.length()>0){
			try{
				Query query =session.createQuery(AfHibernateQueries.AF_GET_ROLE_PERMISSIONS_BY_ID);
				query.setInteger("roleId",Integer.parseInt(roleId));
				afAdminRolesList=query.list();
			}catch(Exception e){
				logger.error("@@@@ Exception in UserManagementDAOImpl at getRolePermissionsById() :", e);
			}
		}//if
		
		if(null!=afAdminRolesList){
			getRolePermission=new String[afAdminRolesList.size()];
			int count=0;
			for(AfAdminRolePermissions afAdminRolePermissions:afAdminRolesList){
				getRolePermission[count]=afAdminRolePermissions.getPermissionStatus()+"";
				count++;
			}
		}
		return getRolePermission;
	}

	@Override
	public String createAdminRolePermission(AfAdminRolePermissions afAdminRolePermissions) {
		String status;
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(afAdminRolePermissions);
			status="success";
		} catch (ConstraintViolationException e) {
			
			logger.error("@@@@ Exception in UserManagementDAOImpl at createAdminRolePermission() :", e);	
			return "failure";
		}catch(Exception e){
			logger.error("@@@@ Exception in UserManagementDAOImpl at createAdminRolePermission() :", e);
			return "failure";
		}
		return status;
	}
}

	


