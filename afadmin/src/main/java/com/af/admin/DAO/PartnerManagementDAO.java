package com.af.admin.DAO;

import java.util.List;

import com.af.admin.entity.AfAmazonPartnerCategory;
import com.af.admin.entity.AfPartner;
import com.af.admin.entity.AfPartnerCategory;
import com.af.admin.entity.AfPartnerKeyword;
import com.af.admin.entity.AfPartnerMapping;



public interface PartnerManagementDAO {

	public String updatePartner(AfPartner afPartner);
	public List<AfPartner> getPartnerByDefault();
	public AfPartner getPartnerByPartnerNO(String prtNo);
	public String createPartner(AfPartner afpartner);

	public boolean checkPartnerID(String partnerId);

	public String deletePartner(Integer prtNo);
	public List<AfPartner> getPartners();
	public List<AfPartner> getPartners(String partnerType);
	public List<AfPartner> getPartnersByLimit(String partnerType, String currPage, String pageLen);

	public AfPartner getPartnerByPartnerName(String partnerName);

	public AfPartnerCategory getPartnerCategoryByPartnerCategoryNo(Integer partnercategoryNo);
	public AfPartner getPartnerById(Integer prtNo);
	public List<AfPartner> checkLogoPrtDisplayOrder(String prtPos, String prtType);
	public List<AfPartner> checkLogoPrtTilePosition(String prtPos, String prtType);
	public List<AfPartner> getLogoPartners();
	public List<AfPartner> getOfflinePartner();
	public List<AfPartner> getHostPartner();
	public List<AfPartner> getTalkPartner();
	public List<AfPartner> getReadPartner();
	public List<AfPartnerKeyword> getKeywordFileName(String logoprtNo); 

	public String saveLogoPartnerConfig(String tileNo);

	public List<AfPartner> getDefaultAndHostPartners();
	public List<AfPartner> getAllPartners();
	public List<AfPartner> getAllPartnersExceptSearch();
	public String getLgoPrtTileSearchresult();
	public AfPartner getPartnerByName(String partnerName);
	public boolean checkActivityID(String activityId);
	public List<AfPartner> getLogoPartners(String prtType);
	
	public List<AfPartner> getAllPrtWebservice();
	public AfAmazonPartnerCategory getCategoryByPartner(String partNo);
	public List<AfPartner> getPartners(int length, int start, String partnerType);
	public Long getPartnerListPaginationCount(String partnerType);
	public boolean checkAggregateID(String aggregateid);
	public String createPartnerMapping(AfPartnerMapping afPartnerMapping);
	public List<AfPartnerMapping> getpartnerMappingDetails();
	public AfPartnerMapping getPartnerMappingDetailsById(Integer id);
	public String updatePartnerMapping(AfPartnerMapping afPartnerMapping);
	public String deletePartnerMapping(Integer id);
	public boolean MappingPartnerNameExist(String patName,String partNumber);
	public List<AfPartnerMapping> getPartnerMappingName(String store);
}
