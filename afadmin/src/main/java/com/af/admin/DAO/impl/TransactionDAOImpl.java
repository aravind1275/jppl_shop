package com.af.admin.DAO.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.af.admin.DAO.TransactionDAO;
import com.af.admin.constants.AfConstants;
import com.af.admin.entity.AfTransaction;

@Repository
public class TransactionDAOImpl implements TransactionDAO {

	
	@Autowired
	SessionFactory sessionFactory;
	private Logger logger = LogManager.getLogger(TransactionDAOImpl.class.getName());
	
	@Override
	public String createTransaction(AfTransaction afTransaction) {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			session.saveOrUpdate(afTransaction);
		} catch (ConstraintViolationException e) {
			
			logger.error("@@@@ Exception in TransactionDAOImpl at createPartner() :", e);	
			return AfConstants.FAILURE;
		}finally{
			session.flush();
			session.close();
		}
		return AfConstants.SUCCESS;
		
	}

	@Override
	public String getTransactionByID(String transactionID) {
		
		return null;
	}

}
