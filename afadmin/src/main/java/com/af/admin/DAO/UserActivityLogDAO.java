package com.af.admin.DAO;

import java.util.List;

import com.af.admin.entity.AfUserActivityLog;

public interface UserActivityLogDAO {

	public List<AfUserActivityLog> getUserActivityLogByDate(String userActFromDate, String userActTodate); 
	
	public List<AfUserActivityLog> getUserActivityLogByDateDefault(String currentDate); 

	public String createUserActLog(AfUserActivityLog afuserActLog);

	public String updateUserLog(AfUserActivityLog updateUsActLog);

}
