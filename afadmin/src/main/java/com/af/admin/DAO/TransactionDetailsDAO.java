package com.af.admin.DAO;

import java.util.List;

import com.af.admin.entity.AfPartnerProduct;
import com.af.admin.entity.AfTransaction;
import com.af.admin.entity.AfTransactionDetails;
import com.af.admin.entity.PartnerReportIn;

public interface TransactionDetailsDAO {

	  public List<Object[]> getTransactionReports(String jpNumber,String fromDate, String toDate);

		List<AfPartnerProduct> getProductNameById(String productId);

		List<AfTransaction> getTransactionId(String trackingId,String fromDate,String toDate);
		
		List<AfTransactionDetails> getTransactiondetails(String affOrderId, String affTransId);
		
		List<AfTransaction> getTransactionByTransactionId(String tranxId);
		
		public String updateTrarnsactionDetails(AfTransactionDetails aftransactionDetails);
		
		public String updateTransaction(AfTransaction transaction);

		public String updateTransaction(PartnerReportIn report);

		List<PartnerReportIn> getPartnerReportIn(String orderid, String transactionId);
		
		 List<AfTransactionDetails>  getTransactionDetails(String jpNumber);
		
		 List<AfTransactionDetails>  getDateTransactionDetails(String jpNumber ,String fromDate,String toDate);
	
}  
  