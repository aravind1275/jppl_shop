package com.af.admin.DAO;

import com.af.admin.entity.AfTransaction;

public interface TransactionDAO {

	public String createTransaction(AfTransaction afTransaction);
	
	public String getTransactionByID(String transactionID);
	
}
