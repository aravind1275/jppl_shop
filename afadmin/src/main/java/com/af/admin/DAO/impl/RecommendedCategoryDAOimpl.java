package com.af.admin.DAO.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.af.admin.DAO.BannerManagementDAO;
import com.af.admin.DAO.RecommendedCategoryDAO;
import com.af.admin.constants.AfHibernateQueries;
import com.af.admin.entity.AfAdminUser;
import com.af.admin.entity.AfBanner;
import com.af.admin.entity.AfRecommendedCategory;
import com.af.admin.entity.AfRecommendedProduct;

@Repository

public class RecommendedCategoryDAOimpl implements RecommendedCategoryDAO {
	private Logger logger = LogManager.getLogger(RecommendedCategoryDAOimpl.class.getName());

	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public List<AfRecommendedCategory> getRecommendedCategories() {
		List<AfRecommendedCategory> afrcList=new ArrayList<AfRecommendedCategory>();
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(AfHibernateQueries.AF_RECOMMENDED_CATEGORY);
		try{
			afrcList = query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in RecommendedCategoryDAOimpl at getRecommendedCategories() :", e);	
		}
		return afrcList;
	}
	
	@Override
	public List<AfRecommendedCategory> getRecommendedCategories(String prtNo) {
		List<AfRecommendedCategory> afrcList=new ArrayList<AfRecommendedCategory>();
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(AfHibernateQueries.AF_RECOMMENDED_CATEGORY_BY_NO);
		try{
			query.setParameter("prtNo", Integer.parseInt(prtNo));
			afrcList = query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in RecommendedCategoryDAOimpl at getRecommendedCategories() :", e);	
		}
		return afrcList;
	}
	
	@Override
	public String createRecommendedCategory(AfRecommendedCategory afRecommendedCategory) {
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(afRecommendedCategory);
			
		} catch (ConstraintViolationException e) {
			logger.error("@@@@ Exception in RecommendedCategoryDAOimpl at createRecommendedCategory() :", e);	
			return "failure";
		}
		return "success";
	}
	
	@Override
	public List<AfRecommendedCategory> getRecommendedCategory(Map rcmap) {		
		List<AfRecommendedCategory> afRecommendedCategory=new ArrayList<AfRecommendedCategory>();
		Session session = sessionFactory.getCurrentSession();
		try{
			logger.debug(rcmap.get("prtNo").toString()+"---"+rcmap.get("prtCatNo").toString());
			Query query = session.createQuery(AfHibernateQueries.AF_RECOMMENDED_CATEGORY_CHECK);		
			query.setParameter("prtNo", Integer.parseInt(rcmap.get("prtNo").toString()));
			query.setParameter("prtCatNo", Integer.parseInt(rcmap.get("prtCatNo").toString()));			
			afRecommendedCategory=query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in RecommendedCategoryDAOimpl at getRecommendedCategory() :", e);	
		}
		return afRecommendedCategory;
	}
	public static Date trim(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.HOUR, 23);
        return calendar.getTime();
    }
	
	@Override
	public AfRecommendedCategory getRecommendedCategoryByNo(String rcNo) {
		AfRecommendedCategory afRecommendedCategory=null;
		try{
			afRecommendedCategory =(AfRecommendedCategory) sessionFactory.getCurrentSession().get(AfRecommendedCategory.class, Integer.parseInt(rcNo));
		}catch(Exception e){
			logger.error("@@@@ Exception in RecommendedCategoryDAOimpl at getRecommendedCategoryByNo() :", e);
		}
		return afRecommendedCategory;
		
	}
	
	@Override
	public String updateRecommendedCategory(AfRecommendedCategory afRecommendedCategory) {
		String status;
		try {
			sessionFactory.getCurrentSession().merge(afRecommendedCategory);
			status="success";
		} catch (Exception e) {
			logger.debug("@@@@ Exception in RecommendedCategoryDAOimpl at updateRecommendedCategory()", e);
			return "failure";
		}
		return status;
		
	}
	
	@Override
	public String deleteRecommendedCategory(Integer prtNo) {
		String deleteStatus="";
		AfRecommendedCategory afRecommendedCategory = (AfRecommendedCategory) sessionFactory.getCurrentSession().get(AfRecommendedCategory.class, prtNo);
		if(null!=afRecommendedCategory){
			afRecommendedCategory.setStatus((byte) 0);			
		}
		try{
			sessionFactory.getCurrentSession().merge(afRecommendedCategory);
			deleteStatus="success";
		}catch(Exception e){
			deleteStatus="failure";
			logger.error("@@@@ Exception in RecommendedCategoryDAOimpl at deleteRecommendedCategory() :", e);	
			return deleteStatus;
		}	
		return deleteStatus;
	}

	@Override
	public List<AfRecommendedCategory> checkRecommendedCategoryPosition(String recommendedCategoryPosition) {
		List<AfRecommendedCategory> afRecommendedCategory=new ArrayList<AfRecommendedCategory>();
		Session session = sessionFactory.getCurrentSession();
		try{
			Query query = session.createQuery(AfHibernateQueries.AF_RECOMMENDED_CATEGORY_POSITION_CHECK);
			query.setParameter("recommendedCategoryPosition", Byte.parseByte(recommendedCategoryPosition));
			afRecommendedCategory=query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in RecommendedCategoryDAOimpl at checkRecommendedCategoryPosition() :", e);	
		}
		return afRecommendedCategory;
	}
	
	@Override
	public List<AfRecommendedCategory> checkRcCatNo(Integer rcCatno) {
		List<AfRecommendedCategory> afRecommendedCategory=new ArrayList<AfRecommendedCategory>();
		Session session = sessionFactory.getCurrentSession();
		try{
			Query query = session.createQuery(AfHibernateQueries.AF_RECOMMENDED_CATEGORY_BY_CATNO);
			query.setParameter("rcCatno",rcCatno);
			afRecommendedCategory=query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in RecommendedCategoryDAOimpl at checkRcCatNo() :", e);	
		}
		return afRecommendedCategory;
	}

}
