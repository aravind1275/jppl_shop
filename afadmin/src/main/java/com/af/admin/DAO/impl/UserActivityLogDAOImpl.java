package com.af.admin.DAO.impl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.af.admin.DAO.UserActivityLogDAO;
import com.af.admin.constants.AfConstants;
import com.af.admin.constants.AfHibernateQueries;
import com.af.admin.controller.UserManagementController;
import com.af.admin.entity.AfUserActivityLog;
import com.af.admin.utils.ServiceProviderUtil;

@Repository
public class UserActivityLogDAOImpl implements UserActivityLogDAO {

	
	private Logger logger = LogManager.getLogger(UserManagementController.class.getName());

	
	@Autowired
	SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AfUserActivityLog> getUserActivityLogByDate(String userActFromDate, String userActTodate) {
	
		Session session = null;
		Query query = null;
		Date startDate = null;
		Date endDate = null;
		List<AfUserActivityLog> results = null;
		SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd");
		
		try {
			session = sessionFactory.getCurrentSession();
			
			if( (userActFromDate != null) && !("".equals(userActFromDate)) ) {
			    
				if( (userActTodate != null) && !("".equals(userActTodate)) ) {
					startDate = ServiceProviderUtil.getInstance().getDateFromString(userActFromDate, AfConstants.DATE_FORMAT);
					endDate = ServiceProviderUtil.getInstance().getDateFromString(userActTodate, AfConstants.DATE_FORMAT);
					userActFromDate = dateFormater.format(startDate);
					
					Calendar c = Calendar.getInstance(); 
					c.setTime(endDate); 
					c.add(Calendar.DATE, 1);
					endDate = c.getTime();
					
					userActTodate =dateFormater.format(endDate);
					query= session.createQuery(AfHibernateQueries.GET_USER_ACT_LOG_WITH_START_AND_END_DATE);
					query.setString(AfConstants.FROM_DATE, userActFromDate);
					query.setString(AfConstants.TO_DATE, userActTodate);
					
				}else{
						startDate = ServiceProviderUtil.getInstance().getDateFromString(userActFromDate, AfConstants.DATE_FORMAT);
						userActFromDate = dateFormater.format(startDate);
						query= session.createQuery(AfHibernateQueries.GET_USER_ACT_LOG_WITH_START_DATE_ONLY);
						query.setString(AfConstants.FROM_DATE, userActFromDate);
					}
				}else{
						endDate = ServiceProviderUtil.getInstance().getDateFromString(userActTodate, AfConstants.DATE_FORMAT);
						Calendar c = Calendar.getInstance();
						c.setTime(endDate);
						c.add(Calendar.DATE, 1);
						endDate = c.getTime();
						userActTodate = dateFormater.format(endDate);
						query = session.createQuery(AfHibernateQueries.GET_USER_ACT_LOG_WITH_END_DATE_ONLY);
						query.setString(AfConstants.TO_DATE, userActTodate);
					}
						results=query.list();
			
		} catch (Exception e) {
			logger.error("@@@@ Exception in UserActivityLogDAOImpl at getUserActivityLogByDate() :", e);
		
		}
		
		return results;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AfUserActivityLog> getUserActivityLogByDateDefault(String currentDate) {
		
		Session session = null;
		Query query = null;
		Date endDate = null;
		List<AfUserActivityLog> results = null;
		SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd");
		
		try {
			session = sessionFactory.getCurrentSession();
			endDate = ServiceProviderUtil.getInstance().getDateFromString(currentDate, AfConstants.DATE_FORMAT);
			Calendar c = Calendar.getInstance();
			c.setTime(endDate);
			c.add(Calendar.DATE, 1);
			endDate = c.getTime();
			currentDate = dateFormater.format(endDate);
			query = session.createQuery(AfHibernateQueries.GET_USER_ACT_LOG_WITH_END_DATE_ONLY);
			query.setString(AfConstants.TO_DATE, currentDate);
			results = query.list();

		} catch (Exception e) {
			logger.error("@@@@ Exception in UserActivityLogDAOImpl at getUserActivityLogByDateDefault() :", e);
			
			}
		return results;
	}

	@Override
	public String createUserActLog(AfUserActivityLog afuserActLog) {
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(afuserActLog);

		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			logger.error("@@@@ Exception in UserActivityLogDAOImpl at createUserActLog() :", e);
			return "failure";
		}
		return "success";

	}

	@Override
	public String updateUserLog(AfUserActivityLog updateUsActLog) {
		try {
			sessionFactory.getCurrentSession().merge(updateUsActLog);
		} catch (Exception e) {
			logger.error("@@@@ Exception in UserActivityLogDAOImpl at updateUserLog() :", e);
			return "failure";
		}
		return "success";
	}

}
