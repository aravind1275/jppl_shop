package com.af.admin.DAO;

import java.util.List;

import com.af.admin.entity.AfAdminUser;
import com.af.admin.entity.AfBanner;;

public interface BannerManagementDAO {

	public List<AfBanner> getBanners();
	public String createBanner(AfBanner afBanner);
	public AfBanner getBannerById(Integer bnrNo);
	public String updateBanner(AfBanner afBanner);
	public String deleteBanner(Integer bnrNo);
	public boolean checkBannerID(String bannerID); 
	public boolean checkUtmParameter(String source,String medium,String campaign);
	public List<AfBanner> getBanners(int length, int start);
	public Long getBannerListPaginationCount(); 
}
