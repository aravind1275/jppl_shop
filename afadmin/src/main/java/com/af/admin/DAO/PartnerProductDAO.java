package com.af.admin.DAO;

import com.af.admin.entity.AfPartnerProduct;
import com.af.admin.entity.AfProduct;

public interface PartnerProductDAO {
	
	public AfPartnerProduct getPartnerProductByPrtPrdNO(String prtPrdNo);
	public AfProduct getProductByPrtPrdNO(String prtPrdNo);
	
	public AfProduct getMasterProductByPrdNO(String prtPrdNo);
	
	
}
