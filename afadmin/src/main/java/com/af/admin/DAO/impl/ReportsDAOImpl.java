package com.af.admin.DAO.impl;

import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.af.admin.DAO.ReportsDAO;
import com.af.admin.beans.BuisnessReportsBean;
import com.af.admin.constants.AfHibernateQueries;
import com.af.admin.entity.AfOfferDealLink;
import com.af.admin.entity.AfPartnerProduct;
import com.af.admin.entity.AfProduct;
import com.af.admin.entity.AfTransaction;
import com.af.admin.entity.AfTransactionDetails;
import com.af.admin.utils.ServiceProviderUtil;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

@Repository     
public class ReportsDAOImpl implements ReportsDAO {

	private Logger logger = LogManager.getLogger(ReportsDAOImpl.class.getName());

	@Autowired  
	SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override 
	public List<Object[]> getShopJPReports(String fromDate, String toDate, String prtNo, String jpNumber,String status,String statusType,Integer length,Integer start) {
		Session session = null;
		Query query = null;
		List<Object[]> results =null;
		Date startDate = null;
		Date endDate = null;
		StringBuffer queryBuffer = null;
		boolean isDateYes = false;
		boolean ispartnerNameYes = false;
		SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd");
		boolean allValid=false;
		boolean allInvalid=false;
		
		try {
			System.out.println("status---->"+status);
			session = sessionFactory.getCurrentSession();
			if("Tentative".equals(status))
					queryBuffer = new StringBuffer(AfHibernateQueries.GET_BUSINESS_REPORT_FOR_TENTATIVE);
			else if("Processed".equals(status))
					queryBuffer = new StringBuffer(AfHibernateQueries.GET_BUSINESS_REPORT_FOR_PROCESSED);
			else if("Failed".equals(status))
					queryBuffer = new StringBuffer(AfHibernateQueries.GET_BUSINESS_REPORT_FOR_FAILED);
			
			else if("JP Number Missing".equals(status))
                queryBuffer = new StringBuffer(AfHibernateQueries.GET_BUSINESS_REPORT_FOR_JP_NUMBER_MISSING);
			else if("Reference Number Missing".equals(status))
                queryBuffer = new StringBuffer(AfHibernateQueries.GET_BUSINESS_REPORT_FOR_REFERENCE_NUMBER_MISSING);
			else if("JP And Reference Number Missing".equals(status))
                queryBuffer = new StringBuffer(AfHibernateQueries.GET_BUSINESS_REPORT_FOR_JP_AND_REFERENCE_NUMBER_MISSING);
			else if("Others".equals(status))
                queryBuffer = new StringBuffer(AfHibernateQueries.GET_BUSINESS_REPORT_FOR_OTHERS);
			
			
			
			else if ("All".equals(status)) {
				if ("valid".equals(statusType)) {
					allValid = true;
					queryBuffer = new StringBuffer(AfHibernateQueries.GET_BUSINESS_REPORT_FOR_ALL_VALID);
				}
				if ("invalid".equals(statusType)) {
					allInvalid =true;
					queryBuffer = new StringBuffer(AfHibernateQueries.GET_BUSINESS_REPORT_FOR_ALL_INVALID);
				}
			}
			
			System.out.println("querry before date ---->"+queryBuffer);
			
			if( (fromDate != null) && !("".equals(fromDate)) ) {
				isDateYes = true;
				startDate = ServiceProviderUtil.getInstance().getDateFromString(fromDate, "dd/MM/yyyy");
				endDate = ServiceProviderUtil.getInstance().getDateFromString(toDate, "dd/MM/yyyy");
				fromDate = dateFormater.format(startDate);
				toDate =dateFormater.format(endDate);
				queryBuffer.append("  date(pr1.FORMATTED_ORDER_DATE) between '" + fromDate + "' and '" + toDate + "'");
			}
			if( (prtNo != null) && !(prtNo.equals("")) ) {
				ispartnerNameYes = true;
				queryBuffer.append(isDateYes ? " and " : "");
				queryBuffer.append(" pr1.PRT_NO=" + Integer.parseInt(prtNo));
			}
			if( (jpNumber != null) && !(jpNumber.equals("")) ) {
				queryBuffer.append(isDateYes || ispartnerNameYes ? " and " : "");
				jpNumber = (jpNumber.length() <= 9) ? "00" + jpNumber : jpNumber;
				queryBuffer.append(" (tr.JP_NUMBER='" + jpNumber + "' or tr.JP_NUMBER='"+Long.valueOf(jpNumber) + "')");
			}
			if (allValid) {
				queryBuffer.append(" order by pr1.FORMATTED_ORDER_DATE LIMIT "+length+" OFFSET "+start );
				query = session.createSQLQuery(queryBuffer.toString());
			} else if(allInvalid) {
				queryBuffer.append(" order by pr1.FORMATTED_ORDER_DATE desc LIMIT "+length+" OFFSET "+start );
				query = session.createSQLQuery(queryBuffer.toString());
			}else{
				queryBuffer.append(" group by pr1.ORDER_ID order by pr1.FORMATTED_ORDER_DATE LIMIT "+length+" OFFSET "+start );
				query = session.createSQLQuery(queryBuffer.toString());
			}
			
			System.out.println("query--->"+query);
			results = query.list();
			System.out.println("results----->"+results);
		}
		catch(Exception e) {
			logger.error("@@@@ Exception in ReportsDAOImpl at getShopJPReportInfo() :", e);
			e.printStackTrace();
		}
		return results;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> getShopJPReports(String fromDate, String toDate, String prtNo, String jpNumber,String status,String statusType) {
		Session session = null;
		Query query = null;
		List<Object[]> results =null;
		Date startDate = null;
		Date endDate = null;
		StringBuffer queryBuffer = null;
		boolean isDateYes = false;
		boolean ispartnerNameYes = false;
		SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd");
		boolean allValid=false;
		boolean allInvalid=false;
		
		try {
			System.out.println("inside getShopJPReports analytics");
			session = sessionFactory.openSession();
			if("Tentative".equals(status))
					queryBuffer = new StringBuffer(AfHibernateQueries.GET_BUSINESS_REPORT_FOR_TENTATIVE);
			else if("Processed".equals(status))
					queryBuffer = new StringBuffer(AfHibernateQueries.GET_BUSINESS_REPORT_FOR_PROCESSED);
			else if("Failed".equals(status))
					queryBuffer = new StringBuffer(AfHibernateQueries.GET_BUSINESS_REPORT_FOR_FAILED);
			
			else if("JP Number Missing".equals(status))
                queryBuffer = new StringBuffer(AfHibernateQueries.GET_BUSINESS_REPORT_FOR_JP_NUMBER_MISSING);
			else if("Reference Number Missing".equals(status))
                queryBuffer = new StringBuffer(AfHibernateQueries.GET_BUSINESS_REPORT_FOR_REFERENCE_NUMBER_MISSING);
			else if("JP And Reference Number Missing".equals(status))
                queryBuffer = new StringBuffer(AfHibernateQueries.GET_BUSINESS_REPORT_FOR_JP_AND_REFERENCE_NUMBER_MISSING);
			else if("Others".equals(status))
                queryBuffer = new StringBuffer(AfHibernateQueries.GET_BUSINESS_REPORT_FOR_OTHERS);
			
			
			else if ("All".equals(status)) {
				if ("valid".equals(statusType)) {
					allValid = true;
					queryBuffer = new StringBuffer(AfHibernateQueries.GET_BUSINESS_REPORT_FOR_ALL_VALID);
				}
				if ("invalid".equals(statusType)) {
					allInvalid =true;
					queryBuffer = new StringBuffer(AfHibernateQueries.GET_BUSINESS_REPORT_FOR_ALL_INVALID);
				}
			}
			if( (fromDate != null) && !("".equals(fromDate)) ) {
				isDateYes = true;
				startDate = ServiceProviderUtil.getInstance().getDateFromString(fromDate, "dd/MM/yyyy");
				endDate = ServiceProviderUtil.getInstance().getDateFromString(toDate, "dd/MM/yyyy");
				fromDate = dateFormater.format(startDate);
				toDate =dateFormater.format(endDate);
				queryBuffer.append("  date(pr1.FORMATTED_ORDER_DATE) between '" + fromDate + "' and '" + toDate + "'");
			}
			if( (prtNo != null) && !(prtNo.equals("")) ) {
				ispartnerNameYes = true;
				queryBuffer.append(isDateYes ? " and " : "");
				queryBuffer.append(" pr1.PRT_NO=" + Integer.parseInt(prtNo));
			}
			if( (jpNumber != null) && !(jpNumber.equals("")) ) {
				queryBuffer.append(isDateYes || ispartnerNameYes ? " and " : "");
				jpNumber = (jpNumber.length() <= 9) ? "00" + jpNumber : jpNumber;
				queryBuffer.append(" (tr.JP_NUMBER='" + jpNumber + "' or tr.JP_NUMBER='"+Long.valueOf(jpNumber) + "')");
			}
			if (allValid) {
				queryBuffer.append(" order by pr1.FORMATTED_ORDER_DATE");
				query = session.createSQLQuery(queryBuffer.toString());
			} else if(allInvalid) {
				queryBuffer.append(" order by pr1.FORMATTED_ORDER_DATE desc LIMIT 0,10000");
				query = session.createSQLQuery(queryBuffer.toString());
			}else{
				queryBuffer.append(" group by pr1.ORDER_ID order by pr1.FORMATTED_ORDER_DATE");
				query = session.createSQLQuery(queryBuffer.toString());
			}
			results = query.list();
			System.out.println("result-------->"+results);
		}
		catch(Exception e) {
			e.printStackTrace();
			logger.error("@@@@ Exception in ReportsDAOImpl at getShopJPReportInfo() :", e);
		}finally {
			session.close();
		}
		
		return results;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Object> getPartnersByProducts() {
		List<Object> partnerList = new ArrayList<Object>();
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(AfHibernateQueries.GET_NEW_PRODUCTS_REPORT_PARTNERS_BY_PRODUCTS);

		try {
			partnerList = query.list();
		} catch(Exception e) {
			e.printStackTrace();
			logger.error("@@@@ Exception in ReportsDAOImpl at getPartnersByProducts() :", e);	
		}
		return partnerList;
	}

	@SuppressWarnings("unchecked")
	public List<String> getBrandsByProducts(String toDate, String fromDate) {
		List<String> result = new ArrayList<String>();
		Date startDate = null;
		Date endDate = null;
		try {
			Session session = sessionFactory.getCurrentSession();
			Query query = session.createQuery(AfHibernateQueries.GET_NEW_PRODUCTS_REPORT_BRANDS_BY_PRODUCTS);
			startDate = ServiceProviderUtil.getInstance().getDateFromString(fromDate, "dd/MM/yyyy");
			endDate = ServiceProviderUtil.getInstance().getDateFromString(toDate, "dd/MM/yyyy");
			query.setDate("toDate", endDate);
			query.setDate("fromDate", startDate);
			query.setDate("toDate", endDate);
			query.setDate("fromDate", startDate);
			result = query.list();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("@@@@ Exception in ReportsDAOImpl at getBrandsByProducts() :", e);
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AfPartnerProduct> getNewProductsReportList(String fromDate,
			String toDate, String partnerNo, String brandName) {
		Session session = null;
		Query query = null;
		List<AfPartnerProduct> result = null;
		Date startDate = null;
		Date endDate = null;
		try {
			session = sessionFactory.getCurrentSession();
			query = session.createQuery(AfHibernateQueries.GET_NEW_PRODUCTS_REPORT);

			query.setParameter("productBrand", brandName);
			query.setParameter("prtNo", Integer.parseInt(partnerNo));

			startDate = ServiceProviderUtil.getInstance().getDateFromString(fromDate, "dd/MM/yyyy");
			endDate = ServiceProviderUtil.getInstance().getDateFromString(toDate, "dd/MM/yyyy");
			query.setDate("toDate", endDate);
			query.setDate("fromDate", startDate);
			query.setDate("toDate", endDate);
			query.setDate("fromDate", startDate);

			result = query.list();
		}
		catch(Exception e) {
			e.printStackTrace();
			logger.error("@@@@ Exception in ReportsDAOImpl at getNewProductsReportList() :", e);
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AfProduct> getMappedProductsReportList(String selectBy, String selectByValue) {
		Session session = null;
		Query query = null;
		List<AfProduct> result = null;
		StringBuffer stringBuffer = null;

		try {
			session = sessionFactory.getCurrentSession();
			stringBuffer = new StringBuffer(AfHibernateQueries.GET_MAPPED_PRODUCTS_REPORT);

			selectBy = ServiceProviderUtil.nullSafe(selectBy, "");
			selectByValue = ServiceProviderUtil.nullSafe(selectByValue, "");

			if(!selectBy.isEmpty() && !selectByValue.isEmpty()) {
				switch (selectBy) {
				case "MPID":
					stringBuffer.append(" and afp.masterProductId='"+ selectByValue +"'");
					break;

				case "PID":
					stringBuffer.append(" and afpp.productId='"+ selectByValue +"'");
					break;

				case "PRDNAME":
					stringBuffer.append(" and afp.productName='"+ selectByValue +"'");
					break;

				case "BRANDNAME":
					stringBuffer.append(" and afp.productBrand='"+ selectByValue +"'");
					break;

				case "JPPLCAT":
					stringBuffer.append(" and afp.afCategory.catNo='"+ Integer.parseInt(selectByValue) +"'");
					break;

				case "PRTNAME":
					stringBuffer.append(" and afpp.afPartner.prtNo='"+ Integer.parseInt(selectByValue) +"'");
					break;

				default:
					break;
				}
			}
			stringBuffer.append(AfHibernateQueries.GET_MAPPED_PRODUCTS_REPORT_JOIN);

			query = session.createQuery(stringBuffer.toString());

			result = query.list();
		}
		catch(Exception e) {
			e.printStackTrace();
			logger.error("@@@@ Exception in ReportsDAOImpl at getMappedProductsReportList() :", e);
		}
		return result;
	}


	@Override
	public List<BuisnessReportsBean> getdailyMilesReports(String fromDate, String toDate, String prtNo,
			String jpNumber) {
		Session session = null;
		Query query = null;
		List<BuisnessReportsBean> result = null;
		Date startDate = null;
		Date endDate = null;
		StringBuffer queryBuffer = null;
		boolean isDateYes = false;
		boolean ispartnerNameYes = false;
		SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd");
		try {
			session = sessionFactory.getCurrentSession();
			queryBuffer = new StringBuffer(AfHibernateQueries.GET_DAILY_MILES_STATUS_REPORT);

			if( (fromDate != null) && !(fromDate.equals("")) ) {
				isDateYes = true;
				startDate = ServiceProviderUtil.getInstance().getDateFromString(fromDate, "dd/MM/yyyy");
				endDate = ServiceProviderUtil.getInstance().getDateFromString(toDate, "dd/MM/yyyy");
				fromDate = dateFormater.format(startDate);
				toDate = dateFormater.format(endDate);
				queryBuffer.append(" date(td.expectedDateOfCredit) between '" + fromDate + "' and '" + toDate + "'");
			}
			if( (prtNo != null) && !(prtNo.equals("")) ) {
				ispartnerNameYes = true;
				queryBuffer.append(isDateYes ? " and " : "");
				queryBuffer.append(" td.afPartner.prtNo=" + Integer.parseInt(prtNo));
			}
			if( (jpNumber != null) && !(jpNumber.equals("")) ) {
				queryBuffer.append(isDateYes || ispartnerNameYes ? " and " : "");
				jpNumber = (jpNumber.length() <= 9) ? "00" + jpNumber : jpNumber;
				queryBuffer.append(" (tr.jpNumber='" + jpNumber + "' or tr.jpNumber='"+Long.valueOf(jpNumber) + "')");
			}
			query = session.createQuery(queryBuffer.toString());

			result = query.list();
		}
		catch(Exception e) {
			e.printStackTrace();
			logger.error("@@@@ Exception in ReportsDAOImpl at getdailyMilesReports() :", e);
		}
		return result;
	}
	@Override
	public String downloadCrisRequest(String orderId) {
		Query query=null;
		String request="";
		try{
			query=sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_GET_CRIS_REQUEST);
			query.setParameter("orderId", orderId);
			query.setMaxResults(1);
			request=(String)query.uniqueResult();
		}catch(Exception e){
		}
		return request;
	}

	@Override
	public String downloadCrisResponse(String orderId) {
		Query query=null;
		String response="";
		try{
			query=sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_GET_CRIS_RESPONSE);
			query.setString("orderId", orderId);
			query.setMaxResults(1);
			response=(String)query.uniqueResult();
		}catch(Exception e){
			logger.error("@@@@ Exception in ReportsDAOImpl downloadCRISResponse() :",e);
		}
		return response;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BuisnessReportsBean> getqueingReportList(String fromDate, String toDate, String prtNo,
			String jpNumber) {
		Session session = null;
		Query query = null;
		List<BuisnessReportsBean> result = null;
		Date startDate = null;
		Date endDate = null;
		StringBuffer queryBuffer = null;
		boolean isDateYes = false;
		boolean ispartnerNameYes = false;
		SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd");
		try {
			session = sessionFactory.getCurrentSession();
			queryBuffer = new StringBuffer(AfHibernateQueries.GET_FAILED_TRANSACTION_STATUS_REPORT);

			if( (fromDate != null) && !(fromDate.equals("")) ) {
				isDateYes = true;
				startDate = ServiceProviderUtil.getInstance().getDateFromString(fromDate, "dd/MM/yyyy");
				endDate = ServiceProviderUtil.getInstance().getDateFromString(toDate, "dd/MM/yyyy");
				fromDate = dateFormater.format(startDate);
				toDate = dateFormater.format(endDate);
				queryBuffer.append(" date(td.expectedDateOfCredit) between '" + fromDate + "' and '" + toDate + "'");
			}
			if( (prtNo != null) && !(prtNo.equals("")) ) {
				ispartnerNameYes = true;
				queryBuffer.append(isDateYes ? " and " : "");
				queryBuffer.append(" td.afPartner.prtNo=" + Integer.parseInt(prtNo));
			}
			if( (jpNumber != null) && !(jpNumber.equals("")) ) {
				queryBuffer.append(isDateYes || ispartnerNameYes ? " and " : "");
				jpNumber = (jpNumber.length() <= 9) ? "00" + jpNumber : jpNumber;
				queryBuffer.append(" (tr.jpNumber='" + jpNumber + "' or tr.jpNumber='"+Long.valueOf(jpNumber) + "')");
			}
			query = session.createQuery(queryBuffer.toString());

			result = query.list();

		} catch(Exception e) {
			e.printStackTrace();
			logger.error("@@@Exception in ReportsDAOImpl getqueingReportList():", e);
		}
		return result;
	}


	@Override
	public Long getOrderCount(String partnerItemId) {
		Query query = null;
		Long count = 0L;
		try {
			query=sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_GET_OREDER_COUNT);
			query.setString("orderId", partnerItemId);
			count = (Long) query.uniqueResult();
		} catch(Exception e) {
			e.printStackTrace();
			logger.error("@@@Exception in ReportsDAOImpl getOrderCount():", e);
		}
		return count;
	}

	@Override
	public List<Object[]> getShopJPReportsZeroMiles(String fromDate, String toDate, String prtNo, String jpNumber,
			String status, String statusType) {
		Session session = null;
		Query query = null;
		List<Object[]> results =null;
		Date startDate = null;
		Date endDate = null;
		StringBuffer queryBuffer = null;
		boolean isDateYes = false;
		boolean ispartnerNameYes = false;
		SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd");
		boolean partnerFlag=true;
		
		try {
			session = sessionFactory.getCurrentSession();
			if("Tentative".equals(status))
					queryBuffer = new StringBuffer(AfHibernateQueries.GET_REPORT_FOR_TENTATIVE_WITH_ZERO_JP_MILES);
			else if("Processed".equals(status))
					queryBuffer = new StringBuffer(AfHibernateQueries.GET_REPORT_FOR_PROCESSED_WITH_ZERO_JP_MILES);
			else if("Failed".equals(status))
					queryBuffer = new StringBuffer(AfHibernateQueries.GET_REPORT_FOR_FAILED_WITH_ZERO_JP_MILES);
			
			else if("JP Number Missing".equals(status))
                queryBuffer = new StringBuffer(AfHibernateQueries.GET_REPORT_FOR_JP_NUMBER_MISSING_WITH_ZERO_JP_MILES);
else if("Reference Number Missing".equals(status))
                queryBuffer = new StringBuffer(AfHibernateQueries.GET_REPORT_FOR_REFERENCE_NUMBER_MISSING_WITH_ZERO_JP_MILES);
else if("JP And Reference Number Missing".equals(status))
                queryBuffer = new StringBuffer(AfHibernateQueries.GET_REPORT_FOR_JP_AND_REFERENCE_NUMBER_MISSING_WITH_ZERO_JP_MILES);

else if("Others".equals(status))
                queryBuffer = new StringBuffer(AfHibernateQueries.GET_REPORT_REPORT_FOR_OTHERS_WITH_ZERO_JP_MILES);
			
		
			else if ("All".equals(status)) {
				if ("valid".equals(statusType)) {
					queryBuffer = new StringBuffer(AfHibernateQueries.GET_REPORT_FOR_ALL_VALID_WITH_ZERO_JP_MILES);
				}
				if ("invalid".equals(statusType)) {
					partnerFlag =false;
					queryBuffer = new StringBuffer(AfHibernateQueries.GET_REPORT_FOR_ALL_INVALID_WITH_ZERO_JP_MILES);
				}
			}
			if( (fromDate != null) && !("".equals(fromDate)) ) {
				isDateYes = true;
				startDate = ServiceProviderUtil.getInstance().getDateFromString(fromDate, "dd/MM/yyyy");
				endDate = ServiceProviderUtil.getInstance().getDateFromString(toDate, "dd/MM/yyyy");
				
				Calendar c = Calendar.getInstance(); 
				c.setTime(endDate); 
				c.add(Calendar.DATE, 1);
				endDate = c.getTime();
				
				fromDate = dateFormater.format(startDate);
				toDate =dateFormater.format(endDate);
				queryBuffer.append("  date(pr1.FORMATTED_ORDER_DATE) between '" + fromDate + "' and '" + toDate + "'");
				}
			if( (prtNo != null) && !(prtNo.equals("")) ) {
				ispartnerNameYes = true;
				queryBuffer.append(isDateYes ? " and " : "");
				queryBuffer.append(" pr1.PRT_NO=" + Integer.parseInt(prtNo));
			}
			if( (jpNumber != null) && !(jpNumber.equals("")) ) {
				queryBuffer.append(isDateYes || ispartnerNameYes ? " and " : "");
				jpNumber = (jpNumber.length() <= 9) ? "00" + jpNumber : jpNumber;
				queryBuffer.append(" (tr.JP_NUMBER='" + jpNumber + "' or tr.JP_NUMBER='"+Long.valueOf(jpNumber) + "')");
			}
			
			if (partnerFlag) {
				queryBuffer.append(" order by pr1.FORMATTED_ORDER_DATE");
				query = session.createSQLQuery(queryBuffer.toString());
			} else {
				queryBuffer.append(" GROUP by pr1.TRANSACTION_ID and pr1.ORDER_ID order by pr1.FORMATTED_ORDER_DATE desc");
				query = session.createSQLQuery(queryBuffer.toString());
			}
			results = query.list();

		}
		catch(Exception e) {
			e.printStackTrace();
			logger.error("@@@@ Exception in ReportsDAOImpl at getShopJPReportInfo() :", e);
		}
		return results;
	}

	
	
	@Override
	public List<Object[]> getShopInValidReports(String fromDate, String toDate, String prtNo, String jpNumber, Integer length, Integer start) {
		
		Session session = null;
		Query query = null;
		List<Object[]> results =null;
		Date startDate = null;
		Date endDate = null;
		StringBuffer queryBuffer = null;
		boolean isDateYes = false;
		boolean ispartnerNameYes = false;
		SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd");
		boolean allValid=false;
		boolean allInvalid=false;
		
		try {
			session = sessionFactory.getCurrentSession();
			queryBuffer = new StringBuffer(AfHibernateQueries.GET_INVALID_REPORT_FOR_ALL_INVALID);
			
			if( (fromDate != null) && !("".equals(fromDate)) ) {
				isDateYes = true;
				startDate = ServiceProviderUtil.getInstance().getDateFromString(fromDate, "dd/MM/yyyy");
				endDate = ServiceProviderUtil.getInstance().getDateFromString(toDate, "dd/MM/yyyy");
				fromDate = dateFormater.format(startDate);
				toDate =dateFormater.format(endDate);
				queryBuffer.append("  date(pr1.FORMATTED_ORDER_DATE) between '" + fromDate + "' and '" + toDate + "'");
			}
			if( (prtNo != null) && !(prtNo.equals("")) ) {
				ispartnerNameYes = true;
				queryBuffer.append(isDateYes ? " and " : "");
				queryBuffer.append(" pr1.PRT_NO=" + Integer.parseInt(prtNo));
			}
			if( (jpNumber != null) && !(jpNumber.equals("")) ) {
				queryBuffer.append(isDateYes || ispartnerNameYes ? " and " : "");
				jpNumber = (jpNumber.length() <= 9) ? "00" + jpNumber : jpNumber;
				queryBuffer.append(" (tr.JP_NUMBER='" + jpNumber + "' or tr.JP_NUMBER='"+Long.valueOf(jpNumber) + "')");
			}
			
				queryBuffer.append(" order by pr1.FORMATTED_ORDER_DATE desc LIMIT "+length+" OFFSET "+start );
				query = session.createSQLQuery(queryBuffer.toString());
			//System.out.println("query-->"+query);
			/*Calendar cal = Calendar.getInstance();
				results = query.list();
				Calendar cal2 = Calendar.getInstance();
				System.out.println("DAo"+(cal2.getTimeInMillis()-cal.getTimeInMillis()));*/
			     results = query.list();
		}
		catch(Exception e) {
			logger.error("@@@@ Exception in ReportsDAOImpl at getShopInValidReports() :", e);
		}
		return results;
	}

	@Override
	public List<Object[]> getShopInValidReports(String fromDate, String toDate, String prtNo, String jpNumber) {
		Session session = null;
		Query query = null;
		List<Object[]> results =null;
		Date startDate = null;
		Date endDate = null;
		StringBuffer queryBuffer = null;
		boolean isDateYes = false;
		boolean ispartnerNameYes = false;
		SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd");
		
		try {
			session = sessionFactory.openSession();
			queryBuffer = new StringBuffer(AfHibernateQueries.GET_INVALID_REPORT_FOR_ALL_INVALID);
		
			if( (fromDate != null) && !("".equals(fromDate)) ) {
				isDateYes = true;
				startDate = ServiceProviderUtil.getInstance().getDateFromString(fromDate, "dd/MM/yyyy");
				endDate = ServiceProviderUtil.getInstance().getDateFromString(toDate, "dd/MM/yyyy");
				fromDate = dateFormater.format(startDate);
				toDate =dateFormater.format(endDate);
				queryBuffer.append("  date(pr1.FORMATTED_ORDER_DATE) between '" + fromDate + "' and '" + toDate + "'");
			}
			if( (prtNo != null) && !(prtNo.equals("")) ) {
				ispartnerNameYes = true;
				queryBuffer.append(isDateYes ? " and " : "");
				queryBuffer.append(" pr1.PRT_NO=" + Integer.parseInt(prtNo));
			}
			if( (jpNumber != null) && !(jpNumber.equals("")) ) {
				queryBuffer.append(isDateYes || ispartnerNameYes ? " and " : "");
				jpNumber = (jpNumber.length() <= 9) ? "00" + jpNumber : jpNumber;
				queryBuffer.append(" (tr.JP_NUMBER='" + jpNumber + "' or tr.JP_NUMBER='"+Long.valueOf(jpNumber) + "')");
			}
			 
		
			queryBuffer.append(" order by pr1.FORMATTED_ORDER_DATE desc");
			query = session.createSQLQuery(queryBuffer.toString());
			results = query.list();
			/*Calendar cal2 = Calendar.getInstance();
			System.out.println("DAo"+(cal2.getTimeInMillis()-cal.getTimeInMillis()));*/
		}
		catch(Exception e) {
			logger.error("@@@@ Exception in ReportsDAOImpl at getShopInValidReports() :", e);
		}
		return results;
	}

	@Override
	public List<Object[]> getJPMilesStatusReports(String fromDate, String toDate, String prtNo, String jpNumber,
			String status, String statusType, Integer length, Integer start) {
		
		Session session = null;
		Query query = null;
		List<Object[]> results =null;
		Date startDate = null;
		Date endDate = null;
		StringBuffer queryBuffer = null;
		boolean isDateYes = false;
		boolean ispartnerNameYes = false;
		SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd");
		boolean allValid=false;
		boolean allInvalid=false;
		try {
			session = sessionFactory.getCurrentSession();
			if("Tentative".equals(status))
					queryBuffer = new StringBuffer(AfHibernateQueries.GET_JPMILESTATUS_REPORT_FOR_TENTATIVE);
			else if("Processed".equals(status))
					queryBuffer = new StringBuffer(AfHibernateQueries.GET_JPMILESTATUS_REPORT_FOR_PROCESSED);
			else if("Failed".equals(status))
					queryBuffer = new StringBuffer(AfHibernateQueries.GET_JPMILESTATUS_REPORT_FOR_FAILED);
			else if("JP Number Missing".equals(status))
                queryBuffer = new StringBuffer(AfHibernateQueries.GET_BUSINESS_REPORT_FOR_JP_NUMBER_MISSING);
else if("Reference Number Missing".equals(status))
                queryBuffer = new StringBuffer(AfHibernateQueries.GET_BUSINESS_REPORT_FOR_REFERENCE_NUMBER_MISSING);
else if("JP And Reference Number Missing".equals(status))
                queryBuffer = new StringBuffer(AfHibernateQueries.GET_BUSINESS_REPORT_FOR_JP_AND_REFERENCE_NUMBER_MISSING);

else if("Others".equals(status))
                queryBuffer = new StringBuffer(AfHibernateQueries.GET_BUSINESS_REPORT_FOR_OTHERS);
			
			
			
			
			else if ("All".equals(status)) {
				if ("valid".equals(statusType)) {
					allValid = true;
					queryBuffer = new StringBuffer(AfHibernateQueries.GET_JPMILESTATUS_REPORT_FOR_ALL_VALID);
				}
				if ("invalid".equals(statusType)) {
					allInvalid =true;
					queryBuffer = new StringBuffer(AfHibernateQueries.GET_JPMILESTATUS_REPORT_FOR_ALL_INVALID);
				}
			}
			if( (fromDate != null) && !("".equals(fromDate)) ) {
				isDateYes = true;
				startDate = ServiceProviderUtil.getInstance().getDateFromString(fromDate, "dd/MM/yyyy");
				endDate = ServiceProviderUtil.getInstance().getDateFromString(toDate, "dd/MM/yyyy");
				fromDate = dateFormater.format(startDate);
				toDate =dateFormater.format(endDate);
				queryBuffer.append("  date(pr1.FORMATTED_ORDER_DATE) between '" + fromDate + "' and '" + toDate + "'");
			}
			if( (prtNo != null) && !(prtNo.equals("")) ) {
				ispartnerNameYes = true;
				queryBuffer.append(isDateYes ? " and " : "");
				queryBuffer.append(" pr1.PRT_NO=" + Integer.parseInt(prtNo));
			}
			if( (jpNumber != null) && !(jpNumber.equals("")) ) {
				queryBuffer.append(isDateYes || ispartnerNameYes ? " and " : "");
				jpNumber = (jpNumber.length() <= 9) ? "00" + jpNumber : jpNumber;
				queryBuffer.append(" (tr.JP_NUMBER='" + jpNumber + "' or tr.JP_NUMBER='"+Long.valueOf(jpNumber) + "')");
			}
			if (allValid) {
				queryBuffer.append(" order by pr1.FORMATTED_ORDER_DATE LIMIT "+length+" OFFSET "+start );
				query = session.createSQLQuery(queryBuffer.toString());
			} else if(allInvalid) {
				queryBuffer.append(" order by pr1.FORMATTED_ORDER_DATE desc LIMIT "+length+" OFFSET "+start );
				query = session.createSQLQuery(queryBuffer.toString());
			}else{
				queryBuffer.append(" group by pr1.ORDER_ID order by pr1.FORMATTED_ORDER_DATE LIMIT "+length+" OFFSET "+start );
				query = session.createSQLQuery(queryBuffer.toString());
			}
			results = query.list();
		}
		catch(Exception e) {
			logger.error("@@@@ Exception in ReportsDAOImpl at getJPMilesStatusReports() :", e);
		}
		return results;
	}

	@Override
	public List<Object[]> getJPMilesStatusReports(String fromDate, String toDate, String prtNo, String jpNumber,
			String status, String statusType) {
		Session session = null;
		Query query = null;
		List<Object[]> results =null;
		Date startDate = null;
		Date endDate = null;
		StringBuffer queryBuffer = null;
		boolean isDateYes = false;
		boolean ispartnerNameYes = false;
		SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd");
		boolean allValid=false;
		boolean allInvalid=false;
		
		try {
			session = sessionFactory.openSession();
			if("Tentative".equals(status))
					queryBuffer = new StringBuffer(AfHibernateQueries.GET_JPMILESTATUS_REPORT_FOR_TENTATIVE);
			else if("Processed".equals(status))
					queryBuffer = new StringBuffer(AfHibernateQueries.GET_JPMILESTATUS_REPORT_FOR_PROCESSED);
			else if("Failed".equals(status))
					queryBuffer = new StringBuffer(AfHibernateQueries.GET_JPMILESTATUS_REPORT_FOR_FAILED);
			else if ("All".equals(status)) {
				if ("valid".equals(statusType)) {
					allValid = true;
					queryBuffer = new StringBuffer(AfHibernateQueries.GET_JPMILESTATUS_REPORT_FOR_ALL_VALID);
				}
				if ("invalid".equals(statusType)) {
					allInvalid =true;
					queryBuffer = new StringBuffer(AfHibernateQueries.GET_JPMILESTATUS_REPORT_FOR_ALL_INVALID);
				}
			}
			if( (fromDate != null) && !("".equals(fromDate)) ) {
				isDateYes = true;
				startDate = ServiceProviderUtil.getInstance().getDateFromString(fromDate, "dd/MM/yyyy");
				endDate = ServiceProviderUtil.getInstance().getDateFromString(toDate, "dd/MM/yyyy");
				fromDate = dateFormater.format(startDate);
				toDate =dateFormater.format(endDate);
				queryBuffer.append("  date(pr1.FORMATTED_ORDER_DATE) between '" + fromDate + "' and '" + toDate + "'");
			}
			if( (prtNo != null) && !(prtNo.equals("")) ) {
				ispartnerNameYes = true;
				queryBuffer.append(isDateYes ? " and " : "");
				queryBuffer.append(" pr1.PRT_NO=" + Integer.parseInt(prtNo));
			}
			if( (jpNumber != null) && !(jpNumber.equals("")) ) {
				queryBuffer.append(isDateYes || ispartnerNameYes ? " and " : "");
				jpNumber = (jpNumber.length() <= 9) ? "00" + jpNumber : jpNumber;
				queryBuffer.append(" (tr.JP_NUMBER='" + jpNumber + "' or tr.JP_NUMBER='"+Long.valueOf(jpNumber) + "')");
			}
			if (allValid) {
				queryBuffer.append(" order by pr1.FORMATTED_ORDER_DATE");
				query = session.createSQLQuery(queryBuffer.toString());
			} else if(allInvalid) {
				queryBuffer.append(" order by pr1.FORMATTED_ORDER_DATE desc");
				query = session.createSQLQuery(queryBuffer.toString());
			}else{
				queryBuffer.append(" group by pr1.ORDER_ID order by pr1.FORMATTED_ORDER_DATE");
				query = session.createSQLQuery(queryBuffer.toString());
			}
			results = query.list();
			
		}
		catch(Exception e) {
			e.printStackTrace();
			logger.error("@@@@ Exception in ReportsDAOImpl at getJPMilesStatusReports() :", e);
		}finally {
			session.close();
		}
		
		return results;
	}

	@Override
	public List<AfTransactionDetails> getJpMilesByTransactionId(String transactionId, String partnerItemId) {
		List<AfTransactionDetails> afTransactionDetails=null;
		try{
			Query query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_TRANSACTION_DETAILS_STATUS_UPDATE);
			query.setParameter("transactionId", transactionId);
			query.setParameter("partnerItemId", partnerItemId);
			afTransactionDetails=query.list();
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in ReportsDAOImpl at getJpMilesByTransactionId() :", e);
		}
		return afTransactionDetails;
	}

	@Override
	public List<AfTransaction> getTransactionId(String transactionId) {
		List<AfTransaction> afTransaction=null;
		try{
			Query query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_TRANSACTION_SMS_STATUS);
			query.setParameter("transactionId", transactionId);
			afTransaction=query.list();
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in ReportsDAOImpl at getTransactionId() :", e);
		}
		return afTransaction;
	}

	@Override
	public List<Object[]> getShopAllJPReports(String fromDate, String toDate, String jpNumber, String status,
			String statusType) {
		
		Session session = null;
		Query query = null;
		List<Object[]> results =null;
		Date startDate = null;
		Date endDate = null;
		StringBuffer queryBuffer = null;
		boolean isDateYes = false;
		boolean ispartnerNameYes = false;
		SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd");
		boolean allValid=false;
		boolean allInvalid=false;
		
		try {
			System.out.println("isnide getShopAllJPReports dao");
			System.out.println("isnide getShopAllJPReports Status dao----------->"+status);
			System.out.println("isnide getShopAllJPReports statusType dao---------->"+statusType);
			session = sessionFactory.openSession();
			
			if("Tentative".equals(status))
				queryBuffer = new StringBuffer(AfHibernateQueries.GET_BUSINESS_REPORT_FOR_TENTATIVE);
			else if("Processed".equals(status))
			{
				System.out.println("proceed inside if");
				queryBuffer = new StringBuffer(AfHibernateQueries.GET_BUSINESS_REPORT_FOR_PROCESSED);
			}
			else if("Failed".equals(status))
				queryBuffer = new StringBuffer(AfHibernateQueries.GET_BUSINESS_REPORT_FOR_FAILED);			
			
			else if("JP Number Missing".equals(status))
                queryBuffer = new StringBuffer(AfHibernateQueries.GET_BUSINESS_REPORT_FOR_JP_NUMBER_MISSING);
			else if("Reference Number Missing".equals(status))
                queryBuffer = new StringBuffer(AfHibernateQueries.GET_BUSINESS_REPORT_FOR_REFERENCE_NUMBER_MISSING);
			else if("JP And Reference Number Missing".equals(status))
                queryBuffer = new StringBuffer(AfHibernateQueries.GET_BUSINESS_REPORT_FOR_JP_AND_REFERENCE_NUMBER_MISSING);
			else if("Others".equals(status))
                queryBuffer = new StringBuffer(AfHibernateQueries.GET_BUSINESS_REPORT_FOR_OTHERS);
			
			else if ("All".equals(status)) {
				if ("valid".equals(statusType)) {
					allValid = true;
					queryBuffer = new StringBuffer(AfHibernateQueries.GET_BUSINESS_REPORT_FOR_ALL_VALID);
				}
				if ("invalid".equals(statusType)) {
					allInvalid =true;
					queryBuffer = new StringBuffer(AfHibernateQueries.GET_BUSINESS_REPORT_FOR_ALL_INVALID);
				}
			}
			
			if( (fromDate != null) && !("".equals(fromDate)) ) {
				isDateYes = true;
				startDate = ServiceProviderUtil.getInstance().getDateFromString(fromDate, "dd/MM/yyyy");
				endDate = ServiceProviderUtil.getInstance().getDateFromString(toDate, "dd/MM/yyyy");
				fromDate = dateFormater.format(startDate);
				toDate =dateFormater.format(endDate);
				queryBuffer.append("  date(pr1.FORMATTED_ORDER_DATE) between '" + fromDate + "' and '" + toDate + "'");
			}
			
			if( (jpNumber != null) && !(jpNumber.equals("")) ) {
				queryBuffer.append(isDateYes || ispartnerNameYes ? " and " : "");
				jpNumber = (jpNumber.length() <= 9) ? "00" + jpNumber : jpNumber;
				queryBuffer.append(" (tr.JP_NUMBER='" + jpNumber + "' or tr.JP_NUMBER='"+Long.valueOf(jpNumber) + "')");
			}
			
			if (allValid) {
				queryBuffer.append(" order by pr1.FORMATTED_ORDER_DATE");
				query = session.createSQLQuery(queryBuffer.toString());
			} else if(allInvalid) {
				queryBuffer.append(" order by pr1.FORMATTED_ORDER_DATE desc LIMIT 0,7000");
				query = session.createSQLQuery(queryBuffer.toString());
			} 
				  else{
				  queryBuffer.append(" group by pr1.ORDER_ID order by pr1.FORMATTED_ORDER_DATE"); 
				  query = session.createSQLQuery(queryBuffer.toString()); }
				 
			System.out.println("query buffer -------------> "+queryBuffer.toString());
			results = query.list();
			System.out.println("resultsSize--->"+results.size());
//			System.out.println("results--->"+results);
			
			
		}
		catch(Exception e) {
//			 e.printStackTrace();
			logger.error("@@@@ Exception in ReportsDAOImpl at getShopJPReportInfo() :", e);
		}finally {
//			session.close();
		}
		
		return results;
		
	}

	
	public List<Object[]> getAllShopJPReportsZeroMiles(String fromDate, String toDate, String jpNumber,
			String status, String statusType) {
		Session session = null;
		Query query = null;
		List<Object[]> results =null;
		Date startDate = null;
		Date endDate = null;
		StringBuffer queryBuffer = null;
		boolean isDateYes = false;
		boolean ispartnerNameYes = false;
		SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd");
		boolean partnerFlag=true;
		
		try {
			System.out.println("inside get All Shop JP Reports Zero Miles");
			System.out.println("status---->"+statusType);
			session = sessionFactory.getCurrentSession();
			if("Tentative".equals(status))
					queryBuffer = new StringBuffer(AfHibernateQueries.GET_REPORT_FOR_TENTATIVE_WITH_ZERO_JP_MILES);
			else if("Processed".equals(status))
					queryBuffer = new StringBuffer(AfHibernateQueries.GET_REPORT_FOR_PROCESSED_WITH_ZERO_JP_MILES);
			else if("Failed".equals(status))
					queryBuffer = new StringBuffer(AfHibernateQueries.GET_REPORT_FOR_FAILED_WITH_ZERO_JP_MILES);
			
			else if("JP Number Missing".equals(status))
                queryBuffer = new StringBuffer(AfHibernateQueries.GET_REPORT_FOR_JP_NUMBER_MISSING_WITH_ZERO_JP_MILES);
			else if("Reference Number Missin".equals(status))
                queryBuffer = new StringBuffer(AfHibernateQueries.GET_REPORT_FOR_REFERENCE_NUMBER_MISSING_WITH_ZERO_JP_MILES);
			else if("JP And Reference Number Missing".equals(status))
                queryBuffer = new StringBuffer(AfHibernateQueries.GET_REPORT_FOR_JP_AND_REFERENCE_NUMBER_MISSING_WITH_ZERO_JP_MILES);

			else if("Others".equals(status))
                queryBuffer = new StringBuffer(AfHibernateQueries.GET_REPORT_REPORT_FOR_OTHERS_WITH_ZERO_JP_MILES);
			
			
			else if ("All".equals(status)) {
				if ("valid".equals(statusType)) {
					queryBuffer = new StringBuffer(AfHibernateQueries.GET_REPORT_FOR_ALL_VALID_WITH_ZERO_JP_MILES);
				}
				if ("invalid".equals(statusType)) {
					partnerFlag =false;
					queryBuffer = new StringBuffer(AfHibernateQueries.GET_REPORT_FOR_ALL_INVALID_WITH_ZERO_JP_MILES);
				}
			}
			if( (fromDate != null) && !("".equals(fromDate)) ) {
				isDateYes = true;
				startDate = ServiceProviderUtil.getInstance().getDateFromString(fromDate, "dd/MM/yyyy");
				endDate = ServiceProviderUtil.getInstance().getDateFromString(toDate, "dd/MM/yyyy");
				
				Calendar c = Calendar.getInstance(); 
				c.setTime(endDate); 
				c.add(Calendar.DATE, 1);
				endDate = c.getTime();
				
				fromDate = dateFormater.format(startDate);
				toDate =dateFormater.format(endDate);
				queryBuffer.append("  date(pr1.FORMATTED_ORDER_DATE) between '" + fromDate + "' and '" + toDate + "'");
				}
			
			if( (jpNumber != null) && !(jpNumber.equals("")) ) {
				queryBuffer.append(isDateYes || ispartnerNameYes ? " and " : "");
				jpNumber = (jpNumber.length() <= 9) ? "00" + jpNumber : jpNumber;
				queryBuffer.append(" (tr.JP_NUMBER='" + jpNumber + "' or tr.JP_NUMBER='"+Long.valueOf(jpNumber) + "')");
			}
			
			if (partnerFlag) {
				queryBuffer.append(" order by pr1.FORMATTED_ORDER_DATE");
				query = session.createSQLQuery(queryBuffer.toString());
			} else {
				queryBuffer.append(" GROUP by pr1.TRANSACTION_ID and pr1.ORDER_ID order by pr1.FORMATTED_ORDER_DATE desc");
				query = session.createSQLQuery(queryBuffer.toString());
			}
			System.out.println("---------------->query "+query);
			results = query.list();
			System.out.println("results--->"+results);

		}
		catch(Exception e) {
			e.printStackTrace();
			logger.error("@@@@ Exception in ReportsDAOImpl at getShopJPReportInfo() :", e);
		}
		return results;
	}

	
	
}