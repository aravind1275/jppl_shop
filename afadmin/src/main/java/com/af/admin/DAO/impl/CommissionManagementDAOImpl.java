package com.af.admin.DAO.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.af.admin.DAO.CommissionManagementDAO;
import com.af.admin.constants.AfHibernateQueries;
import com.af.admin.entity.AfPartnerCategory;
import com.af.admin.entity.AfPartnerCommission;
import com.af.admin.entity.AfPartnerProduct;
import com.af.admin.utils.ServiceProviderUtil;

@Repository
public class CommissionManagementDAOImpl implements CommissionManagementDAO {
	@Autowired
	SessionFactory sessionFactory;

	private Logger logger = LogManager.getLogger(CommissionManagementDAOImpl.class.getName());

	@Override
	public List<AfPartnerCommission> getPartnerCommissionList(String partNo) {
		List<AfPartnerCommission> partnerCommissionList = new ArrayList<>();
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(AfHibernateQueries.AF_PARTNERCOMMISSION_BY_PRT_NO);
		try {
			query.setInteger("partNo", Integer.parseInt(partNo));
			partnerCommissionList = query.list();

		} catch (Exception e) {
			logger.error("@@@@ Exception in CommissionManagementDAOImpl at getPartnerCommissionList() :", e);
		}
		return partnerCommissionList;

	}

	@Override
	public AfPartnerCategory getAfPartnerCategories(String prtCatNo) {
		Session session = sessionFactory.getCurrentSession();
		AfPartnerCategory afPartnercategory = null;
		try {
			afPartnercategory = (AfPartnerCategory) session.get(AfPartnerCategory.class, new Integer(prtCatNo));
		} catch (Exception e) {
			logger.error("@@@@ Exception in CommissionManagementDaoImpl at getAfPartnerCategories() :", e);
		}
		return afPartnercategory;
	}

	@Override
	public AfPartnerCommission getPartnerCommission(Integer prtCommNo) {
		Session session = sessionFactory.getCurrentSession();
		AfPartnerCommission afPartnerCommission = null;
		try {
			afPartnerCommission = (AfPartnerCommission) session.get(AfPartnerCommission.class, prtCommNo);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("@@@@ Exception in CommissionMAnagementDaoImpl at getPartnerCommission() :", e);
		}
		return afPartnerCommission;
	}

	@Override
	public String updateCommission(AfPartnerCommission afPartnerCommission) {
		try {
			sessionFactory.getCurrentSession().merge(afPartnerCommission);
		} catch (ConstraintViolationException e) {
			logger.error("@@@@ Exception in CommissionManagementDAOImpl at updateCommission() :", e);
			return "failure";
		} catch (Exception e) {e.printStackTrace();
			logger.error("@@@@ Exception in CommissionManagementDAOImpl at updateCommission :", e);
			return "failure";
		}
		return "success";
	}

	@Override
	public String addCommission(AfPartnerCommission afpartnercommission) {
		String status;
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(afpartnercommission);
			status = "success";
		} catch (ConstraintViolationException e) {
			logger.error("@@@@ Exception in CommissionmanagementDAOImpl at addCommission() :", e);
			status = "failure";
			return status;
		}
		return status;
	}

	@Override
	public AfPartnerProduct getAfPartnerProducts(String prtProductNo) {
		Session session = sessionFactory.getCurrentSession();
		AfPartnerProduct afPartnerproduct = null;
		try {
			afPartnerproduct = (AfPartnerProduct) session.get(AfPartnerProduct.class, new Integer(prtProductNo));
		} catch (Exception e) {
			logger.error("@@@@ Exception in CommissionManagementDaoImpl at getAfPartnerProducts() :", e);
		}
		return afPartnerproduct;
	}

	@Override
	public Long checkExistingDetailsStartDate(Integer partNo, String partCatNo, String partPrDctNo,
			String activationDate) {

		Date fromDate = null, toDate = null;
		Long count = 0L;
		Query query = sessionFactory.getCurrentSession()
				.createQuery(AfHibernateQueries.AF_COMMISION_EXISTENCE_STARTDATE);
		query.setInteger("partNo", partNo);
		query.setInteger("partCatNo", new Integer(partCatNo));
		query.setInteger("partPdctNo", new Integer(partPrDctNo));
		fromDate = ServiceProviderUtil.getInstance().getDateFromString(activationDate, "dd/MM/yyyy");

		Date dateOnedayAfter = new Date(fromDate.getTime() + 1 * 25 * 3600 * 1000);
		query.setParameter("fromDate", fromDate);

		try {
			count = (Long) query.uniqueResult();
		} catch (Exception e) {
			logger.error("@@@@ Exception in CommissionManagementDaoImpl at checkExistingDetailsStartDate() :", e);
		}
		return count;
	}

	@Override
	public Long checkExistingDetailsStartDate(Integer partNo, String partCatNo, String activationDate) {

		Date fromDate = null;
		Long count = 0L;
		Query query = sessionFactory.getCurrentSession()
				.createQuery(AfHibernateQueries.AF_COMMISION_EXISTENCE_STARTDATE_WITHOUT_PRODUCT);
		query.setInteger("partNo", partNo);
		query.setInteger("partCatNo", new Integer(partCatNo));

		fromDate = ServiceProviderUtil.getInstance().getDateFromString(activationDate, "dd/MM/yyyy");

		Date dateOnedayAfter = new Date(fromDate.getTime() + 1 * 25 * 3600 * 1000);
		query.setParameter("fromDate", fromDate);

		try {
			count = (Long) query.uniqueResult();
		} catch (Exception e) {
			logger.error("@@@@ Exception in CommissionManagementDaoImpl at checkExistingDetailsStartDate() :", e);
		}
		return count;
	}

	@Override
	public Long checkExistingDetailsStartDate(String partNo, String partCatNo, String partPrdNo, String activationDate,
			String brandName, String catPath, Float lowerValue, Float upperValue) {
		Date fromDate = null;
		Long count = 0L;
		fromDate = ServiceProviderUtil.getInstance().getDateFromString(activationDate, "dd/MM/yyyy");
		Query query = null;
		StringBuffer queryBuffer = null;
		try {

			if (partNo != null && partCatNo == null && partPrdNo == null && brandName == null && catPath == null) {
				queryBuffer = new StringBuffer(AfHibernateQueries.AF_COMMISION_EXISTENCE_STARTDATE_WITH_PRT_NO);
				queryBuffer.append(AfHibernateQueries.GET_TRANSACTION_RANGE_FILTER);
				query = sessionFactory.getCurrentSession().createQuery(queryBuffer.toString());
			} else if (partNo != null && partCatNo != null && partPrdNo == null && brandName == null
					&& catPath == null) {
				queryBuffer = new StringBuffer(AfHibernateQueries.AF_COMMISION_EXISTENCE_STARTDATE_WITH_PRT_CAT_NO);
				queryBuffer.append(AfHibernateQueries.GET_TRANSACTION_RANGE_FILTER);
				query = sessionFactory.getCurrentSession().createQuery(queryBuffer.toString());
				query.setParameter("partCatNo", (partCatNo == null ? null : Integer.parseInt(partCatNo)));
			} else if (partNo != null && partCatNo != null && partPrdNo != null && brandName == null
					&& catPath == null) {
				queryBuffer = new StringBuffer(AfHibernateQueries.AF_COMMISION_EXISTENCE_STARTDATE_WITH_PRODUCT);
				queryBuffer.append(AfHibernateQueries.GET_TRANSACTION_RANGE_FILTER);
				query = sessionFactory.getCurrentSession().createQuery(queryBuffer.toString());
				query.setParameter("partCatNo", (partCatNo == null ? null : Integer.parseInt(partCatNo)));
				query.setParameter("partPrdNo", (partPrdNo == null ? null : Integer.parseInt(partPrdNo)));
			} else if (partNo != null && partCatNo != null && partPrdNo == null && brandName != null
					&& catPath == null) {
				queryBuffer = new StringBuffer(AfHibernateQueries.AF_COMMISION_EXISTENCE_STARTDATE_WITH_BRAND);
				queryBuffer.append(AfHibernateQueries.GET_TRANSACTION_RANGE_FILTER);
				query = sessionFactory.getCurrentSession().createQuery(queryBuffer.toString());
				query.setParameter("partCatNo", (partCatNo == null ? null : Integer.parseInt(partCatNo)));
				query.setParameter("brandName", brandName);
			} else if (partNo != null && partCatNo != null && partPrdNo == null && brandName != null
					&& catPath != null) {
				queryBuffer = new StringBuffer(AfHibernateQueries.AF_COMMISION_EXISTENCE_STARTDATE_WITH_BRAND_CAT_PATH);
				queryBuffer.append(AfHibernateQueries.GET_TRANSACTION_RANGE_FILTER);
				query = sessionFactory.getCurrentSession().createQuery(queryBuffer.toString());
				query.setParameter("partCatNo", (partCatNo == null ? null : Integer.parseInt(partCatNo)));
				query.setParameter("brandName", brandName);
				query.setParameter("catPath", catPath);
			} else if (partNo != null && partCatNo != null && partPrdNo == null && brandName == null
					&& catPath != null) {
				queryBuffer = new StringBuffer(
						AfHibernateQueries.AF_COMMISION_EXISTENCE_STARTDATE_WITH_PRT_CAT_AND_CAT_PATH);
				queryBuffer.append(AfHibernateQueries.GET_TRANSACTION_RANGE_FILTER);
				query = sessionFactory.getCurrentSession().createQuery(queryBuffer.toString());
				query.setParameter("partCatNo", (partCatNo == null ? null : Integer.parseInt(partCatNo)));
				query.setParameter("catPath", catPath);
			} else if (partNo != null && partCatNo == null && partPrdNo == null && brandName == null
					&& catPath != null) {
				queryBuffer = new StringBuffer(
						AfHibernateQueries.AF_COMMISION_EXISTENCE_STARTDATE_WITH_PRT_AND_CAT_PATH);
				queryBuffer.append(AfHibernateQueries.GET_TRANSACTION_RANGE_FILTER);
				query = sessionFactory.getCurrentSession().createQuery(queryBuffer.toString());
				query.setParameter("catPath", catPath);
			}

			
			query.setParameter("partNo", (partNo == null ? null : Integer.parseInt(partNo)));
			query.setParameter("fromDate", fromDate);
			query.setParameter("lowerValue", lowerValue);
			query.setParameter("upperValue", upperValue);
			count = (Long) query.uniqueResult();

		} catch (Exception e) {
			logger.error("@@@@ Exception in CommissionManagementDaoImpl at checkExistingDetailsStartDate() :", e);
		}

		return count;
	}

	@Override
	public Long checkExistingDetailsEditStartDate(String activationDateHidden, String partComNo, String partNo,
			String partCatNo, String partPrdNo, String activationDate, String brandName, String catPath) {
		Date fromDate = null, startDateHidden = null;
		Date dt = null;
		Long count = 0L;

		fromDate = ServiceProviderUtil.getInstance().getDateFromString(activationDate, "dd/MM/yyyy");
		startDateHidden = ServiceProviderUtil.getInstance().getDateFromString(activationDateHidden, "dd/MM/yyyy");

		Query query = null;
		try {
			if (partNo != null && partCatNo == null && partPrdNo == null && brandName == null && catPath == null) {
				query = sessionFactory.getCurrentSession()
						.createQuery(AfHibernateQueries.AF_COMMISION_EXISTENCE_EDIT_STARTDATE_WITH_PRT_NO);
			} else if (partNo != null && partCatNo != null && partPrdNo == null && brandName == null
					&& catPath == null) {
				query = sessionFactory.getCurrentSession()
						.createQuery(AfHibernateQueries.AF_COMMISION_EXISTENCE_EDIT_STARTDATE_WITH_PRT_CAT_NO);
				query.setParameter("partCatNo", (partCatNo == null ? null : Integer.parseInt(partCatNo)));
			} else if (partNo != null && partCatNo != null && partPrdNo != null && brandName == null
					&& catPath == null) {
				query = sessionFactory.getCurrentSession()
						.createQuery(AfHibernateQueries.AF_COMMISION_EXISTENCE_EDIT_STARTDATE_WITH_PRODUCT);
				query.setParameter("partCatNo", (partCatNo == null ? null : Integer.parseInt(partCatNo)));
				query.setParameter("partPrdNo", (partPrdNo == null ? null : Integer.parseInt(partPrdNo)));
			} else if (partNo != null && partCatNo != null && partPrdNo == null && brandName != null
					&& catPath == null) {
				query = sessionFactory.getCurrentSession()
						.createQuery(AfHibernateQueries.AF_COMMISION_EXISTENCE_EDIT_STARTDATE_WITH_BRAND);
				query.setParameter("partCatNo", (partCatNo == null ? null : Integer.parseInt(partCatNo)));
				query.setParameter("brandName", brandName);
			} else if (partNo != null && partCatNo != null && partPrdNo == null && brandName != null
					&& catPath != null) {
				query = sessionFactory.getCurrentSession()
						.createQuery(AfHibernateQueries.AF_COMMISION_EXISTENCE_EDIT_STARTDATE_WITH_BRAND_AND_CAT_PATH);
				query.setParameter("partCatNo", (partCatNo == null ? null : Integer.parseInt(partCatNo)));
				query.setParameter("brandName", brandName);
				query.setParameter("catPath", catPath);
			} else if (partNo != null && partCatNo != null && partPrdNo == null && brandName == null
					&& catPath != null) {
				query = sessionFactory.getCurrentSession().createQuery(
						AfHibernateQueries.AF_COMMISION_EXISTENCE_EDIT_STARTDATE_WITH_PRT_CAT_AND_CAT_PATH);
				query.setParameter("partCatNo", (partCatNo == null ? null : Integer.parseInt(partCatNo)));
				query.setParameter("catPath", catPath);
			} else if (partNo != null && partCatNo == null && partPrdNo == null && brandName == null
					&& catPath != null) {
				query = sessionFactory.getCurrentSession()
						.createQuery(AfHibernateQueries.AF_COMMISION_EXISTENCE_EDIT_STARTDATE_WITH_PRT_AND_CAT_PATH);
				query.setParameter("catPath", catPath);
			}

						query.setParameter("partNo", (partNo == null ? null : Integer.parseInt(partNo)));
			query.setParameter("partComNo", (partComNo == null ? null : Integer.parseInt(partComNo)));
			query.setParameter("startDateHidden", startDateHidden);

			dt = (Date) query.uniqueResult();
			String dateFormat = dt.getDate() + "/" + (dt.getMonth() + 1) + "/" + (dt.getYear() + 1900);

			dt = ServiceProviderUtil.getInstance().getDateFromString(dateFormat, "dd/MM/yyyy");

			int x = dt.compareTo(fromDate);

			if (x >= 0) {
				count = 1L;
			}
		} catch (Exception e) {
			logger.error("@@@@ Exception in CommissionManagementDaoImpl at checkExistingDetailsEditStartDate() :", e);
		}

		return count;
	}

	@Override
	public Long checkExistingDetailsEndDate(String partNo, String partCatNo, String partPrdNo, String prtBrand,
			String activationDate, String deactivationDate, String catPath, Float lowerValue, Float upperValue) {
		Date toDate = null;
		Long count = 0L;

		toDate = ServiceProviderUtil.getInstance().getDateFromString(deactivationDate, "dd/MM/yyyy");
		Query query = null;
		StringBuffer queryBuffer = null;

		try {
			if (partNo != null && partCatNo == null && partPrdNo == null && prtBrand == null && catPath == null) {
				queryBuffer = new StringBuffer(AfHibernateQueries.AF_COMMISION_EXISTENCE_ENDDATE_WITH_PRT_NO);
				queryBuffer.append(AfHibernateQueries.GET_TRANSACTION_RANGE_FILTER);
				query = sessionFactory.getCurrentSession().createQuery(queryBuffer.toString());
			} else if (partNo != null && partCatNo != null && partPrdNo == null && prtBrand == null
					&& catPath == null) {
				queryBuffer = new StringBuffer(AfHibernateQueries.AF_COMMISION_EXISTENCE_ENDDATE_WITH_PRT_CAT_NO);
				queryBuffer.append(AfHibernateQueries.GET_TRANSACTION_RANGE_FILTER);
				query = sessionFactory.getCurrentSession().createQuery(queryBuffer.toString());
				query.setParameter("partCatNo", (partCatNo == null ? null : Integer.parseInt(partCatNo)));
			} else if (partNo != null && partCatNo != null && partPrdNo != null && prtBrand == null
					&& catPath == null) {
				queryBuffer = new StringBuffer(AfHibernateQueries.AF_COMMISION_EXISTENCE_ENDDATE_WITH_PRODUCT);
				queryBuffer.append(AfHibernateQueries.GET_TRANSACTION_RANGE_FILTER);
				query = sessionFactory.getCurrentSession().createQuery(queryBuffer.toString());
				query.setParameter("partCatNo", (partCatNo == null ? null : Integer.parseInt(partCatNo)));
				query.setParameter("partPrdNo", (partPrdNo == null ? null : Integer.parseInt(partPrdNo)));
			} else if (partNo != null && partCatNo != null && partPrdNo == null && prtBrand != null
					&& catPath == null) {
				queryBuffer = new StringBuffer(AfHibernateQueries.AF_COMMISION_EXISTENCE_ENDDATE_WITH_BRAND);
				queryBuffer.append(AfHibernateQueries.GET_TRANSACTION_RANGE_FILTER);
				query = sessionFactory.getCurrentSession().createQuery(queryBuffer.toString());
				query.setParameter("partCatNo", (partCatNo == null ? null : Integer.parseInt(partCatNo)));
				query.setParameter("brandName", prtBrand);
			} else if (partNo != null && partCatNo != null && partPrdNo == null && prtBrand != null
					&& catPath != null) {
				queryBuffer = new StringBuffer(
						AfHibernateQueries.AF_COMMISION_EXISTENCE_ENDDATE_WITH_BRAND_AND_CAT_PATH);
				queryBuffer.append(AfHibernateQueries.GET_TRANSACTION_RANGE_FILTER);
				query = sessionFactory.getCurrentSession().createQuery(queryBuffer.toString());
				query.setParameter("partCatNo", (partCatNo == null ? null : Integer.parseInt(partCatNo)));
				query.setParameter("brandName", prtBrand);
				query.setParameter("catPath", catPath);
			} else if (partNo != null && partCatNo != null && partPrdNo == null && prtBrand == null
					&& catPath != null) {
				queryBuffer = new StringBuffer(
						AfHibernateQueries.AF_COMMISION_EXISTENCE_ENDDATE_WITH_PRT_CAT_AND_CAT_PATH);
				queryBuffer.append(AfHibernateQueries.GET_TRANSACTION_RANGE_FILTER);
				query = sessionFactory.getCurrentSession().createQuery(queryBuffer.toString());
				query.setParameter("partCatNo", (partCatNo == null ? null : Integer.parseInt(partCatNo)));
				query.setParameter("catPath", catPath);
			} else if (partNo != null && partCatNo == null && partPrdNo == null && prtBrand == null
					&& catPath != null) {
				queryBuffer = new StringBuffer(AfHibernateQueries.AF_COMMISION_EXISTENCE_ENDDATE_WITH_PRT_AND_CAT_PATH);
				queryBuffer.append(AfHibernateQueries.GET_TRANSACTION_RANGE_FILTER);
				query = sessionFactory.getCurrentSession().createQuery(queryBuffer.toString());
				query.setParameter("catPath", catPath);
			}

			
			query.setParameter("partNo", (partNo == null ? null : Integer.parseInt(partNo)));
			query.setParameter("newtodate", toDate);
			query.setFloat("lowerValue", lowerValue);
			query.setFloat("upperValue", upperValue);
			count = (Long) query.uniqueResult();
		} catch (Exception e) {
			logger.error("@@@@ Exception in CommissionManagementDaoImpl at checkExistingDetailsEndDate() :", e);
		}

		return count;
	}

	@Override
	public Long checkExistingDetailsEditEndDate(String partComNo, String partNo, String partCatNo, String partPrdNo,
			String prtBrand, String activationDate, String deactivationDate, String deactivationDateHidden,
			String catPath, Float lowerValue, Float upperValue) {
		Date startDate = null, endDate = null, endDateHidden = null;
		Date dt = null;
		Long count = 0L;

		startDate = ServiceProviderUtil.getInstance().getDateFromString(activationDate, "dd/MM/yyyy");
		endDate = ServiceProviderUtil.getInstance().getDateFromString(deactivationDate, "dd/MM/yyyy");
		endDateHidden = ServiceProviderUtil.getInstance().getDateFromString(deactivationDateHidden, "dd/MM/yyyy");

		Query query = null;

		try {
			if (partNo != null && partCatNo == null && partPrdNo == null && prtBrand == null && catPath == null) {
				query = sessionFactory.getCurrentSession()
						.createQuery(AfHibernateQueries.AF_COMMISION_EXISTENCE_EDIT_ENDDATE_WITH_PRT_NO);
			} else if (partNo != null && partCatNo != null && partPrdNo == null && prtBrand == null
					&& catPath == null) {
				query = sessionFactory.getCurrentSession()
						.createQuery(AfHibernateQueries.AF_COMMISION_EXISTENCE_EDIT_ENDDATE_WITH_PRT_CAT_NO);
				query.setParameter("partCatNo", (partCatNo == null ? null : Integer.parseInt(partCatNo)));
			} else if (partNo != null && partCatNo != null && partPrdNo != null && prtBrand == null
					&& catPath == null) {
				query = sessionFactory.getCurrentSession()
						.createQuery(AfHibernateQueries.AF_COMMISION_EXISTENCE_EDIT_ENDDATE_WITH_PRODUCT);
				query.setParameter("partCatNo", (partCatNo == null ? null : Integer.parseInt(partCatNo)));
				query.setParameter("partPrdNo", (partPrdNo == null ? null : Integer.parseInt(partPrdNo)));
			} else if (partNo != null && partCatNo != null && partPrdNo == null && prtBrand != null
					&& catPath == null) {
				query = sessionFactory.getCurrentSession()
						.createQuery(AfHibernateQueries.AF_COMMISION_EXISTENCE_EDIT_ENDDATE_WITH_BRAND);
				query.setParameter("partCatNo", (partCatNo == null ? null : Integer.parseInt(partCatNo)));
				query.setParameter("brandName", prtBrand);
			} else if (partNo != null && partCatNo != null && partPrdNo == null && prtBrand != null
					&& catPath != null) {
				query = sessionFactory.getCurrentSession()
						.createQuery(AfHibernateQueries.AF_COMMISION_EXISTENCE_EDIT_ENDDATE_WITH_BRAND_AND_CATPATH);
				query.setParameter("partCatNo", (partCatNo == null ? null : Integer.parseInt(partCatNo)));
				query.setParameter("brandName", prtBrand);
				query.setParameter("catPath", catPath);
			} else if (partNo != null && partCatNo != null && partPrdNo == null && prtBrand == null
					&& catPath != null) {
				query = sessionFactory.getCurrentSession()
						.createQuery(AfHibernateQueries.AF_COMMISION_EXISTENCE_EDIT_ENDDATE_WITH_PRT_CAT_AND_CATPATH);
				query.setParameter("partCatNo", (partCatNo == null ? null : Integer.parseInt(partCatNo)));
				query.setParameter("catPath", catPath);

			} else if (partNo != null && partCatNo == null && partPrdNo == null && prtBrand == null
					&& catPath != null) {
				query = sessionFactory.getCurrentSession()
						.createQuery(AfHibernateQueries.AF_COMMISION_EXISTENCE_EDIT_ENDDATE_WITH_PRT_AND_CATPATH);
				query.setParameter("catPath", catPath);

			}

			

			query.setParameter("partComNo", (partComNo == null ? null : Integer.parseInt(partComNo)));
			query.setParameter("partNo", (partNo == null ? null : Integer.parseInt(partNo)));
			query.setParameter("endDateHidden", endDateHidden);
			query.setFloat("lowerValue", lowerValue);
			query.setFloat("upperValue", upperValue);

			dt = (Date) query.uniqueResult();
			String dateFormat = dt.getDate() + "/" + (dt.getMonth() + 1) + "/" + (dt.getYear() + 1900);

			dt = ServiceProviderUtil.getInstance().getDateFromString(dateFormat, "dd/MM/yyyy");

			int x = dt.compareTo(endDate);
			if (x <= 0) {
				count = 1L;
			}
		} catch (Exception e) {
			logger.error("@@@@ Exception in CommissionManagementDaoImpl at checkExistingDetailsEditEndDate() :", e);
		}

		return count;
	}

	@Override
	public Long checkExistingDetailsEndDate(Integer partNo, String partCatNo, String partPrDctNo, String commissionrate,
			String memberShare, String activationDate, String deactivationDate) {
		Long count = 0L;
		Date toDate = null;
		Query query = sessionFactory.getCurrentSession()
				.createQuery(AfHibernateQueries.AF_COMMISION_EXISTENCE_STARTDATE_ENDDATE);
		query.setInteger("partNo", partNo);
		query.setInteger("partCatNo", new Integer(partCatNo));
		query.setInteger("partPdctNo", new Integer(partPrDctNo));
		toDate = ServiceProviderUtil.getInstance().getDateFromString(deactivationDate, "dd/MM/yyyy");
		Date newtodate = new Date(toDate.getTime() + 1 * 25 * 3600 * 1000);
		query.setParameter("newtodate", toDate);
		try {
			count = (Long) query.uniqueResult();
		} catch (Exception e) {
			logger.error("@@@@ Exception in CommissionManagementDaoImpl at checkExistingDetailsEndDate() :", e);
		}
		return count;
	}

	@Override
	public Long checkExistingDetailsEndDate(Integer partNo, String partCatNo, String commissionrate, String memberShare,
			String activationDate, String deactivationDate) {
		Date toDate = null;
		Long count = 0L;
		Query query = sessionFactory.getCurrentSession()
				.createQuery(AfHibernateQueries.AF_COMMISION_EXISTENCE_STARTDATE_ENDDATE_WITHOUT_PRODUCT);
		query.setInteger("partNo", partNo);
		query.setInteger("partCatNo", new Integer(partCatNo));
		toDate = ServiceProviderUtil.getInstance().getDateFromString(deactivationDate, "dd/MM/yyyy");
		Date newtodate = new Date(toDate.getTime() + 1 * 24 * 3600 * 1000);
		query.setParameter("newtodate", toDate);

		try {
			count = (Long) query.uniqueResult();
		} catch (Exception e) {
			logger.error("@@@@ Exception in CommissionManagementDaoImpl at checkExistingDetailsEndDate() :", e);
		}
		return count;
	}

	@Override
	public Integer checkExistingDetails(Integer partNo, String partCatNo, String partPrDctNo) {
		Integer partCommNumber = null;
		Query query = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_COMMISION_EXISTENCE);
		query.setInteger("partNo", partNo);
		query.setInteger("partCatNo", new Integer(partCatNo));
		query.setInteger("partPdctNo", new Integer(partPrDctNo));

		try {
			partCommNumber = (Integer) query.uniqueResult();
			logger.debug("@@@@ Exception in CommissionMAnagementDaoImpl partCommNumber() :" + partCommNumber);

		} catch (Exception e) {
			logger.error("@@@@ Exception in CommissionManagementDaoImpl at checkExistingDetails() :", e);
		}
		return partCommNumber;
	}

	@Override
	public Integer checkExistingDetails(Integer partNo, String partCatNo) {
		Integer partCommNumber = null;
		Query query = sessionFactory.getCurrentSession()
				.createQuery(AfHibernateQueries.AF_COMMISION_EXISTENCE_WITHOUT_PRODUCT);
		query.setInteger("partNo", partNo);
		query.setInteger("partCatNo", new Integer(partCatNo));

		try {
			partCommNumber = (Integer) query.uniqueResult();

		} catch (Exception e) {
			logger.error("@@@@ Exception in CommissionManagementDaoImpl at checkExistingDetails() :", e);
		}
		return partCommNumber;
	}

	@Override
	public List<AfPartnerCommission> getPartnerCommissions(Map pcmap) {
		List<AfPartnerCommission> afPartnerCommissions = new ArrayList<AfPartnerCommission>();
		Session session = sessionFactory.getCurrentSession();
		try {
			Query query = session.createQuery(AfHibernateQueries.AF_PARTNER_PRODUCT_CHECK);
			query.setParameter("prtNo", Integer.parseInt(pcmap.get("prtNo").toString()));
			query.setParameter("prtPrdNo", Integer.parseInt(pcmap.get("prtPrdNo").toString()));
			query.setParameter("prtCatNo", Integer.parseInt(pcmap.get("prtCatNo").toString()));
			afPartnerCommissions = query.list();
		} catch (Exception e) {
			logger.error("@@@@ Exception in CommissionManagementDaoImpl at getPartnerCommissions() :", e);
		}
		return afPartnerCommissions;
	}

	@Override
	public List<Integer> checkExistingCommDetails(Integer partNo, String partCatNo, String partPrdNo, String prtBrand,
			String catpath, Float lowerValue, Float upperValue) {
		List<Integer> partCommNumber = new ArrayList<>();
		;
		Query query = null;
		StringBuffer queryBuffer = null;
		try {
			if (partNo != null && partCatNo == null && partPrdNo == null && prtBrand == null && catpath == null) {
				queryBuffer = new StringBuffer(AfHibernateQueries.AF_COMMISION_EXISTENCE_WITH_PRT_NO);
				queryBuffer.append(AfHibernateQueries.GET_TRANSACTION_RANGE_FILTER);
				query = sessionFactory.getCurrentSession().createQuery(queryBuffer.toString());

			}
			if (partNo != null && partCatNo != null && partPrdNo == null && prtBrand == null && catpath == null) {
				queryBuffer = new StringBuffer(AfHibernateQueries.AF_COMMISION_EXISTENCE_WITH_PRT_NO_AND_PRT_CAT_NO);
				queryBuffer.append(AfHibernateQueries.GET_TRANSACTION_RANGE_FILTER);
				query = sessionFactory.getCurrentSession().createQuery(queryBuffer.toString());
				query.setInteger("prtCatNo", new Integer(partCatNo));

			} else if (partNo != null && partCatNo != null && partPrdNo == null && prtBrand == null
					&& catpath != null) {
				queryBuffer = new StringBuffer(
						AfHibernateQueries.AF_COMMISION_EXISTENCE_WITH_PRT_NO_AND_PRT_CAT_NO_AND_CAT_PATH);
				queryBuffer.append(AfHibernateQueries.GET_TRANSACTION_RANGE_FILTER);
				query = sessionFactory.getCurrentSession().createQuery(queryBuffer.toString());
				query.setParameter("partCatNo", (partCatNo == null ? null : Integer.parseInt(partCatNo)));
				query.setParameter("catPath", catpath);

			} else if (partNo != null && partCatNo != null && partPrdNo != null && prtBrand == null
					&& catpath == null) {
				queryBuffer = new StringBuffer(
						AfHibernateQueries.AF_COMMISION_EXISTENCE_WITH_PRT_NO_AND_PRT_CAT_NO_AND_PRT_PRD_NO);
				queryBuffer.append(AfHibernateQueries.GET_TRANSACTION_RANGE_FILTER);
				query = sessionFactory.getCurrentSession().createQuery(queryBuffer.toString());
				query.setInteger("prtCatNo", new Integer(partCatNo));
				query.setInteger("prtPrdNo", new Integer(partPrdNo));

			} else if (partNo != null && partCatNo != null && prtBrand != null && partPrdNo == null
					&& catpath == null) {
				queryBuffer = new StringBuffer(
						AfHibernateQueries.AF_COMMISION_EXISTENCE_WITH_PRT_NO_AND_PRT_CAT_NO_AND_BRAND_NAME);
				queryBuffer.append(AfHibernateQueries.GET_TRANSACTION_RANGE_FILTER);
				query = sessionFactory.getCurrentSession().createQuery(queryBuffer.toString());
				query.setInteger("prtCatNo", new Integer(partCatNo));
				query.setParameter("productBrand", prtBrand.trim());

			} else if (partNo != null && partCatNo != null && prtBrand == null && partPrdNo == null
					&& catpath != null) {
				queryBuffer = new StringBuffer(
						AfHibernateQueries.AF_COMMISION_EXISTENCE_WITH_PRT_NO_AND_PRT_CAT_NO_AND_CATPATH);
				queryBuffer.append(AfHibernateQueries.GET_TRANSACTION_RANGE_FILTER);
				query = sessionFactory.getCurrentSession().createQuery(queryBuffer.toString());
				query.setInteger("prtCatNo", new Integer(partCatNo));
				query.setParameter("catPath", catpath);

			} else if (partNo != null && partCatNo != null && prtBrand != null && partPrdNo == null
					&& catpath != null) {
				queryBuffer = new StringBuffer(
						AfHibernateQueries.AF_COMMISION_EXISTENCE_WITH_PRT_NO_AND_PRT_CAT_NO_AND_BRAND_NAME_AND_CAT_PATH);
				queryBuffer.append(AfHibernateQueries.GET_TRANSACTION_RANGE_FILTER);
				query = sessionFactory.getCurrentSession().createQuery(queryBuffer.toString());
				query.setInteger("prtCatNo", new Integer(partCatNo));
				query.setParameter("productBrand", prtBrand.trim());
				query.setParameter("catPath", catpath);
			}
			query.setInteger("partNo", partNo);
			query.setParameter("upperValue", upperValue);
			query.setParameter("lowerValue", lowerValue);
			partCommNumber = query.list();
		} catch (Exception e) {
			logger.error("@@@@ Exception in CommissionManagementDaoImpl checkExistingCommDetails() :", e);
		}
		return partCommNumber;
	}

	@Override
	public List<AfPartnerCommission> getPartnerCommissionList() {

		List<AfPartnerCommission> partnerCommissionList = new ArrayList<AfPartnerCommission>();
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(AfHibernateQueries.AF_PARTNERCOMMISSION);
		try {
			partnerCommissionList = query.list();

		} catch (Exception e) {
			logger.error("@@@@ Exception in CommissionManagementDAOImpl getPartnerCommissionList() :", e);
		}
		return partnerCommissionList;
	}

	@Override
	public List<AfPartnerCommission> getPartnerCommissionList(String fromDate, String toDate, String partNo) {
		Query query = null;
		Session session = null;
		Date startDate = null;
		Date endDate = null;
		List<AfPartnerCommission> result = null;
		StringBuffer queryBuffer = null;
		SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd");
		try {
			session = sessionFactory.getCurrentSession();
			queryBuffer = new StringBuffer(AfHibernateQueries.AF_PARTNERCOMMISSION);

			if (!fromDate.trim().isEmpty() && !toDate.trim().isEmpty()) {
				startDate = ServiceProviderUtil.getInstance().getDateFromString(fromDate, "dd/MM/yyyy");
				endDate = ServiceProviderUtil.getInstance().getDateFromString(toDate, "dd/MM/yyyy");
				fromDate = dateFormater.format(startDate);
				toDate = dateFormater.format(endDate);
				queryBuffer.append(" and (date(apc.activationDate) >='" + fromDate
						+ "' or date(apc.deactivationDate) <='" + toDate + "')");
			}
			if (!partNo.trim().isEmpty()) {
				queryBuffer.append(" and apc.afPartner.prtNo=" + partNo);
			}
			queryBuffer.append(" order by apc.prtCommNo desc");

			query = session.createQuery(queryBuffer.toString());
			result = query.list();
		} catch (Exception e) {
			logger.error("@@@@ Exception in CommissionManagementDAOImpl at getPartnerCommissionList() :", e);
		}
		return result;
	}

	@Override
	public String saveJPMileConfig(String jpMile, String typeName) {
		String status = "failure";
		int updatedEntityCount = 0;
		Session session = sessionFactory.getCurrentSession();

		try {
			Query query = session.createQuery(AfHibernateQueries.AF_JPMILE_SAVE_CONFIG);
			query.setParameter("jpMile", jpMile);
			query.setParameter("typeName", typeName);
			updatedEntityCount = query.executeUpdate();
		} catch (Exception e) {
			updatedEntityCount = -1;
			logger.error("@@@@ Exception in commissionManagementDAOImpl at saveJPMileConfig() :", e);
		}

		if (updatedEntityCount >= 0) {
			status = "success";
		}

		return status;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AfPartnerCommission> getPartnerCommissionWebservice() {
		Query query = null;
		List<AfPartnerCommission> afPrtCmm = null;
		try {
			query = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_PRT_COMMISSION_WS);
			afPrtCmm = query.list();
		} catch (Exception e) {
			logger.error("@@@@ Exception in commissionManagementDAOImpl at getPartnerCommissionWebservice() :", e);
		}

		return afPrtCmm;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AfPartnerCommission> getPartnerCommissionListByName(String partnerName) {
		Query query = null;
		List<AfPartnerCommission> afPrtCmm = null;
		try {
			query = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_PRT_COMMISSION_BY_NAME);
			query.setParameter("partnerName", partnerName);
			afPrtCmm = query.list();
		} catch (Exception e) {
			logger.error("@@@@ Exception in commissionManagementDAOImpl at getPartnerCommissionListByName() :", e);
		}
		return afPrtCmm;
	}

	@Override
	public Integer getpartnerNo(String partnerName) {
		Query query = null;
		Integer result = null;
		try {
			query = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_PRT_NO);
			query.setParameter("partnerName", partnerName);
			result = (Integer) query.uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("@@@@ Exception in commissionManagementDAOImpl at getPartnerCommissionListByName() :", e);
		}
		return result;
	}

	@Override
	public Integer getAfPartnerCategoryName(String prtCategoryName, Integer prtNo) {
		Query query = null;
		Integer result = null;
		try {
			query = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_PRT_CATEGORY_NO);
			query.setParameter("prtCategoryName", prtCategoryName);
			query.setParameter("prtNo", prtNo);
			query.setMaxResults(1);
			result = (Integer) query.uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("@@@@ Exception in commissionManagementDAOImpl at getPartnerCommissionListByName() :", e);
		}
		return result;
	}

	@Override
	public Integer getAfPartnerProductNo(String prtProductName, Integer prtNo, Integer prtCat) {
		Query query = null;
		Integer result = null;
		try {
			query = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_PRT_PRODUCT_NO);
			query.setParameter("prtProductName", prtProductName);
			query.setParameter("prtNo", prtNo);
			query.setParameter("prtCat", prtCat);
			query.setMaxResults(1);
			result = (Integer) query.uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("@@@@ Exception in commissionManagementDAOImpl at getPartnerCommissionListByName() :", e);
		}
		return result;
	}

	@Override
	public List<AfPartnerCommission> getPartnerCommissionListUpload(String fromDate, String toDate, String partNo,
			Integer prtCat, Integer partnerProductNo, String path, Float upVal, Float lowVal) {
		Query query = null;
		Session session = null;
		Date startDate = null;
		Date endDate = null;
		List<AfPartnerCommission> result = null;
		StringBuffer queryBuffer = null;
		SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd");
		try {
			session = sessionFactory.getCurrentSession();
			queryBuffer = new StringBuffer(AfHibernateQueries.AF_PARTNERCOMMISSIONUPLOAD);

			if (!fromDate.trim().isEmpty() && !toDate.trim().isEmpty()) {
				startDate = ServiceProviderUtil.getInstance().getDateFromString(fromDate, "dd/MM/yyyy");
				endDate = ServiceProviderUtil.getInstance().getDateFromString(toDate, "dd/MM/yyyy");
				fromDate = dateFormater.format(startDate);
				toDate = dateFormater.format(endDate);
				queryBuffer.append(" and (date(afpc.activationDate) >='" + fromDate
						+ "' and date(afpc.deactivationDate) <='" + toDate + "')");
			}
			if (partnerProductNo != null) {
				queryBuffer.append(" and afpc.afPartnerProduct.prtPrdNo=" + partnerProductNo);
			}
			if (path!=null){
				queryBuffer.append(" and afpc.afPartnerProduct.categoryPath=" +"'"+ path+"'");
			}
			if (prtCat != null) {
				queryBuffer.append(" and afpc.afPartnerCategory.prtCatNo=" + prtCat);
			}
			if (!partNo.trim().isEmpty()) {
				queryBuffer.append(" and afpc.afPartner.prtNo=" + partNo);
			}
			queryBuffer.append(AfHibernateQueries.GET_TRANSACTION_RANGE_FILTER);
			
			
			queryBuffer.append(" order by afpc.prtCommNo desc");

			query = session.createQuery(queryBuffer.toString());
			query.setParameter("lowerValue", lowVal);
			query.setParameter("upperValue", upVal);
			result = query.list();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("@@@@ Exception in CommissionManagementDAOImpl at getPartnerCommissionList() :", e);
		}
		return result;
	}

	@Override
	public String getAFcategoryPath(String path ,Integer prtNo, Integer prtCat) {
		Query query = null;
		String result = null;
		
		
		
		try {
			query = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_PRT_CATEGORY_PATH);
			query.setParameter("prtCategoryPath", path);
			query.setParameter("prtNo", prtNo);
			query.setParameter("prtCat", prtCat);
			query.setMaxResults(1);
			result = (String) query.uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("@@@@ Exception in commissionManagementDAOImpl at getPartnerCommissionListByName() :", e);
		}
		return result;
	}

	@Override
	public List<AfPartnerCommission> getPartnerComNumWithPrtNoPrtCatNoLowUp(Integer prtNo, Integer prtCat, Float lowVal, Float UpVal) {
		Query query = null;
		StringBuffer queryBuffer = null;
		Session session = null;
		List<AfPartnerCommission> result = null;
		
		try {
			session = sessionFactory.getCurrentSession();
			queryBuffer = new StringBuffer(AfHibernateQueries.AF_COMMISION_NO_FROM_PRT_NO_AND_PRT_CAT_NO_UP_LOW_VAL);
			
			if(lowVal!=null && UpVal!=null){
				queryBuffer.append(" and afpc.lowerValue=" + lowVal+" and afpc.upperValue=" +UpVal);
			}
			if(UpVal!=null && lowVal==null){
				
					queryBuffer.append(" and afpc.upperValue=" +UpVal);
				}
		    if(lowVal!=null && UpVal==null){
					queryBuffer.append(" and afpc.lowerValue=" + lowVal);
					
				}
		    
		    	queryBuffer.append(" order by afpc.prtCommNo");
		    
			
			query = session.createQuery(queryBuffer.toString());
			result =  query.list();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("@@@@ Exception in commissionManagementDAOImpl at getPartnerComNumWithPrtNoPrtCatNoLowUp() :", e);
		}
		
		return result;
	}

	@Override
	public List<AfPartnerCommission> getPartnerCommissionList(String fromDate, String toDate, String partnerNo,
			int length, int start) {

		Query query = null;
		Session session = null;
		Date startDate = null;
		Date endDate = null;
		List<AfPartnerCommission> result = null;
		StringBuffer queryBuffer = null;
		SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd");
		try {
			session = sessionFactory.getCurrentSession();
			queryBuffer = new StringBuffer(AfHibernateQueries.AF_PARTNERCOMMISSION);

			if (!fromDate.trim().isEmpty() && !toDate.trim().isEmpty()) {
				startDate = ServiceProviderUtil.getInstance().getDateFromString(fromDate, "dd/MM/yyyy");
				endDate = ServiceProviderUtil.getInstance().getDateFromString(toDate, "dd/MM/yyyy");
				fromDate = dateFormater.format(startDate);
				toDate = dateFormater.format(endDate);
				queryBuffer.append(" and (date(apc.activationDate) >='" + fromDate
						+ "' or date(apc.deactivationDate) <='" + toDate + "')");
			}
			if (!partnerNo.trim().isEmpty()) {
				queryBuffer.append(" and apc.afPartner.prtNo=" + partnerNo);
			}
			queryBuffer.append(" order by apc.prtCommNo desc");

			query = session.createQuery(queryBuffer.toString());
			
				query.setFirstResult(start);
			result =	query.setMaxResults(length).list();
			
			//result = query.list();
		} catch (Exception e) {
			logger.error("@@@@ Exception in CommissionManagementDAOImpl at getPartnerCommissionList() :", e);
		}
		return result;
	
	}

	@Override
	public Long getPartnersCommissionPaginationCount(String fromDate, String toDate, String partnerNo) {
		Long totalCount =0L; 
		Query query = null;
		Session session = null;
		Date startDate = null;
		Date endDate = null;
		
		StringBuffer queryBuffer = null;
		SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd");
		try {
			session = sessionFactory.getCurrentSession();
			queryBuffer = new StringBuffer(AfHibernateQueries.GET_TOTAL_COMMISSION_COUNT);

			if (!fromDate.trim().isEmpty() && !toDate.trim().isEmpty()) {
				startDate = ServiceProviderUtil.getInstance().getDateFromString(fromDate, "dd/MM/yyyy");
				endDate = ServiceProviderUtil.getInstance().getDateFromString(toDate, "dd/MM/yyyy");
				fromDate = dateFormater.format(startDate);
				toDate = dateFormater.format(endDate);
				queryBuffer.append(" and (date(apc.activationDate) >='" + fromDate
						+ "' or date(apc.deactivationDate) <='" + toDate + "')");
			}
			if (!partnerNo.trim().isEmpty()) {
				queryBuffer.append(" and apc.afPartner.prtNo=" + partnerNo);
			}
			queryBuffer.append(" order by apc.prtCommNo desc");

			query = session.createQuery(queryBuffer.toString());
			
				
			
			totalCount = (Long)query.uniqueResult();

				
		} catch (Exception e) {
			logger.error("@@@@ Exception in CommissionManagementDAOImpl at getPartnerCommissionList() :", e);
		}
		return totalCount;
	
	}

	@Override
	public List<Object[]> getCommissionExportCsvList() {
		Session session = null;
		Query query = null;
		List<Object[]> results =null;
		StringBuffer queryBuffer = null;
		try{
			session = sessionFactory.openSession();
			queryBuffer = new StringBuffer(AfHibernateQueries.GET_COMMISSION_REPORT_LIST);
			query = session.createSQLQuery(queryBuffer.toString());
			results = query.list();
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in CommissionManagementDAOImpl at getCommissionExportCsvList() :", e);
		}
		finally {
			session.close();
		}
		return results;
	}

	

}
