package com.af.admin.DAO;

import java.util.List;

import com.af.admin.entity.AfAmazonPartnerCategory;
import com.af.admin.entity.AfCategory;
import com.af.admin.entity.AfPartner;
import com.af.admin.entity.AfPartnerCategory;
import com.af.admin.entity.AfPriceRangeFilter;
public interface CategoryManagementDAO {
	
	
	
	public List<AfCategory> getCategoryList();
	public String createCategory(AfCategory afcategoryBean);
	public String updateCategory(AfCategory afPartner);
	public AfCategory getCategoryByCategoryNo(String catNo);
	public boolean checkcategoryIdExist(String categoryID);
	public boolean checkCategoryPosition(String categoryDisplayOrder);
	
	public String deleteCategory(Integer catNo);
	
	public void mapCategories(AfPartnerCategory afpartnercategory);		
	
	public List<AfPartnerCategory> getmappingDetails();
	public List<AfPartnerCategory> getmappingDetailsExceptSearch();
	public String deactivateMappedCategory(Integer catNo);
	public String updatePartnerCategory(AfPartnerCategory afPartnerCategory);
	public AfPartnerCategory getPartnerCategoryByPartnerCategoryNo(String partnerCatNo);
	
	public void createPriceRangeFilter(List<AfPriceRangeFilter> afPriceRangeFilter);
	public boolean checkForPartnerCatNo(int prt_cat_no);
	public List<AfPriceRangeFilter> getPriceRangeFilterList(int prt_cat_no);
	public String deactivatePriceRangeFilter(AfPriceRangeFilter afPriceRangeFilter);

	public Long getLastCategoryCatNo(String query);
	public boolean checkInPriceRangeFilter(int catNo, String query);
	public List<AfPriceRangeFilter> getPriceRangeFilterList(int condition, String query);
	public Long getCategoryRowCount(String query);
	public Long getCategoryRowCount(String query, String condition);
	public boolean checkcategoryNameExist(String condition);
	public boolean checkpartnercategoryNameExist(String condition,Integer prtNo);
	public boolean checkpartnercategoryIdExist(String condition);
	public List<AfCategory> getCategoryList(String query, String condition);
	public List<AfPartnerCategory> getPartnerCategories(Integer prtNo);
	
	public boolean checkCategoryNoExistInOtherTable(String query, String condition);
	public AfCategory getCategoryByCategoryID(String categoryId);
	public boolean checkCategoryDispOrd(String displayOrder);
	public AfCategory getCategoryByCategoryName(String categoryName);
	
	public List<String> getBestSellingCategories();
	public String getPrtCatByCatName(String categoryName,Integer prtNo);
	public String createPartnercategories(AfPartnerCategory afPartCat);
	public AfPartnerCategory deleteCategoryvalues(Integer catNo);
	public List<AfAmazonPartnerCategory> getAmazonmappingDetails();
	public AfPartnerCategory getAfPartnerCategories(Integer prtCatNo);
	public String CreateAmazonPartner(AfAmazonPartnerCategory afAmazonPartnerCategory);
	public AfAmazonPartnerCategory getAmazonmappingDetailsById(Integer id);
	
}
