package com.af.admin.DAO.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.af.admin.DAO.PartnerProductDAO;
import com.af.admin.entity.AfPartnerProduct;
import com.af.admin.entity.AfProduct;

@Repository
public class PartnerProductDAOimpl implements PartnerProductDAO {
	private Logger logger = LogManager.getLogger(PartnerProductDAOimpl.class.getName());
	
	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public AfPartnerProduct getPartnerProductByPrtPrdNO(String prtPrdNo) {
		Session session = sessionFactory.getCurrentSession();
		AfPartnerProduct afPartnerProduct=null;
		try{			
			afPartnerProduct=(AfPartnerProduct)session.get(AfPartnerProduct.class, new Integer(prtPrdNo));
		}catch(Exception e){
			logger.error("@@@@ Exception in PartnerProductDAOimpl at getPartnerProductByPrtPrdNO() :", e);	
		}
		return afPartnerProduct;
	}

	@Override
	public AfProduct getProductByPrtPrdNO(String prtPrdNo) {
		Session session = sessionFactory.getCurrentSession();
		AfProduct afProduct=null;
		try{			
			afProduct=(AfProduct)session.get(AfProduct.class, new Integer(prtPrdNo));
		}catch(Exception e){
			logger.error("@@@@ Exception in PartnerProductDAOimpl at getProductByPrtPrdNO() :", e);	
		}
		return afProduct;
	}

	@Override
	public AfProduct getMasterProductByPrdNO(String prtPrdNo) {
		Session session = sessionFactory.getCurrentSession();
		AfProduct afProduct=null;
		try{			
			afProduct=(AfProduct)session.get(AfProduct.class, new Integer(prtPrdNo));
		}catch(Exception e){
			logger.error("@@@@ Exception in PartnerProductDAOimpl at getMasterProductByPrdNO() :", e);	
		}
		return afProduct;
	}
}
