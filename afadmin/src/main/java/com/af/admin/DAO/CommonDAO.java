package com.af.admin.DAO;

import java.util.List;

import com.af.admin.beans.AfAdminMenusBean;
import com.af.admin.entity.AfAdminRoles;
import com.af.admin.entity.AfEnumValues;
import com.af.admin.entity.AfPartnerCategory;
import com.af.admin.entity.AfPartnerKeyword;
import com.af.admin.entity.AfPartnerOffer;
import com.af.admin.entity.AfPartnerProduct;
import com.af.admin.entity.AfPermissionValues;
import com.af.admin.entity.AfProduct;
import com.af.admin.entity.AfSeo;



public interface CommonDAO {
	public List<AfAdminMenusBean> getMenus();
	public AfAdminRoles getAdminRoleById(Integer roleId);

	public List<AfAdminRoles> getAdminRoles();

	public List<AfPermissionValues> getPermissionValues();

	public List<AfEnumValues> getEnumValues(String typeName);
	public AfEnumValues getEnumValues(String typeName, String itemId);

	public List<String> getRolePermissions(Integer userRole);

	
	
	public List<AfPartnerProduct> getPartnerProductList(int prtCatNo);
	
	public List<AfPartnerCategory> getPartnerCategoryList(int prtNo);
	public String cerateKeyWord(List<AfPartnerKeyword> afPartnerKeywords);
	public String deleteKeyWord(Integer prtNo);
	
	public List<AfProduct> getProductList(int catNo);
	
	public String cerateOffersAndDeals(AfPartnerOffer afPartnerOffer);
	public List<AfPartnerOffer> getOffersAndDeals(String queryString);

	public List<AfSeo> getSEOData();
	public AfSeo getSEOData(byte pageId);
	public String saveSEOData(AfSeo afSeo);
	public AfEnumValues getEnumValuebyType(String typeName);
	public String saveSellerText(String value);
}