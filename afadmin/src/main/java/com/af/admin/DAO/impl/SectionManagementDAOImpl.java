package com.af.admin.DAO.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.af.admin.DAO.SectionManagementDAO;
import com.af.admin.constants.AfConstants;
import com.af.admin.constants.AfHibernateQueries;
import com.af.admin.entity.AfEnumValues;
import com.af.admin.entity.AfHomePageSection;
import com.af.admin.entity.AfSectionItems;
import com.af.admin.entity.AfSections;
@Repository
public class SectionManagementDAOImpl implements SectionManagementDAO {
	
	private Logger logger = LogManager.getLogger(SectionManagementDAOImpl.class.getName());
	
	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public List<AfSections> getSections() {

		List<AfSections> sectionList=new ArrayList<AfSections>();
		Session session = sessionFactory.getCurrentSession();
		Query query=session.createQuery(AfHibernateQueries.AF_SECTIONS);
		try{
			sectionList =query.list();
		}catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in SectionManagementDaoImpl at getSections() :", e);	
		}
		return sectionList;
	}
	
	@Override
	public List<AfSections> getSections(String sectionTitle, int sectionType) {
			AfSections afSection=null;
			List<AfSections> sectionList=new ArrayList<AfSections>();
			Query query = null;
			try{
				if(sectionTitle.equalsIgnoreCase("All")){
					query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_SECTIONS_ALL);
					query.setParameter("sectionType", sectionType);
				}
				sectionList = query.list();
			}catch(Exception e){
				e.printStackTrace();
				logger.error("@@@@ Exception in SectionManagementDaoImpl at getHomePageSection() :", e);	
			}
			return sectionList;
		}
	
	@Override
	public AfSections getHomePageSection(int sectionId) {
			AfSections afSection=null;
			try{
				Query query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_SECTIONS_BY_SECTION);
				query.setParameter("sectId", sectionId);
				afSection=(AfSections)query.uniqueResult() ;
			}catch(Exception e){
				e.printStackTrace();
				logger.error("@@@@ Exception in SectionManagementDaoImpl at getHomePageSection() :", e);	
			}
			return afSection;
		}
	
	@Override
	public String updateSection(AfSections afsection) {
		try {
			sessionFactory.getCurrentSession().merge(afsection);
		} catch (ConstraintViolationException e) {
			logger.error("@@@@ Exception in SectionManagementDaoImpl at updateSection() :", e);	
			return "failure";
		}catch (Exception e) {
			logger.error("@@@@ Exception in SectionManagementDaoImpl at updateSection :", e);	
			return "failure";
		}
		return "success";
	}
	
	@Override
	public boolean checkforSecTitle(String secTitle) {
		AfSections afSection=null;
		boolean flag=false;
		Query query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_SECTIONS_BY_SECTION_TITLE);
		query.setString("sectionTitle", secTitle);
		try{
			afSection=(AfSections)query.uniqueResult() ;
		}catch(Exception e){
			logger.error("@@@@ Exception in SectionManagementDaoImpl at checkforSecTitle() :", e);
		}
		if(afSection!=null) 
			flag= true;
		else
			flag =false;
		return flag;
	}
	
	@Override
	public List<AfSections> checkDisOrder(String dipOrder) {
		List<AfSections> afSection=new ArrayList<AfSections>();
		Session session = sessionFactory.getCurrentSession();
		try{
			Query query = session.createQuery(AfHibernateQueries.AF_SECTION_DISPLAY_ORDER_CHECK);
			query.setParameter("sectionDispOrder", Byte.parseByte(dipOrder));
			
			afSection=query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in SectionManagementDaoImpl at checkDisOrder() :", e);	
		}
		return afSection;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AfSectionItems> checkSectionItemDisplayOrder(String displayOrder,String sectionId){
		List<AfSectionItems> afSectionItems = new ArrayList<AfSectionItems>();
		Session session = sessionFactory.getCurrentSession();
		try{
			Query query = session.createQuery(AfHibernateQueries.AF_SECTION_ITEMS_DISPLAY_ORDER);
			query.setParameter("displayOrder", Integer.parseInt(displayOrder));
			query.setParameter("sectionId", Integer.parseInt(sectionId));
			afSectionItems=query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in SectionManagementDaoImpl at checkSectionItemDisplayorder() :", e);	
		}
		return afSectionItems;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AfSectionItems> checkDuplicateRecord(String sectionId, String itemType, String prtNo, String catNo, String prtPrdctNo, String prtOfferNo,String brandName){
		List<AfSectionItems> afSectionItems = new ArrayList<AfSectionItems>();
		Query query = null;
		
		try{
			switch(itemType){
				case AfConstants.ITEM_TYPE_PRODUCT:
					query = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_SECTION_ITEMS_DUPLICATE_RECORD_PRDT);
					query.setParameter("prtNo", Integer.parseInt(prtNo));
					query.setParameter("prtPrdctNo", Integer.parseInt(prtPrdctNo));
					break;
				case AfConstants.ITEM_TYPE_CATEGORY:
					query = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_SECTION_ITEMS_DUPLICATE_RECORD_CAT);
					query.setParameter("catNo", Integer.parseInt(catNo));
					break;
				case AfConstants.ITEM_TYPE_PARTNER:
					
					query = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_SECTION_ITEMS_DUPLICATE_RECORD_PRT);
					query.setParameter("prtNo", Integer.parseInt(prtNo));
					break;
				case AfConstants.ITEM_TYPE_OFFERS_AND_DEALS:
					query = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_SECTION_ITEMS_DUPLICATE_RECORD_OFFERS_DEALS);
					query.setParameter("prtNo", Integer.parseInt(prtNo));
					query.setParameter("catNo", Integer.parseInt(catNo));
					query.setParameter("prtOfferNo", Integer.parseInt(prtOfferNo));
					break;
				case AfConstants.ITEM_TYPE_BRAND:
					query = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_SECTION_ITEMS_DUPLICATE_RECORD_BRANDS);
					query.setParameter("brandName", brandName);
					break;
			}
			
			if(query != null){
				query.setParameter("sectionId", Integer.parseInt(sectionId));
				query.setParameter("itemType", Byte.parseByte(itemType));
				afSectionItems=query.list();
			}
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in SectionManagementDaoImpl at checkDuplicateRecord() :", e);	
		}
		return afSectionItems;
	}
	
	@Override
	public String createSectionItems(AfSectionItems afSectionItems){
		String status = "failure";

		try{
			sessionFactory.getCurrentSession().saveOrUpdate(afSectionItems);
			status = "success";
		}
		catch(Exception e){
			logger.error("@@@@ Exception in SectionManagementDaoImpl at createSectionItems() :", e);	
		}
		return status;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AfSectionItems> getSectionItems(String sectionId) {
		List<AfSectionItems> sectionItemList=new ArrayList<AfSectionItems>();
		Query query = null;
		try{
			query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_SECTION_ITEM);
			query.setParameter("sectId", Integer.parseInt(sectionId));
			sectionItemList = query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in SectionManagementDaoImpl at getSectionItems() :", e);	
		}
		return sectionItemList;
	}
	
	@Override
	public AfSectionItems  getSectionItemByItemId(String sectionItemId) {
			AfSectionItems afSectionItem=null;
			List<AfSectionItems> sectionItemList=new ArrayList<AfSectionItems>();
			try{
			 
				afSectionItem=(AfSectionItems)sessionFactory.getCurrentSession().get(AfSectionItems.class,Integer.parseInt(sectionItemId));
					}catch(Exception e){
				e.printStackTrace();
				logger.error("@@@@ Exception in SectionManagementDaoImpl at getSectionItemByItemId() :", e);	
			}
			return afSectionItem;
		}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AfEnumValues> getEnumValues(String typeName, String itemId) {
		List<AfEnumValues> enumList = new ArrayList<AfEnumValues>();
		Session session = sessionFactory.getCurrentSession();
		Query query = null;
			query = session.createQuery(AfHibernateQueries.AF_ENUM_VALUES_BY_ID);	
			query.setParameter("typeName", typeName);
			query.setParameter("intCode", Byte.parseByte(itemId));
		try {
			enumList = query.list();
		} catch (Exception e) {
			logger.error("@@@@ Exception in SectionManagementDaoImpl at getEnumValues() :", e);
		}
		return enumList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AfHomePageSection> getHomePageSections() {
		List<AfHomePageSection> homeSectionList = new ArrayList<>();
		Session session = sessionFactory.getCurrentSession();
		Query query = null;
			
		try {
			query = session.createQuery(AfHibernateQueries.AF_HOME_SECTIONS);
			homeSectionList = query.list();
		}
		catch (Exception e) {
			logger.error("@@@@ Exception in SectionManagementDaoImpl at getHomePageSections() :", e);
		}
		
		return homeSectionList;
	}
	
	@Override
	public AfHomePageSection getHomePageSectionById(Integer secId) {
		Session session = sessionFactory.getCurrentSession();
		AfHomePageSection afHomePageSection=null;
		try{
			afHomePageSection=(AfHomePageSection)session.get(AfHomePageSection.class, secId);
		}catch(Exception e){
			logger.error("@@@@ Exception in SectionManagementDaoImpl at getHomePageSectionById() :", e);	
		}
		return afHomePageSection;
	}
	
	@Override
	public String updateSection(AfHomePageSection afHomePageSection) {
		String status = "failure";
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(afHomePageSection);
			status = "success";
		}catch (Exception e) {
			logger.error("@@@@ Exception in SectionManagementDaoImpl at updateSection :", e);	;
		}
		return status;
	}
	
	@Override
	public String updateHomePageSectionByName(String sectionName, byte status, String exitValue, byte orderValue) {
		Session session = sessionFactory.getCurrentSession();
		String msg="failure";
		Query query = null;
		try{
				query = session.createQuery(AfHibernateQueries.UPDATE_AF_HOME_PAGE_SECTIONS_BY_NAME);
		        query.setString("sectionName", sectionName);
		        query.setByte("status", status);
		        query.setString("exitValue", exitValue);
		        query.setByte("orderValue", orderValue);
		        int rowCount = query.executeUpdate();
		        if(rowCount>0){
		        	msg="success";
		        }
		}catch(Exception e){
			logger.error("@@@@ Exception in SectionManagementDaoImpl at getHomePageSectionById() :", e);	
		}
		return msg;
	}

	@Override
	public AfSections getAfSectionByTitle(String title){
		AfSections afSections = new AfSections();
		Session session = sessionFactory.getCurrentSession();
		Query query = null;
			query = session.createQuery(AfHibernateQueries.AF_CUSTOM_SECTION_BY_TITLE);	
			query.setString("title",title);
		try {
			afSections = (AfSections) query.uniqueResult();

		} catch (Exception e) {
			logger.error("@@@@ Exception in SectionManagementDaoImpl at getAfSectionByTitle() :", e);
		}
		return afSections;

	}
}
