package com.af.admin.DAO.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.af.admin.DAO.EmailManagementDAO;
import com.af.admin.constants.AfHibernateQueries;
import com.af.admin.entity.AfEmailManagement;

@Repository
public class EmailManagementDAOImpl implements EmailManagementDAO {

	private static Logger logger = LogManager.getLogger(EmailManagementDAOImpl.class.getName());

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public void addContent(AfEmailManagement afEmailManagement) {
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(afEmailManagement);
		} catch (ConstraintViolationException e) {
			logger.error("@@@@ Exception in EmailManagementDAOImpl at addContent() :", e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AfEmailManagement> getAllEmail() {
		List<AfEmailManagement> emailManagementList = new ArrayList<AfEmailManagement>();
		Session session = sessionFactory.getCurrentSession();
		try {
			Query qry = session.createQuery(AfHibernateQueries.AF_GET_CONTENT_MANAGMENT);
			emailManagementList = qry.list();
		} catch (Exception e) {
			logger.error("@@@@ Exception in EmailManagementDAOImpl at getAllEmail() :", e);
		}
		return emailManagementList;
	}

	@Override
	public AfEmailManagement getContentById(int emId) {
		Session session = sessionFactory.getCurrentSession();
		AfEmailManagement afEmailManagement = null;
		try {
			afEmailManagement = (AfEmailManagement) session.get(AfEmailManagement.class, emId);
		} catch (Exception e) {
			logger.error("@@@@ Exception in EmailManagementDAOImpl at getContentById() :", e);
		}
		return afEmailManagement;
	}

	@Override
	public String updateContent(AfEmailManagement afEmailManagement) {
		try {
			sessionFactory.getCurrentSession().merge(afEmailManagement);
		} catch (ConstraintViolationException e) {
			logger.error("@@@@ Exception in EmailManagementDAOImpl at updateContent() :", e);
			return "failure";
		} catch (Exception e) {
			logger.error("@@@@ Exception in EmailManagementDAOImpl at updateContent() :", e);
			return "failure";
		}
		return "success";
	}

	@Override
	public String emailTypeExist(String emailType) {
		emailType = (null != emailType) ? emailType.trim() : "";
		AfEmailManagement afEmailManagement = null;
		if (emailType.length() > 0) {
			Query query;
			try {
				query = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_GET_CONTENT_BY_TYPE);
				query.setByte("contentType", Byte.valueOf(emailType));
				afEmailManagement = (AfEmailManagement) query.uniqueResult();
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("@@@@ Exception in EmailManagementDAOImpl at emailTypeExist() :", e);
			}
		}
		if (null != afEmailManagement) {
			return "exist";
		}
		return "";
	}

}
