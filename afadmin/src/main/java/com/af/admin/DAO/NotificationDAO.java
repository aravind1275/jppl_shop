package com.af.admin.DAO;

import java.util.List;

import com.af.admin.entity.AfNotification;

public interface NotificationDAO {

	public List<AfNotification> getNotification();

	public String createNotification(AfNotification afNotification);

	public String deleteNotification(Integer notificationNo);

	public String updateNotification(AfNotification afNotification);

	public AfNotification getNotificationid(Integer notificationNo);

	public boolean checkNotificationID(String notificationId);


}
