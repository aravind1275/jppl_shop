package com.af.admin.DAO;

import java.util.List;

import com.af.admin.entity.AfOfferDealLink;
import com.af.admin.entity.AfPartnerOffer;

public interface OffersDealsDAO {
	
	List<AfPartnerOffer> getOffersDealsList();
	
	List<AfPartnerOffer> getOffersDealsListByPrtNo(Integer prtNo);
	
	
	List<AfPartnerOffer> getSearchByNameList(String prtNo);
	
	List<AfPartnerOffer> getSearchByNameList(String prtNo, String prtOffrNo);
	
	List<AfPartnerOffer> getOfferByNameList(String searchByName);
	
	List<AfPartnerOffer> getSearchByNamePCOList(String prtNo, String prtCatNo);
	public List<AfPartnerOffer> getOfferTitle(String prtNo, String catNo);
	
	
	List<AfPartnerOffer> getSearchByPrtNoOfferList(String prtNo);
	
	List<AfPartnerOffer> getSearchByCatOfferList(String prtNo, String searchByName, String catNo);
	
	List<AfPartnerOffer> getSearchByNameOfferList(String prtNo, String searchByName, String searchByValue);
	
	List<AfPartnerOffer> getSearchByDescOfferList(String prtNo, String searchByName, String searchByValue);
	
	String defineOffersDeals(AfPartnerOffer afOffersDeals);
	
	AfPartnerOffer getPartnerOfferByNO(String prtOffrNo);
	
	String updateOfferDealsDetails(AfPartnerOffer afPartnerOffer);	
	
	String deleteSelectedOffer(Integer prtNo);
	
	public List<AfPartnerOffer> checkOffersDealSortOrder(String offersDealsSortOrder);
	
	public Integer getMaxOffandDealSl();

	String offersDealsLink(AfOfferDealLink offerLink);

	public List<AfOfferDealLink> getOffersDealList();

	AfOfferDealLink getOfferLmdId(String lmdId);

	AfOfferDealLink getOfferdealLmdId(String lmdId);

	String offersDealsLink(AfPartnerOffer afPartnerOffer);

	AfOfferDealLink getOfferdealId(Integer id);
	
	Integer getMaxValueOfColumn(String tableName,String columnName);

	Long getOfferLmdIdCheckDupl(String string);

	List<Object[]> getExportOffersDealList();

	/*List<AfPartnerOffer> getPartnerOfferLmdId();*/
	
	public List<AfOfferDealLink> getPendingLinkOffers();
	
	public List<AfOfferDealLink> approvedOfferDealLink();

	public List<AfPartnerOffer> getOffersDealsList(int length, int start);

	public Long getPartnersCommissionPaginationCount();

	public List<String> getStoreList();

	

	
}
