package com.af.admin.DAO;

import java.util.List;

import com.af.admin.beans.TransactionReportBean;
import com.af.admin.entity.AfPartnerProduct;
import com.af.admin.entity.AfTransaction;
                                                                                                                
public interface TransactionReportDAO {
	
	List<AfTransaction> getTransactionDetails(String trackingId);

	List<AfPartnerProduct> getProductNameById(String productId);
}
