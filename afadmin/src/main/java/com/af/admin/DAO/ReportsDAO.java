package com.af.admin.DAO;

import java.util.List;

import com.af.admin.beans.BuisnessReportsBean;
import com.af.admin.entity.AfPartnerProduct;
import com.af.admin.entity.AfProduct;
import com.af.admin.entity.AfTransaction;
import com.af.admin.entity.AfTransactionDetails;



public interface ReportsDAO {

	
	
	public List<AfPartnerProduct> getNewProductsReportList(String fromDate, String toDate,String partnerNo, String brandName);
	
	public List<AfProduct> getMappedProductsReportList(String selectBy, String selectByValue);

	public List<Object> getPartnersByProducts();

	public List<String> getBrandsByProducts(String toDate, String fromDate);
	
	public List<Object[]> getShopJPReports(String fromDate, String toDate,String partnerName, String jpNumber,String status,String statusType,Integer length,Integer start);

	public List<BuisnessReportsBean> getdailyMilesReports(String fromDate, String toDate,String partnerName, String jpNumber);
	
	public String downloadCrisRequest(String orderId);
	
	public String downloadCrisResponse(String orderId);

	public List<BuisnessReportsBean> getqueingReportList(String fromDate, String toDate, String prtNo, String jpNumber);

	public Long getOrderCount(String partnerItemId);
	
	public List<Object[]> getShopJPReports(String fromDate, String toDate,String partnerName, String jpNumber,String status,String statusType);

	public List<Object[]> getShopJPReportsZeroMiles(String fromDate, String toDate,String partnerName, String jpNumber,String status,String statusType);

	public List<Object[]> getShopInValidReports(String fromDate, String toDate, String prtNo, String jpNumber,Integer length, Integer start);

	public List<Object[]> getShopInValidReports(String fromDate, String toDate, String prtNo, String jpNumber);

	public List<Object[]> getJPMilesStatusReports(String fromDate, String toDate,String partnerName, String jpNumber,String status,String statusType,Integer length,Integer start);

	public List<Object[]> getJPMilesStatusReports(String fromDate, String toDate, String prtNo, String jpNumber,String status, String statusType);

	public List<AfTransactionDetails> getJpMilesByTransactionId(String transactionId, String partnerItemId);

	public List<AfTransaction> getTransactionId(String transactionId);

	public List<Object[]> getShopAllJPReports(String fromDate, String toDate, String jpNumber, String status,
			String statusType);
	
	public List<Object[]> getAllShopJPReportsZeroMiles(String fromDate, String toDate, String jpNumber,
			String status, String statusType);
	
	


 }
