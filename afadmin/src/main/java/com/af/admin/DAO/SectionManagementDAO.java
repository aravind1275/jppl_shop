package com.af.admin.DAO;

import java.util.List;

import com.af.admin.entity.AfEnumValues;
import com.af.admin.entity.AfHomePageSection;
import com.af.admin.entity.AfSectionItems;
import com.af.admin.entity.AfSections;

public interface SectionManagementDAO {
	
	public List<AfSections> getSections();
	public List<AfSections> getSections(String sectionTitle, int sectionType);
	public AfSections getHomePageSection(int sectionId);
	public String updateSection(AfSections afSection);
	public boolean checkforSecTitle(String secTitle);
	public List<AfSections> checkDisOrder(String dipOrder);
	public List<AfSectionItems> checkSectionItemDisplayOrder(String displayOrder,String sectionId);
	public List<AfSectionItems> checkDuplicateRecord(String sectionId, String itemType, String prtNo, String catNo, String prtPrdctNo, String prtOfferNo,String brandName);
	public String createSectionItems(AfSectionItems afSectionItems);
	public List<AfSectionItems> getSectionItems(String sectionId);
	public AfSectionItems getSectionItemByItemId(String sectionItemId);
	public List<AfEnumValues> getEnumValues(String typeName, String itemId);
	public List<AfHomePageSection> getHomePageSections();
	public AfHomePageSection getHomePageSectionById(Integer secId);
	public String updateSection(AfHomePageSection afHomePageSection);
	public String updateHomePageSectionByName(String sectionName, byte status, String exitValue, byte orderValue);
	public AfSections getAfSectionByTitle(String title);
}
