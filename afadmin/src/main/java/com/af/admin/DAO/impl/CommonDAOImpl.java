package com.af.admin.DAO.impl;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.af.admin.DAO.CommonDAO;
import com.af.admin.beans.AfAdminMenusBean;
import com.af.admin.constants.AfHibernateQueries;
import com.af.admin.entity.AfAdminRoles;
import com.af.admin.entity.AfEnumValues;
import com.af.admin.entity.AfPartnerCategory;
import com.af.admin.entity.AfPartnerKeyword;
import com.af.admin.entity.AfPartnerOffer;
import com.af.admin.entity.AfPartnerProduct;
import com.af.admin.entity.AfPermissionValues;
import com.af.admin.entity.AfProduct;
import com.af.admin.entity.AfSeo;

@Repository
public class CommonDAOImpl implements CommonDAO {
	private static Logger logger = LogManager.getLogger(CommonDAOImpl.class
			.getName());

	@SuppressWarnings("unused")
	private JdbcTemplate jdbcTemplate;
	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplate = new JdbcTemplate(this.dataSource);
	}

	@Autowired
	SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<AfAdminMenusBean> getMenus() {
		List<AfAdminMenusBean> listAdminMenusBeans = new ArrayList<AfAdminMenusBean>();
		try {
			listAdminMenusBeans = sessionFactory.getCurrentSession()
					.createQuery(AfHibernateQueries.AF_ADMIN_MENUS).list();
		} catch (Exception e) {
			logger.error("@@@@ Exception in CommonDAOImpl at getMenus() :", e);
		}
		return listAdminMenusBeans;
	}

	@Override
	public AfAdminRoles getAdminRoleById(Integer roleId) {
		AfAdminRoles adminRoles = null;
		try {
			adminRoles = (AfAdminRoles) sessionFactory.getCurrentSession().get(
					AfAdminRoles.class, roleId);
		} catch (Exception e) {
			logger.error("@@@@ Exception in CommonDAOImpl at getAdminRoleById() :", e);
		}
		return adminRoles;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AfAdminRoles> getAdminRoles() {
		List<AfAdminRoles> userList = new ArrayList<AfAdminRoles>();
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(AfHibernateQueries.AF_ADMIN_ROLES);
		try {
			userList = query.list();
		} catch (Exception e) {
			logger.error("@@@@ Exception in CommonDAOImpl at getAdminRoles() :", e);
		}
		return userList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AfPermissionValues> getPermissionValues() {
		List<AfPermissionValues> permissionValues = null;
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(AfHibernateQueries.AF_PERMISSION_VALUES_QUERY);
		try {
			permissionValues = query.list();
		} catch (Exception e) {
			logger.error("@@@@ Exception in CommonDAOImpl at getPermissionValues() :", e);
		}
		return permissionValues;
	}

	

	@SuppressWarnings("unchecked")
	@Override
	public List<AfEnumValues> getEnumValues(String typeName) {
		List<AfEnumValues> enumList = new ArrayList<AfEnumValues>();
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(AfHibernateQueries.AF_ENUM_VALUES);
		query.setParameter("typeName", typeName);
		try {
			enumList = query.list();
		} catch (Exception e) {
			logger.error("@@@@ Exception in CommonDAOImpl at getEnumValues() :", e);
		}
		return enumList;
	}

	

	@SuppressWarnings("unchecked")
	@Override
	public List<AfPartnerProduct> getPartnerProductList(int prtCatNo) {
		List<AfPartnerProduct> ppList = new ArrayList<AfPartnerProduct>();
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(AfHibernateQueries.AF_PARTNER_PRODUCT);
		try {
			query.setParameter("prtCatNo", prtCatNo);
			ppList = query.list();
		} catch (Exception e) {
			logger.error("@@@@ Exception in CommonDAOImpl at getPartnerProductList() :", e);
		}
		return ppList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getRolePermissions(Integer userRole) {
		List<String> permissionNames = null;
		Session session = sessionFactory.getCurrentSession();
		Query query = null;
		try {
			query = session.createQuery(AfHibernateQueries.AF_ROLE_PERMISSIONS_QUERY);
			query.setParameter("userRole", userRole);
			permissionNames = (List<String>) query.list();
		} catch (Exception e) {
			logger.error("@@@@ Exception in CommonDAOImpl at getRolePermissions() :", e);
		}
		return permissionNames;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AfPartnerCategory> getPartnerCategoryList(int prtNo) {
		List<AfPartnerCategory> pcList = new ArrayList<AfPartnerCategory>();
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(AfHibernateQueries.AF_PARTNER_CATEGORY_BY_PRTNO);
		try {
			query.setParameter("prtNo", prtNo);
			pcList = query.list();
		} catch (Exception e) {
			logger.error("@@@@ Exception in CommonDAOImpl at getPartnerCategoryList() :", e);
		}
		return pcList;
	}

	@Override
	public String cerateKeyWord(List<AfPartnerKeyword> afPartnerKeywords) {
		try {
			int count = 0;
			Session session = sessionFactory.getCurrentSession();
			for (AfPartnerKeyword afPartnerKeyword : afPartnerKeywords) {
				session.saveOrUpdate(afPartnerKeyword);
				if (count % 2 == 0) {
					session.flush();
					session.clear();
				}
				count++;
			}// for each

		} catch (ConstraintViolationException e) {
			logger.error("@@@@ ConstraintViolationException in CommonDAOImpl at  cerateKeyWord() :", e);
			return "failure";
		} catch (Exception e) {
			logger.error("@@@@ Exception in CommonDAOImpl cerateKeyWord at cerateKeyWord() :", e);
			return "failure";
		}
		return "success";
	}

	@Override
	public String deleteKeyWord(Integer prtNo) {
		try {
			Session session = sessionFactory.getCurrentSession();
			Query query = session
					.createQuery(AfHibernateQueries.AF_PARTNER_DELETE_KEYWORD);
			query.setParameter("prtNo", prtNo);
			query.executeUpdate();

		} catch (ConstraintViolationException e) {
			logger.error("@@@@ ConstraintViolationException in CommonDAOImpl at deleteKeyWord() :", e);
			return "failure";
		} catch (Exception e) {
			logger.error("@@@@ Exception in CommonDAOImpl at deleteKeyWord() :", e);
			return "failure";
		}
		return "success";
	}

	@Override
	public List<AfProduct> getProductList(int catNo) {
			List<AfProduct> pList = new ArrayList<AfProduct>();
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(AfHibernateQueries.AF_PRODUCT_BY_CAT_NO);
		try {
			query.setParameter("catNo", catNo);
			pList = query.list();
		} catch (Exception e) {
			logger.error("@@@@ Exception in CommonDAOImpl at getProductList() :", e);
		}
		return pList;
	}
	
	@Override
	public String cerateOffersAndDeals(AfPartnerOffer afPartnerOffer) {
		try {
			Session session = sessionFactory.getCurrentSession();
			session.saveOrUpdate(afPartnerOffer);
		} catch (ConstraintViolationException e) {
			logger.error("@@@@ ConstraintViolationException in CommonDAOImpl on cerateOffersAndDeals(-) :", e);
			e.printStackTrace();
			return "ConstraintViolationException";
		} catch (Exception e) {
			logger.error("@@@@ Exception in CommonDAOImpl on cerateOffersAndDeals(-) :", e);
			e.printStackTrace();
			return "failure";
		}
		return "success";
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AfPartnerOffer> getOffersAndDeals(String queryString) {
		List<AfPartnerOffer> afPartnerOfferList = new ArrayList<AfPartnerOffer>();
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(queryString);
		try {
			afPartnerOfferList = query.list();
		} catch (Exception e) {
			logger.error("@@@@ Exception in CommonDAOImpl at getOffersAndDeals() :", e);
		}
		return afPartnerOfferList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AfSeo> getSEOData(){
		List<AfSeo> afSeo = null;
		Query query = null;
		
		try{
			query = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.GET_SEO_DATA);
			afSeo = (List<AfSeo>)query.list();
		}
		catch(Exception e){
			logger.error("@@@@ Exception in CommonDAOImpl at getSEOData() :", e);
		}
		
		return afSeo;
	}
	
	@Override
	public AfSeo getSEOData(byte pageId){
		AfSeo afSeo = null;
		Query query = null;
		
		try{
			query = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.GET_SEO_DATA_BY_PAGE_ID);
			query.setParameter("pageId", pageId);
			query.setMaxResults(1);
			afSeo = (AfSeo)query.uniqueResult();
		}
		catch(Exception e){
			logger.error("@@@@ Exception in CommonDAOImpl at getSEOData() :", e);
		}
		
		return afSeo;
	}
	
	@Override
	public String saveSEOData(AfSeo afSeo){
		String status = "failure";
		
		try{
			
			sessionFactory.getCurrentSession().saveOrUpdate(afSeo);
			status = "success";
		}
		catch(Exception e){
			logger.error("@@@@ Exception in CommonDAOImpl at saveSEOData() :", e);
		}
		return status;
	}

	@Override
	public AfEnumValues getEnumValuebyType(String typeName) {
		AfEnumValues enamVal=null; 
		Query query = null;
		try{
			query = sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.GET_ENUM_VALUE_BY_TYPENAME);
			query.setParameter("typeName", typeName);
			query.setMaxResults(1);
			enamVal = (AfEnumValues)query.uniqueResult();
		}
		catch(Exception e){
			logger.error("@@@@ Exception in CommonDAOImpl at getEnumValuebyType() :"+ e);
		}
		
		return enamVal;
	}

	@Override
	public String saveSellerText(String value) {
		String status="failure";
		int updatedEntityCount = 0;
		Session session = sessionFactory.getCurrentSession();

		
		try{
			Query query = session.createQuery(AfHibernateQueries.UPDATE_ENUM_VALUE_BY_TYPENAME);
			query.setParameter("value", value);
			updatedEntityCount = query.executeUpdate();
		}
		catch(Exception e){
			updatedEntityCount = -1;
		}

		if(updatedEntityCount >= 0){
			status = "success";
		}

		return status;
	}

	@Override
	public AfEnumValues getEnumValues(String typeName, String itemId) {
		AfEnumValues enumList = new AfEnumValues();
		Session session = sessionFactory.getCurrentSession();
		Query query = null;
			query = session.createQuery(AfHibernateQueries.AF_ENUM_VALUES_BY_ID);	
			query.setParameter("typeName", typeName);
			query.setParameter("intCode", Byte.parseByte(itemId));
		try {
			enumList = (AfEnumValues) query.uniqueResult();
		} catch (Exception e) {
			logger.error("@@@@ Exception in SectionManagementDaoImpl at getEnumValues() :", e);
		}
		return enumList;
	}

	
}