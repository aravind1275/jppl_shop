package com.af.admin.DAO.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.af.admin.DAO.BannerManagementDAO;
import com.af.admin.constants.AfHibernateQueries;
import com.af.admin.entity.AfAdminUser;
import com.af.admin.entity.AfBanner;

@Repository

public class BannerManagementDAOimpl implements BannerManagementDAO {
	private Logger logger = LogManager.getLogger(BannerManagementDAOimpl.class.getName());

	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public List<AfBanner> getBanners() {
		List<AfBanner> bannerList=new ArrayList<AfBanner>();
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(AfHibernateQueries.AF_BANNER);
		try{
			bannerList = query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in BannerManagementDAOimpl at getBanners() :", e);	
		}
		return bannerList;
	}
	
	@Override
	public String createBanner(AfBanner afBanner) {
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(afBanner);
			
		} catch (ConstraintViolationException e) {
			logger.error("@@@@ Exception in BannerManagementDAOimpl at createBanner() :", e);	
			return "failure";
		}
		return "success";
	}
	
	@Override
	public AfBanner getBannerById(Integer bnrNo) {
		Session session = sessionFactory.getCurrentSession();
		AfBanner afBanner=null;
		try{
			afBanner=(AfBanner)session.get(AfBanner.class, bnrNo);
		}catch(Exception e){
			logger.error("@@@@ Exception in BannerManagementDAOimpl at getBannerById() :", e);	
		}
		return afBanner;
	}
	
	@Override
	public String updateBanner(AfBanner afBanner) {
		try {
			
			sessionFactory.getCurrentSession().merge(afBanner);
		} catch (ConstraintViolationException e) {
                    e.printStackTrace();
			logger.error("@@@@ Exception in BannerManagementDAOimpl at updateBanner() :", e);	
			return "failure";
		}catch (Exception e) {
			e.printStackTrace();
                        
			logger.error("@@@@ Exception in BannerManagementDAOimpl at updateBanner() :", e);	
			return "failure";
		}
		return "success";
	}
	
	@Override
	public String deleteBanner(Integer bnrNo) {
		String deleteStatus="";
		AfBanner afBanner = (AfBanner) sessionFactory.getCurrentSession().get(AfBanner.class, bnrNo);
		if(null!=afBanner){
			afBanner.setStatus((byte) 0);
			String bannerName=afBanner.getBannerName();
			bannerName=bannerName+"_#deleted"+"_"+bnrNo;
			afBanner.setBannerName(bannerName);
		}
		try{
			sessionFactory.getCurrentSession().merge(afBanner);
			deleteStatus="success";
		}catch(Exception e){
			deleteStatus="failure";
			logger.error("@@@@ Exception in BannerManagementDAOimpl at deleteBanner() :", e);	
			return deleteStatus;
		}	
		return deleteStatus;
	}
	
	@Override
	public boolean checkBannerID(String bannerID) {
		List<AfBanner> bannerList=new ArrayList<>();
		boolean flag=false;
		Query query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_BANNER_BY_BANNER_ID);
		query.setString("bannerId", bannerID);
		try{
			bannerList=query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in PartnerManagementDaoImpl at checkBannerID() :", e);
		}
		if(!bannerList.isEmpty()) 
			flag= true;
		else
			flag =false;
		return flag;
	}

	@Override
	public boolean checkUtmParameter(String source, String medium, String campaign) {
		List<AfBanner> bannerList=new ArrayList<>();
		boolean flag=false;
		Query query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_BANNER_BY_UTM_PARAMETER);
		query.setString("source", source);
		query.setString("medium", medium);
		query.setString("campaign", campaign);
		try{
			bannerList=query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in PartnerManagementDaoImpl at checkBannerID() :", e);
		}
		if(!bannerList.isEmpty()) 
			flag= true;
		else
			flag =false;
		return flag;
	}

	@Override
	public List<AfBanner> getBanners(int length, int start) {
		List<AfBanner> bannerList=new ArrayList<AfBanner>();
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(AfHibernateQueries.AF_BANNER);
		try{
                    query.setFirstResult(start);
			bannerList =query.setMaxResults(length).list();
                    
//			bannerList = query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in BannerManagementDAOimpl at getBanners() :", e);	
		}
		return bannerList;
	}

	@Override
	public Long getBannerListPaginationCount() {
		Long totalCount =0L; 
		List<AfBanner> bannerList=new ArrayList<AfBanner>();
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(AfHibernateQueries.GET_TOTAL_AF_BANNER_COUNT);
		try{
			bannerList = query.list();
			totalCount = (Long)query.uniqueResult();
		}catch(Exception e){
			logger.error("@@@@ Exception in BannerManagementDAOimpl at getBannerListPaginationCount() :", e);	
		}
		return totalCount;
	}

}
