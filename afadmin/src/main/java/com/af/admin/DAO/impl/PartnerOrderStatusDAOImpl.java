package com.af.admin.DAO.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.af.admin.DAO.PartnerManagementDAO;
import com.af.admin.DAO.PartnerOrderStatusDAO;
import com.af.admin.constants.AfHibernateQueries;
import com.af.admin.entity.AfNotification;
import com.af.admin.entity.AfPartner;
import com.af.admin.entity.PartnerOrderStatus;

@Repository
public class PartnerOrderStatusDAOImpl implements PartnerOrderStatusDAO {
	private Logger logger = LogManager.getLogger(PartnerOrderStatusDAOImpl.class.getName());
	
	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	PartnerManagementDAO partnerManagementDAO;
	
	@Override
	public String savePartnerOrderStatus(PartnerOrderStatus partnerOrderStatus) {
		
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(partnerOrderStatus);
			
		} catch (ConstraintViolationException e) {
			
			logger.error("@@@@ Exception in PartnerOrderStatusDAOImpl at savePartnerOrderStatus() :", e);	
			return "failure";
		}
		return "success";
	}

	@Override
	public List<PartnerOrderStatus> getPartnersStatus(String type) {
		List<PartnerOrderStatus> partnerOrderStatusList=new ArrayList<>();
		Session session = sessionFactory.getCurrentSession();
		Query query = null;
		if(type.equalsIgnoreCase("All")){
		 query=session.createQuery(AfHibernateQueries.AF_PARTNERS_ORDER_STATUS_ALL);
		}else{
			 query=session.createQuery(AfHibernateQueries.AF_PARTNERS_ORDER_STATUS);

		}
		try{
			partnerOrderStatusList =query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in PartnerManagementDaoImpl at getPartners() :", e);	
		}
		return partnerOrderStatusList;
	}

	@Override
	public boolean checkPartnerOrderStatusExist(String prtNo, String partnerStatus, String jpplStatus) {
		List<PartnerOrderStatus> partnerOrderStatusList=new ArrayList<PartnerOrderStatus>();
		
		boolean flag=false;
		AfPartner af=	partnerManagementDAO.getPartnerByPartnerNO(prtNo);
		String partnerName=af.getPartnerName();
		
		Query query =sessionFactory.getCurrentSession().createQuery(AfHibernateQueries.AF_PARTNERS_ORDER_STATUS_DUPLICATE);		
		query.setString("prtNo", prtNo);
		query.setString("partnerStatus", partnerStatus);
		query.setString("jpplStatus", jpplStatus);
		try{
			partnerOrderStatusList=query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in PartnerManagementDaoImpl at checkPartnerOrderStatusExist() :", e);
		}
		if(partnerOrderStatusList.size()>0) 
			flag= true;
		else
			flag =false;
		return flag;
	}

	@Override
	public PartnerOrderStatus getPartnerOrderStatusid(Integer id) {
		Session session = sessionFactory.getCurrentSession();
		PartnerOrderStatus partnerOrderStatus=null;
		try{
			partnerOrderStatus=(PartnerOrderStatus)session.get(PartnerOrderStatus.class, id);
		}catch(Exception e){
			logger.error("@@@@ Exception in PartnerManagementDaoImpl at getPartnerOrderStatusid() :", e);	
		}
		
	return partnerOrderStatus;
	}

	@Override
	public String updatePartnerOrderStatus(PartnerOrderStatus partnerOrderStatus) {
     try {
			sessionFactory.getCurrentSession().merge(partnerOrderStatus);
		} catch (ConstraintViolationException e) {
			logger.error("@@@@ Exception in PartnerManagementDaoImpl at updatePartnerOrderStatus() :", e);	
			return "failure";
		}
		return "success";
	}

}
