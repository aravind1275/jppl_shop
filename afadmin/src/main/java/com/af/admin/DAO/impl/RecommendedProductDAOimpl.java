package com.af.admin.DAO.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.af.admin.DAO.RecommendedCategoryDAO;
import com.af.admin.DAO.RecommendedProductDAO;
import com.af.admin.constants.AfHibernateQueries;
import com.af.admin.entity.AfPartner;
import com.af.admin.entity.AfPartnerProduct;
import com.af.admin.entity.AfRecommendedProduct;
import com.af.admin.utils.ServiceProviderUtil;

@Repository
public class RecommendedProductDAOimpl implements RecommendedProductDAO {
	private Logger logger = LogManager.getLogger(RecommendedProductDAOimpl.class.getName());

	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public List<AfRecommendedProduct> getRecommendedProducts() {
		List<AfRecommendedProduct> afrpList=new ArrayList<AfRecommendedProduct>();
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(AfHibernateQueries.AF_RECOMMENDED_PRODUCT);
		try{
			afrpList = query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in RecommendedProductDAOimpl at getRecommendedProducts() :", e);	
		}
		return afrpList;
	}
	
	
	@Override
	public String createRecommendedProduct(AfRecommendedProduct afRecommendedProduct) {
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(afRecommendedProduct);
			
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			logger.error("@@@@ Exception in RecommendedProductDAOimpl at createRecommendedProduct() :", e);	
			return "failure";
		}
		return "success";
	}
	
	@Override
	public List<AfRecommendedProduct> getRecommendedProduct(Map rpmap) {		
		List<AfRecommendedProduct> afRecommendedProduct=new ArrayList<AfRecommendedProduct>();
		Session session = sessionFactory.getCurrentSession();
		try{
			Query query = session.createQuery(AfHibernateQueries.AF_RECOMMENDED_PRODUCT_CHECK);		
			query.setParameter("prtPrdNo", Integer.parseInt(rpmap.get("prtPrdNo").toString()));			
			afRecommendedProduct=query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in RecommendedProductDAOimpl at getRecommendedProduct() :", e);	
		}
		return afRecommendedProduct;
	}
	
	@Override
	public List<AfRecommendedProduct> checkRecommendedProductPosition(String prtPrdPos) {
		List<AfRecommendedProduct> afRecommendedProduct=new ArrayList<AfRecommendedProduct>();
		Session session = sessionFactory.getCurrentSession();
		try{
			Query query = session.createQuery(AfHibernateQueries.AF_RECOMMENDED_PRODUCT_POSITION_CHECK);
			query.setParameter("prtPrdPos", Byte.parseByte(prtPrdPos));
			afRecommendedProduct=query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in RecommendedProductDAOimpl at checkRecommendedProductPosition() :", e);	
		}
		return afRecommendedProduct;
	}
	
	public static Date trim(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.HOUR, 23);
        return calendar.getTime();
    }
	
	@Override
	public AfRecommendedProduct getRecommendedProductByNo(String rpNo) {
		AfRecommendedProduct afRecommendedProduct=null;
		try{
			afRecommendedProduct =(AfRecommendedProduct) sessionFactory.getCurrentSession().get(AfRecommendedProduct.class, Integer.parseInt(rpNo));
		}catch(Exception e){
			logger.error("@@@@ Exception in RecommendedProductDAOimpl at getRecommendedProductByNo() :", e);
		}
		return afRecommendedProduct;
		
	}
	
	@Override
	public String updateRecommendedProduct(AfRecommendedProduct afRecommendedProduct) {
		String status="";
		try {
			
			sessionFactory.getCurrentSession().merge(afRecommendedProduct);
			status="success";
		} catch (Exception e) {
			logger.error("@@@@ Exception in RecommendedProductDAOimpl at updateRecommendedProduct", e);
			status="failure";
			return status;
		}
		return status;
		
	}
	
	@Override
	public String deleteRecommendedProduct(Integer prtNo) {
		String deleteStatus="";
		AfRecommendedProduct afRecommendedProduct = (AfRecommendedProduct) sessionFactory.getCurrentSession().get(AfRecommendedProduct.class, prtNo);
		if(null!=afRecommendedProduct){
			afRecommendedProduct.setStatus((byte) 0);			
		}
		try{
			sessionFactory.getCurrentSession().merge(afRecommendedProduct);
			deleteStatus="success";
		}catch(Exception e){
			deleteStatus="failure";
			logger.error("@@@@ Exception in RecommendedProductDAOimpl at deleteRecommendedProduct() :", e);	
			return deleteStatus;
		}	
		return deleteStatus;
	}

}
