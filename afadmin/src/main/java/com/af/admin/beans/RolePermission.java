package com.af.admin.beans;

import java.io.Serializable;

public class RolePermission implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Integer permissionID;
	private Integer permissionStatus;
	private Integer permissionValue;
	private byte permissionAction;
	private String categoryName;
	private String permissionCategoryName;
	private String permissionCategoryDisplayName;
	

	
	
	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getPermissionCategoryName() {
		return permissionCategoryName;
	}

	public void setPermissionCategoryName(String permissionCategoryName) {
		this.permissionCategoryName = permissionCategoryName;
	}

	public String getPermissionCategoryDisplayName() {
		return permissionCategoryDisplayName;
	}

	public void setPermissionCategoryDisplayName(
			String permissionCategoryDisplayName) {
		this.permissionCategoryDisplayName = permissionCategoryDisplayName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public RolePermission(Integer permissionID,Integer permissionValue,Byte permissionAction,Integer permissionStatus){
		this.permissionID=permissionID;
		this.permissionValue=permissionValue;
		this.permissionAction=permissionAction;
		this.permissionStatus=permissionStatus;
		
	}
	
	public Integer getPermissionID() {
		return permissionID;
	}
	public void setPermissionID(Integer permissionID) {
		this.permissionID = permissionID;
	}
	public Integer getPermissionStatus() {
		return permissionStatus;
	}
	public void setPermissionStatus(Integer permissionStatus) {
		this.permissionStatus = permissionStatus;
	}
	public Integer getPermissionValue() {
		return permissionValue;
	}
	public void setPermissionValue(Integer permissionValue) {
		this.permissionValue = permissionValue;
	}
	public byte getPermissionAction() {
		return permissionAction;
	}
	public void setPermissionAction(byte permissionAction) {
		this.permissionAction = permissionAction;
	}
	

}
