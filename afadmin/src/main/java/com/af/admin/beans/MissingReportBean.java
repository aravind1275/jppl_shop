package com.af.admin.beans;

public class MissingReportBean {

	private String partnerName;
	private String jpNumber;
	private String transactionId;
	private String SourceName;
	private String mediumName;
	private String campaignName;
	private String transSmsStatus;
	private String remEmailStatus;
	
	
	public MissingReportBean() {
		
	}
	
	public MissingReportBean(String partnerName, String jpNumber, String transactionId, String sourceName,
			String mediumName, String campaignName, String transSmsStatus, String remEmailStatus) {
		
		this.partnerName = partnerName;
		this.jpNumber = jpNumber;
		this.transactionId = transactionId;
		this.SourceName = sourceName;
		this.mediumName = mediumName;
		this.campaignName = campaignName;
		this.transSmsStatus = transSmsStatus;
		this.remEmailStatus = remEmailStatus;
	}
	
	
	
	
	
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	public String getJpNumber() {
		return jpNumber;
	}
	public void setJpNumber(String jpNumber) {
		this.jpNumber = jpNumber;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getSourceName() {
		return SourceName;
	}
	public void setSourceName(String sourceName) {
		SourceName = sourceName;
	}
	public String getMediumName() {
		return mediumName;
	}
	public void setMediumName(String mediumName) {
		this.mediumName = mediumName;
	}
	public String getCampaignName() {
		return campaignName;
	}
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}
	public String getTransSmsStatus() {
		return transSmsStatus;
	}
	public void setTransSmsStatus(String transSmsStatus) {
		this.transSmsStatus = transSmsStatus;
	}
	public String getRemEmailStatus() {
		return remEmailStatus;
	}
	public void setRemEmailStatus(String remEmailStatus) {
		this.remEmailStatus = remEmailStatus;
	}


	
	
}
