package com.af.admin.beans;

import java.util.Date;

import com.af.admin.entity.AfPartner;



public class AfPartnerCategoryBean {
		
	private String prtCatNo;
	private AfPartner afPartner;
	private String categoryId;
	private String categoryName;
	private byte categoryStatus;
	private Date createdTime;
	private Integer createdBy;
	private Date modifiedTime;
	private Integer modifiedBy;
	private String partnerCategoryId;
	private byte status;
	
	public String getPrtCatNo() {
		return prtCatNo;
	}
	public void setPrtCatNo(String prtCatNo) {
		this.prtCatNo = prtCatNo;
	}
	public AfPartner getAfPartner() {
		return afPartner;
	}
	public void setAfPartner(AfPartner afPartner) {
		this.afPartner = afPartner;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public byte getCategoryStatus() {
		return categoryStatus;
	}
	public void setCategoryStatus(byte categoryStatus) {
		this.categoryStatus = categoryStatus;
	}
	public Date getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Date getModifiedTime() {
		return modifiedTime;
	}
	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	public Integer getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public byte getStatus() {
		return status;
	}
	public void setStatus(byte status) {
		this.status = status;
	}
	public String getPartnerCategoryId() {
		return partnerCategoryId;
	}
	public void setPartnerCategoryId(String partnerCategoryId) {
		this.partnerCategoryId = partnerCategoryId;
	}
}
