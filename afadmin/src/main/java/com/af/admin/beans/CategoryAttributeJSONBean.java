/**
 * 
 */
package com.af.admin.beans;

/**
 * @author jogeswarsahu
 *
 */
public class CategoryAttributeJSONBean {

	private String startDate;
	private String endDate;
	private int status;
	private String categoryId;
	private String catNo;
	
	private String  startValue;
	private String  endValue;
	private String  stepValue;
	
	private String goQuicklyCategory;
	private String gqcDisplayOrder;
	private String bestSellingCategory;
	private String linkedCategory;
	private String  categoryImageUrl;
	private String linkedCategoryName;
	
	/**
	 * @return the bestSellingCategory
	 */
	public String getBestSellingCategory() {
		return bestSellingCategory;
	}
	/**
	 * @param bestSellingCategory the bestSellingCategory to set
	 */
	public void setBestSellingCategory(String bestSellingCategory) {
		this.bestSellingCategory = bestSellingCategory;
	}
	
	/**
	 * @return the categoryImageUrl
	 */
	public String getCategoryImageUrl() {
		return categoryImageUrl;
	}
	/**
	 * @param categoryImageUrl the categoryImageUrl to set
	 */
	public void setCategoryImageUrl(String categoryImageUrl) {
		this.categoryImageUrl = categoryImageUrl;
	}
	
	/**
	 * @return the linkedCategory
	 */
	public String getLinkedCategory() {
		return linkedCategory;
	}
	/**
	 * @param linkedCategory the linkedCategory to set
	 */
	public void setLinkedCategory(String linkedCategory) {
		this.linkedCategory = linkedCategory;
	}

	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}
	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}
	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}
	/**
	 * @return the categoryId
	 */
	public String getCategoryId() {
		return categoryId;
	}
	/**
	 * @param categoryId the categoryId to set
	 */
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	/**
	 * @return the catNo
	 */
	public String getCatNo() {
		return catNo;
	}
	/**
	 * @param catNo the catNo to set
	 */
	public void setCatNo(String catNo) {
		this.catNo = catNo;
	}
	/**
	 * @return the startValue
	 */
	public String getStartValue() {
		return startValue;
	}
	/**
	 * @param startValue the startValue to set
	 */
	public void setStartValue(String startValue) {
		this.startValue = startValue;
	}
	/**
	 * @return the endValue
	 */
	public String getEndValue() {
		return endValue;
	}
	/**
	 * @param endValue the endValue to set
	 */
	public void setEndValue(String endValue) {
		this.endValue = endValue;
	}
	/**
	 * @return the stepValue
	 */
	public String getStepValue() {
		return stepValue;
	}
	/**
	 * @param stepValue the stepValue to set
	 */
	public void setStepValue(String stepValue) {
		this.stepValue = stepValue;
	}
	/**
	 * @return the goQuicklyCategory
	 */
	public String getGoQuicklyCategory() {
		return goQuicklyCategory;
	}
	/**
	 * @param goQuicklyCategory the goQuicklyCategory to set
	 */
	public void setGoQuicklyCategory(String goQuicklyCategory) {
		this.goQuicklyCategory = goQuicklyCategory;
	}
	/**
	 * @return the gqcDisplayOrder
	 */
	public String getGqcDisplayOrder() {
		return gqcDisplayOrder;
	}
	/**
	 * @param gqcDisplayOrder the gqcDisplayOrder to set
	 */
	public void setGqcDisplayOrder(String gqcDisplayOrder) {
		this.gqcDisplayOrder = gqcDisplayOrder;
	}
	/**
	 * @return the linkedCategoryName
	 */
	public String getLinkedCategoryName() {
		return linkedCategoryName;
	}
	/**
	 * @param linkedCategoryName the linkedCategoryName to set
	 */
	public void setLinkedCategoryName(String linkedCategoryName) {
		this.linkedCategoryName = linkedCategoryName;
	}
	
	
}
