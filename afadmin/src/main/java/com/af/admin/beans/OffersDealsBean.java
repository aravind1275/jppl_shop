package com.af.admin.beans;

import java.util.List;

public class OffersDealsBean {

	private Integer prtOffrNo;
	private Integer prtNo;
	private Integer prtCatNo;
	private Integer offerId;
	private String  offerName;
	private String  offerDesc;
	private String 	categoryName;
	private Integer 	sortOrder;
	private String 	offerStartDate;
	private String 	offerEndDate;
	private String 	landingUrl;
	private String 	imageUrl;
	private String offerImageUrl;
	private String 	status;
	private String 	offersStatus;
	private String 	offersStatusClass;
	private int    	offersStatusCode;
	private String searchByName;
	private String searchByValue;
	private String catNo;
	private String altTxt;
	private List<PartnerCategoryBean> partnerCategoryBean;
	
	private String  getCategoryList;
	
	private String requestPrtNo;
	private String requestSearchByName;
	private String requestSearchByValue;
	private String requestCatNo;
	private String offerTitle;
	private String LmdId;
	
	public Integer getPrtOffrNo() {
		return prtOffrNo;
	}
	public void setPrtOffrNo(Integer prtOffrNo) {
		this.prtOffrNo = prtOffrNo;
	}
	public Integer getPrtNo() {
		return prtNo;
	}
	public void setPrtNo(Integer prtNo) {
		this.prtNo = prtNo;
	}	
	public Integer getPrtCatNo() {
		return prtCatNo;
	}
	public void setPrtCatNo(Integer prtCatNo) {
		this.prtCatNo = prtCatNo;
	}
	public Integer getOfferId() {
		return offerId;
	}
	public void setOfferId(Integer offerId) {
		this.offerId = offerId;
	}
	public String getOfferName() {
		return offerName;
	}
	public void setOfferName(String offerName) {
		this.offerName = offerName;
	}
	public String getOfferDesc() {
		return offerDesc;
	}
	public void setOfferDesc(String offerDesc) {
		this.offerDesc = offerDesc;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getOfferStartDate() {
		return offerStartDate;
	}
	public void setOfferStartDate(String offerStartDate) {
		this.offerStartDate = offerStartDate;
	}
	public String getOfferEndDate() {
		return offerEndDate;
	}
	public void setOfferEndDate(String offerEndDate) {
		this.offerEndDate = offerEndDate;
	}
	public String getLandingUrl() {
		return landingUrl;
	}
	public void setLandingUrl(String landingUrl) {
		this.landingUrl = landingUrl;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}	
	public String getOffersStatus() {
		return offersStatus;
	}
	public void setOffersStatus(String offersStatus) {
		this.offersStatus = offersStatus;
	}
	public String getOffersStatusClass() {
		return offersStatusClass;
	}
	public void setOffersStatusClass(String offersStatusClass) {
		this.offersStatusClass = offersStatusClass;
	}
	public int getOffersStatusCode() {
		return offersStatusCode;
	}
	public void setOffersStatusCode(int offersStatusCode) {
		this.offersStatusCode = offersStatusCode;
	}
	public String getSearchByName() {
		return searchByName;
	}
	public void setSearchByName(String searchByName) {
		this.searchByName = searchByName;
	}
	public String getSearchByValue() {
		return searchByValue;
	}
	public void setSearchByValue(String searchByValue) {
		this.searchByValue = searchByValue;
	}
	public List<PartnerCategoryBean> getPartnerCategoryBean() {
		return partnerCategoryBean;
	}
	public void setPartnerCategoryBean(List<PartnerCategoryBean> partnerCategoryBean) {
		this.partnerCategoryBean = partnerCategoryBean;
	}
	/**
	 * @return the getCategoryList
	 */
	public String getGetCategoryList() {
		return getCategoryList;
	}
	/**
	 * @param getCategoryList the getCategoryList to set
	 */
	public void setGetCategoryList(String getCategoryList) {
		this.getCategoryList = getCategoryList;
	}
	public String getCatNo() {
		return catNo;
	}
	public void setCatNo(String catNo) {
		this.catNo = catNo;
	}
	/**
	 * @return the altTxt
	 */
	public String getAltTxt() {
		return altTxt;
	}
	/**
	 * @param altTxt the altTxt to set
	 */
	public void setAltTxt(String altTxt) {
		this.altTxt = altTxt;
	}
	/**
	 * @return the sortOrder
	 */
	public Integer getSortOrder() {
		return sortOrder;
	}
	/**
	 * @param sortOrder the sortOrder to set
	 */
	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}
	/**
	 * @return the requestPrtNo
	 */
	public String getRequestPrtNo() {
		return requestPrtNo;
	}
	/**
	 * @param requestPrtNo the requestPrtNo to set
	 */
	public void setRequestPrtNo(String requestPrtNo) {
		this.requestPrtNo = requestPrtNo;
	}
	/**
	 * @return the requestSearchByName
	 */
	public String getRequestSearchByName() {
		return requestSearchByName;
	}
	/**
	 * @param requestSearchByName the requestSearchByName to set
	 */
	public void setRequestSearchByName(String requestSearchByName) {
		this.requestSearchByName = requestSearchByName;
	}
	/**
	 * @return the requestSearchByValue
	 */
	public String getRequestSearchByValue() {
		return requestSearchByValue;
	}
	/**
	 * @param requestSearchByValue the requestSearchByValue to set
	 */
	public void setRequestSearchByValue(String requestSearchByValue) {
		this.requestSearchByValue = requestSearchByValue;
	}
	/**
	 * @return the requestCatNo
	 */
	public String getRequestCatNo() {
		return requestCatNo;
	}
	/**
	 * @param requestCatNo the requestCatNo to set
	 */
	public void setRequestCatNo(String requestCatNo) {
		this.requestCatNo = requestCatNo;
	}
	public String getOfferTitle() {
		return offerTitle;
	}
	public void setOfferTitle(String offerTitle) {
		this.offerTitle = offerTitle;
	}
	public String getLmdId() {
		return LmdId;
	}
	public void setLmdId(String lmdId) {
		LmdId = lmdId;
	}
	public String getOfferImageUrl() {
		return offerImageUrl;
	}
	public void setOfferImageUrl(String offerImageUrl) {
		this.offerImageUrl = offerImageUrl;
	} 
			
}
