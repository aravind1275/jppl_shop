package com.af.admin.beans;


public class KeyWordBean {
		
	private Integer keyWordNo;
	private String activationDate;
	private String deactivationDate;
	private String logoPartnerLandingUrl;
	private String filePath;
	private String fileName;
	
	
	public Integer getKeyWordNo() {
		return keyWordNo;
	}
	public void setKeyWordNo(Integer keyWordNo) {
		this.keyWordNo = keyWordNo;
	}
	public String getActivationDate() {
		return activationDate;
	}
	public void setActivationDate(String activationDate) {
		this.activationDate = activationDate;
	}
	public String getDeactivationDate() {
		return deactivationDate;
	}
	public void setDeactivationDate(String deactivationDate) {
		this.deactivationDate = deactivationDate;
	}
	public String getLogoPartnerLandingUrl() {
		return logoPartnerLandingUrl;
	}
	public void setLogoPartnerLandingUrl(String logoPartnerLandingUrl) {
		this.logoPartnerLandingUrl = logoPartnerLandingUrl;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
		
	
	
	
}