package com.af.admin.beans;

public class CommissionUploadBean {

	private String partnerName;
	private String prtCategoryName;
	private String prtProductName;
	private String categoryPath;
	private String maximumJpMilesEarned;
	private String partnerCommisionType;
	private String commisionValue;
	private String jpMilesCommsion;
	private String bonusJpMilesCommisionType;
	private String bonusJpMilesCommisionValue;
	private String bonusActivateCode;
	private String bonusPartnerCode;
	private String lowerValueRange;
	private String upperValueRange;
	private String commisionStatus;

	private String activationDate;
	private String deactivationDate;
	
	private String transactionStatus;

	public CommissionUploadBean() {

	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getPrtCategoryName() {
		return prtCategoryName;
	}

	public void setPrtCategoryName(String prtCategoryName) {
		this.prtCategoryName = prtCategoryName;
	}

	public String getPrtProductName() {
		return prtProductName;
	}

	public void setPrtProductName(String prtProductName) {
		this.prtProductName = prtProductName;
	}

	public String getCategoryPath() {
		return categoryPath;
	}

	public void setCategoryPath(String categoryPath) {
		this.categoryPath = categoryPath;
	}

	public String getMaximumJpMilesEarned() {
		return maximumJpMilesEarned;
	}

	public void setMaximumJpMilesEarned(String maximumJpMilesEarned) {
		this.maximumJpMilesEarned = maximumJpMilesEarned;
	}

	public String getPartnerCommisionType() {
		return partnerCommisionType;
	}

	public void setPartnerCommisionType(String partnerCommisionType) {
		this.partnerCommisionType = partnerCommisionType;
	}

	public String getCommisionValue() {
		return commisionValue;
	}

	public void setCommisionValue(String commisionValue) {
		this.commisionValue = commisionValue;
	}

	public String getJpMilesCommsion() {
		return jpMilesCommsion;
	}

	public void setJpMilesCommsion(String jpMilesCommsion) {
		this.jpMilesCommsion = jpMilesCommsion;
	}

	public String getBonusJpMilesCommisionType() {
		return bonusJpMilesCommisionType;
	}

	public void setBonusJpMilesCommisionType(String bonusJpMilesCommisionType) {
		this.bonusJpMilesCommisionType = bonusJpMilesCommisionType;
	}

	public String getBonusJpMilesCommisionValue() {
		return bonusJpMilesCommisionValue;
	}

	public void setBonusJpMilesCommisionValue(String bonusJpMilesCommisionValue) {
		this.bonusJpMilesCommisionValue = bonusJpMilesCommisionValue;
	}

	public String getBonusActivateCode() {
		return bonusActivateCode;
	}

	public void setBonusActivateCode(String bonusActivateCode) {
		this.bonusActivateCode = bonusActivateCode;
	}

	public String getBonusPartnerCode() {
		return bonusPartnerCode;
	}

	public void setBonusPartnerCode(String bonusPartnerCode) {
		this.bonusPartnerCode = bonusPartnerCode;
	}

	public String getLowerValueRange() {
		return lowerValueRange;
	}

	public void setLowerValueRange(String lowerValueRange) {
		this.lowerValueRange = lowerValueRange;
	}

	public String getUpperValueRange() {
		return upperValueRange;
	}

	public void setUpperValueRange(String upperValueRange) {
		this.upperValueRange = upperValueRange;
	}

	public String getCommisionStatus() {
		return commisionStatus;
	}

	public void setCommisionStatus(String commisionStatus) {
		this.commisionStatus = commisionStatus;
	}

	public String getActivationDate() {
		return activationDate;
	}

	public void setActivationDate(String activationDate) {
		this.activationDate = activationDate;
	}

	public String getDeactivationDate() {
		return deactivationDate;
	}

	public void setDeactivationDate(String deactivationDate) {
		this.deactivationDate = deactivationDate;
	}

	public String getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

		
}
