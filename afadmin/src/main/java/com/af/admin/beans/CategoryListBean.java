package com.af.admin.beans;

import java.util.Date;

import com.af.admin.entity.AfPartner;



public class CategoryListBean {
		
	private Integer catlNo;
	private AfPartner afPartner;
	private String categoryId;
	private String categoryName;
	private byte categoryStatus;
	private Date createdTime;
	private Integer createdBy;
	private byte status;
	public Integer getCatlNo() {
		return catlNo;
	}
	public void setCatlNo(Integer catlNo) {
		this.catlNo = catlNo;
	}
	public AfPartner getAfPartner() {
		return afPartner;
	}
	public void setAfPartner(AfPartner afPartner) {
		this.afPartner = afPartner;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public byte getCategoryStatus() {
		return categoryStatus;
	}
	public void setCategoryStatus(byte categoryStatus) {
		this.categoryStatus = categoryStatus;
	}
	public Date getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public byte getStatus() {
		return status;
	}
	public void setStatus(byte status) {
		this.status = status;
	}
	
	
	
	
	
}
