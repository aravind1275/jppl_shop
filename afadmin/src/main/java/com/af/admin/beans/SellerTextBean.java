package com.af.admin.beans;

public class SellerTextBean {
	private String sellerFieldName;
	private String sellerFieldValue;
	public String getSellerFieldName() {
		return sellerFieldName;
	}
	public void setSellerFieldName(String sellerFieldName) {
		this.sellerFieldName = sellerFieldName;
	}
	public String getSellerFieldValue() {
		return sellerFieldValue;
	}
	public void setSellerFieldValue(String sellerFieldValue) {
		this.sellerFieldValue = sellerFieldValue;
	}
	

}
