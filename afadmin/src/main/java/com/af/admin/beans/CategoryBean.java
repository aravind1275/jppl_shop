package com.af.admin.beans;

public class CategoryBean {
		
	private Integer catNo;
	private String  partNO;
	private String  partnerName;
	private String  partnerID;
	private String  categoryId;
	private String  categoryName;
	private String  baseCategory;
	private String  partnerCategoryId;
	private String  partnercategoryName;
	private String  prtCatNo;
	
	private String  categoryImageUrl;
	private String  categorySatus;
	private String  categoryStatusClass;
	private int     categoryStatusCode;
	
	private String  startDate;
	private String  endDate;
	
	private String  parentCategoryName;
	private String  categoryNo;
	private String  action;
	private String  getCategoryList;

	private String  startValue;
	private String  endValue;
	private String  stepValue;
	
	private Byte goQuicklyCategory;
	private Byte gqcDisplayOrder;
	private Byte gqcSubDisplayOrder;
	
	private Byte bestSellingCategory;
	private String linkedCategory;
	private String linkedCategoryName;
	
	/**
	 * @return the linkedCategoryName
	 */
	public String getLinkedCategoryName() {
		return linkedCategoryName;
	}
	/**
	 * @param linkedCategoryName the linkedCategoryName to set
	 */
	public void setLinkedCategoryName(String linkedCategoryName) {
		this.linkedCategoryName = linkedCategoryName;
	}
	public String getPartnerCategoryId() {
		return partnerCategoryId;
	}
	public void setPartnerCategoryId(String partnerCategoryId) {
		this.partnerCategoryId = partnerCategoryId;
	}
	public String getPartnercategoryName() {
		return partnercategoryName;
	}
	public void setPartnercategoryName(String partnercategoryName) {
		this.partnercategoryName = partnercategoryName;
	}
	
	public Integer getCatNo() {
		return catNo;
	}
	public void setCatNo(Integer catNo) {
		this.catNo = catNo;
	}
	public String getPartNO() {
		return partNO;
	}
	public void setPartNO(String partNO) {
		this.partNO = partNO;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getBaseCategory() {
		return baseCategory;
	}
	public void setBaseCategory(String baseCategory) {
		this.baseCategory = baseCategory;
	}
	public String getCategoryImageUrl() {
		return categoryImageUrl;
	}
	public void setCategoryImageUrl(String categoryImageUrl) {
		this.categoryImageUrl = categoryImageUrl;
	}
	
	public String getCategorySatus() {
		return categorySatus;
	}
	public void setCategorySatus(String categorySatus) {
		this.categorySatus = categorySatus;
	}
	public String getCategoryStatusClass() {
		return categoryStatusClass;
	}
	public void setCategoryStatusClass(String categoryStatusClass) {
		this.categoryStatusClass = categoryStatusClass;
	}
	public int getCategoryStatusCode() {
		return categoryStatusCode;
	}
	public void setCategoryStatusCode(int categoryStatusCode) {
		this.categoryStatusCode = categoryStatusCode;
	}
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	public String getPartnerID() {
		return partnerID;
	}
	public void setPartnerID(String partnerID) {
		this.partnerID = partnerID;
	}
	public String getPrtCatNo() {
		return prtCatNo;
	}
	public void setPrtCatNo(String prtCatNo) {
		this.prtCatNo = prtCatNo;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	public String getGetCategoryList() {
		return getCategoryList;
	}
	public void setGetCategoryList(String getCategoryList) {
		this.getCategoryList = getCategoryList;
	}
	/**
	 * @return the categoryNo
	 */
	public String getCategoryNo() {
		return categoryNo;
	}
	/**
	 * @param categoryNo the categoryNo to set
	 */
	public void setCategoryNo(String categoryNo) {
		this.categoryNo = categoryNo;
	}
	/**
	 * @return the startValue
	 */
	public String getStartValue() {
		return startValue;
	}
	/**
	 * @param startValue the startValue to set
	 */
	public void setStartValue(String startValue) {
		this.startValue = startValue;
	}
	/**
	 * @return the endValue
	 */
	public String getEndValue() {
		return endValue;
	}
	/**
	 * @param endValue the endValue to set
	 */
	public void setEndValue(String endValue) {
		this.endValue = endValue;
	}
	/**
	 * @return the stepValue
	 */
	public String getStepValue() {
		return stepValue;
	}
	/**
	 * @param stepValue the stepValue to set
	 */
	public void setStepValue(String stepValue) {
		this.stepValue = stepValue;
	}
	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}
	/**
	 * @param action the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}
	/**
	 * @return the parentCategoryName
	 */
	public String getParentCategoryName() {
		return parentCategoryName;
	}
	/**
	 * @param parentCategoryName the parentCategoryName to set
	 */
	public void setParentCategoryName(String parentCategoryName) {
		this.parentCategoryName = parentCategoryName;
	}

	
	public Byte getGoQuicklyCategory() {
		return goQuicklyCategory;
	}

	public void setGoQuicklyCategory(Byte goQuicklyCategory) {
		this.goQuicklyCategory = goQuicklyCategory;
	}

	public Byte getGqcDisplayOrder() {
		return gqcDisplayOrder;
	}
	
	public void setGqcDisplayOrder(Byte gqcDisplayOrder) {
		this.gqcDisplayOrder = gqcDisplayOrder;
	}

	public Byte getGqcSubDisplayOrder() {
		return gqcSubDisplayOrder;
	}

	public void setGqcSubDisplayOrder(Byte gqcSubDisplayOrder) {
		this.gqcSubDisplayOrder = gqcSubDisplayOrder;
	}
	
	public Byte getBestSellingCategory() {
		return bestSellingCategory;
	}
	public void setBestSellingCategory(Byte bestSellingCategory) {
		this.bestSellingCategory = bestSellingCategory;
	}
	
	public String getLinkedCategory() {
		return linkedCategory;
	}
	public void setLinkedCategory(String linkedCategory) {
		this.linkedCategory = linkedCategory;
	}
	
}