package com.af.admin.beans;


import com.fasterxml.jackson.annotation.JsonProperty;

public class offersBean {
	
	 private String id;
	 private String lmd_id;
	 private String store;
	 private String offer_text;
	 private String offers__title;
	 private String offers__description;
	 private String code;
	 private String terms_and_conditions;
	 private String featured;
	 private String smartLink;
	 private String image_url;
	 private String type;
	 private String offer;
	 private String offer_value;
	 private String status;
	 private String offers__categories;
	 private String offers__url;
	 private String offers__start_date;
	 private String offers__end_date;
	 private String partnermapname;
	
		
		 @JsonProperty("lmd_id")
		public String getLmd_id() {
			return lmd_id;
		}
		 public void setLmd_id(String lmd_id) {
				this.lmd_id = lmd_id;
			}
		    
		
		   @JsonProperty("store")
		public String getStore() {
			return store;
		}
		public void setStore(String store) {
			this.store = store;
		}
		
		   @JsonProperty("offer text")
		public String getOffer_text() {
			return offer_text;
		}
		public void setOffer_text(String offer_text) {
			this.offer_text = offer_text;
		}
		
		@JsonProperty("title")
		public String getOffers__title() {
			return offers__title;
		}
		public void setOffers__title(String offers__title) {
			this.offers__title = offers__title;
		}
		
		   @JsonProperty("description")
		public String getOffers__description() {
			return offers__description;
		}
		public void setOffers__description(String offers__description) {
			this.offers__description = offers__description;
		}
		
		   @JsonProperty("categories")
		public String getOffers__categories() {
			return offers__categories;
		}
		public void setOffers__categories(String offers__categories) {
			this.offers__categories = offers__categories;
		}
		
		 @JsonProperty("url")
		public String getOffers__url() {
			return offers__url;
		}
		 
		 
		public void setOffers__url(String offers__url) {
			this.offers__url = offers__url;
		}
		   @JsonProperty("start_date")
		public String getOffers__start_date() {
			return offers__start_date;
		}
		public void setOffers__start_date(String offers__start_date) {
			this.offers__start_date = offers__start_date;
		}
		
		   @JsonProperty("end_date")
		public String getOffers__end_date() {
			return offers__end_date;
		}
		
		public void setOffers__end_date(String offers__end_date) {
			this.offers__end_date = offers__end_date;
		}
		
		   @JsonProperty("code")
		public String getCode() {
			return code;
		}
		
		public void setCode(String code) {
			this.code = code;
		}
		   @JsonProperty("terms_and_conditions")
		public String getTerms_and_conditions() {
			return terms_and_conditions;
		}
		public void setTerms_and_conditions(String terms_and_conditions) {
			this.terms_and_conditions = terms_and_conditions;
		}
		
		   @JsonProperty("featured")
		public String getFeatured() {
			return featured;
		}
		public void setFeatured(String featured) {
			this.featured = featured;
		}
		
		   @JsonProperty("smartLink")
		public String getSmartLink() {
			return smartLink;
		}
		public void setSmartLink(String smartLink) {
			this.smartLink = smartLink;
		}
		   @JsonProperty("image_url")
		public String getImage_url() {
			return image_url;
		}
		public void setImage_url(String image_url) {
			this.image_url = image_url;
		}
		
		   @JsonProperty("type")
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		
		   @JsonProperty("offer")
		public String getOffer() {
			return offer;
		}
		public void setOffer(String offer) {
			this.offer = offer;
		}
		
		   @JsonProperty("offer value")
		public String getOffer_value() {
			return offer_value;
		}
		public void setOffer_value(String offer_value) {
			this.offer_value = offer_value;
		}
		
		   @JsonProperty("status")
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		
		// @JsonProperty("id")
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getPartnermapname() {
			return partnermapname;
		}
		public void setPartnermapname(String partnermapname) {
			this.partnermapname = partnermapname;
		}
		
		

}
