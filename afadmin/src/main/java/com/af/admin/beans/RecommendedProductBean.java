package com.af.admin.beans;

import java.util.List;

public class RecommendedProductBean {
private Integer rpNo;
private Integer prtNo;
private Integer prtCatNo;
private Integer prtPrdNo;
private String prtName;
private String rpId;
private String rpName;
private String pcName;
private String activationDate;
private String deactivationDate;
private int position;
private String rpStatus;
private String status;
private int statusCode;
private String statusClass;
private List<PartnerCategoryBean> partnerCategoryBean; 
public Integer getRpNo() {
	return rpNo;
}
public void setRpNo(Integer rpNo) {
	this.rpNo = rpNo;
}

public Integer getPrtNo() {
	return prtNo;
}
public void setPrtNo(Integer prtNo) {
	this.prtNo = prtNo;
}
public Integer getPrtCatNo() {
	return prtCatNo;
}
public void setPrtCatNo(Integer prtCatNo) {
	this.prtCatNo = prtCatNo;
}
public Integer getPrtPrdNo() {
	return prtPrdNo;
}
public void setPrtPrdNo(Integer prtPrdNo) {
	this.prtPrdNo = prtPrdNo;
}
public String getPrtName() {
	return prtName;
}
public void setPrtName(String prtName) {
	this.prtName = prtName;
}
public String getRpId() {
	return rpId;
}
public void setRpId(String rpId) {
	this.rpId = rpId;
}
public String getRpName() {
	return rpName;
}
public void setRpName(String rpName) {
	this.rpName = rpName;
}

public String getPcName() {
	return pcName;
}
public void setPcName(String pcName) {
	this.pcName = pcName;
}
public String getActivationDate() {
	return activationDate;
}
public void setActivationDate(String activationDate) {
	this.activationDate = activationDate;
}
public String getDeactivationDate() {
	return deactivationDate;
}
public void setDeactivationDate(String deactivationDate) {
	this.deactivationDate = deactivationDate;
}
public int getPosition() {
	return position;
}
public void setPosition(int position) {
	this.position = position;
}

public String getRpStatus() {
	return rpStatus;
}
public void setRpStatus(String rpStatus) {
	this.rpStatus = rpStatus;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public int getStatusCode() {
	return statusCode;
}
public void setStatusCode(int statusCode) {
	this.statusCode = statusCode;
}
public String getStatusClass() {
	return statusClass;
}
public void setStatusClass(String statusClass) {
	this.statusClass = statusClass;
}
public List<PartnerCategoryBean> getPartnerCategoryBean() {
	return partnerCategoryBean;
}
public void setPartnerCategoryBean(List<PartnerCategoryBean> partnerCategoryBean) {
	this.partnerCategoryBean = partnerCategoryBean;
}


}
