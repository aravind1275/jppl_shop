package com.af.admin.beans;

public class UserBean {
	private Integer userId;
	private String firstName;
	private String lastName;
	private String userName;
	private String mobileNumber;
	private String password;
	private String role;
	private Integer roleId;
	private String lastLogin;
	private String userStatus;
	private String userStatusClass;
	private int userStatusCode;
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	public String getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(String lastLogin) {
		this.lastLogin = lastLogin;
	}
	public String getUserStatus() {
		return userStatus;
	}
	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}
	public int getUserStatusCode() {
		return userStatusCode;
	}
	public void setUserStatusCode(int userStatusCode) {
		this.userStatusCode = userStatusCode;
	}
	public String getUserStatusClass() {
		return userStatusClass;
	}
	public void setUserStatusClass(String userStatusClass) {
		this.userStatusClass = userStatusClass;
	}

}
