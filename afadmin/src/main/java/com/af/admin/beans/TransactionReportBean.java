package com.af.admin.beans;


public class TransactionReportBean {
	
	
	private String inputMemberId;
	private String transactionDate;
	private String partnerName;
	private String partnerCategoryName;
	private String partnerProductName;
	private Float  amount;
	private Integer miles;
	private String milesPostingDate;
	private String expectedCreditDate;
	private String transactionStatus;
	
	public String getInputMemberId() {
		return inputMemberId;
	}
	public void setInputMemberId(String inputMemberId) {
		this.inputMemberId = inputMemberId;
	}	
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	public String getPartnerCategoryName() {
		return partnerCategoryName;
	}
	public void setPartnerCategoryName(String partnerCategoryName) {
		this.partnerCategoryName = partnerCategoryName;
	}
	public String getPartnerProductName() {
		return partnerProductName;
	}
	public void setPartnerProductName(String partnerProductName) {
		this.partnerProductName = partnerProductName;
	}
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	public Float getAmount() {
		return amount;
	}
	public void setAmount(Float amount) {
		this.amount = amount;
	}
	public Integer getMiles() {
		return miles;
	}
	public void setMiles(Integer miles) {
		this.miles = miles;
	}
	public String getMilesPostingDate() {
		return milesPostingDate;
	}
	public void setMilesPostingDate(String milesPostingDate) {
		this.milesPostingDate = milesPostingDate;
	}
	public String getExpectedCreditDate() {
		return expectedCreditDate;
	}
	public void setExpectedCreditDate(String expectedCreditDate) {
		this.expectedCreditDate = expectedCreditDate;
	}
	public String getTransactionStatus() {
		return transactionStatus;
	}
	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}
	
	
	
}