/**
 * 
 */
package com.af.admin.beans;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jogeswarsahu
 *
 */
public class CategoryJSONBean {

	private String text; 
	private String id;
	List<CategoryJSONBean> children = new ArrayList<CategoryJSONBean>();
	
	CategoryAttributeJSONBean a_attr = new CategoryAttributeJSONBean();
	CategoryStateJSONBean state = new CategoryStateJSONBean();
	
	
	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}
	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the children
	 */
	public List<CategoryJSONBean> getChildren() {
		return children;
	}
	/**
	 * @param children the children to set
	 */
	public void setChildren(List<CategoryJSONBean> children) {
		this.children = children;
	}
	/**
	 * @return the a_attr
	 */
	public CategoryAttributeJSONBean getA_attr() {
		return a_attr;
	}
	/**
	 * @param a_attr the a_attr to set
	 */
	public void setA_attr(CategoryAttributeJSONBean a_attr) {
		this.a_attr = a_attr;
	}
	/**
	 * @return the state
	 */
	public CategoryStateJSONBean getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(CategoryStateJSONBean state) {
		this.state = state;
	}
	
	
}
