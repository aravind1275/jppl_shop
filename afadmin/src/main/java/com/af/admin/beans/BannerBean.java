package com.af.admin.beans;

import org.springframework.web.multipart.MultipartFile;

public class BannerBean {
private Integer bannerNo;
private String bannerId;
private String bannerName;
private String bannerImageUrl;
private String bannerLandingUrl;
private MultipartFile bannerImage;
private String bannerSections;
private String bannerStatus;
private String bannerStatusClass;
private int bannerStatusCode;
private int bannerSectionsCode;

private String partNo;
private String partnerName;

private String fromDate;
private String toDate;

private String utmSourceName;
private String utmSourceValue;
private String utmMediumName;
private String utmMediumValue;
private String utmCampaignName;
private String utmCampaignValue;




public Integer getBannerNo() {
	return bannerNo;
}
public void setBannerNo(Integer bannerNo) {
	this.bannerNo = bannerNo;
}

public String getBannerId() {
	return bannerId;
}
public void setBannerId(String bannerId) {
	this.bannerId = bannerId;
}
public String getBannerName() {
	return bannerName;
}
public void setBannerName(String bannerName) {
	this.bannerName = bannerName;
}
public String getBannerImageUrl() {
	return bannerImageUrl;
}
public void setBannerImageUrl(String bannerImageUrl) {
	this.bannerImageUrl = bannerImageUrl;
}
public String getBannerLandingUrl() {
	return bannerLandingUrl;
}
public void setBannerLandingUrl(String bannerLandingUrl) {
	this.bannerLandingUrl = bannerLandingUrl;
}
public String getBannerSections() {
	return bannerSections;
}
public void setBannerSections(String bannerSections) {
	this.bannerSections = bannerSections;
}
public String getBannerStatus() {
	return bannerStatus;
}
public void setBannerStatus(String bannerStatus) {
	this.bannerStatus = bannerStatus;
}
public String getBannerStatusClass() {
	return bannerStatusClass;
}
public void setBannerStatusClass(String bannerStatusClass) {
	this.bannerStatusClass = bannerStatusClass;
}
public int getBannerStatusCode() {
	return bannerStatusCode;
}
public void setBannerStatusCode(int bannerStatusCode) {
	this.bannerStatusCode = bannerStatusCode;
}

public int getBannerSectionsCode() {
	return bannerSectionsCode;
}
public void setBannerSectionsCode(int bannerSectionsCode) {
	this.bannerSectionsCode = bannerSectionsCode;
}
public MultipartFile getBannerImage() {
	return bannerImage;
}
public void setBannerImage(MultipartFile bannerImage) {
	this.bannerImage = bannerImage;
}

public String getPartNo() {
	return partNo;
}

public void setPartNo(String partNo) {
	this.partNo = partNo;
}

public String getPartnerName() {
	return partnerName;
}

public void setPartnerName(String partnerName) {
	this.partnerName = partnerName;
}
/**
 * @return the fromDate
 */
public String getFromDate() {
	return fromDate;
}
/**
 * @param fromDate the fromDate to set
 */
public void setFromDate(String fromDate) {
	this.fromDate = fromDate;
}
/**
 * @return the toDate
 */
public String getToDate() {
	return toDate;
}
/**
 * @param toDate the toDate to set
 */
public void setToDate(String toDate) {
	this.toDate = toDate;
}
public String getUtmSourceName() {
	return utmSourceName;
}
public void setUtmSourceName(String utmSourceName) {
	this.utmSourceName = utmSourceName;
}
public String getUtmSourceValue() {
	return utmSourceValue;
}
public void setUtmSourceValue(String utmSourceValue) {
	this.utmSourceValue = utmSourceValue;
}
public String getUtmMediumName() {
	return utmMediumName;
}
public void setUtmMediumName(String utmMediumName) {
	this.utmMediumName = utmMediumName;
}
public String getUtmMediumValue() {
	return utmMediumValue;
}
public void setUtmMediumValue(String utmMediumValue) {
	this.utmMediumValue = utmMediumValue;
}
public String getUtmCampaignName() {
	return utmCampaignName;
}
public void setUtmCampaignName(String utmCampaignName) {
	this.utmCampaignName = utmCampaignName;
}
public String getUtmCampaignValue() {
	return utmCampaignValue;
}
public void setUtmCampaignValue(String utmCampaignValue) {
	this.utmCampaignValue = utmCampaignValue;
}



}
