package com.af.admin.beans;

import java.util.Date;

public class JPMilesStatusUpdateBean {
	private String jpNumber;
	private String partnerId;
	private String transactionId;
	private Date transactionDate;
	private String partnerName;
	private String partnerCategoryName;
	private String productName;
	private String price;
	private String ratePerMile;
	private String commissionRate;
	private String memberShare;
	private String jpmilesPurchase;
	private Date expectedDateOfCredit;
	private String jpmilesCancel;
	private String jpmilesToCredit;
	private String tax;
	private String formatedTransactionDate;
	private String formatedExpectedDateOfCredit;
	private byte productStatus;
	private String product_Status;
	private String partnerProductId;	
	private String fromDate;
	private String toDate;
	private String brandName;	
	private String productID;
	private String categoryName;
	private Integer masterPID;
	private String selectBy;
	private String selectByValue;
	private String productLandingURL;
	private String productImageURL;
	private String prtNo;
	private Long orderDateTime;
	private Long expectedDateOfCreditTime;	
	private String purchase_status;	
	private String source;
	private String medium;
	private String campaign;
	private byte memberOrGuest;
	private String member_or_guest;
	private String partnerItemId;
	//added
	private String orderId;
	private byte jpmilesStatus;
	private String jpmiles_Status;
	

	private boolean isRequest;
	private boolean isResponse;
	private byte transactionType;
	private String transactionVia;
	private String commAmount;
	private String jpplCommAmount;
	private String jpplCategoryName;
	private String emailStatus;
	private String smsStatus;
	private boolean isXml;
	private Long orderCount;
	private String bonusJpmiles;
	private String message;
	private String productId;
	private String trackingId;
	
	public JPMilesStatusUpdateBean() {
		super();
	}

	public JPMilesStatusUpdateBean(String jpNumber,  String partnerId, String transactionId,
			Date transactionDate, String partnerName, String partnerCategoryName, String productName, String price,
			String ratePerMile, String commissionRate, String memberShare, String jpmilesPurchase,
			Date expectedDateOfCredit, String jpmilesCancel, String jpmilesToCredit, String tax,
			String formatedTransactionDate, String formatedExpectedDateOfCredit, byte productStatus,
			String product_Status, String partnerProductId, String fromDate, String toDate, String brandName,
			String productID, String categoryName, Integer masterPID, String selectBy, String selectByValue,
			String productLandingURL, String productImageURL, String prtNo, Long orderDateTime,
			Long expectedDateOfCreditTime, String purchase_status, String source, String medium, String campaign,
			byte memberOrGuest, String member_or_guest, String partnerItemId, String orderId, byte jpmilesStatus,
			String jpmiles_Status, boolean isRequest, boolean isResponse, byte transactionType, String transactionVia,
			String commAmount, String jpplCommAmount, String jpplCategoryName, String emailStatus, String smsStatus,
			boolean isXml, Long orderCount, String bonusJpmiles, String message, String productId2, String trackingId) {
		super();
		this.jpNumber = jpNumber;
		this.partnerId = partnerId;
		this.transactionId = transactionId;
		this.transactionDate = transactionDate;
		this.partnerName = partnerName;
		this.partnerCategoryName = partnerCategoryName;
		this.productName = productName;
		this.price = price;
		this.ratePerMile = ratePerMile;
		this.commissionRate = commissionRate;
		this.memberShare = memberShare;
		this.jpmilesPurchase = jpmilesPurchase;
		this.expectedDateOfCredit = expectedDateOfCredit;
		this.jpmilesCancel = jpmilesCancel;
		this.jpmilesToCredit = jpmilesToCredit;
		this.tax = tax;
		this.formatedTransactionDate = formatedTransactionDate;
		this.formatedExpectedDateOfCredit = formatedExpectedDateOfCredit;
		this.productStatus = productStatus;
		this.product_Status = product_Status;
		this.partnerProductId = partnerProductId;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.brandName = brandName;
		this.productID = productID;
		this.categoryName = categoryName;
		this.masterPID = masterPID;
		this.selectBy = selectBy;
		this.selectByValue = selectByValue;
		this.productLandingURL = productLandingURL;
		this.productImageURL = productImageURL;
		this.prtNo = prtNo;
		this.orderDateTime = orderDateTime;
		this.expectedDateOfCreditTime = expectedDateOfCreditTime;
		this.purchase_status = purchase_status;
		this.source = source;
		this.medium = medium;
		this.campaign = campaign;
		this.memberOrGuest = memberOrGuest;
		this.member_or_guest = member_or_guest;
		this.partnerItemId = partnerItemId;
		this.orderId = orderId;
		this.jpmilesStatus = jpmilesStatus;
		this.jpmiles_Status = jpmiles_Status;
		this.isRequest = isRequest;
		this.isResponse = isResponse;
		this.transactionType = transactionType;
		this.transactionVia = transactionVia;
		this.commAmount = commAmount;
		this.jpplCommAmount = jpplCommAmount;
		this.jpplCategoryName = jpplCategoryName;
		this.emailStatus = emailStatus;
		this.smsStatus = smsStatus;
		this.isXml = isXml;
		this.orderCount = orderCount;
		this.bonusJpmiles = bonusJpmiles;
		this.message = message;
		productId = productId2;
		this.trackingId = trackingId;
	}

	public String getJpNumber() {
		return jpNumber;
	}

	public void setJpNumber(String jpNumber) {
		this.jpNumber = jpNumber;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getPartnerCategoryName() {
		return partnerCategoryName;
	}

	public void setPartnerCategoryName(String partnerCategoryName) {
		this.partnerCategoryName = partnerCategoryName;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getRatePerMile() {
		return ratePerMile;
	}

	public void setRatePerMile(String ratePerMile) {
		this.ratePerMile = ratePerMile;
	}

	public String getCommissionRate() {
		return commissionRate;
	}

	public void setCommissionRate(String commissionRate) {
		this.commissionRate = commissionRate;
	}

	public String getMemberShare() {
		return memberShare;
	}

	public void setMemberShare(String memberShare) {
		this.memberShare = memberShare;
	}

	public String getJpmilesPurchase() {
		return jpmilesPurchase;
	}

	public void setJpmilesPurchase(String jpmilesPurchase) {
		this.jpmilesPurchase = jpmilesPurchase;
	}

	public Date getExpectedDateOfCredit() {
		return expectedDateOfCredit;
	}

	public void setExpectedDateOfCredit(Date expectedDateOfCredit) {
		this.expectedDateOfCredit = expectedDateOfCredit;
	}

	public String getJpmilesCancel() {
		return jpmilesCancel;
	}

	public void setJpmilesCancel(String jpmilesCancel) {
		this.jpmilesCancel = jpmilesCancel;
	}

	public String getJpmilesToCredit() {
		return jpmilesToCredit;
	}

	public void setJpmilesToCredit(String jpmilesToCredit) {
		this.jpmilesToCredit = jpmilesToCredit;
	}

	public String getTax() {
		return tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}

	public String getFormatedTransactionDate() {
		return formatedTransactionDate;
	}

	public void setFormatedTransactionDate(String formatedTransactionDate) {
		this.formatedTransactionDate = formatedTransactionDate;
	}

	public String getFormatedExpectedDateOfCredit() {
		return formatedExpectedDateOfCredit;
	}

	public void setFormatedExpectedDateOfCredit(String formatedExpectedDateOfCredit) {
		this.formatedExpectedDateOfCredit = formatedExpectedDateOfCredit;
	}

	public byte getProductStatus() {
		return productStatus;
	}

	public void setProductStatus(byte productStatus) {
		this.productStatus = productStatus;
	}

	public String getProduct_Status() {
		return product_Status;
	}

	public void setProduct_Status(String product_Status) {
		this.product_Status = product_Status;
	}

	public String getPartnerProductId() {
		return partnerProductId;
	}

	public void setPartnerProductId(String partnerProductId) {
		this.partnerProductId = partnerProductId;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getProductID() {
		return productID;
	}

	public void setProductID(String productID) {
		this.productID = productID;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Integer getMasterPID() {
		return masterPID;
	}

	public void setMasterPID(Integer masterPID) {
		this.masterPID = masterPID;
	}

	public String getSelectBy() {
		return selectBy;
	}

	public void setSelectBy(String selectBy) {
		this.selectBy = selectBy;
	}

	public String getSelectByValue() {
		return selectByValue;
	}

	public void setSelectByValue(String selectByValue) {
		this.selectByValue = selectByValue;
	}

	public String getProductLandingURL() {
		return productLandingURL;
	}

	public void setProductLandingURL(String productLandingURL) {
		this.productLandingURL = productLandingURL;
	}

	public String getProductImageURL() {
		return productImageURL;
	}

	public void setProductImageURL(String productImageURL) {
		this.productImageURL = productImageURL;
	}

	public String getPrtNo() {
		return prtNo;
	}

	public void setPrtNo(String prtNo) {
		this.prtNo = prtNo;
	}

	public Long getOrderDateTime() {
		return orderDateTime;
	}

	public void setOrderDateTime(Long orderDateTime) {
		this.orderDateTime = orderDateTime;
	}

	public Long getExpectedDateOfCreditTime() {
		return expectedDateOfCreditTime;
	}

	public void setExpectedDateOfCreditTime(Long expectedDateOfCreditTime) {
		this.expectedDateOfCreditTime = expectedDateOfCreditTime;
	}

	public String getPurchase_status() {
		return purchase_status;
	}

	public void setPurchase_status(String purchase_status) {
		this.purchase_status = purchase_status;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getMedium() {
		return medium;
	}

	public void setMedium(String medium) {
		this.medium = medium;
	}

	public String getCampaign() {
		return campaign;
	}

	public void setCampaign(String campaign) {
		this.campaign = campaign;
	}

	public byte getMemberOrGuest() {
		return memberOrGuest;
	}

	public void setMemberOrGuest(byte memberOrGuest) {
		this.memberOrGuest = memberOrGuest;
	}

	public String getMember_or_guest() {
		return member_or_guest;
	}

	public void setMember_or_guest(String member_or_guest) {
		this.member_or_guest = member_or_guest;
	}

	public String getPartnerItemId() {
		return partnerItemId;
	}

	public void setPartnerItemId(String partnerItemId) {
		this.partnerItemId = partnerItemId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public byte getJpmilesStatus() {
		return jpmilesStatus;
	}

	public void setJpmilesStatus(byte jpmilesStatus) {
		this.jpmilesStatus = jpmilesStatus;
	}

	public String getJpmiles_Status() {
		return jpmiles_Status;
	}

	public void setJpmiles_Status(String jpmiles_Status) {
		this.jpmiles_Status = jpmiles_Status;
	}

	public boolean isRequest() {
		return isRequest;
	}

	public void setRequest(boolean isRequest) {
		this.isRequest = isRequest;
	}

	public boolean isResponse() {
		return isResponse;
	}

	public void setResponse(boolean isResponse) {
		this.isResponse = isResponse;
	}

	public byte getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(byte transactionType) {
		this.transactionType = transactionType;
	}

	public String getTransactionVia() {
		return transactionVia;
	}

	public void setTransactionVia(String transactionVia) {
		this.transactionVia = transactionVia;
	}

	public String getCommAmount() {
		return commAmount;
	}

	public void setCommAmount(String commAmount) {
		this.commAmount = commAmount;
	}

	public String getJpplCommAmount() {
		return jpplCommAmount;
	}

	public void setJpplCommAmount(String jpplCommAmount) {
		this.jpplCommAmount = jpplCommAmount;
	}

	public String getJpplCategoryName() {
		return jpplCategoryName;
	}

	public void setJpplCategoryName(String jpplCategoryName) {
		this.jpplCategoryName = jpplCategoryName;
	}

	public String getEmailStatus() {
		return emailStatus;
	}

	public void setEmailStatus(String emailStatus) {
		this.emailStatus = emailStatus;
	}

	public String getSmsStatus() {
		return smsStatus;
	}

	public void setSmsStatus(String smsStatus) {
		this.smsStatus = smsStatus;
	}

	public boolean isXml() {
		return isXml;
	}

	public void setXml(boolean isXml) {
		this.isXml = isXml;
	}

	public Long getOrderCount() {
		return orderCount;
	}

	public void setOrderCount(Long orderCount) {
		this.orderCount = orderCount;
	}

	public String getBonusJpmiles() {
		return bonusJpmiles;
	}

	public void setBonusJpmiles(String bonusJpmiles) {
		this.bonusJpmiles = bonusJpmiles;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getTrackingId() {
		return trackingId;
	}

	public void setTrackingId(String trackingId) {
		this.trackingId = trackingId;
	}
	
	
	
}
