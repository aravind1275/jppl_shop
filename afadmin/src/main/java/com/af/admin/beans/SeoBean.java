package com.af.admin.beans;

public class SeoBean {

	private Byte seoPageId;
	private String title;
	private String metaDesc;
	private String metaTag;
	private String headerText;
	
	public Byte getSeoPageId() {
		return seoPageId;
	}
	public void setSeoPageId(Byte seoPageId) {
		this.seoPageId = seoPageId;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getMetaDesc() {
		return metaDesc;
	}
	public void setMetaDesc(String metaDesc) {
		this.metaDesc = metaDesc;
	}
	
	public String getMetaTag() {
		return metaTag;
	}
	public void setMetaTag(String metaTag) {
		this.metaTag = metaTag;
	}
	
	public String getHeaderText() {
		return headerText;
	}
	public void setHeaderText(String headerText) {
		this.headerText = headerText;
	}
}