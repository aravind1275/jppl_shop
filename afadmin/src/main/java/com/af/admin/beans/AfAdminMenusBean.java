package com.af.admin.beans;

public class AfAdminMenusBean {
	
	private Integer menuId;
	private String title;
	private String url;
	private int parentMenuId;
	
	public AfAdminMenusBean(Integer menuId, String title, String url,
			int parentMenuId) {
		super();
		this.menuId = menuId;
		this.title = title;
		this.url = url;
		this.parentMenuId = parentMenuId;
	}
	public Integer getMenuId() {
		return menuId;
	}
	public void setMenuId(Integer menuId) {
		this.menuId = menuId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getParentMenuId() {
		return parentMenuId;
	}
	public void setParentMenuId(int parentMenuId) {
		this.parentMenuId = parentMenuId;
	}
	
	

}

