package com.af.admin.beans;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)

public class OffersAndDealsJson {
	@JsonProperty("result")
	private String result;
	@JsonProperty("offers")
    private List<offersBean> offersBean = null;

    
	 
	 public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public List<offersBean> getOffersBean() {
		return offersBean;
	}

	public void setOffersBean(List<offersBean> offersBean) {
		this.offersBean = offersBean;
	}

	
		

		
}
