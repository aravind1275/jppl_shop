package com.af.admin.beans;

import java.util.Date;

public class ExportBean {

	
	 private String expectedCreditDate;
	    private String partnerID;
	    private String transactionDate;
		private String productStatus;
	        private String createdTime;
	        private String price;
		private String jpmilesToCredit;
	        private Date expectedDateOfCredit;
	        private String status;
	        private String partnercategoryName;
	        
	       private String partnerCategoryName;
	       
	       
	       public String getTransactionDate() {
			return transactionDate;
		}
		public void setTransactionDate(String transactionDate) {
			this.transactionDate = transactionDate;
		}
	       
	       
	       
	     public String getStatus() {
			return this.status;
		}

		public void setStatus(String status) {
			this.status = status;
		}
	    
	     public Date getExpectedDateOfCredit() {
			return expectedDateOfCredit;
		}

		public void setExpectedDateOfCredit(Date expectedDateOfCredit) {
			this.expectedDateOfCredit = expectedDateOfCredit;
		}
	    
	    
	         public String getJpmilesToCredit() {
			return jpmilesToCredit;
		}

		public void setJpmilesToCredit(String jpmilesToCredit) {
			this.jpmilesToCredit = jpmilesToCredit;
		}
	    
	    
	    
	     public String getPrice() {
			return this.price;
		}

		public void setPrice(String price) {
			this.price = price;
		}
	    
	     public String getCreatedTime() {
			return this.createdTime;
		}

		public void setCreatedTime(String createdTime) {
			this.createdTime = createdTime;
		}
	    
	    
	     public String getProductStatus() {
			return productStatus;
		}
		public void setProductStatus(String productStatus) {
			this.productStatus = productStatus;
		}
	    
	    
	     public String getExpectedCreditDate() {
			return expectedCreditDate;
		}
		public void setExpectedCreditDate(String expectedCreditDate) {
			this.expectedCreditDate = expectedCreditDate;
		}
	    
	     public String getPartnerID() {
			return partnerID;
		}
		public void setPartnerID(String partnerID) {
			this.partnerID = partnerID;
		}
	    
	     public String getPartnercategoryName() {
			return partnercategoryName;
		}
		public void setPartnercategoryName(String partnercategoryName) {
			this.partnercategoryName = partnercategoryName;
		}
	    
	        
	         public String getPartnerCategoryName() {
			return partnerCategoryName;
		}
		public void setPartnerCategoryName(String partnerCategoryName) {
			this.partnerCategoryName = partnerCategoryName;
		}
	        
	       
}
