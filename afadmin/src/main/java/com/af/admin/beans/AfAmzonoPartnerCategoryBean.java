package com.af.admin.beans;

import java.util.Date;

public class AfAmzonoPartnerCategoryBean {
	
	private Integer amzCatNo;
	private String prtCatName;
	private Integer prtCatNo;
	private String prtName;
	private Integer prtNo;
	private String amzCategoryId;
	private String amzOrderCategoryName;
	private Date createdTime;
	private Integer createdBy;
	private Date modifiedTime;
	private Integer modifiedBy;
	private byte status;
	private String amazonCategoryStatus;
	private String  amazonCategoryStatusClass;
	private int amazonCategoryStatusCode;
	
	
	public Integer getAmzCatNo() {
		return amzCatNo;
	}
	public void setAmzCatNo(Integer amzCatNo) {
		this.amzCatNo = amzCatNo;
	}
	
	public String getPrtCatName() {
		return prtCatName;
	}
	public void setPrtCatName(String prtCatName) {
		this.prtCatName = prtCatName;
	}
	
	public String getPrtName() {
		return prtName;
	}
	public void setPrtName(String prtName) {
		this.prtName = prtName;
	}
	public String getAmzCategoryId() {
		return amzCategoryId;
	}
	public void setAmzCategoryId(String amzCategoryId) {
		this.amzCategoryId = amzCategoryId;
	}
	
	public String getAmzOrderCategoryName() {
		return amzOrderCategoryName;
	}
	public void setAmzOrderCategoryName(String amzOrderCategoryName) {
		this.amzOrderCategoryName = amzOrderCategoryName;
	}
	public Date getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Date getModifiedTime() {
		return modifiedTime;
	}
	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	public Integer getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public byte getStatus() {
		return status;
	}
	public void setStatus(byte status) {
		this.status = status;
	}
	public String getAmazonCategoryStatus() {
		return amazonCategoryStatus;
	}
	public void setAmazonCategoryStatus(String amazonCategoryStatus) {
		this.amazonCategoryStatus = amazonCategoryStatus;
	}
	public String getAmazonCategoryStatusClass() {
		return amazonCategoryStatusClass;
	}
	public void setAmazonCategoryStatusClass(String amazonCategoryStatusClass) {
		this.amazonCategoryStatusClass = amazonCategoryStatusClass;
	}
	public int getAmazonCategoryStatusCode() {
		return amazonCategoryStatusCode;
	}
	public void setAmazonCategoryStatusCode(int amazonCategoryStatusCode) {
		this.amazonCategoryStatusCode = amazonCategoryStatusCode;
	}
	public Integer getPrtCatNo() {
		return prtCatNo;
	}
	public void setPrtCatNo(Integer prtCatNo) {
		this.prtCatNo = prtCatNo;
	}
	public Integer getPrtNo() {
		return prtNo;
	}
	public void setPrtNo(Integer prtNo) {
		this.prtNo = prtNo;
	}
	
	
	

}
