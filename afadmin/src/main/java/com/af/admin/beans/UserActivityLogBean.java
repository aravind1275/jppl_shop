package com.af.admin.beans;

import java.util.Date;

public class UserActivityLogBean {

	
	private String userActFromDate;
	private String userActTodate;
	//list
	private Integer userActId;
	private String userActivity;
	private String userOldFieldValue;
	private String userNewFieldValue;
	private Date userActCreatedDateTime;
	private String userActCreatedBy;
	private String userActIpVal;
	private String userActModule;
	
	
	
	public UserActivityLogBean() {
		
	}


	public UserActivityLogBean(Integer userActId, String userActivity, String userOldFieldValue,
			String userNewFieldValue, Date userActCreatedDateTime, String userActCreatedBy, String userActIpVal, String userActModule) {
		
		this.userActId = userActId;
		this.userActivity = userActivity;
		this.userOldFieldValue = userOldFieldValue;
		this.userNewFieldValue = userNewFieldValue;
		this.userActCreatedDateTime = userActCreatedDateTime;
		this.userActCreatedBy = userActCreatedBy;
		this.userActIpVal = userActIpVal;
		this.userActModule = userActModule;
	}
	
	
	public String getUserActFromDate() {
		return userActFromDate;
	}
	public void setUserActFromDate(String userActFromDate) {
		this.userActFromDate = userActFromDate;
	}
	public String getUserActTodate() {
		return userActTodate;
	}
	public void setUserActTodate(String userActTodate) {
		this.userActTodate = userActTodate;
	}
	public Integer getUserActId() {
		return userActId;
	}
	public void setUserActId(Integer userActId) {
		this.userActId = userActId;
	}
	public String getUserActivity() {
		return userActivity;
	}
	public void setUserActivity(String userActivity) {
		this.userActivity = userActivity;
	}
	public String getUserOldFieldValue() {
		return userOldFieldValue;
	}
	public void setUserOldFieldValue(String userOldFieldValue) {
		this.userOldFieldValue = userOldFieldValue;
	}
	public String getUserNewFieldValue() {
		return userNewFieldValue;
	}
	public void setUserNewFieldValue(String userNewFieldValue) {
		this.userNewFieldValue = userNewFieldValue;
	}
	public Date getUserActCreatedDateTime() {
		return userActCreatedDateTime;
	}
	public void setUserActCreatedDateTime(Date userActCreatedDateTime) {
		this.userActCreatedDateTime = userActCreatedDateTime;
	}
	public String getUserActCreatedBy() {
		return userActCreatedBy;
	}
	public void setUserActCreatedBy(String userActCreatedBy) {
		this.userActCreatedBy = userActCreatedBy;
	}
	public String getUserActIpVal() {
		return userActIpVal;
	}
	public void setUserActIpVal(String userActIpVal) {
		this.userActIpVal = userActIpVal;
	}

	public String getUserActModule() {
		return userActModule;
	}

	public void setUserActModule(String userActModule) {
		this.userActModule = userActModule;
	}
	
}
