package com.af.admin.beans;

public class CommissionBean {

	private Integer prtCommNo;
	private String partNo;
	private String partnerName;
	
	private String prtCatNo;
	private String prtCategoryName;
	
	private String prtProductNo;
	private String prtProductName;
	
	private String commissionRate;
	private String memberShare;
	private String activationDate;
	private String deactivationDate;
	
	private String commissionSatus;
	private String commissionStatusClass;
	private int commissionStatusCode;
	
	private String productBrand;
	private String categoryPath;
	
	private String maxJpmiles;
	
	private byte commisionType;
	private String lowerValue;
	private String upperValue;
	
	private byte bonusCommissionType;
	private String bonusCommissionRate;
	private String bonusPartnerCode;
	private String bonusActivityCode;
	private byte transactionStatus;
	
	public Integer getPrtCommNo() {
		return prtCommNo;
	}
	public void setPrtCommNo(Integer prtCommNo) {
		this.prtCommNo = prtCommNo;
	}
	
	public String getPartNo() {
		return partNo;
	}
	public void setPartNo(String partNo) {
		this.partNo = partNo;
	}
	
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	
	public String getPrtCatNo() {
		return prtCatNo;
	}
	public void setPrtCatNo(String prtCatNo) {
		this.prtCatNo = prtCatNo;
	}
	
	public String getPrtCategoryName() {
		return prtCategoryName;
	}
	public void setPrtCategoryName(String prtCategoryName) {
		this.prtCategoryName = prtCategoryName;
	}
	
	public String getPrtProductNo() {
		return prtProductNo;
	}
	public void setPrtProductNo(String prtProductNo) {
		this.prtProductNo = prtProductNo;
	}
	
	public String getPrtProductName() {
		return prtProductName;
	}
	public void setPrtProductName(String prtProductName) {
		this.prtProductName = prtProductName;
	}
	
	public String getCommissionRate() {
		return commissionRate;
	}
	public void setCommissionRate(String commissionRate) {
		this.commissionRate = commissionRate;
	}
	
	public String getMemberShare() {
		return memberShare;
	}
	public void setMemberShare(String memberShare) {
		this.memberShare = memberShare;
	}
	
	public String getActivationDate() {
		return activationDate;
	}
	public void setActivationDate(String activationDate) {
		this.activationDate = activationDate;
	}
	
	public String getDeactivationDate() {
		return deactivationDate;
	}
	public void setDeactivationDate(String deactivationDate) {
		this.deactivationDate = deactivationDate;
	}
	
	public String getCommissionSatus() {
		return commissionSatus;
	}
	public void setCommissionSatus(String commissionSatus) {
		this.commissionSatus = commissionSatus;
	}
	
	public String getCommissionStatusClass() {
		return commissionStatusClass;
	}
	public void setCommissionStatusClass(String commissionStatusClass) {
		this.commissionStatusClass = commissionStatusClass;
	}
	
	public int getCommissionStatusCode() {
		return commissionStatusCode;
	}
	public void setCommissionStatusCode(int commissionStatusCode) {
		this.commissionStatusCode = commissionStatusCode;
	}
	
	public String getProductBrand() {
		return productBrand;
	}
	public void setProductBrand(String productBrand) {
		this.productBrand = productBrand;
	}
	
	public String getCategoryPath() {
		return categoryPath;
	}
	public void setCategoryPath(String categoryPath) {
		this.categoryPath = categoryPath;
	}

	public String getMaxJpmiles() {
		return maxJpmiles;
	}
	public void setMaxJpmiles(String maxJpmiles) {
		this.maxJpmiles = maxJpmiles;
	}
	public byte getCommisionType() {
		return commisionType;
	}
	public void setCommisionType(byte commisionType) {
		this.commisionType = commisionType;
	}
	public String getLowerValue() {
		return lowerValue;
	}
	public void setLowerValue(String lowerValue) {
		this.lowerValue = lowerValue;
	}
	public String getUpperValue() {
		return upperValue;
	}
	public void setUpperValue(String upperValue) {
		this.upperValue = upperValue;
	}
	
	public byte getBonusCommissionType() {
		return bonusCommissionType;
	}
	public void setBonusCommissionType(byte bonusCommissionType) {
		this.bonusCommissionType = bonusCommissionType;
	}
	
	public String getBonusCommissionRate() {
		return bonusCommissionRate;
	}
	public void setBonusCommissionRate(String bonusCommissionRate) {
		this.bonusCommissionRate = bonusCommissionRate;
	}
	
	public String getBonusPartnerCode() {
		return bonusPartnerCode;
	}
	public void setBonusPartnerCode(String bonusPartnerCode) {
		this.bonusPartnerCode = bonusPartnerCode;
	}
	
	public String getBonusActivityCode() {
		return bonusActivityCode;
	}
	public void setBonusActivityCode(String bonusActivityCode) {
		this.bonusActivityCode = bonusActivityCode;
	}
	public byte getTransactionStatus() {
		return transactionStatus;
	}
	public void setTransactionStatus(byte transactionStatus) {
		this.transactionStatus = transactionStatus;
	}
}
