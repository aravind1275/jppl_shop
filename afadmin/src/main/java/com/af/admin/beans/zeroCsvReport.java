package com.af.admin.beans;

public class zeroCsvReport {
	
	
	private String emailStatus;
	private String smsStatus;
	private String baseJpmiles;
	private String bonusJpmiles;
	private String afffiliateOrderId;
	private String transactionId;
	
	public zeroCsvReport() {
		
	}

	public zeroCsvReport(String emailStatus, String smsStatus, String jpmilesNumber, 
			String afffiliateOrderId, String transactionId) {
		super();
		this.emailStatus = emailStatus;
		this.smsStatus = smsStatus;
        this.baseJpmiles = baseJpmiles;
        this.bonusJpmiles =  bonusJpmiles;
		this.afffiliateOrderId = afffiliateOrderId;
		this.transactionId = transactionId;
	}

	public String getEmailStatus() {
		return emailStatus;
	}

	public void setEmailStatus(String emailStatus) {
		this.emailStatus = emailStatus;
	}

	public String getSmsStatus() {
		return smsStatus;
	}

	public void setSmsStatus(String smsStatus) {
		this.smsStatus = smsStatus;
	}


	public String getAfffiliateOrderId() {
		return afffiliateOrderId;
	}

	public void setAfffiliateOrderId(String afffiliateOrderId) {
		this.afffiliateOrderId = afffiliateOrderId;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getBaseJpmiles() {
		return baseJpmiles;
	}

	public void setBaseJpmiles(String baseJpmiles) {
		this.baseJpmiles = baseJpmiles;
	}

	public String getBonusJpmiles() {
		return bonusJpmiles;
	}

	public void setBonusJpmiles(String bonusJpmiles) {
		this.bonusJpmiles = bonusJpmiles;
	}
	
	
	
}