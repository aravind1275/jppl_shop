package com.af.admin.beans;

public class PartnerOrderStatusBean {
	
	private Integer id;
	private String prtNo;
	private String partnerName;
	private String partnerStatus;
	private String jpplStatus;
	private byte status;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPrtNo() {
		return prtNo;
	}
	public void setPrtNo(String prtNo) {
		this.prtNo = prtNo;
	}
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	public String getPartnerStatus() {
		return partnerStatus;
	}
	public void setPartnerStatus(String partnerStatus) {
		this.partnerStatus = partnerStatus;
	}
	public String getJpplStatus() {
		return jpplStatus;
	}
	public void setJpplStatus(String jpplStatus) {
		this.jpplStatus = jpplStatus;
	}
	public byte getStatus() {
		return status;
	}
	public void setStatus(byte status) {
		this.status = status;
	}
	
	

}
