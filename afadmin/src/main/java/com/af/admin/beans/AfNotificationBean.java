package com.af.admin.beans;

import org.springframework.web.multipart.MultipartFile;

public class AfNotificationBean {

	private Integer notificationNo;
	private String notificationId;
	private String notificationName;
	private String notificationTitle;
	private String notificationImageUrl;
	private String notificationLandingUrl;
	private  String notificationText;
	private MultipartFile notificationImage;
	private String notificationStatus;
	private String notificationStatusClass;
	private int notificationStatusCode;

	private String partNo;
	private String partnerName;

	private String fromDate;
	private String toDate;
	
	private byte loggedInStatus;
	private byte nonLoggedInStatus;
	
	private String timeStamp;
	
	public Integer getNotificationNo() {
		return notificationNo;
	}
	public void setNotificationNo(Integer notificationNo) {
		this.notificationNo = notificationNo;
	}
	public String getNotificationId() {
		return notificationId;
	}
	public void setNotificationId(String notificationId) {
		this.notificationId = notificationId;
	}
	public String getNotificationName() {
		return notificationName;
	}
	public void setNotificationName(String notificationName) {
		this.notificationName = notificationName;
	}
	public String getNotificationImageUrl() {
		return notificationImageUrl;
	}
	public void setNotificationImageUrl(String notificationImageUrl) {
		this.notificationImageUrl = notificationImageUrl;
	}
	public String getNotificationLandingUrl() {
		return notificationLandingUrl;
	}
	public void setNotificationLandingUrl(String notificationLandingUrl) {
		this.notificationLandingUrl = notificationLandingUrl;
	}
	public String getNotificationText() {
		return notificationText;
	}
	public void setNotificationText(String notificationText) {
		this.notificationText = notificationText;
	}
	public MultipartFile getNotificationImage() {
		return notificationImage;
	}
	public void setNotificationImage(MultipartFile notificationImage) {
		this.notificationImage = notificationImage;
	}
	public String getNotificationStatus() {
		return notificationStatus;
	}
	public void setNotificationStatus(String notificationStatus) {
		this.notificationStatus = notificationStatus;
	}
	public String getNotificationStatusClass() {
		return notificationStatusClass;
	}
	public void setNotificationStatusClass(String notificationStatusClass) {
		this.notificationStatusClass = notificationStatusClass;
	}
	public int getNotificationStatusCode() {
		return notificationStatusCode;
	}
	public void setNotificationStatusCode(int notificationStatusCode) {
		this.notificationStatusCode = notificationStatusCode;
	}
	public String getPartNo() {
		return partNo;
	}
	public void setPartNo(String partNo) {
		this.partNo = partNo;
	}
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	
	public byte getLoggedInStatus() {
		return loggedInStatus;
	}
	public void setLoggedInStatus(byte loggedInStatus) {
		this.loggedInStatus = loggedInStatus;
	}
	
	public byte getNonLoggedInStatus() {
		return nonLoggedInStatus;
	}
	public void setNonLoggedInStatus(byte nonLoggedInStatus) {
		this.nonLoggedInStatus = nonLoggedInStatus;
	}
	public String getNotificationTitle() {
		return notificationTitle;
	}
	public void setNotificationTitle(String notificationTitle) {
		this.notificationTitle = notificationTitle;
	}
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	
}
