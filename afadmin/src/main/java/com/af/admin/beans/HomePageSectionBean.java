package com.af.admin.beans;

public class HomePageSectionBean {
	
	private Integer sectionId;
	private String sectionName;
	private byte sectionCode;
	private byte sectionPosition;
	private byte status;
	private byte loggedInStatus;
	private byte nonLoggedInstatus;
	private String statusValue;
	private String statusCodeClass;

	
	public Integer getSectionId() {
		return sectionId;
	}
	public void setSectionId(Integer sectionId) {
		this.sectionId = sectionId;
	}
	public String getSectionName() {
		return sectionName;
	}
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}
	public byte getSectionCode() {
		return sectionCode;
	}
	public void setSectionCode(byte sectionCode) {
		this.sectionCode = sectionCode;
	}
	public byte getSectionPosition() {
		return sectionPosition;
	}
	public void setSectionPosition(byte sectionPosition) {
		this.sectionPosition = sectionPosition;
	}
	public byte getStatus() {
		return status;
	}
	public void setStatus(byte status) {
		this.status = status;
	}
	public byte getLoggedInStatus() {
		return loggedInStatus;
	}
	public void setLoggedInStatus(byte loggedInStatus) {
		this.loggedInStatus = loggedInStatus;
	}
	public byte getNonLoggedInstatus() {
		return nonLoggedInstatus;
	}
	public void setNonLoggedInstatus(byte nonLoggedInstatus) {
		this.nonLoggedInstatus = nonLoggedInstatus;
	}
	public String getStatusValue() {
		return statusValue;
	}
	public void setStatusValue(String statusValue) {
		this.statusValue = statusValue;
	}
	public String getStatusCodeClass() {
		return statusCodeClass;
	}
	public void setStatusCodeClass(String statusCodeClass) {
		this.statusCodeClass = statusCodeClass;
	}

}
