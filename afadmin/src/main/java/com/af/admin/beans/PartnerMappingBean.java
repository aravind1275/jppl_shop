package com.af.admin.beans;

public class PartnerMappingBean {
	
	private Integer PrtMapNo;
	private String  partNO;
	private String  partnerName;
	private String  partnerMappingName;
	private String  partnerType;
	private byte  allowLandingUrl;
	
	public Integer getPrtMapNo() {
		return PrtMapNo;
	}
	public void setPrtMapNo(Integer prtMapNo) {
		PrtMapNo = prtMapNo;
	}
	public String getPartNO() {
		return partNO;
	}
	public void setPartNO(String partNO) {
		this.partNO = partNO;
	}
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	public String getPartnerMappingName() {
		return partnerMappingName;
	}
	public void setPartnerMappingName(String partnerMappingName) {
		this.partnerMappingName = partnerMappingName;
	}
	public String getPartnerType() {
		return partnerType;
	}
	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}
	public byte getAllowLandingUrl() {
		return allowLandingUrl;
	}
	public void setAllowLandingUrl(byte allowLandingUrl) {
		this.allowLandingUrl = allowLandingUrl;
	}
	
	
	
	
}
