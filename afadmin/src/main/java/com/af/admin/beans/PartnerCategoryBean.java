package com.af.admin.beans;

public class PartnerCategoryBean {

	private String catNo;
	private String partNO;
	private String partnerName;
	private String partnerID;
	private String categoryId;
	private String categoryName;
	private String partnerCategoryId;
	private String partnercategoryName;
	private String prtCatNo;
	
	private String categorySatus;
	private String categoryStatusClass;
	private int   categoryStatusCode;
	private String categoryImageUrl;

	private String prtProductNo;
	private String prtProductName;
	
	
	
	public String getPrtProductNo() {
		return prtProductNo;
	}
	public void setPrtProductNo(String prtProductNo) {
		this.prtProductNo = prtProductNo;
	}
	public String getPrtProductName() {
		return prtProductName;
	}
	public void setPrtProductName(String prtProductName) {
		this.prtProductName = prtProductName;
	}
	private String displayOrder;
	private String filterStartValue;
	private String filterEndValue;
	private String filterStepValue;
	public String getCatNo() {
		return catNo;
	}
	public void setCatNo(String catNo) {
		this.catNo = catNo;
	}
	public String getPartNO() {
		return partNO;
	}
	public void setPartNO(String partNO) {
		this.partNO = partNO;
	}
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	public String getPartnerID() {
		return partnerID;
	}
	public void setPartnerID(String partnerID) {
		this.partnerID = partnerID;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getPartnerCategoryId() {
		return partnerCategoryId;
	}
	public void setPartnerCategoryId(String partnerCategoryId) {
		this.partnerCategoryId = partnerCategoryId;
	}
	public String getPartnercategoryName() {
		return partnercategoryName;
	}
	public void setPartnercategoryName(String partnercategoryName) {
		this.partnercategoryName = partnercategoryName;
	}
	public String getPrtCatNo() {
		return prtCatNo;
	}
	public void setPrtCatNo(String prtCatNo) {
		this.prtCatNo = prtCatNo;
	}
	public String getCategorySatus() {
		return categorySatus;
	}
	public void setCategorySatus(String categorySatus) {
		this.categorySatus = categorySatus;
	}
	public String getCategoryStatusClass() {
		return categoryStatusClass;
	}
	public void setCategoryStatusClass(String categoryStatusClass) {
		this.categoryStatusClass = categoryStatusClass;
	}
	public int getCategoryStatusCode() {
		return categoryStatusCode;
	}
	public void setCategoryStatusCode(int categoryStatusCode) {
		this.categoryStatusCode = categoryStatusCode;
	}
	public String getDisplayOrder() {
		return displayOrder;
	}
	public void setDisplayOrder(String displayOrder) {
		this.displayOrder = displayOrder;
	}
	public String getFilterStartValue() {
		return filterStartValue;
	}
	public void setFilterStartValue(String filterStartValue) {
		this.filterStartValue = filterStartValue;
	}
	public String getFilterEndValue() {
		return filterEndValue;
	}
	public void setFilterEndValue(String filterEndValue) {
		this.filterEndValue = filterEndValue;
	}
	public String getFilterStepValue() {
		return filterStepValue;
	}
	public void setFilterStepValue(String filterStepValue) {
		this.filterStepValue = filterStepValue;
	}
	public String getCategoryImageUrl() {
		return categoryImageUrl;
	}
	public void setCategoryImageUrl(String categoryImageUrl) {
		this.categoryImageUrl = categoryImageUrl;
	}
	
	
	
	
}