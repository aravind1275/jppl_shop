package com.af.admin.beans;

import org.springframework.web.multipart.MultipartFile;



public class PartnerBean {
		
	private Integer prtNo;
	private String partnerId;
	private String partnerName;	
	private float ratePerMile;
	private String partnerImageUrl;
	private MultipartFile partnerImage;
	private String partnerStatus;
	private String partnerStatusClass;
	private int   partnerStatusCode;
	private String activationDate;
	private String deactivationDate;
	private int[]   hostPartner;
	private int[]   defaultPartner;
	private int[]   logoPartner;
	private int[]   offLinePartner;
	private int[]   talkPartner;
	private int[]   readPartner;
	
	private int   hostPartnerCode;
	private int   defaultPartnerCode;
	private int   logoPartnerCode;
	private int   offLinePartnerCode;
	
	private int   readPartnerCode;
	private int   talkPartnerCode;

	private int logoPrtDisplayOrder;
	private int logoPrtTilePos;
	
	private int logoPartnerTilePosition;
	private int logoPartnerDispLimit;
		
	private String partnerType;
	
	private String affiliateId;
	private String sourceOfInfo;
	private String referenceToken;
	private String partnerLandingUrl;
	private String partnerIconUrl;
	private String logoPartnerSearchUrl;
	private MultipartFile partnerIcon;
	private String affiliateIdName;
	private String sourceOfInfoName;
	private String referenceTokenName;
	
	private String jpmileDesc;
	private String partnerDesc;
	private String jpmileReconPeriod;
	private String activityId;
	private String paramFour;
	private String paramFourName;
	
	private String termsAndCondition;
	private String allowJpnumber;
	private String displayPrtPopup;
	
	private String point1;
	private String point2;
	private String point3;
	private String point4;
	private String point5;
	
	private int status;
    private String partCatTextMap;
    
    private String aggregateId;
    
    private String partnerPopupImageUrl;
	private MultipartFile partnerPopupImage;
	
	
	public Integer getPrtNo() {
		return prtNo;
	}
	public void setPrtNo(Integer prtNo) {
		this.prtNo = prtNo;
	}
	public String getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	
		
	public float getRatePerMile() {
		return ratePerMile;
	}
	public void setRatePerMile(float ratePerMile) {
		this.ratePerMile = ratePerMile;
	}
	public MultipartFile getPartnerImage() {
		return partnerImage;
	}
	public void setPartnerImage(MultipartFile partnerImage) {
		this.partnerImage = partnerImage;
	}
	public String getPartnerImageUrl() {
		return partnerImageUrl;
	}
	public void setPartnerImageUrl(String partnerImageUrl) {
		this.partnerImageUrl = partnerImageUrl;
	}
	public String getPartnerStatus() {
		return partnerStatus;
	}
	public void setPartnerStatus(String partnerStatus) {
		this.partnerStatus = partnerStatus;
	}
	public String getPartnerStatusClass() {
		return partnerStatusClass;
	}
	public void setPartnerStatusClass(String partnerStatusClass) {
		this.partnerStatusClass = partnerStatusClass;
	}
	public int getPartnerStatusCode() {
		return partnerStatusCode;
	}
	public void setPartnerStatusCode(int partnerStatusCode) {
		this.partnerStatusCode = partnerStatusCode;
	}
	public String getActivationDate() {
		return activationDate;
	}
	public void setActivationDate(String activationDate) {
		this.activationDate = activationDate;
	}
	public String getDeactivationDate() {
		return deactivationDate;
	}
	public void setDeactivationDate(String deactivationDate) {
		this.deactivationDate = deactivationDate;
	}
	public int[] getHostPartner() {
		return hostPartner;
	}
	public void setHostPartner(int[] hostPartner) {
		this.hostPartner = hostPartner;
	}
	public int[] getDefaultPartner() {
		return defaultPartner;
	}
	public void setDefaultPartner(int[] defaultPartner) {
		this.defaultPartner = defaultPartner;
	}
	public int getHostPartnerCode() {
		return hostPartnerCode;
	}
	public void setHostPartnerCode(int hostPartnerCode) {
		this.hostPartnerCode = hostPartnerCode;
	}
	public int getDefaultPartnerCode() {
		return defaultPartnerCode;
	}
	public void setDefaultPartnerCode(int defaultPartnerCode) {
		this.defaultPartnerCode = defaultPartnerCode;
	}
	public int[] getLogoPartner() {
		return logoPartner;
	}
	public void setLogoPartner(int[] logoPartner) {
		this.logoPartner = logoPartner;
	}
	public int getLogoPartnerCode() {
		return logoPartnerCode;
	}
	public void setLogoPartnerCode(int logoPartnerCode) {
		this.logoPartnerCode = logoPartnerCode;
	}

	public int getLogoPrtDisplayOrder() {
		return logoPrtDisplayOrder;
	}
	public void setLogoPrtDisplayOrder(int logoPrtDisplayOrder) {
		this.logoPrtDisplayOrder = logoPrtDisplayOrder;
	}
	public int getLogoPrtTilePos() {
		return logoPrtTilePos;
	}
	public void setLogoPrtTilePos(int logoPrtTilePos) {
		this.logoPrtTilePos = logoPrtTilePos;
	}
	public String getPartnerType() {
		return partnerType;
	}
	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}
	public int getLogoPartnerTilePosition() {
		return logoPartnerTilePosition;
	}
	public void setLogoPartnerTilePosition(int logoPartnerTilePosition) {
		this.logoPartnerTilePosition = logoPartnerTilePosition;
	}
	public int getLogoPartnerDispLimit() {
		return logoPartnerDispLimit;
	}
	public void setLogoPartnerDispLimit(int logoPartnerDispLimit) {
		this.logoPartnerDispLimit = logoPartnerDispLimit;
	}
	public String getAffiliateId() {
		return affiliateId;
	}
	public void setAffiliateId(String affiliateId) {
		this.affiliateId = affiliateId;
	}
	public String getSourceOfInfo() {
		return sourceOfInfo;
	}
	public void setSourceOfInfo(String sourceOfInfo) {
		this.sourceOfInfo = sourceOfInfo;
	}
	public String getReferenceToken() {
		return referenceToken;
	}
	public void setReferenceToken(String referenceToken) {
		this.referenceToken = referenceToken;
	}
	public String getPartnerLandingUrl() {
		return partnerLandingUrl;
	}
	public void setPartnerLandingUrl(String partnerLandingUrl) {
		this.partnerLandingUrl = partnerLandingUrl;
	}
	public String getPartnerIconUrl() {
		return partnerIconUrl;
	}
	public void setPartnerIconUrl(String partnerIconUrl) {
		this.partnerIconUrl = partnerIconUrl;
	}
	public String getLogoPartnerSearchUrl() {
		return logoPartnerSearchUrl;
	}
	public void setLogoPartnerSearchUrl(String logoPartnerSearchUrl) {
		this.logoPartnerSearchUrl = logoPartnerSearchUrl;
	}
	public MultipartFile getPartnerIcon() {
		return partnerIcon;
	}
	public void setPartnerIcon(MultipartFile partnerIcon) {
		this.partnerIcon = partnerIcon;
	}
	public String getAffiliateIdName() {
		return affiliateIdName;
	}
	public void setAffiliateIdName(String affiliateIdName) {
		this.affiliateIdName = affiliateIdName;
	}
	public String getSourceOfInfoName() {
		return sourceOfInfoName;
	}
	public void setSourceOfInfoName(String sourceOfInfoName) {
		this.sourceOfInfoName = sourceOfInfoName;
	}
	public String getReferenceTokenName() {
		return referenceTokenName;
	}
	public void setReferenceTokenName(String referenceTokenName) {
		this.referenceTokenName = referenceTokenName;
	}

	public String getJpmileDesc() {
		return jpmileDesc;
	}
	public void setJpmileDesc(String jpmileDesc) {
		this.jpmileDesc = jpmileDesc;
	}
	public String getPartnerDesc() {
		return partnerDesc;
	}
	public void setPartnerDesc(String partnerDesc) {
		this.partnerDesc = partnerDesc;
	}
	
	public String getJpmileReconPeriod() {
		return jpmileReconPeriod;
	}
	public void setJpmileReconPeriod(String jpmileReconPeriod) {
		this.jpmileReconPeriod = jpmileReconPeriod;
	}
	public String getActivityId() {
		return activityId;
	}
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}
	public String getParamFour() {
		return paramFour;
	}
	public void setParamFour(String paramFour) {
		this.paramFour = paramFour;
	}
	public String getParamFourName() {
		return paramFourName;
	}
	public void setParamFourName(String paramFourName) {
		this.paramFourName = paramFourName;
	}
	
	public String getTermsAndCondition() {
		return termsAndCondition;
	}
	public void setTermsAndCondition(String termsAndCondition) {
		this.termsAndCondition = termsAndCondition;
	}
	
	public String getAllowJpnumber() {
		return allowJpnumber;
	}
	public void setAllowJpnumber(String allowJpnumber) {
		this.allowJpnumber = allowJpnumber;
	}
	
	public String getDisplayPrtPopup() {
		return displayPrtPopup;
	}
	public void setDisplayPrtPopup(String displayPrtPopup) {
		this.displayPrtPopup = displayPrtPopup;
	}
	
	public String getPoint1() {
		return point1;
	}
	public void setPoint1(String point1) {
		this.point1 = point1;
	}
	
	
	public String getPoint2() {
		return point2;
	}
	public void setPoint2(String point2) {
		this.point2 = point2;
	}
	/**
	 * @return the point3
	 */
	public String getPoint3() {
		return point3;
	}
	/**
	 * @param point3 the point3 to set
	 */
	public void setPoint3(String point3) {
		this.point3 = point3;
	}
	/**
	 * @return the point4
	 */
	public String getPoint4() {
		return point4;
	}
	/**
	 * @param point4 the point4 to set
	 */
	public void setPoint4(String point4) {
		this.point4 = point4;
	}
	
	public String getPoint5() {
		return point5;
	}
	/**
	 * @param point5 the point4 to set
	 */
	public void setPoint5(String point5) {
		this.point5 = point5;
	}
	
	public int[] getOffLinePartner() {
		return offLinePartner;
	}
	public void setOffLinePartner(int[] offLinePartner) {
		this.offLinePartner = offLinePartner;
	}
	
	public int getOffLinePartnerCode() {
		return offLinePartnerCode;
	}
	public void setOffLinePartnerCode(int offLinePartnerCode) {
		this.offLinePartnerCode = offLinePartnerCode;
	}
	public int[] getTalkPartner() {
		return talkPartner;
	}
	public void setTalkPartner(int[] talkPartner) {
		this.talkPartner = talkPartner;
	}
	public int[] getReadPartner() {
		return readPartner;
	}
	public void setReadPartner(int[] readPartner) {
		this.readPartner = readPartner;
	}
	public int getReadPartnerCode() {
		return readPartnerCode;
	}
	public void setReadPartnerCode(int readPartnerCode) {
		this.readPartnerCode = readPartnerCode;
	}
	public int getTalkPartnerCode() {
		return talkPartnerCode;
	}
	public void setTalkPartnerCode(int talkPartnerCode) {
		this.talkPartnerCode = talkPartnerCode;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getPartCatTextMap() {
		return partCatTextMap;
	}
	public void setPartCatTextMap(String partCatTextMap) {
		this.partCatTextMap = partCatTextMap;
	}
	public String getAggregateId() {
		return aggregateId;
	}
	public void setAggregateId(String aggregateId) {
		this.aggregateId = aggregateId;
	}
	public String getPartnerPopupImageUrl() {
		return partnerPopupImageUrl;
	}
	public void setPartnerPopupImageUrl(String partnerPopupImageUrl) {
		this.partnerPopupImageUrl = partnerPopupImageUrl;
	}
	public MultipartFile getPartnerPopupImage() {
		return partnerPopupImage;
	}
	public void setPartnerPopupImage(MultipartFile partnerPopupImage) {
		this.partnerPopupImage = partnerPopupImage;
	}
	
	
}