package com.af.admin.beans;

import java.util.Date;

public class OfferDealLinkBean {
	
	private Integer lmdId;
	private String store;
	private String offerText;
	private String title;
	private String description;
	private String code;
	private String termsAndConditions;
	private String categories;
	private String featured;
	private String url;
	private String smartLink;
	private String image_url;
	private String type;
	private String offer;
	private String offerValue;
	private String status;
	private Date startDate;
	private Date endDate;
	
	public Integer getLmdId() {
		return lmdId;
	}
	public void setLmdId(Integer lmdId) {
		this.lmdId = lmdId;
	}
	public String getStore() {
		return store;
	}
	public void setStore(String store) {
		this.store = store;
	}
	public String getOfferText() {
		return offerText;
	}
	public void setOfferText(String offerText) {
		this.offerText = offerText;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getTermsAndConditions() {
		return termsAndConditions;
	}
	public void setTermsAndConditions(String termsAndConditions) {
		this.termsAndConditions = termsAndConditions;
	}
	public String getCategories() {
		return categories;
	}
	public void setCategories(String categories) {
		this.categories = categories;
	}
	public String getFeatured() {
		return featured;
	}
	public void setFeatured(String featured) {
		this.featured = featured;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getSmartLink() {
		return smartLink;
	}
	public void setSmartLink(String smartLink) {
		this.smartLink = smartLink;
	}
	public String getImage_url() {
		return image_url;
	}
	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getOffer() {
		return offer;
	}
	public void setOffer(String offer) {
		this.offer = offer;
	}
	public String getOfferValue() {
		return offerValue;
	}
	public void setOfferValue(String offerValue) {
		this.offerValue = offerValue;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	
	
}
