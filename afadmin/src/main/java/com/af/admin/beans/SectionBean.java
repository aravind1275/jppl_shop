package com.af.admin.beans;

public class SectionBean {
	
	private Integer sectionId;
	private String sectionTitle;
	private byte displayOrder;
	private String status;
	private int sectionStatusCode;
	private String statusCodeClass;
	private String activationDate;
	private String deactivationDate;
	private String sectionType;
	private String homeSectionTitle;
	private byte homeSectionDisplayOrder;
	private Integer homeSectionId;
	private String footerDesc;
	private String footerImageUrl1;
	private String footerImageUrl2;
	private byte viewLink;
	private String redirectionPage;



	public Integer getSectionId() {
		return sectionId;
	}

	public void setSectionId(Integer sectionId) {
		this.sectionId = sectionId;
	}
	
	public String getSectionTitle() {
		return sectionTitle;
	}
	
	public void setSectionTitle(String sectionTitle) {
		this.sectionTitle = sectionTitle;
	}
	
	public byte getDisplayOrder() {
		return displayOrder;
	}
	
	public void setDisplayOrder(byte displayOrder) {
		this.displayOrder = displayOrder;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public int getSectionStatusCode() {
		return sectionStatusCode;
	}

	public void setSectionStatusCode(int sectionStatusCode) {
		this.sectionStatusCode = sectionStatusCode;
	}

	public String getStatusCodeClass() {
		return statusCodeClass;
	}

	public void setStatusCodeClass(String statusCodeClass) {
		this.statusCodeClass = statusCodeClass;
	}

	public String getActivationDate() {
		return activationDate;
	}
	
	public void setActivationDate(String activationDate) {
		this.activationDate = activationDate;
	}
	
	public String getDeactivationDate() {
		return deactivationDate;
	}
	
	public void setDeactivationDate(String deactivationDate) {
		this.deactivationDate = deactivationDate;
	}

	public String getSectionType() {
		return sectionType;
	}

	public void setSectionType(String sectionType) {
		this.sectionType = sectionType;
	}

	public String getHomeSectionTitle() {
		return homeSectionTitle;
	}

	public void setHomeSectionTitle(String homeSectionTitle) {
		this.homeSectionTitle = homeSectionTitle;
	}

	public byte getHomeSectionDisplayOrder() {
		return homeSectionDisplayOrder;
	}

	public void setHomeSectionDisplayOrder(byte homeSectionDisplayOrder) {
		this.homeSectionDisplayOrder = homeSectionDisplayOrder;
	}

	public Integer getHomeSectionId() {
		return homeSectionId;
	}

	public void setHomeSectionId(Integer homeSectionId) {
		this.homeSectionId = homeSectionId;
	}

	public String getFooterDesc() {
		return footerDesc;
	}

	public void setFooterDesc(String footerDesc) {
		this.footerDesc = footerDesc;
	}

	public String getFooterImageUrl1() {
		return footerImageUrl1;
	}

	public void setFooterImageUrl1(String footerImageUrl1) {
		this.footerImageUrl1 = footerImageUrl1;
	}

	public String getFooterImageUrl2() {
		return footerImageUrl2;
	}

	public void setFooterImageUrl2(String footerImageUrl2) {
		this.footerImageUrl2 = footerImageUrl2;
	}

	public byte getViewLink() {
		return viewLink;
	}

	public void setViewLink(byte viewLink) {
		this.viewLink = viewLink;
	}

	public String getRedirectionPage() {
		return redirectionPage;
	}

	public void setRedirectionPage(String redirectionPage) {
		this.redirectionPage = redirectionPage;
	}

	
}
