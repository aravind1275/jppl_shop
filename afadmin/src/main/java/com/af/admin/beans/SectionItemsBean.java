package com.af.admin.beans;

public class SectionItemsBean {
	private String itemId;
	private String sectionId;
	private String catNo;
	private String prtNo;
	private String prtCatNo;
	private String prtOfferNo;
	private String prtPrdctNo;
	private String productName;
	private String prtCatName;
	private String itemType;
	private String displayOrder;
	private String startDate;
	private String endDate;
	private String getCategoryList;
	private String dupRecord;
	private byte sectionItemStatus;
	private String typeName;
	private String prtOfferName;
	private String status;
	private String statusCodeClass;
	private String sectionTitle;
	private String partnerName;
	private String catName;
	private String prdName;
	private String title;
	private String brandName;
	private String brandImageUrl;
	private String brandDescription;
	private String brandNameHidden;
	private String landingUrl;
	private String customDescription;
	private String brandImageUrlCustom;
	private String displayPrtPopup;
	
	public String getCatName() {
		return catName;
	}
	public void setCatName(String catName) {
		this.catName = catName;
	}
	
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	
	public String getSectionId() {
		return sectionId;
	}
	public void setSectionId(String sectionId) {
		this.sectionId = sectionId;
	}
	
	public String getCatNo() {
		return catNo;
	}
	public void setCatNo(String catNo) {
		this.catNo = catNo;
	}
	
	public String getPrtNo() {
		return prtNo;
	}
	public void setPrtNo(String prtNo) {
		this.prtNo = prtNo;
	}
	
	public String getPrtCatNo() {
		return prtCatNo;
	}
	public void setPrtCatNo(String prtCatNo) {
		this.prtCatNo = prtCatNo;
	}
	
	public String getPrtOfferNo() {
		return prtOfferNo;
	}
	public void setPrtOfferNo(String prtOfferNo) {
		this.prtOfferNo = prtOfferNo;
	}
	
	public String getPrtPrdctNo() {
		return prtPrdctNo;
	}
	public void setPrtPrdctNo(String prtPrdctNo) {
		this.prtPrdctNo = prtPrdctNo;
	}
	
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	public String getPrtCatName() {
		return prtCatName;
	}
	public void setPrtCatName(String prtCatName) {
		this.prtCatName = prtCatName;
	}
	
	public String getItemType() {
		return itemType;
	}
	public void setItemType(String itemType) {
		this.itemType = itemType;
	}
	
	public String getDisplayOrder() {
		return displayOrder;
	}
	public void setDisplayOrder(String displayOrder) {
		this.displayOrder = displayOrder;
	}
	
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getGetCategoryList() {
		return getCategoryList;
	}
	
	public void setGetCategoryList(String getCategoryList) {
		this.getCategoryList = getCategoryList;
	}
	
	public String getDupRecord() {
		return dupRecord;
	}
	
	public void setDupRecord(String dupRecord) {
		this.dupRecord = dupRecord;
	}
	public byte getSectionItemStatus() {
		return sectionItemStatus;
	}
	
	public void setSectionItemStatus(byte sectionItemStatus) {
		this.sectionItemStatus = sectionItemStatus;
	}
	public String getTypeName() {
		return typeName;
	}
	
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public String getPrtOfferName() {
		return prtOfferName;
	}
	public void setPrtOfferName(String prtOfferName) {
		this.prtOfferName = prtOfferName;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getStatusCodeClass() {
		return statusCodeClass;
	}
	public void setStatusCodeClass(String statusCodeClass) {
		this.statusCodeClass = statusCodeClass;
	}
	
	public String getSectionTitle() {
		return sectionTitle;
	}
	public void setSectionTitle(String sectionTitle) {
		this.sectionTitle = sectionTitle;
	}
	
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	
	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getBrandImageUrl() {
		return brandImageUrl;
	}
	public void setBrandImageUrl(String brandImageUrl) {
		this.brandImageUrl = brandImageUrl;
	}
	public String getBrandDescription() {
		return brandDescription;
	}
	public void setBrandDescription(String brandDescription) {
		this.brandDescription = brandDescription;
	}
	public String getBrandNameHidden() {
		return brandNameHidden;
	}
	public void setBrandNameHidden(String brandNameHidden) {
		this.brandNameHidden = brandNameHidden;
	}
	
	public String getLandingUrl() {
		return landingUrl;
	}
	public void setLandingUrl(String landingUrl) {
		this.landingUrl = landingUrl;
	}
	public String getCustomDescription() {
		return customDescription;
	}
	public void setCustomDescription(String customDescription) {
		this.customDescription = customDescription;
	}
	public String getBrandImageUrlCustom() {
		return brandImageUrlCustom;
	}
	public void setBrandImageUrlCustom(String brandImageUrlCustom) {
		this.brandImageUrlCustom = brandImageUrlCustom;
	}
	public String getDisplayPrtPopup() {
		return displayPrtPopup;
	}
	public void setDisplayPrtPopup(String displayPrtPopup) {
		this.displayPrtPopup = displayPrtPopup;
	}
	
	
	
	
	
	
	
}
