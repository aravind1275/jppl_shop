package com.af.admin.beans;

import java.io.Serializable;

public class AdminRolePermissionBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer rolepermissionID;
	private Integer roleId;
	private Integer permissionID;
	private byte permissionStatus;
	private String permissionCategoryName;
	
	public Integer getRolepermissionID() {
		return rolepermissionID;
	}
	public void setRolepermissionID(Integer rolepermissionID) {
		this.rolepermissionID = rolepermissionID;
	}
	public String getPermissionCategoryName() {
		return permissionCategoryName;
	}
	public void setPermissionCategoryName(String permissionCategoryName) {
		this.permissionCategoryName = permissionCategoryName;
	}
	
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	public Integer getPermissionID() {
		return permissionID;
	}
	public void setPermissionID(Integer permissionID) {
		this.permissionID = permissionID;
	}
	public byte getPermissionStatus() {
		return permissionStatus;
	}
	public void setPermissionStatus(byte permissionStatus) {
		this.permissionStatus = permissionStatus;
	}
	
}