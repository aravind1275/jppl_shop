package com.af.admin.beans;

public class RecommendedCategoryBean {
private Integer rcNo;
private Integer prtNo;
private Integer prtCatNo;
private Integer catNo;
private String rcId;
private String rcName;
private String rcLandingUrl;
private String rcImageUrl;
private String rcImageDesc;
private String activationDate;
private String deactivationDate;
private int position;
private String rcStatus;
private int status;
private int statusCode;
private String statusClass;
private String categoryList;
public Integer getRcNo() {
	return rcNo;
}
public void setRcNo(Integer rcNo) {
	this.rcNo = rcNo;
}
public Integer getPrtNo() {
	return prtNo;
}
public void setPrtNo(Integer prtNo) {
	this.prtNo = prtNo;
}

public Integer getPrtCatNo() {
	return prtCatNo;
}
public void setPrtCatNo(Integer prtCatNo) {
	this.prtCatNo = prtCatNo;
}
public String getRcId() {
	return rcId;
}
public void setRcId(String rcId) {
	this.rcId = rcId;
}
public String getRcName() {
	return rcName;
}
public void setRcName(String rcName) {
	this.rcName = rcName;
}
public String getRcLandingUrl() {
	return rcLandingUrl;
}
public void setRcLandingUrl(String rcLandingUrl) {
	this.rcLandingUrl = rcLandingUrl;
}
public String getRcImageUrl() {
	return rcImageUrl;
}
public void setRcImageUrl(String rcImageUrl) {
	this.rcImageUrl = rcImageUrl;
}
public String getRcImageDesc() {
	return rcImageDesc;
}
public void setRcImageDesc(String rcImageDesc) {
	this.rcImageDesc = rcImageDesc;
}

public String getActivationDate() {
	return activationDate;
}
public void setActivationDate(String activationDate) {
	this.activationDate = activationDate;
}
public String getDeactivationDate() {
	return deactivationDate;
}
public void setDeactivationDate(String deactivationDate) {
	this.deactivationDate = deactivationDate;
}
public int getPosition() {
	return position;
}
public void setPosition(int position) {
	this.position = position;
}
public String getRcStatus() {
	return rcStatus;
}
public void setRcStatus(String rcStatus) {
	this.rcStatus = rcStatus;
}
public int getStatus() {
	return status;
}
public void setStatus(int status) {
	this.status = status;
}
public int getStatusCode() {
	return statusCode;
}
public void setStatusCode(int statusCode) {
	this.statusCode = statusCode;
}
public String getStatusClass() {
	return statusClass;
}
public void setStatusClass(String statusClass) {
	this.statusClass = statusClass;
}
public Integer getCatNo() {
	return catNo;
}
public void setCatNo(Integer catNo) {
	this.catNo = catNo;
}
public String getCategoryList() {
	return categoryList;
}
public void setCategoryList(String categoryList) {
	this.categoryList = categoryList;
}



}
