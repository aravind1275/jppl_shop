package com.af.admin.beans;

public class InvalidCsvReprot {
	
	private String afffiliateOrderId;
	private String transactionId;
	private String productName;
	private String partnerCategoryName;
	private String partnerName;
	private String trackingId;
	
	
	
	
	public InvalidCsvReprot() {
	}


	public InvalidCsvReprot(String afffiliateOrderId, String transactionId, String productName,
			String partnerCategoryName, String partnerName, String trackingId) {
		super();
		this.afffiliateOrderId = afffiliateOrderId;
		this.transactionId = transactionId;
		this.productName = productName;
		this.partnerCategoryName = partnerCategoryName;
		this.partnerName = partnerName;
		this.trackingId = trackingId;
	}


	public String getAfffiliateOrderId() {
		return afffiliateOrderId;
	}


	public void setAfffiliateOrderId(String afffiliateOrderId) {
		this.afffiliateOrderId = afffiliateOrderId;
	}


	public String getTransactionId() {
		return transactionId;
	}


	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}


	public String getProductName() {
		return productName;
	}


	public void setProductName(String productName) {
		this.productName = productName;
	}


	public String getPartnerCategoryName() {
		return partnerCategoryName;
	}


	public void setPartnerCategoryName(String partnerCategoryName) {
		this.partnerCategoryName = partnerCategoryName;
	}


	public String getPartnerName() {
		return partnerName;
	}


	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}


	public String getTrackingId() {
		return trackingId;
	}


	public void setTrackingId(String trackingId) {
		this.trackingId = trackingId;
	}
	
	

}
