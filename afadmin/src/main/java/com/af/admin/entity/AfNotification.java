package com.af.admin.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.web.multipart.MultipartFile;

@Entity
@Table(name = "AF_NOTIFICATION")
public class AfNotification implements java.io.Serializable ,Cloneable {
	private Integer notificationNo;
	private AfPartner afPartner;
	private String notificationId;
	private String notificationName;
	private String notificationText;
	private String notificationTitle;
	private String notificationImageUrl;
	private String notificationLandingUrl;
	private Date notificationStartDate;
	private Date notificationEndDate;
	private byte notificationStatus;
	private Date createdTime;
	private Integer createdBy;
	private Date modifiedTime;
	private Integer modifiedBy;
	private byte status;
	
	private byte loggedInStatus;
	private byte nonLoggedInstatus;
	
	private Date timeStamp;
	
	 public AfNotification(){
		 
	 }

	public AfNotification(String notificationId, String notificationName, Date timeStamp,String notificationText,
			byte notificationStatus, byte status) {
		super();
		this.notificationId = notificationId;
		this.notificationName = notificationName;
		this.notificationText = notificationText;
		this.notificationStatus = notificationStatus;
		this.status = status;
		this.timeStamp = timeStamp;
	}

	public AfNotification(AfPartner afPartner, String notificationId, String notificationName, String notificationText,
			String notificationImageUrl, String notificationLandingUrl,Date timeStamp, Date notificationStartDate,
			Date notificationEndDate, byte notificationStatus, Date createdTime, Integer createdBy, Date modifiedTime,
			Integer modifiedBy, byte status,byte loggedInStatus,byte nonLoggedInstatus) {
		super();
		this.afPartner = afPartner;
		this.notificationId = notificationId;
		this.notificationName = notificationName;
		this.notificationText = notificationText;
		this.notificationImageUrl = notificationImageUrl;
		this.notificationLandingUrl = notificationLandingUrl;
		this.timeStamp = timeStamp;
		this.notificationStartDate = notificationStartDate;
		this.notificationEndDate = notificationEndDate;
		this.notificationStatus = notificationStatus;
		this.createdTime = createdTime;
		this.createdBy = createdBy;
		this.modifiedTime = modifiedTime;
		this.modifiedBy = modifiedBy;
		this.status = status;
		this.loggedInStatus = loggedInStatus;
		this.nonLoggedInstatus = nonLoggedInstatus;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "NOTIFICATION_NO")
	public Integer getNotificationNo() {
		return notificationNo;
	}

	public void setNotificationNo(Integer notificationNo) {
		this.notificationNo = notificationNo;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PRT_NO")
	public AfPartner getAfPartner() {
		return afPartner;
	}

	

	public void setAfPartner(AfPartner afPartner) {
		this.afPartner = afPartner;
	}
	@Column(name = "NOTIFICATION_ID", nullable = false, length = 50)
	public String getNotificationId() {
		return notificationId;
	}
	public void setNotificationId(String notificationId) {
		this.notificationId = notificationId;
	}
	@Column(name = "NOTIFICATION_NAME", nullable = false, length = 130)
	public String getNotificationName() {
		return notificationName;
	}
	public void setNotificationName(String notificationName) {
		this.notificationName = notificationName;
	}
	@Column(name = "NOTIFICATION_TEXT", nullable = false, length = 250)
	public String getNotificationText() {
		return notificationText;
	}
	public void setNotificationText(String notificationText) {
		this.notificationText = notificationText;
	}
	@Column(name = "NOTI_IMAGE", length = 250)
	public String getNotificationImageUrl() {
		return notificationImageUrl;
	}
	
	public void setNotificationImageUrl(String notificationImageUrl) {
		this.notificationImageUrl = notificationImageUrl;
	}
	
	@Column(name = "NOTI_LANDING_URL", length = 2000)
	public String getNotificationLandingUrl() {
		return notificationLandingUrl;
	}
	public void setNotificationLandingUrl(String notificationLandingUrl) {
		this.notificationLandingUrl = notificationLandingUrl;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "NOTI_START_DATE", length = 0)
	public Date getNotificationStartDate() {
		return notificationStartDate;
	}
	public void setNotificationStartDate(Date notificationStartDate) {
		this.notificationStartDate = notificationStartDate;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "NOTI_END_DATE", length = 0)
	public Date getNotificationEndDate() {
		return notificationEndDate;
	}
	public void setNotificationEndDate(Date notificationEndDate) {
		this.notificationEndDate = notificationEndDate;
	}
	
	@Column(name = "NOTI_STATUS", nullable = false)
	public byte getNotificationStatus() {
		return notificationStatus;
	}
	public void setNotificationStatus(byte notificationStatus) {
		this.notificationStatus = notificationStatus;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_TIME", length = 0)
	public Date getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
	
	@Column(name = "CREATED_BY")
	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MODIFIED_TIME", length = 0)
	public Date getModifiedTime() {
		return modifiedTime;
	}
	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	
	@Column(name = "MODIFIED_BY")
	public Integer getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	@Column(name = "STATUS", nullable = false)
	public byte getStatus() {
		return status;
	}
	public void setStatus(byte status) {
		this.status = status;
	}
	@Column(name = "LOGGED_IN_STATUS", nullable = false)
	public byte getLoggedInStatus() {
		return loggedInStatus;
	}

	public void setLoggedInStatus(byte loggedInStatus) {
		this.loggedInStatus = loggedInStatus;
	}
	
	@Column(name = "NON_LOGGED_IN_STATUS", nullable = false)
	public byte getNonLoggedInstatus() {
		return nonLoggedInstatus;
	}

	public void setNonLoggedInstatus(byte nonLoggedInstatus) {
		this.nonLoggedInstatus = nonLoggedInstatus;
	}
	@Column(name = "NOTIFICATION_TITLE", nullable = false, length = 255)
	public String getNotificationTitle() {
		return notificationTitle;
	}

	public void setNotificationTitle(String notificationTitle) {
		this.notificationTitle = notificationTitle;
	}
	@Temporal(TemporalType.TIME)
	@Column(name = "TIME_STAMP", nullable = false, length = 0)
	public Date getTimeStamp() {
		return this.timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}
	public Object clone() throws CloneNotSupportedException{  
		return super.clone();  
		}
	
	 
}
