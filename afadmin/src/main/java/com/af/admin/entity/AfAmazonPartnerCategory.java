package com.af.admin.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "AMAZON_PARTNER_CATEGORY")
public class AfAmazonPartnerCategory implements java.io.Serializable, Cloneable{
	
	private static final long serialVersionUID = 1L;
	
	
	private Integer amzCatNo;
	private AfPartner afPartner;
	private String amzCategoryId;
	private String amzCategoryName;
	private Date createdTime;
	private Integer createdBy;
	private Date modifiedTime;
	private Integer modifiedBy;
	private byte status;
	private byte amazonCategoryStatus;
	private  AfPartnerCategory afPartnerCategory;

	public AfAmazonPartnerCategory() {
	}


	public AfAmazonPartnerCategory(Integer amzCatNo, Integer prtCatNo, Integer prtNo, String amzCategoryId,
			String amzCategoryName, byte status) {
		super();
		this.amzCatNo = amzCatNo;
		this.amzCategoryId = amzCategoryId;
		this.amzCategoryName = amzCategoryName;
		this.status = status;
	}


	public AfAmazonPartnerCategory(Integer amzCatNo, Integer prtCatNo, Integer prtNo, String amzCategoryId,
			String amzCategoryName, Date createdTime, Integer createdBy, Date modifiedTime, Integer modifiedBy,
			byte status) {
		super();
		this.amzCatNo = amzCatNo;
		this.amzCategoryId = amzCategoryId;
		this.amzCategoryName = amzCategoryName;
		this.createdTime = createdTime;
		this.createdBy = createdBy;
		this.modifiedTime = modifiedTime;
		this.modifiedBy = modifiedBy;
		this.status = status;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PRT_NO", nullable = false)
	public AfPartner getAfPartner() {
		return afPartner;
	}


	public void setAfPartner(AfPartner afPartner) {
		this.afPartner = afPartner;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PRT_CAT_NO", nullable = false)
	public AfPartnerCategory getAfPartnerCategory() {
		return afPartnerCategory;
	}


	public void setAfPartnerCategory(AfPartnerCategory afPartnerCategory) {
		this.afPartnerCategory = afPartnerCategory;
	}
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "AMZ_CAT_NO", unique = true, nullable = false)
	public Integer getAmzCatNo() {
		return amzCatNo;
	}


	public void setAmzCatNo(Integer amzCatNo) {
		this.amzCatNo = amzCatNo;
	}
	
	
	

	

	@Column(name = "AMZ_CATEGORY_ID", nullable = false)
	public String getAmzCategoryId() {
		return amzCategoryId;
	}


	public void setAmzCategoryId(String amzCategoryId) {
		this.amzCategoryId = amzCategoryId;
	}

	@Column(name = "AMZ_CATEGORY_NAME", length = 150)
	public String getAmzCategoryName() {
		return amzCategoryName;
	}


	public void setAmzCategoryName(String amzCategoryName) {
		this.amzCategoryName = amzCategoryName;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_TIME", length = 0)
	public Date getCreatedTime() {
		return createdTime;
	}


	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	@Column(name = "CREATED_BY")
	public Integer getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MODIFIED_TIME", length = 0)
	public Date getModifiedTime() {
		return modifiedTime;
	}


	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime = modifiedTime;
	}

	@Column(name = "MODIFIED_BY")
	public Integer getModifiedBy() {
		return modifiedBy;
	}


	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	
	@Column(name = "AMAZON_CATEGORY_STATUS", nullable = false)
	public byte getAmazonCategoryStatus() {
		return amazonCategoryStatus;
	}


	public void setAmazonCategoryStatus(byte amazonCategoryStatus) {
		this.amazonCategoryStatus = amazonCategoryStatus;
	}


	@Column(name = "STATUS", nullable = false)
	public byte getStatus() {
		return status;
	}


	public void setStatus(byte status) {
		this.status = status;
	}

	public Object clone() throws CloneNotSupportedException{  
		return super.clone();  
		}
	
	
	
	
}
