package com.af.admin.entity;

// Generated Dec 9, 2016 5:23:47 PM by Hibernate Tools 4.3.1

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * AfEmailSendingDetails generated by hbm2java
 */
@Entity
@Table(name = "AF_EMAIL_SENDING_DETAILS")
public class AfEmailSendingDetails implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer afEmailNo;
	private AfTransactionDetails afTransactionDetails;
	private String jpNumber;
	private String title;
	private String subject;
	private String emailBody;
	private String fromEmailId;
	private String toEmailId;
	private String replyToEmailId;
	private String returnPathEmailId;
	private String attachmentPath;
	private byte isMailSend;
	private Date createdTime;
	private Date modifiedTime;
	private byte status;

	public AfEmailSendingDetails() {
	}

	public AfEmailSendingDetails(byte isMailSend, byte status) {
		this.isMailSend = isMailSend;
		this.status = status;
	}

	public AfEmailSendingDetails(AfTransactionDetails afTransactionDetails,
			String jpNumber, String title, String subject, String emailBody,
			String fromEmailId, String toEmailId, String replyToEmailId,
			String returnPathEmailId, String attachmentPath, byte isMailSend,
			Date createdTime, Date modifiedTime, byte status) {
		this.afTransactionDetails = afTransactionDetails;
		this.jpNumber = jpNumber;
		this.title = title;
		this.subject = subject;
		this.emailBody = emailBody;
		this.fromEmailId = fromEmailId;
		this.toEmailId = toEmailId;
		this.replyToEmailId = replyToEmailId;
		this.returnPathEmailId = returnPathEmailId;
		this.attachmentPath = attachmentPath;
		this.isMailSend = isMailSend;
		this.createdTime = createdTime;
		this.modifiedTime = modifiedTime;
		this.status = status;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "AF_EMAIL_NO", unique = true, nullable = false)
	public Integer getAfEmailNo() {
		return this.afEmailNo;
	}

	public void setAfEmailNo(Integer afEmailNo) {
		this.afEmailNo = afEmailNo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TXN_DTL_NO")
	public AfTransactionDetails getAfTransactionDetails() {
		return this.afTransactionDetails;
	}

	public void setAfTransactionDetails(
			AfTransactionDetails afTransactionDetails) {
		this.afTransactionDetails = afTransactionDetails;
	}

	@Column(name = "JP_NUMBER", length = 25)
	public String getJpNumber() {
		return this.jpNumber;
	}

	public void setJpNumber(String jpNumber) {
		this.jpNumber = jpNumber;
	}

	@Column(name = "TITLE", length = 100)
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "SUBJECT", length = 100)
	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	@Column(name = "EMAIL_BODY", length = 65535)
	public String getEmailBody() {
		return this.emailBody;
	}

	public void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}

	@Column(name = "FROM_EMAIL_ID")
	public String getFromEmailId() {
		return this.fromEmailId;
	}

	public void setFromEmailId(String fromEmailId) {
		this.fromEmailId = fromEmailId;
	}

	@Column(name = "TO_EMAIL_ID")
	public String getToEmailId() {
		return this.toEmailId;
	}

	public void setToEmailId(String toEmailId) {
		this.toEmailId = toEmailId;
	}

	@Column(name = "REPLY_TO_EMAIL_ID")
	public String getReplyToEmailId() {
		return this.replyToEmailId;
	}

	public void setReplyToEmailId(String replyToEmailId) {
		this.replyToEmailId = replyToEmailId;
	}

	@Column(name = "RETURN_PATH_EMAIL_ID")
	public String getReturnPathEmailId() {
		return this.returnPathEmailId;
	}

	public void setReturnPathEmailId(String returnPathEmailId) {
		this.returnPathEmailId = returnPathEmailId;
	}

	@Column(name = "ATTACHMENT_PATH", length = 100)
	public String getAttachmentPath() {
		return this.attachmentPath;
	}

	public void setAttachmentPath(String attachmentPath) {
		this.attachmentPath = attachmentPath;
	}

	@Column(name = "IS_MAIL_SEND", nullable = false)
	public byte getIsMailSend() {
		return this.isMailSend;
	}

	public void setIsMailSend(byte isMailSend) {
		this.isMailSend = isMailSend;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_TIME", length = 0)
	public Date getCreatedTime() {
		return this.createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MODIFIED_TIME", length = 0)
	public Date getModifiedTime() {
		return this.modifiedTime;
	}

	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime = modifiedTime;
	}

	@Column(name = "STATUS", nullable = false)
	public byte getStatus() {
		return this.status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

}
