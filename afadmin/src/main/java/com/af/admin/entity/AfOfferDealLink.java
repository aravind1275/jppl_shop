package com.af.admin.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "AF_OFFER_DEAL_LINK")
public class AfOfferDealLink implements java.io.Serializable{
	
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Integer lmdId;
	private String store;
	private String offerText;
	private String title;
	private String description;
	private String code;
	private String termsAndConditions;
	private String categories;
	private String featured;
	private String url;
	private String smartLink;
	private String image_url;
	private String type;
	private String offer;
	private String offerValue;
	private String status;
	private String startDate;
	private String endDate;
	private byte checkStatus;
	
	private String partnerMapName;
	
	public  AfOfferDealLink(){}

	public AfOfferDealLink(Integer lmdId, String store, String offerText, String title, String description, String code,
			String termsAndConditions, String categories, String featured, String url, String smartLink,
			String image_url, String type, String offer, String offerValue, String status, String startDate,
			String endDate,String partnerMapName) {
		super();
		this.lmdId = lmdId;
		this.store = store;
		this.offerText = offerText;
		this.title = title;
		this.description = description;
		this.code = code;
		this.termsAndConditions = termsAndConditions;
		this.categories = categories;
		this.featured = featured;
		this.url = url;
		this.smartLink = smartLink;
		this.image_url = image_url;
		this.type = type;
		this.offer = offer;
		this.offerValue = offerValue;
		this.status = status;
		this.startDate = startDate;
		this.endDate = endDate;
		this.partnerMapName = partnerMapName;
	}
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "LMD_ID", unique = true, nullable = false)
	public Integer getLmdId() {
		return lmdId;
	}

	public void setLmdId(Integer lmdId) {
		this.lmdId = lmdId;
	}

	
	

	@Column(name = "STORE")
	public String getStore() {
		return store;
	}

	public void setStore(String store) {
		this.store = store;
	}

	@Column(name = "OFFER_TEXT")
	public String getOfferText() {
		return offerText;
	}

	public void setOfferText(String offerText) {
		this.offerText = offerText;
	}

	@Column(name = "TITLE")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "DESCRIPTION")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "CODE")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "TERM_CONDITION")
	public String getTermsAndConditions() {
		return termsAndConditions;
	}

	public void setTermsAndConditions(String termsAndConditions) {
		this.termsAndConditions = termsAndConditions;
	}

	@Column(name = "CATEGORIES")
	public String getCategories() {
		return categories;
	}

	public void setCategories(String categories) {
		this.categories = categories;
	}

	@Column(name = "FEATURED")
	public String getFeatured() {
		return featured;
	}

	public void setFeatured(String featured) {
		this.featured = featured;
	}

	@Column(name = "URL")
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Column(name = "SMART_LINK")
	public String getSmartLink() {
		return smartLink;
	}

	public void setSmartLink(String smartLink) {
		this.smartLink = smartLink;
	}

	@Column(name = "IMAGE_URL")
	public String getImage_url() {
		return image_url;
	}

	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}

	@Column(name = "TYPE")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "OFFER")
	public String getOffer() {
		return offer;
	}

	public void setOffer(String offer) {
		this.offer = offer;
	}
	
	@Column(name = "OFFER_VALUE")
	public String getOfferValue() {
		return offerValue;
	}

	public void setOfferValue(String offerValue) {
		this.offerValue = offerValue;
	}
	@Column(name = "STATUS")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	//@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "START_DATE", length = 0)
	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	@Column(name = "END_DATE", length = 0)
	public String getEndDate() {
		return endDate;
	}

	//@Temporal(TemporalType.TIMESTAMP)
	
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	@Column(name = "PARTNER_MAP_NAME")
	public String getPartnerMapName() {
		return partnerMapName;
	}

	public void setPartnerMapName(String partnerMapName) {
		this.partnerMapName = partnerMapName;
	}

	/*@Column(name = "CHECKSTATUS")
	public byte getCheckStatus() {
		return checkStatus;
	}

	public void setCheckStatus(byte checkStatus) {
		this.checkStatus = checkStatus;
	}*/
	
	
	
}
