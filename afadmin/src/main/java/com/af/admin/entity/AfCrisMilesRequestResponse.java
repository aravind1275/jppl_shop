package com.af.admin.entity;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "AF_CRIS_MILES_REQUEST_RESPONSE")
public class AfCrisMilesRequestResponse implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer afMilesId;
	private String jpNumber;
	private String orderId;
	private String crisRequest;
	private String crisResponse;
	private Date createdTime;
	public AfCrisMilesRequestResponse() {
	}

	public AfCrisMilesRequestResponse(Integer afMilesId, String jpNumber,
			String crisRequest) {
		this.afMilesId = afMilesId;
		this.jpNumber = jpNumber;
		this.crisRequest = crisRequest;
	}

	public AfCrisMilesRequestResponse(Integer afMilesId, String jpNumber, String orderId,
			String crisRequest, Date createdTime) {
		this.afMilesId = afMilesId;
		this.jpNumber = jpNumber;
		this.orderId = orderId;
		this.crisRequest = crisRequest;
		this.createdTime = createdTime;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "AF_MILES_ID", unique = true, nullable = false)
	public Integer getAfMilesId() {
		return this.afMilesId;
	}

	public void setAfMilesId(Integer afMilesId) {
		this.afMilesId = afMilesId;
	}

	@Column(name = "JP_NUMBER", nullable = false, length = 11)
	public String getJpNumber() {
		return this.jpNumber;
	}

	public void setJpNumber(String jpNumber) {
		this.jpNumber = jpNumber;
	}
	
	@Column(name = "ORDER_ID", nullable = false, length = 50)
	public String getOrderId() {
		return this.orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	@Column(name = "CRIS_REQUEST", length = 65535)
	public String getCrisRequest() {
		return this.crisRequest;
	}

	public void setCrisRequest(String crisRequest) {
		this.crisRequest = crisRequest;
	}
	
	@Column(name = "CRIS_RESPONSE", length = 65535)
	public String getCrisResponse() {
		return crisResponse;
	}

	public void setCrisResponse(String crisResponse) {
		this.crisResponse = crisResponse;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_TIME", length = 0)
	public Date getCreatedTime() {
		return this.createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
}
