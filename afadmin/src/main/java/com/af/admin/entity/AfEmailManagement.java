package com.af.admin.entity;

// Generated 18 Jul, 2016 4:03:34 PM by Hibernate Tools 4.3.1

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * AfContentManagement generated by hbm2java
 */
@Entity
@Table(name = "AF_EMAIL_MANAGEMENT")
public class AfEmailManagement implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer emId;
	private String name;
	private String title;
	private String content;
	private Date createdTime;
	private Integer createdBy;
	private Date modifiedTime;
	private Integer modifiedBy;
	private byte status;
	private byte emailStatus;


	public AfEmailManagement() {
	}

	public AfEmailManagement(String content, byte status) {
		this.content = content;
		this.status = status;
	}

	public AfEmailManagement(String name, String title, String content,
			Date createdTime, Integer createdBy, Date modifiedTime,
			Integer modifiedBy, byte status) {
		this.name = name;
		this.title = title;
		this.content = content;
		this.createdTime = createdTime;
		this.createdBy = createdBy;
		this.modifiedTime = modifiedTime;
		this.modifiedBy = modifiedBy;
		this.status = status;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "EM_ID", unique = true, nullable = false)
	public Integer getEmId() {
		return this.emId;
	}

	public void setEmId(Integer emId) {
		this.emId = emId;
	}

	@Column(name = "NAME", length = 20)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "TITLE", length = 50)
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "CONTENT", nullable = false, length = 65535)
	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_TIME", length = 0)
	public Date getCreatedTime() {
		return this.createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	@Column(name = "CREATED_BY")
	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MODIFIED_TIME", length = 0)
	public Date getModifiedTime() {
		return this.modifiedTime;
	}

	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime = modifiedTime;
	}

	@Column(name = "MODIFIED_BY")
	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@Column(name = "STATUS", nullable = false)
	public byte getStatus() {
		return this.status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}
	
	@Column(name = "EMAIL_STATUS", nullable = false)
	public byte getEmailStatus() {
		return this.emailStatus;
	}
	
	public void setEmailStatus(byte emailStatus) {
		this.emailStatus = emailStatus;
	}

}
