package com.af.admin.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "AF_USER_ACTIVITY_LOG")
public class AfUserActivityLog implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer userActId;
	private String userActivity;
	private String userOldFieldValue;
	private String userNewFieldValue;
	private Date userActCreatedDateTime;
	private String userActCreatedBy;
	private String userActIpVal;
	private String userActModule;
	
	public AfUserActivityLog(){
		
	}
	
	
	public AfUserActivityLog(Integer userActId, String userActivity, String userOldFieldValue, String userNewFieldValue,
			Date userActCreatedDateTime, String userActCreatedBy, String userActIpVal, String userActModule) {
		
		this.userActId = userActId;
		this.userActivity = userActivity;
		this.userOldFieldValue = userOldFieldValue;
		this.userNewFieldValue = userNewFieldValue;
		this.userActCreatedDateTime = userActCreatedDateTime;
		this.userActCreatedBy = userActCreatedBy;
		this.userActIpVal = userActIpVal;
		this.userActModule = userActModule;
	}
	
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "UA_ID", unique = true, nullable = false)
	public Integer getUserActId() {
		return userActId;
	}
	public void setUserActId(Integer userActId) {
		this.userActId = userActId;
	}
	
	@Column(name = "ACTIVITY")
	public String getUserActivity() {
		return userActivity;
	}
	public void setUserActivity(String userActivity) {
		this.userActivity = userActivity;
	}
	
	@Column(name = "OLD_FIELD_VALUES")
	public String getUserOldFieldValue() {
		return userOldFieldValue;
	}
	public void setUserOldFieldValue(String userOldFieldValue) {
		this.userOldFieldValue = userOldFieldValue;
	}
	
	@Column(name = "NEW_FIELD_VALUES")
	public String getUserNewFieldValue() {
		return userNewFieldValue;
	}
	public void setUserNewFieldValue(String userNewFieldValue) {
		this.userNewFieldValue = userNewFieldValue;
	}
	
	@Column(name = "CREATED_TIME")
	public Date getUserActCreatedDateTime() {
		return userActCreatedDateTime;
	}
	
	public void setUserActCreatedDateTime(Date userActCreatedDateTime) {
		this.userActCreatedDateTime = userActCreatedDateTime;
	}
	
	@Column(name = "CREATED_BY")
	public String getUserActCreatedBy() {
		return userActCreatedBy;
	}
	public void setUserActCreatedBy(String userActCreatedBy) {
		this.userActCreatedBy = userActCreatedBy;
	}
	
	@Column(name = "IP_ADDRESS")
	public String getUserActIpVal() {
		return userActIpVal;
	}
	public void setUserActIpVal(String userActIpVal) {
		this.userActIpVal = userActIpVal;
	}

	@Column(name = "ACTIVITY_MODULE")
	public String getUserActModule() {
		return userActModule;
	}


	public void setUserActModule(String userActModule) {
		this.userActModule = userActModule;
	}
	
}
