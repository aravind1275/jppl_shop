package com.af.admin.entity;

import static javax.persistence.GenerationType.IDENTITY;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name = "AF_PARTNER_MAPPING")
public class AfPartnerMapping implements java.io.Serializable{
	
	private Integer PrtMapNo;
	private String  partnerMappingName;
	private String  partnerType;
	private AfPartner afPartner;
	private byte status;
	
	private byte  allowLandingUrl;
	
	public AfPartnerMapping() {
		super();
	}
	
	public AfPartnerMapping(Integer prtMapNo, String partnerMappingName, AfPartner afPartner, byte status,String partnerType,byte  allowLandingUrl) {
		super();
		this.PrtMapNo = prtMapNo;
		this.partnerMappingName = partnerMappingName;
		this.afPartner = afPartner;
		this.status = status;
		this.partnerType = partnerType;
		this.allowLandingUrl = allowLandingUrl;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "PRT_MAP_NO", nullable = false)
	public Integer getPrtMapNo() {
		return PrtMapNo;
	}
	public void setPrtMapNo(Integer prtMapNo) {
		PrtMapNo = prtMapNo;
	}
	
	
	@Column(name = "PARTNER_MAPPING_NAME", length = 150)
	public String getPartnerMappingName() {
		return partnerMappingName;
	}
	public void setPartnerMappingName(String partnerMappingName) {
		this.partnerMappingName = partnerMappingName;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PRT_NO", nullable = false)
	public AfPartner getAfPartner() {
		return afPartner;
	}
	public void setAfPartner(AfPartner afPartner) {
		this.afPartner = afPartner;
	}
	
	@Column(name = "STATUS", nullable = false)
	public byte getStatus() {
		return status;
	}
	public void setStatus(byte status) {
		this.status = status;
	}

	@Column(name = "PARTNER_TYPE", nullable = false)
	public String getPartnerType() {
		return partnerType;
	}

	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}

	@Column(name = "PARTNER_LANDING_URL")
	public byte getAllowLandingUrl() {
		return allowLandingUrl;
	}

	public void setAllowLandingUrl(byte allowLandingUrl) {
		this.allowLandingUrl = allowLandingUrl;
	}

	

	

	
	
}
