package com.af.admin.service;

import java.util.List;

import com.af.admin.beans.UserActivityLogBean;

public interface UserActivityLogService {

	public List<UserActivityLogBean> getUserActLogList(String userActFromDate, String userActTodate);
	
	public List<UserActivityLogBean> getUserActLogListDefault(String currentDate);
}
