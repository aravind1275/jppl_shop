package com.af.admin.service.impl;

import java.net.URLEncoder;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.af.admin.DAO.NotificationDAO;
import com.af.admin.DAO.PartnerManagementDAO;
import com.af.admin.DAO.UserActivityLogDAO;
import com.af.admin.beans.AfNotificationBean;
import com.af.admin.beans.BannerBean;
import com.af.admin.constants.AfConstants;
import com.af.admin.entity.AfBanner;
import com.af.admin.entity.AfNotification;
import com.af.admin.entity.AfPartner;
import com.af.admin.entity.AfUserActivityLog;
import com.af.admin.service.NotificationService;
import com.af.admin.utils.AdminUserActivityUtil;
import com.af.admin.utils.ServiceProviderUtil;

@Component
@Transactional(readOnly=true)
public class NotificationServiceImpl  implements NotificationService{
	private Logger logger = LogManager.getLogger(BannerManagementServiceimpl.class.getName());
	
	@Autowired
	NotificationDAO notificationDAO;
	
	@Autowired
	PartnerManagementDAO partnerManagementDAO;
	
	@Autowired
	UserActivityLogDAO userActLogDao;
	
	@Autowired
	AdminUserActivityUtil adminUserActivityUtil;
	
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
	@Override
	public List<AfNotificationBean> getNotificationList() {
		List<AfNotification> afNotificationList=null;
		List<AfNotificationBean> notificationList=new ArrayList<>();
	
		int notificationStatus;
		AfPartner afPartner=null;
		String dateStr = "";
		Date dt = null;
		try{
			afNotificationList=notificationDAO.getNotification();
			for(AfNotification afNotification:afNotificationList){
				AfNotificationBean afNotificationBean=new AfNotificationBean();
				afNotificationBean.setNotificationNo(afNotification.getNotificationNo());
				afNotificationBean.setNotificationId(afNotification.getNotificationId());
				afNotificationBean.setNotificationName(afNotification.getNotificationName());
				String tAndC = afNotification.getNotificationText();
				if(tAndC != null){
					tAndC = URLEncoder.encode(tAndC, "UTF-8");
				}
				afNotificationBean.setNotificationText(tAndC!=null?tAndC:"");
				afNotificationBean.setNotificationTitle(afNotification.getNotificationTitle());
				afNotificationBean.setNotificationImageUrl(afNotification.getNotificationImageUrl());
				afNotificationBean.setNotificationLandingUrl(afNotification.getNotificationLandingUrl());
				afNotificationBean.setLoggedInStatus(afNotification.getLoggedInStatus());
				afNotificationBean.setNonLoggedInStatus(afNotification.getNonLoggedInstatus());
				
				notificationStatus=afNotification.getNotificationStatus();
				afNotificationBean.setNotificationStatusCode(notificationStatus);

				if(afNotification.getAfPartner() != null){
					afNotificationBean.setPartNo(String.valueOf(afNotification.getAfPartner().getPrtNo()));
					afPartner = partnerManagementDAO.getPartnerById(afNotification.getAfPartner().getPrtNo());
					afNotificationBean.setPartnerName(String.valueOf(afPartner.getPartnerName().toString()));
				}
				afNotificationBean.setTimeStamp(afNotification.getTimeStamp().toString());
				dt = afNotification.getNotificationStartDate();
				dateStr = ServiceProviderUtil.getInstance().getFormattedDate(afNotification.getNotificationStartDate(),"dd/MM/yyyy");
				if(dt != null && !dt.equals(""))
					afNotificationBean.setFromDate(dateStr);
				else
					afNotificationBean.setFromDate("");
				dt = afNotification.getNotificationEndDate();
				dateStr = ServiceProviderUtil.getInstance().getFormattedDate(afNotification.getNotificationEndDate(),"dd/MM/yyyy");
				if(dt != null && !dt.equals(""))
					afNotificationBean.setToDate(dateStr);
				else
					afNotificationBean.setToDate("");
							
				if(1==notificationStatus){
					afNotificationBean.setNotificationStatus(AfConstants.STATUS_ACTIVE);
					afNotificationBean.setNotificationStatusClass(AfConstants.STATUS_CLASS_GREEN);				
				}
				if(0==notificationStatus){
					afNotificationBean.setNotificationStatus(AfConstants.STATUS_IN_ACTIVE);
					afNotificationBean.setNotificationStatusClass(AfConstants.STATUS_CLASS_RED);				
				}
				notificationList.add(afNotificationBean);
			}
			
			
			
		}catch(Exception e){
			logger.error("@@@@ Exception in NotificationServiceImpl at getNotification() :", e);
		}
		return notificationList;
		
	}
	
	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String createNotification(AfNotificationBean notificationBean, HttpServletRequest request) {
		AfNotification afNotification=new AfNotification();
		String status="";
		Date startDate = null;
		Date endDate = null;
		String oldOrNewVal = "";
		try{
			
		afNotification.setNotificationId(notificationBean.getNotificationId());
		afNotification.setNotificationName(notificationBean.getNotificationName());
		afNotification.setNotificationText(notificationBean.getNotificationText());
		afNotification.setNotificationTitle(notificationBean.getNotificationTitle());
		afNotification.setNotificationImageUrl(notificationBean.getNotificationImageUrl().replaceAll(",",""));
		afNotification.setNotificationLandingUrl(notificationBean.getNotificationLandingUrl());
		
		
		
		if(notificationBean.getPartNo() !=null && notificationBean.getPartNo().length() > 0){
			afNotification.setAfPartner(partnerManagementDAO.getPartnerByPartnerNO(notificationBean.getPartNo()));
		}
		/*else{
			//afNotification.setAfPartner(null);
		}*/
		afNotification.setLoggedInStatus(notificationBean.getLoggedInStatus());
		afNotification.setNonLoggedInstatus(notificationBean.getNonLoggedInStatus());		
		
		
		afNotification.setNotificationStatus((byte)notificationBean.getNotificationStatusCode());		
		afNotification.setTimeStamp(Time.valueOf(notificationBean.getTimeStamp()));
		afNotification.setCreatedBy(1);
		afNotification.setCreatedTime(new Date());
		afNotification.setStatus((byte) 1);
		startDate = ServiceProviderUtil.getInstance().getDateFromString(notificationBean.getFromDate(), "dd/MM/yyyy");
		endDate = ServiceProviderUtil.getInstance().getDateFromString(notificationBean.getToDate(), "dd/MM/yyyy");
		
		afNotification.setNotificationStartDate(startDate);
		afNotification.setNotificationEndDate(endDate);
		
		String status1=notificationBean.getNotificationStatusCode()+"";
		status1 = status1.replaceAll("0","InActive");
		status1 = status1.replaceAll("1", "Active");
		
		String status2=notificationBean.getLoggedInStatus()+"";
		status2 = status2.replaceAll("0","InActive");
		status2 = status2.replaceAll("1", "Active");
		
		String status3=notificationBean.getNonLoggedInStatus()+"";
		status3 = status3.replaceAll("0","InActive");
		status3 = status3.replaceAll("1", "Active");
		
		status=notificationDAO.createNotification(afNotification);
		if("success".equalsIgnoreCase(status)){
			String ipAddress = request.getHeader("X-FORWARDED-FOR");
			if(ipAddress == null) {
				ipAddress = request.getRemoteAddr();
			}
			String partnerName= afNotification.getAfPartner()==null?"NA":afNotification.getAfPartner().getPartnerName();
			ipAddress = (null != ipAddress) ? ipAddress.trim() : "";
			AfUserActivityLog afPrtActLog=new AfUserActivityLog();

			oldOrNewVal=" Partners : "+partnerName+" | "+"Notification ID : "+notificationBean.getNotificationId()+" | "+
					""+"Notification Name: "+notificationBean.getNotificationName()+"|"+"Notification Text: "+notificationBean.getNotificationText()+"|"+"Notification Title: "+afNotification.getNotificationTitle()+" |"
							+ " "+"Landing URL: "+notificationBean.getNotificationLandingUrl()+" |"+" Logged In Status: "+status2+" |"+" nonLogged In Status: "+status3+"|"
							+ " "+"Notification Image : "+notificationBean.getNotificationImageUrl()+" |"+"Notification Status: "+status1+" |"
									+ ""+"Start Date : "+afNotification.getNotificationStartDate()+" | "+
					"End Date : "+afNotification.getNotificationEndDate()+" | "+"Timestamp of notification Trigger: "+notificationBean.getTimeStamp()+" |";
							
		
					afPrtActLog.setUserNewFieldValue(oldOrNewVal);
					afPrtActLog.setUserOldFieldValue("NA");
					afPrtActLog.setUserActivity("Create Notification");
					afPrtActLog.setUserActCreatedDateTime(new Date());
					afPrtActLog.setUserActIpVal(ipAddress);
					afPrtActLog.setUserActModule("Notification");
					afPrtActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
					
					userActLogDao.createUserActLog(afPrtActLog);
		}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in NotificationServiceImpl at createNotification() :", e);
			return "failure";
		}
		return status;
	}
    @Transactional(readOnly=false)
	@Override
	public String deleteNotification(Integer notificationNo, HttpServletRequest request) {
		AfNotification afNotification=null;
		String deleteStatus="";
		String oldOrNewVal = "";
		try{
			String ipAddress = request.getHeader("X-FORWARDED-FOR");
			if (ipAddress == null) {
				ipAddress = request.getRemoteAddr();
			}
			afNotification = notificationDAO.getNotificationid(notificationNo);
			String status1=afNotification.getNotificationStatus()+"";
			status1 = status1.replaceAll("0","InActive");
			status1 = status1.replaceAll("1", "Active");
		
			
			AfUserActivityLog updateUsActLog = new AfUserActivityLog();
			oldOrNewVal="Notification ID : "+afNotification.getNotificationId()+" | "+
					""+"Notification Name: "+afNotification.getNotificationName()+"| "+"Notification Title: "+afNotification.getNotificationTitle()+" |"+" Landing URL: "+afNotification.getNotificationLandingUrl()+" |"
					+ " "+"Notification Image : "+afNotification.getNotificationImageUrl()+" |"+"Notification Status : "+status1+" |";
							
			updateUsActLog.setUserActivity("Delete Notification ");
	    	updateUsActLog.setUserActCreatedDateTime(new Date());
	    	updateUsActLog.setUserNewFieldValue("NA");
	    	updateUsActLog.setUserActIpVal(ipAddress);
	    	updateUsActLog.setUserActModule("Notification ");
	    	updateUsActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
	    	updateUsActLog.setUserOldFieldValue(oldOrNewVal);
	    	userActLogDao.updateUserLog(updateUsActLog);
			deleteStatus=notificationDAO.deleteNotification(notificationNo);
		}
		catch(Exception e){
			logger.error("@@@@ Exception in NotificationServiceImpl at deleteNotification() :", e);
			deleteStatus="failure";
			return deleteStatus;
		}
		return deleteStatus;
	}

	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String updateNotification(JSONObject userJson, HttpServletRequest request) {
		Integer notificationNo = -1;		
		String status="";
		Date startDate = null;
		Date endDate = null;
		AfNotification afNotificationOld=null;
		String replaceInOld="";
		String replaceInNew="";
		AfNotification afNotification=null;
		try{
			notificationNo = userJson.getInt("notificationNo");
			afNotification=notificationDAO.getNotificationid(notificationNo);
			afNotificationOld=(AfNotification) afNotification.clone();
			afNotification.setNotificationId(userJson.getString("notificationId"));
			afNotification.setNotificationName(userJson.getString("notificationName")); 
			if(!userJson.getString("notificationText").trim().isEmpty()){
				afNotification.setNotificationText(userJson.getString("notificationText"));
			}
			afNotification.setNotificationTitle(userJson.getString("notificationTitle"));
			afNotification.setLoggedInStatus((byte)userJson.getInt("loggedInNotification"));
			afNotification.setNonLoggedInstatus((byte)userJson.getInt("nonLoggedInNotification"));
			afNotification.setNotificationImageUrl(userJson.getString("editNotificationImageUrl"));
			afNotification.setNotificationLandingUrl(userJson.getString("notificationLandingUrl"));
			afNotification.setNotificationStatus((byte) userJson.getInt("notificationStatus"));
			afNotification.setTimeStamp(Time.valueOf(userJson.getString("timeStamp")));
			startDate = ServiceProviderUtil.getInstance().getDateFromString(userJson.getString("editFromDate"), "dd/MM/yyyy");
			endDate = ServiceProviderUtil.getInstance().getDateFromString(userJson.getString("editToDate"), "dd/MM/yyyy");
			afNotification.setNotificationStartDate(startDate);
			afNotification.setNotificationEndDate(endDate);
			
			
			afNotification.setModifiedBy(1);
			afNotification.setModifiedTime(new Date());
			String prtNo = userJson.getString("prtNo");
			
			if(!"".equals(prtNo)){
				afNotification.setAfPartner(partnerManagementDAO.getPartnerByPartnerNO(prtNo));
			} else {
				afNotification.setAfPartner(null);
			}
			String statusInactive=	afNotificationOld.getNotificationStatus()==0?"InActive":"Active";
	     	String statusActive=afNotification.getNotificationStatus()==0?"InActive":"Active";
	     	
	     	String logStatusInactive=	afNotificationOld.getLoggedInStatus()==0?"No":"Yes";
	     	String logStatusActive=afNotification.getLoggedInStatus()==0?"No":"Yes";
	     	
	     	String nonLogStatusInactive=	afNotificationOld.getNonLoggedInstatus()==0?"No":"Yes";
	     	String nonLogStatusActive=afNotification.getNonLoggedInstatus()==0?"No":"Yes";
	     	
	     	String notificationLandingUrl = afNotification.getNotificationLandingUrl().length() !=0?afNotification.getNotificationLandingUrl():"NA";
			String notificationLandingUrlOld = afNotificationOld.getNotificationLandingUrl().length() !=0?afNotificationOld.getNotificationLandingUrl():"NA";
			
			/*String Partner = afNotification.getAfPartner().getPartnerName().length() !=0?afNotification.getAfPartner().getPartnerName():"NA";
			String PartnerOld = afNotificationOld.getAfPartner().getPartnerName().length() !=0?afNotificationOld.getAfPartner().getPartnerName():"NA";*/
			
			String Partner= afNotification.getAfPartner()==null?"NA":afNotification.getAfPartner().getPartnerName();
			String PartnerOld=afNotificationOld.getAfPartner()==null?"NA":afNotificationOld.getAfPartner().getPartnerName();
			
			status=notificationDAO.updateNotification(afNotification);
			if("success".equalsIgnoreCase(status)){
				String ipAddress = request.getHeader("X-FORWARDED-FOR");
				if (ipAddress == null) {
					ipAddress = request.getRemoteAddr();
				}
				AfUserActivityLog updateUsActLog = new AfUserActivityLog();
				new LinkedHashMap<>();
				
					updateUsActLog.setUserActivity("Update Notification-");
					updateUsActLog.setUserActCreatedDateTime(new Date());
					updateUsActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
					updateUsActLog.setUserActIpVal(ipAddress);
					updateUsActLog.setUserActModule("Notification");
					if(Partner!=PartnerOld){
						replaceInOld=replaceInOld+"|Partner Name :"+PartnerOld;
					    replaceInNew=replaceInNew+"|Partner Name :"+Partner;
					}
					if(!afNotification.getNotificationId().equalsIgnoreCase(afNotificationOld.getNotificationId())){
						replaceInOld=replaceInOld+"|Notification Id:"+afNotificationOld.getNotificationId();
						replaceInNew=replaceInNew+"|Notification Id:"+afNotification.getNotificationId();
					}
					if(!afNotification.getNotificationName().equalsIgnoreCase(afNotificationOld.getNotificationName())){
						replaceInOld=replaceInOld+"|Notification Name:"+afNotificationOld.getNotificationName();
						replaceInNew=replaceInNew+"|Notification Name:"+afNotification.getNotificationName();
					}
					if(!afNotification.getNotificationText().equalsIgnoreCase(afNotificationOld.getNotificationText())){
						replaceInOld=replaceInOld+"|Notification Text:"+afNotificationOld.getNotificationText();
						replaceInNew=replaceInNew+"|Notification Text:"+afNotification.getNotificationText();
					}
					if(!afNotification.getNotificationTitle().equalsIgnoreCase(afNotificationOld.getNotificationTitle())){
						replaceInOld=replaceInOld+"|Notification Title:"+afNotificationOld.getNotificationTitle();
						replaceInNew=replaceInNew+"|Notification Title:"+afNotification.getNotificationTitle();
					}
					if(!notificationLandingUrl.equalsIgnoreCase(notificationLandingUrlOld)){
						replaceInOld=replaceInOld+"|Notification LandUrl:"+notificationLandingUrlOld;
					     replaceInNew=replaceInNew+"|Notification LandUrl:"+notificationLandingUrl;
					}
					if(!afNotification.getNotificationImageUrl().equalsIgnoreCase(afNotificationOld.getNotificationImageUrl())){
						replaceInOld=replaceInOld+"|Notification ImageUrl:"+afNotificationOld.getNotificationImageUrl();
						replaceInNew=replaceInNew+"|Notification ImageUrl:"+afNotification.getNotificationImageUrl();
					}
					if(!afNotification.getTimeStamp().equals(afNotificationOld.getTimeStamp())){
						replaceInOld=replaceInOld+"|Notification TimeStamp:"+afNotificationOld.getTimeStamp();
						replaceInNew=replaceInNew+"|Notification TimeStamp:"+afNotification.getTimeStamp();
					}
					if(afNotification.getNotificationStatus()!=afNotificationOld.getNotificationStatus()){
						replaceInOld=replaceInOld+"|Notification Status:"+statusInactive;
						replaceInNew=replaceInNew+"|NotificationStatus:"+statusActive;
					}
					if(afNotification.getLoggedInStatus()!=afNotificationOld.getLoggedInStatus()){
						replaceInOld=replaceInOld+"|Logged Status:"+logStatusInactive;
						replaceInNew=replaceInNew+"|Logged Status:"+logStatusActive;
					}
					if(afNotification.getNonLoggedInstatus()!=afNotificationOld.getNonLoggedInstatus()){
						replaceInOld=replaceInOld+"|NonLogged Status:"+nonLogStatusInactive;
						replaceInNew=replaceInNew+"|NonLogged Status:"+nonLogStatusActive;
					}
					if(!afNotification.getNotificationStartDate().equals(afNotificationOld.getNotificationStartDate())){
						replaceInOld=replaceInOld+"|Notification StartDate:"+simpleDateFormat.format(afNotificationOld.getNotificationStartDate());
						replaceInNew=replaceInNew+"|Notification StartDate:"+simpleDateFormat.format(afNotification.getNotificationStartDate());
					}
					
					if(!afNotification.getNotificationEndDate().equals(afNotificationOld.getNotificationEndDate())){
						replaceInOld=replaceInOld+"|Notification EndDate:"+simpleDateFormat.format(afNotificationOld.getNotificationEndDate());
						replaceInNew=replaceInNew+"|Notification EndDate:"+simpleDateFormat.format(afNotification.getNotificationEndDate());
					}
					if(replaceInNew.isEmpty() && replaceInOld.isEmpty()) {
                        replaceInOld=replaceInOld+"|ModifiedTime:"+simpleDateFormat.format(afNotificationOld.getModifiedTime());
                        replaceInNew=replaceInNew+"|ModifiedTime:"+simpleDateFormat.format(afNotification.getModifiedTime());
                    }
					updateUsActLog.setUserNewFieldValue(replaceInNew);
		    		updateUsActLog.setUserOldFieldValue(replaceInOld);
					userActLogDao.updateUserLog(updateUsActLog);
				
				
			}
		}
		catch(Exception e){
			logger.error("@@@@ Exception in NotificationServiceImpl at updateNotification() :", e);
		}
		return status;
	}

	@Override
	public Boolean checkNotificationID(String notificationId) {
		boolean notificationIdExist=false;
		try{
			notificationIdExist=notificationDAO.checkNotificationID(notificationId);
		}catch(Exception e){
			logger.error("@@Exception in NotificationServiceImpl checkNotificationID", e);
		}
		return notificationIdExist;
	}
}
