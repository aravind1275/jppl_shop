package com.af.admin.service;

import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import com.af.admin.beans.RecommendedProductBean;


public interface RecommendedProductService {
	
	public List<RecommendedProductBean> getRecommendedProducts();
	public String createRecommendedProduct(RecommendedProductBean recommendedProductBean);
	public boolean checkRecommendedProduct(Map<String, String> rpmap);
	public boolean checkRecommendedProductPosition(String prtPrdPos);
	public RecommendedProductBean getRecommendedProductByNo(String rpNo);
	public String updateRecommendedProduct(RecommendedProductBean recommendedProductBean);	
	public String deleteRecommendedProduct(Integer bnrNo);
	public Map<String, String> getProductList(String prtCatNo,String keyword);
	
	
}
