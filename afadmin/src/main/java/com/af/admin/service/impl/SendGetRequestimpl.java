package com.af.admin.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.zip.GZIPInputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.af.admin.service.SendGetRequest;
@Component
public class SendGetRequestimpl implements SendGetRequest {
	private Logger logger = LogManager.getLogger(SendGetRequestimpl.class.getName());
	synchronized public String  sendGet(String requesturl){
		StringBuilder sb = new StringBuilder();

		try{
			  URL url = new URL(requesturl);
			  HttpURLConnection conn =
			      (HttpURLConnection) url.openConnection();
			  conn.setRequestProperty("Accept-Encoding", "gzip");	
			  if (conn.getResponseCode() != 200) {
			    throw new IOException(conn.getResponseMessage());
			  }
			  BufferedReader rd = new BufferedReader(
			      new InputStreamReader(conn.getInputStream()));
			  if ("gzip".equals(conn.getContentEncoding())) {
				  rd = new BufferedReader(new InputStreamReader(new GZIPInputStream(conn.getInputStream())));
			      }
			      else {
			    	  rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			      }
			  String line;
			  while ((line = rd.readLine()) != null) {
			    sb.append(line);
			  }
			  rd.close();
			  conn.disconnect();
		}catch(Exception e){
			sb.append("failure");
			logger.error("@@@@ Exception /SendGetRequestimpl e", e);
		}
			return sb.toString();
	}
}
