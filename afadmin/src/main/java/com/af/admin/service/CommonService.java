package com.af.admin.service;

import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.ModelMap;
import org.springframework.web.multipart.MultipartFile;

import com.af.admin.beans.AdminMenu;
import com.af.admin.beans.AdminRolesBean;
import com.af.admin.beans.AfPermissionValuesBean;
import com.af.admin.beans.PartnerCategoryBean;
import com.af.admin.beans.SellerTextBean;
import com.af.admin.beans.SeoBean;
import com.af.admin.entity.AfEnumValues;



public interface CommonService {

	 public List<AdminMenu> getMenus(List<AdminMenu> adminMenus,Map<String ,List<AdminMenu>> submenuMap,Integer userRole);
		
	 public List<AdminRolesBean> getAdminRoles();

	 public List<AfPermissionValuesBean> getPermissionValues();

	 public Map<Byte, String>  getEnumValues(String typeName);
	 
	 public String saveImage(MultipartFile file,String moduleName);
	 
	 void saveExcel(String filename, MultipartFile excel, HttpServletRequest request);

	 public List<String> getPermissionList(Integer userRole);
 
	 public Map<String, String> getPartnerCategoryListMap(String partner);
	 
	 public List<PartnerCategoryBean> getPartnerCategoryListBean(String partner);
	 
	 public Map<String, String> getPartnerProductList(String prtCatNo, String rpName);

	 public String cerateKeyWord(List<String> keyWord,String filePath,String prtNo, HttpServletRequest request, String prtType);
	 
	 public String deleteFile(Integer partnerID, HttpServletRequest request);
	 
	 public String getFileContent(MultipartFile file, HttpServletRequest request);
	
	 public HashSet<String> getBrandForParnerCategory(String prtCatNo);
	 
	 public String  getJPMilesEnumValues(String typeName);
	 
	public void cerateOffersAndDeals(List<String> offersAndDealsList, String filePath, int NoOfcolumns, ModelMap model);
	 public List<SeoBean> getSEOData();
	 public String saveSEOData(SeoBean seoBean, HttpServletRequest request);
	 
	 public String clearCache();
	 
	 public AfEnumValues getEnumValuebyType(String TypeName);
	 
	 public String saveSellerText(String value,HttpServletRequest request);
	 
	 public Map<Integer, String> getBrandList(String brName);
}