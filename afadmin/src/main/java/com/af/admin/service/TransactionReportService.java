package com.af.admin.service;

import java.util.List;

import com.af.admin.beans.TransactionReportBean;

public interface TransactionReportService {

	List<TransactionReportBean> getTransactionDetails(String trackingId);
}
