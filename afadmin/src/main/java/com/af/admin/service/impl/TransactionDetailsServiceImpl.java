package com.af.admin.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.af.admin.DAO.TransactionDetailsDAO;
import com.af.admin.beans.ExportBean;
import com.af.admin.beans.TransactionDetailsBean;
import com.af.admin.constants.AfConstants;
import com.af.admin.entity.AfEnumValues;
import com.af.admin.entity.AfPartner;
import com.af.admin.entity.AfPartnerProduct;
import com.af.admin.entity.AfTransaction;
import com.af.admin.entity.AfTransactionDetails;
import com.af.admin.service.CommonService;
import com.af.admin.service.TransactionDetailsService;
import com.af.admin.utils.ServiceProviderUtil;

@Component
@Transactional(readOnly=true)
public class TransactionDetailsServiceImpl implements TransactionDetailsService {
	private Logger logger = LogManager.getLogger(TransactionDetailsServiceImpl.class.getName());
	@Autowired
	TransactionDetailsDAO transactionDetailsDAO;
	
	@Autowired
	CommonService commonService;
	
	
	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public List<TransactionDetailsBean> getTransactionDetails(String jpNumber) {
		List<AfTransactionDetails> afTransactionDetailsList= null;
		List<AfTransaction> afTransactionList= null;
		List<TransactionDetailsBean> transactionDetailsBeanList= new ArrayList<TransactionDetailsBean>();
		List<AfPartnerProduct> afPartnerProductList = null;
		logger.info("inside getTransactionDetails () jpNumber "+jpNumber);
		try{
			String dateStr = "", productId="", partnerProductName="",partnerName="",partnerCategoryName="";
			int productStatus;
			Date date = null;
			
			afTransactionList = transactionDetailsDAO.getTransactionId(jpNumber,"","");
			for(AfTransaction afTransaction:afTransactionList){    
			          
			                         System.out.println("-------afTransaction.getTransactionId() ->"+afTransaction.getTransactionId());	
                            afTransactionDetailsList=transactionDetailsDAO.getTransactionDetails(afTransaction.getTransactionId().toString());
				
				for(AfTransactionDetails afTransactionDetails : afTransactionDetailsList){
                                    
					
					TransactionDetailsBean transactionDetailsBean=new TransactionDetailsBean();
					partnerProductName="";
					partnerName="";
					partnerCategoryName="";
					date 	= afTransactionDetails.getTransactionDate();
					dateStr = (String) (date!=null && !date.equals("")?(ServiceProviderUtil.getInstance().getFormattedDate(date,"dd/MM/yyyy")).toString():"");
					transactionDetailsBean.setTransactionDate(dateStr); 
					
					date 	= afTransactionDetails.getMilesPostingDate();
					dateStr = (String) (date!=null && !date.equals("")?(ServiceProviderUtil.getInstance().getFormattedDate(date,"dd/MM/yyyy")).toString():"");
					transactionDetailsBean.setMilesPostingDate(dateStr);
					partnerName 		= ServiceProviderUtil.nullSafe(afTransactionDetails.getPartnerId().toString(),"");
					if(null!=afTransactionDetails.getPartnerProductId() && !afTransactionDetails.getPartnerProductId().isEmpty()){
					productId = ServiceProviderUtil.nullSafe(afTransactionDetails.getPartnerProductId().toString(),"");
                                            System.out.println("productId    &&  "+productId);
					afPartnerProductList=transactionDetailsDAO.getProductNameById(productId);  					
					for(AfPartnerProduct afPartnerProduct:afPartnerProductList){
						partnerProductName 	= ServiceProviderUtil.nullSafe(afPartnerProduct.getProductName(),"");						
						partnerCategoryName	= ServiceProviderUtil.nullSafe(afPartnerProduct.getAfPartnerCategory().getPartnerCategoryName().toString(),"");
					}
					}
					
					transactionDetailsBean.setPartnerProductName(partnerProductName);
					transactionDetailsBean.setPartnerName(partnerName);
					transactionDetailsBean.setPartnerCategoryName(partnerCategoryName);
					transactionDetailsBean.setAmount(afTransactionDetails.getPrice());
					transactionDetailsBean.setMiles(afTransactionDetails.getJpmilesToCredit());
					
					productStatus = afTransactionDetails.getProductStatus();  
					
					Map<Byte, String> mapProductStatus = commonService.getEnumValues("PRODUCT_STATUS");
					
					for (Entry<Byte, String> entry : mapProductStatus.entrySet()) {
						if(entry.getKey()==productStatus){
							transactionDetailsBean.setProductStatus(entry.getValue());
						}
					}
					date 	= afTransactionDetails.getExpectedDateOfCredit();
					dateStr = (String) (date!=null && !date.equals("")?(ServiceProviderUtil.getInstance().getFormattedDate(date,"dd/MM/yyyy")).toString():"");
					transactionDetailsBean.setExpectedCreditDate(dateStr);
					
					transactionDetailsBeanList.add(transactionDetailsBean);
			}
		}
		}catch(Exception e){
			logger.error("@@@ Exception inside /getTransactionDetails while fetching transactionDetails e:", e);
			e.printStackTrace();
		}
		return transactionDetailsBeanList;
	}


	@Override
	public List<TransactionDetailsBean> getTransactionDateDetails(String jpNumber, String fromDate, String toDate) {
		
		List<AfTransactionDetails> afTransactionDetailsList= null;
		List<AfTransaction> afTransactionList= null;
		List<TransactionDetailsBean> transactionDetailsBeanList= new ArrayList<TransactionDetailsBean>();
		List<AfPartnerProduct> afPartnerProductList = null;
		logger.info("inside getTransactionDetails () jpNumber "+jpNumber);
		try{
			String dateStr = "", productId="", partnerProductName="",partnerName="",partnerCategoryName="";
			int productStatus;
			Date date = null;
			
			afTransactionList = transactionDetailsDAO.getTransactionId(jpNumber,fromDate,toDate);
//			afTransactionList = transactionDetailsDAO.getTransactionId(jpNumber);
			for(AfTransaction afTransaction:afTransactionList){    
			
				afTransactionDetailsList=transactionDetailsDAO.getDateTransactionDetails(afTransaction.getTransactionId().toString(),fromDate,toDate);
				
				for(AfTransactionDetails afTransactionDetails : afTransactionDetailsList){
					
					TransactionDetailsBean transactionDetailsBean=new TransactionDetailsBean();
					partnerProductName="";
					partnerName="";
					partnerCategoryName="";
					date 	= afTransactionDetails.getTransactionDate();
					dateStr = (String) (date!=null && !date.equals("")?(ServiceProviderUtil.getInstance().getFormattedDate(date,"dd/MM/yyyy")).toString():"");
					transactionDetailsBean.setTransactionDate(dateStr); 
					
					date 	= afTransactionDetails.getMilesPostingDate();
					dateStr = (String) (date!=null && !date.equals("")?(ServiceProviderUtil.getInstance().getFormattedDate(date,"dd/MM/yyyy")).toString():"");
					transactionDetailsBean.setMilesPostingDate(dateStr);
					partnerName 		= ServiceProviderUtil.nullSafe(afTransactionDetails.getPartnerId().toString(),"");
					if(null!=afTransactionDetails.getPartnerProductId() && !afTransactionDetails.getPartnerProductId().isEmpty()){
					productId = ServiceProviderUtil.nullSafe(afTransactionDetails.getPartnerProductId().toString(),"");
					afPartnerProductList=transactionDetailsDAO.getProductNameById(productId);  					
					for(AfPartnerProduct afPartnerProduct:afPartnerProductList){
						partnerProductName 	= ServiceProviderUtil.nullSafe(afPartnerProduct.getProductName(),"");						
						partnerCategoryName	= ServiceProviderUtil.nullSafe(afPartnerProduct.getAfPartnerCategory().getPartnerCategoryName().toString(),"");
					}
					}
					
					transactionDetailsBean.setPartnerProductName(partnerProductName);
					transactionDetailsBean.setPartnerName(partnerName);
					transactionDetailsBean.setPartnerCategoryName(partnerCategoryName);
					transactionDetailsBean.setAmount(afTransactionDetails.getPrice());
					transactionDetailsBean.setMiles(afTransactionDetails.getJpmilesToCredit());
					
					productStatus = afTransactionDetails.getProductStatus();  
					
					Map<Byte, String> mapProductStatus = commonService.getEnumValues("PRODUCT_STATUS");
					
					for (Entry<Byte, String> entry : mapProductStatus.entrySet()) {
						if(entry.getKey()==productStatus){
							transactionDetailsBean.setProductStatus(entry.getValue());
						}
					}
					date 	= afTransactionDetails.getExpectedDateOfCredit();
					dateStr = (String) (date!=null && !date.equals("")?(ServiceProviderUtil.getInstance().getFormattedDate(date,"dd/MM/yyyy")).toString():"");
					transactionDetailsBean.setExpectedCreditDate(dateStr);
					
					transactionDetailsBeanList.add(transactionDetailsBean);
			}
		}
		}catch(Exception e){
			logger.error("@@@ Exception inside /getTransactionDetails while fetching transactionDetails e:", e);
			e.printStackTrace();
		}
		return transactionDetailsBeanList;
		
	}


	@Override
	public List<ExportBean> getExportTransactionReportsList(String jpNumber, String fromDate, String toDate) {
		// TODO Auto-generated method stub
		return null;
	}


	
}