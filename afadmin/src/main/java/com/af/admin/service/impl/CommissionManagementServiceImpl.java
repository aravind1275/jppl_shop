package com.af.admin.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.af.admin.DAO.CategoryManagementDAO;
import com.af.admin.DAO.CommissionManagementDAO;
import com.af.admin.DAO.CommonDAO;
import com.af.admin.DAO.PartnerManagementDAO;
import com.af.admin.DAO.UserActivityLogDAO;
import com.af.admin.beans.CommissionBean;
import com.af.admin.beans.CommissionUploadBean;
import com.af.admin.beans.offersBean;
import com.af.admin.constants.AfConstants;
import com.af.admin.entity.AfEnumValues;
import com.af.admin.entity.AfOfferDealLink;
import com.af.admin.entity.AfPartner;
import com.af.admin.entity.AfPartnerCategory;
import com.af.admin.entity.AfPartnerCommission;
import com.af.admin.entity.AfPartnerProduct;
import com.af.admin.entity.AfUserActivityLog;
import com.af.admin.service.CommissionManagementService;
import com.af.admin.service.CommonService;
import com.af.admin.utils.AdminUserActivityUtil;
import com.af.admin.utils.CommonUtil;
import com.af.admin.utils.ServiceProviderUtil;



@Component
@Transactional(readOnly = true)
public class CommissionManagementServiceImpl implements CommissionManagementService {

	private Logger logger = LogManager.getLogger(CommissionManagementServiceImpl.class.getName());

	@Autowired
	CategoryManagementDAO categorymanagementDAO;

	@Autowired
	CommissionManagementDAO commisionManagementDao;

	@Autowired
	PartnerManagementDAO partnermanagementDAO;

	@Autowired
	CommonDAO commonDAO;

	@Autowired
	CommonService commonService;
	
	@Autowired
	UserActivityLogDAO userActLogDao;
	
	@Autowired
	AdminUserActivityUtil adminUserActivityUtil;

	AfPartnerCommission afPartnerCommission_;
	
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
	@Override
	public List<CommissionBean> getPartnersCommission(String partNo) {
		String dateStr = "";
		Date dt = null;
		int categoryStatusCode;
		AfPartner partner = null;
		AfPartnerCategory partnercategory = null;
		AfPartnerProduct afpartnerproduct = null;
		List<AfPartnerCommission> partnercommission = null;
		List<CommissionBean> listcommissionbean = new ArrayList<CommissionBean>();
		try {
			partnercommission = commisionManagementDao.getPartnerCommissionList(partNo);

			if (partnercommission != null) {
				for (AfPartnerCommission afPartnerCommission : partnercommission) {
					CommissionBean commissionbean = new CommissionBean();

					commissionbean.setPrtCommNo(afPartnerCommission.getPrtCommNo());
					partner = afPartnerCommission.getAfPartner();

					if (partner != null) {
						commissionbean.setPartNo(partner.getPrtNo().toString());

						commissionbean.setPartnerName(partner.getPartnerName());
					}
					partnercategory = afPartnerCommission.getAfPartnerCategory();

					if (partnercategory != null) {
						commissionbean.setPrtCatNo(partnercategory.getPrtCatNo().toString());
						commissionbean.setPrtCategoryName(partnercategory.getPartnerCategoryName());
					}
					try {
						afpartnerproduct = afPartnerCommission.getAfPartnerProduct();
					} catch (Exception e) {
						logger.error("getPartnersCommission() afpartnerproduct ", e);
						continue;
					}
					if (afpartnerproduct != null) {
						commissionbean.setPrtProductNo(afpartnerproduct.getPrtPrdNo().toString());

						commissionbean.setPrtProductName(afpartnerproduct.getProductName());

					}
					dt = afPartnerCommission.getActivationDate();
					if (dt != null)
						dateStr = ServiceProviderUtil.getInstance().getFormattedDate(dt, "dd/MM/yyyy");
					if (dt != null && !dt.equals(""))
						commissionbean.setActivationDate(dateStr);

					dt = afPartnerCommission.getDeactivationDate();
					if (dt != null)
						dateStr = ServiceProviderUtil.getInstance().getFormattedDate(dt, "dd/MM/yyyy");
					if (dt != null && !dt.equals(""))
						commissionbean.setDeactivationDate(dateStr);

					categoryStatusCode = afPartnerCommission.getCommissionStatus();

					if (afPartnerCommission.getCommissionRate() != null)
						commissionbean.setCommissionRate(afPartnerCommission.getCommissionRate().toString());

					if (afPartnerCommission.getMemberShare() != null)
						commissionbean.setMemberShare(afPartnerCommission.getMemberShare().toString());

					if (afPartnerCommission.getProductBrand() != null)
						commissionbean.setProductBrand(afPartnerCommission.getProductBrand());

					if (afPartnerCommission.getCategoryPath() != null)
						commissionbean.setCategoryPath(afPartnerCommission.getCategoryPath());

					if (1 == categoryStatusCode) {
						commissionbean.setCommissionSatus(AfConstants.STATUS_ACTIVE);
						commissionbean.setCommissionStatusClass(AfConstants.STATUS_CLASS_GREEN);
						commissionbean.setCommissionStatusCode(1);

					}
					if (0 == categoryStatusCode) {
						commissionbean.setCommissionSatus(AfConstants.STATUS_IN_ACTIVE);
						commissionbean.setCommissionStatusClass(AfConstants.STATUS_CLASS_RED);
						commissionbean.setCommissionStatusCode(0);
					}
					listcommissionbean.add(commissionbean);
				}

			}
		} catch (Exception e) {
			logger.error("@@@@Exception in CommissionManagementServiceImpl at getPartnersCommission() :", e);
		}
		return listcommissionbean;
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public String updateCommission(JSONObject commissionJson, HttpServletRequest request) {
		DateFormat sourceDf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		Date dt = null;
		AfPartnerCommission afPartnerCommission = null;
		AfPartnerCommission afPartnerCommissionOld=null;
		String replaceInOld="";
		String replaceInNew="";
		String status = "failure";
		try {
			String deactivationdate = commissionJson.getString("partnerDeactivationDate");
			String activationdate = commissionJson.getString("partnerActivationDate");
			Integer maxJpmiles = "".equals(commissionJson.getString("maxJpmiles")) ? null
					: Integer.parseInt(commissionJson.getString("maxJpmiles"));

			afPartnerCommission = commisionManagementDao
					.getPartnerCommission(new Integer(commissionJson.getInt("prtCommNo")));
			afPartnerCommissionOld=(AfPartnerCommission) afPartnerCommission.clone();
			
			if (commissionJson.getString("partnerCommission") != null)
				afPartnerCommission.setCommissionRate(new Float(commissionJson.getString("partnerCommission")));

			if (commissionJson.getString("jpMileCommission") != null)
				afPartnerCommission.setMemberShare(new Float(commissionJson.getString("jpMileCommission")));

			afPartnerCommission.setCommissionStatus((byte) commissionJson.getInt("commissionStatus"));

			if (activationdate != null && !activationdate.isEmpty()) {
				Date dt1 = new Date();
				String hh_mi_ss = "" + dt1;
				String format[] = hh_mi_ss.split(" ");
				hh_mi_ss = format[3];
				activationdate = activationdate + " " + hh_mi_ss;
				dt = sourceDf.parse(activationdate);
			}
			afPartnerCommission.setActivationDate(dt);
			dt = null;
			if (deactivationdate != null && !deactivationdate.isEmpty()) {
				Date dt1 = new Date();
				String hh_mi_ss = "" + dt1;
				String format[] = hh_mi_ss.split(" ");
				hh_mi_ss = format[3];
				deactivationdate = deactivationdate + " " + hh_mi_ss;
				dt = sourceDf.parse(deactivationdate);
			}
			afPartnerCommission.setDeactivationDate(dt);

			afPartnerCommission.setMaxJpmiles(maxJpmiles);
			afPartnerCommission.setModifiedTime(new Date());
			afPartnerCommission.setModifiedBy(1);
			afPartnerCommission.setCommissionType((byte) commissionJson.getInt("commType"));
			afPartnerCommission.setTransactionStatus((byte)commissionJson.getInt("transactionStatus"));
			if (!commissionJson.getString("lowerValue").equals("")) {
				afPartnerCommission.setLowerValue((float) commissionJson.getDouble("lowerValue"));
			}
			if (!commissionJson.getString("upperValue").equals("")) {
				afPartnerCommission.setUpperValue((float) commissionJson.getDouble("upperValue"));
			}

			String bonusCommissionType = ServiceProviderUtil.nullSafe(commissionJson.getString("bonusCommissionType"),
					"3");
			String bonusCommissionRate = ServiceProviderUtil.nullSafe(commissionJson.getString("bonusCommissionRate"),
					null);
			String bonusPartnerCode = ServiceProviderUtil.nullSafe(commissionJson.getString("bonusPartnerCode"), null);
			String bonusActivityCode = ServiceProviderUtil.nullSafe(commissionJson.getString("bonusActivityCode"),
					null);

			afPartnerCommission.setBonusCommissionType(Byte.valueOf(bonusCommissionType));

			if (bonusCommissionRate == null) {
				afPartnerCommission.setBonusCommissionRate(null);
			} else {
				afPartnerCommission.setBonusCommissionRate(Float.valueOf(bonusCommissionRate));
			}

			afPartnerCommission.setBonusPartnerCode(bonusPartnerCode);
			afPartnerCommission.setBonusActivityCode(bonusActivityCode);
			
			String statusInactive=	afPartnerCommissionOld.getCommissionStatus()==0?"InActive":"Active";
	     	String statusActive=afPartnerCommission.getCommissionStatus()==0?"InActive":"Active";

			status = commisionManagementDao.updateCommission(afPartnerCommission);
			
			if("success".equalsIgnoreCase(status)){
				
				String ipAddress = request.getHeader("X-FORWARDED-FOR");
				if (ipAddress == null) {
					ipAddress = request.getRemoteAddr();
				}
				AfUserActivityLog updateUsActLog = new AfUserActivityLog();
				Map<String,String> objectCom = new LinkedHashMap<>();
				try {
					updateUsActLog.setUserActivity("Update Commission--"+afPartnerCommission.getAfPartner().getPartnerName());
					updateUsActLog.setUserActCreatedDateTime(new Date());
					updateUsActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
					updateUsActLog.setUserActIpVal(ipAddress);
					updateUsActLog.setUserActModule("Commission");
					/*objectCom = adminUserActivityUtil.objectComparision(afPartnerCommission, afPartnerCommissionOld);
					replaceInOld=objectCom.get(AfConstants.ACTIVITY_OLD_KEY).toString().replace(":0 |",":InActive |").replace(":1 |", ":Active |");
		    		replaceInNew=objectCom.get(AfConstants.ACTIVITY_NEW_KEY).toString().replace(":0 |",":InActive |").replace(":1 |", ":Active |");*/
					
					if(afPartnerCommission.getAfPartner().getPartnerName()!=(afPartnerCommissionOld.getAfPartner().getPartnerName())){
						replaceInOld=replaceInOld+"|Partner Name :"+afPartnerCommissionOld.getAfPartner().getPartnerName();
					    replaceInNew=replaceInNew+"|Partner Name :"+afPartnerCommission.getAfPartner().getPartnerName();
					}
					if(afPartnerCommission.getCommissionStatus()!=afPartnerCommissionOld.getCommissionStatus()){
						replaceInOld=replaceInOld+"|Commission Status:"+statusInactive;
						replaceInNew=replaceInNew+"|Commission Status:"+statusActive;
					}
					if(afPartnerCommission.getDeactivationDate()!=afPartnerCommissionOld.getDeactivationDate()){
						replaceInOld=replaceInOld+"|Commission End Date:"+afPartnerCommissionOld.getDeactivationDate();
						replaceInNew=replaceInNew+"|Commission End Date:"+afPartnerCommission.getDeactivationDate();
					}
					if(replaceInNew.isEmpty() && replaceInOld.isEmpty()) {
                        replaceInOld=replaceInOld+"|ModifiedTime:"+simpleDateFormat.format(afPartnerCommissionOld.getModifiedTime());
                        replaceInNew=replaceInNew+"|ModifiedTime:"+simpleDateFormat.format(afPartnerCommission.getModifiedTime());
                    }
					
					
					
					updateUsActLog.setUserNewFieldValue(replaceInNew);
		    		updateUsActLog.setUserOldFieldValue(replaceInOld);
					userActLogDao.updateUserLog(updateUsActLog);
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("@@@@Exception in CommissionManagementServiceImpl at updateCommission() :", e);

		}
		return status;
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public String addCommission(CommissionBean commissionBean, HttpServletRequest request) {
		String status = "";
		try {
			String activationDate = null, deactivationDate = null;
			Date dt = null;
			List<Integer> commList = new ArrayList<>();
			DateFormat sourceDf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
			AfPartnerCommission afpartnercommission = null;
			AfPartnerCommission afpartnercommission_ = null;
			Integer partNo = new Integer(commissionBean.getPartNo());
			String partCatNo = commissionBean.getPrtCatNo();
			String partPrdNo = commissionBean.getPrtProductNo();
			String prdBrand = commissionBean.getProductBrand();
			String catPath = commissionBean.getCategoryPath();
			String activationdate = commissionBean.getActivationDate();
			String deactivationdate = commissionBean.getDeactivationDate();
			Integer maxJpmiles = "".equals(commissionBean.getMaxJpmiles()) ? null
					: Integer.parseInt(commissionBean.getMaxJpmiles());

			String bonusCommissionRate = ServiceProviderUtil.nullSafe(commissionBean.getBonusCommissionRate(), null);
			String bonusPartnerCode = ServiceProviderUtil.nullSafe(commissionBean.getBonusPartnerCode(), null);
			String bonusActivityCode = ServiceProviderUtil.nullSafe(commissionBean.getBonusActivityCode(), null);

			Float upperValue = null;
			Float lowerValue = null;

			if (commissionBean.getLowerValue() != null && !"".equals(commissionBean.getLowerValue())) {
				lowerValue = (new Float(commissionBean.getLowerValue()));
			}
			if (commissionBean.getUpperValue() != null && !"".equals(commissionBean.getUpperValue())) {
				upperValue = (new Float(commissionBean.getUpperValue()));
			}

			partCatNo = "".equals(partCatNo) ? null : partCatNo;
			partPrdNo = "".equals(partPrdNo) ? null : partPrdNo;
			prdBrand = "".equals(prdBrand) ? null : prdBrand;
			catPath = "".equals(catPath) ? null : catPath;

			commList = commisionManagementDao.checkExistingCommDetails(partNo, partCatNo, partPrdNo, prdBrand, catPath,
					lowerValue, upperValue);
			for (Integer partcommNo : commList) {
				if (partcommNo != null) {
					afpartnercommission = commisionManagementDao.getPartnerCommission(partcommNo);
				}

				afpartnercommission_ = afpartnercommission;

				if (afpartnercommission == null) {
					afpartnercommission = new AfPartnerCommission();
					AfPartner afpartner = null;
					AfPartnerCategory afpartnercategories = null;
					AfPartnerProduct afpartnerproduct = null;
					Date dt1 = new Date();
					String hh_mi_ss = "" + dt1;
					String format[] = hh_mi_ss.split(" ");
					hh_mi_ss = format[3];
					afpartner = partnermanagementDAO.getPartnerByPartnerNO(commissionBean.getPartNo());

					afpartnercommission.setAfPartner(afpartner);
					afpartnercategories = commisionManagementDao.getAfPartnerCategories(commissionBean.getPrtCatNo());
					afpartnercommission.setAfPartnerCategory(afpartnercategories);
					if (commissionBean.getPrtProductNo() != null && !commissionBean.getPrtProductNo().isEmpty()) {
						afpartnerproduct = commisionManagementDao
								.getAfPartnerProducts(commissionBean.getPrtProductNo());
						afpartnercommission.setAfPartnerProduct(afpartnerproduct);
					}

					activationDate = commissionBean.getActivationDate();
					activationDate = activationDate + " " + hh_mi_ss;
					if (activationDate.length() > 0)
						dt = sourceDf.parse(activationDate);
					afpartnercommission.setActivationDate(dt);

					deactivationDate = commissionBean.getDeactivationDate().trim();
					if (!deactivationDate.isEmpty()) {
						deactivationDate = deactivationDate + " " + hh_mi_ss;

						if (deactivationDate.length() > 0)
							dt = sourceDf.parse(deactivationDate);
						afpartnercommission.setDeactivationDate(dt);
					}

					if (deactivationDate.isEmpty()) {
						afpartnercommission.setDeactivationDate(null);
					}

					if (commissionBean.getCommissionRate() != null)
						;
					afpartnercommission.setCommissionRate(new Float(commissionBean.getCommissionRate()));

					if (commissionBean.getMemberShare() != null)
						afpartnercommission.setMemberShare(new Float(commissionBean.getMemberShare()));

					afpartnercommission.setMaxJpmiles(maxJpmiles);
					afpartnercommission.setCommissionStatus((byte) commissionBean.getCommissionStatusCode());
					afpartnercommission.setStatus((byte) 1);
					afpartnercommission.setCreatedTime(new Date());
					afpartnercommission.setCreatedBy(1);
					afpartnercommission.setModifiedTime(new Date());
					afpartnercommission.setModifiedBy(1);
					afpartnercommission.setProductBrand(prdBrand);
					afpartnercommission.setCategoryPath(catPath);
					afpartnercommission.setCommissionType((byte) commissionBean.getCommisionType());
					afpartnercommission.setTransactionStatus((byte)commissionBean.getTransactionStatus());
					if (commissionBean.getLowerValue() != null && !"".equals(commissionBean.getLowerValue())) {
						afpartnercommission.setLowerValue(new Float(commissionBean.getLowerValue()));
					}
					if (commissionBean.getUpperValue() != null && !"".equals(commissionBean.getUpperValue())) {
						afpartnercommission.setUpperValue(new Float(commissionBean.getUpperValue()));
					}

					if (commissionBean.getBonusCommissionType() == 0) {
						afpartnercommission.setBonusCommissionType((byte) 3);
					} else {
						afpartnercommission.setBonusCommissionType(commissionBean.getBonusCommissionType());
					}

					if (bonusCommissionRate != null) {
						afpartnercommission.setBonusCommissionRate(Float.parseFloat(bonusCommissionRate));
					}

					afpartnercommission.setBonusPartnerCode(bonusPartnerCode);
					afpartnercommission.setBonusActivityCode(bonusActivityCode);

					status = commisionManagementDao.addCommission(afpartnercommission);

				}

				if (afpartnercommission_ != null) {
					int x;
					Date newactivatiodate = null;
					Date newdeactivatiodate = null;
					Date dt1 = new Date();
					String hh_mi_ss = "" + dt1;
					String format[] = hh_mi_ss.split(" ");
					hh_mi_ss = format[3];

					Date oldactivationdate = afpartnercommission.getActivationDate();
					Date olddeactivationdate = afpartnercommission.getDeactivationDate();

					activationdate = activationdate + " " + hh_mi_ss;
					if (activationdate.length() > 0)
						newactivatiodate = sourceDf.parse(activationdate);

					String newactivatiodate_ = ServiceProviderUtil.getInstance().getFormattedDate(newactivatiodate,
							"dd/MM/yyyy");
					String oldactivationdate_ = ServiceProviderUtil.getInstance().getFormattedDate(oldactivationdate,
							"dd/MM/yyyy");
					newactivatiodate = ServiceProviderUtil.getInstance().getDateFromString(newactivatiodate_,
							"dd/MM/yyyy");
					oldactivationdate = ServiceProviderUtil.getInstance().getDateFromString(oldactivationdate_,
							"dd/MM/yyyy");

					x = newactivatiodate.compareTo(oldactivationdate);

					if (x > 0) {
						Date dateOnedayBefore = new Date(newactivatiodate.getTime() - 1 * 24 * 3600 * 1000);
						dateOnedayBefore = ServiceProviderUtil.trim(dateOnedayBefore);
						afpartnercommission.setDeactivationDate(dateOnedayBefore);
						status = commisionManagementDao.updateCommission(afpartnercommission);
					}
					if (deactivationdate != null && !deactivationdate.trim().isEmpty()) {
						deactivationdate = deactivationdate + " " + hh_mi_ss;
					} else {
						deactivationdate = null;
					}

					if (deactivationdate != null && deactivationdate.length() > 0) {
						newdeactivatiodate = sourceDf.parse(deactivationdate);
						String newdeactivatiodate_ = ServiceProviderUtil.getInstance()
								.getFormattedDate(newdeactivatiodate, "dd/MM/yyyy");
						String olddeactivationdate_ = ServiceProviderUtil.getInstance()
								.getFormattedDate(olddeactivationdate, "dd/MM/yyyy");
						newdeactivatiodate = ServiceProviderUtil.getInstance().getDateFromString(newdeactivatiodate_,
								"dd/MM/yyyy");
						olddeactivationdate = ServiceProviderUtil.getInstance().getDateFromString(olddeactivationdate_,
								"dd/MM/yyyy");
					}
					if (x < 0) {
						Date dateOnedayAfter = new Date(newdeactivatiodate.getTime() + 1 * 24 * 3600 * 1000);
						Date dateOneMonthAfter = new Date(dateOnedayAfter.getTime() + 30 * 24 * 3600 * 1000);
						dateOnedayAfter = ServiceProviderUtil.trim(dateOnedayAfter);
						afpartnercommission.setActivationDate(dateOnedayAfter);
						if (deactivationdate != null)
							afpartnercommission.setDeactivationDate(dateOneMonthAfter);
						status = commisionManagementDao.updateCommission(afpartnercommission);
					}
				}
			}

			afpartnercommission = new AfPartnerCommission();
			AfPartner afpartner = null;
			AfPartnerCategory afpartnercategories = null;
			AfPartnerProduct afpartnerproduct = null;
			Date dt2 = new Date();
			String hh_mi_ss_ = "" + dt2;
			String format_[] = hh_mi_ss_.split(" ");
			hh_mi_ss_ = format_[3];
			afpartner = partnermanagementDAO.getPartnerByPartnerNO(commissionBean.getPartNo());

			afpartnercommission.setAfPartner(afpartner);
			afpartnercategories = commisionManagementDao.getAfPartnerCategories(commissionBean.getPrtCatNo());
			afpartnercommission.setAfPartnerCategory(afpartnercategories);
			if (!commissionBean.getPrtProductNo().isEmpty()) {
				afpartnerproduct = commisionManagementDao.getAfPartnerProducts(commissionBean.getPrtProductNo());
				afpartnercommission.setAfPartnerProduct(afpartnerproduct);
			}

			activationDate = commissionBean.getActivationDate();
			activationDate = activationDate + " " + hh_mi_ss_;
			if (activationDate.length() > 0)
				dt = sourceDf.parse(activationDate);
			afpartnercommission.setActivationDate(dt);

			deactivationDate = commissionBean.getDeactivationDate().trim();
			if (!deactivationDate.isEmpty()) {
				deactivationDate = deactivationDate + " " + hh_mi_ss_;

				if (deactivationDate.length() > 0)
					dt = sourceDf.parse(deactivationDate);
				afpartnercommission.setDeactivationDate(dt);
			}
			if (deactivationDate.isEmpty()) {
				afpartnercommission.setDeactivationDate(null);
			}

			if (commissionBean.getCommissionRate() != null)
				
			afpartnercommission.setCommissionRate(new Float(commissionBean.getCommissionRate()));

			if (commissionBean.getMemberShare() != null)
				afpartnercommission.setMemberShare(new Float(commissionBean.getMemberShare()));

			afpartnercommission.setMaxJpmiles(maxJpmiles);
			afpartnercommission.setCommissionStatus((byte) commissionBean.getCommissionStatusCode());
			afpartnercommission.setStatus((byte) 1);
			afpartnercommission.setCreatedTime(new Date());
			afpartnercommission.setCreatedBy(1);
			afpartnercommission.setModifiedTime(new Date());
			afpartnercommission.setModifiedBy(1);
			afpartnercommission.setProductBrand(prdBrand);
			afpartnercommission.setCategoryPath(catPath);

			afpartnercommission.setCommissionType((byte) commissionBean.getCommisionType());
			afpartnercommission.setTransactionStatus((byte)commissionBean.getTransactionStatus());
			if (commissionBean.getLowerValue() != null && !"".equals(commissionBean.getLowerValue())) {
				afpartnercommission.setLowerValue(new Float(commissionBean.getLowerValue()));
			}
			if (commissionBean.getUpperValue() != null && !"".equals(commissionBean.getUpperValue())) {
				afpartnercommission.setUpperValue(new Float(commissionBean.getUpperValue()));
			}

			if (commissionBean.getBonusCommissionType() == 0) {
				afpartnercommission.setBonusCommissionType((byte) 3);
			} else {
				afpartnercommission.setBonusCommissionType(commissionBean.getBonusCommissionType());
			}

			if (bonusCommissionRate != null) {
				afpartnercommission.setBonusCommissionRate(Float.parseFloat(bonusCommissionRate));
			}
			afpartnercommission.setBonusPartnerCode(bonusPartnerCode);
			afpartnercommission.setBonusActivityCode(bonusActivityCode);

			status = commisionManagementDao.addCommission(afpartnercommission);
			
			if(status.equalsIgnoreCase(AfConstants.SUCCESS)){
				String ipAddress = request.getHeader("X-FORWARDED-FOR");
				if(ipAddress == null) {
					ipAddress = request.getRemoteAddr();
				}
				ipAddress = (null != ipAddress) ? ipAddress.trim() : "";			
				
				String oldOrNewVal = "";
				String commLogStatus = "";
				String BonusCommType = ( (Byte) commissionBean.getBonusCommissionType()).toString();
				String maxjpm = "";
				String lowervalue;
				String endDate="";
				String uppervalue="";
				AfUserActivityLog afPrtActLog=new AfUserActivityLog();
				partCatNo = partCatNo!=null?partCatNo:"NA";
				bonusCommissionRate=bonusCommissionRate!=null?bonusCommissionRate:"NA";
				partCatNo=partCatNo!=null?partCatNo:"NA";
				partPrdNo=partPrdNo!=null?partPrdNo:"NA";
				catPath=catPath!=null?catPath:"NA";
				if(maxJpmiles!=null){
					maxjpm = maxJpmiles.toString();
				}else{
					maxjpm = "NA";
				}
				if(lowerValue!=null){
					lowervalue=lowerValue.toString();
				}
				else
				{
					lowervalue="NA";
				}
				if(upperValue!=null){
					uppervalue=upperValue.toString();
				}
				else
				{
					uppervalue="NA";
				}
				if(deactivationDate.isEmpty()) {
					endDate = "NA";
				}
				else
				{
					endDate=deactivationdate;
				}
				String status1=commissionBean.getCommissionStatusCode()+"";
				status1 = status1.replaceAll("0","InActive");
				status1 = status1.replaceAll("1", "Active");
				
				bonusPartnerCode=bonusPartnerCode!=null?bonusPartnerCode:"NA";
				bonusActivityCode=bonusActivityCode!=null?bonusActivityCode:"NA";
				BonusCommType=BonusCommType!=null?BonusCommType:"NA";
				
				String transactionStatus = "";
				if(afpartnercommission.getTransactionStatus()== 2) {
					transactionStatus = "Processed";
				} else {
					transactionStatus = "Tentative";
				}
				String partnerCategory = afpartnercommission.getAfPartnerCategory()==null?"NA":afpartnercommission.getAfPartnerCategory().getPartnerCategoryName();
				oldOrNewVal="Partner No : "+afpartnercommission.getAfPartner().getPartnerName()+" | "+"Partner Categories : "+partnerCategory+" | "+
				""+"Partner Products: "+partPrdNo+"| "+"Category Path: "+catPath+" |"+" Maximum JPMiles can be Earned: "+maxjpm+" |"
						+ " "+"Partner Commission Type : "+commissionBean.getCommisionType()+" | "+"Transaction Status : "+transactionStatus+" |"+"Jp Miles commission : "+commissionBean.getMemberShare()+" |"
								+ ""+"Commission Status : "+status1+" | "+
				"Commission Rate : "+commissionBean.getCommissionRate()+" | "+"Bonus Jpmiles Commission: "+BonusCommType+"|"
						+ ""+"Bonus Partner Code: "+bonusPartnerCode+"|"+"Bonus Activity Code: "+bonusActivityCode+"|"+"Lower Value: "+lowervalue+" |"+"Upper Value: "+uppervalue+" |"
								+ " "+"Start Date : "+dt+" | "+"End Date: "+endDate+" |  ";
						
				
				afPrtActLog.setUserNewFieldValue(oldOrNewVal);
				afPrtActLog.setUserOldFieldValue("NA");
				afPrtActLog.setUserActivity("Adding Commission");
				afPrtActLog.setUserActCreatedDateTime(new Date());
				afPrtActLog.setUserActIpVal(ipAddress);
				afPrtActLog.setUserActModule("Commision Management");
				afPrtActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
				
				commLogStatus = userActLogDao.createUserActLog(afPrtActLog);
				
				
			}
			

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("@@@@Exception in CommissionManagementServiceImpl at addCommission() :", e);
			status = "failure";
			return status;
		}
		return status;
	}

	@Override
	public String checkExistingDetailsStartDate(JSONObject commissionJson) {
		String status = "success";
		Long count = 0L;

		try {
			String partNo = "".equals(commissionJson.getString("partNo")) ? null : commissionJson.getString("partNo");
			String partCatNo = "".equals(commissionJson.getString("partCategoryNo")) ? null
					: commissionJson.getString("partCategoryNo");
			String partPrdNo = "".equals(commissionJson.getString("partProductNo")) ? null
					: commissionJson.getString("partProductNo");
			String activationDate = "".equals(commissionJson.getString("activationDate")) ? null
					: commissionJson.getString("activationDate");
			String brandName = "".equals(commissionJson.getString("brandName")) ? null
					: commissionJson.getString("brandName");
			String catPath = "".equals(commissionJson.getString("catPath")) ? null
					: commissionJson.getString("catPath");
			Float lowerValue = "".equals(commissionJson.getString("lowerValue")) ? null
					: new Float(commissionJson.getString("lowerValue"));
			Float upperValue = "".equals(commissionJson.getString("upperValue")) ? null
					: new Float(commissionJson.getString("upperValue"));

			count = commisionManagementDao.checkExistingDetailsStartDate(partNo, partCatNo, partPrdNo, activationDate,
					brandName, catPath, lowerValue, upperValue);
			if (count > 0L) {
				status = "failure";
				return status;
			}
		} catch (Exception e) {
			logger.error("@@@@Exception in CommissionManagementServiceImpl at checkExistingDetailsStartDate() :", e);
		}
		return status;
	}

	@Override
	public String checkExistingDetailsEditStartDate(JSONObject commissionJson) {

		String status = "success";
		Long count = 0L;

		try {
			String partComNo = "".equals(commissionJson.getString("partComNo")) ? null
					: commissionJson.getString("partComNo");
			;
			String partNo = "".equals(commissionJson.getString("partNo")) ? null : commissionJson.getString("partNo");
			String partCatNo = "".equals(commissionJson.getString("partCategoryNo")) ? null
					: commissionJson.getString("partCategoryNo");
			String partPrdNo = "".equals(commissionJson.getString("partProductNo")) ? null
					: commissionJson.getString("partProductNo");
			String activationDate = "".equals(commissionJson.getString("activationDate")) ? null
					: commissionJson.getString("activationDate");
			String activationDateHidden = "".equals(commissionJson.getString("activationDateHidden")) ? null
					: commissionJson.getString("activationDateHidden");
			String brandName = "".equals(commissionJson.getString("brandName")) ? null
					: commissionJson.getString("brandName");
			String catPath = "".equals(commissionJson.getString("catPath")) ? null
					: commissionJson.getString("catPath");

			count = commisionManagementDao.checkExistingDetailsEditStartDate(activationDateHidden, partComNo, partNo,
					partCatNo, partPrdNo, activationDate, brandName, catPath);

			if (count > 0L) {
				status = "failure";
				return status;
			}
		} catch (Exception e) {
			logger.error("@@@@Exception in CommissionManagementServiceImpl at checkExistingDetailsEditStartDate() :",
					e);
		}
		return status;
	}

	public String checkExistingDetailsEndDate(JSONObject commissionJson) {
		Long count = 0L;
		String status = "success";
		try {
			String partNo = "".equals(commissionJson.getString("partNo")) ? null : commissionJson.getString("partNo");
			String partCatNo = "".equals(commissionJson.getString("partCategoryNo")) ? null
					: commissionJson.getString("partCategoryNo");
			String partPrdNo = "".equals(commissionJson.getString("partProductNo")) ? null
					: commissionJson.getString("partProductNo");
			String activationDate = "".equals(commissionJson.getString("activationDate")) ? null
					: commissionJson.getString("activationDate");
			String deactivationDate = "".equals(commissionJson.getString("deactivationDate")) ? null
					: commissionJson.getString("deactivationDate");
			String brandName = "".equals(commissionJson.getString("brandName")) ? null
					: commissionJson.getString("brandName");
			String catPath = "".equals(commissionJson.getString("catPath")) ? null
					: commissionJson.getString("catPath");
			Float lowerValue = "".equals(commissionJson.getString("lowerValue")) ? null
					: new Float(commissionJson.getString("lowerValue"));
			Float upperValue = "".equals(commissionJson.getString("upperValue")) ? null
					: new Float(commissionJson.getString("upperValue"));
			count = commisionManagementDao.checkExistingDetailsEndDate(partNo, partCatNo, partPrdNo, brandName,
					activationDate, deactivationDate, catPath, lowerValue, upperValue);

			if (count > 0L) {
				status = "failure";
				return status;
			}
		} catch (Exception e) {
			logger.error("@@@@Exception in CommissionManagementServiceImpl at checkExistingDetailsEndDate() :", e);

		}
		return status;
	}

	@Override
	public String checkExistingDetailsEditEndDate(JSONObject commissionJson) {
		Long count = 0L;
		String status = "success";
		try {
			String partComNo = "".equals(commissionJson.getString("partComNo")) ? null
					: commissionJson.getString("partComNo");
			String partNo = "".equals(commissionJson.getString("partNo")) ? null : commissionJson.getString("partNo");
			String partCatNo = "".equals(commissionJson.getString("partCategoryNo")) ? null
					: commissionJson.getString("partCategoryNo");
			String partPrdNo = "".equals(commissionJson.getString("partProductNo")) ? null
					: commissionJson.getString("partProductNo");
			String activationDate = "".equals(commissionJson.getString("activationDate")) ? null
					: commissionJson.getString("activationDate");
			String deactivationDate = "".equals(commissionJson.getString("deactivationDate")) ? null
					: commissionJson.getString("deactivationDate");
			String deactivationDateHidden = "".equals(commissionJson.getString("deactivationDateHidden")) ? null
					: commissionJson.getString("deactivationDateHidden");
			String brandName = "".equals(commissionJson.getString("brandName")) ? null
					: commissionJson.getString("brandName");
			String catPath = "".equals(commissionJson.getString("catPath")) ? null
					: commissionJson.getString("catPath");
			Float lowerValue = "".equals(commissionJson.getString("lowerValue")) ? null
					: new Float(commissionJson.getString("lowerValue"));
			Float upperValue = "".equals(commissionJson.getString("upperValue")) ? null
					: new Float(commissionJson.getString("upperValue"));
			count = commisionManagementDao.checkExistingDetailsEditEndDate(partComNo, partNo, partCatNo, partPrdNo,
					brandName, activationDate, deactivationDate, deactivationDateHidden, catPath, lowerValue,
					upperValue);

			if (count > 0L) {
				status = "failure";
				return status;
			}
		} catch (Exception e) {
			logger.error("@@@@Exception in CommissionManagementServiceImpl at checkExistingDetailsEditEndDate() :", e);

		}
		return status;
	}
	/*
	 * public String checkExistingDetailsEndDate(JSONObject commissionJson) {
	 * Long count=0L; String status="success"; try{ Integer
	 * partNo=commissionJson.getInt("partNo"); String
	 * partCatNo=commissionJson.getString("partCategoryNo"); String
	 * partPrDctNo=commissionJson.getString("partProductNo"); String
	 * commissionrate=commissionJson.getString("commissionrate"); String
	 * memberShare=commissionJson.getString("membershare"); String
	 * activationdate=commissionJson.getString("activationDate"); String
	 * deactivationdate=commissionJson.getString("deactivationDate");
	 * 
	 * if(partPrDctNo.isEmpty()) partPrDctNo=null;
	 * 
	 * if(partPrDctNo==null)
	 * count=commisionManagementDao.checkExistingDetailsEndDate(partNo,partCatNo
	 * ,commissionrate,memberShare,activationdate,deactivationdate);
	 * 
	 * if(partPrDctNo!=null)
	 * count=commisionManagementDao.checkExistingDetailsEndDate(partNo,partCatNo
	 * ,partPrDctNo,commissionrate,memberShare,activationdate,deactivationdate);
	 * 
	 * 
	 * String partNo = "".equals(commissionJson.getString("partNo")) ? null :
	 * commissionJson.getString("partNo"); String partCatNo =
	 * "".equals(commissionJson.getString("partCategoryNo")) ? null :
	 * commissionJson.getString("partCategoryNo"); String partPrdNo =
	 * "".equals(commissionJson.getString("partProductNo")) ? null :
	 * commissionJson.getString("partProductNo"); String activationDate =
	 * "".equals(commissionJson.getString("activationDate")) ? null :
	 * commissionJson.getString("activationDate"); String deactivationDate =
	 * "".equals(commissionJson.getString("deactivationDate")) ? null :
	 * commissionJson.getString("deactivationDate"); String brandName =
	 * "".equals(commissionJson.getString("brandName")) ? null :
	 * commissionJson.getString("brandName"); //public Long
	 * checkExistingDetailsEndDate(String partNo,String partCatNo,String
	 * partPrdNo,String prtBrand,String activationDate,String deactivationDate);
	 * count = commisionManagementDao.checkExistingDetailsEndDate(partNo,
	 * partCatNo, partPrdNo, brandName, activationDate, deactivationDate);
	 * 
	 * if(count>0L) { status="failure"; return status; } }catch(Exception e){
	 * logger.
	 * error("@@@@ Exception in CommissionManagementServiceImpl checkExistingDetailsEndDate()"
	 * +e);
	 * 
	 * } return status; }
	 */

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public String addNewCommission(JSONObject commissionJson,HttpServletRequest request) {
		AfPartnerCommission afPartnerCommission = null;
		AfPartnerCommission afpartnercommission = null;
		String status = "failure";
		AfPartner partner = null;
		AfPartnerCategory afPartnerCategory = null;
		AfPartnerProduct afPartnerProduct = null;
		String startdate = "", enddate = "";
		Date dt = null;
		Date activationdate = null;
		DateFormat sourceDf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		Date dt1 = new Date();
		String hh_mi_ss = "" + dt1;
		String format[] = hh_mi_ss.split(" ");
		hh_mi_ss = format[3];
		List<Integer> commList = new ArrayList<>();
		try {
			Integer partNo = new Integer(commissionJson.getString("partnerNo"));
			String partCatNo = commissionJson.getString("partnerCatNo");
			String partPrdNo = commissionJson.getString("partnerPrdctNo");
			String prtBrand = commissionJson.getString("brandName");
			String catPath = commissionJson.getString("categoryPath");
			Integer maxJpmiles = "".equals(commissionJson.getString("maxJpmiles")) ? null
					: Integer.parseInt(commissionJson.getString("maxJpmiles"));
			Float upperValue = null;
			Float lowerValue = null;
			if (!commissionJson.getString("upperValue").isEmpty())
				upperValue = new Float(commissionJson.getString("upperValue"));
			if (!commissionJson.getString("lowerValue").isEmpty())
				lowerValue = new Float(commissionJson.getString("lowerValue"));

			startdate = commissionJson.getString("partnerActivationDate");
			enddate = commissionJson.getString("partnerDeactivationDate");

			partCatNo = "".equals(partCatNo) ? null : partCatNo;
			partPrdNo = "".equals(partPrdNo) ? null : partPrdNo;
			prtBrand = "".equals(prtBrand) ? null : prtBrand;
			catPath = "".equals(catPath) ? null : catPath;

			commList = commisionManagementDao.checkExistingCommDetails(partNo, partCatNo, partPrdNo, prtBrand, catPath,
					lowerValue, upperValue);
			for (Integer partcommNo : commList) {
				if (partcommNo != null) {
					afPartnerCommission = commisionManagementDao.getPartnerCommission(partcommNo);
				}

				if (afPartnerCommission != null) {
					if (!startdate.isEmpty()) {
						if (startdate.length() > 0) {
							startdate = startdate + " " + hh_mi_ss;
							dt = sourceDf.parse(startdate);

						}
						activationdate = dt;
					}

					Date dateOnedayBefore = new Date(activationdate.getTime() - 1 * 24 * 3600 * 1000);
					logger.debug("dateOnedayBefore " + dateOnedayBefore);
					afPartnerCommission.setDeactivationDate(dateOnedayBefore);
					status = commisionManagementDao.updateCommission(afPartnerCommission);
					afPartnerCommission = new AfPartnerCommission();
				}
			}
			if (afPartnerCommission == null) {
				afPartnerCommission = new AfPartnerCommission();
			}
			partner = partnermanagementDAO.getPartnerByPartnerNO(commissionJson.getString("partnerNo"));
			if (partner != null)
				afPartnerCommission.setAfPartner(partner);

			afPartnerCategory = commisionManagementDao.getAfPartnerCategories(commissionJson.getString("partnerCatNo"));
			if (afPartnerCategory != null)
				afPartnerCommission.setAfPartnerCategory(afPartnerCategory);

			if (partPrdNo != null && !commissionJson.getString("partnerPrdctNo").isEmpty()) {
				afPartnerProduct = commisionManagementDao.getAfPartnerProducts(partPrdNo);

				if (afPartnerProduct != null)
					afPartnerCommission.setAfPartnerProduct(afPartnerProduct);
			}
			if (!startdate.isEmpty()) {

				if (!startdate.isEmpty()) {
					if (startdate.length() > 0) {
						startdate = startdate + " " + hh_mi_ss;
						dt = sourceDf.parse(startdate);

					}
					activationdate = dt;
				}
				afPartnerCommission.setActivationDate(dt);
			}

			if (!enddate.isEmpty()) {
				if (enddate.length() > 0) {
					hh_mi_ss = format[3];
					enddate = enddate + " " + hh_mi_ss;
					dt = sourceDf.parse(enddate);

				}

				afPartnerCommission.setDeactivationDate(dt);
			}

			if (commissionJson.getString("partnerCommission") != null)
				afPartnerCommission.setCommissionRate(new Float(commissionJson.getString("partnerCommission")));

			if (commissionJson.getString("jpMileCommission") != null)
				afPartnerCommission.setMemberShare(new Float(commissionJson.getString("jpMileCommission")));
			afPartnerCommission.setMaxJpmiles(maxJpmiles);
			afPartnerCommission.setCommissionStatus((byte) commissionJson.getInt("commissionStatus"));
			afPartnerCommission.setCreatedTime(new Date());
			afPartnerCommission.setCreatedBy(1);
			afPartnerCommission.setStatus((byte) 1);
			afPartnerCommission.setProductBrand(prtBrand);
			afPartnerCommission.setCategoryPath(catPath);
			afPartnerCommission.setCommissionType((byte) commissionJson.getInt("commType"));
			afPartnerCommission.setTransactionStatus((byte) commissionJson.getInt("transactionStatus"));
			if (!commissionJson.getString("lowerValue").equalsIgnoreCase(""))
				afPartnerCommission.setLowerValue(new Float(commissionJson.getString("lowerValue")));

			if (!commissionJson.getString("upperValue").equalsIgnoreCase("")) {
				afPartnerCommission.setUpperValue(new Float(commissionJson.getString("upperValue")));
			}

			String bonusCommissionType = ServiceProviderUtil.nullSafe(commissionJson.getString("bonusCommissionType"),
					"3");
			String bonusCommissionRate = ServiceProviderUtil.nullSafe(commissionJson.getString("bonusCommissionRate"),
					null);
			String bonusPartnerCode = ServiceProviderUtil.nullSafe(commissionJson.getString("bonusPartnerCode"), null);
			String bonusActivityCode = ServiceProviderUtil.nullSafe(commissionJson.getString("bonusActivityCode"),
					null);

			afPartnerCommission.setBonusCommissionType(Byte.valueOf(bonusCommissionType));

			if (bonusCommissionRate == null) {
				afPartnerCommission.setBonusCommissionRate(null);
			} else {
				afPartnerCommission.setBonusCommissionRate(Float.valueOf(bonusCommissionRate));
			}

			afPartnerCommission.setBonusPartnerCode(bonusPartnerCode);
			afPartnerCommission.setBonusActivityCode(bonusActivityCode);

			status = commisionManagementDao.addCommission(afPartnerCommission);
			
			if("success".equalsIgnoreCase(status)){
				String ipAddress = request.getHeader("X-FORWARDED-FOR");
				if (ipAddress == null) {
					ipAddress = request.getRemoteAddr();
				}
				ipAddress = (null != ipAddress) ? ipAddress.trim() : "";
				String oldOrNewVal = "";
				String commLogStatus = "";
				String BonusCommType = ( (Byte) afPartnerCommission.getBonusCommissionType()).toString();
				String maxjpm = "";
				String lowervalue;
				String endDate="";
				String uppervalue="";
				AfUserActivityLog afPrtActLog=new AfUserActivityLog();
				partCatNo = partCatNo!=null?partCatNo:"NA";
				bonusCommissionRate=bonusCommissionRate!=null?bonusCommissionRate:"NA";
				partCatNo=partCatNo!=null?partCatNo:"NA";
				partPrdNo=partPrdNo!=null?partPrdNo:"NA";
				catPath=catPath!=null?catPath:"NA";
				if(maxJpmiles!=null){
					maxjpm = maxJpmiles.toString();
				}else{
					maxjpm = "NA";
				}
				if(lowerValue!=null){
					lowervalue=lowerValue.toString();
				}
				else
				{
					lowervalue="NA";
				}
				if(upperValue!=null){
					uppervalue=upperValue.toString();
				}
				else
				{
					uppervalue="NA";
				}
				if(enddate.isEmpty()) {
					endDate = "NA";
				}
				else
				{
					endDate=enddate;
				}
				
				bonusPartnerCode=bonusPartnerCode!=null?bonusPartnerCode:"NA";
				bonusActivityCode=bonusActivityCode!=null?bonusActivityCode:"NA";
				BonusCommType=BonusCommType!=null?BonusCommType:"NA";
				String transactionStatus = "";
				if(afPartnerCommission.getTransactionStatus()== 2) {
					transactionStatus = "Processed";
				} else {
					transactionStatus = "Tentative";
				}
				
				oldOrNewVal="Partner No : "+partNo+" | "+"Partner Categories : "+partCatNo+" | "+
				""+" Maximum JPMiles can be Earned: "+maxjpm+" |"
						+ " "+"Partner Commission Type : "+afPartnerCommission.getCommissionType()+" | "+"Transaction Status : "+transactionStatus+" |"+"Jp Miles commission : "+afPartnerCommission.getMemberShare()+" |"
								+ ""+"Commission Status : "+afPartnerCommission.getCommissionStatus()+" | "+
				"Commission Rate : "+afPartnerCommission.getCommissionRate()+" | "+"Bonus Jpmiles Commission: "+BonusCommType+"|"
						+ ""+"Bonus Partner Code: "+bonusPartnerCode+"|"+"Bonus Activity Code: "+bonusActivityCode+"|"+"Lower Value: "+lowervalue+" |"+"Upper Value: "+uppervalue+" |"
								+ " "+"Start Date : "+dt+" | "+"End Date: "+endDate+" |  ";
						
				
				afPrtActLog.setUserNewFieldValue(oldOrNewVal);
				afPrtActLog.setUserOldFieldValue("NA");
				afPrtActLog.setUserActivity("Add New Commission");
				afPrtActLog.setUserActCreatedDateTime(new Date());
				afPrtActLog.setUserActIpVal(ipAddress);
				afPrtActLog.setUserActModule("Commision Management");
				afPrtActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
				
				commLogStatus = userActLogDao.createUserActLog(afPrtActLog);
				

				
			}

		} catch (Exception e) {
			e.printStackTrace();

			logger.error("@@@@Exception in CommissionManagementServiceImpl at addNewCommission() :", e);
			status = "failure";
			return status;
		}
		return status;
	}

	@Override
	public boolean checkPartnerProducts(Map<String, String> rpmap) {
		boolean checkFlag = false;
		List<AfPartnerCommission> afPartnerCommissions;
		try {

			afPartnerCommissions = commisionManagementDao.getPartnerCommissions(rpmap);
			if (afPartnerCommissions != null && afPartnerCommissions.size() > 0)
				checkFlag = true;
			logger.debug(">>>>>>>>checkPartnerProducts checkFlag>>>" + checkFlag);
		} catch (Exception e) {
			logger.error("@@@@Exception in CommissionManagementServiceImpl at checkPartnerProducts() :", e);
		}
		return checkFlag;
	}

	@Override
	public List<CommissionBean> getPartnersCommission() {
		String dateStr = "";
		Date dt = null;
		int categoryStatusCode;
		AfPartner partner = null;
		AfPartnerCategory partnercategory = null;
		AfPartnerProduct afpartnerproduct = null;
		List<AfPartnerCommission> partnercommission = null;
		List<CommissionBean> listcommissionbean = new ArrayList<CommissionBean>();
		try {
			partnercommission = commisionManagementDao.getPartnerCommissionList();

			if (partnercommission != null) {
				for (AfPartnerCommission afPartnerCommission : partnercommission) {
					CommissionBean commissionbean = new CommissionBean();

					commissionbean.setPrtCommNo(afPartnerCommission.getPrtCommNo());
					partner = afPartnerCommission.getAfPartner();

					if (partner != null) {
						commissionbean.setPartNo(partner.getPrtNo().toString());

						commissionbean.setPartnerName(partner.getPartnerName());
					}
					partnercategory = afPartnerCommission.getAfPartnerCategory();

					if (partnercategory != null) {
						commissionbean.setPrtCatNo(partnercategory.getPrtCatNo().toString());
						commissionbean.setPrtCategoryName(partnercategory.getPartnerCategoryName());
					}
					try {
						afpartnerproduct = afPartnerCommission.getAfPartnerProduct();
					} catch (Exception e) {
						logger.error("getPartnersCommission() afpartnerproduct ", e);
						continue;
					}
					if (afpartnerproduct != null) {
						commissionbean.setPrtProductNo(afpartnerproduct.getPrtPrdNo().toString());

						commissionbean.setPrtProductName(afpartnerproduct.getProductName());

					}
					dt = afPartnerCommission.getActivationDate();
					if (dt != null)
						dateStr = ServiceProviderUtil.getInstance().getFormattedDate(dt, "dd/MM/yyyy");
					if (dt != null && !dt.equals(""))
						commissionbean.setActivationDate(dateStr);

					dt = afPartnerCommission.getDeactivationDate();
					if (dt != null)
						dateStr = ServiceProviderUtil.getInstance().getFormattedDate(dt, "dd/MM/yyyy");
					if (dt != null && !dt.equals(""))
						commissionbean.setDeactivationDate(dateStr);

					categoryStatusCode = afPartnerCommission.getCommissionStatus();

					if (afPartnerCommission.getCommissionRate() != null)
						commissionbean.setCommissionRate(afPartnerCommission.getCommissionRate().toString());

					if (afPartnerCommission.getMemberShare() != null)
						commissionbean.setMemberShare(afPartnerCommission.getMemberShare().toString());

					if (afPartnerCommission.getProductBrand() != null)
						commissionbean.setProductBrand(afPartnerCommission.getProductBrand());

					if (afPartnerCommission.getCategoryPath() != null)
						commissionbean.setCategoryPath(afPartnerCommission.getCategoryPath());

					if (1 == categoryStatusCode) {
						commissionbean.setCommissionSatus(AfConstants.STATUS_ACTIVE);
						commissionbean.setCommissionStatusClass(AfConstants.STATUS_CLASS_GREEN);
						commissionbean.setCommissionStatusCode(1);

					}
					if (0 == categoryStatusCode) {
						commissionbean.setCommissionSatus(AfConstants.STATUS_IN_ACTIVE);
						commissionbean.setCommissionStatusClass(AfConstants.STATUS_CLASS_RED);
						commissionbean.setCommissionStatusCode(0);
					}
					listcommissionbean.add(commissionbean);
				}

			}
		} catch (Exception e) {
			logger.error("@@@@Exception in CommissionManagementServiceImpl at getPartnersCommission() :", e);
		}
		return listcommissionbean;
	}

	@Override
	public List<CommissionBean> getPartnersCommission(String fromDate, String toDate, String partNo) {
		String dateStr = "";
		Date dt = null;
		int categoryStatusCode;
		AfPartner partner = null;
		AfPartnerCategory partnercategory = null;
		AfPartnerProduct afpartnerproduct = null;
		List<AfPartnerCommission> partnercommission = null;
		List<CommissionBean> listcommissionbean = new ArrayList<>();
		Date  currentDt=new Date();
		try {
			partnercommission = commisionManagementDao.getPartnerCommissionList(fromDate, toDate, partNo);

			if (partnercommission != null) {
				for (AfPartnerCommission afPartnerCommission : partnercommission) {
					CommissionBean commissionbean = new CommissionBean();

					commissionbean.setPrtCommNo(afPartnerCommission.getPrtCommNo());
					partner = afPartnerCommission.getAfPartner();

					if (partner != null) {
						commissionbean.setPartNo(partner.getPrtNo().toString());

						commissionbean.setPartnerName(partner.getPartnerName());
					}
					partnercategory = afPartnerCommission.getAfPartnerCategory();

					if (partnercategory != null) {
						commissionbean.setPrtCatNo(partnercategory.getPrtCatNo().toString());
						commissionbean.setPrtCategoryName(partnercategory.getPartnerCategoryName());
					}
					try {
						afpartnerproduct = afPartnerCommission.getAfPartnerProduct();
					} catch (Exception e) {
						logger.error("getPartnersCommission() afpartnerproduct ", e);
						continue;
					}

					if (afpartnerproduct != null) {
						commissionbean.setPrtProductNo(afpartnerproduct.getPrtPrdNo().toString());
						commissionbean.setPrtProductName(afpartnerproduct.getProductName());
					}
					dt = afPartnerCommission.getActivationDate();
					if (dt != null)
						dateStr = ServiceProviderUtil.getInstance().getFormattedDate(dt, "dd/MM/yyyy");
					if (dt != null && !dt.equals(""))
						commissionbean.setActivationDate(dateStr);

					dt = afPartnerCommission.getDeactivationDate();
					if (dt != null)
						dateStr = ServiceProviderUtil.getInstance().getFormattedDate(dt, "dd/MM/yyyy");
					if (dt != null && !dt.equals(""))
						commissionbean.setDeactivationDate(dateStr);
					
					dateStr = ServiceProviderUtil.getInstance().getFormattedDate(dt,"dd/MM/yyyy");
					categoryStatusCode = afPartnerCommission.getCommissionStatus();

					if (afPartnerCommission.getCommissionRate() != null)
						commissionbean.setCommissionRate(afPartnerCommission.getCommissionRate().toString());

					if (afPartnerCommission.getMemberShare() != null)
						commissionbean.setMemberShare(afPartnerCommission.getMemberShare().toString());

					if (afPartnerCommission.getProductBrand() != null)
						commissionbean.setProductBrand(afPartnerCommission.getProductBrand());

					if (afPartnerCommission.getCategoryPath() != null)
						commissionbean.setCategoryPath(afPartnerCommission.getCategoryPath());
                  
					if (1 == categoryStatusCode) {
						commissionbean.setCommissionSatus(AfConstants.STATUS_ACTIVE);
						commissionbean.setCommissionStatusClass(AfConstants.STATUS_CLASS_GREEN);
						commissionbean.setCommissionStatusCode(1);

					}
                    
                    if(0 == categoryStatusCode) {
						commissionbean.setCommissionSatus(AfConstants.STATUS_IN_ACTIVE);
						commissionbean.setCommissionStatusClass(AfConstants.STATUS_CLASS_RED);
						commissionbean.setCommissionStatusCode(0);
					}
                    
                    
                     
						
						if(dt !=null && currentDt.before(dt)){
							commissionbean.setCommissionSatus(AfConstants.STATUS_ACTIVE);
							commissionbean.setCommissionStatusClass(AfConstants.STATUS_CLASS_GREEN);}
						else{
							commissionbean.setCommissionSatus(AfConstants.STATUS_IN_ACTIVE);
							commissionbean.setCommissionStatusClass(AfConstants.STATUS_CLASS_RED);
								
							}
						if(afPartnerCommission.getDeactivationDate()==null){
							commissionbean.setCommissionSatus(AfConstants.STATUS_ACTIVE);
							commissionbean.setCommissionStatusClass(AfConstants.STATUS_CLASS_GREEN);}
					
				
					if (afPartnerCommission.getMaxJpmiles() != null) {
						commissionbean.setMaxJpmiles(afPartnerCommission.getMaxJpmiles() + "");
					}

					commissionbean.setCommisionType(afPartnerCommission.getCommissionType());
					commissionbean.setTransactionStatus(afPartnerCommission.getTransactionStatus());
					if (afPartnerCommission.getLowerValue() != null) {
						commissionbean.setLowerValue(afPartnerCommission.getLowerValue().toString());
					}
					if (afPartnerCommission.getUpperValue() != null) {
						commissionbean.setUpperValue(afPartnerCommission.getUpperValue().toString());
					}
					if (afPartnerCommission.getBonusCommissionType() != null) {
					commissionbean.setBonusCommissionType(afPartnerCommission.getBonusCommissionType());
					}
					if (afPartnerCommission.getBonusCommissionRate() != null) {
						commissionbean.setBonusCommissionRate(afPartnerCommission.getBonusCommissionRate().toString());
					}

					commissionbean.setBonusPartnerCode(afPartnerCommission.getBonusPartnerCode());
					commissionbean.setBonusActivityCode(afPartnerCommission.getBonusActivityCode());

					listcommissionbean.add(commissionbean);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("@@@@Exception in CommissionManagementServiceImpl at getPartnersCommission() :", e);
		}
		return listcommissionbean;
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public String saveJPMileConfiguration(String jpMile,HttpServletRequest request) {
		String replaceInOld="";
		String replaceInNew="";
		String status = "failure";
		String typeName = "";
		List<AfEnumValues> enumList = new ArrayList<AfEnumValues>();
		enumList = commonDAO.getEnumValues("OFFERED_JPMILES");
		for (AfEnumValues afEnum : enumList) {
			if (afEnum.getTypeName().equalsIgnoreCase("OFFERED_JPMILES")) {
				typeName = afEnum.getTypeName();
			}
		}

		if (!typeName.equals("")) {

			try {
				status = commisionManagementDao.saveJPMileConfig(jpMile, typeName);
				replaceInNew=jpMile;
				if("success".equalsIgnoreCase(status)){
					replaceInOld = commonService.getJPMilesEnumValues("OFFERED_JPMILES");
					String ipAddress = request.getHeader("X-FORWARDED-FOR");
					if (ipAddress == null) {
						ipAddress = request.getRemoteAddr();
					}
					AfUserActivityLog updateUsActLog = new AfUserActivityLog();
					try {
						updateUsActLog.setUserActivity("Update jpmiles Commission");
						updateUsActLog.setUserActCreatedDateTime(new Date());
						updateUsActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
						updateUsActLog.setUserActIpVal(ipAddress);
						updateUsActLog.setUserActModule("Commission");
						updateUsActLog.setUserNewFieldValue(replaceInNew);
			    		updateUsActLog.setUserOldFieldValue(replaceInOld);
						userActLogDao.updateUserLog(updateUsActLog);
					}
					catch(Exception e){
						e.printStackTrace();
					}
				}

				
			} catch (Exception ex) {
				logger.error("@@@@ Exception in commissionManagementServiceImpl saveJPMileConfiguration() :", ex);
			}

		}
		return status;
	}

	@Override
	public List<Map<String, String>> getPartnerCommissionWebservice() {
		Map<String, String> afPartnerCmmMap = null;
		List<Map<String, String>> afPartnerCmmMapList = new ArrayList<>();
		List<AfPartnerCommission> afPrtCmmList = null;

		try {
			afPrtCmmList = commisionManagementDao.getPartnerCommissionWebservice();

			if (afPrtCmmList != null) {
				for (AfPartnerCommission afPc : afPrtCmmList) {
					afPartnerCmmMap = new HashMap<>();
					afPartnerCmmMap.put("PRT_COMM_NO", CommonUtil.getStringValue(afPc.getPrtCommNo()));
					afPartnerCmmMap.put("PRT_NO", CommonUtil.getStringValue(afPc.getAfPartner().getPrtNo()));
					afPartnerCmmMap.put("PRT_CAT_NO", "");
					afPartnerCmmMap.put("PRT_PRD_NO", "");
					afPartnerCmmMap.put("COMMISSION_RATE", CommonUtil.getStringValue(afPc.getCommissionRate()));
					afPartnerCmmMap.put("MEMBER_SHARE", CommonUtil.getStringValue(afPc.getMemberShare()));
					afPartnerCmmMap.put("MAX_JPMILES", CommonUtil.getStringValue(afPc.getMaxJpmiles()));
					afPartnerCmmMap.put("PRODUCT_BRAND", "");
					afPartnerCmmMap.put("CATEGORY_PATH", "");
					afPartnerCmmMap.put("ACTIVATION_DATE", CommonUtil.getStringValue(afPc.getActivationDate()));
					afPartnerCmmMap.put("DEACTIVATION_DATE", CommonUtil.getStringValue(afPc.getDeactivationDate()));
					afPartnerCmmMap.put("COMMISSION_STATUS", CommonUtil.getStringValue(afPc.getCommissionStatus()));
					afPartnerCmmMap.put("CREATED_TIME", CommonUtil.getStringValue(afPc.getCreatedTime()));
					afPartnerCmmMap.put("CREATED_BY", CommonUtil.getStringValue(afPc.getCreatedBy()));
					afPartnerCmmMap.put("MODIFIED_TIME", CommonUtil.getStringValue(afPc.getModifiedTime()));
					afPartnerCmmMap.put("MODIFIED_BY", CommonUtil.getStringValue(afPc.getModifiedBy()));
					afPartnerCmmMap.put("STATUS", CommonUtil.getStringValue(afPc.getStatus()));

					afPartnerCmmMapList.add(afPartnerCmmMap);
				}
			}
		} catch (Exception e) {
			logger.error("@@@@ Exception in commissionManagementServiceImpl getPartnerCommissionWebservice() :", e);
		}

		return afPartnerCmmMapList;
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public String updateCommissionReport(List<CommissionUploadBean> commissionList) {
		String partnerName;
		String activationDate;
		String deactivationDate;
		List<AfPartnerCommission> afPartnerCommission;
		String partnerstatus = "failure";
		Date startDate = null;
		Date endDate = null;
		AfPartnerProduct afpartnerproduct = null;
		int count=0;
		ArrayList<Integer> counts= new ArrayList<>();
		try {
			for (CommissionUploadBean uploadBean : commissionList) {
				partnerName = uploadBean.getPartnerName();
				activationDate = uploadBean.getActivationDate();
				deactivationDate = uploadBean.getDeactivationDate();
				startDate = ServiceProviderUtil.getInstance().getDateFromString(activationDate, "dd/MM/yyyy");
				endDate = ServiceProviderUtil.getInstance().getDateFromString(deactivationDate, "dd/MM/yyyy");
				Date today = new Date();
				
 				Integer prtNo = commisionManagementDao.getpartnerNo(partnerName);
 				if(prtNo==null){
 					count++;
 					counts.add(count);
 					continue;
 				}
 				
				Integer prtCat = commisionManagementDao.getAfPartnerCategoryName(uploadBean.getPrtCategoryName(), prtNo);

				Integer partnerProductNo = commisionManagementDao.getAfPartnerProductNo(uploadBean.getPrtProductName(), prtNo, prtCat);

				String path = commisionManagementDao.getAFcategoryPath(uploadBean.getCategoryPath(), prtNo, prtCat);
				partnerName = prtNo.toString();
				Float lowerValue= uploadBean.getLowerValueRange().equalsIgnoreCase("")?null:new Float(uploadBean.getLowerValueRange());
				Float upperValue= uploadBean.getUpperValueRange().equalsIgnoreCase("")?null:new Float(uploadBean.getUpperValueRange());
				Float memberShare= uploadBean.getMaximumJpMilesEarned().equalsIgnoreCase("")?null:new Float(uploadBean.getMaximumJpMilesEarned());
				Float bonusCommissionRate= uploadBean.getBonusJpMilesCommisionValue().equalsIgnoreCase("")?null:new Float(uploadBean.getBonusJpMilesCommisionValue());
				Float commissionRate= uploadBean.getCommisionValue().equalsIgnoreCase("")?null:new Float(uploadBean.getCommisionValue());
				
				if (partnerName.isEmpty() || uploadBean.getCommisionValue().isEmpty() ||  uploadBean.getJpMilesCommsion().isEmpty() || uploadBean.getCommisionStatus().isEmpty() || uploadBean.getActivationDate().isEmpty()){
					count++;
 					counts.add(count);
					continue;
				}
				

				afPartnerCommission = commisionManagementDao.getPartnerCommissionListUpload(activationDate,
						deactivationDate, partnerName, prtCat, partnerProductNo, path, upperValue, lowerValue );
			           Boolean checkEndDate =  endDate==null?true:endDate.after(today);   
				if (afPartnerCommission == null || afPartnerCommission.isEmpty()) {
					AfPartnerCommission partnerCommission = new AfPartnerCommission();
					if(startDate !=null && startDate.after(today) && checkEndDate){				
					partnerCommission.setMemberShare(memberShare);
					partnerCommission.setUpperValue(upperValue);
					partnerCommission.setLowerValue(lowerValue);
					partnerCommission.setAfPartnerCategory(partnermanagementDAO.getPartnerCategoryByPartnerCategoryNo(prtCat));
					if (partnerProductNo != null) {
						afpartnerproduct = commisionManagementDao.getAfPartnerProducts(partnerProductNo.toString());
						partnerCommission.setAfPartnerProduct(afpartnerproduct);
					}
					partnerCommission.setCommissionType(Byte.valueOf(uploadBean.getPartnerCommisionType()));
					partnerCommission.setCommissionRate(commissionRate);
					partnerCommission.setBonusCommissionType(Byte.valueOf(uploadBean.getBonusJpMilesCommisionType()));
					partnerCommission.setBonusCommissionRate(bonusCommissionRate);
					partnerCommission.setCreatedTime(new Date());
					partnerCommission.setActivationDate(startDate);
					partnerCommission.setDeactivationDate(endDate);
					partnerCommission.setAfPartner(partnermanagementDAO.getPartnerById(prtNo));
					partnerCommission.setCategoryPath(path);
					partnerCommission.setStatus((byte)1);
					partnerCommission.setCommissionStatus(Byte.parseByte(uploadBean.getTransactionStatus()));
					partnerCommission.setBonusPartnerCode(uploadBean.getBonusPartnerCode());
					partnerCommission.setBonusActivityCode(uploadBean.getBonusActivateCode());
					partnerCommission.setTransactionStatus(Byte.parseByte(uploadBean.getTransactionStatus()));
					partnerstatus = commisionManagementDao.updateCommission(partnerCommission);
					}
				}else{
					count++;
 					counts.add(count);
					continue;
					
				}
			
					/*
					for(AfPartnerCommission afPartnerCommissions:afPartnerCommission){
						
						
						 Date today = new Date();
						 Date startDateCheck = null;
						 Date endDateCheck = null;
						 Date yesterday = DateUtils.addDays(new Date(), -1);
						 SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
						 sdf.format(yesterday);
						 
						 startDateCheck = afPartnerCommissions.getActivationDate();
						 endDateCheck = afPartnerCommissions.getDeactivationDate();
	
						if(startDateCheck!=null && endDateCheck!=null){
							
							if(startDateCheck.after(today)){
								afPartnerCommissions.setActivationDate(startDate);
								afPartnerCommissions.setDeactivationDate(endDate);
								afPartnerCommissions.setMemberShare(memberShare);
								afPartnerCommissions.setUpperValue(upperValue);
								afPartnerCommissions.setLowerValue(lowerValue);
								afPartnerCommissions.setAfPartnerCategory(partnermanagementDAO.getPartnerCategoryByPartnerCategoryNo(prtCat));
								if (partnerProductNo != null) {
									afpartnerproduct = commisionManagementDao.getAfPartnerProducts(partnerProductNo.toString());
									afPartnerCommissions.setAfPartnerProduct(afpartnerproduct);
								}
								afPartnerCommissions.setCommissionType(Byte.valueOf(uploadBean.getPartnerCommisionType()));
								afPartnerCommissions.setCommissionRate(commissionRate);
								afPartnerCommissions.setBonusCommissionType(Byte.valueOf(uploadBean.getBonusJpMilesCommisionType()));
								afPartnerCommissions.setBonusCommissionRate(bonusCommissionRate);
								afPartnerCommissions.setCreatedTime(new Date());
								afPartnerCommissions.setAfPartner(partnermanagementDAO.getPartnerById(prtNo));
								afPartnerCommissions.setCategoryPath(path);
								afPartnerCommissions.setStatus(new Byte (uploadBean.getCommisionStatus()));
								afPartnerCommissions.setCommissionStatus(new Byte (uploadBean.getCommisionStatus()));
								
						}
							else if(startDateCheck.before(today) && endDateCheck.after(today)){
								
								if(startDate.after(startDateCheck) && startDate.before(endDateCheck)){
									
									if(endDate.after(endDateCheck)){
										
										AfPartnerCommission newAfPrtCom = new AfPartnerCommission();
										afPartnerCommissions.setDeactivationDate(yesterday);
										newAfPrtCom.setActivationDate(today);
										newAfPrtCom.setDeactivationDate(endDate);
										newAfPrtCom.setMemberShare(memberShare);
										newAfPrtCom.setUpperValue(upperValue);
										newAfPrtCom.setLowerValue(lowerValue);
										afPartnerCommissions.setAfPartnerCategory(partnermanagementDAO.getPartnerCategoryByPartnerCategoryNo(prtCat));
										if (partnerProductNo != null) {
											afpartnerproduct = commisionManagementDao.getAfPartnerProducts(partnerProductNo.toString());
											newAfPrtCom.setAfPartnerProduct(afpartnerproduct);
										}
										newAfPrtCom.setCommissionType(Byte.valueOf(uploadBean.getPartnerCommisionType()));
										newAfPrtCom.setCommissionRate(commissionRate);
										newAfPrtCom.setBonusCommissionType(Byte.valueOf(uploadBean.getBonusJpMilesCommisionType()));
										newAfPrtCom.setBonusCommissionRate(bonusCommissionRate);
										newAfPrtCom.setCreatedTime(new Date());
										newAfPrtCom.setAfPartner(partnermanagementDAO.getPartnerById(prtNo));
										newAfPrtCom.setCategoryPath(path);
										newAfPrtCom.setStatus(new Byte (uploadBean.getCommisionStatus()));
										newAfPrtCom.setCommissionStatus(new Byte (uploadBean.getCommisionStatus()));
										
										commisionManagementDao.updateCommission(newAfPrtCom); 
									}
								}
								
							}
							
						}else if(startDateCheck!=null && endDateCheck==null){
							if(startDateCheck.before(today)){
								afPartnerCommissions.setDeactivationDate(endDate);
								afPartnerCommissions.setStatus(new Byte (uploadBean.getCommisionStatus()));
							}
							}
						else{
							
						}
						
						 
						partnerstatus = commisionManagementDao.updateCommission(afPartnerCommissions); 
						
						}
					
					count++;
					counts.add(count);
					continue;
					
				} else {
				new Date();
					AfPartnerCommission partnerCommission = new AfPartnerCommission();
					
					partnerCommission.getActivationDate();
					partnerCommission.getDeactivationDate();
					
					partnerCommission.setMemberShare(memberShare);
					partnerCommission.setUpperValue(upperValue);
					partnerCommission.setLowerValue(lowerValue);
					partnerCommission.setAfPartnerCategory(partnermanagementDAO.getPartnerCategoryByPartnerCategoryNo(prtCat));
					if (partnerProductNo != null) {
						afpartnerproduct = commisionManagementDao.getAfPartnerProducts(partnerProductNo.toString());
						partnerCommission.setAfPartnerProduct(afpartnerproduct);
					}
					partnerCommission.setCommissionType(Byte.valueOf(uploadBean.getPartnerCommisionType()));
					partnerCommission.setCommissionRate(commissionRate);
					partnerCommission.setBonusCommissionType(Byte.valueOf(uploadBean.getBonusJpMilesCommisionType()));
					partnerCommission.setBonusCommissionRate(bonusCommissionRate);
					partnerCommission.setCreatedTime(new Date());
					partnerCommission.setActivationDate(startDate);
					partnerCommission.setDeactivationDate(endDate);
					partnerCommission.setAfPartner(partnermanagementDAO.getPartnerById(prtNo));
					partnerCommission.setCategoryPath(path);
					partnerCommission.setStatus((byte)1);
					partnerCommission.setCommissionStatus((byte)1);
					
					partnerstatus = commisionManagementDao.updateCommission(partnerCommission);
				}*/
				count++;
				if(count==commissionList.size())break;
				
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("@@@Exception in commissionManagementServiceImpl updateCommissionReport():", e);
		}
		if(!counts.isEmpty()){
			partnerstatus="failure "+counts.toString().replace(", ", ",")
					
					+ ""
					+ ""
					+ ""
					+ "";
		}
		return partnerstatus;
	}

	@Override
	public List<CommissionBean> getPartnersCommissionPagination(String fromDate, String toDate, String partnerNo,
			int length, int start) {

		String dateStr = "";
		Date dt = null;
		int categoryStatusCode;
		AfPartner partner = null;
		AfPartnerCategory partnercategory = null;
		AfPartnerProduct afpartnerproduct = null;
		List<AfPartnerCommission> partnercommission = null;
		List<CommissionBean> listcommissionbean = new ArrayList<>();
		Date  currentDt=new Date();
		try {
			partnercommission = commisionManagementDao.getPartnerCommissionList(fromDate, toDate, partnerNo,length,start);

			if (partnercommission != null) {
				for (AfPartnerCommission afPartnerCommission : partnercommission) {
					CommissionBean commissionbean = new CommissionBean();

					commissionbean.setPrtCommNo(afPartnerCommission.getPrtCommNo());
					partner = afPartnerCommission.getAfPartner();

					if (partner != null) {
						commissionbean.setPartNo(partner.getPrtNo().toString());

						commissionbean.setPartnerName(partner.getPartnerName());
					}
					partnercategory = afPartnerCommission.getAfPartnerCategory();

					if (partnercategory != null) {
						commissionbean.setPrtCatNo(partnercategory.getPrtCatNo().toString());
						commissionbean.setPrtCategoryName(partnercategory.getPartnerCategoryName());
					}
					try {
						afpartnerproduct = afPartnerCommission.getAfPartnerProduct();
					} catch (Exception e) {
						logger.error("getPartnersCommission() afpartnerproduct ", e);
						continue;
					}

					if (afpartnerproduct != null) {
						commissionbean.setPrtProductNo(afpartnerproduct.getPrtPrdNo().toString());
						commissionbean.setPrtProductName(afpartnerproduct.getProductName());
					}
					dt = afPartnerCommission.getActivationDate();
					if (dt != null)
						dateStr = ServiceProviderUtil.getInstance().getFormattedDate(dt, "dd/MM/yyyy");
					if (dt != null && !dt.equals(""))
						commissionbean.setActivationDate(dateStr);

					dt = afPartnerCommission.getDeactivationDate();
					if (dt != null)
						dateStr = ServiceProviderUtil.getInstance().getFormattedDate(dt, "dd/MM/yyyy");
					if (dt != null && !dt.equals(""))
						commissionbean.setDeactivationDate(dateStr);
					
					dateStr = ServiceProviderUtil.getInstance().getFormattedDate(dt,"dd/MM/yyyy");
					categoryStatusCode = afPartnerCommission.getCommissionStatus();

					if (afPartnerCommission.getCommissionRate() != null)
						commissionbean.setCommissionRate(afPartnerCommission.getCommissionRate().toString());

					if (afPartnerCommission.getMemberShare() != null)
						commissionbean.setMemberShare(afPartnerCommission.getMemberShare().toString());

					if (afPartnerCommission.getProductBrand() != null)
						commissionbean.setProductBrand(afPartnerCommission.getProductBrand());

					if (afPartnerCommission.getCategoryPath() != null)
						commissionbean.setCategoryPath(afPartnerCommission.getCategoryPath());
                  
					
						if(dt !=null && currentDt.before(dt)){
							commissionbean.setCommissionSatus(AfConstants.STATUS_ACTIVE);
							commissionbean.setCommissionStatusClass(AfConstants.STATUS_CLASS_GREEN);}
						else{
							commissionbean.setCommissionSatus(AfConstants.STATUS_IN_ACTIVE);
							commissionbean.setCommissionStatusClass(AfConstants.STATUS_CLASS_RED);
								
							}
						if(afPartnerCommission.getDeactivationDate()==null){
							commissionbean.setCommissionSatus(AfConstants.STATUS_ACTIVE);
							commissionbean.setCommissionStatusClass(AfConstants.STATUS_CLASS_GREEN);
							}
					
						if (1 == categoryStatusCode && commissionbean.getCommissionSatus().equalsIgnoreCase(AfConstants.STATUS_ACTIVE)) {
							commissionbean.setCommissionSatus(AfConstants.STATUS_ACTIVE);
							commissionbean.setCommissionStatusClass(AfConstants.STATUS_CLASS_GREEN);
							commissionbean.setCommissionStatusCode(1);

						}else{
							commissionbean.setCommissionSatus(AfConstants.STATUS_IN_ACTIVE);
							commissionbean.setCommissionStatusClass(AfConstants.STATUS_CLASS_RED);
							commissionbean.setCommissionStatusCode(0);
						}
					if (null != afPartnerCommission.getMaxJpmiles()) {
						commissionbean.setMaxJpmiles(afPartnerCommission.getMaxJpmiles() + "");
					}
					commissionbean.setCommisionType(afPartnerCommission.getCommissionType());
					commissionbean.setTransactionStatus(afPartnerCommission.getTransactionStatus());
					if (afPartnerCommission.getLowerValue() != null) {
						commissionbean.setLowerValue(afPartnerCommission.getLowerValue().toString());
					}
					if (afPartnerCommission.getUpperValue() != null) {
						commissionbean.setUpperValue(afPartnerCommission.getUpperValue().toString());
					}
					if (afPartnerCommission.getBonusCommissionType() != null) {
					commissionbean.setBonusCommissionType(afPartnerCommission.getBonusCommissionType());
					}
					if (afPartnerCommission.getBonusCommissionRate() != null) {
						commissionbean.setBonusCommissionRate(afPartnerCommission.getBonusCommissionRate().toString());
					}

					commissionbean.setBonusPartnerCode(afPartnerCommission.getBonusPartnerCode());
					commissionbean.setBonusActivityCode(afPartnerCommission.getBonusActivityCode());

					listcommissionbean.add(commissionbean);
				}
			}
		} catch (Exception e) {
			logger.error("@@@@Exception in CommissionManagementServiceImpl at getPartnersCommissionPagination() :", e);
		}
		return listcommissionbean;
	
	}

	@Override
	public Long getPartnersCommissionPaginationCount(String fromDate, String toDate, String partnerNo) {
		Long totalCount =0L;
		try {
			totalCount = commisionManagementDao.getPartnersCommissionPaginationCount(fromDate,toDate,partnerNo);
		} catch (Exception e) {
			logger.error("@@@@Exception in CommissionManagementServiceImpl at getPartnersCommissionPaginationCount() :", e);
		}
		return totalCount;
	}

	@Override
	public List<CommissionBean> getCommissionExportCsvList() {
		List<Object[]> reportObject = null;
		List<CommissionBean> reportList = null;
		String status;
		try{
			reportObject = commisionManagementDao.getCommissionExportCsvList();
			if (reportObject != null && !reportObject.isEmpty()) {
				reportList = new ArrayList<>();
			for (Object[] row : reportObject) {
				CommissionBean commisiionBeanLinkList =new CommissionBean();
				commisiionBeanLinkList.setPartnerName(ServiceProviderUtil.checkNullSafeWithObject(row[0], "NA"));
				commisiionBeanLinkList.setPrtCategoryName((ServiceProviderUtil.checkNullSafeWithObject(row[1], "NA")));
				commisiionBeanLinkList.setPrtProductName(ServiceProviderUtil.checkNullSafeWithObject(row[2], "NA"));
				commisiionBeanLinkList.setCommisionType((byte)row[3]);
				commisiionBeanLinkList.setTransactionStatus((byte)row[4]);
				commisiionBeanLinkList.setCommissionRate(ServiceProviderUtil.checkNullSafeWithObject(row[5], "NA"));
				commisiionBeanLinkList.setMemberShare(ServiceProviderUtil.checkNullSafeWithObject(row[6], "NA"));
				commisiionBeanLinkList.setLowerValue(ServiceProviderUtil.checkNullSafeWithObject(row[7], "NA"));
				commisiionBeanLinkList.setUpperValue(ServiceProviderUtil.checkNullSafeWithObject(row[8], "NA"));
				commisiionBeanLinkList.setActivationDate(ServiceProviderUtil.checkNullSafeWithObject(row[9], "NA"));
				commisiionBeanLinkList.setDeactivationDate(ServiceProviderUtil.checkNullSafeWithObject(row[10], "NA"));
		  	    status= ServiceProviderUtil.checkNullSafeWithObject(row[11], "NA");
			if("1".equalsIgnoreCase(status)){
				commisiionBeanLinkList.setCommissionSatus(AfConstants.STATUS_ACTIVE);
			}
			else {
				commisiionBeanLinkList.setCommissionSatus(AfConstants.STATUS_IN_ACTIVE);
			}
			
			
			reportList.add(commisiionBeanLinkList);
			}
		}
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error("@@Exception in offers&dealsServiceimpl getExportOffersDealList():", e);
		}
		
		return reportList;
	}


}

