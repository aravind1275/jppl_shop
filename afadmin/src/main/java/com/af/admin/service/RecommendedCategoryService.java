package com.af.admin.service;

import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import com.af.admin.beans.RecommendedCategoryBean;
import com.af.admin.beans.RecommendedProductBean;


public interface RecommendedCategoryService {
	
	public List<RecommendedCategoryBean> getRecommendedCategories();
	public List<RecommendedCategoryBean> getRecommendedCategories(String prtNo);
	public String createRecommendedCategory(RecommendedCategoryBean recommendedCategoryBean);
	public boolean checkRecommendedCategory(Map<String, String> rpmap);
	public boolean checkRecommendedCategoryPosition(String recommendedCategoryPosition);
	public RecommendedCategoryBean getRecommendedCategoryByNo(String rpNo);
	public String updateRecommendedCategory(RecommendedCategoryBean recommendedCategoryBean);	
	public String deleteRecommendedCategory(Integer bnrNo);
	public boolean checkRcCatNo(String rcCatNo);

	
}
