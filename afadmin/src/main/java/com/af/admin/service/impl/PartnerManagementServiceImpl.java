package com.af.admin.service.impl;

import java.net.URLEncoder;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.af.admin.DAO.CommonDAO;
import com.af.admin.DAO.PartnerManagementDAO;
import com.af.admin.DAO.UserActivityLogDAO;
import com.af.admin.beans.AfAmzonoPartnerCategoryBean;
import com.af.admin.beans.KeyWordBean;
import com.af.admin.beans.PartnerBean;
import com.af.admin.beans.PartnerMappingBean;
import com.af.admin.constants.AfConstants;
import com.af.admin.entity.AfAmazonPartnerCategory;
import com.af.admin.entity.AfPartner;
import com.af.admin.entity.AfPartnerCategory;
import com.af.admin.entity.AfPartnerKeyword;
import com.af.admin.entity.AfPartnerMapping;
import com.af.admin.entity.AfPartnerProduct;
import com.af.admin.entity.AfSeo;
import com.af.admin.entity.AfUserActivityLog;
import com.af.admin.service.CommonService;
import com.af.admin.service.PartnerManagementService;
import com.af.admin.utils.AdminUserActivityUtil;
import com.af.admin.utils.CommonUtil;
import com.af.admin.utils.ServiceProviderUtil;


@Component
@Transactional(readOnly=true)
public class PartnerManagementServiceImpl implements PartnerManagementService{
	
	private Logger logger = LogManager.getLogger(PartnerManagementServiceImpl.class.getName());
	
	@Autowired
	PartnerManagementDAO partnermanagementDAO;
	
	@Autowired
	CommonDAO commonDAO;
	
	@Autowired
	CommonService commonService;
	
	@Autowired
	UserActivityLogDAO userActLogDao;

	@Autowired
	AdminUserActivityUtil adminUserActivityUtil;
	
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
	
	@Override
	public List<PartnerBean> getPartners(String partnerType) {
		List<AfPartner> afPartnerList=null;
		List<PartnerBean> partnerList=new ArrayList<>();
		int partnerStatusCode;
		String dateStr="";
		String[] affiliateVal=null;
		String[] sourceVal=null;
		String[] referenceVal=null;
		String[] paramfourVal=null;
		Date dt=null;
		
		try{
			afPartnerList=partnermanagementDAO.getPartners(partnerType);
		for(AfPartner afPartner:afPartnerList){
			PartnerBean partnerBean=new PartnerBean();
			partnerBean.setPrtNo(afPartner.getPrtNo());
			partnerBean.setPartnerId(afPartner.getPartnerId());
			partnerBean.setPartnerName(afPartner.getPartnerName()!=null?afPartner.getPartnerName():"");			
			partnerBean.setRatePerMile(afPartner.getRatePerMile()!=null?afPartner.getRatePerMile():0);
			partnerBean.setHostPartnerCode(afPartner.getHostPartner()!=null?afPartner.getHostPartner():0);
			partnerBean.setDefaultPartnerCode(afPartner.getDefaultPartner());
			partnerBean.setLogoPartnerCode((Byte)afPartner.getLogoPartner()!=null?afPartner.getLogoPartner():0);
			partnerBean.setOffLinePartnerCode((Byte)afPartner.getOffLinePartner()!=null?afPartner.getOffLinePartner():0);
			
			partnerBean.setReadPartnerCode((Byte)afPartner.getReadPartner()!=null?afPartner.getReadPartner():0);
			partnerBean.setTalkPartnerCode((Byte)afPartner.getTalkPartner()!=null?afPartner.getTalkPartner():0);
			
			partnerBean.setPartnerLandingUrl(afPartner.getPartnerLandingUrl()!=null?afPartner.getPartnerLandingUrl():"");
			partnerBean.setLogoPartnerSearchUrl(afPartner.getLogoPartnerSearchUrl()!=null?afPartner.getLogoPartnerSearchUrl():"");
			partnerBean.setLogoPrtDisplayOrder(afPartner.getPartnerPosition()!=null?afPartner.getPartnerPosition():0);
			partnerBean.setLogoPrtTilePos(afPartner.getLogoPartnerPosition()!=null?afPartner.getLogoPartnerPosition():0);
			dt=afPartner.getActivationDate()!=null?afPartner.getActivationDate():new Date();
			dateStr=ServiceProviderUtil.getInstance().getFormattedDate(afPartner.getActivationDate(),"dd/MM/yyyy");
			if(dt!=null && !dt.equals(""))
			partnerBean.setActivationDate(dateStr);
			dt=afPartner.getDeactivationDate()!=null?afPartner.getDeactivationDate():new Date();
			dateStr=ServiceProviderUtil.getInstance().getFormattedDate(afPartner.getDeactivationDate(),"dd/MM/yyyy");
			if(dt!=null && !dt.equals(""))
			partnerBean.setDeactivationDate(dateStr);
			partnerBean.setPartnerStatusCode(afPartner.getPartnerStatus());
			if(afPartner.getPartnerLogoUrl()!=null)
			partnerBean.setPartnerImageUrl(afPartner.getPartnerLogoUrl());
			partnerStatusCode=afPartner.getPartnerStatus();
			
			String jpmDesc = "";
			jpmDesc = afPartner.getJpmileDesc()!=null ?afPartner.getJpmileDesc() : "";
			jpmDesc = URLEncoder.encode(jpmDesc, "UTF-8");
			partnerBean.setJpmileDesc(jpmDesc);
			
			partnerBean.setPartnerDesc(afPartner.getPartnerDesc()!=null?afPartner.getPartnerDesc():"");
			partnerBean.setJpmileReconPeriod(null!=afPartner.getJpmileReconPeriod()?afPartner.getJpmileReconPeriod()+"":"");
			partnerBean.setActivityId(null!=afPartner.getActivityId()?afPartner.getActivityId()+"":"");

			if(1==partnerStatusCode){
				partnerBean.setPartnerStatus(AfConstants.STATUS_ACTIVE);
				partnerBean.setPartnerStatusClass(AfConstants.STATUS_CLASS_GREEN);
				
			}
			if(0==partnerStatusCode){
				partnerBean.setPartnerStatus(AfConstants.STATUS_IN_ACTIVE);
				partnerBean.setPartnerStatusClass(AfConstants.STATUS_CLASS_RED);
				
			}
			
			if(afPartner.getPartnerIconUrl()!=null)
				partnerBean.setPartnerIconUrl(afPartner.getPartnerIconUrl());
			
			if(afPartner.getAffiliateId()!=null){
				affiliateVal = afPartner.getAffiliateId().toString().split("=");
				if(affiliateVal.length != 0){
					if(affiliateVal.length > 1){
						partnerBean.setAffiliateIdName(affiliateVal[0]);
						partnerBean.setAffiliateId(affiliateVal[1]);
					}else{
					partnerBean.setAffiliateIdName(affiliateVal[0]);
					}
				}else{
					partnerBean.setAffiliateId("");
					partnerBean.setAffiliateIdName("");
				}
			}else{
				partnerBean.setAffiliateId("");
				partnerBean.setAffiliateIdName("");
			}

			if(afPartner.getSourceOfInfo()!=null){
				sourceVal = afPartner.getSourceOfInfo().toString().split("=");
				if(sourceVal.length != 0){
					if(sourceVal.length > 1){
						partnerBean.setSourceOfInfoName(sourceVal[0]);
						partnerBean.setSourceOfInfo(sourceVal[1]);
					}else{
						partnerBean.setSourceOfInfoName(sourceVal[0]);
					}
				}else{
					partnerBean.setSourceOfInfo("");
					partnerBean.setSourceOfInfoName("");
				}	
			}else{
				partnerBean.setSourceOfInfo("");
				partnerBean.setSourceOfInfoName("");
			}
			
			if(afPartner.getParamFour()!=null){
				paramfourVal = afPartner.getParamFour().toString().split("=",2);
				if(paramfourVal.length != 0){
					if(paramfourVal.length > 1){
						partnerBean.setParamFourName(paramfourVal[0]);
						partnerBean.setParamFour(paramfourVal[1]);
					}else{
						partnerBean.setParamFourName(paramfourVal[0]);
					}
				}else{
					partnerBean.setParamFour("");
					partnerBean.setParamFourName("");
				}	
			}else{
				partnerBean.setParamFour("");
				partnerBean.setParamFourName("");
			}

			if(afPartner.getReferenceToken()!=null){
				referenceVal = afPartner.getReferenceToken().toString().split("=");
				if(referenceVal.length != 0){
					if(referenceVal.length > 1){
						partnerBean.setReferenceTokenName(referenceVal[0]);
						partnerBean.setReferenceToken(referenceVal[1]);
					}else{
						partnerBean.setReferenceTokenName(referenceVal[0]);
					}
				}else{
					partnerBean.setReferenceToken("");
					partnerBean.setReferenceTokenName("");
				}	
			}else{
				partnerBean.setReferenceToken("");
				partnerBean.setReferenceTokenName("");
			}
			
			String tAndC = afPartner.getTermsAndCondition();
			
			if(tAndC != null){
				tAndC = URLEncoder.encode(tAndC, "UTF-8");
			}
			
			partnerBean.setTermsAndCondition(tAndC!=null?tAndC:"");
			partnerBean.setAllowJpnumber(afPartner.getAllowJpnumber()+"");
			partnerBean.setDisplayPrtPopup(afPartner.getDisplayPrtPopup()+"");
			
			partnerList.add(partnerBean);
		}
		}catch(Exception e){
			logger.error("@@@@ Exception in PartnerManagementServiceImpl getPartners()  by type :"+e);
		}
		return partnerList;
	}

	public List<PartnerBean> getPartners() {
		List<AfPartner> afPartnerList=null;
		List<PartnerBean> partnerList=new ArrayList<>();
		int partnerStatusCode;
		String dateStr="";

		Date dt=null;

		try{
			afPartnerList=partnermanagementDAO.getPartners();
			for(AfPartner afPartner:afPartnerList){
				PartnerBean partnerBean=new PartnerBean();
				partnerBean.setPrtNo(afPartner.getPrtNo());
				partnerBean.setPartnerId(afPartner.getPartnerId());
				partnerBean.setPartnerName(afPartner.getPartnerName()!=null?afPartner.getPartnerName():"");			
				partnerBean.setRatePerMile(afPartner.getRatePerMile()!=null?afPartner.getRatePerMile():0);
				partnerBean.setHostPartnerCode(afPartner.getHostPartner()!=null?afPartner.getHostPartner():0);
				partnerBean.setDefaultPartnerCode(afPartner.getDefaultPartner());
				partnerBean.setJpmileDesc(afPartner.getJpmileDesc()!=null?afPartner.getJpmileDesc():"");
				partnerBean.setPartnerDesc(afPartner.getPartnerDesc()!=null?afPartner.getPartnerDesc():"");
				partnerBean.setJpmileReconPeriod(null!=afPartner.getJpmileReconPeriod()?afPartner.getJpmileReconPeriod()+"":"");
				
				dt=afPartner.getActivationDate()!=null?afPartner.getActivationDate():new Date();
				dateStr=ServiceProviderUtil.getInstance().getFormattedDate(afPartner.getActivationDate(),"dd/MM/yyyy");
				if(dt!=null && !dt.equals(""))
					partnerBean.setActivationDate(dateStr);
				dt=afPartner.getDeactivationDate()!=null?afPartner.getDeactivationDate():new Date();
				dateStr=ServiceProviderUtil.getInstance().getFormattedDate(afPartner.getDeactivationDate(),"dd/MM/yyyy");
				if(dt!=null && !dt.equals(""))
					partnerBean.setDeactivationDate(dateStr);
				partnerBean.setPartnerStatusCode(afPartner.getPartnerStatus());
				if(afPartner.getPartnerLogoUrl()!=null)
					partnerBean.setPartnerImageUrl(afPartner.getPartnerLogoUrl());
				partnerStatusCode=afPartner.getPartnerStatus();

				if(1==partnerStatusCode){
					partnerBean.setPartnerStatus(AfConstants.STATUS_ACTIVE);
					partnerBean.setPartnerStatusClass(AfConstants.STATUS_CLASS_GREEN);

				}
				if(0==partnerStatusCode){
					partnerBean.setPartnerStatus(AfConstants.STATUS_IN_ACTIVE);
					partnerBean.setPartnerStatusClass(AfConstants.STATUS_CLASS_RED);
				}
				String tAndC = afPartner.getTermsAndCondition();
				if(tAndC != null){
					tAndC = URLEncoder.encode(tAndC, "UTF-8");
				}
				
				partnerBean.setTermsAndCondition(tAndC!=null?tAndC:"");
				partnerBean.setAllowJpnumber(afPartner.getAllowJpnumber()+"");
				partnerBean.setDisplayPrtPopup(afPartner.getDisplayPrtPopup()+"");
				
				partnerList.add(partnerBean);
			}
		}catch(Exception e){
			logger.error("@@@@ Exception in PartnerManagementServiceImpl getPartners() :"+e);
		}
		return partnerList;
	}


	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String createPartner(PartnerBean partnerBean,HttpServletRequest request) {
		String partnerName="";
		String partnerId="";
		String activationDate="";
		String deactivationDate="";
		Date dt=null;
		String status="failure";
		String logopartnerLandingUrl=null;
		String logopartnerSearchUrl=null;
		DateFormat sourceDf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		AfPartner afpartner=new AfPartner();
	
		Date dt1=new Date();
		String hh_mi_ss=""+dt1;
		
		String format[]=hh_mi_ss.split(" ");
		hh_mi_ss=format[3];		
		try{
			partnerName=partnerBean.getPartnerName();
			partnerName=(null!=partnerName)?partnerName.trim():"";
			partnerId=partnerBean.getPartnerId();
			partnerId=(null!=partnerId)?partnerId.trim():"";
			
			activationDate=partnerBean.getActivationDate();
			activationDate=activationDate+" "+hh_mi_ss;
			if(activationDate.length()>0)
			dt=sourceDf.parse(activationDate);
			afpartner.setActivationDate(dt);
			
			dt = null;
			deactivationDate=partnerBean.getDeactivationDate();
			if(deactivationDate.length()>0){
				deactivationDate=deactivationDate+" "+hh_mi_ss;
				dt=sourceDf.parse(deactivationDate);
			}

			afpartner.setDeactivationDate(dt);
			afpartner.setPartnerId(partnerBean.getPartnerId());
			afpartner.setPartnerName(partnerBean.getPartnerName());		
			afpartner.setRatePerMile(partnerBean.getRatePerMile());
			
			afpartner.setPartnerLogoUrl(partnerBean.getPartnerImageUrl().replaceAll(",",""));
			afpartner.setPartnerIconUrl(partnerBean.getPartnerIconUrl().replaceAll(",",""));
			afpartner.setPartnerPopupLogoUrl(partnerBean.getPartnerPopupImageUrl().replaceAll(",",""));
			
			System.out.println("Partner Logo Url"+afpartner.getPartnerPopupLogoUrl());
			
			afpartner.setPartnerLandingUrl(partnerBean.getPartnerLandingUrl()); 
			
			afpartner.setTermsAndCondition(partnerBean.getTermsAndCondition());
			
			if(partnerBean.getDisplayPrtPopup() != null){
				afpartner.setDisplayPrtPopup(Byte.parseByte(partnerBean.getDisplayPrtPopup()));
			}
			if(partnerBean.getAllowJpnumber() != null){
				afpartner.setAllowJpnumber(Byte.parseByte(partnerBean.getAllowJpnumber()));
			}
			
		switch(partnerBean.getPartnerType()){
			case "hostPartner":
				afpartner.setHostPartner((byte)1);
				break;
			case "searchPartner":
				afpartner.setDefaultPartner((byte)1);
				break;
			case "logoPartner":
				afpartner.setLogoPartner((byte)1);
				break;
			case "offLinePartner":
				afpartner.setOffLinePartner((byte)1);
				break;	
			case "readPartner":
				afpartner.setReadPartner((byte)1);
				break;
			case "talkPartner":
				afpartner.setTalkPartner((byte)1);
				break;
		}

		afpartner.setPartnerStatus((byte)partnerBean.getPartnerStatusCode());
		afpartner.setStatus((byte)1);
		afpartner.setCreatedTime(new Date());
		afpartner.setCreatedBy(1);
		
		logopartnerSearchUrl=(null!=partnerBean.getLogoPartnerSearchUrl())?partnerBean.getLogoPartnerSearchUrl().trim():"";
		afpartner.setLogoPartnerSearchUrl(logopartnerSearchUrl);
		 
		if((null!=partnerBean.getLogoPartner() && partnerBean.getLogoPartner().length>0) || (null!=partnerBean.getOffLinePartner() && partnerBean.getOffLinePartner().length>0))
			
			afpartner.setLogoPartner((byte)partnerBean.getLogoPartner()[0]);
			afpartner.setPartnerPosition((byte)partnerBean.getLogoPrtDisplayOrder());
			afpartner.setLogoPartnerPosition((byte)partnerBean.getLogoPrtTilePos());
			
			if(!partnerBean.getAffiliateIdName().equals("") || !partnerBean.getAffiliateId().equals("")){
				afpartner.setAffiliateId(partnerBean.getAffiliateIdName()+"="+partnerBean.getAffiliateId());
			}else{
				afpartner.setAffiliateId("");
			}
			if(!partnerBean.getSourceOfInfoName().equals("") || !partnerBean.getSourceOfInfo().equals("")){
				afpartner.setSourceOfInfo(partnerBean.getSourceOfInfoName()+"="+partnerBean.getSourceOfInfo());
			}else{
				afpartner.setSourceOfInfo("");
			}
			if(!partnerBean.getParamFourName().equals("") || !partnerBean.getParamFour().equals("")){
				afpartner.setParamFour(partnerBean.getParamFourName()+"="+partnerBean.getParamFour());
			}else{
				afpartner.setParamFour("");
			}
			afpartner.setReferenceToken(partnerBean.getReferenceTokenName()+"="+partnerBean.getReferenceToken());
			
			afpartner.setJpmileDesc(partnerBean.getJpmileDesc());
			afpartner.setPartnerDesc(partnerBean.getPartnerDesc());
			afpartner.setJpmileReconPeriod(Integer.parseInt(partnerBean.getJpmileReconPeriod()));
			afpartner.setActivityId(partnerBean.getActivityId());
			afpartner.setAggregateId(partnerBean.getAggregateId());

			logger.info("PartnerName>>"+partnerBean.getPartnerName().toString()+">>AffiliateID>>"+partnerBean.getAffiliateIdName().toString()+"="+partnerBean.getAffiliateId().toString()
					+">>ReferenceToken>>"+partnerBean.getReferenceTokenName().toString()+"="+partnerBean.getReferenceToken().toString()
					+">>Sourceofinfo>>"+partnerBean.getSourceOfInfoName().toString()+"="+partnerBean.getSourceOfInfo().toString()
					+">>paramFour>>"+partnerBean.getParamFourName().toString()+"="+partnerBean.getParamFour().toString());
		status=partnermanagementDAO.createPartner(afpartner);
		System.out.println("status inside service impl---->"+status);
	
		if(status.equalsIgnoreCase(AfConstants.SUCCESS)){
			String ipAddress = request.getHeader("X-FORWARDED-FOR");
			if (ipAddress == null) {
				ipAddress = request.getRemoteAddr();
			}
			ipAddress = (null != ipAddress) ? ipAddress.trim() : "";			
			
			String prtLogStatus = "";
		    String oldOrNewVal = "";
		    String stCode = "";
		    String disPop = "";
		    String allJpNum = "";
		    
		    stCode = (partnerBean.getPartnerStatusCode()== 1)?"Yes":"No";
		    disPop = (partnerBean.getDisplayPrtPopup() != null)?"Yes":"No";
		    allJpNum = (partnerBean.getAllowJpnumber() != null)?"Yes":"No";
		    
			AfUserActivityLog afPrtActLog=new AfUserActivityLog();
				
			if(partnerBean.getPartnerType().equals("logoPartner")){
				
				oldOrNewVal = "Activation Date : "+activationDate+" | "+"Deactivation Date : "+deactivationDate+" | "+
						"Partner Id : "+partnerBean.getPartnerId()+" | "+"Partner Name : "+partnerBean.getPartnerName()+" | "+
						"Rate Per Mile : "+partnerBean.getRatePerMile()+" | "+"Partner Logo Url : "+partnerBean.getPartnerImageUrl().replaceAll(",","")+" | "+
						"PartnerIconUrl : "+partnerBean.getPartnerIconUrl().replaceAll(",","")+" | "+"PartnerPopupImageUrl : "+partnerBean.getPartnerPopupImageUrl().replaceAll(",","")+" | "+"Partner Landing Url : "+partnerBean.getPartnerLandingUrl()+" | "+
						"Terms And Condition : "+partnerBean.getTermsAndCondition()+" | "+"Display Partner Popup : "+disPop+" | "+
						"Allow Jp Number : "+allJpNum+" | "+"Partner Type : "+partnerBean.getPartnerType()+" | "+
						"Status Code : "+stCode+" | "+"Created Time : "+new Date()+" | "+"Affiliate Id and Name : "+partnerBean.getAffiliateIdName()+" "+partnerBean.getAffiliateId()+" | "+
						"Source of Info and its Name : "+partnerBean.getSourceOfInfoName()+" "+partnerBean.getSourceOfInfo()+" | "+
						"Param Four Name : "+partnerBean.getParamFourName()+" "+partnerBean.getParamFour()+" | "+
						"Reference Token : "+partnerBean.getReferenceTokenName()+" "+partnerBean.getParamFour()+" | "+
						"Logo Partner Position on Search Page : "+(byte)partnerBean.getLogoPrtDisplayOrder()+" | "+"Logo Partner Search Url : "+partnerBean.getLogoPartnerSearchUrl()+" | "+ 
						"JP Miles Description : "+partnerBean.getJpmileDesc()+" | "+"Partner Description : "+partnerBean.getPartnerDesc()+" | "+
						"JP Miles Recon Period : "+(partnerBean.getJpmileReconPeriod())+" | "+"Partner Activity Id : "+partnerBean.getActivityId()+".";
				
			}
			
			if(partnerBean.getPartnerType().equals("offLinePartner")){
				
				oldOrNewVal = "Activation Date : "+activationDate+" | "+"Deactivation Date : "+deactivationDate+" | "+
						"Partner Id : "+partnerBean.getPartnerId()+" | "+"Partner Name : "+partnerBean.getPartnerName()+" | "+
						"Partner Logo Url : "+partnerBean.getPartnerImageUrl().replaceAll(",","")+" | "+
						"PartnerIconUrl : "+partnerBean.getPartnerIconUrl().replaceAll(",","")+" | "+"PartnerPopupImageUrl : "+partnerBean.getPartnerPopupImageUrl().replaceAll(",","")+" | "+"Partner Landing Url : "+partnerBean.getPartnerLandingUrl()+" | "+
						"Terms And Condition : "+partnerBean.getTermsAndCondition()+" | "+"Display Partner Popup : "+disPop+" | "+
						"Allow Jp Number : "+allJpNum+" | "+"Partner Type : "+partnerBean.getPartnerType()+" | "+
						"Status Code : "+stCode+" | "+"Created Time : "+new Date()+" | "+"Affiliate Id and Name : "+partnerBean.getAffiliateIdName()+" "+partnerBean.getAffiliateId()+" | "+
						"Source of Info and its Name : "+partnerBean.getSourceOfInfoName()+" "+partnerBean.getSourceOfInfo()+" | "+
						"Param Four Name : "+partnerBean.getParamFourName()+" "+partnerBean.getParamFour()+" | "+
						"Reference Token : "+partnerBean.getReferenceTokenName()+" "+partnerBean.getParamFour()+" | "+
						"Offline Partner Position On Search Page : "+(byte)partnerBean.getLogoPrtDisplayOrder()+" | "+"OffLine Partner Search Url : "+partnerBean.getLogoPartnerSearchUrl()+" | "+ 
						"JP Miles Description : "+partnerBean.getJpmileDesc()+" | "+"Partner Description : "+partnerBean.getPartnerDesc()+" | "+
						"Partner Activity Id : "+partnerBean.getActivityId()+".";
			}
			
			if(partnerBean.getPartnerType().equals("readPartner") || partnerBean.getPartnerType().equals("talkPartner") || partnerBean.getPartnerType().equals("searchPartner") || partnerBean.getPartnerType().equals("hostPartner")){
				
				oldOrNewVal = "Activation Date : "+activationDate+" | "+"Deactivation Date : "+deactivationDate+" | "+
						"Partner Id : "+partnerBean.getPartnerId()+" | "+"Partner Name : "+partnerBean.getPartnerName()+" | "+
						"Partner Logo Url : "+partnerBean.getPartnerImageUrl().replaceAll(",","")+" | "+
						"Rate Per Mile : "+partnerBean.getRatePerMile()+" | "+
						"PartnerIconUrl : "+partnerBean.getPartnerIconUrl().replaceAll(",","")+" | "+"PartnerPopupImageUrl : "+partnerBean.getPartnerPopupImageUrl().replaceAll(",","")+" | "+"Partner Landing Url : "+partnerBean.getPartnerLandingUrl()+" | "+
						"Terms And Condition : "+partnerBean.getTermsAndCondition()+" | "+"Display Partner Popup : "+disPop+" | "+
						"Allow Jp Number : "+allJpNum+" | "+"Partner Type : "+partnerBean.getPartnerType()+" | "+
						"Status Code : "+stCode+" | "+"Created Time : "+new Date()+" | "+"Affiliate Id and Name : "+partnerBean.getAffiliateIdName()+" "+partnerBean.getAffiliateId()+" | "+
						"Source of Info and its Name : "+partnerBean.getSourceOfInfoName()+" "+partnerBean.getSourceOfInfo()+" | "+
						"Param Four Name : "+partnerBean.getParamFourName()+" "+partnerBean.getParamFour()+" | "+
						"Reference Token : "+partnerBean.getReferenceTokenName()+" "+partnerBean.getParamFour()+" | "+ 
						"JP Miles Description : "+partnerBean.getJpmileDesc()+" | "+"Partner Description : "+partnerBean.getPartnerDesc()+" | "+
						"JP Miles Recon Period : "+(partnerBean.getJpmileReconPeriod())+" | "+"Partner Activity Id : "+partnerBean.getActivityId()+".";
			}
			afPrtActLog.setUserNewFieldValue(oldOrNewVal);
			afPrtActLog.setUserOldFieldValue("NA");
			afPrtActLog.setUserActivity("Created Partner");
			afPrtActLog.setUserActCreatedDateTime(new Date());
			afPrtActLog.setUserActIpVal(ipAddress);
			afPrtActLog.setUserActModule("Partner Management");
			afPrtActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
			
			prtLogStatus = userActLogDao.createUserActLog(afPrtActLog);
			logger.info("PartrnerManagementServiceImpl createPartner() prtLogStatus ",prtLogStatus);
			System.out.println("PartrnerManagementServiceImpl createPartner() prtLogStatus "+prtLogStatus);
		}
		
		}catch(Exception e){
			logger.error("@@@@ Exception in PartrnerManagementServiceImpl createPartner() "+e);
		}
		return status;
		
	}

	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String updatePartner(JSONObject partnerJson, HttpServletRequest request) {
		
		Date dt=null;
		DateFormat sourceDf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
	    
	String replaceInOld="";
	String replaceInNew="";
		String status="failure",activationDate="",deactivationDate="",partnerId="";
		AfPartner afpartner = null;
		AfPartner afPartnerOld = null; 
	    String dateStr = "";
	    String deAct = "";
		
		List<AfPartner> afpartnerlist = null;
		try{
				
			System.out.println("inside update partner management service impl");
			afpartner=partnermanagementDAO.getPartnerByPartnerNO(partnerJson.getString("partnerNo"));
			afpartner.setAfPartnerProducts(new HashSet<AfPartnerProduct>(0));

			afPartnerOld = (AfPartner) afpartner.clone();
			
			partnerId=afpartner.getPartnerId();
			if(partnerId!=partnerJson.getString("partnerId"))
				afpartner.setPartnerId(partnerJson.getString("partnerId"));
			afpartner.setPartnerName(partnerJson.getString("partnerName"));	
			if(partnerJson.getString("ratePermile") != null && !partnerJson.getString("ratePermile").equals("")){
			afpartner.setRatePerMile(new Float(partnerJson.getString("ratePermile")));
			}else{
				afpartner.setRatePerMile(0f);
			}
			dateStr=ServiceProviderUtil.getInstance().getFormattedDate(afpartner.getActivationDate(),"dd/MM/yyyy");
			deAct = ServiceProviderUtil.getInstance().getFormattedDate(afpartner.getDeactivationDate(),"dd/MM/yyyy");
			afpartner.setPartnerPosition((byte)Integer.parseInt(partnerJson.getString("logoPartnerDispOrd")));
			activationDate=partnerJson.getString("partnerActivationDate");
			
			if(activationDate.equals(dateStr)){
				afpartner.setActivationDate(afpartner.getActivationDate());
			}
			else{
			
			if(activationDate.length()>0 && activationDate!=null){
				Date dt1=new Date();
				String hh_mi_ss=""+dt1;
				String format[]=hh_mi_ss.split(" ");
				hh_mi_ss=format[3];
				activationDate=activationDate+" "+hh_mi_ss;
				dt=sourceDf.parse(activationDate);
				afpartner.setActivationDate(dt);
			}
			}
			
			
			if(!partnerJson.getString("talkreadsearchurl").equals("") || partnerJson.getString("talkreadsearchurl")!=null){
				afpartner.setLogoPartnerSearchUrl(partnerJson.getString("talkreadsearchurl"));
			}
			
			if(!partnerJson.getString("talktilepos").equals("") || partnerJson.getString("talktilepos")!=null){
				afpartner.setLogoPartnerPosition(new Byte(partnerJson.getString("talktilepos")));
			}
			
			
			
			
			deactivationDate=partnerJson.getString("partnerDeactivationDate");	
			if(deactivationDate.equals(deAct)){
				afpartner.setDeactivationDate(afpartner.getDeactivationDate());
			}
			else{
			if(deactivationDate.length()>0 && deactivationDate!=null){
				Date dt1=new Date();
				String hh_mi_ss=""+dt1;
				String format[]=hh_mi_ss.split(" ");
				hh_mi_ss=format[3];
				deactivationDate=deactivationDate+" "+hh_mi_ss;
				dt=sourceDf.parse(deactivationDate);
				afpartner.setDeactivationDate(dt);
			}
			else{
				afpartner.setDeactivationDate(null);
			}
			}
			afpartner.setHostPartner((byte) partnerJson.getInt("hostPartner"));
			afpartner.setDefaultPartner((byte) partnerJson.getInt("defaultPartner"));
			afpartner.setPartnerLogoUrl(partnerJson.getString("partnerImageUrl"));
			afpartner.setPartnerStatus((byte) partnerJson.getInt("partnerStatus"));
			
			afpartner.setPartnerPopupLogoUrl(partnerJson.getString("partnerPopupImageUrl"));
			System.out.println("partner popup update logo url---------->"+afpartner.getPartnerPopupLogoUrl());
			
			afpartner.setModifiedBy(1);
			afpartner.setModifiedTime(new Date());
			
			afpartner.setPartnerLandingUrl(partnerJson.getString("partnerLandingUrl"));
			afpartner.setPartnerIconUrl(partnerJson.getString("partnerIconUrl"));
			
			if(!partnerJson.getString("affiliateIdName").equals("") || !partnerJson.getString("affiliateId").equals("")){
				afpartner.setAffiliateId(partnerJson.getString("affiliateIdName")+"="+partnerJson.getString("affiliateId"));
			}else if(partnerJson.getString("affiliateIdName").equals("") && partnerJson.getString("affiliateId").equals("")){
				afpartner.setAffiliateId("");
				}else{
				afpartner.setAffiliateId("");
			     }
			if(!partnerJson.getString("sourceOfInfoName").equals("") || !partnerJson.getString("sourceOfInfo").equals("")){
				afpartner.setSourceOfInfo(partnerJson.getString("sourceOfInfoName")+"="+partnerJson.getString("sourceOfInfo"));
				}else if(partnerJson.getString("sourceOfInfoName").equals("") && partnerJson.getString("sourceOfInfo").equals("")){
				afpartner.setSourceOfInfo("");
				}
				else{
				afpartner.setSourceOfInfo("");
			}
			if(!partnerJson.getString("paramFourName").equals("") || !partnerJson.getString("paramFour").equals("")){
				afpartner.setParamFour(partnerJson.getString("paramFourName")+"="+partnerJson.getString("paramFour"));
			}else if(partnerJson.getString("paramFourName").equals("") && partnerJson.getString("paramFour").equals("")){
				afpartner.setParamFour("");
			}else{
				afpartner.setParamFour("");
			}
			
			if(!partnerJson.getString("referenceTokenName").equals("") || !partnerJson.getString("referenceToken").equals("")){
			   afpartner.setReferenceToken(partnerJson.getString("referenceTokenName")+"="+partnerJson.getString("referenceToken"));
			}else if(partnerJson.getString("referenceTokenName").equals("") && partnerJson.getString("referenceToken").equals("")){
				afpartner.setReferenceToken("");
			}else{
				afpartner.setReferenceToken("");
			}
			
			
			
			if(!partnerJson.getString("jpmileDesc").trim().isEmpty()){
				afpartner.setJpmileDesc(partnerJson.getString("jpmileDesc"));
			}
			
			
			
			afpartner.setPartnerDesc(partnerJson.getString("partnerDesc"));
			afpartner.setJpmileReconPeriod(Integer.parseInt(partnerJson.getString("jpmileReconPeriod")));
			afpartner.setActivityId(partnerJson.getString("activityId"));
			
			if(!partnerJson.getString("termsAndCondition").trim().isEmpty()){
				afpartner.setTermsAndCondition(partnerJson.getString("termsAndCondition"));
			}
			afpartner.setDisplayPrtPopup(Byte.parseByte(partnerJson.getString("displayPrtPopup")));
			afpartner.setAllowJpnumber(Byte.parseByte(partnerJson.getString("allowJpnumber")));
			afpartner.setAggregateId(partnerJson.getString("aggregateId"));
			status=partnermanagementDAO.updatePartner(afpartner);
			System.out.println("Status update ---->"+status);
					
		    if(status.equalsIgnoreCase(AfConstants.SUCCESS)){
		    	String ipAddress = request.getHeader("X-FORWARDED-FOR");
				if (ipAddress == null) {
					ipAddress = request.getRemoteAddr();
				}
				ipAddress = (null != ipAddress) ? ipAddress.trim() : "";
		    	
		    	AfUserActivityLog updateUsActLog = new AfUserActivityLog();
		    	Map<String,String> objectCom=new LinkedHashMap<>();
		    		
		    		updateUsActLog.setUserActivity("Update Partner");
		    		updateUsActLog.setUserActCreatedDateTime(new Date());
		    		updateUsActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
		    		updateUsActLog.setUserActIpVal(ipAddress);
		    		updateUsActLog.setUserActModule("Partner Management : "+partnerJson.getString("partnerName"));
		    		objectCom = adminUserActivityUtil.objectComparision(afPartnerOld, afpartner);

		    		replaceInOld=objectCom.get(AfConstants.ACTIVITY_OLD_KEY).toString().replace(":0 |",":No |").replace(":1 |", ":Yes |");
		    		replaceInNew=objectCom.get(AfConstants.ACTIVITY_NEW_KEY).toString().replace(":0 |",":No |").replace(":1 |", ":Yes |");
		    		updateUsActLog.setUserNewFieldValue(replaceInNew);
		    		updateUsActLog.setUserOldFieldValue(replaceInOld);
		    		userActLogDao.updateUserLog(updateUsActLog);
		    		
		    }
			
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in PartnerManagementServiceImpl getPartnerByPartnerNO() :"+e);
		}
		
		return status;
	}
	
	
	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String updatePartnerCategoryMap(JSONObject userMapJson, HttpServletRequest request) {
		AfPartner afpartner = null;
		List<AfPartner> afpartnerlist = null;
		String status = "failure";
		try {
			afpartner=partnermanagementDAO.getPartnerByPartnerNO(userMapJson.getString("mapPrtNo"));
			if(userMapJson.getString("mapPrtNo") != null && !userMapJson.getString("prtCatMapText").equals("")){
				afpartner.setTextCatMapping(userMapJson.getString("prtCatMapText"));
				status=partnermanagementDAO.updatePartner(afpartner);
			}
		} catch (JSONException e) {
			logger.error("@@@@ Exception in PartnerManagementServiceImpl updatePartnerCategoryMap() :"+e);

		}

		return status;
	}
	
	
	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String getPartnerMappedCategoriesByPrtNo(String prtNameCatMapping) {
		String textMapped = "";
		AfPartner afpartner = null;
		try{
		afpartner = partnermanagementDAO.getPartnerByPartnerNO(prtNameCatMapping);
		if(afpartner.getTextCatMapping()!=null && !afpartner.getTextCatMapping().equals(null)){
		textMapped = afpartner.getTextCatMapping();
		}
	
		}catch(Exception e){
		logger.error("@@@@ Exception in PartnerManagementServiceImpl getPartnerMappedCategoriesByPrtNo() :"+e);    
		}
		return textMapped;
	}

	
	
	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String updateLogoPartner(JSONObject partnerJson, HttpServletRequest request) {
		Date dt=null;
		DateFormat sourceDf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
	
	
		String status="failure";
		String activationDate="";
		String deactivationDate="";
		String partnerId="";
		String actD="";
		String dactD="";
		String replaceInOld="";
		String replaceInNew="";

		AfPartner afpartner = null;
		AfPartner afPartnerOld = null;
		List<AfPartner> afpartnerlist = null;
		try{
				if(partnerJson.getInt("defaultPartner")==1){
				afpartnerlist = partnermanagementDAO.getPartnerByDefault();
				if(afpartnerlist.size()>0){
				afpartner=afpartnerlist.get(0);
				afpartner.setDefaultPartner((byte)0);				
				status=partnermanagementDAO.updatePartner(afpartner);
				}
			}
						
			afpartner=partnermanagementDAO.getPartnerByPartnerNO(partnerJson.getString("partnerNo"));
			afPartnerOld = (AfPartner) afpartner.clone();
			
			partnerId=afpartner.getPartnerId();
			if(partnerId!=partnerJson.getString("partnerId"))
				afpartner.setPartnerId(partnerJson.getString("partnerId"));
			afpartner.setPartnerName(partnerJson.getString("partnerName"));			
			afpartner.setRatePerMile(new Float(partnerJson.getString("ratePermile")));
			activationDate=partnerJson.getString("partnerActivationDate");	
			actD = ServiceProviderUtil.getInstance().getFormattedDate(afpartner.getActivationDate(),"dd/MM/yyyy");
			dactD = ServiceProviderUtil.getInstance().getFormattedDate(afpartner.getDeactivationDate(),"dd/MM/yyyy");
			
			if(activationDate.equals(actD)){
				afpartner.setActivationDate(afpartner.getActivationDate());
			}
			else{
			if(activationDate.length()>0 && activationDate!=null){
				Date dt1=new Date();
				String hh_mi_ss=""+dt1;
				String format[]=hh_mi_ss.split(" ");
				hh_mi_ss=format[3];
				activationDate=activationDate+" "+hh_mi_ss;
				dt=sourceDf.parse(activationDate);
				afpartner.setActivationDate(dt);
			}
			}
			
			deactivationDate=partnerJson.getString("partnerDeactivationDate");
			
			if(deactivationDate.equals(dactD)){
				afpartner.setDeactivationDate(afpartner.getDeactivationDate());
			}
			else{
			if(deactivationDate.length()>0 && deactivationDate!=null){
				Date dt1=new Date();
				String hh_mi_ss=""+dt1;
				String format[]=hh_mi_ss.split(" ");
				hh_mi_ss=format[3];
				deactivationDate=deactivationDate+" "+hh_mi_ss;
				dt=sourceDf.parse(deactivationDate);
				afpartner.setDeactivationDate(dt);
			}
			else{
				afpartner.setDeactivationDate(null);
			}
			}
			afpartner.setHostPartner((byte) partnerJson.getInt("hostPartner"));
			afpartner.setDefaultPartner((byte) partnerJson.getInt("defaultPartner"));
			afpartner.setLogoPartner((byte) partnerJson.getInt("logoPartner"));

            afpartner.setTalkPartner((byte)partnerJson.getInt("talkPartner"));
            afpartner.setReadPartner((byte)partnerJson.getInt("readPartner"));
			
			afpartner.setPartnerLogoUrl(partnerJson.getString("partnerImageUrl"));
			afpartner.setPartnerPosition((byte)Integer.parseInt(partnerJson.getString("logoPartnerDispOrd")));
			afpartner.setLogoPartnerPosition((byte)Integer.parseInt(partnerJson.getString("logoPartnerTilePos")));
						
			afpartner.setPartnerStatus((byte) partnerJson.getInt("partnerStatus"));
			
			afpartner.setModifiedBy(1);
			afpartner.setModifiedTime(new Date());
			
			afpartner.setPartnerLandingUrl(partnerJson.getString("partnerLandingUrl"));
			afpartner.setPartnerIconUrl(partnerJson.getString("partnerIconUrl"));
			afpartner.setPartnerPopupLogoUrl(partnerJson.getString("partnerPopupImageUrl"));
			afpartner.setLogoPartnerSearchUrl(partnerJson.getString("logoPartnersearchUrl"));
			if(!partnerJson.getString("affiliateIdName").equals("") || !partnerJson.getString("affiliateId").equals("")){
				afpartner.setAffiliateId(partnerJson.getString("affiliateIdName")+"="+partnerJson.getString("affiliateId"));
			}else if(partnerJson.getString("affiliateIdName").equals("") && partnerJson.getString("affiliateId").equals("")){
				afpartner.setAffiliateId("");
			}else{
				afpartner.setAffiliateId("");
			}
			if(!partnerJson.getString("sourceOfInfoName").equals("") || !partnerJson.getString("sourceOfInfo").equals("")){
				afpartner.setSourceOfInfo(partnerJson.getString("sourceOfInfoName")+"="+partnerJson.getString("sourceOfInfo"));
			}else if(partnerJson.getString("sourceOfInfoName").equals("") && partnerJson.getString("sourceOfInfo").equals("")){
				afpartner.setSourceOfInfo("");
			}else{
				afpartner.setSourceOfInfo("");
			}
			if(!partnerJson.getString("paramFourName").equals("") || !partnerJson.getString("paramFour").equals("")){
				afpartner.setParamFour(partnerJson.getString("paramFourName")+"="+partnerJson.getString("paramFour"));
			}else if(partnerJson.getString("paramFourName").equals("") && partnerJson.getString("paramFour").equals("")){
				afpartner.setParamFour("");
			}else{
				afpartner.setParamFour("");
			}
			
			if(!partnerJson.getString("referenceTokenName").equals("") || !partnerJson.getString("referenceToken").equals("")){
				afpartner.setReferenceToken(partnerJson.getString("referenceTokenName")+"="+partnerJson.getString("referenceToken"));
			}else if(partnerJson.getString("referenceTokenName").equals("") && partnerJson.getString("referenceToken").equals("")){
				afpartner.setReferenceToken("");
			}else{
				afpartner.setReferenceToken("");
			}
			 
			afpartner.setJpmileDesc(partnerJson.getString("jpmileDesc"));	
			afpartner.setPartnerDesc(partnerJson.getString("partnerDesc"));
			afpartner.setJpmileReconPeriod(Integer.parseInt(partnerJson.getString("jpmileReconPeriod")));
			afpartner.setActivityId(partnerJson.getString("activityId"));

			if(!partnerJson.getString("termsAndCondition").trim().isEmpty()){
				afpartner.setTermsAndCondition(partnerJson.getString("termsAndCondition"));
			}
			afpartner.setDisplayPrtPopup(Byte.parseByte(partnerJson.getString("displayPrtPopup")));
			afpartner.setAllowJpnumber(Byte.parseByte(partnerJson.getString("allowJpnumber")));
			afpartner.setAggregateId(partnerJson.getString("aggregateId"));
			status=partnermanagementDAO.updatePartner(afpartner);
			 
			if (status.equalsIgnoreCase("success")) {
				String ipAddress = request.getHeader("X-FORWARDED-FOR");
				if (ipAddress == null) {
					ipAddress = request.getRemoteAddr();
				}
			 
			
			AfUserActivityLog updateUsActLog = new AfUserActivityLog();
			Map<String,String> objectCom=new LinkedHashMap<>();
			
			
				updateUsActLog.setUserActivity("Update Logo Partner");
	    		updateUsActLog.setUserActCreatedDateTime(new Date());
	    		updateUsActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
	    		updateUsActLog.setUserActIpVal(ipAddress);
	    		updateUsActLog.setUserActModule("Partner Management : "+partnerJson.getString("partnerName"));
	    			
	    		objectCom = adminUserActivityUtil.objectComparision(afPartnerOld, afpartner);
	    		replaceInOld=objectCom.get(AfConstants.ACTIVITY_OLD_KEY).toString().replace(":0 |",":No |").replace(":1 |", ":Yes |");
	    		replaceInNew=objectCom.get(AfConstants.ACTIVITY_NEW_KEY).toString().replace(":0 |",":No |").replace(":1 |", ":Yes |");
	    		updateUsActLog.setUserNewFieldValue(replaceInNew);
	    		updateUsActLog.setUserOldFieldValue(replaceInOld);
	    		userActLogDao.updateUserLog(updateUsActLog);
	    		
		}
		}
		catch(Exception e){
			logger.error("@@@@ Exception in PartnerManagementServiceImpl getPartnerByPartnerNO() :"+e);
		}
		
		return status;
	}

	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String saveOrder(String reOrder, String prtType,String curPage, String pageLen, HttpServletRequest request) {
		StringTokenizer reOrderTkn = null, reOrderTkn_ = null ;
		Integer prtNo = -1;
		int partnerPosition=-1, part=0;
		String status="", temp = "";
		String replaceInOld="";
		String replaceInNew="";
		List<AfPartner> afpartnerlist = null;
		AfPartner afPartnerOld = null;
		List<Integer> partnerlist = new ArrayList<Integer>();
		List<Integer> prtDisOrdlist = new ArrayList<Integer>();
		int partdisOrd=0;
		try{
			
			afpartnerlist = partnermanagementDAO.getPartnersByLimit(prtType, curPage, pageLen);
			System.out.println("size of limit --> "+afpartnerlist.size());
			for(AfPartner afPartner : afpartnerlist){
				partnerPosition =afPartner.getPartnerPosition();	
				System.out.println("partner position ----> "+partnerPosition);
				prtDisOrdlist.add(partnerPosition);
				}
			
			reOrderTkn = new StringTokenizer(reOrder,",");
			while(reOrderTkn.hasMoreTokens()){				
				reOrderTkn_ = new StringTokenizer(reOrderTkn.nextToken(),",");
				prtNo = (int)Integer.parseInt(reOrderTkn_.nextToken());
				partnerlist.add(prtNo);
			}		
			partdisOrd=0;
			
			if(prtDisOrdlist.size() >= partnerlist.size()){
		
				for(int i=0, j=0;i<=prtDisOrdlist.size()&&j<=partnerlist.size();i++,j++){
				partdisOrd = prtDisOrdlist.get(i);
				AfPartner afPartner=partnermanagementDAO.getPartnerById(partnerlist.get(j));								
				afPartnerOld =(AfPartner) afPartner.clone();
				afPartner.setPartnerPosition((byte) partdisOrd);
				afPartner.setModifiedBy(1);
				afPartner.setModifiedTime(new Date());
				
				status=partnermanagementDAO.updatePartner(afPartner);
				
				
			if (status.equalsIgnoreCase("success")) {
				
				String ipAddress = request.getHeader("X-FORWARDED-FOR");
				if (ipAddress == null) {
					ipAddress = request.getRemoteAddr();
				}
			 
			
			AfUserActivityLog updateUsActLog = new AfUserActivityLog();
			
			
				updateUsActLog.setUserActivity("Update Display order");
	    		updateUsActLog.setUserActCreatedDateTime(new Date());
	    		updateUsActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
	    		updateUsActLog.setUserActIpVal(ipAddress);
	    		updateUsActLog.setUserActModule("Partner Management : "+afPartner.getPartnerName());
	    			

	    		updateUsActLog.setUserNewFieldValue("Display Order : "+afPartner.getPartnerPosition().toString());
	    		updateUsActLog.setUserOldFieldValue("Display Order : "+afPartnerOld.getPartnerPosition().toString());
	    		if(afPartner.getPartnerPosition() != afPartnerOld.getPartnerPosition())
	    		userActLogDao.updateUserLog(updateUsActLog);
	    		
			}
			}
			}
			
			
				
				for(int i=0, j=0;i<=prtDisOrdlist.size()&&j<=partnerlist.size();i++,j++){
					partdisOrd = prtDisOrdlist.get(i);
					AfPartner afPartner=partnermanagementDAO.getPartnerById(partnerlist.get(j));								
					afPartnerOld =(AfPartner) afPartner.clone();
					afPartner.setPartnerPosition((byte) partdisOrd);
					afPartner.setModifiedBy(1);
					afPartner.setModifiedTime(new Date());
					
					status=partnermanagementDAO.updatePartner(afPartner);
					
					
				if (status.equalsIgnoreCase("success")) {
					
					String ipAddress = request.getHeader("X-FORWARDED-FOR");
					if (ipAddress == null) {
						ipAddress = request.getRemoteAddr();
					}
				 
				
				AfUserActivityLog updateUsActLog = new AfUserActivityLog();
				
				
					updateUsActLog.setUserActivity("Update Display order");
		    		updateUsActLog.setUserActCreatedDateTime(new Date());
		    		updateUsActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
		    		updateUsActLog.setUserActIpVal(ipAddress);
		    		updateUsActLog.setUserActModule("Partner Management : "+afPartner.getPartnerName());
		    			

		    		updateUsActLog.setUserNewFieldValue("Display Order : "+afPartner.getPartnerPosition().toString());
		    		updateUsActLog.setUserOldFieldValue("Display Order : "+afPartnerOld.getPartnerPosition().toString());
		    		if(afPartner.getPartnerPosition() != afPartnerOld.getPartnerPosition())
		    		userActLogDao.updateUserLog(updateUsActLog);		    		
				}
				}			
		}
		catch(Exception e){
			logger.error("@@@@ Exception in UserManagementServiceImpl updateAdminUser() :"+e);
		}
		
		return status;
	}


	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String deletePartner(Integer prtNo, HttpServletRequest request) {
		String deleteStatus="";
		AfPartner prtDelUsActLog = null;
		String oldOrNewVal = "";
		String stCode = "";
	    String disPop = "";
	    String allJpNum = "";
	    String hostPartner="";
	    String talkPartner="";
	    String readPartner="";
	    String defaultPartner="";
	    String offlinePartner="";
	    String logoPartner="";
	    
		try{
			

			String ipAddress = request.getHeader("X-FORWARDED-FOR");
			if (ipAddress == null) {
				ipAddress = request.getRemoteAddr();
			}
			prtDelUsActLog = partnermanagementDAO.getPartnerById(prtNo);
			stCode = prtDelUsActLog.getStatus() == 1 ? "Yes" : "No";
			disPop = prtDelUsActLog.getDisplayPrtPopup() == 1 ? "Yes" : "No";
			allJpNum = prtDelUsActLog.getAllowJpnumber() == 1 ? "Yes" : "No";
			hostPartner=(prtDelUsActLog.getHostPartner()==null || prtDelUsActLog.getHostPartner()==0)?"No":"Yes";
			
			talkPartner=prtDelUsActLog.getTalkPartner()==1 ? "Yes":"No";
			readPartner=prtDelUsActLog.getReadPartner()==1 ? "Yes":"No";
			defaultPartner=prtDelUsActLog.getDefaultPartner()==1 ? "Yes":"No";
			offlinePartner=prtDelUsActLog.getOffLinePartner()==1 ? "Yes":"No";
			logoPartner=prtDelUsActLog.getLogoPartner()==1 ? "Yes":"No";

			prtDelUsActLog = partnermanagementDAO.getPartnerById(prtNo);
	    	AfUserActivityLog updateUsActLog = new AfUserActivityLog();
	    	
	    	if(prtDelUsActLog.getTalkPartner()==1 || prtDelUsActLog.getReadPartner()==1 || hostPartner.equalsIgnoreCase("Yes") || prtDelUsActLog.getDefaultPartner()==1 ){
	    		
	    		oldOrNewVal = "Activation Date : "+prtDelUsActLog.getActivationDate()+" | "+"Deactivation Date : "+prtDelUsActLog.getDeactivationDate()+" | "+
		    			"Partner Id : "+prtDelUsActLog.getPartnerId()+" | "+"Partner Name : "+prtDelUsActLog.getPartnerName()+" | "+
	    				"Host Partner : "+hostPartner+" | "+"Search Partner : "+defaultPartner+" | "+"Logo Partner : "+logoPartner+" | "+"Offline Partner : "+offlinePartner+" | "+"Talk Partner : "+talkPartner+" | "+"Read Partner : "+readPartner+" | "+ 
	    				"Rate Per Mile : "+prtDelUsActLog.getRatePerMile()+" | "+
		    			"Partner Logo Url : "+prtDelUsActLog.getPartnerLogoUrl()+" | "+"Partner Icon Url : "+prtDelUsActLog.getPartnerIconUrl()+" | "+"Partner Landing Url : "+prtDelUsActLog.getPartnerLandingUrl()+" | "+
		    			"Terms And Condition  : "+prtDelUsActLog.getTermsAndCondition()+" | "+"Display Partner Popup : "+disPop+" | "+
		    			"Allow Jp Number : "+allJpNum+" | "+"Status Code : "+stCode+" | "+"Created Time : "+prtDelUsActLog.getCreatedTime()+" | "+
		    			"Affiliate Id and Name : "+prtDelUsActLog.getAffiliateId()+" | "+"Source of Info : "+prtDelUsActLog.getSourceOfInfo()+" | "+
		    			"Param Four Name : "+prtDelUsActLog.getParamFour()+" | "+"Reference Token : "+prtDelUsActLog.getReferenceToken()+" | "+
		    			"Jp Miles Description : "+prtDelUsActLog.getJpmileDesc()+" | "+"Partner Description : "+prtDelUsActLog.getPartnerDesc()+" | "+
		    			"Jp Miles Recon Period : "+prtDelUsActLog.getJpmileReconPeriod()+" | "+"Partner Activity Id : "+prtDelUsActLog.getActivityId()+"Partner Position : "+prtDelUsActLog.getPartnerPosition()+".";
	    		
	    	}
	    	
	    	
	    	if(prtDelUsActLog.getLogoPartner()==1){	    		
	    		oldOrNewVal = "Activation Date : "+prtDelUsActLog.getActivationDate()+" | "+"Deactivation Date : "+prtDelUsActLog.getDeactivationDate()+" | "+
		    			"Partner Id : "+prtDelUsActLog.getPartnerId()+" | "+"Partner Name : "+prtDelUsActLog.getPartnerName()+" | "+"Rate Per mile : "+prtDelUsActLog.getRatePerMile()+" | "+
	    				"Host Partner : "+hostPartner+" | "+"Search Partner : "+defaultPartner+" | "+"Logo Partner : "+logoPartner+" | "+"Offline Partner : "+offlinePartner+" | "+"Talk Partner : "+talkPartner+" | "+"Read Partner : "+readPartner+" | "+ 
		    			"Partner Logo Url : "+prtDelUsActLog.getPartnerLogoUrl()+" | "+"Partner Icon Url : "+prtDelUsActLog.getPartnerIconUrl()+" | "+"Partner Landing Url : "+prtDelUsActLog.getPartnerLandingUrl()+" | "+
		    			"Terms And Condition  : "+prtDelUsActLog.getTermsAndCondition()+" | "+"Display Partner Popup : "+disPop+" | "+
		    			"Allow Jp Number : "+allJpNum+" | "+"Status Code : "+stCode+" | "+"Created Time : "+prtDelUsActLog.getCreatedTime()+" | "+
		    			"Affiliate Id and Name : "+prtDelUsActLog.getAffiliateId()+" | "+"Source of Info : "+prtDelUsActLog.getSourceOfInfo()+" | "+
		    			"Logo Partner Position on Search Page : "+prtDelUsActLog.getLogoPartnerTilePosition()+" | "+"Logo Partner Search Url : "+prtDelUsActLog.getLogoPartnerSearchUrl()+" | "+
		    			"Param Four Name : "+prtDelUsActLog.getParamFour()+" | "+"Reference Token : "+prtDelUsActLog.getReferenceToken()+" | "+
		    			"Jp Miles Description : "+prtDelUsActLog.getJpmileDesc()+" | "+"Partner Description : "+prtDelUsActLog.getPartnerDesc()+" | "+
		    			"Jp Miles Recon Period : "+prtDelUsActLog.getJpmileReconPeriod()+" | "+"Partner Activity Id : "+prtDelUsActLog.getActivityId()+" | "+" Partner Position : "+prtDelUsActLog.getPartnerPosition()+".";
	    		
	    	}
	    	
	    	if(prtDelUsActLog.getOffLinePartner()==1){	    		
	    		oldOrNewVal = "Activation Date : "+prtDelUsActLog.getActivationDate()+" | "+"Deactivation Date : "+prtDelUsActLog.getDeactivationDate()+" | "+
		    			"Partner Id : "+prtDelUsActLog.getPartnerId()+" | "+"Partner Name : "+prtDelUsActLog.getPartnerName()+" | "+
	    				"Host Partner : "+hostPartner+" | "+"Search Partner : "+defaultPartner+" | "+"Logo Partner : "+logoPartner+" | "+"Offline Partner : "+offlinePartner+" | "+"Talk Partner : "+talkPartner+" | "+"Read Partner : "+readPartner+" | "+ 
		    			"Partner Logo Url : "+prtDelUsActLog.getPartnerLogoUrl()+" | "+"Partner Icon Url : "+prtDelUsActLog.getPartnerIconUrl()+" | "+"Partner Landing Url : "+prtDelUsActLog.getPartnerLandingUrl()+" | "+
		    			"Terms And Condition  : "+prtDelUsActLog.getTermsAndCondition()+" | "+"Display Partner Popup : "+disPop+" | "+
		    			"Allow Jp Number : "+allJpNum+" | "+"Status Code : "+stCode+" | "+"Created Time : "+prtDelUsActLog.getCreatedTime()+" | "+
		    			"Affiliate Id and Name : "+prtDelUsActLog.getAffiliateId()+" | "+"Source of Info : "+prtDelUsActLog.getSourceOfInfo()+" | "+
		    			"Offline Partner Position On Search Page : "+prtDelUsActLog.getLogoPartnerTilePosition()+" | "+"Logo Partner Search Url : "+prtDelUsActLog.getLogoPartnerSearchUrl()+" | "+
		    			"Param Four Name : "+prtDelUsActLog.getParamFour()+" | "+"Reference Token : "+prtDelUsActLog.getReferenceToken()+" | "+
		    			"Jp Miles Description : "+prtDelUsActLog.getJpmileDesc()+" | "+"Partner Description : "+prtDelUsActLog.getPartnerDesc()+" | "+
		    			"Partner Activity Id : "+prtDelUsActLog.getActivityId()+"Partner Position : "+prtDelUsActLog.getPartnerPosition()+".";
	    		
	    	}
	    	
	    	updateUsActLog.setUserActivity("Deleted Partner");
	    	updateUsActLog.setUserActCreatedDateTime(new Date());
	    	updateUsActLog.setUserNewFieldValue("NA");
	    	updateUsActLog.setUserActIpVal(ipAddress);
	    	updateUsActLog.setUserActModule("Partner Management");
	    	updateUsActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
	    	updateUsActLog.setUserOldFieldValue(oldOrNewVal);
			
	    	userActLogDao.updateUserLog(updateUsActLog);
	    	deleteStatus=partnermanagementDAO.deletePartner(prtNo);
			
		}catch(Exception e){
			logger.error("@@@@ Exception in PartnerManagementServiceImpl deletePartner() :"+e);
		}
		return deleteStatus;
	}

	@Override
	public boolean checkPartnerID(String partnerID) {
		boolean prtnerIdExist=false;
		try{
			prtnerIdExist=partnermanagementDAO.checkPartnerID(partnerID);
			logger.debug(">>>>>>>>checkPartnerID service>>>"+prtnerIdExist);
		}catch(Exception e){
			logger.error("@@Exception in PartnerManagementServiceImpl checkPartnerID", e);
		}
		return prtnerIdExist;
	}

	@Override
	public boolean checkPartnerName(String partnerName) {
		boolean prtnerNameExist=false;
		AfPartner afpartner;
		try{
			
			afpartner=partnermanagementDAO.getPartnerByPartnerName(partnerName);
			
			if(afpartner!=null)
			{
				String prtName=afpartner.getPartnerName();
				if(prtName.equalsIgnoreCase(partnerName)){
				prtnerNameExist=true;
				}
			}
				logger.debug(">>>>>>>>checkPartnerName service>>>"+prtnerNameExist);
		}catch(Exception e){
			logger.error("@@Exception in PartnerManagementServiceImpl checkPartnerName", e);
		}
		return prtnerNameExist;
	}


	@Override
	public boolean checkLogoPrtDisplayOrder(String prtPos, String prtType) {
		boolean checkFlag=false;
		List<AfPartner> afPartners;
		try{
			afPartners=partnermanagementDAO.checkLogoPrtDisplayOrder(prtPos, prtType);
			if(afPartners!=null && !afPartners.isEmpty()){
				checkFlag=true;
			}
		}catch(Exception e){
			logger.error("@@Exception in PartnerManagementServiceImpl checkLogoPrtDisplayOrder():", e);
		}
		return checkFlag;
	}


	@Override
	public boolean checkLogoPrtTilePosition(String prtPos, String prtType) {
		boolean checkFlag=false;
		List<AfPartner> afPartners;
		try{
			afPartners=partnermanagementDAO.checkLogoPrtTilePosition(prtPos, prtType);
			if(afPartners!=null && !afPartners.isEmpty()){
				checkFlag=true;
			}
		}catch(Exception e){
			
			logger.error("@@Exception in PartnerManagementServiceImpl checkLogoPrtTilePosition():", e);
		}
		return checkFlag;
	}
	
	
	public List<PartnerBean> getLogoPartners() {
		List<AfPartner> afPartnerList=null;
		List<PartnerBean> partnerList=new ArrayList<>();
		int partnerStatusCode;
		String dateStr="";
	
		Date dt=null;
		
		try{
			afPartnerList=partnermanagementDAO.getLogoPartners();
		for(AfPartner afPartner:afPartnerList){
			PartnerBean partnerBean=new PartnerBean();
			partnerBean.setPrtNo(afPartner.getPrtNo());
			partnerBean.setPartnerId(afPartner.getPartnerId());
			partnerBean.setPartnerName(afPartner.getPartnerName()!=null?afPartner.getPartnerName():"");			
			partnerBean.setRatePerMile(afPartner.getRatePerMile()!=null?afPartner.getRatePerMile():0);
			partnerBean.setHostPartnerCode(afPartner.getHostPartner()!=null?afPartner.getHostPartner():0);
			partnerBean.setDefaultPartnerCode(afPartner.getDefaultPartner());
			partnerBean.setLogoPartnerCode(afPartner.getLogoPartner());
			dt=afPartner.getActivationDate()!=null?afPartner.getActivationDate():new Date();
			dateStr=ServiceProviderUtil.getInstance().getFormattedDate(afPartner.getActivationDate(),"dd/MM/yyyy");
			if(dt!=null && !dt.equals(""))
			partnerBean.setActivationDate(dateStr);
			dt=afPartner.getDeactivationDate()!=null?afPartner.getDeactivationDate():new Date();
			dateStr=ServiceProviderUtil.getInstance().getFormattedDate(afPartner.getDeactivationDate(),"dd/MM/yyyy");
			if(dt!=null && !dt.equals(""))
			partnerBean.setDeactivationDate(dateStr);
			partnerBean.setPartnerStatusCode(afPartner.getPartnerStatus());
			if(afPartner.getPartnerLogoUrl()!=null)
			partnerBean.setPartnerImageUrl(afPartner.getPartnerLogoUrl());
			partnerStatusCode=afPartner.getPartnerStatus();
			
			if(1==partnerStatusCode){
				partnerBean.setPartnerStatus(AfConstants.STATUS_ACTIVE);
				partnerBean.setPartnerStatusClass(AfConstants.STATUS_CLASS_GREEN);
				
			}
			if(0==partnerStatusCode){
				partnerBean.setPartnerStatus(AfConstants.STATUS_IN_ACTIVE);
				partnerBean.setPartnerStatusClass(AfConstants.STATUS_CLASS_RED);
				
			}
			partnerBean.setLogoPartnerTilePosition(null!=afPartner.getLogoPartnerTilePosition()?afPartner.getLogoPartnerTilePosition():0);
			partnerBean.setLogoPartnerDispLimit(null!=afPartner.getLogoPartnerDispLimit()?afPartner.getLogoPartnerDispLimit():0);
			partnerList.add(partnerBean);
		}
		}catch(Exception e){
			logger.error("@@@@ Exception in PartnerManagementServiceImpl getPartners() :"+e);
		}
		return partnerList;
	}

	@Override
	public List<PartnerBean> getOfflinePartner() {
    
    List<AfPartner> afPartnerList=null;
	List<PartnerBean> partnerList=new ArrayList<>();
	int partnerStatusCode;
	String dateStr="";
	Date dt=null;
	
	try {
		afPartnerList = partnermanagementDAO.getOfflinePartner();
		for(AfPartner afPartner : afPartnerList){
			PartnerBean partnerBean=new PartnerBean();
			partnerBean.setPrtNo(afPartner.getPrtNo());
			partnerBean.setPartnerId(afPartner.getPartnerId());
			partnerBean.setPartnerName(afPartner.getPartnerName()!=null?afPartner.getPartnerName():"");			
			partnerBean.setRatePerMile(afPartner.getRatePerMile()!=null?afPartner.getRatePerMile():0);
			partnerBean.setHostPartnerCode(afPartner.getHostPartner()!=null?afPartner.getHostPartner():0);
			partnerBean.setDefaultPartnerCode(afPartner.getDefaultPartner());
			partnerBean.setLogoPartnerCode(afPartner.getLogoPartner());
			dt=afPartner.getActivationDate()!=null?afPartner.getActivationDate():new Date();
			dateStr=ServiceProviderUtil.getInstance().getFormattedDate(afPartner.getActivationDate(),"dd/MM/yyyy");
			if(dt!=null && !dt.equals(""))
			partnerBean.setActivationDate(dateStr);
			dt=afPartner.getDeactivationDate()!=null?afPartner.getDeactivationDate():new Date();
			dateStr=ServiceProviderUtil.getInstance().getFormattedDate(afPartner.getDeactivationDate(),"dd/MM/yyyy");
			if(dt!=null && !dt.equals(""))
			partnerBean.setDeactivationDate(dateStr);
			partnerBean.setPartnerStatusCode(afPartner.getPartnerStatus());
			if(afPartner.getPartnerLogoUrl()!=null)
			partnerBean.setPartnerImageUrl(afPartner.getPartnerLogoUrl());
			partnerStatusCode=afPartner.getPartnerStatus();
			
			if(1==partnerStatusCode){
				partnerBean.setPartnerStatus(AfConstants.STATUS_ACTIVE);
				partnerBean.setPartnerStatusClass(AfConstants.STATUS_CLASS_GREEN);
				
			}
			if(0==partnerStatusCode){
				partnerBean.setPartnerStatus(AfConstants.STATUS_IN_ACTIVE);
				partnerBean.setPartnerStatusClass(AfConstants.STATUS_CLASS_RED);
				
			}
			partnerBean.setLogoPartnerTilePosition(null!=afPartner.getLogoPartnerTilePosition()?afPartner.getLogoPartnerTilePosition():0);
			partnerBean.setLogoPartnerDispLimit(null!=afPartner.getLogoPartnerDispLimit()?afPartner.getLogoPartnerDispLimit():0);
			partnerList.add(partnerBean);
		}
	} catch (Exception e) {
		logger.error("@@@@ Exception in PartnerManagementServiceImpl getOfflinePartner() :"+e);
	}
		return partnerList;
	}
	
	@Override
	public List<PartnerBean> getHostPartner() {
	
	List<AfPartner> afPartnerList=null;
	List<PartnerBean> partnerList=new ArrayList<>();
	int partnerStatusCode;
	String dateStr="";
	Date dt=null;
	
	try {
		afPartnerList = partnermanagementDAO.getHostPartner();
		for(AfPartner afPartner : afPartnerList){
			PartnerBean partnerBean=new PartnerBean();
			partnerBean.setPrtNo(afPartner.getPrtNo());
			partnerBean.setPartnerId(afPartner.getPartnerId());
			partnerBean.setPartnerName(afPartner.getPartnerName()!=null?afPartner.getPartnerName():"");			
			partnerBean.setRatePerMile(afPartner.getRatePerMile()!=null?afPartner.getRatePerMile():0);
			partnerBean.setHostPartnerCode(afPartner.getHostPartner()!=null?afPartner.getHostPartner():0);
			partnerBean.setDefaultPartnerCode(afPartner.getDefaultPartner());
			partnerBean.setLogoPartnerCode(afPartner.getLogoPartner());
			dt=afPartner.getActivationDate()!=null?afPartner.getActivationDate():new Date();
			dateStr=ServiceProviderUtil.getInstance().getFormattedDate(afPartner.getActivationDate(),"dd/MM/yyyy");
			if(dt!=null && !dt.equals(""))
			partnerBean.setActivationDate(dateStr);
			dt=afPartner.getDeactivationDate()!=null?afPartner.getDeactivationDate():new Date();
			dateStr=ServiceProviderUtil.getInstance().getFormattedDate(afPartner.getDeactivationDate(),"dd/MM/yyyy");
			if(dt!=null && !dt.equals(""))
			partnerBean.setDeactivationDate(dateStr);
			partnerBean.setPartnerStatusCode(afPartner.getPartnerStatus());
			if(afPartner.getPartnerLogoUrl()!=null)
			partnerBean.setPartnerImageUrl(afPartner.getPartnerLogoUrl());
			partnerStatusCode=afPartner.getPartnerStatus();
			
			if(1==partnerStatusCode){
				partnerBean.setPartnerStatus(AfConstants.STATUS_ACTIVE);
				partnerBean.setPartnerStatusClass(AfConstants.STATUS_CLASS_GREEN);
				
			}
			if(0==partnerStatusCode){
				partnerBean.setPartnerStatus(AfConstants.STATUS_IN_ACTIVE);
				partnerBean.setPartnerStatusClass(AfConstants.STATUS_CLASS_RED);
				
			}
			partnerBean.setLogoPartnerTilePosition(null!=afPartner.getLogoPartnerTilePosition()?afPartner.getLogoPartnerTilePosition():0);
			partnerBean.setLogoPartnerDispLimit(null!=afPartner.getLogoPartnerDispLimit()?afPartner.getLogoPartnerDispLimit():0);
			partnerList.add(partnerBean);
		}
	} catch (Exception e) {
		logger.error("@@@@ Exception in PartnerManagementServiceImpl getOfflinePartner() :"+e);
	}
		return partnerList;
	}
	
	
	@Override
	public List<PartnerBean> getTalkPartner() {
    List<AfPartner> afPartnerList=null;
    List<PartnerBean> partnerList=new ArrayList<>();
    int partnerStatusCode;
    String dateStr="";
    Date dt=null;

try {
	afPartnerList = partnermanagementDAO.getTalkPartner();
	for(AfPartner afPartner : afPartnerList){
		PartnerBean partnerBean=new PartnerBean();
		partnerBean.setPrtNo(afPartner.getPrtNo());
		partnerBean.setPartnerId(afPartner.getPartnerId());
		partnerBean.setPartnerName(afPartner.getPartnerName()!=null?afPartner.getPartnerName():"");			
		partnerBean.setRatePerMile(afPartner.getRatePerMile()!=null?afPartner.getRatePerMile():0);
		partnerBean.setHostPartnerCode(afPartner.getHostPartner()!=null?afPartner.getHostPartner():0);
		partnerBean.setDefaultPartnerCode(afPartner.getDefaultPartner());
		partnerBean.setLogoPartnerCode(afPartner.getLogoPartner());
		dt=afPartner.getActivationDate()!=null?afPartner.getActivationDate():new Date();
		dateStr=ServiceProviderUtil.getInstance().getFormattedDate(afPartner.getActivationDate(),"dd/MM/yyyy");
		if(dt!=null && !dt.equals(""))
		partnerBean.setActivationDate(dateStr);
		dt=afPartner.getDeactivationDate()!=null?afPartner.getDeactivationDate():new Date();
		dateStr=ServiceProviderUtil.getInstance().getFormattedDate(afPartner.getDeactivationDate(),"dd/MM/yyyy");
		if(dt!=null && !dt.equals(""))
		partnerBean.setDeactivationDate(dateStr);
		partnerBean.setPartnerStatusCode(afPartner.getPartnerStatus());
		if(afPartner.getPartnerLogoUrl()!=null)
		partnerBean.setPartnerImageUrl(afPartner.getPartnerLogoUrl());
		partnerStatusCode=afPartner.getPartnerStatus();
		
		if(1==partnerStatusCode){
			partnerBean.setPartnerStatus(AfConstants.STATUS_ACTIVE);
			partnerBean.setPartnerStatusClass(AfConstants.STATUS_CLASS_GREEN);
			
		}
		if(0==partnerStatusCode){
			partnerBean.setPartnerStatus(AfConstants.STATUS_IN_ACTIVE);
			partnerBean.setPartnerStatusClass(AfConstants.STATUS_CLASS_RED);
			
		}
		partnerBean.setLogoPartnerTilePosition(null!=afPartner.getLogoPartnerTilePosition()?afPartner.getLogoPartnerTilePosition():0);
		partnerBean.setLogoPartnerDispLimit(null!=afPartner.getLogoPartnerDispLimit()?afPartner.getLogoPartnerDispLimit():0);
		partnerList.add(partnerBean);
	}
} catch (Exception e) {
	logger.error("@@@@ Exception in PartnerManagementServiceImpl getOfflinePartner() :"+e);
}

return partnerList;
	}

	
	@Override
	public List<PartnerBean> getReadPartner() {
		List<AfPartner> afPartnerList=null;
	    List<PartnerBean> partnerList=new ArrayList<>();
	    int partnerStatusCode;
	    String dateStr="";
	    Date dt=null;

	try {
		afPartnerList = partnermanagementDAO.getReadPartner();
		for(AfPartner afPartner : afPartnerList){
			PartnerBean partnerBean=new PartnerBean();
			partnerBean.setPrtNo(afPartner.getPrtNo());
			partnerBean.setPartnerId(afPartner.getPartnerId());
			partnerBean.setPartnerName(afPartner.getPartnerName()!=null?afPartner.getPartnerName():"");			
			partnerBean.setRatePerMile(afPartner.getRatePerMile()!=null?afPartner.getRatePerMile():0);
			partnerBean.setHostPartnerCode(afPartner.getHostPartner()!=null?afPartner.getHostPartner():0);
			partnerBean.setDefaultPartnerCode(afPartner.getDefaultPartner());
			partnerBean.setLogoPartnerCode(afPartner.getLogoPartner());
			dt=afPartner.getActivationDate()!=null?afPartner.getActivationDate():new Date();
			dateStr=ServiceProviderUtil.getInstance().getFormattedDate(afPartner.getActivationDate(),"dd/MM/yyyy");
			if(dt!=null && !dt.equals(""))
			partnerBean.setActivationDate(dateStr);
			dt=afPartner.getDeactivationDate()!=null?afPartner.getDeactivationDate():new Date();
			dateStr=ServiceProviderUtil.getInstance().getFormattedDate(afPartner.getDeactivationDate(),"dd/MM/yyyy");
			if(dt!=null && !dt.equals(""))
			partnerBean.setDeactivationDate(dateStr);
			partnerBean.setPartnerStatusCode(afPartner.getPartnerStatus());
			if(afPartner.getPartnerLogoUrl()!=null)
			partnerBean.setPartnerImageUrl(afPartner.getPartnerLogoUrl());
			partnerStatusCode=afPartner.getPartnerStatus();
			
			if(1==partnerStatusCode){
				partnerBean.setPartnerStatus(AfConstants.STATUS_ACTIVE);
				partnerBean.setPartnerStatusClass(AfConstants.STATUS_CLASS_GREEN);
				
			}
			if(0==partnerStatusCode){
				partnerBean.setPartnerStatus(AfConstants.STATUS_IN_ACTIVE);
				partnerBean.setPartnerStatusClass(AfConstants.STATUS_CLASS_RED);
				
			}
			partnerBean.setLogoPartnerTilePosition(null!=afPartner.getLogoPartnerTilePosition()?afPartner.getLogoPartnerTilePosition():0);
			partnerBean.setLogoPartnerDispLimit(null!=afPartner.getLogoPartnerDispLimit()?afPartner.getLogoPartnerDispLimit():0);
			partnerList.add(partnerBean);
		}
	} catch (Exception e) {
		logger.error("@@@@ Exception in PartnerManagementServiceImpl getOfflinePartner() :"+e);
	}

		
		return partnerList;
	}

	
	
	
	@Override
	public List<KeyWordBean> getKeywordFileName(String logoprtNo) {
		List<AfPartnerKeyword> afPartnerKeywords=null;
		List<KeyWordBean> keywordLists=new ArrayList<>();
		Date dt=null;
		String dateStr="";
		try{
			afPartnerKeywords=partnermanagementDAO.getKeywordFileName(logoprtNo);
			AfPartnerKeyword afPartnerKeyword=afPartnerKeywords.get(0);
			KeyWordBean keywordBean=new KeyWordBean();
			keywordBean.setKeyWordNo(afPartnerKeyword.getPrtKeywordNo());
			dt=afPartnerKeyword.getActivationDate()!=null?afPartnerKeyword.getActivationDate():new Date();
			dateStr=ServiceProviderUtil.getInstance().getFormattedDate(afPartnerKeyword.getActivationDate(),"dd/MM/yyyy");
			if(dt!=null && !dt.equals(""))
			keywordBean.setActivationDate(dateStr);
			keywordBean.setFilePath(afPartnerKeyword.getFilePath());
			String filepath[]=afPartnerKeyword.getFilePath().split("/");
			String filename="";
			for (int i = filepath.length; i >0 ; i--) {
				filename=filepath[i-1];
				break;
			}
			
			keywordBean.setFileName(filename);
			keywordLists.add(keywordBean);
		
		}catch(Exception e){
			logger.error("@@@@ Exception in PartnerManagementServiceImpl getPartners() :"+e);
		}
		return keywordLists;
		
	}


	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
		public String saveLogoPartnerConfig(String tileNo, HttpServletRequest request){
		String status="failure";
		String logoPartConfigPos = "";

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}
		
		try{
			logoPartConfigPos = partnermanagementDAO.getLgoPrtTileSearchresult();
			
			
			status = partnermanagementDAO.saveLogoPartnerConfig(tileNo);
			
			if(status.equalsIgnoreCase("success")){
				
				AfUserActivityLog updateUsActLog = new AfUserActivityLog();
				
				updateUsActLog.setUserActivity("Configuration of Logo Partner Update");
		    	updateUsActLog.setUserActCreatedDateTime(new Date());
		    	updateUsActLog.setUserNewFieldValue("Logo Partner Tile Position : "+tileNo);
		    	updateUsActLog.setUserActIpVal(ipAddress);
		    	updateUsActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
				updateUsActLog.setUserOldFieldValue("Logo Partner Tile Position : "+logoPartConfigPos);
				updateUsActLog.setUserActModule("Partner Management");
				userActLogDao.updateUserLog(updateUsActLog);
			}
		}
		catch(Exception ex){
			logger.error("@@@@ Exception in PartnerManagementServiceImpl saveLogoPartnerConfig() :"+ex);
		}
		
		return status;
	}
	
	public List<PartnerBean> getDefaultPartners() {
		List<AfPartner> afPartnerList = null;
		List<PartnerBean> partnerList = new ArrayList<PartnerBean>();
		int partnerStatusCode;
		String dateStr = "";
		Date dt = null;

		try {
			afPartnerList = partnermanagementDAO.getPartnerByDefault();
			for(AfPartner afPartner : afPartnerList){
				PartnerBean partnerBean = new PartnerBean();
				partnerBean.setPrtNo(afPartner.getPrtNo());
				partnerBean.setPartnerId(afPartner.getPartnerId());
				partnerBean.setPartnerName(afPartner.getPartnerName() != null ? afPartner.getPartnerName() : "");			
				partnerBean.setRatePerMile(afPartner.getRatePerMile() != null ? afPartner.getRatePerMile() : 0);
				partnerBean.setHostPartnerCode(afPartner.getHostPartner() != null ? afPartner.getHostPartner() : 0);
				partnerBean.setDefaultPartnerCode(afPartner.getDefaultPartner());
				dt = afPartner.getActivationDate() != null ? afPartner.getActivationDate() : new Date();
				dateStr = ServiceProviderUtil.getInstance().getFormattedDate(afPartner.getActivationDate(),"dd/MM/yyyy");
				if(dt != null && !dt.equals(""))
					partnerBean.setActivationDate(dateStr);
				dt = afPartner.getDeactivationDate() != null ? afPartner.getDeactivationDate() : new Date();
				dateStr = ServiceProviderUtil.getInstance().getFormattedDate(afPartner.getDeactivationDate(),"dd/MM/yyyy");
				if(dt != null && !dt.equals(""))
					partnerBean.setDeactivationDate(dateStr);
				partnerBean.setPartnerStatusCode(afPartner.getPartnerStatus());
				if(afPartner.getPartnerLogoUrl() != null)
					partnerBean.setPartnerImageUrl(afPartner.getPartnerLogoUrl());
				partnerStatusCode = afPartner.getPartnerStatus();

				if(1 == partnerStatusCode) {
					partnerBean.setPartnerStatus(AfConstants.STATUS_ACTIVE);
					partnerBean.setPartnerStatusClass(AfConstants.STATUS_CLASS_GREEN);
				}
				if(0 == partnerStatusCode) {
					partnerBean.setPartnerStatus(AfConstants.STATUS_IN_ACTIVE);
					partnerBean.setPartnerStatusClass(AfConstants.STATUS_CLASS_RED);
				}
				partnerList.add(partnerBean);
			}
		} catch(Exception e) {
			logger.error("@@@@ Exception in PartnerManagementServiceImpl getPartners() :" + e);
		}
		return partnerList;
	}
	
	
	public List<PartnerBean> getDefaultAndHostPartners() {
		List<AfPartner> afPartnerList = null;
		List<PartnerBean> partnerList = new ArrayList<PartnerBean>();
		int partnerStatusCode;
		String dateStr = "";
		Date dt = null;
		
		try {
			afPartnerList = partnermanagementDAO.getDefaultAndHostPartners();
			for(AfPartner afPartner : afPartnerList){
				PartnerBean partnerBean = new PartnerBean();
				partnerBean.setPrtNo(afPartner.getPrtNo());
				partnerBean.setPartnerId(afPartner.getPartnerId());
				partnerBean.setPartnerName(afPartner.getPartnerName() != null ? afPartner.getPartnerName() : "");			
				partnerBean.setRatePerMile(afPartner.getRatePerMile() != null ? afPartner.getRatePerMile() : 0);
				partnerBean.setHostPartnerCode(afPartner.getHostPartner() != null ? afPartner.getHostPartner() : 0);
				partnerBean.setDefaultPartnerCode(afPartner.getDefaultPartner());
				dt = afPartner.getActivationDate() != null ? afPartner.getActivationDate() : new Date();
				dateStr = ServiceProviderUtil.getInstance().getFormattedDate(afPartner.getActivationDate(),"dd/MM/yyyy");
				if(dt != null && !dt.equals(""))
					partnerBean.setActivationDate(dateStr);
				dt = afPartner.getDeactivationDate() != null ? afPartner.getDeactivationDate() : new Date();
				dateStr = ServiceProviderUtil.getInstance().getFormattedDate(afPartner.getDeactivationDate(),"dd/MM/yyyy");
				if(dt != null && !dt.equals(""))
					partnerBean.setDeactivationDate(dateStr);
				partnerBean.setPartnerStatusCode(afPartner.getPartnerStatus());
				if(afPartner.getPartnerLogoUrl() != null)
					partnerBean.setPartnerImageUrl(afPartner.getPartnerLogoUrl());
				partnerStatusCode = afPartner.getPartnerStatus();
				
				if(1 == partnerStatusCode) {
					partnerBean.setPartnerStatus(AfConstants.STATUS_ACTIVE);
					partnerBean.setPartnerStatusClass(AfConstants.STATUS_CLASS_GREEN);
				}
				if(0 == partnerStatusCode) {
					partnerBean.setPartnerStatus(AfConstants.STATUS_IN_ACTIVE);
					partnerBean.setPartnerStatusClass(AfConstants.STATUS_CLASS_RED);
				}
				partnerList.add(partnerBean);
			}
		} catch(Exception e) {
			logger.error("@@@@ Exception in PartnerManagementServiceImpl getPartners() :" + e);
		}
		return partnerList;
	}
	
	public List<PartnerBean> getAllPartners() {
		List<AfPartner> afPartnerList = null;
		List<PartnerBean> partnerList = new ArrayList<PartnerBean>();
		int partnerStatusCode;
		String dateStr = "";
		Date dt = null;

		try {
			afPartnerList = partnermanagementDAO.getAllPartners();
			for(AfPartner afPartner : afPartnerList){
				PartnerBean partnerBean = new PartnerBean();
				partnerBean.setPrtNo(afPartner.getPrtNo());
				partnerBean.setPartnerId(afPartner.getPartnerId());
				partnerBean.setPartnerName(afPartner.getPartnerName() != null ? afPartner.getPartnerName() : "");
				partnerBean.setRatePerMile(afPartner.getRatePerMile() != null ? afPartner.getRatePerMile() : 0);
				partnerBean.setHostPartnerCode(afPartner.getHostPartner() != null ? afPartner.getHostPartner() : 0);
				partnerBean.setDefaultPartnerCode(afPartner.getDefaultPartner());
				dt = afPartner.getActivationDate() != null ? afPartner.getActivationDate() : new Date();
				dateStr = ServiceProviderUtil.getInstance().getFormattedDate(afPartner.getActivationDate(),"dd/MM/yyyy");
				if(dt != null && !dt.equals(""))
					partnerBean.setActivationDate(dateStr);
				dt = afPartner.getDeactivationDate() != null ? afPartner.getDeactivationDate() : new Date();
				dateStr = ServiceProviderUtil.getInstance().getFormattedDate(afPartner.getDeactivationDate(),"dd/MM/yyyy");
				if(dt != null && !dt.equals(""))
					partnerBean.setDeactivationDate(dateStr);
				partnerBean.setPartnerStatusCode(afPartner.getPartnerStatus());
				if(afPartner.getPartnerLogoUrl() != null)
					partnerBean.setPartnerImageUrl(afPartner.getPartnerLogoUrl());
				partnerStatusCode = afPartner.getPartnerStatus();

				if(1 == partnerStatusCode) {
					partnerBean.setPartnerStatus(AfConstants.STATUS_ACTIVE);
					partnerBean.setPartnerStatusClass(AfConstants.STATUS_CLASS_GREEN);
				}
				if(0 == partnerStatusCode) {
					partnerBean.setPartnerStatus(AfConstants.STATUS_IN_ACTIVE);
					partnerBean.setPartnerStatusClass(AfConstants.STATUS_CLASS_RED);
				}
				//URLEncoder.encode(tAndC, "UTF-8");
				/*String point1, point2, point3, point4, point5;
				point1 = afPartner.getPoint1()!=null ?afPartner.getPoint1() : "";
				partnerBean.setPoint1(URLEncoder.encode(point1, "UTF-8"));
				
				point2 = afPartner.getPoint1()!=null ?afPartner.getPoint2() : "";
				partnerBean.setPoint2(URLEncoder.encode(point2, "UTF-8"));
				
				point3 = afPartner.getPoint3()!=null ?afPartner.getPoint3() : "";
				partnerBean.setPoint3(URLEncoder.encode(point3, "UTF-8"));
				
				point4 = afPartner.getPoint4()!=null ?afPartner.getPoint4() : "";
				partnerBean.setPoint4(URLEncoder.encode(point4, "UTF-8"));
				
				point5 = afPartner.getPoint5()!=null ?afPartner.getPoint5() : "";
				partnerBean.setPoint5(URLEncoder.encode(point5, "UTF-8"));*/
				
				partnerBean.setPoint1(afPartner.getPoint1()!=null ?afPartner.getPoint1() : "");
				partnerBean.setPoint2(afPartner.getPoint2()!=null ?afPartner.getPoint2() : "");
				partnerBean.setPoint3(afPartner.getPoint3()!=null ?afPartner.getPoint3() : "");
				partnerBean.setPoint4(afPartner.getPoint4()!=null ?afPartner.getPoint4() : "");
				partnerBean.setPoint5(afPartner.getPoint5()!=null ?afPartner.getPoint5() : "");
		       
				
				partnerList.add(partnerBean);
			}
		} catch(Exception e) {
			logger.error("@@@@ Exception in PartnerManagementServiceImpl getAllPartners() :",e);
		}
		return partnerList;
	}

	@Override
	public String getLgoPrtTileSearchresult() {
		String tileNo="";

		try{
			tileNo = partnermanagementDAO.getLgoPrtTileSearchresult();
		}
		catch(Exception ex){
			logger.error("@@@@ Exception in PartnerManagementServiceImpl getLgoPrtTileSearchresult() :",ex);
		}
		
		return tileNo;
	}

	@Override
	public AfPartner getPartnerByName(String partnerName) {
		AfPartner afPartner = null;
		try {
			afPartner = partnermanagementDAO.getPartnerByName(partnerName);
		} catch (Exception ex) {
			logger.error("@@@@ Exception in PartnerManagementServiceImpl getPartnerByName() :"+ex);
		}
		return afPartner;
	}
	
	@Override
	public Map<String, AfPartner> getPartnerMap() {

		List<AfPartner> afPartnerList = null;
		Map<String, AfPartner> afPartnerMap  = new HashMap<String, AfPartner>();
		try {
			afPartnerList = partnermanagementDAO.getAllPartners();
			for(AfPartner afPartner : afPartnerList) {
				afPartnerMap.put(afPartner.getPartnerName().toLowerCase().trim(), afPartner);
			}
		} catch(Exception e) {
			logger.error("@@@@ Exception in PartnerManagementServiceImpl at getPartnerMap() :", e);
		}
		return afPartnerMap;
	}

	@Override
	public boolean checkActivityID(String activityID) {
		boolean activityIdExist=false;
		try{
			activityIdExist=partnermanagementDAO.checkActivityID(activityID);
			logger.debug(">>>>>>>>checkActivityID service>>>"+activityIdExist);
		}catch(Exception e){
			logger.error("@@Exception in PartnerManagementServiceImpl checkActivityID", e);
		}
		return activityIdExist;
	}
	
	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String savePopupContentByPartner(PartnerBean partnerBean,HttpServletRequest request){
		String status = "failure";
		AfPartner afPartner = null;
		AfPartner afPartnerOld = null;
		String replaceInOld="";
		String replaceInNew="";
		try{
			afPartner =partnermanagementDAO.getPartnerByPartnerNO(partnerBean.getPrtNo()+"");
			afPartnerOld=(AfPartner) afPartner.clone();
			if(afPartner != null){
				
				
				afPartner.setPoint1(partnerBean.getPoint1());
				afPartner.setPoint2(partnerBean.getPoint2());
				afPartner.setPoint3(partnerBean.getPoint3());
				afPartner.setPoint4(partnerBean.getPoint4());
				afPartner.setPoint5(partnerBean.getPoint5());
				
				afPartner.setModifiedBy(1);
				afPartner.setModifiedTime(new Date());
				
				String getPoint1 = afPartner.getPoint1().length() !=0?afPartner.getPoint1():"NA";
				String getPoint1Old = afPartnerOld.getPoint1().length() !=0?afPartnerOld.getPoint1():"NA";
				
				String getPoint2 = afPartner.getPoint2().length() !=0?afPartner.getPoint2():"NA";
				String getPoint2Old = afPartnerOld.getPoint2().length() !=0?afPartnerOld.getPoint2():"NA";
				
				String getPoint3 = afPartner.getPoint3().length() !=0?afPartner.getPoint3():"NA";
				String getPoint3Old = afPartnerOld.getPoint3().length() !=0?afPartnerOld.getPoint3():"NA";
				
				String getPoint4 = afPartner.getPoint4().length() !=0?afPartner.getPoint4():"NA";
				String getPoint4Old = afPartnerOld.getPoint4().length() !=0?afPartnerOld.getPoint4():"NA";
				
				String getPoint5 = afPartner.getPoint5().length() !=0?afPartner.getPoint5():"NA";
				String getPoint5Old = afPartnerOld.getPoint5().length() !=0?afPartnerOld.getPoint5():"NA";
				status = partnermanagementDAO.updatePartner(afPartner);
				if("success".equalsIgnoreCase(status)){
					String ipAddress = request.getHeader("X-FORWARDED-FOR");
					if (ipAddress == null) {
						ipAddress = request.getRemoteAddr();
					}
					try{
						AfUserActivityLog updateUsActLog = new AfUserActivityLog();
						Map<String,String> objectCom = new LinkedHashMap<>();
						updateUsActLog.setUserActivity("Update Partner Popup Content :"+afPartner.getPartnerName());
						updateUsActLog.setUserActCreatedDateTime(new Date());
						updateUsActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
						updateUsActLog.setUserActIpVal(ipAddress);
						updateUsActLog.setUserActModule("Partner Popup Content");
						
						if(!getPoint1.equalsIgnoreCase(getPoint1Old)){
							replaceInOld=replaceInOld+"| You Must Know:"+getPoint1Old;
							replaceInNew=replaceInNew+"| You Must Know:"+getPoint1;
						}
						if(!getPoint2.equalsIgnoreCase(getPoint2Old)){
							replaceInOld=replaceInOld+"| Point 1:"+getPoint2Old;
							replaceInNew=replaceInNew+"| Point 1:"+getPoint2;
						}
						if(!getPoint3.equalsIgnoreCase(getPoint3Old)){
							replaceInOld=replaceInOld+"| Point 2:"+getPoint3Old;
							replaceInNew=replaceInNew+"| Point 2:"+getPoint3;
						}
						if(!getPoint4.equalsIgnoreCase(getPoint4Old)){
							replaceInOld=replaceInOld+"| Point 3:"+getPoint4Old;
							replaceInNew=replaceInNew+"| Point 3:"+getPoint4;
						}
						if(!getPoint5.equalsIgnoreCase(getPoint5Old)){
							replaceInOld=replaceInOld+"| Point 4:"+getPoint5Old;
							replaceInNew=replaceInNew+"| Point 4:"+getPoint5;
						}
						if(replaceInNew.isEmpty() && replaceInOld.isEmpty()) {
	                        replaceInOld=replaceInOld+"|ModifiedTime:"+simpleDateFormat.format(afPartnerOld.getModifiedTime());
	                        replaceInNew=replaceInNew+"|ModifiedTime:"+simpleDateFormat.format(afPartner.getModifiedTime());
	                    }
						
						updateUsActLog.setUserNewFieldValue(replaceInNew);
			    		updateUsActLog.setUserOldFieldValue(replaceInOld);
						userActLogDao.updateUserLog(updateUsActLog);
					}
					catch(Exception e){
						logger.error("@@Exception in Activity Log PartnerManagementServiceImpl savePopupContentByPartner() :", e);
					}
				}
			}	
		}
		catch(Exception e){
			logger.error("@@Exception in PartnerManagementServiceImpl savePopupContentByPartner() :", e);
		}
		return status;
	}
	
	public List<PartnerBean> getLogoPartners(String prtType) {
		List<AfPartner> afPartnerList=null;
		List<PartnerBean> partnerList=new ArrayList<PartnerBean>();
		int partnerStatusCode;
		String dateStr="";
	
		Date dt=null;
		
		try{
			afPartnerList=partnermanagementDAO.getLogoPartners(prtType);
		for(AfPartner afPartner:afPartnerList){
			PartnerBean partnerBean=new PartnerBean();
			partnerBean.setPrtNo(afPartner.getPrtNo());
			partnerBean.setPartnerId(afPartner.getPartnerId());
			partnerBean.setPartnerName(afPartner.getPartnerName()!=null?afPartner.getPartnerName():"");			
			partnerBean.setRatePerMile(afPartner.getRatePerMile()!=null?afPartner.getRatePerMile():0);
			partnerBean.setHostPartnerCode(afPartner.getHostPartner()!=null?afPartner.getHostPartner():0);
			partnerBean.setDefaultPartnerCode(afPartner.getDefaultPartner());
			partnerBean.setLogoPartnerCode(afPartner.getLogoPartner());
			dt=afPartner.getActivationDate()!=null?afPartner.getActivationDate():new Date();
			dateStr=ServiceProviderUtil.getInstance().getFormattedDate(afPartner.getActivationDate(),"dd/MM/yyyy");
			if(dt!=null && !dt.equals(""))
			partnerBean.setActivationDate(dateStr);
			dt=afPartner.getDeactivationDate()!=null?afPartner.getDeactivationDate():new Date();
			dateStr=ServiceProviderUtil.getInstance().getFormattedDate(afPartner.getDeactivationDate(),"dd/MM/yyyy");
			if(dt!=null && !dt.equals(""))
			partnerBean.setDeactivationDate(dateStr);
			partnerBean.setPartnerStatusCode(afPartner.getPartnerStatus());
			if(afPartner.getPartnerLogoUrl()!=null)
			partnerBean.setPartnerImageUrl(afPartner.getPartnerLogoUrl());
			partnerStatusCode=afPartner.getPartnerStatus();
			
			if(1==partnerStatusCode){
				partnerBean.setPartnerStatus(AfConstants.STATUS_ACTIVE);
				partnerBean.setPartnerStatusClass(AfConstants.STATUS_CLASS_GREEN);
				
			}
			if(0==partnerStatusCode){
				partnerBean.setPartnerStatus(AfConstants.STATUS_IN_ACTIVE);
				partnerBean.setPartnerStatusClass(AfConstants.STATUS_CLASS_RED);
				
			}
			partnerBean.setLogoPartnerTilePosition(null!=afPartner.getLogoPartnerTilePosition()?afPartner.getLogoPartnerTilePosition():0);
			partnerBean.setLogoPartnerDispLimit(null!=afPartner.getLogoPartnerDispLimit()?afPartner.getLogoPartnerDispLimit():0);
			partnerList.add(partnerBean);
		}
		}catch(Exception e){
			logger.error("@@@@ Exception in PartnerManagementServiceImpl getPartners() :", e);
		}
		return partnerList;
	}
	
	@Override
	public List<Map<String, String>> getAllPrtWebservice(){
		Map<String, String> afPartnerMap  = null;
		List<AfPartner> afPartnerList = null;
		List<Map<String, String>> afPartnerMapList = new ArrayList<>();
		
		try{
			afPartnerList = partnermanagementDAO.getAllPrtWebservice();
			
			if(afPartnerList != null){
				for(AfPartner afPartner : afPartnerList){
					afPartnerMap  = new HashMap<>();
					afPartnerMap.put("PRT_NO", CommonUtil.getStringValue(afPartner.getPrtNo()));
					afPartnerMap.put("PARTNER_ID", CommonUtil.getStringValue(afPartner.getPartnerId()));
					afPartnerMap.put("ACTIVITY_ID", CommonUtil.getStringValue(afPartner.getActivityId()));
					afPartnerMap.put("PARTNER_NAME", CommonUtil.getStringValue(afPartner.getPartnerName()));
					afPartnerMap.put("RATE_PER_MILE", CommonUtil.getStringValue(afPartner.getRatePerMile()));
					afPartnerMap.put("PARTNER_LOGO_URL", CommonUtil.getStringValue(afPartner.getPartnerLogoUrl()));
					afPartnerMap.put("PARTNER_ICON_URL", CommonUtil.getStringValue(afPartner.getPartnerIconUrl()));
					afPartnerMap.put("PARTNER_DESC", CommonUtil.getStringValue(afPartner.getPartnerDesc()));
					afPartnerMap.put("JPMILE_DESC", CommonUtil.getStringValue(afPartner.getJpmileDesc()));
					afPartnerMap.put("JPMILE_RECON_PERIOD", CommonUtil.getStringValue(afPartner.getJpmileReconPeriod()));
					afPartnerMap.put("AFFILIATE_ID", CommonUtil.getStringValue(afPartner.getAffiliateId()));
					afPartnerMap.put("SOURCE_OF_INFO", CommonUtil.getStringValue(afPartner.getSourceOfInfo()));
					afPartnerMap.put("PARAM_FOUR", CommonUtil.getStringValue(afPartner.getParamFour()));
					afPartnerMap.put("REFERENCE_TOKEN", CommonUtil.getStringValue(afPartner.getReferenceToken()));
					afPartnerMap.put("PARTNER_LANDING_URL", CommonUtil.getStringValue(afPartner.getPartnerLandingUrl()));
					afPartnerMap.put("TERMS_AND_CONDITION", CommonUtil.getStringValue(afPartner.getTermsAndCondition()));
					afPartnerMap.put("POINT_1", CommonUtil.getStringValue(afPartner.getPoint1()));
					
					afPartnerMap.put("POINT_2", CommonUtil.getStringValue(afPartner.getPoint2()));
					afPartnerMap.put("POINT_3", CommonUtil.getStringValue(afPartner.getPoint3()));
					afPartnerMap.put("POINT_4", CommonUtil.getStringValue(afPartner.getPoint4()));
					afPartnerMap.put("POINT_5", CommonUtil.getStringValue(afPartner.getPoint5()));
					afPartnerMap.put("HOST_PARTNER", CommonUtil.getStringValue(afPartner.getHostPartner()));
					afPartnerMap.put("DEFAULT_PARTNER", CommonUtil.getStringValue(afPartner.getDefaultPartner()));
					afPartnerMap.put("ALLOW_JPNUMBER", CommonUtil.getStringValue(afPartner.getAllowJpnumber()));
					afPartnerMap.put("DISPLAY_PRT_POPUP", CommonUtil.getStringValue(afPartner.getDisplayPrtPopup()));
					afPartnerMap.put("ACTIVATION_DATE", CommonUtil.getStringValue(afPartner.getActivationDate()));
					afPartnerMap.put("DEACTIVATION_DATE", CommonUtil.getStringValue(afPartner.getDeactivationDate()));
					afPartnerMap.put("PARTNER_POSITION", CommonUtil.getStringValue(afPartner.getPartnerPosition()));
					afPartnerMap.put("PARTNER_STATUS", CommonUtil.getStringValue(afPartner.getPartnerStatus()));
					afPartnerMap.put("CREATED_TIME", CommonUtil.getStringValue(afPartner.getCreatedTime()));
					afPartnerMap.put("CREATED_BY", CommonUtil.getStringValue(afPartner.getCreatedBy()));
					afPartnerMap.put("MODIFIED_TIME", CommonUtil.getStringValue(afPartner.getModifiedTime()));
					afPartnerMap.put("MODIFIED_BY", CommonUtil.getStringValue(afPartner.getModifiedBy()));
					afPartnerMap.put("STATUS", CommonUtil.getStringValue(afPartner.getStatus()));
					afPartnerMap.put("LOGO_PARTNER", CommonUtil.getStringValue(afPartner.getLogoPartner()));
					afPartnerMap.put("OFFLINE_PARTNER", CommonUtil.getStringValue(afPartner.getOffLinePartner()));
					afPartnerMap.put("LOGO_PARTNER_POSITION", CommonUtil.getStringValue(afPartner.getLogoPartnerPosition()));
					afPartnerMap.put("LOGO_PARTNER_TILE_ORDER", CommonUtil.getStringValue(afPartner.getLogoPartnerTileOrder()));
					afPartnerMap.put("LOGO_PARTNER_TILE_POSITION", CommonUtil.getStringValue(afPartner.getLogoPartnerTilePosition()));
					afPartnerMap.put("LOGO_PARTNER_DISP_LIMIT", CommonUtil.getStringValue(afPartner.getLogoPartnerDispLimit()));
					afPartnerMap.put("LOGO_PARTNER_SEARCH_URL", CommonUtil.getStringValue(afPartner.getLogoPartnerSearchUrl()));
					afPartnerMap.put("CATEGORY_MAPPING", CommonUtil.getStringValue(afPartner.getTextCatMapping()));
					afPartnerMap.put("READ_PARTNER", CommonUtil.getStringValue(afPartner.getReadPartner()));
					afPartnerMap.put("TALK_PARTNER", CommonUtil.getStringValue(afPartner.getTalkPartner()));
					afPartnerMapList.add(afPartnerMap);
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in PartnerManagementServiceImpl getAllPrtWebservice() :", e);
		}
		
		return afPartnerMapList;
	}

	@Override
	public List<PartnerBean> getPartnerListPagination(int length, int start, String partnerType) {

		List<AfPartner> afPartnerList=null;
		List<PartnerBean> partnerList=new ArrayList<>();
		int partnerStatusCode;
		String dateStr="";
		String[] affiliateVal=null;
		String[] sourceVal=null;
		String[] referenceVal=null;
		String[] paramfourVal=null;
		Date dt=null;
		
		try{
			afPartnerList=partnermanagementDAO.getPartners(length,start,partnerType);
		for(AfPartner afPartner:afPartnerList){
			PartnerBean partnerBean=new PartnerBean();
			partnerBean.setPrtNo(afPartner.getPrtNo());
			partnerBean.setPartnerId(afPartner.getPartnerId());
			partnerBean.setPartnerName(afPartner.getPartnerName()!=null?afPartner.getPartnerName():"");			
			partnerBean.setRatePerMile(afPartner.getRatePerMile()!=null?afPartner.getRatePerMile():0);
			partnerBean.setHostPartnerCode(afPartner.getHostPartner()!=null?afPartner.getHostPartner():0);
			partnerBean.setDefaultPartnerCode(afPartner.getDefaultPartner());
			partnerBean.setLogoPartnerCode((Byte)afPartner.getLogoPartner()!=null?afPartner.getLogoPartner():0);
			partnerBean.setOffLinePartnerCode((Byte)afPartner.getOffLinePartner()!=null?afPartner.getOffLinePartner():0);
			
			partnerBean.setReadPartnerCode((Byte)afPartner.getReadPartner()!=null?afPartner.getReadPartner():0);
			partnerBean.setTalkPartnerCode((Byte)afPartner.getTalkPartner()!=null?afPartner.getTalkPartner():0);
			
			partnerBean.setPartnerLandingUrl(afPartner.getPartnerLandingUrl()!=null?afPartner.getPartnerLandingUrl():"");
			partnerBean.setLogoPartnerSearchUrl(afPartner.getLogoPartnerSearchUrl()!=null?afPartner.getLogoPartnerSearchUrl():"");
			partnerBean.setLogoPrtDisplayOrder(afPartner.getPartnerPosition()!=null?afPartner.getPartnerPosition():0);
			partnerBean.setLogoPrtTilePos(afPartner.getLogoPartnerPosition()!=null?afPartner.getLogoPartnerPosition():0);
			dt=afPartner.getActivationDate()!=null?afPartner.getActivationDate():new Date();
			dateStr=ServiceProviderUtil.getInstance().getFormattedDate(afPartner.getActivationDate(),"dd/MM/yyyy");
			if(dt!=null && !dt.equals(""))
			partnerBean.setActivationDate(dateStr);
			dt=afPartner.getDeactivationDate()!=null?afPartner.getDeactivationDate():new Date();
			dateStr=ServiceProviderUtil.getInstance().getFormattedDate(afPartner.getDeactivationDate(),"dd/MM/yyyy");
			if(dt!=null && !dt.equals(""))
			partnerBean.setDeactivationDate(dateStr);
			partnerBean.setPartnerStatusCode(afPartner.getPartnerStatus());
			if(afPartner.getPartnerLogoUrl()!=null)
			partnerBean.setPartnerImageUrl(afPartner.getPartnerLogoUrl());
			partnerBean.setPartnerPopupImageUrl(afPartner.getPartnerPopupLogoUrl());
			System.out.println("partner popup------------>"+partnerBean.getPartnerPopupImageUrl());
			partnerStatusCode=afPartner.getPartnerStatus();
			
			String jpmDesc = "";
			jpmDesc = afPartner.getJpmileDesc()!=null ?afPartner.getJpmileDesc() : "";
			jpmDesc = URLEncoder.encode(jpmDesc, "UTF-8");
			partnerBean.setJpmileDesc(jpmDesc);
			
			partnerBean.setPartnerDesc(afPartner.getPartnerDesc()!=null?afPartner.getPartnerDesc():"");
			partnerBean.setJpmileReconPeriod(null!=afPartner.getJpmileReconPeriod()?afPartner.getJpmileReconPeriod()+"":"");
			partnerBean.setActivityId(null!=afPartner.getActivityId()?afPartner.getActivityId()+"":"");
			partnerBean.setAggregateId(null!=afPartner.getAggregateId()?afPartner.getAggregateId()+"":"");
			if(1==partnerStatusCode){
				partnerBean.setPartnerStatus(AfConstants.STATUS_ACTIVE);
				partnerBean.setPartnerStatusClass(AfConstants.STATUS_CLASS_GREEN);
				
			}
			if(0==partnerStatusCode){
				partnerBean.setPartnerStatus(AfConstants.STATUS_IN_ACTIVE);
				partnerBean.setPartnerStatusClass(AfConstants.STATUS_CLASS_RED);
				
			}
			
			if(afPartner.getPartnerIconUrl()!=null)
				partnerBean.setPartnerIconUrl(afPartner.getPartnerIconUrl());
			
			if(afPartner.getAffiliateId()!=null){
				affiliateVal = afPartner.getAffiliateId().toString().split("=");
				if(affiliateVal.length != 0){
					if(affiliateVal.length > 1){
						partnerBean.setAffiliateIdName(affiliateVal[0]);
						partnerBean.setAffiliateId(affiliateVal[1]);
					}else{
					partnerBean.setAffiliateIdName(affiliateVal[0]);
					}
				}else{
					partnerBean.setAffiliateId("");
					partnerBean.setAffiliateIdName("");
				}
			}else{
				partnerBean.setAffiliateId("");
				partnerBean.setAffiliateIdName("");
			}

			if(afPartner.getSourceOfInfo()!=null){
				sourceVal = afPartner.getSourceOfInfo().toString().split("=");
				if(sourceVal.length != 0){
					if(sourceVal.length > 1){
						partnerBean.setSourceOfInfoName(sourceVal[0]);
						partnerBean.setSourceOfInfo(sourceVal[1]);
					}else{
						partnerBean.setSourceOfInfoName(sourceVal[0]);
					}
				}else{
					partnerBean.setSourceOfInfo("");
					partnerBean.setSourceOfInfoName("");
				}	
			}else{
				partnerBean.setSourceOfInfo("");
				partnerBean.setSourceOfInfoName("");
			}
			
			if(afPartner.getParamFour()!=null){
				paramfourVal = afPartner.getParamFour().toString().split("=",2);
				if(paramfourVal.length != 0){
					if(paramfourVal.length > 1){
						partnerBean.setParamFourName(paramfourVal[0]);
						partnerBean.setParamFour(paramfourVal[1]);
					}else{
						partnerBean.setParamFourName(paramfourVal[0]);
					}
				}else{
					partnerBean.setParamFour("");
					partnerBean.setParamFourName("");
				}	
			}else{
				partnerBean.setParamFour("");
				partnerBean.setParamFourName("");
			}

			if(afPartner.getReferenceToken()!=null){
				referenceVal = afPartner.getReferenceToken().toString().split("=");
				if(referenceVal.length != 0){
					if(referenceVal.length > 1){
						partnerBean.setReferenceTokenName(referenceVal[0]);
						partnerBean.setReferenceToken(referenceVal[1]);
					}else{
						partnerBean.setReferenceTokenName(referenceVal[0]);
					}
				}else{
					partnerBean.setReferenceToken("");
					partnerBean.setReferenceTokenName("");
				}	
			}else{
				partnerBean.setReferenceToken("");
				partnerBean.setReferenceTokenName("");
			}
			
			String tAndC = afPartner.getTermsAndCondition();
			
			if(tAndC != null){
				tAndC = URLEncoder.encode(tAndC, "UTF-8");
			}
			
			partnerBean.setTermsAndCondition(tAndC!=null?tAndC:"");
			partnerBean.setAllowJpnumber(afPartner.getAllowJpnumber()+"");
			partnerBean.setDisplayPrtPopup(afPartner.getDisplayPrtPopup()+"");
			
			partnerList.add(partnerBean);
		}
		}catch(Exception e){
			logger.error("@@@@ Exception in PartnerManagementServiceImpl at getPartnerListPagination :"+e);
		}
		return partnerList;
	
	}

	@Override
	public Long getPartnerListPaginationCount(String partnerType) {
		Long totalCount =0L;
		try {
			totalCount = partnermanagementDAO.getPartnerListPaginationCount(partnerType);
		} catch (Exception e) {
			logger.error("@@@@Exception in PartnerManagementServiceImpl at getPartnersCommissionPaginationCount() :", e);
		}
		return totalCount;
	}

	@Override
	public Boolean checkAggregateID(String aggregateid) {
		boolean aggregateIdExist=false;
		try{
			aggregateIdExist=partnermanagementDAO.checkAggregateID(aggregateid);
			logger.debug(">>>>>>>>checkActivityID service>>>"+aggregateIdExist);
		}catch(Exception e){
			logger.error("@@Exception in PartnerManagementServiceImpl checkAggregateID", e);
		}
		return aggregateIdExist;
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public String createPartnerMapping(JSONObject createPatJson, HttpServletRequest request) {
		AfPartnerMapping AfPartnerMapping= new AfPartnerMapping();
		String status = "failure"; 
		try {
			String partType=createPatJson.getString("partType");
			String partNo = createPatJson.getString("partNumber");
			String createCategoryName = createPatJson.getString("createMappingPartnerName");
			String catStat = "";
			
			AfPartner afpartner = partnermanagementDAO.getPartnerByPartnerNO(partNo);
			if(afpartner!=null){
				AfPartnerMapping.setAfPartner(afpartner);
			}
			AfPartnerMapping.setPartnerType(partType);
			AfPartnerMapping.setPartnerMappingName(createCategoryName);
			AfPartnerMapping.setStatus(Byte.parseByte((createPatJson.getString("createpartcatstatus"))));
			AfPartnerMapping.setAllowLandingUrl(Byte.parseByte((createPatJson.getString("allowLandingUrl"))));
			status=partnermanagementDAO.createPartnerMapping(AfPartnerMapping);
			
		} catch (Exception e) {
		e.printStackTrace();
		logger.error("@@@@ Exception in PartnerManagementServiceImpl at createPartnerMapping() :", e);
		}		
		return status;
	}

	@Override
	public List<PartnerMappingBean> getPartnerMappingDetails() {
		List<PartnerMappingBean> partnerMappingBean=new ArrayList<>();
		try{
			List<AfPartnerMapping> afPartnerMappingList = partnermanagementDAO.getpartnerMappingDetails();
			for(AfPartnerMapping afPartnerMapping :afPartnerMappingList){
				PartnerMappingBean PartnerMappingBeanNew= new PartnerMappingBean();
				PartnerMappingBeanNew.setPrtMapNo(afPartnerMapping.getPrtMapNo());
				PartnerMappingBeanNew.setPartnerMappingName(afPartnerMapping.getPartnerMappingName());
				PartnerMappingBeanNew.setPartnerName(afPartnerMapping.getAfPartner().getPartnerName());
				PartnerMappingBeanNew.setPartNO(afPartnerMapping.getAfPartner().getPrtNo().toString());
				PartnerMappingBeanNew.setPartnerType(afPartnerMapping.getPartnerType());
				PartnerMappingBeanNew.setAllowLandingUrl(afPartnerMapping.getAllowLandingUrl());
				partnerMappingBean.add(PartnerMappingBeanNew);
			}
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in PartnerManagementServiceImpl at getPartnerMappingDetails() :", e);
		}
		return partnerMappingBean;
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public String updatePartnerMapping(JSONObject createPrtJson, HttpServletRequest request) {
		String status=AfConstants.SUCCESS;
		AfPartnerMapping afPartnerMapping=null;
		try{
			afPartnerMapping= partnermanagementDAO.getPartnerMappingDetailsById(createPrtJson.getInt("id"));
			afPartnerMapping.setAfPartner(partnermanagementDAO.getPartnerByPartnerNO(createPrtJson.getString("partNumber")));
			afPartnerMapping.setPartnerMappingName((createPrtJson.getString("createMappingPartnerName")));
			afPartnerMapping.setAllowLandingUrl(Byte.parseByte(createPrtJson.getString(("allowlandingurl"))));
			status=partnermanagementDAO.updatePartnerMapping(afPartnerMapping);
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in PartnerManagementServiceImpl at updatePartnerMapping() :", e);
		}
		return status;
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public String deletePartnerMapping(Integer id, HttpServletRequest request) {
		String deleteStatus="";
		try{
		deleteStatus=partnermanagementDAO.deletePartnerMapping(id);
		}
		catch(Exception e){
			logger.error("@@@@ Exception in PartnerManagementServiceImpl at deletePartnerMapping() :", e);
		}
		return deleteStatus;
	}

	@Override
	public Boolean MappingPartnerNameExist(String patName,String partNumber) {
		boolean isPartnerNameExist = false;
		try {
			isPartnerNameExist = partnermanagementDAO.MappingPartnerNameExist(patName,partNumber);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("@@@@Exception in PartnerManagementServiceImpl at MappingPartnerNameExist() :", e);
		}
		return isPartnerNameExist;
	}

}
