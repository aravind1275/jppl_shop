package com.af.admin.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.af.admin.DAO.EmailManagementDAO;
import com.af.admin.beans.EmailManagementBean;
import com.af.admin.constants.AfConstants;
import com.af.admin.entity.AfEmailManagement;
import com.af.admin.service.CommonService;
import com.af.admin.service.EmailManagementService;

@Component
@Transactional(readOnly = true)
public class EmailManagementServiceImpl implements EmailManagementService {

	@Autowired
	CommonService commonService;

	@Autowired
	EmailManagementDAO emailManagementDAO;

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void addEmail(EmailManagementBean emailManagementBean) {

		String title = "", content = "", name = "";

		AfEmailManagement afEmailManagement = new AfEmailManagement();


		name = emailManagementBean.getName();
		name = (null != name) ? name.trim() : "";
		afEmailManagement.setName(name);

		title = emailManagementBean.getTitle();
		title = (null != title) ? title.trim() : "";
		afEmailManagement.setTitle(title);

		content = emailManagementBean.getContent();
		content = (null != content) ? content.trim() : "";
		afEmailManagement.setContent(content);

		afEmailManagement.setCreatedTime(new Date());
		afEmailManagement.setEmailStatus(emailManagementBean.getEmailStatus());
		afEmailManagement.setStatus(AfConstants.STATUS_ACTIVE_VALUE);

		if (null != emailManagementBean.getUserId()) {
			afEmailManagement.setCreatedBy(emailManagementBean.getUserId());
		}
		emailManagementDAO.addContent(afEmailManagement);
	}

	@Override
	public List<EmailManagementBean> getAllEmail() {
		List<EmailManagementBean> contentManagementBeans = null;
		String name = "", content = "", title = "";

		try {
			List<AfEmailManagement> listContentManagement = new ArrayList<AfEmailManagement>();
			listContentManagement = emailManagementDAO.getAllEmail();
			contentManagementBeans = new ArrayList<EmailManagementBean>();
			for (AfEmailManagement contentManagement : listContentManagement) {
				EmailManagementBean emailManagementBean = new EmailManagementBean();

				name = contentManagement.getName();
				name = (null != name) ? name.trim() : "";
				emailManagementBean.setName(name);

				content = contentManagement.getContent();
				content = (null != content) ? content.trim() : "";
				emailManagementBean.setContent(content);


				title = contentManagement.getTitle();
				title = (null != title) ? title.trim() : "";
				emailManagementBean.setTitle(title);

				emailManagementBean.setEmId(contentManagement.getEmId());
				emailManagementBean.setEmailStatus(contentManagement.getEmailStatus());

				contentManagementBeans.add(emailManagementBean);
			}
		} catch (Exception e) {
		}
		return contentManagementBeans;
	}

	public EmailManagementBean getEmailById(int emId) {
		EmailManagementBean emailManagementBean = null;
		AfEmailManagement afEmailManagement = new AfEmailManagement();
		try {
			afEmailManagement = emailManagementDAO.getContentById(emId);
		} catch (Exception e) {
		}
		if (null != afEmailManagement) {
			emailManagementBean = new EmailManagementBean();
			emailManagementBean.setName(afEmailManagement.getName());
			emailManagementBean.setEmId(afEmailManagement.getEmId());
			emailManagementBean.setTitle(afEmailManagement.getTitle());
			emailManagementBean.setContent(afEmailManagement.getContent());
			emailManagementBean.setEmailStatus(afEmailManagement.getEmailStatus());
			
		}
		return emailManagementBean;
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public String updateEmail(EmailManagementBean emailManagementBean) {

		String status = "";
		AfEmailManagement afEmailManagement = null;
		try {
			afEmailManagement = emailManagementDAO.getContentById(emailManagementBean.getEmId());
		} catch (Exception e) {
		}

		if (null != afEmailManagement) {
			if (null != emailManagementBean.getUserId())
				afEmailManagement.setModifiedBy(emailManagementBean.getUserId());

			afEmailManagement.setModifiedTime(new Date());
			afEmailManagement.setName(emailManagementBean.getName());
			afEmailManagement.setContent(emailManagementBean.getContent());
			afEmailManagement.setTitle(emailManagementBean.getTitle());
			afEmailManagement.setEmailStatus(emailManagementBean.getEmailStatus());

			status = emailManagementDAO.updateContent(afEmailManagement);
		}
		return status;
	}

	@Override
	public String emailTypeExist(String emailType) {
		return emailManagementDAO.emailTypeExist(emailType);
	}


}
