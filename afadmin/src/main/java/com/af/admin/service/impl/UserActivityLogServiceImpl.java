package com.af.admin.service.impl;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.af.admin.DAO.UserActivityLogDAO;
import com.af.admin.beans.UserActivityLogBean;
import com.af.admin.controller.UserManagementController;
import com.af.admin.entity.AfUserActivityLog;
import com.af.admin.service.UserActivityLogService;

@Component
@Transactional(readOnly = true)
public class UserActivityLogServiceImpl implements UserActivityLogService{

	private Logger logger = LogManager.getLogger(UserManagementController.class.getName());
	
	
	
	@Autowired
	UserActivityLogDAO userActLogDao;
	
	
	@Override
	public List<UserActivityLogBean> getUserActLogList(String userActFromDate, String userActTodate) {
		
		List<AfUserActivityLog> afUserActivityLogList = null;
		List<UserActivityLogBean> userActLogBean = new ArrayList<UserActivityLogBean>();
	
	try {
		afUserActivityLogList = userActLogDao.getUserActivityLogByDate(userActFromDate, userActTodate);
		
	
		if(afUserActivityLogList != null){
			
			for(AfUserActivityLog afUserActLog : afUserActivityLogList){
				
				UserActivityLogBean usActLogBean = new UserActivityLogBean();
				
				usActLogBean.setUserActId(afUserActLog.getUserActId());
				usActLogBean.setUserOldFieldValue(afUserActLog.getUserOldFieldValue());
				usActLogBean.setUserNewFieldValue(afUserActLog.getUserNewFieldValue());
				usActLogBean.setUserActCreatedBy(afUserActLog.getUserActCreatedBy());
				usActLogBean.setUserActCreatedDateTime(afUserActLog.getUserActCreatedDateTime());
				usActLogBean.setUserActivity(afUserActLog.getUserActivity());
				usActLogBean.setUserActIpVal(afUserActLog.getUserActIpVal());
				usActLogBean.setUserActModule(afUserActLog.getUserActModule());
				
				userActLogBean.add(usActLogBean);
			}
		}
		
	    } catch (Exception e) {
			logger.error("@@@@ Exception in UserActivityLogServiceImpl at getUserActLogList() :", e);
	    }
		
		return userActLogBean;
	}

	@Override
	public List<UserActivityLogBean> getUserActLogListDefault(String currentDate) {
		
		List<AfUserActivityLog> afUserActivityLogList = null;
		List<UserActivityLogBean> userActLogBean = new ArrayList<UserActivityLogBean>();
	
	try {
		afUserActivityLogList = userActLogDao.getUserActivityLogByDateDefault(currentDate);
		
		if(afUserActivityLogList != null){
			
			for(AfUserActivityLog afUserActLog : afUserActivityLogList){
				
				UserActivityLogBean usActLogBean = new UserActivityLogBean();
				
				usActLogBean.setUserActId(afUserActLog.getUserActId());
				usActLogBean.setUserOldFieldValue(afUserActLog.getUserOldFieldValue());
				usActLogBean.setUserNewFieldValue(afUserActLog.getUserNewFieldValue());
				usActLogBean.setUserActCreatedBy(afUserActLog.getUserActCreatedBy());
				usActLogBean.setUserActCreatedDateTime(afUserActLog.getUserActCreatedDateTime());
				usActLogBean.setUserActivity(afUserActLog.getUserActivity());
				usActLogBean.setUserActIpVal(afUserActLog.getUserActIpVal());
				usActLogBean.setUserActModule(afUserActLog.getUserActModule());
				
				userActLogBean.add(usActLogBean);
			}
		}
		
	    } catch (Exception e) {
			logger.error("@@@@ Exception in UserActivityLogServiceImpl at getUserActLogListDefault() :", e);

	    }
	
		return userActLogBean;
	}

}
