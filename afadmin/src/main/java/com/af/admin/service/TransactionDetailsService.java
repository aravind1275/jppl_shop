package com.af.admin.service;

import java.util.List;

import com.af.admin.beans.ExportBean;
import com.af.admin.beans.TransactionDetailsBean;

public interface TransactionDetailsService {

	List<TransactionDetailsBean> getTransactionDetails(String trackingId);
	 List<TransactionDetailsBean> getTransactionDateDetails(String jpNumber,String fromDate, String toDate);
        List<ExportBean> getExportTransactionReportsList(String jpNumber,String fromDate,String toDate);
}
