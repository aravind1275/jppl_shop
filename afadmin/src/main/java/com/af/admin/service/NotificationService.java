package com.af.admin.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;

import com.af.admin.beans.AfNotificationBean;


public interface NotificationService {

	public List<AfNotificationBean> getNotificationList();

	public String createNotification(AfNotificationBean notificationBean, HttpServletRequest request);

	public String deleteNotification(Integer notificationNo, HttpServletRequest request);

	public String updateNotification(JSONObject userJson, HttpServletRequest request);

	public Boolean checkNotificationID(String notificationId);

}
