package com.af.admin.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.af.admin.DAO.CommonDAO;
import com.af.admin.DAO.UserActivityLogDAO;
import com.af.admin.DAO.UserManagementDAO;
import com.af.admin.beans.AdminRolesBean;
import com.af.admin.beans.AdminUserBean;
import com.af.admin.beans.ChangePasswordBean;
import com.af.admin.beans.UserBean;
import com.af.admin.constants.AfConstants;
import com.af.admin.entity.AfAdminRolePermissions;
import com.af.admin.entity.AfAdminRoles;
import com.af.admin.entity.AfAdminUser;
import com.af.admin.entity.AfBanner;
import com.af.admin.entity.AfPermissionValues;
import com.af.admin.entity.AfUserActivityLog;
import com.af.admin.service.CommonService;
import com.af.admin.service.UserManagementService;
import com.af.admin.utils.AdminUserActivityUtil;
import com.af.admin.utils.ServiceProviderUtil;

@Component
@Transactional(readOnly=true)
public class UserManagementServiceImpl implements UserManagementService{
	
	private Logger logger = LogManager.getLogger(UserManagementServiceImpl.class.getName());
	
	@Autowired
	UserManagementDAO userManagementDAO;
	
	@Autowired
	CommonDAO commonDAO;
	
	@Autowired
	CommonService commonService;
	
	@Autowired
	UserActivityLogDAO userActLogDao;
	
	@Autowired
	AdminUserActivityUtil adminUserActivityUtil;
	
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");

	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public AdminUserBean authenticateUser(AdminUserBean adminUserBean) {
		AdminUserBean bean=null;
		AfAdminUser afAdminUser= new AfAdminUser();
		try{
			afAdminUser=userManagementDAO.getUserDetailsByUserName(adminUserBean.getUserName());
		
		}
		catch(Exception e){
			logger.error("@@@@ Exception in UserManagementServiceImpl authenticateUser() :"+e);
		}
		if(null!=afAdminUser)
		{
			afAdminUser.setLastLogin(new Date());
			userManagementDAO.updateUser(afAdminUser);
			bean=new AdminUserBean();
			bean.setUserId(afAdminUser.getUserId());
			bean.setFirstName(afAdminUser.getFirstName());
			bean.setUserRole(afAdminUser.getAfAdminRoles().getRoleId());
			bean.setPassword(afAdminUser.getPassword());
			if(!afAdminUser.getPassword().trim().equals(adminUserBean.getPassword().trim()))bean.setErrorMsg("Password is invalid");
			else if(afAdminUser.getUserStatus()==AfConstants.USER_STATUS_IN_ACTIVE)bean.setErrorMsg("You have been denied access. Please contact Admin");
			else if(afAdminUser.getStatus()==AfConstants.USER_STATUS_IN_ACTIVE)bean.setErrorMsg("User does not exist");
			else if(afAdminUser.getAfAdminRoles().getStatus()==AfConstants.ROLE_STATUS_IN_ACTIVE)bean.setErrorMsg("User Role does not exist. Please contact Admin");
			
		}
		return bean;
	}


	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String updateAdminUser(JSONObject userJson,HttpServletRequest request) {
		Integer userId=-1;
		Integer roleId=-1;
		AfAdminRoles adminRoles=null;
		String status="";
		AfAdminUser adminUserOld=null;
		String replaceInOld="";
		String replaceInNew="";
		try{
			userId=userJson.getInt("userId");
			roleId=userJson.getInt("roleId");
			AfAdminUser adminUser=userManagementDAO.getAdminUserById(userId);
			adminUserOld=(AfAdminUser) adminUser.clone();
			if(adminUser.getAfAdminRoles().getRoleId()!=roleId){
				adminRoles=commonDAO.getAdminRoleById(roleId);
				adminUser.setAfAdminRoles(adminRoles);
			}
			adminUser.setFirstName(userJson.getString("firstName")); 
			adminUser.setLastName(userJson.getString("lastName"));
			adminUser.setMobileNumber(userJson.getString("mobileNumber"));
			adminUser.setUserName(userJson.getString("emailId"));
			adminUser.setUserStatus((byte) userJson.getInt("statusType"));
			adminUser.setModifiedBy(1);
			adminUser.setModifiedTime(new Date());
			String statusInactive=	adminUserOld.getUserStatus()==0?"InActive":"Active";
	     	String statusActive=adminUser.getUserStatus()==0?"InActive":"Active";
			status=userManagementDAO.updateUser(adminUser);
			if("success".equalsIgnoreCase(status)){
				String ipAddress = request.getHeader("X-FORWARDED-FOR");
				if (ipAddress == null) {
					ipAddress = request.getRemoteAddr();
				}
				AfUserActivityLog updateUsActLog = new AfUserActivityLog();
				Map<String,String> objectCom = new LinkedHashMap<>();
				
					updateUsActLog.setUserActivity("Update User ");
					updateUsActLog.setUserActCreatedDateTime(new Date());
					updateUsActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
					updateUsActLog.setUserActIpVal(ipAddress);
					updateUsActLog.setUserActModule("User Management");
					if(!adminUser.getFirstName().equalsIgnoreCase(adminUserOld.getFirstName())){
						replaceInOld=replaceInOld+"|First Name :"+adminUserOld.getFirstName();
					    replaceInNew=replaceInNew+"|First Name :"+adminUser.getFirstName();
					}
					if(!adminUser.getLastName().equalsIgnoreCase(adminUserOld.getLastName())){
						replaceInOld=replaceInOld+"|Last Name:"+adminUserOld.getLastName();
						replaceInNew=replaceInNew+"|Last Name:"+adminUser.getLastName();
					}
					if(!adminUser.getMobileNumber().equalsIgnoreCase(adminUserOld.getMobileNumber())){
						replaceInOld=replaceInOld+"|Mobile No:"+adminUserOld.getMobileNumber();
						replaceInNew=replaceInNew+"|Mobile No:"+adminUser.getMobileNumber();
					}
					if(!adminUser.getAfAdminRoles().getRoleName().equalsIgnoreCase(adminUserOld.getAfAdminRoles().getRoleName())){
						replaceInOld=replaceInOld+"|User Role:"+adminUserOld.getAfAdminRoles().getRoleName();
					     replaceInNew=replaceInNew+"|User Role:"+adminUser.getAfAdminRoles().getRoleName();
					}
					
					if(adminUser.getUserStatus()!=adminUserOld.getUserStatus()){
						replaceInOld=replaceInOld+"|Status:"+statusInactive;
						replaceInNew=replaceInNew+"|Status:"+statusActive;
					}
					
					if(replaceInNew.isEmpty() && replaceInOld.isEmpty()) {
                        replaceInOld=replaceInOld+"|ModifiedTime:"+simpleDateFormat.format(adminUserOld.getModifiedTime());
                        replaceInNew=replaceInNew+"|ModifiedTime:"+simpleDateFormat.format(adminUser.getModifiedTime());
                    }
					updateUsActLog.setUserNewFieldValue(replaceInNew);
		    		updateUsActLog.setUserOldFieldValue(replaceInOld);
					userActLogDao.updateUserLog(updateUsActLog);
				
				
			}
			
		}
		catch(Exception e){
			logger.error("@@@@ Exception in UserManagementServiceImpl updateAdminUser() :"+e);
		}
		
		return status;
	}

	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String createUser(AdminUserBean adminUserBean,HttpServletRequest request) {
		
		AfAdminUser adminUser=new AfAdminUser();
		String status="";
		String oldOrNewVal = "";
		String commLogStatus = "";
		
		try{
		adminUser.setFirstName(adminUserBean.getFirstName());
		adminUser.setLastName(adminUserBean.getLastName());
		adminUser.setMobileNumber(adminUserBean.getMobileNumber());
		adminUser.setUserName(adminUserBean.getUserName());
		adminUser.setPassword(adminUserBean.getPassword());
		adminUser.setAfAdminRoles(commonDAO.getAdminRoleById(adminUserBean.getUserRole()));
		adminUser.setUserStatus((byte) adminUserBean.getStatusType());
		adminUser.setCreatedBy(1);
		adminUser.setCreatedTime(new Date());
		adminUser.setStatus((byte) 1);
		String status1=adminUser.getUserStatus()+"";
		status1 = status1.replaceAll("0","InActive");
		status1 = status1.replaceAll("1", "Active");
		status=userManagementDAO.createUser(adminUser);
		if("success".equalsIgnoreCase(status)){
			String ipAddress = request.getHeader("X-FORWARDED-FOR");
			if(ipAddress == null) {
				ipAddress = request.getRemoteAddr();
			}
			ipAddress = (null != ipAddress) ? ipAddress.trim() : "";
			AfUserActivityLog afPrtActLog=new AfUserActivityLog();
			oldOrNewVal="First Name : "+adminUser.getFirstName()+" | "+"Last Name: "+adminUser.getLastName()+" | "+
					""+"Mobile Number: "+adminUser.getMobileNumber()+"| "+"Email ID (@jetprivilege only): "+adminUser.getUserName()+" |"+" Password: "+adminUser.getPassword()+" |"
							+ " "+"Assign Role : "+adminUser.getAfAdminRoles().getRoleName()+" |"+"Status : "+status1+" |";
									
							
		
					afPrtActLog.setUserNewFieldValue(oldOrNewVal);
					afPrtActLog.setUserOldFieldValue("NA");
					afPrtActLog.setUserActivity("Create User");
					afPrtActLog.setUserActCreatedDateTime(new Date());
					afPrtActLog.setUserActIpVal(ipAddress);
					afPrtActLog.setUserActModule("User Management");
					afPrtActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
					
					commLogStatus = userActLogDao.createUserActLog(afPrtActLog);
		}
		}
		catch(Exception e)
		{
			logger.debug("Exception in Usermanagement ServiceImpl createUser()"+e);
			return "failure";
		}
		return status;
	}

	@Override
	public boolean checkUserName(String userName) {
		boolean userNameExist=false;
		try{
			userNameExist=userManagementDAO.checkUserName(userName);
		}catch(Exception e){
			logger.error("@@Exception in checkUserName", e);
		}
		return userNameExist;
	}

	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String deleteUser(Integer userId,HttpServletRequest request) {
		String deleteStatus="";
		String oldOrNewVal = "";
		AfAdminUser adminUser=null;
		AdminUserBean bean=null;
		try{
			String ipAddress = request.getHeader("X-FORWARDED-FOR");
			if (ipAddress == null) {
				ipAddress = request.getRemoteAddr();
			}
			adminUser = userManagementDAO.getAdminUserById(userId);
			String status1=adminUser.getUserStatus()+"";
			status1 = status1.replaceAll("0","InActive");
			status1 = status1.replaceAll("1", "Active");
			
			
			AfUserActivityLog updateUsActLog = new AfUserActivityLog();
			oldOrNewVal="First Name : "+adminUser.getFirstName()+" | "+"Last Name: "+adminUser.getLastName()+" | "+
					""+"Mobile Number: "+adminUser.getMobileNumber()+"| "+"Email ID (@jetprivilege only): "+adminUser.getUserName()+" |"+" Password: "+adminUser.getPassword()+" |"
							+ " "+"Assign Role : "+adminUser.getAfAdminRoles().getRoleName()+" |"+"Status : "+status1+" |";
			updateUsActLog.setUserActivity("Delete User");
	    	updateUsActLog.setUserActCreatedDateTime(new Date());
	    	updateUsActLog.setUserNewFieldValue("NA");
	    	updateUsActLog.setUserActIpVal(ipAddress);
	    	updateUsActLog.setUserActModule("User Management");
	    	updateUsActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
	    	updateUsActLog.setUserOldFieldValue(oldOrNewVal);
	    	userActLogDao.updateUserLog(updateUsActLog);
			deleteStatus=userManagementDAO.deleteUser(userId);
		}catch(Exception e){
			logger.error("@@@@ Exception in UserManagementServiceImpl deleteUser() :"+e);
		}
		return deleteStatus;
	}


	@Override
	public AdminUserBean getUserDetailsByUserName(String userName) {
		AdminUserBean bean=null;
	AfAdminUser afAdminUser=new AfAdminUser();
		try{
			afAdminUser=userManagementDAO.getUserDetailsByUserName(userName);
		}catch(Exception e){
			logger.error("@@@@ Exception in UserManagementServiceImpl authenticateUser() :"+e);
		}
		if(null!=afAdminUser){
			bean=new AdminUserBean();
			bean.setUserId(afAdminUser.getUserId());
			bean.setPassword(afAdminUser.getPassword());
		}
		return bean;
	}


	@Override
	public List<UserBean> getAdminUsers() {
		List<AfAdminUser> afAdminUserList=null;
		List<UserBean> userList=new ArrayList<UserBean>();
		int userStatus;
		String dateStr="";
		
		try{
		afAdminUserList=userManagementDAO.getAdminUsers();
		for(AfAdminUser afAdminUser:afAdminUserList){
			UserBean userBean=new UserBean();
			userBean.setUserId(afAdminUser.getUserId());
			userBean.setFirstName(afAdminUser.getFirstName()!=null?afAdminUser.getFirstName():"");
			userBean.setLastName(afAdminUser.getLastName()!=null?afAdminUser.getLastName():"");
			dateStr=ServiceProviderUtil.getInstance().getFormattedDate(afAdminUser.getLastLogin(),"dd MMM yyyy HH:mm a");
			if(dateStr!=null && !dateStr.equals(""))
			userBean.setLastLogin(dateStr+" IST");
			else
			userBean.setLastLogin(dateStr);
			userBean.setMobileNumber(afAdminUser.getMobileNumber());
			userBean.setUserName(afAdminUser.getUserName());
			userBean.setRole(afAdminUser.getAfAdminRoles().getRoleName());
			userBean.setRoleId(afAdminUser.getAfAdminRoles().getRoleId());
			userStatus=afAdminUser.getUserStatus();
			userBean.setUserStatusCode(userStatus);
						
			if(1==userStatus){
				userBean.setUserStatus(AfConstants.STATUS_ACTIVE);
				userBean.setUserStatusClass(AfConstants.STATUS_CLASS_GREEN);
				
			}
			if(0==userStatus){
				userBean.setUserStatus(AfConstants.STATUS_IN_ACTIVE);
				userBean.setUserStatusClass(AfConstants.STATUS_CLASS_RED);
				
			}
			userList.add(userBean);
		}
		}catch(Exception e){
			logger.error("@@@@ Exception in UserManagementServiceImpl campaignManagement() :"+e);
		}
		return userList;

	}


	@Override
	public Boolean checkRoleName(String roleName) {
		boolean userRoleExist=false;
		try{
			userRoleExist=userManagementDAO.checkRoleName(roleName);
		}catch(Exception e){
			logger.error("@@Exception in checkRoleName", e);
		}
		return userRoleExist;
	}

	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String changePassword(ChangePasswordBean changePasswordBean) {
		String message="";
		AfAdminUser afAdminUser=new AfAdminUser();
		afAdminUser=userManagementDAO.getAdminUserById(changePasswordBean.getUserId());
		if(null!=afAdminUser){
			afAdminUser.setPassword(null!=changePasswordBean.getNewPassword()?changePasswordBean.getNewPassword().trim():"");
			message=userManagementDAO.updateUser(afAdminUser);
			if(message=="success")
			message="Your password is updated successfully";
		}
		
		return message;
	}


	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String createRole(AdminRolesBean adminRolesBean, HttpServletRequest request) {
		String roleName="",description="",roleStatus="", permissionStatus="";
		AfAdminRoles afAdminRoles=new AfAdminRoles();
		Set<AfAdminRolePermissions> afAdminRolePermissionses = new HashSet<AfAdminRolePermissions>(0);
		List<AfPermissionValues> listAfPermissionValues=null;
		String oldOrNewVal = "";
		

		try{
		roleName=adminRolesBean.getRoleName();
		roleName=(null!=roleName)?roleName.trim():"";
		afAdminRoles.setRoleName(roleName);
		description=adminRolesBean.getDescription();
		description=(null!=description)?description.trim():"";
		afAdminRoles.setDescription(description);
		afAdminRoles.setStatus(AfConstants.STATUS_ACTIVE_VALUE);
		afAdminRoles.setCreatedTime(new Date());
		afAdminRoles.setCreatedBy(adminRolesBean.getUserId());
		String permissionName = "";
		String a = "";
		roleStatus=userManagementDAO.createRole(afAdminRoles);
		if(roleStatus.equalsIgnoreCase("success")){
			listAfPermissionValues= commonDAO.getPermissionValues();
			Integer permissionId=null;
			String[] rolePermissionIds=adminRolesBean.getRolePermisionId();
			for(AfPermissionValues  permissions:listAfPermissionValues){
				permissionId=permissions.getPermissionId();
				permissionName = permissions.getPermissionName();
				AfAdminRolePermissions afAdminRolePermissions=new AfAdminRolePermissions();
					String action="";
					
					afAdminRolePermissions.setAfAdminRoles(afAdminRoles);
					afAdminRolePermissions.setAfPermissionValues(permissions);
					action +=permissionId;
					if(Arrays.asList(rolePermissionIds).contains(action)){
						a += permissionName+" ,";
						afAdminRolePermissions.setPermissionStatus(AfConstants.CHECKBOX_STATUS_CHECKED);
					}else { 
						afAdminRolePermissions.setPermissionStatus(AfConstants.CHECKBOX_STATUS_UNCHECKED);
					}
					afAdminRolePermissions.setCreatedTime(new Date());
					afAdminRolePermissions.setCreatedBy(adminRolesBean.getUserId());
					afAdminRolePermissionses.add(afAdminRolePermissions);
					
					permissionStatus=userManagementDAO.createAdminRolePermission(afAdminRolePermissions);
			}
			
			if("success".equalsIgnoreCase(permissionStatus)) {
				String ipAddress = request.getHeader("X-FORWARDED-FOR");
				if(ipAddress == null) {
					ipAddress = request.getRemoteAddr();
				}
				ipAddress = (null != ipAddress) ? ipAddress.trim() : "";
				AfUserActivityLog afPrtActLog = new AfUserActivityLog();
				oldOrNewVal = "Role Name :"+adminRolesBean.getRoleName() +" | "+" Description : "+adminRolesBean.getDescription() +" | "+"Permission: "+a;
				afPrtActLog.setUserNewFieldValue(oldOrNewVal);
				afPrtActLog.setUserOldFieldValue("NA");
				afPrtActLog.setUserActivity("Add New Role ");
				afPrtActLog.setUserActCreatedDateTime(new Date());
				afPrtActLog.setUserActIpVal(ipAddress);
				afPrtActLog.setUserActModule("Role Management");
				afPrtActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
				
				userActLogDao.createUserActLog(afPrtActLog);
				}
			
			
		}
		}catch(Exception e){
			logger.error("@@@@ Exception in UserManagementServiceImpl createRole() :"+e);	
			return "failure";
		}
		
			return permissionStatus;
	
	}
	
	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public void updateRole(AdminRolesBean adminRolesBean, HttpServletRequest request) {
		String roleName="",description="";
		AfAdminRoles afAdminRoles=null;
		Set<AfAdminRolePermissions> afAdminRolePermissionses = new HashSet<AfAdminRolePermissions>();
		List<AfPermissionValues> listAfPermissionValues=new ArrayList<AfPermissionValues>();
		String status = AfConstants.SUCCESS;
		Set<AfAdminRolePermissions> afAdminRolePermissionsOld=null;
		String replaceInOld = "";
		String replaceInNew ="";
		AfAdminRoles  afAdminRolesOld = null;
		StringBuffer sbOld =new StringBuffer();
		StringBuffer sbNew =new StringBuffer();
		
		try{
			afAdminRoles=userManagementDAO.getRoleById(adminRolesBean.getRoleId()+"");
			afAdminRolesOld = (AfAdminRoles) afAdminRoles.clone();
			afAdminRolePermissionsOld = afAdminRoles.getAfAdminRolePermissionses();
			for(AfAdminRolePermissions adminRolePermissions : afAdminRolePermissionsOld){
				if(adminRolePermissions.getPermissionStatus() == 1)
				sbOld = sbOld.append(adminRolePermissions.getAfPermissionValues().getPermissionName()+",");
			}
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in UserManagementServiceImpl updateRole() :"+e);
		}
		if(null!=afAdminRoles){
			roleName=adminRolesBean.getRoleName();
			roleName=(null!=roleName)?roleName.trim():"";
			afAdminRoles.setRoleName(roleName);
			
			description=adminRolesBean.getDescription();
			description=(null!=description)?description.trim():"";
			afAdminRoles.setDescription(description);
			
			afAdminRoles.setStatus(AfConstants.STATUS_ACTIVE_VALUE);
			
			afAdminRoles.setModifiedTime(new Date());
			afAdminRoles.setModifiedBy(adminRolesBean.getUserId());
			String permissionName = "";
			try{
				String[] rolePermissionIds=adminRolesBean.getRolePermisionId();
				
				afAdminRolePermissionses=afAdminRoles.getAfAdminRolePermissionses();	
				

				if(null!=afAdminRolePermissionses){
					for(AfAdminRolePermissions permissions:afAdminRolePermissionses){
						String action=permissions.getAfPermissionValues().getPermissionId()+"";
						permissionName =  permissions.getAfPermissionValues().getPermissionName();
						if(Arrays.asList(rolePermissionIds).contains(action)){
							sbNew.append(permissionName+",");
							permissions.setPermissionStatus(AfConstants.CHECKBOX_STATUS_CHECKED);
						}else {
							permissions.setPermissionStatus(AfConstants.CHECKBOX_STATUS_UNCHECKED);
						}
						afAdminRolePermissionses.add(permissions);
					}
					afAdminRoles.setAfAdminRolePermissionses(afAdminRolePermissionses);
				}
				
				
				
				if (null!=afAdminRolePermissionses&&afAdminRolePermissionses.size()==0){
				
					afAdminRolePermissionses = new HashSet<AfAdminRolePermissions>(0);
					listAfPermissionValues= commonDAO.getPermissionValues();
					Integer permissionId=null;
					for(AfPermissionValues  permissions:listAfPermissionValues){
						permissionId=permissions.getPermissionId();
						permissionName = permissions.getPermissionName();
						
							String action="";
							AfAdminRolePermissions afAdminRolePermissions=new AfAdminRolePermissions();
							afAdminRolePermissions.setAfAdminRoles(afAdminRoles);
							afAdminRolePermissions.setAfPermissionValues(permissions);
							action +=permissionId;
							if(Arrays.asList(rolePermissionIds).contains(action)){
								afAdminRolePermissions.setPermissionStatus(AfConstants.CHECKBOX_STATUS_CHECKED);
							}else {
								afAdminRolePermissions.setPermissionStatus(AfConstants.CHECKBOX_STATUS_UNCHECKED);
							}
							afAdminRolePermissions.setPermissionStatus(AfConstants.CHECKBOX_STATUS_UNCHECKED);
							afAdminRolePermissions.setCreatedTime(new Date());
							afAdminRolePermissions.setCreatedBy(adminRolesBean.getUserId());
							afAdminRolePermissionses.add(afAdminRolePermissions);
							
					}
					
				}
				
				
				afAdminRoles.setAfAdminRolePermissionses(afAdminRolePermissionses);
				status = userManagementDAO.updateRole(afAdminRoles);
				if("success".equalsIgnoreCase(status)) {
					String ipAddress = request.getHeader("X-FORWARDED-FOR");
					if (ipAddress == null) {
						ipAddress = request.getRemoteAddr();
					}
					AfUserActivityLog updateUsActLog = new AfUserActivityLog();
					
						updateUsActLog.setUserActivity("Update Role :"+afAdminRoles.getRoleName());
						updateUsActLog.setUserActCreatedDateTime(new Date());
						updateUsActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
						updateUsActLog.setUserActIpVal(ipAddress);
						updateUsActLog.setUserActModule("Role Management");
						
						if(!afAdminRoles.getRoleName().equalsIgnoreCase(afAdminRolesOld.getRoleName())){
							replaceInOld=replaceInOld+"|Role Name :"+afAdminRolesOld.getRoleName();
						    replaceInNew=replaceInNew+"|Role Name  :"+afAdminRoles.getRoleName();
						}
						if(!afAdminRoles.getDescription().equalsIgnoreCase(afAdminRolesOld.getDescription())){
							replaceInOld=replaceInOld+"|Description:"+afAdminRolesOld.getDescription();
							replaceInNew=replaceInNew+"|Description:"+afAdminRoles.getDescription();
						}
						if(!sbNew.equals(sbOld)){
							replaceInOld=replaceInOld+"|Permission:"+sbOld;
							replaceInNew=replaceInNew+"|Permission:"+sbNew;
						}
						if(replaceInNew.isEmpty() && replaceInOld.isEmpty()){
						 replaceInOld=replaceInOld+"|ModifiedTime:"+simpleDateFormat.format(afAdminRolesOld.getModifiedTime());
	                     replaceInNew=replaceInNew+"|ModifiedTime:"+simpleDateFormat.format(afAdminRoles.getModifiedTime());
						}
						updateUsActLog.setUserNewFieldValue(replaceInNew);
			    		updateUsActLog.setUserOldFieldValue(replaceInOld);
						userActLogDao.updateUserLog(updateUsActLog);
				}
			}catch(Exception e){
				logger.error("@@@@ Exception in UserManagementServiceImpl updateRole() :"+e);
			}
		}
		
		
	}
	

	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String deleteRole(Integer roleId, HttpServletRequest request) {
		String deleteStatus="";
		AfAdminRoles roles=null;
		String oldOrNewVal = "";
		Set<AfAdminRolePermissions> afAdminRolePermissionsOld=null;
		AdminRolesBean adminRolesBean = new AdminRolesBean();
		adminRolesBean.setUserId((Integer)request.getSession().getAttribute("userId"));
		StringBuffer sbOld =new StringBuffer();
		try{
			
			deleteStatus=userManagementDAO.deleteRole(roleId);
			roles = userManagementDAO.getRoleById(roleId.toString());
			afAdminRolePermissionsOld = roles.getAfAdminRolePermissionses();
			for(AfAdminRolePermissions adminRolePermissions : afAdminRolePermissionsOld){
				if(adminRolePermissions.getPermissionStatus() == 1)
				sbOld = sbOld.append(adminRolePermissions.getAfPermissionValues().getPermissionName()+",");
			}
			
			String ipAddress = request.getHeader("X-FORWARDED-FOR");
			if (ipAddress == null) {
				ipAddress = request.getRemoteAddr();
			}
			
			
			String status1=roles.getStatus()+"";
			status1 = status1.replaceAll("0","InActive");
			status1 = status1.replaceAll("1", "Active");
			AfUserActivityLog updateUsActLog = new AfUserActivityLog();
			oldOrNewVal="Role Name : "+roles.getRoleName()+" | "+"Description: "+roles.getDescription()+" | "+
					""+"Permission values: "+sbOld+" |";
			updateUsActLog.setUserActivity("Delete Role :"+roles.getRoleName());
	    	updateUsActLog.setUserActCreatedDateTime(new Date());
	    	updateUsActLog.setUserNewFieldValue("NA");
	    	updateUsActLog.setUserActIpVal(ipAddress);
	    	updateUsActLog.setUserActModule("Role Management");
	    	updateUsActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
	    	updateUsActLog.setUserOldFieldValue(oldOrNewVal);
	    	userActLogDao.updateUserLog(updateUsActLog);
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in UserManagementServiceImpl deleteRole() :"+e);
		}
		return deleteStatus;
		
	}


	@Override
	public Map<AfPermissionValues, Map<Byte, String>> permissionActionsForvalues(List<AfPermissionValues> permissionValues,Map<Byte, String> permissions) {
		Map<AfPermissionValues, Map<Byte, String>> permissionActionsForvalues=new LinkedHashMap<AfPermissionValues, Map<Byte,String>>();
		for(AfPermissionValues afPermissionValues:permissionValues){
			Map<Byte, String> permissionActions=new LinkedHashMap<Byte, String>();
			permissionActions.putAll(permissions);
			if(1==afPermissionValues.getPermissionId()||4==afPermissionValues.getPermissionId()||5==afPermissionValues.getPermissionId()||6==afPermissionValues.getPermissionId()||7==afPermissionValues.getPermissionId()){
				permissionActions.remove(AfConstants.PERMISSION_ACTIONS_EXPORT);
				permissionActionsForvalues.put(afPermissionValues, permissionActions);
			}
			else if(2==afPermissionValues.getPermissionId()||3==afPermissionValues.getPermissionId()){
				permissionActions.remove(AfConstants.PERMISSION_ACTIONS_DELETE);
				permissionActions.remove(AfConstants.PERMISSION_ACTIONS_EXPORT);
				permissionActionsForvalues.put(afPermissionValues, permissionActions);
			}
			
			else if(8==afPermissionValues.getPermissionId()||9==afPermissionValues.getPermissionId()){
				permissionActions.remove(AfConstants.PERMISSION_ACTIONS_ADD);
				permissionActions.remove(AfConstants.PERMISSION_ACTIONS_EXPORT);
				permissionActionsForvalues.put(afPermissionValues, permissionActions);
			}
			
			else{
				permissionActions.remove(AfConstants.PERMISSION_ACTIONS_ADD);
				permissionActions.remove(AfConstants.PERMISSION_ACTIONS_EDIT);
				permissionActions.remove(AfConstants.PERMISSION_ACTIONS_DELETE);
				permissionActionsForvalues.put(afPermissionValues, permissionActions);
			}
			 
			permissionActions=null;
		}
		return permissionActionsForvalues;
	}


	@Override
	public AdminRolesBean getRoleById(String roleId) {
		AfAdminRoles afAdminRoles=null;
		String roleName="",description="";
		AdminRolesBean adminRolesBean=new AdminRolesBean();
		Set<AfAdminRolePermissions> afAdminRolePermissionses=null;
		List<String> checkedActions=new ArrayList<String>();
		try{
			afAdminRoles=userManagementDAO.getRoleById(roleId);
			if(null!=afAdminRoles){
				roleName=afAdminRoles.getRoleName();
				roleName=(null!=roleName)?roleName.trim():"";
				adminRolesBean.setRoleName(roleName);
				
				adminRolesBean.setRoleId(afAdminRoles.getRoleId());
				description=afAdminRoles.getDescription();
				description=(null!=description)?description.trim():"";
				adminRolesBean.setDescription(description);
				
				afAdminRolePermissionses=afAdminRoles.getAfAdminRolePermissionses();
				if(null!=afAdminRolePermissionses){
					for(AfAdminRolePermissions permissions:afAdminRolePermissionses){
						if(permissions.getPermissionStatus()==AfConstants.CHECKBOX_STATUS_CHECKED){
							checkedActions.add(permissions.getAfPermissionValues().getPermissionId()+"");
						}
					}
					adminRolesBean.setRolePermisionId(checkedActions.toArray(new String[checkedActions.size()]));
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return adminRolesBean;
	}


	@Override
	public String[] getRolePermissionsById(String roleId) {
		return userManagementDAO.getRolePermissionsById(roleId);
	}
	


}


