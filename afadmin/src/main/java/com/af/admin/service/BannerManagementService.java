package com.af.admin.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;

import com.af.admin.beans.AdminUserBean;
import com.af.admin.beans.BannerBean;


public interface BannerManagementService {
	
	public Map<String, List<BannerBean>> getBanners();	
	public String createBanner(BannerBean bannerBean,HttpServletRequest request);
	public String updateBanner(JSONObject userJson,HttpServletRequest request);
	public String saveOrder(String reOrder,HttpServletRequest request);
	public String deleteBanner(Integer bnrNo,HttpServletRequest request);
	public boolean checkBannerID(String bannerID);
	public boolean checkUtmParameter(JSONObject userJson);
	public List<BannerBean> getBannerListPagination(int length, int start);
	public Long getBannerListPaginationCount();
}
