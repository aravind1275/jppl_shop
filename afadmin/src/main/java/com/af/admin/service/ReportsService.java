package com.af.admin.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;

import com.af.admin.beans.BuisnessReportsBean;
import com.af.admin.beans.InvalidCsvReprot;
import com.af.admin.beans.JPMilesStatusUpdateBean;
import com.af.admin.beans.MissingReportBean;
import com.af.admin.beans.zeroCsvReport;

public interface ReportsService {
	
	public List<BuisnessReportsBean> getBuisnessReportsList(String fromDate, String toDate,String partnerName,String jpNumber,String status,String statusType,int length,int start);
	
	public List<BuisnessReportsBean> getFinanceReportsList(String fromDate, String toDate,String partnerName,String jpNumber,String status,String statusType,int length,int start);
	
	public List<BuisnessReportsBean> getAnalyticsReportsList(String fromDate, String toDate,String partnerName,String jpNumber,String status,String statusType,int length,int start);
	
	public List<BuisnessReportsBean> getServiceCenterReportsList(String fromDate, String toDate,String partnerName,String jpNumber,String status,String statusType,int length,int start);

	public List<BuisnessReportsBean> getNewProductsReportList(BuisnessReportsBean newProductsReport);
	
	public List<BuisnessReportsBean> getMappedProductsReportList(BuisnessReportsBean mappedProductsReport);

	public List<BuisnessReportsBean> getPartnersByProducts();
	
	public List<BuisnessReportsBean> getBrandsByProducts(String toDate, String fromDate);
	
	public List<BuisnessReportsBean> getdailyMilesReportsList(String toDate, String fromDate,String partnerName,String jpNumber);
	
	public String downloadXmlFile( String orderId,String type);
	
	public List<BuisnessReportsBean> getZeroJpMilesReport(String fromDate, String toDate,String partnerName,String jpNumber,String status,String statusType);

	public String updateCsvReport(List<zeroCsvReport> reportList);

	public List<BuisnessReportsBean> getqueingLogicReportList(String fromDate, String toDate, String prtNo,
			String jpNumber);
	
	public List<BuisnessReportsBean> getExportBuisnessReportsList(String fromDate, String toDate,String partnerName,String jpNumber,String status,String statusType);
	
	public List<BuisnessReportsBean> getExportFinanceReportsList(String fromDate, String toDate,String partnerName,String jpNumber,String status,String statusType);
	
	public List<BuisnessReportsBean> getExportAnalyticsReportsList(String fromDate, String toDate,String partnerName,String jpNumber,String status,String statusType);
	
	public List<BuisnessReportsBean> getExportServiceCenterReportsList(String fromDate, String toDate,String partnerName,String jpNumber,String status,String statusType);


	public List<BuisnessReportsBean> getExportInvalidReportsList(String fromDate, String toDate, String prtNo, String jpNumber);

	public List<BuisnessReportsBean> getInvalidReportsList(String fromDate, String toDate, String prtNo,
			String jpNumber, int length, int start);

	public String updateInvalidCsvReport(List<InvalidCsvReprot> reportList);

	public List<BuisnessReportsBean> getJPMilesStatusUpdateWithJPNumber(String fromDate, String toDate,String prtNo, String jpNumber, String status,  String statusType, int length, int start);

	public String jpplCreditUpdate(JSONObject userJson,HttpServletRequest request);
	
	public List<BuisnessReportsBean> getAllExportBuisnessReportsList(String fromDate, String toDate, String jpNumber,
			String status, String statusType);

	public List<BuisnessReportsBean> getAllExportServiceCenterReportsList(String fromDate, String toDate,
			String jpNumber, String status, String statusType);
	
	public List<BuisnessReportsBean> getAllExportFinanceReportsList(String fromDate, String toDate,
			String jpNumber, String status, String statusType);
	
	public List<BuisnessReportsBean> getAllZeroJpMilesReport(String fromDate, String toDate,
			String jpNumber, String status, String statusType);
	
	public List<BuisnessReportsBean> getAllExportAnalyticsReportsList(String fromDate, String toDate,
			String jpNumber, String status, String statusType);
	



	
}
