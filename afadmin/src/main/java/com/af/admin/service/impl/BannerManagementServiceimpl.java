package com.af.admin.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.af.admin.DAO.BannerManagementDAO;
import com.af.admin.DAO.PartnerManagementDAO;
import com.af.admin.DAO.UserActivityLogDAO;
import com.af.admin.beans.BannerBean;
import com.af.admin.constants.AfConstants;
import com.af.admin.entity.AfBanner;
import com.af.admin.entity.AfPartner;
import com.af.admin.entity.AfUserActivityLog;
import com.af.admin.service.BannerManagementService;
import com.af.admin.utils.AdminUserActivityUtil;
import com.af.admin.utils.ServiceProviderUtil;

@Component
@Transactional(readOnly=true)
public class BannerManagementServiceimpl implements BannerManagementService {
	private Logger logger = LogManager.getLogger(BannerManagementServiceimpl.class.getName());
	
	@Autowired
	BannerManagementDAO bannerManagementDAO;
	
	@Autowired
	PartnerManagementDAO partnerManagementDAO;
	
	@Autowired
	UserActivityLogDAO userActLogDao;
	
	@Autowired
	AdminUserActivityUtil adminUserActivityUtil;
	
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
	
	@Override
	public Map<String, List<BannerBean>> getBanners() {
		List<AfBanner> afBannerList=null;
		Map<String, List<BannerBean>> bannerList = new HashMap<>();
		List<BannerBean> cBannerList=new ArrayList<>();
		List<BannerBean> sBannerList=new ArrayList<>();
		int bannerStatus, bannerSections;
		AfPartner afPartner=null;
		String dateStr = "";
		Date dt = null;
		try{
			afBannerList=bannerManagementDAO.getBanners();
			for(AfBanner afBanner:afBannerList){
				BannerBean bannerBean=new BannerBean();

				bannerBean.setBannerNo(afBanner.getBnrNo());
				bannerBean.setBannerId(afBanner.getBannerId());
				bannerBean.setBannerName(afBanner.getBannerName());
				bannerBean.setBannerImageUrl(afBanner.getBannerImageUrl());
				bannerBean.setBannerLandingUrl(afBanner.getBannerLandingUrl());
				bannerSections = afBanner.getBannerSections();
				bannerBean.setBannerSectionsCode(afBanner.getBannerSections());
				bannerStatus=afBanner.getBannerStatus();
				bannerBean.setBannerStatusCode(bannerStatus);
				String[] source;
				String[] medium;
				String[] campaign;
				if (afBanner.getUtmSource() != null && afBanner.getUtmSource().contains("=")) {
					source = afBanner.getUtmSource().split("=");
					bannerBean.setUtmSourceName(source[0]);
					if(source.length>1)
					bannerBean.setUtmSourceValue(source[1]);
				}
				if (afBanner.getUtmMedium() != null && afBanner.getUtmMedium().contains("=")) {
					medium = afBanner.getUtmMedium().split("=");
					bannerBean.setUtmMediumName(medium[0]);
					if(medium.length>1)
					bannerBean.setUtmMediumValue(medium[1]);
				}
				if (afBanner.getUtmCampaign() != null && afBanner.getUtmCampaign().contains("=")) {
					campaign = afBanner.getUtmCampaign().split("=");
					bannerBean.setUtmCampaignName(campaign[0]);
					if(campaign.length>1)
					bannerBean.setUtmCampaignValue(campaign[1]);
				}

				if(afBanner.getAfPartner() != null){
					bannerBean.setPartNo(String.valueOf(afBanner.getAfPartner().getPrtNo()));
					afPartner = partnerManagementDAO.getPartnerById(afBanner.getAfPartner().getPrtNo());
					bannerBean.setPartnerName(String.valueOf(afPartner.getPartnerName()));
				}
				dt = afBanner.getBannerStartDate();
				dateStr = ServiceProviderUtil.getInstance().getFormattedDate(afBanner.getBannerStartDate(),"dd/MM/yyyy");
				if(dt != null && !dt.equals("")){
					bannerBean.setFromDate(dateStr);
				}
				else
					bannerBean.setFromDate("");
				dt = afBanner.getBannerEndDate();
				dateStr = ServiceProviderUtil.getInstance().getFormattedDate(afBanner.getBannerEndDate(),"dd/MM/yyyy");
				if(dt != null && !dt.equals("")){
					bannerBean.setToDate(dateStr);
				}
				else
					bannerBean.setToDate("");

				if(0==bannerSections)
					bannerBean.setBannerSections(AfConstants.BANNER_SECTIONS_C);											
				if(1==bannerSections)
					bannerBean.setBannerSections(AfConstants.BANNER_SECTIONS_S);								

				if(1==bannerStatus){
					bannerBean.setBannerStatus(AfConstants.STATUS_ACTIVE);
					bannerBean.setBannerStatusClass(AfConstants.STATUS_CLASS_GREEN);				
				}
				if(0==bannerStatus){
					bannerBean.setBannerStatus(AfConstants.STATUS_IN_ACTIVE);
					bannerBean.setBannerStatusClass(AfConstants.STATUS_CLASS_RED);				
				}
				if(0==bannerSections)
					cBannerList.add(bannerBean);
				if(1==bannerSections)
					sBannerList.add(bannerBean);
			}
			bannerList.put("cbannerlist", cBannerList);
			bannerList.put("sbannerlist", sBannerList);
		}catch(Exception e){
			logger.error("@@@@ Exception in BannerManagementServiceimpl at getBanners() :", e);
		}
		return bannerList;
	}
	
	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String createBanner(BannerBean bannerBean,HttpServletRequest request) {
		AfBanner afBanner=new AfBanner();
		String status="";
		Date startDate = null;
		Date endDate = null;
		String oldOrNewVal = "";
		try{
		afBanner.setBannerId(bannerBean.getBannerId());
		afBanner.setBannerName(bannerBean.getBannerName());
		afBanner.setBannerImageUrl(bannerBean.getBannerImageUrl().replaceAll(",",""));
		afBanner.setBannerLandingUrl(bannerBean.getBannerLandingUrl());
		String utmSource = (bannerBean.getUtmSourceName()+"="+bannerBean.getUtmSourceValue()).equalsIgnoreCase("=")?"":(bannerBean.getUtmSourceName()+"="+bannerBean.getUtmSourceValue());
		String utmMedium = (bannerBean.getUtmMediumName()+"="+bannerBean.getUtmMediumValue()).equalsIgnoreCase("=")?"":(bannerBean.getUtmMediumName()+"="+bannerBean.getUtmMediumValue());
		String utmCampaign = (bannerBean.getUtmCampaignName()+"="+bannerBean.getUtmCampaignValue()).equalsIgnoreCase("=")?"":(bannerBean.getUtmCampaignName()+"="+bannerBean.getUtmCampaignValue());
		afBanner.setUtmSource(utmSource);
		afBanner.setUtmCampaign(utmCampaign);
		afBanner.setUtmMedium(utmMedium);
		
		if(bannerBean.getPartNo().length() > 0){
			afBanner.setAfPartner(partnerManagementDAO.getPartnerByPartnerNO(bannerBean.getPartNo()));
		}
		else{
			afBanner.setAfPartner(null);
		}
		
		afBanner.setBannerSections((byte)bannerBean.getBannerSectionsCode());
		afBanner.setBannerPosition((byte)0);
		
		
		afBanner.setBannerStatus((byte)bannerBean.getBannerStatusCode());		
		afBanner.setCreatedBy(1);
		afBanner.setCreatedTime(new Date());
		afBanner.setStatus((byte) 1);
		startDate = ServiceProviderUtil.getInstance().getDateFromString(bannerBean.getFromDate(), "dd/MM/yyyy");
		endDate = ServiceProviderUtil.getInstance().getDateFromString(bannerBean.getToDate(), "dd/MM/yyyy");
		
		afBanner.setBannerStartDate(startDate);
		afBanner.setBannerEndDate(endDate);
		
		String bannerSections=bannerBean.getBannerSectionsCode()+"";
		bannerSections = bannerSections.replaceAll("0","Carousel");
		bannerSections = bannerSections.replaceAll("1", "Static");
		
		
		String status1=bannerBean.getBannerStatusCode()+"";
		status1 = status1.replaceAll("0","InActive");
		status1 = status1.replaceAll("1", "Active");
		status=bannerManagementDAO.createBanner(afBanner);
		if("success".equalsIgnoreCase(status)){
			String ipAddress = request.getHeader("X-FORWARDED-FOR");
			if(ipAddress == null) {
				ipAddress = request.getRemoteAddr();
			}
			String partnerName = (afBanner.getAfPartner() == null)? "NA" : afBanner.getAfPartner().getPartnerName();
			ipAddress = (null != ipAddress) ? ipAddress.trim() : "";
			AfUserActivityLog afPrtActLog=new AfUserActivityLog();
			oldOrNewVal=" Partners : "+partnerName+" | "+"Banner ID : "+bannerBean.getBannerId()+" | "+
					""+"Banner Name: "+bannerBean.getBannerName()+"| "+"Landing URL: "+bannerBean.getBannerLandingUrl()+" |"+" Banner Sections: "+bannerSections+" |"
							+ " "+"Banner Image : "+bannerBean.getBannerImageUrl()+" |"+"Banner Status : "+status1+" |"
									+ ""+"Start Date : "+afBanner.getBannerStartDate()+" | "+
					"End Date : "+afBanner.getBannerEndDate()+" | ";
							
		
					afPrtActLog.setUserNewFieldValue(oldOrNewVal);
					afPrtActLog.setUserOldFieldValue("NA");
					afPrtActLog.setUserActivity("Create Banner");
					afPrtActLog.setUserActCreatedDateTime(new Date());
					afPrtActLog.setUserActIpVal(ipAddress);
					afPrtActLog.setUserActModule("Banner Management");
					afPrtActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
					
					userActLogDao.createUserActLog(afPrtActLog);
		}
		}catch(Exception e){
			logger.error("@@@@ Exception in BannerManagementServiceimpl at createBanner() :", e);
			return "failure";
		}
		return status;
	}
	
	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String updateBanner(JSONObject userJson,HttpServletRequest request) {
		Integer bnrNo = -1;		
		String status="";
		Date startDate = null;
		Date endDate = null;
		AfBanner afBannerOld=null;
		String replaceInOld="";
		String replaceInNew="";
		AfBanner afBanner = null;
		try {
			bnrNo = userJson.getInt("bnrNo");
			
			afBanner = bannerManagementDAO.getBannerById(bnrNo);
			afBannerOld=(AfBanner) afBanner.clone();
			afBanner.setBannerId(userJson.getString("bannerId"));
			afBanner.setBannerName(userJson.getString("bannerName")); 
			afBanner.setBannerSections((byte)userJson.getInt("bannerSections"));
			afBanner.setBannerImageUrl(userJson.getString("editBannerImageUrl"));
			afBanner.setBannerLandingUrl(userJson.getString("bannerLandingUrl"));
			afBanner.setBannerStatus((byte) userJson.getInt("bannerStatus"));
			String utmSource = (userJson.getString("utmSourceName")+"="+userJson.getString("utmSourceValue")).equalsIgnoreCase("=")?"":(userJson.getString("utmSourceName")+"="+userJson.getString("utmSourceValue"));
			String utmMedium = (userJson.getString("utmMediumName")+"="+userJson.getString("utmMediumValue")).equalsIgnoreCase("=")?"":(userJson.getString("utmMediumName")+"="+userJson.getString("utmMediumValue"));
			String utmCampaign = (userJson.getString("utmCampaignName")+"="+userJson.getString("utmCampaignValue")).equalsIgnoreCase("=")?"":(userJson.getString("utmCampaignName")+"="+userJson.getString("utmCampaignValue"));
			startDate = ServiceProviderUtil.getInstance().getDateFromString(userJson.getString("editFromDate"), "dd/MM/yyyy");
			endDate = ServiceProviderUtil.getInstance().getDateFromString(userJson.getString("editToDate"), "dd/MM/yyyy");
			afBanner.setBannerStartDate(startDate);
			afBanner.setBannerEndDate(endDate);
			
			afBanner.setUtmSource(utmSource);
			afBanner.setUtmCampaign(utmCampaign);
			afBanner.setUtmMedium(utmMedium);
			afBanner.setModifiedBy(1);
			afBanner.setModifiedTime(new Date());
			String prtNo = userJson.getString("prtNo");
			
			if(!"".equals(prtNo)){
				afBanner.setAfPartner(partnerManagementDAO.getPartnerByPartnerNO(prtNo));
			} else {
				afBanner.setAfPartner(null);
			}
			
	     	String statusInactive=	afBannerOld.getBannerStatus()==0?"InActive":"Active";
	     	String statusActive=afBanner.getBannerStatus()==0?"InActive":"Active";
	     	
			status=bannerManagementDAO.updateBanner(afBanner);
			if("success".equalsIgnoreCase(status)){
				String ipAddress = request.getHeader("X-FORWARDED-FOR");
				if (ipAddress == null) {
					ipAddress = request.getRemoteAddr();
				}
				AfUserActivityLog updateUsActLog = new AfUserActivityLog();
				new LinkedHashMap<>();
				
					updateUsActLog.setUserActivity("Update Banner");
					updateUsActLog.setUserActCreatedDateTime(new Date());
					updateUsActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
					updateUsActLog.setUserActIpVal(ipAddress);
					updateUsActLog.setUserActModule("Banner Management");
					String prtNameNew = afBanner.getAfPartner() == null ?"":afBanner.getAfPartner().getPartnerName();
					String prtNameOld = afBannerOld.getAfPartner() == null ?"":afBannerOld.getAfPartner().getPartnerName();
					if(!prtNameNew.equalsIgnoreCase(prtNameOld)){
						replaceInOld=replaceInOld+"|Partner Name :"+afBannerOld.getAfPartner().getPartnerName();
					    replaceInNew=replaceInNew+"|Partner Name :"+afBanner.getAfPartner().getPartnerName();
					}
					if(!afBanner.getBannerId().equalsIgnoreCase(afBannerOld.getBannerId())){
						replaceInOld=replaceInOld+"|Banner Id:"+afBannerOld.getBannerId();
						replaceInNew=replaceInNew+"|Banner Id:"+afBanner.getBannerId();
					}
					if(!afBanner.getBannerName().equalsIgnoreCase(afBannerOld.getBannerName())){
						replaceInOld=replaceInOld+"|Banner Name:"+afBannerOld.getBannerName();
						replaceInNew=replaceInNew+"|Banner Name:"+afBanner.getBannerName();
					}
					if(!afBanner.getBannerLandingUrl().equalsIgnoreCase(afBannerOld.getBannerLandingUrl())){
						replaceInOld=replaceInOld+"|BannerLandUrl:"+afBannerOld.getBannerLandingUrl();
					     replaceInNew=replaceInNew+"|BannerLandUrl:"+afBanner.getBannerLandingUrl();
					}
					if(!afBanner.getBannerImageUrl().equalsIgnoreCase(afBannerOld.getBannerImageUrl())){
						replaceInOld=replaceInOld+"|BannerImageUrl:"+afBannerOld.getBannerImageUrl();
						replaceInNew=replaceInNew+"|BannerImageUrl:"+afBanner.getBannerImageUrl();
					}
					if(afBanner.getBannerStatus()!=afBannerOld.getBannerStatus()){
						replaceInOld=replaceInOld+"|BannerStatus:"+statusInactive;
						replaceInNew=replaceInNew+"|BannerStatus:"+statusActive;
					}
					if(!afBanner.getBannerStartDate().equals(afBannerOld.getBannerStartDate())){
						replaceInOld=replaceInOld+"|BannerStartDate:"+simpleDateFormat.format(afBannerOld.getBannerStartDate());
						replaceInNew=replaceInNew+"|BannerStartDate:"+simpleDateFormat.format(afBanner.getBannerStartDate());
					}
					
					if(!afBanner.getBannerEndDate().equals(afBannerOld.getBannerEndDate())){
						replaceInOld=replaceInOld+"|BannerEndDate:"+simpleDateFormat.format(afBannerOld.getBannerEndDate());
						replaceInNew=replaceInNew+"|BannerEndDate:"+simpleDateFormat.format(afBanner.getBannerEndDate());
					}
					if(replaceInNew.isEmpty() && replaceInOld.isEmpty()) {
                        replaceInOld=replaceInOld+"|ModifiedTime:"+simpleDateFormat.format(afBannerOld.getModifiedTime());
                        replaceInNew=replaceInNew+"|ModifiedTime:"+simpleDateFormat.format(afBanner.getModifiedTime());
                    }
					updateUsActLog.setUserNewFieldValue(replaceInNew);
		    		updateUsActLog.setUserOldFieldValue(replaceInOld);
					userActLogDao.updateUserLog(updateUsActLog);
				
				
			}
				
		} catch(Exception e) {
			e.printStackTrace();
			logger.error("@@@@ Exception in UserManagementServiceImpl at updateBanner() :", e);
		}
		return status;
	}
	
	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String saveOrder(String reOrder,HttpServletRequest request) {
		StringTokenizer reOrderTkn = null, reOrderTkn_ = null ;		
		Integer bnrNo = -1;
		int bannerPosition=-1;
		String status="";
		String replaceInOld="";
		String replaceInNew="";
		AfBanner afBannerOld=null;
		try{
			System.out.println("--------> reorder -------> "+reOrder);
			
			reOrderTkn = new StringTokenizer(reOrder,",");
			
			System.out.println("length of token ---> "+reOrderTkn.countTokens());
			
			while(reOrderTkn.hasMoreTokens()){				
				
				reOrderTkn_ = new StringTokenizer(reOrderTkn.nextToken(),"-");

				bnrNo = (int)Integer.parseInt(reOrderTkn_.nextToken());
				System.out.println("banner no ---> "+bnrNo);
				bannerPosition = (int)Integer.parseInt(reOrderTkn_.nextToken());

				AfBanner afBanner=bannerManagementDAO.getBannerById(bnrNo);		
				System.out.println("current banner ---> "+afBanner.getBannerName());
				afBannerOld = (AfBanner)afBanner.clone();
				
				afBanner.setBannerPosition((byte) bannerPosition);
				afBanner.setModifiedBy(1);
				afBanner.setModifiedTime(new Date());
				status=bannerManagementDAO.updateBanner(afBanner);
                                System.out.println("------->"+status);
				if("success".equalsIgnoreCase(status)){
					String ipAddress = request.getHeader("X-FORWARDED-FOR");
					if (ipAddress == null) {
						ipAddress = request.getRemoteAddr();
					}
					AfUserActivityLog updateUsActLog = new AfUserActivityLog();
					Map<String,String> objectCom = new LinkedHashMap<>();
					try{
						updateUsActLog.setUserActivity("Save banner order");
						updateUsActLog.setUserActCreatedDateTime(new Date());
						updateUsActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
						updateUsActLog.setUserActIpVal(ipAddress);
						updateUsActLog.setUserActModule("Banner Management");
						
						objectCom = adminUserActivityUtil.objectComparision(afBanner, afBannerOld);
						replaceInOld=objectCom.get(AfConstants.ACTIVITY_OLD_KEY);
			    		replaceInNew=objectCom.get(AfConstants.ACTIVITY_NEW_KEY);
						updateUsActLog.setUserNewFieldValue(replaceInNew);
			    		updateUsActLog.setUserOldFieldValue(replaceInOld);
						userActLogDao.updateUserLog(updateUsActLog);
			    		
					}
					catch(Exception e){
						e.printStackTrace();
					}
				}
			}												
			
		}
		catch(Exception e){
			logger.error("@@@@ Exception in UserManagementServiceImpl at saveOrder() :", e);
		}
		
		return status;
	}
	
	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String deleteBanner(Integer bnrNo,HttpServletRequest request) {
		String deleteStatus="";
		String oldOrNewVal = "";
		AfBanner afBanner= null;
		
		
		try{
			String ipAddress = request.getHeader("X-FORWARDED-FOR");
			if (ipAddress == null) {
				ipAddress = request.getRemoteAddr();
			}
			afBanner = bannerManagementDAO.getBannerById(bnrNo);
			String status1=afBanner.getBannerStatus()+"";
			status1 = status1.replaceAll("0","InActive");
			status1 = status1.replaceAll("1", "Active");
			
			String BannerSections=afBanner.getBannerSections()+"";
			BannerSections = BannerSections.replaceAll("0","Carousel");
			BannerSections = BannerSections.replaceAll("1", "Static");
			String partnerName = (afBanner.getAfPartner() == null)? "NA" : afBanner.getAfPartner().getPartnerName();

			AfUserActivityLog updateUsActLog = new AfUserActivityLog();
			oldOrNewVal=" Partners : "+partnerName+" | "+"Banner ID : "+afBanner.getBannerId()+" | "+
					""+"Banner Name: "+afBanner.getBannerName()+"| "+"Landing URL: "+afBanner.getBannerLandingUrl()+" |"+" Banner Sections: "+BannerSections+" |"
					+ " "+"Banner Image : "+afBanner.getBannerImageUrl()+" |"+"Banner Status : "+status1+" |"
							+ ""+"Start Date : "+afBanner.getBannerStartDate()+" | "+
			"End Date : "+afBanner.getBannerEndDate()+" | ";
			updateUsActLog.setUserActivity("Banner Delete");
	    	updateUsActLog.setUserActCreatedDateTime(new Date());
	    	updateUsActLog.setUserNewFieldValue("NA");
	    	updateUsActLog.setUserActIpVal(ipAddress);
	    	updateUsActLog.setUserActModule("Banner Management");
	    	updateUsActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
	    	updateUsActLog.setUserOldFieldValue(oldOrNewVal);
	    	userActLogDao.updateUserLog(updateUsActLog);
			deleteStatus=bannerManagementDAO.deleteBanner(bnrNo);
		}catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in BannerManagementServiceImpl at deleteBanner() :", e);
			deleteStatus="failure";
			return deleteStatus;
		}
		return deleteStatus;
	}
	
	@Override
	public boolean checkBannerID(String bannerID) {
		boolean bannerIdExist=false;
		try{
			bannerIdExist=bannerManagementDAO.checkBannerID(bannerID);
			logger.debug(">>>>>>>>checkBannerID service>>>"+bannerIdExist);
		}catch(Exception e){
			logger.error("@@Exception in BannerManagementServiceImpl checkBannerID", e);
		}
		return bannerIdExist;
	}

	@Override
	public boolean checkUtmParameter(JSONObject userJson) {
		boolean bannerIdExist=false;
		try{
			String source=userJson.getString("utmSource").equals("=")?"":userJson.getString("utmSource");
			String medium = userJson.getString("utmMedium").equals("=")?"":userJson.getString("utmMedium");
			String campaign = userJson.getString("utmCampaign").equals("=")?"":userJson.getString("utmCampaign");
			if(source.equals("") && medium.equals("") && campaign.equals("")){
				
			}else{
			bannerIdExist=bannerManagementDAO.checkUtmParameter(source,medium,campaign);
			}
			logger.debug(">>>>>>>>checkBannerID service>>>"+bannerIdExist);
		}catch(Exception e){
			logger.error("@@Exception in BannerManagementServiceImpl checkBannerID", e);
		}
		return bannerIdExist;
	}

	@Override
	public List<BannerBean> getBannerListPagination(int length, int start) {
		List<AfBanner> afBannerList=null;
		List<BannerBean> bannerList = new ArrayList<>();
		List<BannerBean> cBannerList=new ArrayList<>();
		List<BannerBean> sBannerList=new ArrayList<>();
		int bannerStatus, bannerSections;
		AfPartner afPartner=null;
		String dateStr = "";
		Date dt = null;
		try{
//                    int trt=10;
                    int tem=0;
			afBannerList=bannerManagementDAO.getBanners(length,start);
			for(AfBanner afBanner:afBannerList){
                            tem=tem+1;
				BannerBean bannerBean=new BannerBean();

				bannerBean.setBannerNo(afBanner.getBnrNo());
				bannerBean.setBannerId(afBanner.getBannerId());
				bannerBean.setBannerName(afBanner.getBannerName());
				bannerBean.setBannerImageUrl(afBanner.getBannerImageUrl());
				bannerBean.setBannerLandingUrl(afBanner.getBannerLandingUrl());
				bannerSections = afBanner.getBannerSections();
				bannerBean.setBannerSectionsCode(afBanner.getBannerSections());
				bannerStatus=afBanner.getBannerStatus();
				bannerBean.setBannerStatusCode(bannerStatus);
                               
				String[] source;
				String[] medium;
				String[] campaign;
				if (afBanner.getUtmSource() != null && afBanner.getUtmSource().contains("=")) {
					source = afBanner.getUtmSource().split("=");
					bannerBean.setUtmSourceName(source[0]);
					if(source.length>1)
					bannerBean.setUtmSourceValue(source[1]);
				}
				if (afBanner.getUtmMedium() != null && afBanner.getUtmMedium().contains("=")) {
					medium = afBanner.getUtmMedium().split("=");
					bannerBean.setUtmMediumName(medium[0]);
					if(medium.length>1)
					bannerBean.setUtmMediumValue(medium[1]);
				}
				if (afBanner.getUtmCampaign() != null && afBanner.getUtmCampaign().contains("=")) {
					campaign = afBanner.getUtmCampaign().split("=");
					bannerBean.setUtmCampaignName(campaign[0]);
					if(campaign.length>1)
					bannerBean.setUtmCampaignValue(campaign[1]);
				}

				if(afBanner.getAfPartner() != null){
					bannerBean.setPartNo(String.valueOf(afBanner.getAfPartner().getPrtNo()));
					afPartner = partnerManagementDAO.getPartnerById(afBanner.getAfPartner().getPrtNo());
					bannerBean.setPartnerName(String.valueOf(afPartner.getPartnerName()));
				}
				dt = afBanner.getBannerStartDate();
				dateStr = ServiceProviderUtil.getInstance().getFormattedDate(afBanner.getBannerStartDate(),"dd/MM/yyyy");
				if(dt != null && !dt.equals("")){
					bannerBean.setFromDate(dateStr);
				}
				else
					bannerBean.setFromDate("");
				dt = afBanner.getBannerEndDate();
				dateStr = ServiceProviderUtil.getInstance().getFormattedDate(afBanner.getBannerEndDate(),"dd/MM/yyyy");
				if(dt != null && !dt.equals("")){
					bannerBean.setToDate(dateStr);
				}
				else
					bannerBean.setToDate("");

				Date todayDate = new Date();
				SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy");
				todayDate = dateFormatter.parse(dateFormatter.format(new Date() ));


				if((afBanner.getBannerStartDate().compareTo(afBanner.getBannerEndDate()) > 0) || (todayDate.compareTo(afBanner.getBannerEndDate()) > 0)) 
				{
					System.out.println("inside if of date");
					bannerBean.setBannerStatus(AfConstants.STATUS_IN_ACTIVE);
					bannerBean.setBannerStatusClass(AfConstants.STATUS_CLASS_RED);
				}
				else {
					bannerBean.setBannerStatus(AfConstants.STATUS_ACTIVE);
					bannerBean.setBannerStatusClass(AfConstants.STATUS_CLASS_GREEN);
				}
				
				if(0==bannerSections)
					bannerBean.setBannerSections(AfConstants.BANNER_SECTIONS_C);											
				if(1==bannerSections)
					bannerBean.setBannerSections(AfConstants.BANNER_SECTIONS_S);



				// if(0==bannerSections)
				// 	bannerBean.setBannerSections(AfConstants.BANNER_SECTIONS_C);											
				// if(1==bannerSections)
				// 	bannerBean.setBannerSections(AfConstants.BANNER_SECTIONS_S);								

				// if(1==bannerStatus){
				// 	bannerBean.setBannerStatus(AfConstants.STATUS_ACTIVE);
				// 	bannerBean.setBannerStatusClass(AfConstants.STATUS_CLASS_GREEN);				
				// }
				// if(0==bannerStatus){
				// 	bannerBean.setBannerStatus(AfConstants.STATUS_IN_ACTIVE);
				// 	bannerBean.setBannerStatusClass(AfConstants.STATUS_CLASS_RED);				
				// }
				/*if(0==bannerSections)
					cBannerList.add(bannerBean);
				if(1==bannerSections)
				
                                sBannerList.add(bannerBean);*/
//				if(length>=tem){
                                    bannerList.add(bannerBean);
//                                
//                                }else{
//                                break;
//                                }
                                
                                
                                
			}
			/*bannerList.put("cbannerlist", cBannerList);
			bannerList.put("sbannerlist", sBannerList);*/
		}catch(Exception e){
			logger.error("@@@@ Exception in BannerManagementServiceimpl at getBannerListPagination() :", e);
		}
		return bannerList;
	}

	@Override
	public Long getBannerListPaginationCount() {
		Long totalCount =0L;
		try {
			totalCount = bannerManagementDAO.getBannerListPaginationCount();
		} catch (Exception e) {
			logger.error("@@@@Exception in BannerManagementServiceimpl at getBannerListPaginationCount() :", e);
		}
		return totalCount;
	}
	
	
}
