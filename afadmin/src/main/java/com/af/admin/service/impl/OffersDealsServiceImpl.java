package com.af.admin.service.impl;



import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.af.admin.DAO.CategoryManagementDAO;
import com.af.admin.DAO.OffersDealsDAO;
import com.af.admin.DAO.PartnerCategoryDAO;
import com.af.admin.DAO.PartnerManagementDAO;
import com.af.admin.DAO.UserActivityLogDAO;
import com.af.admin.beans.OffersAndDealsJson;
import com.af.admin.beans.OffersDealsBean;
import com.af.admin.beans.PartnerCategoryBean;
import com.af.admin.beans.offersBean;
import com.af.admin.constants.AfConstants;
import com.af.admin.entity.AfCategory;
import com.af.admin.entity.AfOfferDealLink;
import com.af.admin.entity.AfPartner;
import com.af.admin.entity.AfPartnerCategory;
import com.af.admin.entity.AfPartnerMapping;
import com.af.admin.entity.AfPartnerOffer;
import com.af.admin.entity.AfUserActivityLog;
import com.af.admin.service.CommonService;
import com.af.admin.service.OffersDealsService;
import com.af.admin.service.SendGetRequest;
import com.af.admin.utils.AdminUserActivityUtil;
import com.af.admin.utils.ServiceProviderUtil;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
@Transactional(readOnly=true)
public class OffersDealsServiceImpl implements OffersDealsService{
	private Logger logger = LogManager.getLogger(OffersDealsServiceImpl.class.getName());
	@Autowired
	OffersDealsDAO offersDealsDAO;

	@Autowired
	PartnerManagementDAO partnerManagementDAO;

	@Autowired
	CategoryManagementDAO categorymanagementDAO;

	@Autowired
	PartnerCategoryDAO partnerCategoryDAO;

	@Autowired
	CommonService commonService;
	
	@Autowired
	UserActivityLogDAO userActLogDao;
	
	@Autowired
	AdminUserActivityUtil adminUserActivityUtil;
	
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
	
	@Autowired
	SendGetRequest sendGetRequest;
	
	@Value("${application.af.offers_deals_list}")
	private String link_my_deal_uri;
	

	@Value("${application.af.offers_deals_list_increment}")
	private String link_my_deal_new_uri;
	
	@Value("${application.af.akamai.purgeUrl}")
    private String application_URL;
	
	
	@Value("${application.afadmin_serverUrl}")
    private String application_URL_Image;

	@Override
	public List<OffersDealsBean> getOffersDealsList() {

		String dateStr = "";
		int offerStatusCode;
		Date date=null;;
		List<AfPartnerOffer> afPartnerOfferList	  = null;
		List<OffersDealsBean> offersDealsBeanList = new ArrayList<OffersDealsBean>();
		try{
			afPartnerOfferList = offersDealsDAO.getOffersDealsList();
			if(null!=afPartnerOfferList){
				for(AfPartnerOffer afPartnerOffer : afPartnerOfferList){

					OffersDealsBean offersDealsBean = new OffersDealsBean();
					offersDealsBean.setPrtOffrNo(afPartnerOffer.getPrtOffrNo());
					offersDealsBean.setPrtNo(afPartnerOffer.getAfPartner().getPrtNo());
					AfPartnerCategory afPartnerCategory= afPartnerOffer.getAfPartnerCategory();
					if(afPartnerCategory!=null)
						offersDealsBean.setPrtCatNo(afPartnerOffer.getAfPartnerCategory().getPrtCatNo());
					else
						offersDealsBean.setPrtCatNo(0);
					offersDealsBean.setOfferName(ServiceProviderUtil.nullSafe(afPartnerOffer.getOfferName(),""));
					offersDealsBean.setOfferDesc(ServiceProviderUtil.nullSafe(afPartnerOffer.getDescription(),""));
					AfCategory afCategory=afPartnerOffer.getAfCategory();
					if(afCategory!=null)
					{
						offersDealsBean.setCatNo(ServiceProviderUtil.nullSafe(afPartnerOffer.getAfCategory().getCatNo().toString(),""));  			
						Integer catNo_=afPartnerOffer.getAfCategory().getCatNo();
						
						if(catNo_!=null && catNo_.intValue()!=0)
							offersDealsBean.setCategoryName(ServiceProviderUtil.nullSafe(afPartnerOffer.getAfCategory().getCategoryName(),""));
						else
							offersDealsBean.setCategoryName("");
					}
					else{
						offersDealsBean.setCatNo("");  			
						offersDealsBean.setCategoryName("");

					}
					offersDealsBean.setSortOrder(afPartnerOffer.getOfferPosition());
					date 	= afPartnerOffer.getActivationDate();
					dateStr =(String) ((date!=null && !date.equals(""))?ServiceProviderUtil.getInstance().getFormattedDate(date,"dd/MM/yyyy"):"");
					offersDealsBean.setOfferStartDate(dateStr);
					date=null;
					date 	= afPartnerOffer.getDeactivationDate();
					dateStr = (String) ((date!=null && !date .equals(""))?ServiceProviderUtil.getInstance().getFormattedDate(date,"dd/MM/yyyy"):"");
					offersDealsBean.setOfferEndDate(dateStr);

					offerStatusCode = afPartnerOffer.getOfferStatus();

					offersDealsBean.setOffersStatusCode(offerStatusCode);														

					if(1 == offerStatusCode){
						offersDealsBean.setOffersStatus(AfConstants.STATUS_ACTIVE);
						offersDealsBean.setOffersStatusClass(AfConstants.STATUS_CLASS_GREEN);				
					}
					if(0 == offerStatusCode){
						offersDealsBean.setOffersStatus(AfConstants.STATUS_IN_ACTIVE);
						offersDealsBean.setOffersStatusClass(AfConstants.STATUS_CLASS_RED);				
					}
					offersDealsBeanList.add(offersDealsBean);  
				}
			}
		}catch(Exception e){
			logger.debug("Inside getOffersDealsList Exception:"+e);
		}
		return offersDealsBeanList;
	}


	@Override
	public Map<String, String> getSearchByNameList( String prtNo, String searchByName) {

		Map<String, String> searchByNameList=new LinkedHashMap<String, String>();
		List<AfPartnerOffer> afPartnerOfferList=new ArrayList<AfPartnerOffer>();		
		try {

			if(prtNo.equals("")){
				afPartnerOfferList=offersDealsDAO.getOfferByNameList(searchByName);
			}
			else{
				afPartnerOfferList=offersDealsDAO.getSearchByNameList(prtNo);	
			}


			for(AfPartnerOffer afPartnerOffer:afPartnerOfferList){ 
				if(searchByName.equalsIgnoreCase("OffrName")){
					searchByNameList.put(afPartnerOffer.getPrtOffrNo().toString(), afPartnerOffer.getOfferName());
				}
				else if(searchByName.equalsIgnoreCase("OffrDesc")){
					searchByNameList.put(afPartnerOffer.getPrtOffrNo().toString(), afPartnerOffer.getDescription());
				}
				else if(searchByName.equalsIgnoreCase("CatName")){
					if(afPartnerOffer.getAfCategory()!= null) {
						if(!searchByNameList.containsValue(afPartnerOffer.getAfCategory().getCategoryName())) {
							searchByNameList.put(afPartnerOffer.getAfCategory().getCatNo().toString(),
									afPartnerOffer.getAfCategory().getCategoryName());
						}
					}
				}
			}
		} catch (Exception e) {
			logger.debug("Inside getSearchByNameList Exception:"+e);
		}
		return searchByNameList;
	}

	@Override
	public List<OffersDealsBean> searchOffesrDeals(OffersDealsBean offersDealsBean_) {
		try {
			List<AfPartnerOffer> afPartnerOfferList = new ArrayList<AfPartnerOffer>();

			List<OffersDealsBean>  offersDealsBeanList = new  ArrayList<OffersDealsBean>();
			String prtNo = offersDealsBean_.getPrtNo()!=null?offersDealsBean_.getPrtNo().toString():"";		
			String searchByName = ServiceProviderUtil.nullSafe(offersDealsBean_.getSearchByName(),"");	
			String searchByValue = ServiceProviderUtil.nullSafe(offersDealsBean_.getSearchByValue(),"");	
			String catNo = ServiceProviderUtil.nullSafe(offersDealsBean_.getCatNo(),"");

			String dateStr = "";
			int offerStatusCode;
			Date date;

			if(searchByName.equals("") && searchByValue.equals("")){
                            System.out.println("inside if conditon ------------");
				afPartnerOfferList = offersDealsDAO.getSearchByPrtNoOfferList(prtNo);
			}
			else {
                              System.out.println("inside else conditon ------------");
				switch(searchByName){
                                    
				case "OffrName":
					afPartnerOfferList = offersDealsDAO.getSearchByNameOfferList(prtNo, searchByName, searchByValue);
					break;

				case "OffrDesc":
					afPartnerOfferList = offersDealsDAO.getSearchByDescOfferList(prtNo, searchByName, searchByValue);
					break;

				case "CatName":
					afPartnerOfferList = offersDealsDAO.getSearchByCatOfferList(prtNo, searchByName, catNo);
					break;
				
                                }
			}

			if(null!=afPartnerOfferList){
				for(AfPartnerOffer afPartnerOffer : afPartnerOfferList){

					OffersDealsBean offersDealsBean = new OffersDealsBean();
					offersDealsBean.setPrtOffrNo(afPartnerOffer.getPrtOffrNo());
					offersDealsBean.setPrtNo(afPartnerOffer.getAfPartner().getPrtNo());
					AfPartnerCategory afPartnerCategory= afPartnerOffer.getAfPartnerCategory();
					if(afPartnerCategory!=null)
						offersDealsBean.setPrtCatNo(afPartnerOffer.getAfPartnerCategory().getPrtCatNo());
					else
						offersDealsBean.setPrtCatNo(0);
					offersDealsBean.setOfferName(ServiceProviderUtil.nullSafe(afPartnerOffer.getOfferName(),""));
					offersDealsBean.setOfferDesc(ServiceProviderUtil.nullSafe(afPartnerOffer.getDescription(),""));
					offersDealsBean.setSortOrder(afPartnerOffer.getOfferPosition());

					AfCategory afCategory=afPartnerOffer.getAfCategory();
					if(afCategory!=null)
					{
						offersDealsBean.setCatNo(ServiceProviderUtil.nullSafe(afPartnerOffer.getAfCategory().getCatNo().toString(),""));  			
						Integer catNo_=afPartnerOffer.getAfCategory().getCatNo();
						
						if(catNo_!=null && catNo_.intValue()!=0)
							offersDealsBean.setCategoryName(ServiceProviderUtil.nullSafe(afPartnerOffer.getAfCategory().getCategoryName(),""));
						else
							offersDealsBean.setCategoryName("");
					}
					else{
						offersDealsBean.setCatNo("");  			
						offersDealsBean.setCategoryName("");

					}
					date 	= afPartnerOffer.getActivationDate();
					dateStr =(String) ((date!=null && !date.equals(""))?ServiceProviderUtil.getInstance().getFormattedDate(date,"dd/MM/yyyy"):"");
					offersDealsBean.setOfferStartDate(dateStr);
					date=null;
					date 	= afPartnerOffer.getDeactivationDate();
					dateStr = (String) ((date!=null && !date.equals(""))?ServiceProviderUtil.getInstance().getFormattedDate(date,"dd/MM/yyyy"):"");
					offersDealsBean.setOfferEndDate(dateStr);

					offerStatusCode = afPartnerOffer.getOfferStatus();

					offersDealsBean.setOffersStatusCode(offerStatusCode);														

					if(1 == offerStatusCode){
						offersDealsBean.setOffersStatus(AfConstants.STATUS_ACTIVE);
						offersDealsBean.setOffersStatusClass(AfConstants.STATUS_CLASS_GREEN);				
					}
					if(0 == offerStatusCode){
						offersDealsBean.setOffersStatus(AfConstants.STATUS_IN_ACTIVE);
						offersDealsBean.setOffersStatusClass(AfConstants.STATUS_CLASS_RED);				
					}
					offersDealsBeanList.add(offersDealsBean);  
				}
			}
			return offersDealsBeanList;
		} catch (Exception e) {
			logger.debug("Inside searchOffesrDeals Exception:"+e);
			e.printStackTrace();
			return null;
		}
	}

	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String defineOffersDeals(OffersDealsBean offersDealsBean,HttpServletRequest request) {
		AfPartnerOffer afPartnerOffer = new AfPartnerOffer();
		AfPartner afPartner = null;
		AfCategory afCategory = null;
		AfPartnerCategory afPartnerCategory = null;			

		String activationDate = "", deactivationDate = "", status = "";
		Date dt = null;		
		DateFormat sourceDf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		Date dt1 = new Date();
		String hh_mi_ss = "" + dt1;		
		String format[] = hh_mi_ss.split(" ");
		hh_mi_ss = format[3];
		String oldOrNewVal = "";
		try {
			afPartner = partnerManagementDAO.getPartnerByPartnerNO(offersDealsBean.getPrtNo().toString());
			afPartnerOffer.setAfPartner(afPartner);
			if( (offersDealsBean.getPrtCatNo() != null)
					&& !("".equalsIgnoreCase(offersDealsBean.getPrtCatNo().toString())) ) {
				afPartnerCategory = partnerCategoryDAO.getPartnerCategoryByPrtCatNO(String.valueOf(offersDealsBean.getPrtCatNo()));
				afPartnerOffer.setAfPartnerCategory(afPartnerCategory);
			}
			if( (offersDealsBean.getCatNo() != null)
					&& !("".equalsIgnoreCase(offersDealsBean.getCatNo().toString())) ) {
				afCategory = categorymanagementDAO.getCategoryByCategoryNo(offersDealsBean.getCatNo());
				afPartnerOffer.setAfCategory(afCategory);
			}
			afPartnerOffer.setImageAltTxt(offersDealsBean.getAltTxt());
			afPartnerOffer.setOfferId("");
			afPartnerOffer.setOfferName(offersDealsBean.getOfferName());
			afPartnerOffer.setDescription(offersDealsBean.getOfferDesc());
			afPartnerOffer.setOfferLandingUrl(offersDealsBean.getLandingUrl());
			afPartnerOffer.setOfferLogoUrl(application_URL+"images/image/"+offersDealsBean.getImageUrl());//shilpi
			afPartnerOffer.setOfferPosition(offersDealsBean.getSortOrder());
			activationDate = offersDealsBean.getOfferStartDate();
			activationDate = activationDate + " " + hh_mi_ss;
			if(activationDate.length() > 0)
				dt = sourceDf.parse(activationDate);
			afPartnerOffer.setActivationDate(dt);

			deactivationDate = offersDealsBean.getOfferEndDate();
			if( (deactivationDate != null)
					&& (deactivationDate.length() > 0) ) {
				deactivationDate = deactivationDate + " " + hh_mi_ss;
				dt = sourceDf.parse(deactivationDate);
				afPartnerOffer.setDeactivationDate(dt);
			} else {
				afPartnerOffer.setDeactivationDate(null);
			}
			afPartnerOffer.setCreatedBy(1);
			afPartnerOffer.setCreatedTime(new Date());
			afPartnerOffer.setStatus((byte) 1);
			afPartnerOffer.setOfferStatus((byte)offersDealsBean.getOffersStatusCode());
			
			String status1=afPartnerOffer.getOfferStatus()+"";
			status1 = status1.replaceAll("0","InActive");
			status1 = status1.replaceAll("1", "Active");
			
			status = offersDealsDAO.defineOffersDeals(afPartnerOffer);
			if("success".equalsIgnoreCase(status)){
				String ipAddress = request.getHeader("X-FORWARDED-FOR");
				if(ipAddress == null) {
					ipAddress = request.getRemoteAddr();
				}
				ipAddress = (null != ipAddress) ? ipAddress.trim() : "";
				AfUserActivityLog afPrtActLog=new AfUserActivityLog();
				oldOrNewVal=" Select A Partner : "+afPartnerOffer.getAfPartner().getPartnerName()+" | "+"Select Offer Category : "+afPartnerOffer.getAfCategory().getCategoryName()+" | "+
						""+"Offers Name/Title: "+afPartnerOffer.getOfferName()+"| "+"Offer Description: "+afPartnerOffer.getDescription()+" |"+" Landing URL: "+afPartnerOffer.getOfferLandingUrl()+" |"
								+ " "+"Image URL : "+afPartnerOffer.getOfferLogoUrl()+" |"+"Sort Order : "+afPartnerOffer.getOfferPosition()+" |"
										+ ""+"Alt Tex : "+afPartnerOffer.getImageAltTxt()+" | "+"Offer Start Date : "+afPartnerOffer.getActivationDate()+" | "
												+ "|"+"Offer End Date : "+afPartnerOffer.getDeactivationDate()+"|"+"Offers & Deals Status : "+status1+"|";
								
			
						afPrtActLog.setUserNewFieldValue(oldOrNewVal);
						afPrtActLog.setUserOldFieldValue("NA");
						afPrtActLog.setUserActivity("Create Offers & Deals");
						afPrtActLog.setUserActCreatedDateTime(new Date());
						afPrtActLog.setUserActIpVal(ipAddress);
						afPrtActLog.setUserActModule("Offers & Deals");
						afPrtActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
						
						userActLogDao.createUserActLog(afPrtActLog);
			}
			
		} catch(Exception e) {
			logger.debug("defineOffersDeals Exception:" + e);	
			return "failure";
		}
		return status;
	}


	@Override
	public OffersDealsBean getPartnerOfferByNO(String prtOffrNo) {

		String dateStr = "";
		Date dt = null;

		AfPartnerOffer afPartnerOffer = null;
		OffersDealsBean offersDealsBean = null;
		List<PartnerCategoryBean> partnerCategoryBean = null;

		try{
			afPartnerOffer = offersDealsDAO.getPartnerOfferByNO(prtOffrNo);
			if(null != afPartnerOffer){
				partnerCategoryBean = commonService.getPartnerCategoryListBean(afPartnerOffer.getAfPartner().getPrtNo().toString());
				offersDealsBean = new OffersDealsBean();			
				offersDealsBean.setPrtOffrNo(afPartnerOffer.getPrtOffrNo());
				offersDealsBean.setPrtNo(afPartnerOffer.getAfPartner().getPrtNo());
				offersDealsBean.setAltTxt(afPartnerOffer.getImageAltTxt());
				AfPartnerCategory afPartnerCategory= afPartnerOffer.getAfPartnerCategory();
				if(afPartnerCategory!=null)
					offersDealsBean.setPrtCatNo(afPartnerOffer.getAfPartnerCategory().getPrtCatNo());
				else
					offersDealsBean.setPrtCatNo(0);
				offersDealsBean.setOfferName(ServiceProviderUtil.nullSafe(afPartnerOffer.getOfferName(),""));
				offersDealsBean.setOfferDesc(ServiceProviderUtil.nullSafe(afPartnerOffer.getDescription(),""));
				AfCategory afCategory=afPartnerOffer.getAfCategory();
				if(afCategory!=null)
				{
					offersDealsBean.setCatNo(afPartnerOffer.getAfCategory().getCatNo().toString());  			
					offersDealsBean.setCategoryName(ServiceProviderUtil.nullSafe(afPartnerOffer.getAfCategory().getCategoryName(),""));
				}
				else{
					offersDealsBean.setCatNo("");  			
					offersDealsBean.setCategoryName("");

				}
				offersDealsBean.setLandingUrl(ServiceProviderUtil.nullSafe(afPartnerOffer.getOfferLandingUrl(),""));
				offersDealsBean.setImageUrl(ServiceProviderUtil.nullSafe(afPartnerOffer.getOfferLogoUrl(),""));
				if(afPartnerOffer.getOfferLogoUrl().contains("https://") || afPartnerOffer.getOfferLogoUrl().contains("http://") ){
					offersDealsBean.setOfferImageUrl(afPartnerOffer.getOfferLogoUrl());
				}
				else{
				offersDealsBean.setOfferImageUrl(application_URL+"images/image/"+afPartnerOffer.getOfferLogoUrl());}
				offersDealsBean.setSortOrder(afPartnerOffer.getOfferPosition());

				dt=afPartnerOffer.getActivationDate();
				dateStr=ServiceProviderUtil.getInstance().getFormattedDate(afPartnerOffer.getActivationDate(),"dd/MM/yyyy");
				if(dt!=null && !dt.equals(""))
					offersDealsBean.setOfferStartDate(dateStr);

				dt=afPartnerOffer.getDeactivationDate();
				dateStr=ServiceProviderUtil.getInstance().getFormattedDate(afPartnerOffer.getDeactivationDate(),"dd/MM/yyyy");
				if(dt!=null && !dt.equals(""))
					offersDealsBean.setOfferEndDate(dateStr);			
				offersDealsBean.setOffersStatus(afPartnerOffer.getOfferStatus()+"");
				offersDealsBean.setPartnerCategoryBean(partnerCategoryBean);
			}
		}catch(Exception e){
			logger.debug("getPartnerOfferByNO Exception:"+e);	
		}
		return offersDealsBean;
	}

	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String updateOffersDealsDetails(OffersDealsBean offersDealsBean, HttpServletRequest request) {

		String activationDate = "", deactivationDate = "", status="";
		AfPartnerOffer afPartnerOffer = null; 
		AfPartner afPartner           = null;
		DateFormat dateFormat         = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		AfPartnerCategory afPartnerCategory = null;
		Date dt         = null;		
		Date dt1        = new Date();
		String hh_mi_ss = "" + dt1;
		String format[] = hh_mi_ss.split(" ");
		hh_mi_ss        = format[3];	
		String replaceInOld="";
		String replaceInNew="";
		AfPartnerOffer afPartnerOfferOld = null;
		try {		
			logger.debug("updateOfferDealsDetails getPrtOffrNo:"+offersDealsBean.getPrtOffrNo().toString());
			afPartnerOffer = offersDealsDAO.getPartnerOfferByNO(offersDealsBean.getPrtOffrNo().toString());
			afPartner = partnerManagementDAO.getPartnerByPartnerNO(offersDealsBean.getPrtNo().toString());			
			afPartnerOffer.setAfPartner(afPartner);
			afPartnerOfferOld=(AfPartnerOffer) afPartnerOffer.clone();
			if( (offersDealsBean.getPrtCatNo() != null) && !(offersDealsBean.getPrtCatNo().equals(0)) ) {
				afPartnerCategory = partnerCategoryDAO.getPartnerCategoryByPrtCatNO(offersDealsBean.getPrtCatNo().toString());			
				afPartnerOffer.setAfPartnerCategory(afPartnerCategory);			
			}
			if( (offersDealsBean.getCatNo() != null) && !(offersDealsBean.getCatNo().equalsIgnoreCase("")) ) {
				AfCategory afCategory = null;
				afCategory = categorymanagementDAO.getCategoryByCategoryNo(offersDealsBean.getCatNo());
				afPartnerOffer.setAfCategory(afCategory);
			}
			afPartnerOffer.setImageAltTxt(offersDealsBean.getAltTxt());
			afPartnerOffer.setOfferName(offersDealsBean.getOfferName());			
			afPartnerOffer.setOfferPosition(offersDealsBean.getSortOrder());
			afPartnerOffer.setDescription(offersDealsBean.getOfferDesc());
			afPartnerOffer.setOfferLandingUrl(offersDealsBean.getLandingUrl());
			afPartnerOffer.setOfferLogoUrl(application_URL+"images/image/"+offersDealsBean.getImageUrl().split(",")[0]);

			activationDate = offersDealsBean.getOfferStartDate();			
			activationDate = activationDate + " " + hh_mi_ss;
			if(activationDate.length() > 0)
				dt = dateFormat.parse(activationDate);
			afPartnerOffer.setActivationDate(dt);

			deactivationDate = offersDealsBean.getOfferEndDate();
			if( (deactivationDate != null) && (deactivationDate.trim().length() > 0) ) {
				deactivationDate = deactivationDate + " " + hh_mi_ss;
				dt = dateFormat.parse(deactivationDate);
				afPartnerOffer.setDeactivationDate(dt);
			} else {
				afPartnerOffer.setDeactivationDate(null);
			}
			afPartnerOffer.setModifiedTime(new Date());			
			afPartnerOffer.setOfferStatus((byte)Integer.parseInt(offersDealsBean.getOffersStatus()));
			String statusInactive=	afPartnerOfferOld.getOfferStatus()==0?"InActive":"Active";
	     	String statusActive=afPartnerOffer.getOfferStatus()==0?"InActive":"Active";
			try {
				status = offersDealsDAO.updateOfferDealsDetails(afPartnerOffer);
				if("Success".equalsIgnoreCase(status)){
					String ipAddress = request.getHeader("X-FORWARDED-FOR");
					if (ipAddress == null) {
						ipAddress = request.getRemoteAddr();
					}
					AfUserActivityLog updateUsActLog = new AfUserActivityLog();
					new LinkedHashMap<>();
					
						updateUsActLog.setUserActivity("Update Offers & Deals");
						updateUsActLog.setUserActCreatedDateTime(new Date());
						updateUsActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
						updateUsActLog.setUserActIpVal(ipAddress);
						updateUsActLog.setUserActModule("Offers & Deals");
						if(!afPartnerOffer.getAfPartner().getPartnerName().equalsIgnoreCase(afPartnerOfferOld.getAfPartner().getPartnerName())){
							replaceInOld=replaceInOld+"|Select A Partner :"+afPartnerOfferOld.getAfPartner().getPartnerName();
						    replaceInNew=replaceInNew+"|Select A Partner :"+afPartnerOffer.getAfPartner().getPartnerName();
						}
						if(!afPartnerOffer.getAfCategory().getCategoryName().equalsIgnoreCase(afPartnerOfferOld.getAfCategory().getCategoryName())){
							replaceInOld=replaceInOld+"|Select Offer Category :"+afPartnerOfferOld.getAfPartner().getPartnerName();
						    replaceInNew=replaceInNew+"|Select Offer Category :"+afPartnerOffer.getAfPartner().getPartnerName();
						}
						if(!afPartnerOffer.getOfferName().equalsIgnoreCase(afPartnerOfferOld.getOfferName())){
							replaceInOld=replaceInOld+"|Offers Name/Title :"+afPartnerOfferOld.getOfferName();
						    replaceInNew=replaceInNew+"|Offers Name/Title :"+afPartnerOffer.getOfferName();
						}
						
						if(!afPartnerOffer.getDescription().equalsIgnoreCase(afPartnerOfferOld.getDescription())){
							replaceInOld=replaceInOld+"|Description :"+afPartnerOfferOld.getDescription();
						    replaceInNew=replaceInNew+"|Description :"+afPartnerOffer.getDescription();
						}
						if(afPartnerOffer.getOfferPosition()!=afPartnerOfferOld.getOfferPosition()){
							replaceInOld=replaceInOld+"|Sort Order :"+afPartnerOfferOld.getOfferPosition();
						    replaceInNew=replaceInNew+"|Sort Order :"+afPartnerOffer.getOfferPosition();
						}
						if(!afPartnerOffer.getImageAltTxt().equalsIgnoreCase(afPartnerOfferOld.getImageAltTxt())){
							replaceInOld=replaceInOld+"|Alt Text :"+afPartnerOfferOld.getImageAltTxt();
						    replaceInNew=replaceInNew+"|Alt Text :"+afPartnerOffer.getImageAltTxt();
						}
						if(!afPartnerOffer.getOfferLandingUrl().equalsIgnoreCase(afPartnerOfferOld.getOfferLandingUrl())){
							replaceInOld=replaceInOld+"|Landing URL :"+afPartnerOfferOld.getOfferLandingUrl();
						    replaceInNew=replaceInNew+"|Landing URL :"+afPartnerOffer.getOfferLandingUrl();
						}
						if(!afPartnerOffer.getOfferLogoUrl().equalsIgnoreCase(afPartnerOfferOld.getOfferLogoUrl())){
							replaceInOld=replaceInOld+"|Image URL :"+afPartnerOfferOld.getOfferLogoUrl();
						    replaceInNew=replaceInNew+"|Image URL :"+afPartnerOffer.getOfferLogoUrl();
						}
						/*if(!afPartnerOffer.getActivationDate().equals(afPartnerOfferOld.getActivationDate())){
							replaceInOld=replaceInOld+"|Offer Start Date :"+afPartnerOfferOld.getActivationDate();
						    replaceInNew=replaceInNew+"|Offer Start Date :"+afPartnerOffer.getActivationDate();
						}*/
						if(afPartnerOffer.getDeactivationDate()!=(afPartnerOfferOld.getDeactivationDate())){
							replaceInOld=replaceInOld+"|Offer End Date :"+afPartnerOfferOld.getDeactivationDate();
						    replaceInNew=replaceInNew+"|Offer End Date :"+afPartnerOffer.getDeactivationDate();
						}
						if(afPartnerOffer.getOfferStatus()!=afPartnerOfferOld.getOfferStatus()){
							replaceInOld=replaceInOld+"|Offers & Deals Status :"+statusInactive;
						    replaceInNew=replaceInNew+"|Offers & Deals Status:"+statusActive;
						}
			    		if(replaceInNew.isEmpty() && replaceInOld.isEmpty()) {
	                        replaceInOld=replaceInOld+"|ModifiedTime:"+simpleDateFormat.format(afPartnerOfferOld.getModifiedTime());
	                        replaceInNew=replaceInNew+"|ModifiedTime:"+simpleDateFormat.format(afPartnerOffer.getModifiedTime());
	                    }
			    		updateUsActLog.setUserNewFieldValue(replaceInNew);
			    		updateUsActLog.setUserOldFieldValue(replaceInOld);
			    		userActLogDao.updateUserLog(updateUsActLog);
				}
			} catch(Exception e) {
				logger.debug("updateOfferDealsDetails Exception while calling update:" + e);
				return "failure";
			}
		} catch(Exception e) {
			logger.debug("updateOfferDealsDetails Exception in updateOfferDealsDetails:" + e);
			return "failure";
		}
		return status;
	}

	@Override
	public Map<String, String> getOfferNamesByPrtCatNo(String prtNo, String prtCatNo) {				
		Map<String, String> offerNamesList = new LinkedHashMap<String, String>();
		List<AfPartnerOffer> afOfferNamesList = new ArrayList<AfPartnerOffer>();
		try {
			afOfferNamesList = offersDealsDAO.getSearchByNamePCOList(prtNo,prtCatNo);					
		} catch (Exception e) {
			logger.debug("getOfferNamesByPrtCatNo Exception:"+e);	
		}	
		for(AfPartnerOffer afPartnerOffer:afOfferNamesList){ 
			offerNamesList.put(afPartnerOffer.getPrtOffrNo().toString(), afPartnerOffer.getOfferName());
		}
		return offerNamesList;
	}
	
	@Override
	public Map<String, String> getOfferTitle(String prtNo, String catNo){
		Map<String, String> offerNamesList = new LinkedHashMap<String, String>();
		List<AfPartnerOffer> afOfferNamesList = new ArrayList<AfPartnerOffer>();
		try {
			afOfferNamesList = offersDealsDAO.getOfferTitle(prtNo, catNo);					
		} catch (Exception e) {
			logger.error("@@@Exception in OffersDealsServiceImpl getOfferTitle():", e);	
		}
		for(AfPartnerOffer afPartnerOffer:afOfferNamesList){ 
			offerNamesList.put(afPartnerOffer.getPrtOffrNo().toString(), afPartnerOffer.getOfferName());
		}
		return offerNamesList;
	}
	
	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String deleteSelectedOffer(Integer prtOffrNo,HttpServletRequest request) {
		String deleteStatus="";
		String oldOrNewVal = "";
		AfPartnerOffer afPartnerOffer=null;
		try{
			String ipAddress = request.getHeader("X-FORWARDED-FOR");
			if (ipAddress == null) {
				ipAddress = request.getRemoteAddr();
			}
			afPartnerOffer =offersDealsDAO.getPartnerOfferByNO(prtOffrNo.toString());
			String status1=afPartnerOffer.getOfferStatus()+"";
			status1 = status1.replaceAll("0","InActive");
			status1 = status1.replaceAll("1", "Active");
			
			AfUserActivityLog updateUsActLog = new AfUserActivityLog();
			oldOrNewVal=" Offer Name/Title : "+afPartnerOffer.getOfferName()+" | "+"Offer Description : "+afPartnerOffer.getDescription()+" | "+
					""+"Category Name: "+afPartnerOffer.getAfPartnerCategory().getPartnerCategoryName()+"| "+"Sort Order: "+afPartnerOffer.getOfferPosition()+" |"+" Offer Start Date: "+afPartnerOffer.getActivationDate()+" |"
					+ " "+"Offer End Date : "+afPartnerOffer.getDeactivationDate()+" |"+"Status : "+status1+" |";
							
			updateUsActLog.setUserActivity("Offers & Deals Delete");
	    	updateUsActLog.setUserActCreatedDateTime(new Date());
	    	updateUsActLog.setUserNewFieldValue("NA");
	    	updateUsActLog.setUserActIpVal(ipAddress);
	    	updateUsActLog.setUserActModule("Offers & Deals");
	    	updateUsActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
	    	updateUsActLog.setUserOldFieldValue(oldOrNewVal);
	    	userActLogDao.updateUserLog(updateUsActLog);
			
			deleteStatus=offersDealsDAO.deleteSelectedOffer(prtOffrNo);
			
		}catch(Exception e){
            e.printStackTrace();
			logger.debug("deleteSelectedOffer Exception:"+e);
			return "failure";
		}
		return deleteStatus;
	}

	@Override
	public boolean checkOffersDealSortOrder(String offersDealsSortOrder) {
		boolean checkFlag=false;
		List<AfPartnerOffer> afOfferSortOrderList;
		try{
			afOfferSortOrderList=offersDealsDAO.checkOffersDealSortOrder(offersDealsSortOrder);
			if(afOfferSortOrderList!=null && afOfferSortOrderList.size()>0){
				checkFlag=true;
			}
		}catch(Exception e){
			logger.error("@@Exception in RecommendedProductServiceimpl checkRecommendedProduct():", e);
		}
		return checkFlag;
	}


	@Override
	public List<OffersDealsBean> getOffersDealsListByPrtNo(String prtNo) {
		String dateStr = "";
		int offerStatusCode;
		Date date;
		List<AfPartnerOffer> afPartnerOfferList	  = null;
		List<OffersDealsBean> offersDealsBeanList = new ArrayList<OffersDealsBean>();
		try{
			afPartnerOfferList = offersDealsDAO.getOffersDealsListByPrtNo(Integer.parseInt(prtNo));
			if(null!=afPartnerOfferList){
				for(AfPartnerOffer afPartnerOffer : afPartnerOfferList){

					OffersDealsBean offersDealsBean = new OffersDealsBean();
					offersDealsBean.setPrtOffrNo(afPartnerOffer.getPrtOffrNo());
					offersDealsBean.setPrtNo(afPartnerOffer.getAfPartner().getPrtNo());
					offersDealsBean.setOfferName(ServiceProviderUtil.nullSafe(afPartnerOffer.getOfferName(),""));
					offersDealsBean.setOfferDesc(ServiceProviderUtil.nullSafe(afPartnerOffer.getDescription(),""));
					offersDealsBean.setPrtCatNo(afPartnerOffer.getAfCategory().getCatNo());  			
					offersDealsBean.setCategoryName(ServiceProviderUtil.nullSafe(afPartnerOffer.getAfCategory().getCategoryName(),""));
					offersDealsBean.setSortOrder(afPartnerOffer.getOfferPosition());
					date 	= afPartnerOffer.getActivationDate();
					dateStr =(String) ((date!=null && !date.equals(""))?ServiceProviderUtil.getInstance().getFormattedDate(date,"dd/MM/yyyy"):new Date());
					offersDealsBean.setOfferStartDate(dateStr);
					date 	= afPartnerOffer.getDeactivationDate();
					dateStr = (String) ((date!=null && !date.equals(""))?ServiceProviderUtil.getInstance().getFormattedDate(date,"dd/MM/yyyy"):new Date());
					offersDealsBean.setOfferEndDate(dateStr);

					offerStatusCode = afPartnerOffer.getOfferStatus();

					offersDealsBean.setOffersStatusCode(offerStatusCode);														

					if(1 == offerStatusCode){
						offersDealsBean.setOffersStatus(AfConstants.STATUS_ACTIVE);
						offersDealsBean.setOffersStatusClass(AfConstants.STATUS_CLASS_GREEN);				
					}
					if(0 == offerStatusCode){
						offersDealsBean.setOffersStatus(AfConstants.STATUS_IN_ACTIVE);
						offersDealsBean.setOffersStatusClass(AfConstants.STATUS_CLASS_RED);				
					}
					offersDealsBeanList.add(offersDealsBean);  
				}
			}
		}catch(Exception e){
			logger.debug("Inside getOffersDealsList Exception:"+e);
		}
		return offersDealsBeanList;
	}
	
	public Integer getMaxOfferandDealsSlNo() {
		Integer maxCount = 0;
		try {
			maxCount = offersDealsDAO.getMaxOffandDealSl();
		} catch(Exception e) {
			logger.debug("Inside getMaxOfferandDealsSlNo Exception: ", e);
		}
		return maxCount;
	}


	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public List<AfOfferDealLink> getAllOffersDealsList() {
		List<AfOfferDealLink> afOfferDealLink= null;
		AfOfferDealLink offerLink = null;
		List<AfPartnerMapping> afPartnerMappingList = new ArrayList<AfPartnerMapping>();
		String status = AfConstants.SUCCESS;
		try{
		ObjectMapper mapper = new ObjectMapper();
		String url=link_my_deal_uri,response="";
		response = sendGetRequest.sendGet(url);
		String lmdId;
		OffersAndDealsJson offersAndDealsJson=mapper.readValue(response, OffersAndDealsJson.class);
		for (offersBean offersBean : offersAndDealsJson.getOffersBean()) {
			offerLink = new AfOfferDealLink();
			offersAndDealsJson.getOffersBean();
			
			offerLink = offersDealsDAO.getOfferLmdId(offersBean.getLmd_id());
			
			if (null != offerLink) {
				lmdId = StringUtils.trimToEmpty(offersBean.getLmd_id());
				if (NumberUtils.isDigits(lmdId)) {
					offerLink.setLmdId(Integer.parseInt(lmdId));
				}
				afPartnerMappingList = partnerManagementDAO.getPartnerMappingName(offersBean.getStore());
				for(AfPartnerMapping afPartnerMappingList1:afPartnerMappingList){
					offerLink.setPartnerMapName(afPartnerMappingList1.getAfPartner().getPrtNo().toString());
					if((offersBean.getImage_url()!=null) && (!offersBean.getImage_url().isEmpty())){
						offerLink.setImage_url(offersBean.getImage_url());
					}
					else{
						offerLink.setImage_url(application_URL_Image+"images/partner/"+afPartnerMappingList1.getAfPartner().getPartnerLogoUrl());
					}
					if(afPartnerMappingList1.getAllowLandingUrl()==1){
						offerLink.setUrl(afPartnerMappingList1.getAfPartner().getPartnerLandingUrl());
					}
					else{
						offerLink.setUrl(offersBean.getOffers__url());
					}
				}
				
				offerLink.setStore(offersBean.getStore());
				offerLink.setOfferText(offersBean.getOffer_text());
				offerLink.setTitle(offersBean.getOffers__title());
				//offerLink.setUrl(offersBean.getOffers__url());
				offerLink.setDescription(offersBean.getOffers__description());
				offerLink.setCategories(offersBean.getOffers__categories());
				offerLink.setCode(offersBean.getCode());
				offerLink.setFeatured(offersBean.getFeatured());
				//offerLink.setImage_url(offersBean.getImage_url());
				offerLink.setOfferValue(offersBean.getOffer_value());
				offerLink.setOffer(offersBean.getOffer());
				offerLink.setSmartLink(offersBean.getSmartLink());
				offerLink.setTermsAndConditions(offersBean.getTerms_and_conditions());
				offerLink.setStartDate(offersBean.getOffers__start_date());
				offerLink.setEndDate(offersBean.getOffers__end_date());
				offerLink.setStatus(offersBean.getStatus());
				status = offersDealsDAO.offersDealsLink(offerLink);
			}
		}
			if(status.equalsIgnoreCase(AfConstants.SUCCESS)){
				afOfferDealLink = offersDealsDAO.getOffersDealList();
			}
		}
		
		catch(Exception e){
			logger.error("@@Exception in offers&dealsServiceimpl getAllOffersDealsList():", e);
		}
		return afOfferDealLink;
	}


	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String addOfferDeal(String lmdId,String id) {/*
		AfOfferDealLink afOfferDealLink = null;
		AfPartnerOffer afPartnerOffer = new AfPartnerOffer();
		String status = AfConstants.SUCCESS;
		 AfPartnerCategory afPartnerCategory=null;
		try{
			Long  afPartnerOffer1=offersDealsDAO.getOfferLmdIdCheckDupl(lmdId);
            
            if(afPartnerOffer1==0){
			afOfferDealLink=offersDealsDAO.getOfferdealLmdId(id);
			if(afOfferDealLink!=null){
			Integer  maxValue=offersDealsDAO.getMaxValueOfColumn("AfPartnerOffer","offerPosition");
			Integer a=maxValue+1;
			AfPartner afPartner = partnerManagementDAO.getPartnerByName(afOfferDealLink.getStore());
			afPartnerOffer.setAfPartner(afPartner);
            AfCategory afCategory =categorymanagementDAO.getCategoryByCategoryName(afOfferDealLink.getCategories());
            afPartnerOffer.setAfCategory(afCategory);
            
            afPartnerOffer.setLmdId(afOfferDealLink.getLmdId().toString());
            afPartnerOffer.setDescription(afOfferDealLink.getDescription());
            afPartnerOffer.setOfferLogoUrl(afOfferDealLink.getImage_url());
            afPartnerOffer.setOfferLandingUrl(afOfferDealLink.getSmartLink());
            afPartnerOffer.setOfferName(afOfferDealLink.getTitle());
            afPartnerOffer.setImageAltTxt(afOfferDealLink.getTitle());
            afPartnerOffer.setOfferPosition(a);
            afPartnerOffer.setAfPartnerCategory(afPartnerCategory);
            afPartnerOffer.setOfferId("null");
        	SimpleDateFormat offerdateFormat = new SimpleDateFormat("yyyy-MM-dd");
        	SimpleDateFormat offerdateFormatDateFirst = new SimpleDateFormat("dd/MM/yyyy");
        	 Date activationDate= null;
        	 Date deActivationDate = null;
        	if(afOfferDealLink.getStartDate().contains("-")){
        		 activationDate = offerdateFormat.parse(afOfferDealLink.getStartDate());
        	}
        	if(afOfferDealLink.getEndDate().contains("-")){
        		deActivationDate = offerdateFormat.parse(afOfferDealLink.getEndDate());
        	}
        	if(afOfferDealLink.getStartDate().contains("/")){
       		 activationDate = offerdateFormatDateFirst.parse(afOfferDealLink.getStartDate());
	       	}
	       	if(afOfferDealLink.getEndDate().contains("/")){
	       		deActivationDate = offerdateFormatDateFirst.parse(afOfferDealLink.getEndDate());
	       	}
            
             
            afPartnerOffer.setActivationDate(activationDate);
            afPartnerOffer.setDeactivationDate(deActivationDate);
           
            
            String offerStatus=afOfferDealLink.getStatus();
            if(offerStatus.equalsIgnoreCase("active")){
            	afPartnerOffer.setOfferStatus((byte)1);
            }else{
            	afPartnerOffer.setOfferStatus((byte)0);
            }
            afPartnerOffer.setStatus((byte)1);
			status = offersDealsDAO.offersDealsLink(afPartnerOffer);
			}
            }
            else{
            	status="failure";
            }
		}
		catch(Exception e){
			status="failure";
			e.printStackTrace();
			logger.error("@@Exception in offers&dealsServiceimpl addOfferDeal():", e);
		}
		return status;
	*/
		AfOfferDealLink afOfferDealLink = null;
		String[] lmdIds= lmdId.split(",");
		String[] ids= id.split(",");
		
		String status = AfConstants.SUCCESS;
		 AfPartnerCategory afPartnerCategory=null;
		try{
			for(int lmd=0;lmd<lmdIds.length;lmd++){
			AfPartnerOffer afPartnerOffer = new AfPartnerOffer();
			Long  afPartnerOffer1=offersDealsDAO.getOfferLmdIdCheckDupl(lmdIds[lmd]);
            
            if(afPartnerOffer1==0){
			afOfferDealLink=offersDealsDAO.getOfferdealLmdId(ids[lmd]);
			if(afOfferDealLink!=null){
				Integer  maxValue=offersDealsDAO.getMaxValueOfColumn("AfPartnerOffer","offerPosition");
				Integer a=maxValue+1;
			AfPartner afPartner = partnerManagementDAO.getPartnerByName(afOfferDealLink.getStore());
			afPartnerOffer.setAfPartner(afPartner);
            AfCategory afCategory =categorymanagementDAO.getCategoryByCategoryName(afOfferDealLink.getCategories());
            if(afCategory == null){
            	continue;
            }
            afPartnerOffer.setAfCategory(afCategory);
            afPartnerOffer.setLmdId(afOfferDealLink.getLmdId().toString());
            afPartnerOffer.setDescription(afOfferDealLink.getDescription());
            if(afOfferDealLink.getImage_url().startsWith("http") || afOfferDealLink.getImage_url().startsWith("https")){
            afPartnerOffer.setOfferLogoUrl(afOfferDealLink.getImage_url());
            }else{
            	afPartnerOffer.setOfferLogoUrl(application_URL+"images/image/"+afOfferDealLink.getImage_url());
            }
            if(afPartner.getPartnerLandingUrl().contains("https://ad.admitad.com")){
            	afPartnerOffer.setOfferLandingUrl(afPartner.getPartnerLandingUrl());
            }else{
            afPartnerOffer.setOfferLandingUrl(afOfferDealLink.getUrl());
            }
            afPartnerOffer.setOfferName(afOfferDealLink.getTitle());
            afPartnerOffer.setImageAltTxt(afOfferDealLink.getTitle());
            afPartnerOffer.setOfferPosition(a);
            afPartnerOffer.setAfPartnerCategory(afPartnerCategory);
            afPartnerOffer.setOfferId("Offer");
        	SimpleDateFormat offerdateFormat = new SimpleDateFormat("yyyy-MM-dd");
        	SimpleDateFormat offerdateFormatDateFirst = new SimpleDateFormat("dd/MM/yyyy");
       	    Date activationDate= null;
       	    Date deActivationDate = null;
          	if(afOfferDealLink.getStartDate().contains("-")){
       		 activationDate = offerdateFormat.parse(afOfferDealLink.getStartDate());
       	    }
       	    if(afOfferDealLink.getEndDate().contains("-")){
       		deActivationDate = offerdateFormat.parse(afOfferDealLink.getEndDate());
         	}
        	if(afOfferDealLink.getStartDate().contains("/")){
      		 activationDate = offerdateFormatDateFirst.parse(afOfferDealLink.getStartDate());
	       	}
	       	if(afOfferDealLink.getEndDate().contains("/")){
	       		deActivationDate = offerdateFormatDateFirst.parse(afOfferDealLink.getEndDate());
	       	}
           
            
           afPartnerOffer.setActivationDate(activationDate);
           afPartnerOffer.setDeactivationDate(deActivationDate);
            String offerStatus=afOfferDealLink.getStatus();
            if(offerStatus.equalsIgnoreCase("active")){
            	afPartnerOffer.setOfferStatus((byte)1);
            }else{
            	afPartnerOffer.setOfferStatus((byte)0);
            }
            afPartnerOffer.setStatus((byte)1);
			status = offersDealsDAO.offersDealsLink(afPartnerOffer);
			}
            }
            else{
            	status="failure";
            }
		}
		}
		catch(Exception e){
			status="failure";
			
			logger.error("@@Exception in offers&dealsServiceimpl addOfferDeal():", e);
		}
		return status;
	}


	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String updateOffersDealsLink(JSONObject userJson, HttpServletRequest request) {
		String status="";
		AfOfferDealLink afOfferDealLink = null;
		AfPartner	afPartner=null;
		Integer id = -1;		
		new SimpleDateFormat("yyyy-MM-dd");
		try{
			
			id= userJson.getInt("id");
			afOfferDealLink=offersDealsDAO.getOfferdealId(id);
			afOfferDealLink.setLmdId(userJson.getInt("lmdId"));
			 afPartner = partnerManagementDAO.getPartnerByPartnerNO(userJson.getString("store"));
			 String prtName="";
			 if(afPartner == null){
				  prtName = userJson.getString("store");
			 }else{
				 prtName= afPartner.getPartnerName();
			 }
			afOfferDealLink.setStore(prtName);
			afOfferDealLink.setOfferText(userJson.getString("offerText"));
			afOfferDealLink.setTitle(userJson.getString("title"));
			afOfferDealLink.setDescription(userJson.getString("description"));
			afOfferDealLink.setCategories(userJson.getString("categories"));
			afOfferDealLink.setImage_url(userJson.getString("editImageUrl"));
		/*	String a = dateformat.format(userJson.getString("startDate"));
			String b= dateformat.format(userJson.getString("endDate"));*/
			afOfferDealLink.setStartDate(userJson.getString("startDate"));
			afOfferDealLink.setEndDate(userJson.getString("endDate"));
			status=offersDealsDAO.offersDealsLink(afOfferDealLink);
		}
		catch(Exception e){
			logger.error("@@Exception in offers&dealsServiceimpl addOfferDeal():", e);
		}
		return status;
	}


	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public List<AfOfferDealLink> getAllOffersDealsListcheck() {
		List<AfOfferDealLink> afOfferDealLink= null;
		try{
			afOfferDealLink = offersDealsDAO.getOffersDealList();
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error("@@Exception in offers&dealsServiceimpl getAllOffersDealsList():", e);
		}
		return afOfferDealLink;
	}


	@Override
	public List<offersBean> getExportOffersDealList() {
		
		List<Object[]> reportObject = null;
		List<offersBean> reportList = null;
		String status;
		try{
			reportObject = offersDealsDAO.getExportOffersDealList();
			if (reportObject != null && !reportObject.isEmpty()) {
				reportList = new ArrayList<>();
			for (Object[] row : reportObject) {
			offersBean offersBeanLinkList =new offersBean();
			offersBeanLinkList.setLmd_id(ServiceProviderUtil.checkNullSafeWithObject(row[0], "NA"));
			offersBeanLinkList.setStore(ServiceProviderUtil.checkNullSafeWithObject(row[1], "NA"));
			offersBeanLinkList.setOffer_text(ServiceProviderUtil.checkNullSafeWithObject(row[2], "NA"));
			offersBeanLinkList.setOffers__title(ServiceProviderUtil.checkNullSafeWithObject(row[3], "NA"));
			offersBeanLinkList.setOffers__categories(ServiceProviderUtil.checkNullSafeWithObject(row[4], "NA"));
			offersBeanLinkList.setOffers__description(ServiceProviderUtil.checkNullSafeWithObject(row[5], "NA"));
			offersBeanLinkList.setOffers__start_date(ServiceProviderUtil.checkNullSafeWithObject(row[6], "NA"));
			offersBeanLinkList.setOffers__end_date(ServiceProviderUtil.checkNullSafeWithObject(row[7], "NA"));
			status= ServiceProviderUtil.checkNullSafeWithObject(row[8], "NA");
			if("1".equalsIgnoreCase(status)){
				offersBeanLinkList.setStatus(AfConstants.STATUS_ACTIVE);
			}
			else {
				offersBeanLinkList.setStatus(AfConstants.STATUS_IN_ACTIVE);
			}
			reportList.add(offersBeanLinkList);
			}
		}
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error("@@Exception in offers&dealsServiceimpl getExportOffersDealList():", e);
		}
		
		return reportList;
	}


	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public List<AfOfferDealLink> approvedOfferDealLink() {
		List<AfOfferDealLink> afOfferDealLinks = new ArrayList<>();
		try{
			afOfferDealLinks=offersDealsDAO.approvedOfferDealLink();
			
		}
		catch(Exception e){
			
			logger.error("@@Exception in offers&dealsServiceimpl approvedOfferDealLink():", e);
		}
		return afOfferDealLinks;
	}


	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public List<AfOfferDealLink> pendingOfferDealLink() {
		List<AfOfferDealLink> afOfferDealLinks= null;
		
		try {
			afOfferDealLinks = offersDealsDAO.getPendingLinkOffers();
		} catch (Exception e) {
			logger.error("@@Exception in offers&dealsServiceimpl pendingOfferDealLink():", e);
		}
		return afOfferDealLinks;
	}


	@Override
	public List<AfOfferDealLink> newRecordOfferDealLink() {

		List<AfOfferDealLink> afOfferDealLink= new ArrayList<>();
		AfOfferDealLink offerLink = null;
		try{
		ObjectMapper mapper = new ObjectMapper();
			
		String url=link_my_deal_new_uri;
		String response="";
		response = sendGetRequest.sendGet(url);
		String lmdId;
		OffersAndDealsJson offersAndDealsJson=mapper.readValue(response, OffersAndDealsJson.class);
		for (offersBean offersBean : offersAndDealsJson.getOffersBean()) {
			offerLink = new AfOfferDealLink();
			offersAndDealsJson.getOffersBean();
			
			offerLink = offersDealsDAO.getOfferLmdId(offersBean.getLmd_id());
			
			if (null != offerLink) {
				lmdId = StringUtils.trimToEmpty(offersBean.getLmd_id());
				if (NumberUtils.isDigits(lmdId)) {
					offerLink.setLmdId(Integer.parseInt(lmdId));
				}
				
				offerLink.setStore(offersBean.getStore());
				offerLink.setOfferText(offersBean.getOffer_text());
				offerLink.setTitle(offersBean.getOffers__title());
				offerLink.setUrl(offersBean.getOffers__url());
				offerLink.setDescription(offersBean.getOffers__description());
				offerLink.setCategories(offersBean.getOffers__categories());
				offerLink.setCode(offersBean.getCode());
				offerLink.setFeatured(offersBean.getFeatured());
				offerLink.setImage_url(offersBean.getImage_url());
				offerLink.setOfferValue(offersBean.getOffer_value());
				offerLink.setOffer(offersBean.getOffer());
				offerLink.setSmartLink(offersBean.getSmartLink());
				offerLink.setTermsAndConditions(offersBean.getTerms_and_conditions());
				offerLink.setStartDate(offersBean.getOffers__start_date());
				offerLink.setEndDate(offersBean.getOffers__end_date());
				offerLink.setStatus(offersBean.getStatus());
				afOfferDealLink.add(offerLink);
				offersDealsDAO.offersDealsLink(offerLink);
			}
		}
			
		}
		
		catch(Exception e){
			
			logger.error("@@Exception in offers&dealsServiceimpl getAllOffersDealsList():", e);
		}
		return afOfferDealLink;
	
	}


	@Override
	public List<OffersDealsBean> getOffersDealsListPagination(int length, int start) {
		String dateStr = "";
		int offerStatusCode;
		Date date=null;;
		List<AfPartnerOffer> afPartnerOfferList	  = null;
		List<OffersDealsBean> offersDealsBeanList = new ArrayList<OffersDealsBean>();
		try{
			afPartnerOfferList = offersDealsDAO.getOffersDealsList(length, start);
			if(null!=afPartnerOfferList){
				for(AfPartnerOffer afPartnerOffer : afPartnerOfferList){

					OffersDealsBean offersDealsBean = new OffersDealsBean();
					offersDealsBean.setPrtOffrNo(afPartnerOffer.getPrtOffrNo());
					offersDealsBean.setPrtNo(afPartnerOffer.getAfPartner().getPrtNo());
					AfPartnerCategory afPartnerCategory= afPartnerOffer.getAfPartnerCategory();
					if(afPartnerCategory!=null)
						offersDealsBean.setPrtCatNo(afPartnerOffer.getAfPartnerCategory().getPrtCatNo());
					else
						offersDealsBean.setPrtCatNo(0);
					offersDealsBean.setOfferName(ServiceProviderUtil.nullSafe(afPartnerOffer.getOfferName(),""));
					offersDealsBean.setOfferDesc(ServiceProviderUtil.nullSafe(afPartnerOffer.getDescription(),""));
					AfCategory afCategory=afPartnerOffer.getAfCategory();
					if(afCategory!=null)
					{
						offersDealsBean.setCatNo(ServiceProviderUtil.nullSafe(afPartnerOffer.getAfCategory().getCatNo().toString(),""));  			
						Integer catNo_=afPartnerOffer.getAfCategory().getCatNo();
						
						if(catNo_!=null && catNo_.intValue()!=0)
							offersDealsBean.setCategoryName(ServiceProviderUtil.nullSafe(afPartnerOffer.getAfCategory().getCategoryName(),""));
						else
							offersDealsBean.setCategoryName("");
					}
					else{
						offersDealsBean.setCatNo("");  			
						offersDealsBean.setCategoryName("");

					}
					offersDealsBean.setSortOrder(afPartnerOffer.getOfferPosition());
					date 	= afPartnerOffer.getActivationDate();
					dateStr =(String) ((date!=null && !date.equals(""))?ServiceProviderUtil.getInstance().getFormattedDate(date,"dd/MM/yyyy"):"");
					offersDealsBean.setOfferStartDate(dateStr);
					date=null;
					date 	= afPartnerOffer.getDeactivationDate();
					dateStr = (String) ((date!=null && !date .equals(""))?ServiceProviderUtil.getInstance().getFormattedDate(date,"dd/MM/yyyy"):"");
					offersDealsBean.setOfferEndDate(dateStr);

					offerStatusCode = afPartnerOffer.getOfferStatus();

					offersDealsBean.setOffersStatusCode(offerStatusCode);														

					if(1 == offerStatusCode){
						offersDealsBean.setOffersStatus(AfConstants.STATUS_ACTIVE);
						offersDealsBean.setOffersStatusClass(AfConstants.STATUS_CLASS_GREEN);				
					}
					if(0 == offerStatusCode){
						offersDealsBean.setOffersStatus(AfConstants.STATUS_IN_ACTIVE);
						offersDealsBean.setOffersStatusClass(AfConstants.STATUS_CLASS_RED);				
					}
					offersDealsBeanList.add(offersDealsBean);  
				}
			}
		}catch(Exception e){
			logger.debug("Inside getOffersDealsList getOffersDealsListPagination:"+e);
		}
		return offersDealsBeanList;
	}


	@Override
	public Long getPartnersCommissionPaginationCount() {
		Long totalCount =0L;
		try {
			totalCount = offersDealsDAO.getPartnersCommissionPaginationCount();
		} catch (Exception e) {
			logger.error("@@@@Exception in OffersDealServiceImpl at getPartnersCommissionPaginationCount() :", e);
		}
		return totalCount;
	}


	@Override
	public List<String> getStoreList() {
		List<String> afOfferDealLink= null;
		try{
			afOfferDealLink=offersDealsDAO.getStoreList();
		}
		catch(Exception e){
			logger.error("@@@@Exception in OffersDealServiceImpl at getStoreList() :", e);
		}
		return afOfferDealLink;
	}

	
}
