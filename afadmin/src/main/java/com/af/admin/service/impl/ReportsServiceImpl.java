package com.af.admin.service.impl;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.af.admin.DAO.CommissionManagementDAO;
import com.af.admin.DAO.ReportsDAO;
import com.af.admin.DAO.TransactionDetailsDAO;
import com.af.admin.beans.BuisnessReportsBean;
import com.af.admin.beans.InvalidCsvReprot;
import com.af.admin.beans.JPMilesStatusUpdateBean;
import com.af.admin.beans.zeroCsvReport;
import com.af.admin.constants.AfConstants;
import com.af.admin.entity.AfBanner;
import com.af.admin.entity.AfCategory;
import com.af.admin.entity.AfPartner;
import com.af.admin.entity.AfPartnerProduct;
import com.af.admin.entity.AfProduct;
import com.af.admin.entity.AfTransaction;
import com.af.admin.entity.AfTransactionDetails;
import com.af.admin.entity.PartnerReportIn;
import com.af.admin.service.CategoryManagementService;
import com.af.admin.service.CommonService;
import com.af.admin.service.PartnerManagementService;
import com.af.admin.service.ReportsService;
import com.af.admin.utils.ServiceProviderUtil;

@Component
@Transactional(readOnly = true)
public class ReportsServiceImpl implements ReportsService {
	private Logger logger = LogManager.getLogger(ReportsServiceImpl.class.getName());

	@Autowired
	ReportsDAO reportsDAO;

	@Autowired
	CommonService commonService;

	@Autowired
	TransactionDetailsDAO transactionDetailsDAO;

	@Autowired
	CategoryManagementService categoryManagementService;

	@Autowired
	PartnerManagementService partnerManagementService;
	
	@Autowired
	CommissionManagementDAO commissionManagementDAO;

	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

	@Override
	public List<BuisnessReportsBean> getBuisnessReportsList(String fromDate, String toDate, String prtNo, String jpNumber,String status,String statusType,int length,int start) {
		
		BuisnessReportsBean buisnessReportsBean = null;
		String partnerProductId = "";
		String productName="";
		String partnerCategoryName="";
		String jpplCategoryName="";
		List<AfPartnerProduct> afPartnerProductList = null;
		
		List<Object[]> reportObject = null;
		List<BuisnessReportsBean> reportList = null;
		String transactionType;
		String productStatus;
		int count=1;
		try {
			reportObject = reportsDAO.getShopJPReports(fromDate, toDate, prtNo, jpNumber,status,statusType,length,start);
			Map<Byte, String> memberGuestMap = commonService.getEnumValues(AfConstants.MEMBER_OR_GUEST);
			if(reportObject != null && !reportObject.isEmpty()){
				reportList = new ArrayList<>();
				
				for(Object[] row : reportObject){
					buisnessReportsBean = new BuisnessReportsBean();
					buisnessReportsBean.setJpNumber(ServiceProviderUtil.checkNullSafeWithObject(row[0], "NA"));
					buisnessReportsBean.setSource(ServiceProviderUtil.checkNullSafeWithObject(row[1], "NA"));
					buisnessReportsBean.setMedium(ServiceProviderUtil.checkNullSafeWithObject(row[2], "NA"));
					buisnessReportsBean.setCampaign(ServiceProviderUtil.checkNullSafeWithObject(row[3], "NA"));
                                  
					if(status.equals("JP Number Missing") || status.equals("Reference Number Missing") || status.equals("JP And Reference Number Missing") || status.equals("Others") ) {
						buisnessReportsBean.setPartnerOrderId("NA");
					}
					else
					{
						buisnessReportsBean.setPartnerOrderId(ServiceProviderUtil.checkNullSafeWithObject(row[24], "NA"));
					}
					
					
					buisnessReportsBean.setMember_or_guest((memberGuestMap.get(row[4])==null)?"NA":memberGuestMap.get(row[4]));
					buisnessReportsBean.setTransactionId(ServiceProviderUtil.checkNullSafeWithObject(row[5], "NA"));
					buisnessReportsBean.setPartnerItemId(ServiceProviderUtil.checkNullSafeWithObject(row[6], "NA"));
					buisnessReportsBean.setFormatedTransactionDate((row[7] != null) ? simpleDateFormat.format(row[7]) : "NA");
					buisnessReportsBean.setOrderDateTime(
							(row[7] != null) ? ((Date)row[7]).getTime() : 0);
					buisnessReportsBean.setPartnerName(ServiceProviderUtil.checkNullSafeWithObject(row[8], "NA"));
					
					partnerProductId = ServiceProviderUtil.checkNullSafeWithObject(row[18], "");
					if(null!=partnerProductId && !partnerProductId.isEmpty()){

						afPartnerProductList=transactionDetailsDAO.getProductNameById(partnerProductId);  
						for(AfPartnerProduct afPartnerProduct:afPartnerProductList){
							productName 	= ServiceProviderUtil.checkNullSafeWithObject(afPartnerProduct.getProductName(),"NA");						
							partnerCategoryName	= ServiceProviderUtil.checkNullSafeWithObject(afPartnerProduct.getAfPartnerCategory().getPartnerCategoryName(),"NA");
							jpplCategoryName = ServiceProviderUtil.checkNullSafeWithObject(afPartnerProduct.getAfPartnerCategory().getAfCategory().getCategoryName(),"NA");
//							System.out.println("jpplCat--->"+jpplCategoryName);
							buisnessReportsBean.setJpplCategoryName(jpplCategoryName);
						}

					}
					if(jpplCategoryName==null || jpplCategoryName.isEmpty()){
						buisnessReportsBean.setJpplCategoryName("NA");
					}else{
						buisnessReportsBean.setJpplCategoryName(jpplCategoryName);
					}
					
					if(partnerCategoryName==null || partnerCategoryName.isEmpty()){
						buisnessReportsBean.setPartnerCategoryName("NA");
					}else{
						buisnessReportsBean.setPartnerCategoryName(partnerCategoryName);
					}
					if(productName == null || productName.isEmpty()){
						buisnessReportsBean.setProductName("NA");
					}else{
						buisnessReportsBean.setProductName(productName);
					}
					
						
					buisnessReportsBean.setPrice(ServiceProviderUtil.checkNullSafeWithObject(row[9], "NA"));
					buisnessReportsBean.setRatePerMile(ServiceProviderUtil.checkNullSafeWithObject(row[10], "NA"));
					buisnessReportsBean.setCommissionRate(ServiceProviderUtil.checkNullSafeWithObject(row[11],"NA"));
					buisnessReportsBean.setMemberShare(ServiceProviderUtil.checkNullSafeWithObject(row[12],"NA"));
					if(!"".equals(row[13]))
						buisnessReportsBean.setJpmilesPurchase(row[13].toString());	
					else
						buisnessReportsBean.setJpmilesPurchase("NA");
					
					buisnessReportsBean.setFormatedExpectedDateOfCredit(
							(row[14] != null && !"".equals(row[14])) ? simpleDateFormat.format(
									row[14]) : "NA");
					buisnessReportsBean.setExpectedDateOfCreditTime((row[14] != null && !row[14].equals("")) ? ((Date)row[14]).getTime() : 0);
					if(!"".equals(row[15])){
						buisnessReportsBean.setJpmilesCancel(row[15].toString());
					}else{
						buisnessReportsBean.setJpmilesCancel("NA");

					}
					if(!"".equals(row[16])){
						buisnessReportsBean.setJpmilesToCredit(row[16].toString());
					}else{
						buisnessReportsBean.setJpmilesToCredit("NA");

					}
						
					productStatus=ServiceProviderUtil.checkNullSafeWithObject(row[17], "NA");
					if("1".equalsIgnoreCase(productStatus)){
						buisnessReportsBean.setPurchase_status(AfConstants.TENTATIVE);	
					}else if("2".equalsIgnoreCase(productStatus)){
						buisnessReportsBean.setPurchase_status(AfConstants.PROCESSED);	
					}else if("3".equalsIgnoreCase(productStatus)){
						buisnessReportsBean.setPurchase_status(AfConstants.DISAPPROVED);	
					}else if("4".equalsIgnoreCase(productStatus)){
						buisnessReportsBean.setPurchase_status(AfConstants.FAILED);	
					}else{
						buisnessReportsBean.setPurchase_status("NA");	
					}
					buisnessReportsBean.setPartnerProductId(ServiceProviderUtil.checkNullSafeWithObject(row[18], "NA"));
					transactionType=ServiceProviderUtil.checkNullSafeWithObject(row[19], "NA");
					if(transactionType.equalsIgnoreCase("7"))
						buisnessReportsBean.setTransactionVia(AfConstants.CHROME_EXTENSION);
					else
						buisnessReportsBean.setTransactionVia(AfConstants.SHOP_SITE);
					buisnessReportsBean.setTax("NA");
					String prtCommAmount=ServiceProviderUtil.checkNullSafeWithObject(row[20], "NA");
					if(prtCommAmount.contains(",")){
						prtCommAmount=prtCommAmount.replace(",", "");
					}
					if (prtCommAmount.contains(".") && (prtCommAmount.length()-1-prtCommAmount.indexOf(".")>2)) {
						prtCommAmount = prtCommAmount.substring(0, prtCommAmount.indexOf(".")+3);
					}
						buisnessReportsBean.setCommAmount(prtCommAmount);
						
						System.out.println("row[9]"+row[9]);
						System.out.println("row[11]"+row[11]);
						
					if (row[9] != null && row[9].toString().length() > 0 && row[11] != null && row[11].toString().length() > 0 && !AfConstants.INVALID.equals(statusType)) {
//					if (row[9] > "0" && row[11] > "0" && row[11] != "" && !AfConstants.INVALID.equals(statusType)) {	
						Float jpplCommission = (((Float) row[9])* ((Float) row[11])/100);
						DecimalFormat df = new DecimalFormat("#.##");
						String jpplCommissions = df.format(jpplCommission);
						buisnessReportsBean.setJpplCommAmount(jpplCommissions);
					} else {
						buisnessReportsBean.setJpplCommAmount("NA");
					}
					if((Integer) row[21] == AfConstants.AMAZON_PRT_NO ){
					String partnerCategory = ServiceProviderUtil.checkNullSafeWithObject(row[22],"NA");
					buisnessReportsBean.setPartnerCategoryName(partnerCategory);
					jpplCategoryName = categoryManagementService.getJpplCatByPrtCategory(partnerCategory,(Integer) row[21]);
					}
					if(jpplCategoryName.equalsIgnoreCase("") || jpplCategoryName == null){
						buisnessReportsBean.setJpplCategoryName("NA");
					}else{
						buisnessReportsBean.setJpplCategoryName(jpplCategoryName);
					}
					reportList.add(buisnessReportsBean);
					
				}
			}
			
			
			
		} catch(Exception e) {
			e.printStackTrace();
			logger.error("@@@@ Exception in ReportsServiceImpl of getBuisnessReportsList() :", e);
		}
		return reportList;
	}
	
	

	@Override
	public List<BuisnessReportsBean> getFinanceReportsList(String fromDate,
			String toDate, String prtNo, String jpNumber,String status,String statusType,int length,int start) {
		List<BuisnessReportsBean> financeReportlist = new ArrayList<>();
		BuisnessReportsBean financeReportsBean = null;
		List<Object[]> reportObject = null;
		String transactionType;
		String productStatus;
		try {
			reportObject =reportsDAO.getShopJPReports(fromDate, toDate, prtNo, jpNumber, status,statusType,length,start);
			Map<Byte, String> memberGuestMap = commonService.getEnumValues(AfConstants.MEMBER_OR_GUEST);

			if(reportObject != null && !reportObject.isEmpty()){
				financeReportlist = new ArrayList<>();
				
				for(Object[] row : reportObject){
					financeReportsBean = new BuisnessReportsBean();
					financeReportsBean.setJpNumber(ServiceProviderUtil.checkNullSafeWithObject(row[0], "NA"));
					financeReportsBean.setSource(ServiceProviderUtil.checkNullSafeWithObject(row[1], "NA"));
					financeReportsBean.setMedium(ServiceProviderUtil.checkNullSafeWithObject(row[2], "NA"));
					financeReportsBean.setCampaign(ServiceProviderUtil.checkNullSafeWithObject(row[3], "NA"));
					financeReportsBean.setMember_or_guest((memberGuestMap.get(row[4])==null)?"NA":memberGuestMap.get(row[4]));
					financeReportsBean.setPartnerItemId(ServiceProviderUtil.checkNullSafeWithObject(row[6], "NA"));
					financeReportsBean.setCommissionRate(ServiceProviderUtil.checkNullSafeWithObject(row[11], "NA"));
					financeReportsBean.setMemberShare(ServiceProviderUtil.checkNullSafeWithObject(row[12], "NA"));
					financeReportsBean.setTax("NA");
					if(!"".equals(row[16])){
						financeReportsBean.setJpmilesToCredit(row[16].toString());
					}else{
						financeReportsBean.setJpmilesToCredit("NA");

					}
					productStatus=ServiceProviderUtil.checkNullSafeWithObject(row[17], "NA");
					if("1".equalsIgnoreCase(productStatus)){
						financeReportsBean.setPurchase_status(AfConstants.TENTATIVE);	
					}else if("2".equalsIgnoreCase(productStatus)){
						financeReportsBean.setPurchase_status(AfConstants.PROCESSED);	
					}else if("3".equalsIgnoreCase(productStatus)){
						financeReportsBean.setPurchase_status(AfConstants.DISAPPROVED);	
					}else if("4".equalsIgnoreCase(productStatus)){
						financeReportsBean.setPurchase_status(AfConstants.FAILED);	
					}else{
						financeReportsBean.setPurchase_status("NA");	
					}
					
					transactionType=ServiceProviderUtil.checkNullSafeWithObject(row[19], "NA");
					if(transactionType.equalsIgnoreCase("7"))
						financeReportsBean.setTransactionVia(AfConstants.CHROME_EXTENSION);
					else
						financeReportsBean.setTransactionVia(AfConstants.SHOP_SITE);
					financeReportsBean.setRatePerMile(ServiceProviderUtil.checkNullSafeWithObject(row[10], "NA"));
					String prtCommAmount=ServiceProviderUtil.checkNullSafeWithObject(row[20], "NA");
					if(prtCommAmount.contains(",")){
						prtCommAmount=prtCommAmount.replace(",", "");
					}
					if (prtCommAmount.contains(".") && (prtCommAmount.length()-1-prtCommAmount.indexOf(".")>2)) {
						prtCommAmount = prtCommAmount.substring(0, prtCommAmount.indexOf(".")+3);
					}
						financeReportsBean.setCommAmount(prtCommAmount);

						if (row[9] != null && row[9].toString().length() > 0 && row[11] != null && row[11].toString().length() > 0 && !AfConstants.INVALID.equals(statusType)) {
						Float jpplCommission = (((Float) row[9])* ((Float) row[11])/100);
						DecimalFormat df = new DecimalFormat("#.##");
						String jpplCommissions = df.format(jpplCommission);
						financeReportsBean.setJpplCommAmount(jpplCommissions);
					} else {
						financeReportsBean.setJpplCommAmount("NA");
					}


					financeReportlist.add(financeReportsBean);
				}
			}
		} catch(Exception e) {
			logger.error("@@@@ Exception in ReportsServiceImpl of getFinanceReportsList() :",e);
		}
		return financeReportlist;
	}

	@Override
	public List<BuisnessReportsBean> getAnalyticsReportsList(String fromDate,
			String toDate, String prtNo, String jpNumber,String status,String statusType,int length,int start) {
		List<BuisnessReportsBean> analyticsReportlist = new ArrayList<>();
		BuisnessReportsBean analyticsReportsBean = null;
		String partnerProductId = "";
		String productName="";
		String partnerCategoryName="";
		String jpplCategoryName="";
		List<AfPartnerProduct> afPartnerProductList = null;
		List<Object[]> reportObject = null;
		String transactionType;
		String productStatus;
		try {
			reportObject =reportsDAO.getShopJPReports(fromDate, toDate, prtNo, jpNumber, status,statusType,length,start);
			Map<Byte, String> memberGuestMap = commonService.getEnumValues(AfConstants.MEMBER_OR_GUEST);


			if(reportObject != null && !reportObject.isEmpty()){
				analyticsReportlist = new ArrayList<>();
				
				for(Object[] row : reportObject){
					analyticsReportsBean = new BuisnessReportsBean();
					analyticsReportsBean.setJpNumber(ServiceProviderUtil.checkNullSafeWithObject(row[0], "NA"));
					analyticsReportsBean.setSource(ServiceProviderUtil.checkNullSafeWithObject(row[1], "NA"));
					analyticsReportsBean.setMedium(ServiceProviderUtil.checkNullSafeWithObject(row[2], "NA"));
					analyticsReportsBean.setCampaign(ServiceProviderUtil.checkNullSafeWithObject(row[3], "NA"));
					analyticsReportsBean.setMember_or_guest((memberGuestMap.get(row[4])==null)?"NA":memberGuestMap.get(row[4]));
					analyticsReportsBean.setTransactionId(ServiceProviderUtil.checkNullSafeWithObject(row[5], "NA"));
					analyticsReportsBean.setPartnerItemId(ServiceProviderUtil.checkNullSafeWithObject(row[6], "NA"));
					analyticsReportsBean.setFormatedTransactionDate(
							(row[7] != null && !row[7].equals("")) ? simpleDateFormat.format(row[7]) : "NA");
					analyticsReportsBean.setOrderDateTime(
							(row[7] != null && !row[7].equals("")) ? ((Date)row[7]).getTime() : 0);
					analyticsReportsBean.setPartnerName(ServiceProviderUtil.checkNullSafeWithObject(row[8], "NA"));
					partnerProductId =ServiceProviderUtil.checkNullSafeWithObject(row[18], "");
					if(null!=partnerProductId && !partnerProductId.isEmpty()){

						afPartnerProductList=transactionDetailsDAO.getProductNameById(partnerProductId);  
						for(AfPartnerProduct afPartnerProduct:afPartnerProductList){
							productName 	= ServiceProviderUtil.nullSafe(afPartnerProduct.getProductName(),"NA");						
							partnerCategoryName	= ServiceProviderUtil.nullSafe(afPartnerProduct.getAfPartnerCategory().getPartnerCategoryName().toString(),"NA");
							
						}													
						
					}
					if(partnerCategoryName==null || partnerCategoryName.isEmpty()){
						analyticsReportsBean.setPartnerCategoryName("NA");
					}else{
						analyticsReportsBean.setPartnerCategoryName(partnerCategoryName);
					}
					if(productName == null || productName.isEmpty()){
						analyticsReportsBean.setProductName("NA");
					}else{
						analyticsReportsBean.setProductName(productName);
					}
					
					analyticsReportsBean.setJpplCategoryName("NA");
					analyticsReportsBean.setPrice(ServiceProviderUtil.checkNullSafeWithObject(row[9], "NA"));
					if(!"".equals(row[13])){
					analyticsReportsBean.setJpmilesPurchase(row[13].toString());
					}else{
						analyticsReportsBean.setJpmilesPurchase("NA");

					}
					analyticsReportsBean.setFormatedExpectedDateOfCredit(
							(row[14] != null && !row[14].equals("")) ? simpleDateFormat.format(
									row[14]) : "NA");
					analyticsReportsBean.setExpectedDateOfCreditTime(
							(row[14] != null && !row[14].equals("")) ? ((Date)row[14]).getTime() : 0);
					if(!"".equals(row[15])){
					analyticsReportsBean.setJpmilesCancel(row[15].toString());
					}else{
						analyticsReportsBean.setJpmilesCancel("NA");

					}
					if(!"".equals(row[16])){
					analyticsReportsBean.setJpmilesToCredit(row[16].toString());
					}else{
						analyticsReportsBean.setJpmilesToCredit("NA");

					}
					productStatus=ServiceProviderUtil.checkNullSafeWithObject(row[17], "NA");
					if("1".equalsIgnoreCase(productStatus)){
						analyticsReportsBean.setPurchase_status(AfConstants.TENTATIVE);	
					}else if("2".equalsIgnoreCase(productStatus)){
						analyticsReportsBean.setPurchase_status(AfConstants.PROCESSED);	
					}else if("3".equalsIgnoreCase(productStatus)){
						analyticsReportsBean.setPurchase_status(AfConstants.DISAPPROVED);	
					}else if("4".equalsIgnoreCase(productStatus)){
						analyticsReportsBean.setPurchase_status(AfConstants.FAILED);	
					}else{
						analyticsReportsBean.setPurchase_status("NA");	
					}
					
					transactionType=ServiceProviderUtil.checkNullSafeWithObject(row[19], "NA");
					if(transactionType.equalsIgnoreCase("7"))
						analyticsReportsBean.setTransactionVia(AfConstants.CHROME_EXTENSION);
					else
						analyticsReportsBean.setTransactionVia(AfConstants.SHOP_SITE);
					analyticsReportsBean.setRatePerMile(ServiceProviderUtil.checkNullSafeWithObject(row[10], "NA"));
					String prtCommAmount=ServiceProviderUtil.checkNullSafeWithObject(row[20], "NA");
					if(prtCommAmount.contains(",")){
						prtCommAmount=prtCommAmount.replace(",", "");
					}
					if (prtCommAmount.contains(".") && (prtCommAmount.length()-1-prtCommAmount.indexOf(".")>2)) {
						prtCommAmount = prtCommAmount.substring(0, prtCommAmount.indexOf(".")+3);
					}
					analyticsReportsBean.setCommAmount(prtCommAmount);
					if (row[9] != null && row[9].toString().length() > 0 && row[11] != null && row[11].toString().length() > 0 && !AfConstants.INVALID.equals(statusType)) {
						Float jpplCommission = (((Float) row[9])* ((Float) row[11])/100);
						DecimalFormat df = new DecimalFormat("#.##");
						String jpplCommissions = df.format(jpplCommission);
						analyticsReportsBean.setJpplCommAmount(jpplCommissions);
					} else {
						analyticsReportsBean.setJpplCommAmount("NA");
					}
					if((Integer) row[21] == AfConstants.AMAZON_PRT_NO ){
						String partnerCategory = ServiceProviderUtil.checkNullSafeWithObject(row[22],"NA");
						analyticsReportsBean.setPartnerCategoryName(partnerCategory);
						jpplCategoryName = categoryManagementService.getJpplCatByPrtCategory(partnerCategory,(Integer) row[21]);
						}
					if(jpplCategoryName.equalsIgnoreCase("") || jpplCategoryName == null){
						analyticsReportsBean.setJpplCategoryName("NA");
					}else{
						analyticsReportsBean.setJpplCategoryName(jpplCategoryName);
					}
					analyticsReportlist.add(analyticsReportsBean);
				}
			}
		} catch(Exception e) {
			
			logger.error("@@@@ Exception in ReportsServiceImpl of getAnalyticsReportsList() :", e);
		}
		return analyticsReportlist;
	}

	@Override
	public List<BuisnessReportsBean> getServiceCenterReportsList(String fromDate,
			String toDate, String prtNo, String jpNumber,String status,String statusType,int length,int start) {
		List<BuisnessReportsBean> serviceCenterReportlist = new ArrayList<>();
		BuisnessReportsBean serviceCenterReportsBean = null;
		String partnerProductId = "";
		String productName="";
		String partnerCategoryName="";
		String jpplCategoryName="";
		List<AfPartnerProduct> afPartnerProductList = null;
		List<Object[]> reportObject = null;
		String transactionType;
		String productStatus;
		try {
			reportObject =reportsDAO.getShopJPReports(fromDate, toDate, prtNo, jpNumber, status,statusType,length,start);
			Map<Byte, String> memberGuestMap = commonService.getEnumValues(AfConstants.MEMBER_OR_GUEST);


			if(reportObject != null && !reportObject.isEmpty()){
				serviceCenterReportlist = new ArrayList<>();				
				for(Object[] row : reportObject){
					serviceCenterReportsBean = new BuisnessReportsBean();
					serviceCenterReportsBean.setJpNumber(ServiceProviderUtil.checkNullSafeWithObject(row[0], "NA"));
					serviceCenterReportsBean.setSource(ServiceProviderUtil.checkNullSafeWithObject(row[1], "NA"));
					serviceCenterReportsBean.setMedium(ServiceProviderUtil.checkNullSafeWithObject(row[2], "NA"));
					serviceCenterReportsBean.setCampaign(ServiceProviderUtil.checkNullSafeWithObject(row[3], "NA"));
					serviceCenterReportsBean.setMember_or_guest((memberGuestMap.get(row[4])==null)?"NA":memberGuestMap.get(row[4]));
					serviceCenterReportsBean.setTransactionId(ServiceProviderUtil.checkNullSafeWithObject(row[5], "NA"));
					serviceCenterReportsBean.setPartnerItemId(ServiceProviderUtil.checkNullSafeWithObject(row[6], "NA"));
					serviceCenterReportsBean.setFormatedTransactionDate(
							(row[7] != null && !row[7].equals("")) ? simpleDateFormat.format(row[7]) : "NA");
					serviceCenterReportsBean.setOrderDateTime(
							(row[7] != null && !row[7].equals("")) ? ((Date)row[7]).getTime() : 0);
					serviceCenterReportsBean.setPartnerName(ServiceProviderUtil.checkNullSafeWithObject(row[8], "NA"));
					partnerProductId = ServiceProviderUtil.checkNullSafeWithObject(row[18], "");
					if(null!=partnerProductId && !partnerProductId.isEmpty()){

						afPartnerProductList=transactionDetailsDAO.getProductNameById(partnerProductId); 
						for(AfPartnerProduct afPartnerProduct:afPartnerProductList){
							productName 	= ServiceProviderUtil.nullSafe(afPartnerProduct.getProductName(),"NA");						
							partnerCategoryName	= ServiceProviderUtil.nullSafe(afPartnerProduct.getAfPartnerCategory().getPartnerCategoryName(), "NA");
						}													
					}
					if(partnerCategoryName==null || partnerCategoryName.isEmpty()){
						serviceCenterReportsBean.setPartnerCategoryName("NA");
					}else{
						serviceCenterReportsBean.setPartnerCategoryName(partnerCategoryName);
					}
					if(productName == null || productName.isEmpty()){
						serviceCenterReportsBean.setProductName("NA");
					}else{
						serviceCenterReportsBean.setProductName(productName);
					}
					
					serviceCenterReportsBean.setJpplCategoryName("NA");
					serviceCenterReportsBean.setPrice(ServiceProviderUtil.checkNullSafeWithObject(row[9], "NA"));
					if(!"".equals(row[13])){
					serviceCenterReportsBean.setJpmilesPurchase(row[13].toString());
					}else{
						serviceCenterReportsBean.setJpmilesPurchase("NA");
					}
					serviceCenterReportsBean.setFormatedExpectedDateOfCredit(
							(row[14] != null && !row[14].equals("")) ? simpleDateFormat.format(
									row[14]) : "NA");
					serviceCenterReportsBean.setExpectedDateOfCreditTime(
							(row[14] != null && !row[14].equals("")) ? ((Date)row[14]).getTime() : 0);
					if(!"".equals(row[15])){
					serviceCenterReportsBean.setJpmilesCancel(row[15].toString());
					}else{
						serviceCenterReportsBean.setJpmilesCancel("NA");

					}
					if(!"".equals(row[16])){
					serviceCenterReportsBean.setJpmilesToCredit(row[16].toString());
					}else{
						serviceCenterReportsBean.setJpmilesToCredit("NA");

					}
					productStatus=ServiceProviderUtil.checkNullSafeWithObject(row[17], "NA");
					if("1".equalsIgnoreCase(productStatus)){
						serviceCenterReportsBean.setPurchase_status(AfConstants.TENTATIVE);	
					}else if("2".equalsIgnoreCase(productStatus)){
						serviceCenterReportsBean.setPurchase_status(AfConstants.PROCESSED);	
					}else if("3".equalsIgnoreCase(productStatus)){
						serviceCenterReportsBean.setPurchase_status(AfConstants.DISAPPROVED);	
					}else if("4".equalsIgnoreCase(productStatus)){
						serviceCenterReportsBean.setPurchase_status(AfConstants.FAILED);	
					}else{
						serviceCenterReportsBean.setPurchase_status("NA");	
					}
					transactionType=ServiceProviderUtil.checkNullSafeWithObject(row[19], "NA");
					if(transactionType.equalsIgnoreCase("7"))
						serviceCenterReportsBean.setTransactionVia(AfConstants.CHROME_EXTENSION);
					else
						serviceCenterReportsBean.setTransactionVia(AfConstants.SHOP_SITE);
					
					serviceCenterReportsBean.setRatePerMile(ServiceProviderUtil.checkNullSafeWithObject(row[10], "NA"));
					String prtCommAmount=ServiceProviderUtil.checkNullSafeWithObject(row[20], "NA");
					if(prtCommAmount.contains(",")){
						prtCommAmount=prtCommAmount.replace(",", "");
					}
					if (prtCommAmount.contains(".") && (prtCommAmount.length()-1-prtCommAmount.indexOf(".")>2)) {
						prtCommAmount = prtCommAmount.substring(0, prtCommAmount.indexOf(".")+3);
					}
					serviceCenterReportsBean.setCommAmount(prtCommAmount);
					
//					if (row[9] != null && row[11] != null && !AfConstants.INVALID.equals(statusType)) {
					if (row[9] != null && row[9].toString().length() > 0 && row[11] != null && row[11].toString().length() > 0 && !AfConstants.INVALID.equals(statusType)) {
						Float jpplCommission = (((Float) row[9])* ((Float) row[11])/100);
						DecimalFormat df = new DecimalFormat("#.##");
						String jpplCommissions = df.format(jpplCommission);
						serviceCenterReportsBean.setJpplCommAmount(jpplCommissions);
					} else {
						serviceCenterReportsBean.setJpplCommAmount("NA");
					}
					if((Integer) row[21] == AfConstants.AMAZON_PRT_NO ){
						String partnerCategory = ServiceProviderUtil.checkNullSafeWithObject(row[22],"NA");
						serviceCenterReportsBean.setPartnerCategoryName(partnerCategory);
						jpplCategoryName = categoryManagementService.getJpplCatByPrtCategory(partnerCategory,(Integer) row[21]);
						}
					if(jpplCategoryName.equalsIgnoreCase("") || jpplCategoryName == null){
						serviceCenterReportsBean.setJpplCategoryName("NA");
					}else{
						serviceCenterReportsBean.setJpplCategoryName(jpplCategoryName);
					}
					serviceCenterReportlist.add(serviceCenterReportsBean);
				}
			}
		} catch(Exception e) {
			logger.error("@@@@ Exception in ReportsServiceImpl of getServiceCenterReportsList() :", e);
			e.printStackTrace();
		}
		return serviceCenterReportlist;
	}

	public List<BuisnessReportsBean> getPartnersByProducts() {
		List<Object> afPartnerList = new ArrayList<>();
		List<BuisnessReportsBean> partnerList = new ArrayList<>();
		BuisnessReportsBean reportsBean = new BuisnessReportsBean();

		try {
			afPartnerList = reportsDAO.getPartnersByProducts();

			for (int i = 0; i < afPartnerList.size(); i++) {
				Object[] obj = (Object[]) afPartnerList.get(i);

				reportsBean = new BuisnessReportsBean();
				reportsBean.setPartnerName(obj[0] != null ? String.valueOf(obj[0]) : "");
				reportsBean.setPrtNo(obj[1] != null ? String.valueOf(obj[1]) : "");
				partnerList.add(reportsBean);
			}
		} catch (Exception e) {
			logger.error("@@@@ Exception in ReportsServiceImpl getPartnersByProducts() :" + e);
		}
		return partnerList;
	}

	public List<BuisnessReportsBean> getBrandsByProducts(String toDate, String fromDate) {
		List<String> afBrandList = null;
		List<BuisnessReportsBean> brandList = new ArrayList<>();
		BuisnessReportsBean reportsBean = new BuisnessReportsBean();

		try {
			afBrandList = reportsDAO.getBrandsByProducts(toDate, fromDate);
			Set<String> afBrandSet = new HashSet<>(afBrandList);
			for (String brandName : afBrandSet) {
				reportsBean = new BuisnessReportsBean();
				reportsBean.setBrandName(brandName != null ? brandName : "");
				brandList.add(reportsBean);
			}
		} catch (Exception e) {
			logger.error("@@@@ Exception in ReportsServiceImpl getBrandsByProducts() :" + e);
		}
		return brandList;
	}

	@Override
	public List<BuisnessReportsBean> getNewProductsReportList(BuisnessReportsBean newProductsReport) {
		List<BuisnessReportsBean> newProductsReportList = new ArrayList<>();
		BuisnessReportsBean newProductsReportsBean = null;
		List<AfPartnerProduct> queryResults = null;

		try {
			String fromDate = newProductsReport.getFromDate();
			String toDate = newProductsReport.getToDate();
			String partnerNo = newProductsReport.getPartnerName();
			String brandName = newProductsReport.getBrandName();

			queryResults = reportsDAO.getNewProductsReportList(fromDate, toDate, partnerNo, brandName);

			if (queryResults != null && !queryResults.isEmpty()) {
				for (AfPartnerProduct afPartnerProduct : queryResults) {
					newProductsReportsBean = new BuisnessReportsBean();

					newProductsReportsBean.setPartnerName(afPartnerProduct.getAfPartner().getPartnerName());
					newProductsReportsBean.setBrandName(afPartnerProduct.getProductBrand());
					newProductsReportsBean.setProductID(afPartnerProduct.getProductId());
					newProductsReportsBean.setProductName(afPartnerProduct.getProductName());
					newProductsReportsBean.setPrice(afPartnerProduct.getProductPrice().toString());

					newProductsReportsBean.setProductLandingURL(afPartnerProduct.getProductLandingUrl());
					newProductsReportsBean.setProductImageURL(afPartnerProduct.getProductImageUrl());
					newProductsReportsBean
							.setCategoryName(afPartnerProduct.getAfProduct().getAfCategory().getCategoryName());

					newProductsReportList.add(newProductsReportsBean);
				}
			}
		} catch (Exception e) {
			logger.error("@@@@ Exception in ReportsServiceImpl of getNewProductsReportList() :", e);
		}
		return newProductsReportList;
	}

	@Override
	public List<BuisnessReportsBean> getMappedProductsReportList(BuisnessReportsBean mappedProductsReport) {
		List<BuisnessReportsBean> mappedProductsReportList = new ArrayList<>();
		BuisnessReportsBean mappedProductsReportsBean = null;
		List<AfProduct> queryResults = null;

		try {
			String selectBy = mappedProductsReport.getSelectBy();
			String selectByValue = mappedProductsReport.getSelectByValue();

			selectBy = ServiceProviderUtil.nullSafe(selectBy, "");
			selectByValue = ServiceProviderUtil.nullSafe(selectByValue, "");

			switch (selectBy) {
			case "JPPLCAT":
				AfCategory afCategory = categoryManagementService.getCategoryByCategoryName(selectByValue);
				selectByValue = afCategory != null ? String.valueOf(afCategory.getCatNo()) : "0";
				break;

			case "PRTNAME":
				AfPartner afPartner = partnerManagementService.getPartnerByName(selectByValue);
				selectByValue = afPartner != null ? String.valueOf(afPartner.getPrtNo()) : "0";
				break;
			}
			queryResults = reportsDAO.getMappedProductsReportList(selectBy, selectByValue);

			if (queryResults != null && !queryResults.isEmpty()) {
				for (AfProduct afProduct : queryResults) {
					for (AfPartnerProduct afPartnerProduct : afProduct.getAfPartnerProducts()) {
						if ((("PRTNAME".equalsIgnoreCase(selectBy)) && !(String
								.valueOf(afPartnerProduct.getAfPartner().getPrtNo()).equalsIgnoreCase(selectByValue)))
								|| (("PID".equalsIgnoreCase(selectBy)) && !(String
										.valueOf(afPartnerProduct.getProductId()).equalsIgnoreCase(selectByValue)))) {
							continue;
						}
						mappedProductsReportsBean = new BuisnessReportsBean();
						mappedProductsReportsBean.setPartnerName(afPartnerProduct.getAfPartner().getPartnerName());
						mappedProductsReportsBean.setBrandName(afPartnerProduct.getProductBrand());
						mappedProductsReportsBean.setProductID(afPartnerProduct.getProductId());
						mappedProductsReportsBean.setProductName(afPartnerProduct.getProductName());
						mappedProductsReportsBean
								.setCategoryName(afPartnerProduct.getAfProduct().getAfCategory().getCategoryName());
						mappedProductsReportsBean.setMasterPID(afPartnerProduct.getAfProduct().getMasterProductId());
						mappedProductsReportList.add(mappedProductsReportsBean);
					}
				}
			}
		} catch (Exception e) {
			logger.error("@@@@ Exception in ReportsServiceImpl of getMappedProductsReportList() :", e);
		}
		return mappedProductsReportList;
	}

	@Override
	public List<BuisnessReportsBean> getdailyMilesReportsList(String fromDate, String toDate, String partnerName,
			String jpNumber) {
		List<BuisnessReportsBean> dailyMilesStatusReportList = new ArrayList<>();
		BuisnessReportsBean dailyReportBean = null;
		List<BuisnessReportsBean> results = null;
		String partnerProductId = "";
		String productName = "";
		List<AfPartnerProduct> afPartnerProductList = null;
		String crisRequest = "";
		String crisResponse;
		byte transactionType;
		try {
			results = reportsDAO.getdailyMilesReports(fromDate, toDate, partnerName, jpNumber);
			Map<Byte, String> mapjpMilesStatus = commonService.getEnumValues("JPMILE_STATUS");

			if (results != null && !results.isEmpty()) {
				for (BuisnessReportsBean dailyMilesBean : results) {
					dailyReportBean = new BuisnessReportsBean();
					dailyReportBean.setJpNumber(dailyMilesBean.getJpNumber());
					dailyReportBean.setTransactionId(dailyMilesBean.getTransactionId());
					dailyReportBean.setFormatedTransactionDate((dailyMilesBean.getTransactionDate() != null)
							? simpleDateFormat.format(dailyMilesBean.getTransactionDate()) : "NA");
					dailyReportBean.setOrderDateTime((dailyMilesBean.getTransactionDate() != null)
							? dailyMilesBean.getTransactionDate().getTime() : 0);
					dailyReportBean.setPartnerName(dailyMilesBean.getPartnerName());
					partnerProductId = dailyMilesBean.getPartnerProductId();

					if (null != partnerProductId && !partnerProductId.isEmpty()) {

						afPartnerProductList = transactionDetailsDAO.getProductNameById(partnerProductId);
						for (AfPartnerProduct afPartnerProduct : afPartnerProductList) {
							productName = ServiceProviderUtil.nullSafe(afPartnerProduct.getProductName(), "");
							if (!productName.isEmpty()) {
								dailyReportBean.setProductName(productName);
							} else {
								dailyReportBean.setProductName("NA");
							}
						}

					}
					dailyReportBean.setJpmilesPurchase(dailyMilesBean.getJpmilesPurchase());
					dailyReportBean.setFormatedExpectedDateOfCredit((dailyMilesBean.getExpectedDateOfCredit() != null)
							? simpleDateFormat.format(dailyMilesBean.getExpectedDateOfCredit()) : "NA");
					dailyReportBean.setExpectedDateOfCreditTime((dailyMilesBean.getExpectedDateOfCredit() != null)
							? dailyMilesBean.getExpectedDateOfCredit().getTime() : 0);
					if (dailyMilesBean.getJpmilesStatus() == 4) {
						dailyReportBean.setJpmiles_Status("Failed");
					} else {
						dailyReportBean.setJpmiles_Status(mapjpMilesStatus.get(dailyMilesBean.getJpmilesStatus()));
					}
					dailyReportBean.setPartnerItemId(
							(dailyMilesBean.getPartnerItemId() == null) ? "NA" : (dailyMilesBean.getPartnerItemId()));
					crisRequest = reportsDAO.downloadCrisRequest(dailyMilesBean.getPartnerItemId());
					if (crisRequest == null) {
						dailyReportBean.setRequest(false);
					} else {
						dailyReportBean.setRequest(true);
					}
					crisResponse = reportsDAO.downloadCrisResponse(dailyMilesBean.getPartnerItemId());
					if (crisResponse == null) {
						dailyReportBean.setResponse(false);
					} else {
						dailyReportBean.setResponse(true);
						if (crisResponse.contains("<?xml")) {
							dailyReportBean.setIsXml(true);
						} else {
							dailyReportBean.setIsXml(false);
						}
					}

					transactionType = dailyMilesBean.getTransactionType();
					if (transactionType == 7)
						dailyReportBean.setTransactionVia(AfConstants.CHROME_EXTENSION);
					else
						dailyReportBean.setTransactionVia(AfConstants.SHOP_SITE);
					dailyMilesStatusReportList.add(dailyReportBean);
				}
			}
		} catch (Exception e) {
			logger.error("@@@@ Exception in ReportsServiceImpl of getdailyMilesReportsList() :", e);
		}
		return dailyMilesStatusReportList;
	}

	@Override
	public String downloadXmlFile(String orderId, String type) {
		orderId = (null != orderId) ? orderId.trim() : "";
		type = (null != type) ? type.trim() : "";

		String downloadText = "";

		try {
			if ("1".equalsIgnoreCase(type)) {
				downloadText = reportsDAO.downloadCrisRequest(orderId);
			}

			if ("2".equalsIgnoreCase(type)) {
				downloadText = reportsDAO.downloadCrisResponse(orderId);
			}

			if ("3".equalsIgnoreCase(type)) {
				downloadText = reportsDAO.downloadCrisRequest(orderId);
			}

			if ("4".equalsIgnoreCase(type)) {
				downloadText = reportsDAO.downloadCrisResponse(orderId);
			}
		} catch (Exception e) {
			logger.error("@@@@ Exception in ReportsServiceImpl downloadXmlFile() :", e);
		}

		return downloadText;

	}

	@Override
	public List<BuisnessReportsBean> getZeroJpMilesReport(String fromDate, String toDate, String prtNo, String jpNumber,
		String status, String statusType) {
		BuisnessReportsBean buisnessReportsBean = null;
		String partnerProductId = "";
		String productName = "";
		String partnerCategoryName = "";
		String jpplCategoryName = "";
		List<AfPartnerProduct> afPartnerProductList = null;

		List<Object[]> reportObject = null;
		List<BuisnessReportsBean> reportList = null;
		String transactionType;
		String productStatus;
		
		
		
		try {
			reportObject = reportsDAO.getShopJPReportsZeroMiles(fromDate, toDate, prtNo, jpNumber, status, statusType);
			Map<Byte, String> memberGuestMap = commonService.getEnumValues(AfConstants.MEMBER_OR_GUEST);
			if (reportObject != null && !reportObject.isEmpty()) {
				reportList = new ArrayList<>();

				for (Object[] row : reportObject) {
					if ((Integer) row[13] == 0) {
						buisnessReportsBean = new BuisnessReportsBean();
						buisnessReportsBean.setJpNumber(ServiceProviderUtil.checkNullSafeWithObject(row[0], "NA"));
						buisnessReportsBean.setSource(ServiceProviderUtil.checkNullSafeWithObject(row[1], "NA"));
						buisnessReportsBean.setMedium(ServiceProviderUtil.checkNullSafeWithObject(row[2], "NA"));
						buisnessReportsBean.setCampaign(ServiceProviderUtil.checkNullSafeWithObject(row[3], "NA"));
						buisnessReportsBean.setMember_or_guest(
								(memberGuestMap.get(row[4]) == null) ? "NA" : memberGuestMap.get(row[4]));
						buisnessReportsBean.setTransactionId(ServiceProviderUtil.checkNullSafeWithObject(row[5], "NA"));
						buisnessReportsBean.setPartnerItemId(ServiceProviderUtil.checkNullSafeWithObject(row[6], "NA"));
						buisnessReportsBean
								.setFormatedTransactionDate((row[7] != null) ? simpleDateFormat.format(row[7]) : "NA");
						buisnessReportsBean.setOrderDateTime((row[7] != null) ? ((Date) row[7]).getTime() : 0);
						buisnessReportsBean.setPartnerName(ServiceProviderUtil.checkNullSafeWithObject(row[8], "NA"));

						partnerProductId = ServiceProviderUtil.checkNullSafeWithObject(row[18], "");
						if (null != partnerProductId && !partnerProductId.isEmpty()) {

							afPartnerProductList = transactionDetailsDAO.getProductNameById(partnerProductId);
							for (AfPartnerProduct afPartnerProduct : afPartnerProductList) {
								productName = ServiceProviderUtil
										.checkNullSafeWithObject(afPartnerProduct.getProductName(), "NA");
								partnerCategoryName = ServiceProviderUtil.checkNullSafeWithObject(
										afPartnerProduct.getAfPartnerCategory().getPartnerCategoryName(), "NA");
							}
							buisnessReportsBean.setPartnerCategoryName(partnerCategoryName);
							buisnessReportsBean.setProductName(productName);
						} else {
							buisnessReportsBean.setPartnerCategoryName("NA");
							buisnessReportsBean.setProductName("NA");
						}
						buisnessReportsBean.setPrice(ServiceProviderUtil.checkNullSafeWithObject(row[9], "NA"));
						buisnessReportsBean.setRatePerMile(ServiceProviderUtil.checkNullSafeWithObject(row[10], "NA"));
						buisnessReportsBean
								.setCommissionRate(ServiceProviderUtil.checkNullSafeWithObject(row[11], "NA"));
						buisnessReportsBean.setMemberShare(ServiceProviderUtil.checkNullSafeWithObject(row[12], "NA"));
						if (!"".equals(row[13]))
							buisnessReportsBean.setJpmilesPurchase( row[13].toString());
                        if ((row[25]) !=null && !"".equals(row[25])){
                        	buisnessReportsBean.setBonusJpmiles(row[25].toString());
                        }else{
                        	buisnessReportsBean.setBonusJpmiles("NA");
                        }
						buisnessReportsBean.setFormatedExpectedDateOfCredit(
								(row[14] != null && !"".equals(row[14])) ? simpleDateFormat.format(row[14]) : "NA");
						buisnessReportsBean.setExpectedDateOfCreditTime(
								(row[14] != null && !row[14].equals("")) ? ((Date) row[14]).getTime() : 0);
						if (!"".equals(row[15])) {
							buisnessReportsBean.setJpmilesCancel( row[15].toString());
						}
						if (!"".equals(row[16])) {
							buisnessReportsBean.setJpmilesToCredit( row[16].toString());
						}

						productStatus = ServiceProviderUtil.checkNullSafeWithObject(row[17], "NA");
						if ("1".equalsIgnoreCase(productStatus)) {
							buisnessReportsBean.setPurchase_status(AfConstants.TENTATIVE);
						} else if ("2".equalsIgnoreCase(productStatus)) {
							buisnessReportsBean.setPurchase_status(AfConstants.PROCESSED);
						} else if ("3".equalsIgnoreCase(productStatus)) {
							buisnessReportsBean.setPurchase_status(AfConstants.DISAPPROVED);
						} else if ("4".equalsIgnoreCase(productStatus)) {
							buisnessReportsBean.setPurchase_status(AfConstants.FAILED);
						} else {
							buisnessReportsBean.setPurchase_status("NA");
						}
						buisnessReportsBean
								.setPartnerProductId(ServiceProviderUtil.checkNullSafeWithObject(row[18], "NA"));
						transactionType = ServiceProviderUtil.checkNullSafeWithObject(row[19], "NA");
						if (transactionType.equalsIgnoreCase("7"))
							buisnessReportsBean.setTransactionVia(AfConstants.CHROME_EXTENSION);
						else
							buisnessReportsBean.setTransactionVia(AfConstants.SHOP_SITE);
						buisnessReportsBean.setTax("NA");
						buisnessReportsBean.setCommAmount(ServiceProviderUtil.checkNullSafeWithObject(row[20], "NA"));

//						if (row[9] != null && row[11] != null && !AfConstants.INVALID.equals(statusType)) {
						if (row[9] != null && row[9].toString().length() > 0 && row[11] != null && row[11].toString().length() > 0 && !AfConstants.INVALID.equals(statusType)) {	
							Float jpplCommission = (((Float) row[9]) * ((Float) row[11]) / 100);
							DecimalFormat df = new DecimalFormat("#.##");
							String jpplCommissions = df.format(jpplCommission);
							buisnessReportsBean.setJpplCommAmount(jpplCommissions);
						} else {
							buisnessReportsBean.setJpplCommAmount("NA");
						}
						if ((Integer) row[21] == AfConstants.AMAZON_PRT_NO) {
							String partnerCategory = ServiceProviderUtil.checkNullSafeWithObject(row[22], "NA");
							buisnessReportsBean.setPartnerCategoryName(partnerCategory);
							String jpplCategory = categoryManagementService.getJpplCatByPrtCategory(partnerCategory,(Integer) row[21]);
							buisnessReportsBean.setJpplCategoryName(jpplCategory);
						}
						buisnessReportsBean.setEmailStatus(row[23].toString());
						if (row[24] != null){
							buisnessReportsBean.setSmsStatus( row[24].toString());
						}
						else{
							buisnessReportsBean.setSmsStatus("0");
						}
						reportList.add(buisnessReportsBean);
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("@@@@ Exception in ReportsServiceImpl of getBuisnessReportsList() :", e);
		}
		return reportList;
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public String updateCsvReport(List<zeroCsvReport> reportList) {
		String transStatus = "";
		String transDetailStatus = "";
		String status = "";
		List<AfTransactionDetails> aftd = new ArrayList<>();
		List<AfTransaction> afTrans = new ArrayList<>();
		String orderid = "";
		String transactionId = "";
		int emailUpdateCount = 0;
		int smsUpdateCount = 0;
		try {
			for (zeroCsvReport z : reportList) {
				orderid = z.getAfffiliateOrderId();
				transactionId = z.getTransactionId();

				aftd = transactionDetailsDAO.getTransactiondetails(orderid, transactionId);
				if (aftd.isEmpty() || aftd == null) {
					return AfConstants.ORDER_TRANS_ERROR;
				} else {
					for (AfTransactionDetails aftrans : aftd) {

						if (aftrans.getTransactionId().equals(z.getTransactionId())
								|| aftrans.getPartnerItemId().equals(orderid)) {

							if (z.getEmailStatus().equals("") || z.getEmailStatus() == null) {
								aftrans.setEmailStatus(new Byte("0"));
								emailUpdateCount++;
							} else {
								if (new Byte(z.getEmailStatus()) < 1 || new Byte(z.getEmailStatus()) == 1) {
									aftrans.setEmailStatus(new Byte(z.getEmailStatus()));
									emailUpdateCount++;
								} else {
									return AfConstants.EMAIL_SMS_ERROR + ", only " + emailUpdateCount
											+ " rows are updated";
								}
							}

							aftrans.setJpmilesPurchase(new Integer(z.getBaseJpmiles()));
							aftrans.setJpmilesToCredit(new Integer(z.getBaseJpmiles()));
							if(z.getBonusJpmiles()!=null && !z.getBonusJpmiles().equals("")){
								
								if(z.getBonusJpmiles().equalsIgnoreCase("NA")){
									aftrans.setBonusJpmiles(null);
								}
								else{
									aftrans.setBonusJpmiles(new Integer(z.getBonusJpmiles()));		
								}
							
							}else{
								aftrans.setBonusJpmiles(null);
							}
							transactionId = aftrans.getTransactionId();
							transDetailStatus = transactionDetailsDAO.updateTrarnsactionDetails(aftrans);

						} else {
							return AfConstants.ORDER_TRANS_ERROR;
						}
					}
					afTrans = transactionDetailsDAO.getTransactionByTransactionId(transactionId);
					for (AfTransaction aft : afTrans) {

						if (z.getSmsStatus().equals("") || z.getSmsStatus() == null) {
							aft.setTransactionSmsStatus(new Byte("0"));
						} else {
							if (new Byte(z.getSmsStatus()) < 1 || new Byte(z.getSmsStatus()) == 1) {
								aft.setTransactionSmsStatus(new Byte(z.getSmsStatus()));
							} else {
								return AfConstants.EMAIL_SMS_ERROR+", ";
							}
						}
						transStatus = transactionDetailsDAO.updateTransaction(aft);
					}
				}
				if (!transStatus.equals("") || !transDetailStatus.equals("")) {
					status = "success";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("@@@@ Exception in ReportsServiceImpl of updateCsvReport() :", e);
		}
		return status;
	}

	@Override
	public List<BuisnessReportsBean> getqueingLogicReportList(String fromDate, String toDate, String prtNo,
			String jpNumber) {
		
		BuisnessReportsBean buisnessReportsBean = null;
		String productId = "";
		String productName = "";
		String crisRequest = "";
		String crisResponse;
		Long orderCount = 0L;
		List<AfPartnerProduct> afPartnerProductList = null;
		List<BuisnessReportsBean> buisnessReportsBeanList = new ArrayList<>();
		try {
			List<BuisnessReportsBean> logicReportList = reportsDAO.getqueingReportList(fromDate, toDate, prtNo,jpNumber);
			Map<Byte, String> mapjpMilesStatus = commonService.getEnumValues("JPMILE_STATUS");
			
			if (null != logicReportList && !logicReportList.isEmpty()) {
			for (BuisnessReportsBean reportBean : logicReportList){
				buisnessReportsBean = new BuisnessReportsBean();
				buisnessReportsBean.setJpNumber(reportBean.getJpNumber());
				buisnessReportsBean.setTransactionId(reportBean.getTransactionId());
				buisnessReportsBean.setFormatedTransactionDate((reportBean.getTransactionDate()!=null)? simpleDateFormat.format(reportBean.getTransactionDate()): "NA");
				buisnessReportsBean.setOrderDateTime((reportBean.getTransactionDate()!=null) ? reportBean.getTransactionDate().getTime(): 0);
				buisnessReportsBean.setPartnerName(reportBean.getPartnerName());

				productId = reportBean.getPartnerProductId();
				
				if(null!=productId && !productId.isEmpty()) {
					afPartnerProductList = transactionDetailsDAO.getProductNameById(productId);
					for (AfPartnerProduct afpartnerproduct : afPartnerProductList) {
						productName = ServiceProviderUtil.nullSafe(afpartnerproduct.getProductName(), "");
						if (!productName.isEmpty()) {
							buisnessReportsBean.setProductName(productName);
						} else {
							buisnessReportsBean.setProductName("");
						}
					}
				}
				buisnessReportsBean.setJpmilesPurchase(reportBean.getJpmilesPurchase());
				buisnessReportsBean.setFormatedExpectedDateOfCredit((reportBean.getExpectedDateOfCredit() != null) ? simpleDateFormat.format(
						reportBean.getExpectedDateOfCredit()) : "NA");
				buisnessReportsBean.setExpectedDateOfCreditTime((reportBean.getExpectedDateOfCredit() != null) ? reportBean.getExpectedDateOfCredit().getTime() : 0);
				if (reportBean.getJpmilesStatus()==4) {
				buisnessReportsBean.setJpmiles_Status(mapjpMilesStatus.get(reportBean.getJpmilesStatus()));
				}
				buisnessReportsBean.setPartnerItemId((reportBean.getPartnerItemId()!=null) ? reportBean.getPartnerItemId() : "NA");
				crisRequest=reportsDAO.downloadCrisRequest(reportBean.getPartnerItemId());
				if(crisRequest==null){
					reportBean.setRequest(false);
				}else{
					reportBean.setRequest(true);
				}
				crisResponse=reportsDAO.downloadCrisResponse(reportBean.getPartnerItemId());
				if(crisResponse==null){
					reportBean.setResponse(false);
				}else{
					reportBean.setResponse(true);
					if(crisResponse.contains("<?xml")){
						reportBean.setIsXml(true);
					}else{
						reportBean.setIsXml(false);
					}
				}
				orderCount = reportsDAO.getOrderCount(reportBean.getPartnerItemId());
				buisnessReportsBean.setOrderCount(orderCount);
				buisnessReportsBeanList.add(buisnessReportsBean);
			}
		 }	
		} catch (Exception e) {
			logger.error("@@@Exception in ReportServiceImpl of getqueingLogicReportList():", e);
		}
		return buisnessReportsBeanList;
	}
	
	@Override
	public List<BuisnessReportsBean> getExportBuisnessReportsList(String fromDate, String toDate, String prtNo,
			String jpNumber, String status, String statusType) {
		BuisnessReportsBean buisnessReportsBean = null;
		String partnerProductId = "";
		String productName = "";
		String partnerCategoryName = "";
		String jpplCategoryName = "";
		List<AfPartnerProduct> afPartnerProductList = null;

		List<Object[]> reportObject = null;
		List<BuisnessReportsBean> reportList = null;
		String transactionType;
		String productStatus;
		
		try {
			reportObject = reportsDAO.getShopJPReports(fromDate, toDate, prtNo, jpNumber, status, statusType);
			Map<Byte, String> memberGuestMap = commonService.getEnumValues(AfConstants.MEMBER_OR_GUEST);
			if (reportObject != null && !reportObject.isEmpty()) {
				reportList = new ArrayList<>();
				
				for (Object[] row : reportObject) {
					buisnessReportsBean = new BuisnessReportsBean();
					buisnessReportsBean.setJpNumber(ServiceProviderUtil.checkNullSafeWithObject(row[0], "NA"));
					buisnessReportsBean.setSource(ServiceProviderUtil.checkNullSafeWithObject(row[1], "NA"));
					buisnessReportsBean.setMedium(ServiceProviderUtil.checkNullSafeWithObject(row[2], "NA"));
					buisnessReportsBean.setCampaign(ServiceProviderUtil.checkNullSafeWithObject(row[3], "NA"));
					buisnessReportsBean.setPartnerOrderId(ServiceProviderUtil.checkNullSafeWithObject(row[24], "NA"));
					buisnessReportsBean.setMember_or_guest(
							(memberGuestMap.get(row[4]) == null) ? "NA" : memberGuestMap.get(row[4]));
					buisnessReportsBean.setTransactionId(ServiceProviderUtil.checkNullSafeWithObject(row[5], "NA"));
					buisnessReportsBean.setPartnerItemId(ServiceProviderUtil.checkNullSafeWithObject(row[6], "NA"));
					buisnessReportsBean
							.setFormatedTransactionDate((row[7] != null) ? simpleDateFormat.format(row[7]) : "NA");
					buisnessReportsBean.setOrderDateTime((row[7] != null) ? ((Date) row[7]).getTime() : 0);
					buisnessReportsBean.setPartnerName(ServiceProviderUtil.checkNullSafeWithObject(row[8], "NA"));

					partnerProductId = ServiceProviderUtil.checkNullSafeWithObject(row[18], "");
					if (null != partnerProductId && !partnerProductId.isEmpty()) {

						afPartnerProductList = transactionDetailsDAO.getProductNameById(partnerProductId);
						if (!afPartnerProductList.isEmpty()) {
							for (AfPartnerProduct afPartnerProduct : afPartnerProductList) {
								productName = ServiceProviderUtil
										.checkNullSafeWithObject(afPartnerProduct.getProductName(), "NA");
								partnerCategoryName = ServiceProviderUtil.checkNullSafeWithObject(
										afPartnerProduct.getAfPartnerCategory().getPartnerCategoryName(), "NA");
							}
						}
					}
					if(partnerCategoryName==null || partnerCategoryName.isEmpty()){
						buisnessReportsBean.setPartnerCategoryName("NA");
					}else{
						buisnessReportsBean.setPartnerCategoryName(partnerCategoryName);
					}
					if(productName == null || productName.isEmpty()){
						buisnessReportsBean.setProductName("NA");
					}else{
						buisnessReportsBean.setProductName(productName);
					}
					buisnessReportsBean.setJpplCategoryName("NA");
					buisnessReportsBean.setPrice(ServiceProviderUtil.checkNullSafeWithObject(row[9], "NA"));
					buisnessReportsBean.setRatePerMile(ServiceProviderUtil.checkNullSafeWithObject(row[10], "NA"));
					buisnessReportsBean.setCommissionRate(ServiceProviderUtil.checkNullSafeWithObject(row[11], "NA"));
					buisnessReportsBean.setMemberShare(ServiceProviderUtil.checkNullSafeWithObject(row[12], "NA"));
					if (!"".equals(row[13])){
						buisnessReportsBean.setJpmilesPurchase(row[13].toString());
					}else{
						buisnessReportsBean.setJpmilesPurchase("NA");

					}
					buisnessReportsBean.setFormatedExpectedDateOfCredit(
							(row[14] != null && !"".equals(row[14])) ? simpleDateFormat.format(row[14]) : "NA");
					buisnessReportsBean.setExpectedDateOfCreditTime(
							(row[14] != null && !row[14].equals("")) ? ((Date) row[14]).getTime() : 0);
					if (!"".equals(row[15])) {
						buisnessReportsBean.setJpmilesCancel(row[15].toString());
					}else{
						buisnessReportsBean.setJpmilesCancel("NA");

					}
					if (!"".equals(row[16])) {
						buisnessReportsBean.setJpmilesToCredit(row[16].toString());
					}else{
						buisnessReportsBean.setJpmilesToCredit("NA");

					}

					productStatus = ServiceProviderUtil.checkNullSafeWithObject(row[17], "NA");
					if ("1".equalsIgnoreCase(productStatus)) {
						buisnessReportsBean.setPurchase_status(AfConstants.TENTATIVE);
					} else if ("2".equalsIgnoreCase(productStatus)) {
						buisnessReportsBean.setPurchase_status(AfConstants.PROCESSED);
					} else if ("3".equalsIgnoreCase(productStatus)) {
						buisnessReportsBean.setPurchase_status(AfConstants.DISAPPROVED);
					} else if ("4".equalsIgnoreCase(productStatus)) {
						buisnessReportsBean.setPurchase_status(AfConstants.FAILED);
					} else {
						buisnessReportsBean.setPurchase_status("NA");
					}
					buisnessReportsBean.setPartnerProductId(ServiceProviderUtil.checkNullSafeWithObject(row[18], "NA"));
					transactionType = ServiceProviderUtil.checkNullSafeWithObject(row[19], "NA");
					if (transactionType.equalsIgnoreCase("7"))
						buisnessReportsBean.setTransactionVia(AfConstants.CHROME_EXTENSION);
					else
						buisnessReportsBean.setTransactionVia(AfConstants.SHOP_SITE);
					buisnessReportsBean.setTax("NA");
					String prtCommAmount=ServiceProviderUtil.checkNullSafeWithObject(row[20], "NA");
					if(prtCommAmount.contains(",")){
						prtCommAmount=prtCommAmount.replace(",", "");
					}
					if (prtCommAmount.contains(".") && (prtCommAmount.length()-1-prtCommAmount.indexOf(".")>2)) {
						prtCommAmount = prtCommAmount.substring(0, prtCommAmount.indexOf(".")+3);
					}
						buisnessReportsBean.setCommAmount(prtCommAmount);
//					if (row[9] != null && row[11] != null && !AfConstants.INVALID.equals(statusType)) {
					if (row[9] != null && row[9].toString().length() > 0 && row[11] != null && row[11].toString().length() > 0 && !AfConstants.INVALID.equals(statusType)) {	
						Float jpplCommission = (((Float) row[9]) * ((Float) row[11]) / 100);
						DecimalFormat df = new DecimalFormat("#.##");
						String jpplCommissions = df.format(jpplCommission);
						buisnessReportsBean.setJpplCommAmount(jpplCommissions);
					} else {
						buisnessReportsBean.setJpplCommAmount("NA");
					}
					if ((Integer) row[21] == AfConstants.AMAZON_PRT_NO) {
						String partnerCategory = ServiceProviderUtil.checkNullSafeWithObject(row[22], "NA");
						buisnessReportsBean.setPartnerCategoryName(partnerCategory);
						String jpplCategory = categoryManagementService.getJpplCatByPrtCategory(partnerCategory,(Integer) row[21]);
						buisnessReportsBean.setJpplCategoryName(jpplCategory);
					}else{
                                        
                                        
                                        }
					reportList.add(buisnessReportsBean);
				}
			}

		} catch (Exception e) {
			logger.error("@@@@ Exception in ReportsServiceImpl of getBuisnessReportsList() :", e);
		}
		
		return reportList;
	}
	@Override
	public List<BuisnessReportsBean> getExportFinanceReportsList(String fromDate, String toDate, String prtNo,
			String jpNumber, String status, String statusType) {
		List<BuisnessReportsBean> financeReportlist = new ArrayList<>();
		BuisnessReportsBean financeReportsBean = null;
		List<Object[]> reportObject = null;
		String transactionType;
		String productStatus;
		try {
			reportObject = reportsDAO.getShopJPReports(fromDate, toDate, prtNo, jpNumber, status, statusType);
			Map<Byte, String> memberGuestMap = commonService.getEnumValues(AfConstants.MEMBER_OR_GUEST);

			if (reportObject != null && !reportObject.isEmpty()) {
				financeReportlist = new ArrayList<>();

				for (Object[] row : reportObject) {
					financeReportsBean = new BuisnessReportsBean();
					financeReportsBean.setJpNumber(ServiceProviderUtil.checkNullSafeWithObject(row[0], "NA"));
					financeReportsBean.setSource(ServiceProviderUtil.checkNullSafeWithObject(row[1], "NA"));
					financeReportsBean.setMedium(ServiceProviderUtil.checkNullSafeWithObject(row[2], "NA"));
					financeReportsBean.setCampaign(ServiceProviderUtil.checkNullSafeWithObject(row[3], "NA"));
					financeReportsBean.setMember_or_guest(
							(memberGuestMap.get(row[4]) == null) ? "NA" : memberGuestMap.get(row[4]));
					financeReportsBean.setPartnerItemId(ServiceProviderUtil.checkNullSafeWithObject(row[6], "NA"));
					financeReportsBean.setCommissionRate(ServiceProviderUtil.checkNullSafeWithObject(row[11], "NA"));
					financeReportsBean.setMemberShare(ServiceProviderUtil.checkNullSafeWithObject(row[12], "NA"));
					financeReportsBean.setTax("NA");
					if (!"".equals(row[16])) {
						financeReportsBean.setJpmilesToCredit(row[16].toString());
					}else{
						financeReportsBean.setJpmilesToCredit("NA");

					}
					productStatus = ServiceProviderUtil.checkNullSafeWithObject(row[17], "NA");
					if ("1".equalsIgnoreCase(productStatus)) {
						financeReportsBean.setPurchase_status(AfConstants.TENTATIVE);
					} else if ("2".equalsIgnoreCase(productStatus)) {
						financeReportsBean.setPurchase_status(AfConstants.PROCESSED);
					} else if ("3".equalsIgnoreCase(productStatus)) {
						financeReportsBean.setPurchase_status(AfConstants.DISAPPROVED);
					} else if ("4".equalsIgnoreCase(productStatus)) {
						financeReportsBean.setPurchase_status(AfConstants.FAILED);
					} else {
						financeReportsBean.setPurchase_status("NA");
					}

					transactionType = ServiceProviderUtil.checkNullSafeWithObject(row[19], "NA");
					if (transactionType.equalsIgnoreCase("7"))
						financeReportsBean.setTransactionVia(AfConstants.CHROME_EXTENSION);
					else
						financeReportsBean.setTransactionVia(AfConstants.SHOP_SITE);
					financeReportsBean.setRatePerMile(ServiceProviderUtil.checkNullSafeWithObject(row[10], "NA"));
					String prtCommAmount=ServiceProviderUtil.checkNullSafeWithObject(row[20], "NA");
					if(prtCommAmount.contains(",")){
						prtCommAmount=prtCommAmount.replace(",", "");
					}
					if (prtCommAmount.contains(".") && (prtCommAmount.length()-1-prtCommAmount.indexOf(".")>2)) {
						prtCommAmount = prtCommAmount.substring(0, prtCommAmount.indexOf(".")+3);
					}
					financeReportsBean.setCommAmount(prtCommAmount);
//					if (row[9] != null && row[11] != null && !AfConstants.INVALID.equals(statusType)) {
					if (row[9] != null && row[9].toString().length() > 0 && row[11] != null && row[11].toString().length() > 0 && !AfConstants.INVALID.equals(statusType)) {
						Float jpplCommission = (((Float) row[9]) * ((Float) row[11]) / 100);
						DecimalFormat df = new DecimalFormat("#.##");
						String jpplCommissions = df.format(jpplCommission);
						financeReportsBean.setJpplCommAmount(jpplCommissions);
					} else {
						financeReportsBean.setJpplCommAmount("NA");
					}

					financeReportlist.add(financeReportsBean);
				}
			}
		} catch (Exception e) {
			logger.error("@@@@ Exception in ReportsServiceImpl of getFinanceReportsList() :", e);
		}
		return financeReportlist;
	}

	@Override
	public List<BuisnessReportsBean> getExportAnalyticsReportsList(String fromDate, String toDate, String prtNo,
			String jpNumber, String status, String statusType) {
		List<BuisnessReportsBean> analyticsReportlist = new ArrayList<>();
		BuisnessReportsBean analyticsReportsBean = null;
		String partnerProductId = "";
		String productName = "";
		String partnerCategoryName = "";
		String jpplCategoryName = "";
		List<AfPartnerProduct> afPartnerProductList = null;
		List<Object[]> reportObject = null;
		String transactionType;
		String productStatus;
		try {
			reportObject = reportsDAO.getShopJPReports(fromDate, toDate, prtNo, jpNumber, status, statusType);
			Map<Byte, String> memberGuestMap = commonService.getEnumValues(AfConstants.MEMBER_OR_GUEST);

			if (reportObject != null && !reportObject.isEmpty()) {
				analyticsReportlist = new ArrayList<>();

				for (Object[] row : reportObject) {
					analyticsReportsBean = new BuisnessReportsBean();
					analyticsReportsBean.setJpNumber(ServiceProviderUtil.checkNullSafeWithObject(row[0], "NA"));
					analyticsReportsBean.setSource(ServiceProviderUtil.checkNullSafeWithObject(row[1], "NA"));
					analyticsReportsBean.setMedium(ServiceProviderUtil.checkNullSafeWithObject(row[2], "NA"));
					analyticsReportsBean.setCampaign(ServiceProviderUtil.checkNullSafeWithObject(row[3], "NA"));
					analyticsReportsBean.setMember_or_guest(
							(memberGuestMap.get(row[4]) == null) ? "NA" : memberGuestMap.get(row[4]));
					analyticsReportsBean.setTransactionId(ServiceProviderUtil.checkNullSafeWithObject(row[5], "NA"));
					analyticsReportsBean.setPartnerItemId(ServiceProviderUtil.checkNullSafeWithObject(row[6], "NA"));
					analyticsReportsBean.setFormatedTransactionDate(
							(row[7] != null && !row[7].equals("")) ? simpleDateFormat.format(row[7]) : "NA");
					analyticsReportsBean
							.setOrderDateTime((row[7] != null && !row[7].equals("")) ? ((Date) row[7]).getTime() : 0);
					analyticsReportsBean.setPartnerName(ServiceProviderUtil.checkNullSafeWithObject(row[8], "NA"));
					partnerProductId = ServiceProviderUtil.checkNullSafeWithObject(row[18], "");
					if (null != partnerProductId && !partnerProductId.isEmpty()) {

						afPartnerProductList = transactionDetailsDAO.getProductNameById(partnerProductId);
						if (!afPartnerProductList.isEmpty()) {
							for (AfPartnerProduct afPartnerProduct : afPartnerProductList) {
								productName = ServiceProviderUtil.nullSafe(afPartnerProduct.getProductName(), "NA");
								partnerCategoryName = ServiceProviderUtil.nullSafe(
										afPartnerProduct.getAfPartnerCategory().getPartnerCategoryName().toString(),
										"NA");
							}
						}
					} 
					if(partnerCategoryName==null || partnerCategoryName.isEmpty()){
						analyticsReportsBean.setPartnerCategoryName("NA");
					}else{
						analyticsReportsBean.setPartnerCategoryName(partnerCategoryName);
					}
					if(productName == null || productName.isEmpty()){
						analyticsReportsBean.setProductName("NA");
					}else{
						analyticsReportsBean.setProductName(productName);
					}
					analyticsReportsBean.setJpplCategoryName("NA");
					analyticsReportsBean.setPrice(ServiceProviderUtil.checkNullSafeWithObject(row[9], "NA"));
					if (!"".equals(row[13])) {
						analyticsReportsBean.setJpmilesPurchase(row[13].toString());
					}else{
						analyticsReportsBean.setJpmilesPurchase("NA");
					}
					analyticsReportsBean.setFormatedExpectedDateOfCredit(
							(row[14] != null && !row[14].equals("")) ? simpleDateFormat.format(row[14]) : "NA");
					analyticsReportsBean.setExpectedDateOfCreditTime(
							(row[14] != null && !row[14].equals("")) ? ((Date) row[14]).getTime() : 0);
					if (!"".equals(row[15])) {
						analyticsReportsBean.setJpmilesCancel(row[15].toString());
					}else{
						analyticsReportsBean.setJpmilesCancel("NA");
					}
					if (!"".equals(row[16])) {
						analyticsReportsBean.setJpmilesToCredit(row[16].toString());
					}else{
						analyticsReportsBean.setJpmilesToCredit("NA");
					}
					productStatus = ServiceProviderUtil.checkNullSafeWithObject(row[17], "NA");
					if ("1".equalsIgnoreCase(productStatus)) {
						analyticsReportsBean.setPurchase_status(AfConstants.TENTATIVE);
					} else if ("2".equalsIgnoreCase(productStatus)) {
						analyticsReportsBean.setPurchase_status(AfConstants.PROCESSED);
					} else if ("3".equalsIgnoreCase(productStatus)) {
						analyticsReportsBean.setPurchase_status(AfConstants.DISAPPROVED);
					} else if ("4".equalsIgnoreCase(productStatus)) {
						analyticsReportsBean.setPurchase_status(AfConstants.FAILED);
					} else {
						analyticsReportsBean.setPurchase_status("NA");
					}

					transactionType = ServiceProviderUtil.checkNullSafeWithObject(row[19], "NA");
					if (transactionType.equalsIgnoreCase("7"))
						analyticsReportsBean.setTransactionVia(AfConstants.CHROME_EXTENSION);
					else
						analyticsReportsBean.setTransactionVia(AfConstants.SHOP_SITE);
					analyticsReportsBean.setRatePerMile(ServiceProviderUtil.checkNullSafeWithObject(row[10], "NA"));
					String prtCommAmount=ServiceProviderUtil.checkNullSafeWithObject(row[20], "NA");
					if(prtCommAmount.contains(",")){
						prtCommAmount=prtCommAmount.replace(",", "");
					}
					if (prtCommAmount.contains(".") && (prtCommAmount.length()-1-prtCommAmount.indexOf(".")>2)) {
						prtCommAmount = prtCommAmount.substring(0, prtCommAmount.indexOf(".")+3);
					}
					analyticsReportsBean.setCommAmount(prtCommAmount);					
//					if (row[9] != null && row[11] != null && !AfConstants.INVALID.equals(statusType)) {
						if (row[9] != null && row[9].toString().length() > 0 && row[11] != null && row[11].toString().length() > 0 && !AfConstants.INVALID.equals(statusType)) {
						Float jpplCommission = (((Float) row[9]) * ((Float) row[11]) / 100);
						DecimalFormat df = new DecimalFormat("#.##");
						String jpplCommissions = df.format(jpplCommission);
						analyticsReportsBean.setJpplCommAmount(jpplCommissions);
					} else {
						analyticsReportsBean.setJpplCommAmount("NA");
					}
					if ((Integer) row[21] == AfConstants.AMAZON_PRT_NO) {
						String partnerCategory = ServiceProviderUtil.checkNullSafeWithObject(row[22], "NA");
						analyticsReportsBean.setPartnerCategoryName(partnerCategory);
						String jpplCategory = categoryManagementService.getJpplCatByPrtCategory(partnerCategory,(Integer) row[21]);
						analyticsReportsBean.setJpplCategoryName(jpplCategory);
					}
					analyticsReportlist.add(analyticsReportsBean);
				}
			}
		} catch (Exception e) {

			logger.error("@@@@ Exception in ReportsServiceImpl of getAnalyticsReportsList() :", e);
		}
		return analyticsReportlist;
	}

	@Override
	public List<BuisnessReportsBean> getExportServiceCenterReportsList(String fromDate, String toDate, String prtNo,
			String jpNumber, String status, String statusType) {
		List<BuisnessReportsBean> serviceCenterReportlist = new ArrayList<>();
		BuisnessReportsBean serviceCenterReportsBean = null;
		String partnerProductId = "";
		String productName = "";
		String partnerCategoryName = "";
		String jpplCategoryName = "";
		List<AfPartnerProduct> afPartnerProductList = null;
		List<Object[]> reportObject = null;
		String transactionType;
		String productStatus;
		try {
			reportObject = reportsDAO.getShopJPReports(fromDate, toDate, prtNo, jpNumber, status, statusType);
			Map<Byte, String> memberGuestMap = commonService.getEnumValues(AfConstants.MEMBER_OR_GUEST);

			if (reportObject != null && !reportObject.isEmpty()) {
				serviceCenterReportlist = new ArrayList<>();
				for (Object[] row : reportObject) {
					serviceCenterReportsBean = new BuisnessReportsBean();
					serviceCenterReportsBean.setJpNumber(ServiceProviderUtil.checkNullSafeWithObject(row[0], "NA"));
					serviceCenterReportsBean.setSource(ServiceProviderUtil.checkNullSafeWithObject(row[1], "NA"));
					serviceCenterReportsBean.setMedium(ServiceProviderUtil.checkNullSafeWithObject(row[2], "NA"));
					serviceCenterReportsBean.setCampaign(ServiceProviderUtil.checkNullSafeWithObject(row[3], "NA"));
					serviceCenterReportsBean.setMember_or_guest(
							(memberGuestMap.get(row[4]) == null) ? "NA" : memberGuestMap.get(row[4]));
					serviceCenterReportsBean
							.setTransactionId(ServiceProviderUtil.checkNullSafeWithObject(row[5], "NA"));
					serviceCenterReportsBean
							.setPartnerItemId(ServiceProviderUtil.checkNullSafeWithObject(row[6], "NA"));
					serviceCenterReportsBean.setFormatedTransactionDate(
							(row[7] != null && !row[7].equals("")) ? simpleDateFormat.format(row[7]) : "NA");
					serviceCenterReportsBean
							.setOrderDateTime((row[7] != null && !row[7].equals("")) ? ((Date) row[7]).getTime() : 0);
					serviceCenterReportsBean.setPartnerName(ServiceProviderUtil.checkNullSafeWithObject(row[8], "NA"));
					partnerProductId = ServiceProviderUtil.checkNullSafeWithObject(row[18], "");
					if (null != partnerProductId && !partnerProductId.isEmpty()) {

						afPartnerProductList = transactionDetailsDAO.getProductNameById(partnerProductId);
						if (!afPartnerProductList.isEmpty()) {
							for (AfPartnerProduct afPartnerProduct : afPartnerProductList) {
								productName = ServiceProviderUtil.nullSafe(afPartnerProduct.getProductName(), "NA");
								partnerCategoryName = ServiceProviderUtil.nullSafe(
										afPartnerProduct.getAfPartnerCategory().getPartnerCategoryName(), "NA");
							}
						}
					} 
					if(partnerCategoryName==null || partnerCategoryName.isEmpty()){
						serviceCenterReportsBean.setPartnerCategoryName("NA");
					}else{
						serviceCenterReportsBean.setPartnerCategoryName(partnerCategoryName);
					}
					if(productName == null || productName.isEmpty()){
						serviceCenterReportsBean.setProductName("NA");
					}else{
						serviceCenterReportsBean.setProductName(productName);
					}
					serviceCenterReportsBean.setJpplCategoryName("NA");
					serviceCenterReportsBean.setPrice(ServiceProviderUtil.checkNullSafeWithObject(row[9], "NA"));
					if (!"".equals(row[13])) {
						serviceCenterReportsBean.setJpmilesPurchase( row[13].toString());
					}else{
						serviceCenterReportsBean.setJpmilesPurchase("NA");
					}
					serviceCenterReportsBean.setFormatedExpectedDateOfCredit(
							(row[14] != null && !row[14].equals("")) ? simpleDateFormat.format(row[14]) : "NA");
					serviceCenterReportsBean.setExpectedDateOfCreditTime(
							(row[14] != null && !row[14].equals("")) ? ((Date) row[14]).getTime() : 0);
					if (!"".equals(row[15])) {
						serviceCenterReportsBean.setJpmilesCancel(row[15].toString());
					}else{
						serviceCenterReportsBean.setJpmilesCancel("NA");
					}
					if (!"".equals(row[16])) {
						serviceCenterReportsBean.setJpmilesToCredit( row[16].toString());
					}else{
						serviceCenterReportsBean.setJpmilesToCredit("NA");

					}
					productStatus = ServiceProviderUtil.checkNullSafeWithObject(row[17], "NA");
					if ("1".equalsIgnoreCase(productStatus)) {
						serviceCenterReportsBean.setPurchase_status(AfConstants.TENTATIVE);
					} else if ("2".equalsIgnoreCase(productStatus)) {
						serviceCenterReportsBean.setPurchase_status(AfConstants.PROCESSED);
					} else if ("3".equalsIgnoreCase(productStatus)) {
						serviceCenterReportsBean.setPurchase_status(AfConstants.DISAPPROVED);
					} else if ("4".equalsIgnoreCase(productStatus)) {
						serviceCenterReportsBean.setPurchase_status(AfConstants.FAILED);
					} else {
						serviceCenterReportsBean.setPurchase_status("NA");
					}
					transactionType = ServiceProviderUtil.checkNullSafeWithObject(row[19], "NA");
					if (transactionType.equalsIgnoreCase("7"))
						serviceCenterReportsBean.setTransactionVia(AfConstants.CHROME_EXTENSION);
					else
						serviceCenterReportsBean.setTransactionVia(AfConstants.SHOP_SITE);

					serviceCenterReportsBean.setRatePerMile(ServiceProviderUtil.checkNullSafeWithObject(row[10], "NA"));
					String prtCommAmount=ServiceProviderUtil.checkNullSafeWithObject(row[20], "NA");
					if(prtCommAmount.contains(",")){
						prtCommAmount=prtCommAmount.replace(",", "");
					}
					if (prtCommAmount.contains(".") && (prtCommAmount.length()-1-prtCommAmount.indexOf(".")>2)) {
						prtCommAmount = prtCommAmount.substring(0, prtCommAmount.indexOf(".")+3);
					}
					serviceCenterReportsBean.setCommAmount(prtCommAmount);
//					if (row[9] != null && row[11] != null && !AfConstants.INVALID.equals(statusType)) {
					if (row[9] != null && row[9].toString().length() > 0 && row[11] != null && row[11].toString().length() > 0 && !AfConstants.INVALID.equals(statusType)) {
						Float jpplCommission = (((Float) row[9]) * ((Float) row[11]) / 100);
						DecimalFormat df = new DecimalFormat("#.##");
						String jpplCommissions = df.format(jpplCommission);
						serviceCenterReportsBean.setJpplCommAmount(jpplCommissions);
					} else {
						serviceCenterReportsBean.setJpplCommAmount("NA");
					}
					if ((Integer) row[21] == AfConstants.AMAZON_PRT_NO) {
						String partnerCategory = ServiceProviderUtil.checkNullSafeWithObject(row[22], "NA");
						serviceCenterReportsBean.setPartnerCategoryName(partnerCategory);
						String jpplCategory = categoryManagementService.getJpplCatByPrtCategory(partnerCategory,(Integer) row[21]);
						serviceCenterReportsBean.setJpplCategoryName(jpplCategory);
					}
					serviceCenterReportlist.add(serviceCenterReportsBean);
				}
			}
		} catch (Exception e) {
			
			logger.error("@@@@ Exception in ReportsServiceImpl of getServiceCenterReportsList() :", e);
		}
		return serviceCenterReportlist;
	}



	@Override
	public List<BuisnessReportsBean> getInvalidReportsList(String fromDate, String toDate, String prtNo,
			String jpNumber, int length, int start) {
		BuisnessReportsBean invalidReportsBean = null;
		String partnerProductId = "";
		String productName="";
		String partnerCategoryName="";
		List<AfPartnerProduct> afPartnerProductList = null;
		Calendar cal = Calendar.getInstance();
		List<Object[]> reportObject = null;
		List<BuisnessReportsBean> reportList = null;
		try {
			reportObject = reportsDAO.getShopInValidReports(fromDate, toDate, prtNo, jpNumber,length,start);
			if(reportObject != null && !reportObject.isEmpty()){
				reportList = new ArrayList<>();
								for(Object[] row : reportObject){
					invalidReportsBean = new BuisnessReportsBean();
					invalidReportsBean.setJpNumber(ServiceProviderUtil.checkNullSafeWithObject(row[0], "NA"));
					invalidReportsBean.setTransactionId(ServiceProviderUtil.checkNullSafeWithObject(row[1], "NA"));
					invalidReportsBean.setPartnerItemId(ServiceProviderUtil.checkNullSafeWithObject(row[2], "NA"));
					invalidReportsBean.setFormatedTransactionDate((row[3] != null) ? simpleDateFormat.format(row[3]) : "NA");
					invalidReportsBean.setPartnerName(ServiceProviderUtil.checkNullSafeWithObject(row[4], "NA"));
					invalidReportsBean.setMessage(ServiceProviderUtil.checkNullSafeWithObject(row[5], "NA"));
					
					partnerProductId = ServiceProviderUtil.checkNullSafeWithObject(row[6], "");
					partnerCategoryName = ServiceProviderUtil.checkNullSafeWithObject(row[8], "");
					if(null!=partnerProductId && !partnerProductId.isEmpty()){

    						afPartnerProductList=transactionDetailsDAO.getProductNameById(partnerProductId);  
						for(AfPartnerProduct afPartnerProduct:afPartnerProductList){
							productName 	= ServiceProviderUtil.checkNullSafeWithObject(afPartnerProduct.getProductName(),"NA");						
						//	partnerCategoryName	= ServiceProviderUtil.checkNullSafeWithObject(afPartnerProduct.getAfPartnerCategory().getPartnerCategoryName(),"NA");
						}													
					}
					if(partnerCategoryName==null || partnerCategoryName.isEmpty()){
						invalidReportsBean.setPartnerCategoryName("NA");
					}else{
						invalidReportsBean.setPartnerCategoryName(partnerCategoryName);
					}
					if(productName == null || productName.isEmpty()){
						invalidReportsBean.setProductName("NA");
					}else{
						invalidReportsBean.setProductName(productName);
					}
					
					invalidReportsBean.setPartnerProductId(ServiceProviderUtil.checkNullSafeWithObject(row[6], "NA"));
					
					if((Integer) row[7] == AfConstants.AMAZON_PRT_NO ){
					String partnerCategory = ServiceProviderUtil.checkNullSafeWithObject(row[8],"NA");
					invalidReportsBean.setPartnerCategoryName(partnerCategory);
					}
					invalidReportsBean.setTrackingId(ServiceProviderUtil.checkNullSafeWithObject(row[9], "NA"));
					reportList.add(invalidReportsBean);
					
				}
			}
			
			
			
		} catch(Exception e) {
			e.printStackTrace();
			logger.error("@@@@ Exception in ReportsServiceImpl of getinvalidReportsList() :", e);
		}
		/*Calendar cal2 = Calendar.getInstance();
		System.out.println("service"+(cal2.getTimeInMillis()-cal.getTimeInMillis()));*/
		return reportList;
	}



	@Override
	public List<BuisnessReportsBean> getExportInvalidReportsList(String fromDate, String toDate, String prtNo,
			String jpNumber) {
		
		BuisnessReportsBean invalidReportsBean = null;
		String partnerProductId = "";
		String productName = "";
		String partnerCategoryName = "";
		String jpplCategoryName = "";
		List<AfPartnerProduct> afPartnerProductList = null;

		List<Object[]> reportObject = null;
		List<BuisnessReportsBean> reportList = null;
		String transactionType;
		String productStatus;
		
		try {
			reportObject = reportsDAO.getShopInValidReports(fromDate, toDate, prtNo, jpNumber);
			if (reportObject != null && !reportObject.isEmpty()) {
				reportList = new ArrayList<>();
				
				for (Object[] row : reportObject) {
					invalidReportsBean = new BuisnessReportsBean();
					invalidReportsBean.setJpNumber(ServiceProviderUtil.checkNullSafeWithObject(row[0], "NA"));
					
					invalidReportsBean.setTransactionId(ServiceProviderUtil.checkNullSafeWithObject(row[1], "NA"));
					invalidReportsBean.setPartnerItemId(ServiceProviderUtil.checkNullSafeWithObject(row[2], "NA"));
					invalidReportsBean.setFormatedTransactionDate((row[3] != null) ? simpleDateFormat.format(row[3]) : "NA");
					invalidReportsBean.setPartnerName(ServiceProviderUtil.checkNullSafeWithObject(row[4], "NA"));
					invalidReportsBean.setMessage(ServiceProviderUtil.checkNullSafeWithObject(row[5], "NA"));

					partnerProductId = ServiceProviderUtil.checkNullSafeWithObject(row[6], "");
					
					if (null != partnerProductId && !partnerProductId.isEmpty()) {

						afPartnerProductList = transactionDetailsDAO.getProductNameById(partnerProductId);
						if (!afPartnerProductList.isEmpty()) {
							for (AfPartnerProduct afPartnerProduct : afPartnerProductList) {
								productName = ServiceProviderUtil
										.checkNullSafeWithObject(afPartnerProduct.getProductName(), "NA");
								
							}
						}
					}
					if(partnerCategoryName==null || partnerCategoryName.isEmpty()){
						invalidReportsBean.setPartnerCategoryName("NA");
					}else{
						invalidReportsBean.setPartnerCategoryName(partnerCategoryName);
					}
					if(productName == null || productName.isEmpty()){
						invalidReportsBean.setProductName("NA");
					}else{
						invalidReportsBean.setProductName(productName);
					}
					
					   invalidReportsBean.setPartnerProductId(ServiceProviderUtil.checkNullSafeWithObject(row[6], "NA"));
					
					
					if ((Integer) row[7] == AfConstants.AMAZON_PRT_NO) {
						String partnerCategory = ServiceProviderUtil.checkNullSafeWithObject(row[8], "NA");
						invalidReportsBean.setPartnerCategoryName(partnerCategory);
					}
					invalidReportsBean.setTrackingId(ServiceProviderUtil.checkNullSafeWithObject(row[9], "NA"));
					reportList.add(invalidReportsBean);
					partnerCategoryName="";
					productName="";
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("@@@@ Exception in ReportsServiceImpl of getInvalidReportsList() :", e);
		}
		
		return reportList;
	}



	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public String updateInvalidCsvReport(List<InvalidCsvReprot> reportList) {
		
		String transStatus = "failure";
		String status = "";
		List<AfTransactionDetails> aftd = new ArrayList<>();
		List<PartnerReportIn> pr = new ArrayList<>();
		List<AfTransaction> afTrans = new ArrayList<>();
		String orderid = "";
		String transactionId = "";
		int count=0;
		ArrayList<Integer> counts= new ArrayList<>();
		try {
			
			for (InvalidCsvReprot invalidBean : reportList) {
				String fileCategoryName=invalidBean.getPartnerCategoryName().equalsIgnoreCase("NA")?"":invalidBean.getPartnerCategoryName();
				String filePartnerProductName=invalidBean.getProductName().equalsIgnoreCase("NA")?"":invalidBean.getProductName();
				String partnerName=invalidBean.getPartnerName();
				Integer prtNo = commissionManagementDAO.getpartnerNo(partnerName);
				Integer prtCat = commissionManagementDAO.getAfPartnerCategoryName(invalidBean.getPartnerCategoryName(), prtNo);
				Integer partnerProductNo = commissionManagementDAO.getAfPartnerProductNo(invalidBean.getProductName(), prtNo, prtCat);
				if(prtNo ==null ){
					count++;
					counts.add(count);
					continue;
				}else if((!fileCategoryName.isEmpty()) && prtCat ==null){
					count++;
					counts.add(count);
					continue;
				}else if((!filePartnerProductName.isEmpty()) && partnerProductNo ==null){
					count++;
					counts.add(count);
					continue;
				}
				orderid = invalidBean.getAfffiliateOrderId();
				transactionId = invalidBean.getTransactionId();
				pr = transactionDetailsDAO.getPartnerReportIn(orderid, transactionId);
				if (pr == null || pr.isEmpty()) {
					count++;
					counts.add(count);
					continue;
				} 
				count++;
				for (PartnerReportIn report : pr) {
					if (report.getTransactionId().equals(invalidBean.getTransactionId())
							|| report.getOrderId().equals(orderid)) {
					report.setProductName(invalidBean.getProductName());
					report.setCategory(invalidBean.getPartnerCategoryName());
					report.setPartnerId(new String (invalidBean.getPartnerName()));
					transStatus = transactionDetailsDAO.updateTransaction(report);
					}
					
				}
			}
			/*if(!counts.isEmpty()){
				transStatus="failure "+counts.toString().replace(", ", ",")
						
						+ ""
						+ ""
						+ ""
						+ "";
			}*/
			
		} catch (Exception e) {
			logger.error("@@@@ Exception in ReportsServiceImpl of updateInvalidCsvReport() :", e);
		}
		
		if(!counts.isEmpty()){
			transStatus="failure "+counts.toString().replace(", ", ",")
					
					+ ""
					+ ""
					+ ""
					+ "";
		}
		return transStatus ;
	}



	@Override
	public List<BuisnessReportsBean> getJPMilesStatusUpdateWithJPNumber(String fromDate, String toDate,
			String prtNo, String jpNumber, String status, String statusType, int length, int start) {
		

		BuisnessReportsBean jpMilesStatusUpdateBean = null;
		String partnerProductId = "";
		String productName="";
		String partnerCategoryName="";
		String jpplCategoryName="";
		List<AfPartnerProduct> afPartnerProductList = null;
		
		List<Object[]> reportObject = null;
		List<BuisnessReportsBean> jpMilesStatusUpdateBeanList = null;
		String transactionType;
		String productStatus;
		String jpplStatus;
		int count=1;
		try {
			reportObject = reportsDAO.getJPMilesStatusReports(fromDate, toDate, prtNo, jpNumber,status,statusType,length,start);
			Map<Byte, String> memberGuestMap = commonService.getEnumValues(AfConstants.MEMBER_OR_GUEST);
			if(reportObject != null && !reportObject.isEmpty()){
				jpMilesStatusUpdateBeanList = new ArrayList<>();
				
				for(Object[] row : reportObject){
					jpMilesStatusUpdateBean = new BuisnessReportsBean();
					jpMilesStatusUpdateBean.setJpNumber(ServiceProviderUtil.checkNullSafeWithObject(row[0], "NA"));
					jpMilesStatusUpdateBean.setSource(ServiceProviderUtil.checkNullSafeWithObject(row[1], "NA"));
					jpMilesStatusUpdateBean.setMedium(ServiceProviderUtil.checkNullSafeWithObject(row[2], "NA"));
					jpMilesStatusUpdateBean.setCampaign(ServiceProviderUtil.checkNullSafeWithObject(row[3], "NA"));
					jpMilesStatusUpdateBean.setMember_or_guest((memberGuestMap.get(row[4])==null)?"NA":memberGuestMap.get(row[4]));
					jpMilesStatusUpdateBean.setTransactionId(ServiceProviderUtil.checkNullSafeWithObject(row[5], "NA"));
					jpMilesStatusUpdateBean.setPartnerItemId(ServiceProviderUtil.checkNullSafeWithObject(row[6], "NA"));
					jpMilesStatusUpdateBean.setFormatedTransactionDate((row[7] != null) ? simpleDateFormat.format(row[7]) : "NA");
					jpMilesStatusUpdateBean.setOrderDateTime(
							(row[7] != null) ? ((Date)row[7]).getTime() : 0);
					jpMilesStatusUpdateBean.setPartnerName(ServiceProviderUtil.checkNullSafeWithObject(row[8], "NA"));
					
					partnerProductId = ServiceProviderUtil.checkNullSafeWithObject(row[18], "");
					if(null!=partnerProductId && !partnerProductId.isEmpty()){

						afPartnerProductList=transactionDetailsDAO.getProductNameById(partnerProductId);  
						for(AfPartnerProduct afPartnerProduct:afPartnerProductList){
							productName 	= ServiceProviderUtil.checkNullSafeWithObject(afPartnerProduct.getProductName(),"NA");						
							partnerCategoryName	= ServiceProviderUtil.checkNullSafeWithObject(afPartnerProduct.getAfPartnerCategory().getPartnerCategoryName(),"NA");
						}													
					}
					if(partnerCategoryName==null || partnerCategoryName.isEmpty()){
						jpMilesStatusUpdateBean.setPartnerCategoryName("NA");
					}else{
						jpMilesStatusUpdateBean.setPartnerCategoryName(partnerCategoryName);
					}
					if(productName == null || productName.isEmpty()){
						jpMilesStatusUpdateBean.setProductName("NA");
					}else{
						jpMilesStatusUpdateBean.setProductName(productName);
					}
					
						
					jpMilesStatusUpdateBean.setPrice(ServiceProviderUtil.checkNullSafeWithObject(row[9], "NA"));
					jpMilesStatusUpdateBean.setRatePerMile(ServiceProviderUtil.checkNullSafeWithObject(row[10], "NA"));
					jpMilesStatusUpdateBean.setCommissionRate(ServiceProviderUtil.checkNullSafeWithObject(row[11],"NA"));
					jpMilesStatusUpdateBean.setMemberShare(ServiceProviderUtil.checkNullSafeWithObject(row[12],"NA"));
					jpMilesStatusUpdateBean.setSmsStatus(ServiceProviderUtil.checkNullSafeWithObject(row[25],"NA"));
					jpplStatus=ServiceProviderUtil.checkNullSafeWithObject(row[24],"NA");
					jpMilesStatusUpdateBean.setJpmilesStatus((byte)row[24]);
					if("1".equalsIgnoreCase(jpplStatus)){
						jpMilesStatusUpdateBean.setJpmiles_Status("Requested");	
					}else if("2".equalsIgnoreCase(jpplStatus)){
						jpMilesStatusUpdateBean.setJpmiles_Status("Sent For Credit");	
					}else if("3".equalsIgnoreCase(jpplStatus)){
						jpMilesStatusUpdateBean.setJpmiles_Status("Credited");	
					}else if("4".equalsIgnoreCase(jpplStatus)){
						jpMilesStatusUpdateBean.setJpmiles_Status("Declined");	
					}else{
						jpMilesStatusUpdateBean.setJpmiles_Status("NA");	
					}
					if(!"".equals(row[13]))
						jpMilesStatusUpdateBean.setJpmilesPurchase(row[13].toString());	
					else
						jpMilesStatusUpdateBean.setJpmilesPurchase("NA");
					
					jpMilesStatusUpdateBean.setFormatedExpectedDateOfCredit(
							(row[14] != null && !"".equals(row[14])) ? simpleDateFormat.format(
									row[14]) : "NA");
					jpMilesStatusUpdateBean.setExpectedDateOfCreditTime((row[14] != null && !row[14].equals("")) ? ((Date)row[14]).getTime() : 0);
					if(!"".equals(row[15])){
						jpMilesStatusUpdateBean.setJpmilesCancel(row[15].toString());
					}else{
						jpMilesStatusUpdateBean.setJpmilesCancel("NA");

					}
					if(!"".equals(row[16])){
						jpMilesStatusUpdateBean.setJpmilesToCredit(row[16].toString());
					}else{
						jpMilesStatusUpdateBean.setJpmilesToCredit("NA");

					}
						
					productStatus=ServiceProviderUtil.checkNullSafeWithObject(row[17], "NA");
					if("1".equalsIgnoreCase(productStatus)){
						jpMilesStatusUpdateBean.setPurchase_status(AfConstants.TENTATIVE);	
					}else if("2".equalsIgnoreCase(productStatus)){
						jpMilesStatusUpdateBean.setPurchase_status(AfConstants.PROCESSED);	
					}else if("3".equalsIgnoreCase(productStatus)){
						jpMilesStatusUpdateBean.setPurchase_status(AfConstants.DISAPPROVED);	
					}else if("4".equalsIgnoreCase(productStatus)){
						jpMilesStatusUpdateBean.setPurchase_status(AfConstants.FAILED);	
					}else{
						jpMilesStatusUpdateBean.setPurchase_status("NA");	
					}
					jpMilesStatusUpdateBean.setPartnerProductId(ServiceProviderUtil.checkNullSafeWithObject(row[18], "NA"));
					transactionType=ServiceProviderUtil.checkNullSafeWithObject(row[19], "NA");
					if(transactionType.equalsIgnoreCase("7"))
						jpMilesStatusUpdateBean.setTransactionVia(AfConstants.CHROME_EXTENSION);
					else
						jpMilesStatusUpdateBean.setTransactionVia(AfConstants.SHOP_SITE);
					jpMilesStatusUpdateBean.setTax("NA");
					String prtCommAmount=ServiceProviderUtil.checkNullSafeWithObject(row[20], "NA");
					if(prtCommAmount.contains(",")){
						prtCommAmount=prtCommAmount.replace(",", "");
					}
					if (prtCommAmount.contains(".") && (prtCommAmount.length()-1-prtCommAmount.indexOf(".")>2)) {
						prtCommAmount = prtCommAmount.substring(0, prtCommAmount.indexOf(".")+3);
					}
					jpMilesStatusUpdateBean.setCommAmount(prtCommAmount);
//					if (row[9] != null && row[11] != null && !AfConstants.INVALID.equals(statusType)) {
					if (row[9] != null && row[9].toString().length() > 0 && row[11] != null && row[11].toString().length() > 0 && !AfConstants.INVALID.equals(statusType)) {	
						Float jpplCommission = (((Float) row[9])* ((Float) row[11])/100);
						DecimalFormat df = new DecimalFormat("#.##");
						String jpplCommissions = df.format(jpplCommission);
						jpMilesStatusUpdateBean.setJpplCommAmount(jpplCommissions);
					} else {
						jpMilesStatusUpdateBean.setJpplCommAmount("NA");
					}
					if((Integer) row[21] == AfConstants.AMAZON_PRT_NO ){
					String partnerCategory = ServiceProviderUtil.checkNullSafeWithObject(row[22],"NA");
					jpMilesStatusUpdateBean.setPartnerCategoryName(partnerCategory);
					jpplCategoryName = categoryManagementService.getJpplCatByPrtCategory(partnerCategory,(Integer) row[21]);
					}
					if(jpplCategoryName.equalsIgnoreCase("") || jpplCategoryName == null){
						jpMilesStatusUpdateBean.setJpplCategoryName("NA");
					}else{
						jpMilesStatusUpdateBean.setJpplCategoryName(jpplCategoryName);
					}
					jpMilesStatusUpdateBeanList.add(jpMilesStatusUpdateBean);
					
				}
			}
			
		} catch(Exception e) {
			e.printStackTrace();
			logger.error("@@@@ Exception in ReportsServiceImpl of getJPMilesStatusUpdateWithJPNumber() :", e);
		}
		return jpMilesStatusUpdateBeanList;
	}



	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String jpplCreditUpdate(JSONObject userJson, HttpServletRequest request) {
		String transactionId;
		String partnerItemId;
		String status=AfConstants.SUCCESS;
		List<AfTransactionDetails> afTransactionDetails=null;
		List<AfTransaction> afTransaction=null;
		try{
			transactionId= userJson.getString("transactionId");
			partnerItemId= userJson.getString("partnerItemId");
			afTransaction=reportsDAO.getTransactionId(transactionId);
			for(AfTransaction afTransactionList:afTransaction){
				afTransactionList.setSmsStatus((byte)userJson.getInt("smsStatus"));
			}
			
			afTransactionDetails=reportsDAO.getJpMilesByTransactionId(transactionId,partnerItemId);
			for(AfTransactionDetails afTransactionDetailsList:afTransactionDetails){
				   afTransactionDetailsList.setJpmilesCancel(userJson.getInt("jpmilesCancel"));
				   afTransactionDetailsList.setJpmilesToCredit(userJson.getInt("jpmilesToCredit"));
				   afTransactionDetailsList.setJpmileStatus((byte)userJson.getInt("jpmiles_Status"));
			}
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in ReportsServiceImpl of jpplCreditUpdate() :", e);
		}
		
		return status;
	}



	
	@Override
	public List<BuisnessReportsBean> getAllExportBuisnessReportsList(String fromDate, String toDate, String jpNumber,
			String status, String statusType) {
		
		BuisnessReportsBean buisnessReportsBean = null;
		String partnerProductId = "";
		String productName = "";
		String partnerCategoryName = "";
		String jpplCategoryName = "";
		List<AfPartnerProduct> afPartnerProductList = null;

		List<Object[]> reportObject = null;
		List<BuisnessReportsBean> reportList = null;
		String transactionType;
		String productStatus;
		
		try {
			reportObject = reportsDAO.getShopAllJPReports(fromDate, toDate, jpNumber, status, statusType);
			System.out.println("reportList ---> before    "+reportObject);
//			System.out.println("size of report object ---> "+reportObject.size());
			Map<Byte, String> memberGuestMap = commonService.getEnumValues(AfConstants.MEMBER_OR_GUEST);
			System.out.println("size of reportObject before ---> "+reportList);
//			if(reportObject.isEmpty() || reportObject==null)
//			{
//				
//			}
			if (null != reportObject && !reportObject.isEmpty()) {
				reportList = new ArrayList<>();
				int i = 0;
				for (Object[] row : reportObject) {
					System.out.println("i-> "+i);
					buisnessReportsBean = new BuisnessReportsBean();
					buisnessReportsBean.setJpNumber(ServiceProviderUtil.checkNullSafeWithObject(row[0], "NA"));
					buisnessReportsBean.setSource(ServiceProviderUtil.checkNullSafeWithObject(row[1], "NA"));
					buisnessReportsBean.setMedium(ServiceProviderUtil.checkNullSafeWithObject(row[2], "NA"));
					buisnessReportsBean.setCampaign(ServiceProviderUtil.checkNullSafeWithObject(row[3], "NA"));
					
					if(status.equals("JP Number Missing") || status.equals("Reference Number Missing") || status.equals("JP And Reference Number Missing") || status.equals("Others") ) {
						buisnessReportsBean.setPartnerOrderId("NA");
					}
					else
					{
						buisnessReportsBean.setPartnerOrderId(ServiceProviderUtil.checkNullSafeWithObject(row[24], "NA"));
					}
					
					
					buisnessReportsBean.setMember_or_guest(
							(memberGuestMap.get(row[4]) == null) ? "NA" : memberGuestMap.get(row[4]));
					buisnessReportsBean.setTransactionId(ServiceProviderUtil.checkNullSafeWithObject(row[5], "NA"));
					buisnessReportsBean.setPartnerItemId(ServiceProviderUtil.checkNullSafeWithObject(row[6], "NA"));
					buisnessReportsBean
							.setFormatedTransactionDate((row[7] != null) ? simpleDateFormat.format(row[7]) : "NA");
					buisnessReportsBean.setOrderDateTime((row[7] != null) ? ((Date) row[7]).getTime() : 0);
					buisnessReportsBean.setPartnerName(ServiceProviderUtil.checkNullSafeWithObject(row[8], "NA"));

					partnerProductId = ServiceProviderUtil.checkNullSafeWithObject(row[18], "");
					if (null != partnerProductId && !partnerProductId.isEmpty()) {

						afPartnerProductList = transactionDetailsDAO.getProductNameById(partnerProductId);
						if (!afPartnerProductList.isEmpty()) {
							for (AfPartnerProduct afPartnerProduct : afPartnerProductList) {
								productName = ServiceProviderUtil
										.checkNullSafeWithObject(afPartnerProduct.getProductName(), "NA");
								partnerCategoryName = ServiceProviderUtil.checkNullSafeWithObject(
										afPartnerProduct.getAfPartnerCategory().getPartnerCategoryName(), "NA");
							}
						}
					}
					if(partnerCategoryName==null || partnerCategoryName.isEmpty()){
						buisnessReportsBean.setPartnerCategoryName("NA");
					}else{
						buisnessReportsBean.setPartnerCategoryName(partnerCategoryName);
					}
					if(productName == null || productName.isEmpty()){
						buisnessReportsBean.setProductName("NA");
					}else{
						buisnessReportsBean.setProductName(productName);
					}
					buisnessReportsBean.setJpplCategoryName("NA");
					buisnessReportsBean.setPrice(ServiceProviderUtil.checkNullSafeWithObject(row[9], "NA"));
					buisnessReportsBean.setRatePerMile(ServiceProviderUtil.checkNullSafeWithObject(row[10], "NA"));
					buisnessReportsBean.setCommissionRate(ServiceProviderUtil.checkNullSafeWithObject(row[11], "NA"));
					buisnessReportsBean.setMemberShare(ServiceProviderUtil.checkNullSafeWithObject(row[12], "NA"));
					if (!"".equals(row[13])){
						buisnessReportsBean.setJpmilesPurchase(row[13].toString());
					}else{
						buisnessReportsBean.setJpmilesPurchase("NA");

					}
					buisnessReportsBean.setFormatedExpectedDateOfCredit(
							(row[14] != null && !"".equals(row[14])) ? simpleDateFormat.format(row[14]) : "NA");
					buisnessReportsBean.setExpectedDateOfCreditTime(
							(row[14] != null && !row[14].equals("")) ? ((Date) row[14]).getTime() : 0);
					if (!"".equals(row[15])) {
						buisnessReportsBean.setJpmilesCancel(row[15].toString());
					}else{
						buisnessReportsBean.setJpmilesCancel("NA");

					}
					if (!"".equals(row[16])) {
						buisnessReportsBean.setJpmilesToCredit(row[16].toString());
					}else{
						buisnessReportsBean.setJpmilesToCredit("NA");

					}

					productStatus = ServiceProviderUtil.checkNullSafeWithObject(row[17], "NA");
					if ("1".equalsIgnoreCase(productStatus)) {
						buisnessReportsBean.setPurchase_status(AfConstants.TENTATIVE);
					} else if ("2".equalsIgnoreCase(productStatus)) {
						buisnessReportsBean.setPurchase_status(AfConstants.PROCESSED);
					} else if ("3".equalsIgnoreCase(productStatus)) {
						buisnessReportsBean.setPurchase_status(AfConstants.DISAPPROVED);
					} else if ("4".equalsIgnoreCase(productStatus)) {
						buisnessReportsBean.setPurchase_status(AfConstants.FAILED);
					} else {
						buisnessReportsBean.setPurchase_status("NA");
					}
					buisnessReportsBean.setPartnerProductId(ServiceProviderUtil.checkNullSafeWithObject(row[18], "NA"));
					transactionType = ServiceProviderUtil.checkNullSafeWithObject(row[19], "NA");
					if (transactionType.equalsIgnoreCase("7"))
						buisnessReportsBean.setTransactionVia(AfConstants.CHROME_EXTENSION);
					else
						buisnessReportsBean.setTransactionVia(AfConstants.SHOP_SITE);
					buisnessReportsBean.setTax("NA");
					String prtCommAmount=ServiceProviderUtil.checkNullSafeWithObject(row[20], "NA");
					if(prtCommAmount.contains(",")){
						prtCommAmount=prtCommAmount.replace(",", "");
					}
					if (prtCommAmount.contains(".") && (prtCommAmount.length()-1-prtCommAmount.indexOf(".")>2)) {
						prtCommAmount = prtCommAmount.substring(0, prtCommAmount.indexOf(".")+3);
					}
						buisnessReportsBean.setCommAmount(prtCommAmount);
//					if (row[9] != null && row[11] != null && !AfConstants.INVALID.equals(statusType)) {
						if (row[9] != null && row[9].toString().length() > 0 && row[11] != null && row[11].toString().length() > 0 && !AfConstants.INVALID.equals(statusType)) {	
						Float jpplCommission = (((Float) row[9]) * ((Float) row[11]) / 100);
						DecimalFormat df = new DecimalFormat("#.##");
						String jpplCommissions = df.format(jpplCommission);
						buisnessReportsBean.setJpplCommAmount(jpplCommissions);
					} else {
						buisnessReportsBean.setJpplCommAmount("NA");
					}
					if ((Integer) row[21] == AfConstants.AMAZON_PRT_NO) {
						String partnerCategory = ServiceProviderUtil.checkNullSafeWithObject(row[22], "NA");
						buisnessReportsBean.setPartnerCategoryName(partnerCategory);
						String jpplCategory = categoryManagementService.getJpplCatByPrtCategory(partnerCategory,(Integer) row[21]);
						buisnessReportsBean.setJpplCategoryName(jpplCategory);
					}
					reportList.add(buisnessReportsBean);
					i++;
				}
			}else{
                        buisnessReportsBean = new BuisnessReportsBean();
				buisnessReportsBean.setJpNumber("No data found");
				reportList = new ArrayList<>();
				reportList.add(buisnessReportsBean);
				return reportList;
                        
                        }
			System.out.println("entered after executing if");

		} catch (Exception e) {
			logger.error("@@@@ Exception in ReportsServiceImpl of getBuisnessReportsList() :", e);
			System.out.println("Execerep----->"+e);
			e.printStackTrace();
		}
		System.out.println("reportList ---> "+reportList.size());
		System.out.println("reportList ---> "+reportList);
		return reportList;
		
	}
	
	
	public List<BuisnessReportsBean> getAllExportServiceCenterReportsList(String fromDate, String toDate,
			String jpNumber, String status, String statusType) {
		List<BuisnessReportsBean> serviceCenterReportlist = new ArrayList<>();
		BuisnessReportsBean serviceCenterReportsBean = null;
		String partnerProductId = "";
		String productName = "";
		String partnerCategoryName = "";
		String jpplCategoryName = "";
		List<AfPartnerProduct> afPartnerProductList = null;
		List<Object[]> reportObject = null;
		String transactionType;
		String productStatus;
		try {
			reportObject = reportsDAO.getShopAllJPReports(fromDate, toDate, jpNumber, status, statusType);
			Map<Byte, String> memberGuestMap = commonService.getEnumValues(AfConstants.MEMBER_OR_GUEST);
			
			if(reportObject.size() == 0)
			{
				serviceCenterReportsBean = new BuisnessReportsBean();
				serviceCenterReportsBean.setJpNumber("No data found");
				serviceCenterReportlist = new ArrayList<>();
				serviceCenterReportlist.add(serviceCenterReportsBean);
				return serviceCenterReportlist;
			}

			if (reportObject != null && !reportObject.isEmpty()) {
				serviceCenterReportlist = new ArrayList<>();
				for (Object[] row : reportObject) {
					serviceCenterReportsBean = new BuisnessReportsBean();
					serviceCenterReportsBean.setJpNumber(ServiceProviderUtil.checkNullSafeWithObject(row[0], "NA"));
					serviceCenterReportsBean.setSource(ServiceProviderUtil.checkNullSafeWithObject(row[1], "NA"));
					serviceCenterReportsBean.setMedium(ServiceProviderUtil.checkNullSafeWithObject(row[2], "NA"));
					serviceCenterReportsBean.setCampaign(ServiceProviderUtil.checkNullSafeWithObject(row[3], "NA"));
					serviceCenterReportsBean.setMember_or_guest(
							(memberGuestMap.get(row[4]) == null) ? "NA" : memberGuestMap.get(row[4]));
					serviceCenterReportsBean
							.setTransactionId(ServiceProviderUtil.checkNullSafeWithObject(row[5], "NA"));
					serviceCenterReportsBean
							.setPartnerItemId(ServiceProviderUtil.checkNullSafeWithObject(row[6], "NA"));
					serviceCenterReportsBean.setFormatedTransactionDate(
							(row[7] != null && !row[7].equals("")) ? simpleDateFormat.format(row[7]) : "NA");
					serviceCenterReportsBean
							.setOrderDateTime((row[7] != null && !row[7].equals("")) ? ((Date) row[7]).getTime() : 0);
					serviceCenterReportsBean.setPartnerName(ServiceProviderUtil.checkNullSafeWithObject(row[8], "NA"));
					partnerProductId = ServiceProviderUtil.checkNullSafeWithObject(row[18], "");
					if (null != partnerProductId && !partnerProductId.isEmpty()) {

						afPartnerProductList = transactionDetailsDAO.getProductNameById(partnerProductId);
						if (!afPartnerProductList.isEmpty()) {
							for (AfPartnerProduct afPartnerProduct : afPartnerProductList) {
								productName = ServiceProviderUtil.nullSafe(afPartnerProduct.getProductName(), "NA");
								partnerCategoryName = ServiceProviderUtil.nullSafe(
										afPartnerProduct.getAfPartnerCategory().getPartnerCategoryName(), "NA");
							}
						}
					} 
					if(partnerCategoryName==null || partnerCategoryName.isEmpty()){
						serviceCenterReportsBean.setPartnerCategoryName("NA");
					}else{
						serviceCenterReportsBean.setPartnerCategoryName(partnerCategoryName);
					}
					if(productName == null || productName.isEmpty()){
						serviceCenterReportsBean.setProductName("NA");
					}else{
						serviceCenterReportsBean.setProductName(productName);
					}
					serviceCenterReportsBean.setJpplCategoryName("NA");
					serviceCenterReportsBean.setPrice(ServiceProviderUtil.checkNullSafeWithObject(row[9], "NA"));
					if (!"".equals(row[13])) {
						serviceCenterReportsBean.setJpmilesPurchase( row[13].toString());
					}else{
						serviceCenterReportsBean.setJpmilesPurchase("NA");
					}
					serviceCenterReportsBean.setFormatedExpectedDateOfCredit(
							(row[14] != null && !row[14].equals("")) ? simpleDateFormat.format(row[14]) : "NA");
					serviceCenterReportsBean.setExpectedDateOfCreditTime(
							(row[14] != null && !row[14].equals("")) ? ((Date) row[14]).getTime() : 0);
					if (!"".equals(row[15])) {
						serviceCenterReportsBean.setJpmilesCancel(row[15].toString());
					}else{
						serviceCenterReportsBean.setJpmilesCancel("NA");
					}
					if (!"".equals(row[16])) {
						serviceCenterReportsBean.setJpmilesToCredit( row[16].toString());
					}else{
						serviceCenterReportsBean.setJpmilesToCredit("NA");

					}
					productStatus = ServiceProviderUtil.checkNullSafeWithObject(row[17], "NA");
					if ("1".equalsIgnoreCase(productStatus)) {
						serviceCenterReportsBean.setPurchase_status(AfConstants.TENTATIVE);
					} else if ("2".equalsIgnoreCase(productStatus)) {
						serviceCenterReportsBean.setPurchase_status(AfConstants.PROCESSED);
					} else if ("3".equalsIgnoreCase(productStatus)) {
						serviceCenterReportsBean.setPurchase_status(AfConstants.DISAPPROVED);
					} else if ("4".equalsIgnoreCase(productStatus)) {
						serviceCenterReportsBean.setPurchase_status(AfConstants.FAILED);
					} else {
						serviceCenterReportsBean.setPurchase_status("NA");
					}
					transactionType = ServiceProviderUtil.checkNullSafeWithObject(row[19], "NA");
					if (transactionType.equalsIgnoreCase("7"))
						serviceCenterReportsBean.setTransactionVia(AfConstants.CHROME_EXTENSION);
					else
						serviceCenterReportsBean.setTransactionVia(AfConstants.SHOP_SITE);

					serviceCenterReportsBean.setRatePerMile(ServiceProviderUtil.checkNullSafeWithObject(row[10], "NA"));
					String prtCommAmount=ServiceProviderUtil.checkNullSafeWithObject(row[20], "NA");
					if(prtCommAmount.contains(",")){
						prtCommAmount=prtCommAmount.replace(",", "");
					}
					if (prtCommAmount.contains(".") && (prtCommAmount.length()-1-prtCommAmount.indexOf(".")>2)) {
						prtCommAmount = prtCommAmount.substring(0, prtCommAmount.indexOf(".")+3);
					}
					serviceCenterReportsBean.setCommAmount(prtCommAmount);
//					if (row[9] != null && row[11] != null && !AfConstants.INVALID.equals(statusType)) {
					if (row[9] != null && row[9].toString().length() > 0 && row[11] != null && row[11].toString().length() > 0 && !AfConstants.INVALID.equals(statusType)) {
						Float jpplCommission = (((Float) row[9]) * ((Float) row[11]) / 100);
						DecimalFormat df = new DecimalFormat("#.##");
						String jpplCommissions = df.format(jpplCommission);
						serviceCenterReportsBean.setJpplCommAmount(jpplCommissions);
					} else {
						serviceCenterReportsBean.setJpplCommAmount("NA");
					}
					if ((Integer) row[21] == AfConstants.AMAZON_PRT_NO) {
						String partnerCategory = ServiceProviderUtil.checkNullSafeWithObject(row[22], "NA");
						serviceCenterReportsBean.setPartnerCategoryName(partnerCategory);
						String jpplCategory = categoryManagementService.getJpplCatByPrtCategory(partnerCategory,(Integer) row[21]);
						serviceCenterReportsBean.setJpplCategoryName(jpplCategory);
					}
					serviceCenterReportlist.add(serviceCenterReportsBean);
				}
			}
		} catch (Exception e) {
			
			logger.error("@@@@ Exception in ReportsServiceImpl of getServiceCenterReportsList() :", e);
		}
		return serviceCenterReportlist;
	}

	
	public List<BuisnessReportsBean> getAllExportFinanceReportsList(String fromDate, String toDate,
			String jpNumber, String status, String statusType) {
		List<BuisnessReportsBean> financeReportlist = new ArrayList<>();
		BuisnessReportsBean financeReportsBean = null;
		List<Object[]> reportObject = null;
		String transactionType;
		String productStatus;
		try {
			reportObject = reportsDAO.getShopAllJPReports(fromDate, toDate, jpNumber, status, statusType);
			Map<Byte, String> memberGuestMap = commonService.getEnumValues(AfConstants.MEMBER_OR_GUEST);

			
			if(reportObject.size() == 0)
			{
				financeReportsBean = new BuisnessReportsBean();
				financeReportsBean.setJpNumber("No data found");
				financeReportlist = new ArrayList<>();
				financeReportlist.add(financeReportsBean);
				return financeReportlist;
			}
			
			
			if (reportObject != null && !reportObject.isEmpty()) {
				financeReportlist = new ArrayList<>();

				for (Object[] row : reportObject) {
					financeReportsBean = new BuisnessReportsBean();
					financeReportsBean.setJpNumber(ServiceProviderUtil.checkNullSafeWithObject(row[0], "NA"));
					financeReportsBean.setSource(ServiceProviderUtil.checkNullSafeWithObject(row[1], "NA"));
					financeReportsBean.setMedium(ServiceProviderUtil.checkNullSafeWithObject(row[2], "NA"));
					financeReportsBean.setCampaign(ServiceProviderUtil.checkNullSafeWithObject(row[3], "NA"));
					financeReportsBean.setMember_or_guest(
							(memberGuestMap.get(row[4]) == null) ? "NA" : memberGuestMap.get(row[4]));
					financeReportsBean.setPartnerItemId(ServiceProviderUtil.checkNullSafeWithObject(row[6], "NA"));
					financeReportsBean.setCommissionRate(ServiceProviderUtil.checkNullSafeWithObject(row[11], "NA"));
					financeReportsBean.setMemberShare(ServiceProviderUtil.checkNullSafeWithObject(row[12], "NA"));
					financeReportsBean.setTax("NA");
					if (!"".equals(row[16])) {
						financeReportsBean.setJpmilesToCredit(row[16].toString());
					}else{
						financeReportsBean.setJpmilesToCredit("NA");

					}
					productStatus = ServiceProviderUtil.checkNullSafeWithObject(row[17], "NA");
					if ("1".equalsIgnoreCase(productStatus)) {
						financeReportsBean.setPurchase_status(AfConstants.TENTATIVE);
					} else if ("2".equalsIgnoreCase(productStatus)) {
						financeReportsBean.setPurchase_status(AfConstants.PROCESSED);
					} else if ("3".equalsIgnoreCase(productStatus)) {
						financeReportsBean.setPurchase_status(AfConstants.DISAPPROVED);
					} else if ("4".equalsIgnoreCase(productStatus)) {
						financeReportsBean.setPurchase_status(AfConstants.FAILED);
					} else {
						financeReportsBean.setPurchase_status("NA");
					}

					transactionType = ServiceProviderUtil.checkNullSafeWithObject(row[19], "NA");
					if (transactionType.equalsIgnoreCase("7"))
						financeReportsBean.setTransactionVia(AfConstants.CHROME_EXTENSION);
					else
						financeReportsBean.setTransactionVia(AfConstants.SHOP_SITE);
					financeReportsBean.setRatePerMile(ServiceProviderUtil.checkNullSafeWithObject(row[10], "NA"));
					String prtCommAmount=ServiceProviderUtil.checkNullSafeWithObject(row[20], "NA");
					if(prtCommAmount.contains(",")){
						prtCommAmount=prtCommAmount.replace(",", "");
					}
					if (prtCommAmount.contains(".") && (prtCommAmount.length()-1-prtCommAmount.indexOf(".")>2)) {
						prtCommAmount = prtCommAmount.substring(0, prtCommAmount.indexOf(".")+3);
					}
					financeReportsBean.setCommAmount(prtCommAmount);
//					if (row[9] != null && row[11] != null && !AfConstants.INVALID.equals(statusType)) {
					if (row[9] != null && row[9].toString().length() > 0 && row[11] != null && row[11].toString().length() > 0 && !AfConstants.INVALID.equals(statusType)) {
						Float jpplCommission = (((Float) row[9]) * ((Float) row[11]) / 100);
						DecimalFormat df = new DecimalFormat("#.##");
						String jpplCommissions = df.format(jpplCommission);
						financeReportsBean.setJpplCommAmount(jpplCommissions);
					} else {
						financeReportsBean.setJpplCommAmount("NA");
					}

					financeReportlist.add(financeReportsBean);
				}
			}
		} catch (Exception e) {
			logger.error("@@@@ Exception in ReportsServiceImpl of getFinanceReportsList() :", e);
		}
		return financeReportlist;
	}
	
	
	
	@Override
	public List<BuisnessReportsBean> getAllZeroJpMilesReport(String fromDate, String toDate,  String jpNumber,
		String status, String statusType) {
		BuisnessReportsBean buisnessReportsBean = null;
		String partnerProductId = "";
		String productName = "";
		String partnerCategoryName = "";
		String jpplCategoryName = "";
		List<AfPartnerProduct> afPartnerProductList = null;

		List<Object[]> reportObject = null;
		List<BuisnessReportsBean> reportList = null;
		String transactionType;
		String productStatus;
		
		
		
		try {
			reportObject = reportsDAO.getAllShopJPReportsZeroMiles(fromDate, toDate, jpNumber, status, statusType);
			Map<Byte, String> memberGuestMap = commonService.getEnumValues(AfConstants.MEMBER_OR_GUEST);
//			if (reportObject != null && !reportObject.isEmpty()) {
				reportList = new ArrayList<>();
				
				if(reportObject.size() == 0)
				{
					System.out.println("insdie 0");
					buisnessReportsBean = new BuisnessReportsBean();
					buisnessReportsBean.setJpNumber("No data found");
					reportList = new ArrayList<>();
					reportList.add(buisnessReportsBean);
					return reportList;
				}

			if (reportObject != null && !reportObject.isEmpty()) {
				System.out.println("insdie many");
				for (Object[] row : reportObject) {
					System.out.println("row-->"+row[0]);
//					if ((Integer) row[13] == 0) {
						
						buisnessReportsBean = new BuisnessReportsBean();
						buisnessReportsBean.setJpNumber(ServiceProviderUtil.checkNullSafeWithObject(row[0], "NA"));
						buisnessReportsBean.setSource(ServiceProviderUtil.checkNullSafeWithObject(row[1], "NA"));
						buisnessReportsBean.setMedium(ServiceProviderUtil.checkNullSafeWithObject(row[2], "NA"));
						buisnessReportsBean.setCampaign(ServiceProviderUtil.checkNullSafeWithObject(row[3], "NA"));
						buisnessReportsBean.setMember_or_guest(
								(memberGuestMap.get(row[4]) == null) ? "NA" : memberGuestMap.get(row[4]));
						buisnessReportsBean.setTransactionId(ServiceProviderUtil.checkNullSafeWithObject(row[5], "NA"));
						buisnessReportsBean.setPartnerItemId(ServiceProviderUtil.checkNullSafeWithObject(row[6], "NA"));
						buisnessReportsBean
								.setFormatedTransactionDate((row[7] != null) ? simpleDateFormat.format(row[7]) : "NA");
						buisnessReportsBean.setOrderDateTime((row[7] != null) ? ((Date) row[7]).getTime() : 0);
						buisnessReportsBean.setPartnerName(ServiceProviderUtil.checkNullSafeWithObject(row[8], "NA"));

						partnerProductId = ServiceProviderUtil.checkNullSafeWithObject(row[18], "");
						if (null != partnerProductId && !partnerProductId.isEmpty()) {

							afPartnerProductList = transactionDetailsDAO.getProductNameById(partnerProductId);
							for (AfPartnerProduct afPartnerProduct : afPartnerProductList) {
								productName = ServiceProviderUtil
										.checkNullSafeWithObject(afPartnerProduct.getProductName(), "NA");
								partnerCategoryName = ServiceProviderUtil.checkNullSafeWithObject(
										afPartnerProduct.getAfPartnerCategory().getPartnerCategoryName(), "NA");
							}
							buisnessReportsBean.setPartnerCategoryName(partnerCategoryName);
							buisnessReportsBean.setProductName(productName);
						} else {
							buisnessReportsBean.setPartnerCategoryName("NA");
							buisnessReportsBean.setProductName("NA");
						}
						buisnessReportsBean.setPrice(ServiceProviderUtil.checkNullSafeWithObject(row[9], "NA"));
						buisnessReportsBean.setRatePerMile(ServiceProviderUtil.checkNullSafeWithObject(row[10], "NA"));
						buisnessReportsBean
								.setCommissionRate(ServiceProviderUtil.checkNullSafeWithObject(row[11], "NA"));
						buisnessReportsBean.setMemberShare(ServiceProviderUtil.checkNullSafeWithObject(row[12], "NA"));
						if (!"".equals(row[13]))
							buisnessReportsBean.setJpmilesPurchase( row[13].toString());
                        
						if(!("invalid".equals(statusType))) {
						if ((row[25]) !=null && !"".equals(row[25])){
                        	buisnessReportsBean.setBonusJpmiles(row[25].toString());
                        }else{
                        	buisnessReportsBean.setBonusJpmiles("NA");
                        }
						}
						
						buisnessReportsBean.setFormatedExpectedDateOfCredit(
								(row[14] != null && !"".equals(row[14])) ? simpleDateFormat.format(row[14]) : "NA");
						buisnessReportsBean.setExpectedDateOfCreditTime(
								(row[14] != null && !row[14].equals("")) ? ((Date) row[14]).getTime() : 0);
						if (!"".equals(row[15])) {
							buisnessReportsBean.setJpmilesCancel( row[15].toString());
						}
						if (!"".equals(row[16])) {
							buisnessReportsBean.setJpmilesToCredit( row[16].toString());
						}

						productStatus = ServiceProviderUtil.checkNullSafeWithObject(row[17], "NA");
						if ("1".equalsIgnoreCase(productStatus)) {
							buisnessReportsBean.setPurchase_status(AfConstants.TENTATIVE);
						} else if ("2".equalsIgnoreCase(productStatus)) {
							buisnessReportsBean.setPurchase_status(AfConstants.PROCESSED);
						} else if ("3".equalsIgnoreCase(productStatus)) {
							buisnessReportsBean.setPurchase_status(AfConstants.DISAPPROVED);
						} else if ("4".equalsIgnoreCase(productStatus)) {
							buisnessReportsBean.setPurchase_status(AfConstants.FAILED);
						} else {
							buisnessReportsBean.setPurchase_status("NA");
						}
						buisnessReportsBean
								.setPartnerProductId(ServiceProviderUtil.checkNullSafeWithObject(row[18], "NA"));
						transactionType = ServiceProviderUtil.checkNullSafeWithObject(row[19], "NA");
						if (transactionType.equalsIgnoreCase("7"))
							buisnessReportsBean.setTransactionVia(AfConstants.CHROME_EXTENSION);
						else
							buisnessReportsBean.setTransactionVia(AfConstants.SHOP_SITE);
						buisnessReportsBean.setTax("NA");
						buisnessReportsBean.setCommAmount(ServiceProviderUtil.checkNullSafeWithObject(row[20], "NA"));

//						if (row[9] != null && row[11] != null && !AfConstants.INVALID.equals(statusType)) {
						if (row[9] != null && row[9].toString().length() > 0 && row[11] != null && row[11].toString().length() > 0 && !AfConstants.INVALID.equals(statusType)) {
							Float jpplCommission = (((Float) row[9]) * ((Float) row[11]) / 100);
							DecimalFormat df = new DecimalFormat("#.##");
							String jpplCommissions = df.format(jpplCommission);
							buisnessReportsBean.setJpplCommAmount(jpplCommissions);
						} else {
							buisnessReportsBean.setJpplCommAmount("NA");
						}
						if ((Integer) row[21] == AfConstants.AMAZON_PRT_NO) {
							String partnerCategory = ServiceProviderUtil.checkNullSafeWithObject(row[22], "NA");
							buisnessReportsBean.setPartnerCategoryName(partnerCategory);
							String jpplCategory = categoryManagementService.getJpplCatByPrtCategory(partnerCategory,(Integer) row[21]);
							buisnessReportsBean.setJpplCategoryName(jpplCategory);
						}
						
						
						if(!("invalid".equals(statusType))) {	
						buisnessReportsBean.setEmailStatus(row[23].toString());
						
						if (row[24] != null){
							buisnessReportsBean.setSmsStatus( row[24].toString());
						}
						else{
							buisnessReportsBean.setSmsStatus("0");
						}
					}
						reportList.add(buisnessReportsBean);
					}
				}
//			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("@@@@ Exception in ReportsServiceImpl of getBuisnessReportsList() :", e);
		}
		return reportList;
	}

	
	@Override
	public List<BuisnessReportsBean> getAllExportAnalyticsReportsList(String fromDate, String toDate,
			String jpNumber, String status, String statusType) {
		List<BuisnessReportsBean> analyticsReportlist = new ArrayList<>();
		BuisnessReportsBean analyticsReportsBean = null;
		String partnerProductId = "";
		String productName = "";
		String partnerCategoryName = "";
		String jpplCategoryName = "";
		List<AfPartnerProduct> afPartnerProductList = null;
		List<Object[]> reportObject = null;
		String transactionType;
		String productStatus;
		try {
			reportObject = reportsDAO.getShopAllJPReports(fromDate, toDate, jpNumber, status, statusType);
			Map<Byte, String> memberGuestMap = commonService.getEnumValues(AfConstants.MEMBER_OR_GUEST);

			if(reportObject.size() == 0)
			{
				System.out.println("insdie 0");
				analyticsReportsBean = new BuisnessReportsBean();
				analyticsReportsBean.setJpNumber("");
				analyticsReportlist = new ArrayList<>();
				analyticsReportlist.add(analyticsReportsBean);
				return analyticsReportlist;
			}
			
			
			if (reportObject != null && !reportObject.isEmpty()) {
				analyticsReportlist = new ArrayList<>();

				for (Object[] row : reportObject) {
					analyticsReportsBean = new BuisnessReportsBean();
					analyticsReportsBean.setJpNumber(ServiceProviderUtil.checkNullSafeWithObject(row[0], "NA"));
					analyticsReportsBean.setSource(ServiceProviderUtil.checkNullSafeWithObject(row[1], "NA"));
					analyticsReportsBean.setMedium(ServiceProviderUtil.checkNullSafeWithObject(row[2], "NA"));
					analyticsReportsBean.setCampaign(ServiceProviderUtil.checkNullSafeWithObject(row[3], "NA"));
					analyticsReportsBean.setMember_or_guest(
							(memberGuestMap.get(row[4]) == null) ? "NA" : memberGuestMap.get(row[4]));
					analyticsReportsBean.setTransactionId(ServiceProviderUtil.checkNullSafeWithObject(row[5], "NA"));
					analyticsReportsBean.setPartnerItemId(ServiceProviderUtil.checkNullSafeWithObject(row[6], "NA"));
					analyticsReportsBean.setFormatedTransactionDate(
							(row[7] != null && !row[7].equals("")) ? simpleDateFormat.format(row[7]) : "NA");
					analyticsReportsBean
							.setOrderDateTime((row[7] != null && !row[7].equals("")) ? ((Date) row[7]).getTime() : 0);
					analyticsReportsBean.setPartnerName(ServiceProviderUtil.checkNullSafeWithObject(row[8], "NA"));
					partnerProductId = ServiceProviderUtil.checkNullSafeWithObject(row[18], "");
					if (null != partnerProductId && !partnerProductId.isEmpty()) {

						afPartnerProductList = transactionDetailsDAO.getProductNameById(partnerProductId);
						if (!afPartnerProductList.isEmpty()) {
							for (AfPartnerProduct afPartnerProduct : afPartnerProductList) {
								productName = ServiceProviderUtil.nullSafe(afPartnerProduct.getProductName(), "NA");
								partnerCategoryName = ServiceProviderUtil.nullSafe(
										afPartnerProduct.getAfPartnerCategory().getPartnerCategoryName().toString(),
										"NA");
							}
						}
					} 
					if(partnerCategoryName==null || partnerCategoryName.isEmpty()){
						analyticsReportsBean.setPartnerCategoryName("NA");
					}else{
						analyticsReportsBean.setPartnerCategoryName(partnerCategoryName);
					}
					if(productName == null || productName.isEmpty()){
						analyticsReportsBean.setProductName("NA");
					}else{
						analyticsReportsBean.setProductName(productName);
					}
					analyticsReportsBean.setJpplCategoryName("NA");
					analyticsReportsBean.setPrice(ServiceProviderUtil.checkNullSafeWithObject(row[9], "NA"));
					if (!"".equals(row[13])) {
						analyticsReportsBean.setJpmilesPurchase(row[13].toString());
					}else{
						analyticsReportsBean.setJpmilesPurchase("NA");
					}
					analyticsReportsBean.setFormatedExpectedDateOfCredit(
							(row[14] != null && !row[14].equals("")) ? simpleDateFormat.format(row[14]) : "NA");
					analyticsReportsBean.setExpectedDateOfCreditTime(
							(row[14] != null && !row[14].equals("")) ? ((Date) row[14]).getTime() : 0);
					if (!"".equals(row[15])) {
						analyticsReportsBean.setJpmilesCancel(row[15].toString());
					}else{
						analyticsReportsBean.setJpmilesCancel("NA");
					}
					if (!"".equals(row[16])) {
						analyticsReportsBean.setJpmilesToCredit(row[16].toString());
					}else{
						analyticsReportsBean.setJpmilesToCredit("NA");
					}
					productStatus = ServiceProviderUtil.checkNullSafeWithObject(row[17], "NA");
					if ("1".equalsIgnoreCase(productStatus)) {
						analyticsReportsBean.setPurchase_status(AfConstants.TENTATIVE);
					} else if ("2".equalsIgnoreCase(productStatus)) {
						analyticsReportsBean.setPurchase_status(AfConstants.PROCESSED);
					} else if ("3".equalsIgnoreCase(productStatus)) {
						analyticsReportsBean.setPurchase_status(AfConstants.DISAPPROVED);
					} else if ("4".equalsIgnoreCase(productStatus)) {
						analyticsReportsBean.setPurchase_status(AfConstants.FAILED);
					} else {
						analyticsReportsBean.setPurchase_status("NA");
					}

					transactionType = ServiceProviderUtil.checkNullSafeWithObject(row[19], "NA");
					if (transactionType.equalsIgnoreCase("7"))
						analyticsReportsBean.setTransactionVia(AfConstants.CHROME_EXTENSION);
					else
						analyticsReportsBean.setTransactionVia(AfConstants.SHOP_SITE);
					analyticsReportsBean.setRatePerMile(ServiceProviderUtil.checkNullSafeWithObject(row[10], "NA"));
					String prtCommAmount=ServiceProviderUtil.checkNullSafeWithObject(row[20], "NA");
					if(prtCommAmount.contains(",")){
						prtCommAmount=prtCommAmount.replace(",", "");
					}
					if (prtCommAmount.contains(".") && (prtCommAmount.length()-1-prtCommAmount.indexOf(".")>2)) {
						prtCommAmount = prtCommAmount.substring(0, prtCommAmount.indexOf(".")+3);
					}
					analyticsReportsBean.setCommAmount(prtCommAmount);					
//					if (row[9] != null && row[11] != null && !AfConstants.INVALID.equals(statusType)) {
					if (row[9] != null && row[9].toString().length() > 0 && row[11] != null && row[11].toString().length() > 0 && !AfConstants.INVALID.equals(statusType)) {
						Float jpplCommission = (((Float) row[9]) * ((Float) row[11]) / 100);
						DecimalFormat df = new DecimalFormat("#.##");
						String jpplCommissions = df.format(jpplCommission);
						analyticsReportsBean.setJpplCommAmount(jpplCommissions);
					} else {
						analyticsReportsBean.setJpplCommAmount("NA");
					}
					if ((Integer) row[21] == AfConstants.AMAZON_PRT_NO) {
						String partnerCategory = ServiceProviderUtil.checkNullSafeWithObject(row[22], "NA");
						analyticsReportsBean.setPartnerCategoryName(partnerCategory);
						String jpplCategory = categoryManagementService.getJpplCatByPrtCategory(partnerCategory,(Integer) row[21]);
						analyticsReportsBean.setJpplCategoryName(jpplCategory);
					}
					analyticsReportlist.add(analyticsReportsBean);
				}
			}
		} catch (Exception e) {

			logger.error("@@@@ Exception in ReportsServiceImpl of getAnalyticsReportsList() :", e);
		}
		return analyticsReportlist;
	}
	

	
	
	
}
