package com.af.admin.service;

import java.util.List;

import com.af.admin.beans.EmailManagementBean;


public interface EmailManagementService   {

	void addEmail(EmailManagementBean emailManagementBean);
	public String updateEmail(EmailManagementBean emailManagementBean);
	public List<EmailManagementBean> getAllEmail();
	public EmailManagementBean getEmailById(int emId);
	public String emailTypeExist(String emailType);

}
