package com.af.admin.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;

import com.af.admin.beans.PartnerOrderStatusBean;

public interface PartnerOrderStatusService {

	String savePartnerOrderStatus(PartnerOrderStatusBean partnerOrderStatusBean, HttpServletRequest request);

	public List<PartnerOrderStatusBean> getPartnersStatus(String type);

	public Boolean checkPartnerOrderStatusExist(String prtNo, String partnerStatus, String jpplStatus);

	public String updatePartnerOrderStatus(JSONObject userJson, HttpServletRequest request);

}
