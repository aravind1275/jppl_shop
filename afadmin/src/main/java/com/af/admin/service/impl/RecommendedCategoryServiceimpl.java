package com.af.admin.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.af.admin.DAO.CategoryManagementDAO;
import com.af.admin.DAO.PartnerCategoryDAO;
import com.af.admin.DAO.PartnerManagementDAO;
import com.af.admin.DAO.RecommendedCategoryDAO;
import com.af.admin.beans.RecommendedCategoryBean;
import com.af.admin.constants.AfConstants;
import com.af.admin.entity.AfCategory;
import com.af.admin.entity.AfPartner;
import com.af.admin.entity.AfPartnerCategory;
import com.af.admin.entity.AfRecommendedCategory;
import com.af.admin.service.RecommendedCategoryService;
import com.af.admin.utils.ServiceProviderUtil;

@Component
@Transactional(readOnly=true)
public class RecommendedCategoryServiceimpl implements RecommendedCategoryService {
	private Logger logger = LogManager.getLogger(RecommendedCategoryServiceimpl.class.getName());
	
	@Autowired
	PartnerManagementDAO partnerManagementDAO;
	
	@Autowired
	CategoryManagementDAO categoryManagementDAO;
	
	@Autowired
	PartnerCategoryDAO partnerCategoryDAO;
	
	@Autowired
	RecommendedCategoryDAO rcDAO;
	
	@Override
	public List<RecommendedCategoryBean> getRecommendedCategories() {
		List<AfRecommendedCategory> afRCList = null;		
		List<RecommendedCategoryBean> rcList = new ArrayList<RecommendedCategoryBean>();		
		int rcStatus;
		Integer catNo;
		String dateStr = "";		
		Date dt = null;
		Date  currentDt=new Date();
		try{
			afRCList=rcDAO.getRecommendedCategories();
			for(AfRecommendedCategory afRC:afRCList){
				try{
				RecommendedCategoryBean rcBean = new RecommendedCategoryBean();
				rcBean.setRcNo(afRC.getRcNo());

				logger.info("afRC.getAfCategory() :" + afRC.getAfCategory());
				rcBean.setCatNo((afRC.getAfCategory().getCatNo()!=null)?afRC.getAfCategory().getCatNo():0);
				rcBean.setRcName((afRC.getAfCategory().getCategoryName()!=null)?afRC.getAfCategory().getCategoryName():"");
				
				rcBean.setPosition(afRC.getRcPosition());			
				dt = afRC.getActivationDate();
				dateStr = ServiceProviderUtil.getInstance().getFormattedDate(afRC.getActivationDate(),"dd/MM/yyyy");
				if(dt != null && !dt.equals(""))
					rcBean.setActivationDate(dateStr);
				dt = afRC.getDeactivationDate();
				if(dt!=null && !dt.equals(""))
					dateStr = ServiceProviderUtil.getInstance().getFormattedDate(afRC.getDeactivationDate(),"dd/MM/yyyy");
				if(dt != null && !dt.equals(""))
					rcBean.setDeactivationDate(dateStr);			
				rcStatus = afRC.getRcStatus();

				rcBean.setStatusCode(rcStatus);														

				if(1 == rcStatus){
					rcBean.setRcStatus(AfConstants.STATUS_ACTIVE);
					rcBean.setStatusClass(AfConstants.STATUS_CLASS_GREEN);				
				}
				if(0 == rcStatus){
					rcBean.setRcStatus(AfConstants.STATUS_IN_ACTIVE);
					rcBean.setStatusClass(AfConstants.STATUS_CLASS_RED);				
				}	
				if(dt!=null && currentDt.before(dt)){
					rcBean.setRcStatus(AfConstants.STATUS_ACTIVE);
					rcBean.setStatusClass(AfConstants.STATUS_CLASS_GREEN);
				}
				else{
					rcBean.setRcStatus(AfConstants.STATUS_IN_ACTIVE);
					rcBean.setStatusClass(AfConstants.STATUS_CLASS_RED);
				}
				rcList.add(rcBean);
				}catch(Exception e)
				{
					logger.error("Objec Not Found getRecommendedCategories ", e);
					
				}
			}		
		}catch(Exception e){
			logger.error("@@@@ Exception in RecommendedCategoryServiceimpl getRecommendedCategories() :" + e);
		}
		return rcList;

	}
	
	@Override
	public List<RecommendedCategoryBean> getRecommendedCategories(String prtNo) {
		List<AfRecommendedCategory> afRCList=null;		
		List<RecommendedCategoryBean> rcList=new ArrayList<RecommendedCategoryBean>();		
		int rcStatus;
		String dateStr="";		
		Date dt=null;
		try{
			afRCList=rcDAO.getRecommendedCategories(prtNo);
			logger.debug("afRPList size:"+afRCList.size());
		for(AfRecommendedCategory afRC:afRCList){
			RecommendedCategoryBean rcBean=new RecommendedCategoryBean();
			
			rcBean.setRcNo(afRC.getRcNo());
			rcBean.setPrtNo(afRC.getAfPartner().getPrtNo());
			rcBean.setRcId(afRC.getAfPartnerCategory().getPartnerCategoryId());			
			rcBean.setRcName(afRC.getAfPartnerCategory().getPartnerCategoryName());			
			rcBean.setPosition(afRC.getRcPosition());			
			dt=afRC.getActivationDate();
			dateStr=ServiceProviderUtil.getInstance().getFormattedDate(afRC.getActivationDate(),"dd/MM/yyyy");
			if(dt!=null && !dt.equals(""))
				rcBean.setActivationDate(dateStr);
			dt=afRC.getDeactivationDate();
			
			if(dt!=null && !dt.equals(""))
				dateStr=ServiceProviderUtil.getInstance().getFormattedDate(afRC.getDeactivationDate(),"dd/MM/yyyy");
			if(dt!=null && !dt.equals(""))
				rcBean.setDeactivationDate(dateStr);			
			rcStatus=afRC.getRcStatus();
			
			rcBean.setStatusCode(rcStatus);														
			
			if(1==rcStatus){
				rcBean.setRcStatus(AfConstants.STATUS_ACTIVE);
				rcBean.setStatusClass(AfConstants.STATUS_CLASS_GREEN);				
			}
			if(0==rcStatus){
				rcBean.setRcStatus(AfConstants.STATUS_IN_ACTIVE);
				rcBean.setStatusClass(AfConstants.STATUS_CLASS_RED);				
			}			
			rcList.add(rcBean);
		}		
		}catch(Exception e){
			logger.error("@@@@ Exception in RecommendedCategoryServiceimpl getRecommendedCategories(prtNo) :"+e);
		}
		return rcList;

	}
	
	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String createRecommendedCategory(RecommendedCategoryBean recommendedCategoryBean) {
		AfRecommendedCategory afRecommendedCategory=new AfRecommendedCategory();
		
		AfCategory afcategory=null;
		
		String activationDate="",deactivationDate="",status="";
		Date dt=null;		
		DateFormat sourceDf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");			
		Date dt1=new Date();
		String hh_mi_ss=""+dt1;		
		String format[]=hh_mi_ss.split(" ");
		hh_mi_ss=format[3];		
		try{
			afcategory=categoryManagementDAO.getCategoryByCategoryNo(recommendedCategoryBean.getCatNo().toString());
			if(afcategory!=null)
				afRecommendedCategory.setAfCategory(afcategory);
			
			afRecommendedCategory.setRcPosition((byte)recommendedCategoryBean.getPosition());
			
			activationDate=recommendedCategoryBean.getActivationDate();
			activationDate=activationDate+" "+hh_mi_ss;
			if(activationDate.length()>0)
			dt=sourceDf.parse(activationDate);
			afRecommendedCategory.setActivationDate(dt);
			dt=null;
			
			deactivationDate=recommendedCategoryBean.getDeactivationDate();
			if(deactivationDate!=null && !deactivationDate.isEmpty())
				deactivationDate=deactivationDate+" "+hh_mi_ss;
			
			if(deactivationDate!=null || !deactivationDate.isEmpty())
			{
			if(deactivationDate.length()>0)
			dt=sourceDf.parse(deactivationDate);
			
			}
		
			afRecommendedCategory.setDeactivationDate(dt);
			afRecommendedCategory.setCategoryImageUrl(recommendedCategoryBean.getRcImageUrl());
			afRecommendedCategory.setCategoryLandingUrl(recommendedCategoryBean.getRcLandingUrl());
			afRecommendedCategory.setCategoryImageText(recommendedCategoryBean.getRcImageDesc());
			afRecommendedCategory.setRcStatus((byte)recommendedCategoryBean.getStatusCode());
			afRecommendedCategory.setCreatedBy(1);
			afRecommendedCategory.setCreatedTime(new Date());
			afRecommendedCategory.setStatus((byte) 1);
			status=rcDAO.createRecommendedCategory(afRecommendedCategory);
		
		}catch(Exception e){
			logger.error("@@@@ Exception in RecommendedProductServiceimpl createRecommendedCategory() :"+e);
			return "failure";
		}
		return status;
	}	
	
	@Override
	public boolean checkRecommendedCategory(Map<String, String> rpmap) {
		boolean checkFlag=false;
		List<AfRecommendedCategory> afRecommendedCategory;
		try{
			
			afRecommendedCategory=rcDAO.getRecommendedCategory(rpmap);
			if(afRecommendedCategory!=null && afRecommendedCategory.size()>0)
				checkFlag=true;
			logger.debug(">>>>>>>>checkRecommendedProduct checkFlag>>>"+checkFlag);
		}catch(Exception e){
			logger.error("@@Exception in RecommendedProductServiceimpl checkRecommendedProduct():", e);
		}
		return checkFlag;
	}
	
	@Override
	public RecommendedCategoryBean getRecommendedCategoryByNo(String rcNo) {

		String dateStr = "";
		Date dt = null;
		RecommendedCategoryBean recommendedCategoryBean = null;
		AfRecommendedCategory afRecommendedCategory = null;
		try {
			afRecommendedCategory = rcDAO.getRecommendedCategoryByNo(rcNo);
		} catch(Exception e) {
		}
		if(null != afRecommendedCategory) {
			recommendedCategoryBean = new RecommendedCategoryBean();
			recommendedCategoryBean.setRcNo(afRecommendedCategory.getRcNo());
			if(afRecommendedCategory.getAfCategory() != null) {
				recommendedCategoryBean.setCatNo(afRecommendedCategory.getAfCategory().getCatNo());
				recommendedCategoryBean.setRcName(afRecommendedCategory.getAfCategory().getCategoryName());
			}
			recommendedCategoryBean.setPosition(afRecommendedCategory.getRcPosition());	

			dt = afRecommendedCategory.getActivationDate();
			dateStr = ServiceProviderUtil.getInstance().getFormattedDate(
					afRecommendedCategory.getActivationDate(), "dd/MM/yyyy");
			if(dt != null && !dt.equals(""))
				recommendedCategoryBean.setActivationDate(dateStr);
			dt = null;
			dt = afRecommendedCategory.getDeactivationDate();
			if(dt != null && !dt.equals("")) {
				dateStr = ServiceProviderUtil.getInstance().getFormattedDate(
						afRecommendedCategory.getDeactivationDate(), "dd/MM/yyyy");
				if(dt != null && !dt.equals(""))
					recommendedCategoryBean.setDeactivationDate(dateStr);
			}
			recommendedCategoryBean.setRcImageUrl(afRecommendedCategory.getCategoryImageUrl());
			recommendedCategoryBean.setRcLandingUrl(afRecommendedCategory.getCategoryLandingUrl());
			recommendedCategoryBean.setRcImageDesc(afRecommendedCategory.getCategoryImageText());
			recommendedCategoryBean.setRcStatus(afRecommendedCategory.getRcStatus()+"");
		}
		return recommendedCategoryBean;
	}
	
	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String updateRecommendedCategory(RecommendedCategoryBean recommendedCategoryBean) {
		String activationDate="",deactivationDate="",status="";
		AfRecommendedCategory afRecommendedCategory=null;
		AfPartner afPartner=null;
		AfCategory afCategory=null;
		Date dt=null;		
		DateFormat sourceDf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");			
		Date dt1=new Date();
		String hh_mi_ss=""+dt1;
		
		String format[]=hh_mi_ss.split(" ");
		hh_mi_ss=format[3];		
		
		try{
			afRecommendedCategory=rcDAO.getRecommendedCategoryByNo(recommendedCategoryBean.getRcNo()+"");
		
			if(null!=afRecommendedCategory){
				afCategory=categoryManagementDAO.getCategoryByCategoryNo(recommendedCategoryBean.getCatNo().toString());
				afRecommendedCategory.setAfCategory(afCategory);
				afRecommendedCategory.setRcPosition((byte)recommendedCategoryBean.getPosition());		
				System.out.print("adate:"+recommendedCategoryBean.getActivationDate().split(" ")[0]);
				activationDate=recommendedCategoryBean.getActivationDate().split(" ")[0];
				activationDate=activationDate+" "+hh_mi_ss;
				if(activationDate.length()>0)
				dt=sourceDf.parse(activationDate);
				afRecommendedCategory.setActivationDate(dt);
				
				
				dt=null;
				
				deactivationDate=recommendedCategoryBean.getDeactivationDate();
				if(deactivationDate!=null && !deactivationDate.isEmpty())
					deactivationDate=deactivationDate+" "+hh_mi_ss;
				
				if(deactivationDate!=null || !deactivationDate.isEmpty())
				{
				if(deactivationDate.length()>0)
				dt=sourceDf.parse(deactivationDate);
				
				}
			
				afRecommendedCategory.setDeactivationDate(dt);
				afRecommendedCategory.setCategoryImageUrl(recommendedCategoryBean.getRcImageUrl());
				afRecommendedCategory.setCategoryLandingUrl(recommendedCategoryBean.getRcLandingUrl());
				afRecommendedCategory.setCategoryImageText(recommendedCategoryBean.getRcImageDesc());
				afRecommendedCategory.setModifiedTime(new Date());
				afRecommendedCategory.setRcStatus((byte)Integer.parseInt(recommendedCategoryBean.getRcStatus()));
			
				try{
					status=rcDAO.updateRecommendedCategory(afRecommendedCategory);
					return status;
				}catch(Exception e){
					
					logger.error("@@@@ Exception updateRecommendedCategory():"+e);
					return "failure";
				}
		
		}
		}catch(Exception e){
			logger.error("@@@@ Exception updateRecommendedCategory():"+e);
			return "failure";
		}
		return status;
	}
	
	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String deleteRecommendedCategory(Integer prtNo) {
		String deleteStatus="";
		try{
			deleteStatus=rcDAO.deleteRecommendedCategory(prtNo);
		}catch(Exception e){
			
			logger.error("@@@@ Exception in PartnerManagementServiceImpl deletePartner() :"+e);
			return "failure";
		}
		return deleteStatus;
	}

	@Override
	public boolean checkRecommendedCategoryPosition(String recommendedCategoryPosition) {
		boolean checkFlag=false;
		List<AfRecommendedCategory> afRecommendedCategory;
		try{
			afRecommendedCategory=rcDAO.checkRecommendedCategoryPosition(recommendedCategoryPosition);
			if(afRecommendedCategory!=null && afRecommendedCategory.size()>0){
				checkFlag=true;
			}
		}catch(Exception e){
			logger.error("@@Exception in RecommendedProductServiceimpl checkRecommendedProduct():", e);
		}
		return checkFlag;
	}
	
	@Override
	public boolean checkRcCatNo(String rcCatNo){
		boolean checkFlag=false;
		List<AfRecommendedCategory> afRecommendedCategory;
		try{
			afRecommendedCategory=rcDAO.checkRcCatNo(Integer.parseInt(rcCatNo));
			if(afRecommendedCategory!=null && afRecommendedCategory.size()>0){
				checkFlag=true;
			}
		}catch(Exception e){
			logger.error("@@Exception in RecommendedProductServiceimpl checkRecommendedProduct():", e);
		}
		return checkFlag;
	}
	
	
}
