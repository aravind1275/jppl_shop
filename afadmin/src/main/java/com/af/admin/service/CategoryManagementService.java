package com.af.admin.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.af.admin.beans.AfAmzonoPartnerCategoryBean;
import com.af.admin.beans.CategoryBean;
import com.af.admin.beans.CategoryJSONBean;
import com.af.admin.beans.PartnerCategoryBean;
import com.af.admin.entity.AfCategory;




public interface CategoryManagementService {
	
	public List<CategoryBean> getCategoryList();	
	public String createCategory(CategoryBean categoryBean, HttpServletRequest request);
	public boolean checkcategoryIdExist(String categoryID);
	public boolean checkCategoryPosition(String categoryDisplayOrder);
	public String updateCategory(JSONObject userJson);
	public List<PartnerCategoryBean> getMappingDetails();
	public List<PartnerCategoryBean> getMappingDetailsExceptSearch();
	public String deleteCategory(Integer categoryNo);
	public String deactivateMappedCategory(Integer categoryNo, HttpServletRequest request);
	public String updatePartnerCategory(JSONObject categoryJson, HttpServletRequest request);
	
	public List<PartnerCategoryBean> getPartnerCategories(String partnerNo);

	public String updateCategory(CategoryBean categoryBean, HttpServletRequest request);
	public String deleteCategory(String catNo, RedirectAttributes redirectAttrs, HttpServletRequest request);
	public List<CategoryJSONBean> getCategoryTree(List<CategoryBean> categoryList, int count,
			boolean isStateSelectedYes, String catNo, String actionType);

	public boolean checkcategoryNameExist(String categoryName);
	public boolean checkpartnerCategoryNameExist(String partcategoryName,Integer prtNo);
	public boolean checkpartnerCategoryIdExist(String partcategoryId);
	public Long getLastCategoryCatNo();
	
	public boolean checkCategoryDispOrd(String displayOrder);
	public AfCategory getCategoryByCategoryName(String categoryName);
	
	public String getBestSellingCategories();
	
	public Map<String, AfCategory> getCategoryMap();
	
	public String getJpplCatByPrtCategory(String prtCategory,Integer prtNo);
	public String createPartnerCategory(JSONObject createCatJson, HttpServletRequest request);
	public String updatePartnerCategoryMapped(JSONObject categoryJson, HttpServletRequest request);
	public List<AfAmzonoPartnerCategoryBean> getAmazonMappingDetails();
	public String CreateAmazonPartner(JSONObject categoryJson,
			HttpServletRequest request);
	public String updateAmazonPartner(JSONObject categoryJson,
			HttpServletRequest request);
}
