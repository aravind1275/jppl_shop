package com.af.admin.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;

import com.af.admin.beans.HomePageSectionBean;
import com.af.admin.beans.SectionBean;
import com.af.admin.beans.SectionItemsBean;

public interface SectionManagementService {
	
	public List<SectionBean> getSections();
	public List<SectionBean> getSections(String sectionTitle, int sectionType);
	public String updateHomePageSection(JSONObject homePageSectionJson,HttpServletRequest request);
	public boolean checksecTitle(String secTitle);
	public boolean checkDisplayOrder(String secTitle);
	public boolean checkSectionItemDisplayOrder(String displayOrder,String sectionId);
	public boolean checkDuplicateRecord(String sectionId, String itemType, String prtNo, String catNo, String prtPrdctNo, String prtOfferNo,String brandName);
	public String createSectionItems(SectionItemsBean sectionItemsBean,HttpServletRequest request);
	public List<SectionItemsBean> getSectionItems(String sectionId);
	public SectionItemsBean getSectionItemByItemId(String sectionItemId);
	public String updateSectionItems(JSONObject userJson,HttpServletRequest request);
	public String deleteItem(String itemId,HttpServletRequest request);
	public List<HomePageSectionBean> getHomePageSections();
	public String homeSectionSaveOrder(String reOrder,HttpServletRequest request);
	public String updateHomePageSections(JSONObject homePageSectionJson,HttpServletRequest request);

}
