package com.af.admin.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.af.admin.DAO.PartnerManagementDAO;
import com.af.admin.DAO.PartnerOrderStatusDAO;
import com.af.admin.DAO.UserActivityLogDAO;
import com.af.admin.beans.PartnerOrderStatusBean;
import com.af.admin.entity.AfPartner;
import com.af.admin.entity.AfUserActivityLog;
import com.af.admin.entity.PartnerOrderStatus;
import com.af.admin.service.PartnerOrderStatusService;
import com.af.admin.utils.AdminUserActivityUtil;


@Component
@Transactional(readOnly=true)
public class PartnerOrderStatusServiceImpl implements PartnerOrderStatusService {
	private Logger logger = LogManager.getLogger(BannerManagementServiceimpl.class.getName());
	
	@Autowired
	PartnerOrderStatusDAO partnerOrderStatusDAO;
	
	@Autowired
	PartnerManagementDAO partnerManagementDAO;
	
	@Autowired
	UserActivityLogDAO userActLogDao;
	
	@Autowired
	AdminUserActivityUtil adminUserActivityUtil;

	@Override
	public List<PartnerOrderStatusBean> getPartnersStatus(String type) {
		List<PartnerOrderStatus> partnerOrderStatusList=null;
		List<PartnerOrderStatusBean> partnerOrderStatusBeanList=new ArrayList<>();
		try{
			partnerOrderStatusList=partnerOrderStatusDAO.getPartnersStatus(type);
			for(PartnerOrderStatus partnerOrderStatus:partnerOrderStatusList){
				PartnerOrderStatusBean partnerOrderStatusBean=new PartnerOrderStatusBean();
				partnerOrderStatusBean.setId(partnerOrderStatus.getId());
				partnerOrderStatusBean.setPrtNo(partnerOrderStatus.getPrtNo().toString());
				partnerOrderStatusBean.setPartnerName(partnerOrderStatus.getPartnerName());
				partnerOrderStatusBean.setPartnerStatus(partnerOrderStatus.getPartnerStatus());
				partnerOrderStatusBean.setJpplStatus(partnerOrderStatus.getJpplStatus());
				
				partnerOrderStatusBeanList.add(partnerOrderStatusBean);
			}
			
		}
		catch(Exception e){
			logger.error("@@@@ Exception in PartnerOrderStatusServiceImpl at getPartnersStatus() :", e);
		}
		return partnerOrderStatusBeanList;
	}
	
	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String savePartnerOrderStatus(PartnerOrderStatusBean partnerOrderStatusBean, HttpServletRequest request) {
		PartnerOrderStatus partnerOrderStatus =new PartnerOrderStatus();
		String status="";
		String oldOrNewVal = "";
		try{
			partnerOrderStatus.setId(partnerOrderStatusBean.getId());
		    AfPartner af=	partnerManagementDAO.getPartnerByPartnerNO(partnerOrderStatusBean.getPrtNo().toString());
		    partnerOrderStatus.setPrtNo(af.getPrtNo());
		    partnerOrderStatus.setPartnerName(af.getPartnerName());
		    partnerOrderStatus.setPartnerStatus(partnerOrderStatusBean.getPartnerStatus());
		    switch(partnerOrderStatusBean.getJpplStatus()){
			case "tentative":
				partnerOrderStatus.setJpplStatus("tentative");
				break;
			case "processed":
				partnerOrderStatus.setJpplStatus("processed");
				break;
			case "disapproved":
				partnerOrderStatus.setJpplStatus("disapproved");
				break;
			case "failed":
				partnerOrderStatus.setJpplStatus("failed");
				break;	
		}
		    partnerOrderStatus.setStatus((byte) 1);
		    status=partnerOrderStatusDAO.savePartnerOrderStatus(partnerOrderStatus);
		    /*if("success".equalsIgnoreCase(status)){
				String ipAddress = request.getHeader("X-FORWARDED-FOR");
				if(ipAddress == null) {
					ipAddress = request.getRemoteAddr();
				}
				ipAddress = (null != ipAddress) ? ipAddress.trim() : "";
				AfUserActivityLog afPrtActLog=new AfUserActivityLog();
				oldOrNewVal=" Partners : "+partnerOrderStatus.getPartnerName()+" | "+"Partners Status: "+partnerOrderStatus.getPartnerStatus()+" | "+
						""+"Partners Jppl Status: "+partnerOrderStatus.getJpplStatus()+"| ";
								
			
						afPrtActLog.setUserNewFieldValue(oldOrNewVal);
						afPrtActLog.setUserOldFieldValue("NA");
						afPrtActLog.setUserActivity("Create Order Status");
						afPrtActLog.setUserActCreatedDateTime(new Date());
						afPrtActLog.setUserActIpVal(ipAddress);
						afPrtActLog.setUserActModule("Order Status");
						afPrtActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
						
						userActLogDao.createUserActLog(afPrtActLog);
			}*/
		}
		catch(Exception e){
			logger.error("@@@@ Exception in PartnerOrderStatusServiceImpl at savePartnerOrderStatus() :", e);
			return "failure";
		}
		return status;
	}

	@Override
	public Boolean checkPartnerOrderStatusExist(String prtNo, String partnerStatus, String jpplStatus) {
		boolean partnerOrderStatusExist=false;
		try{
			partnerOrderStatusExist=partnerOrderStatusDAO.checkPartnerOrderStatusExist(prtNo,partnerStatus,jpplStatus);
		}catch(Exception e){
			logger.error("@@Exception in PartnerOrderStatusServiceImpl checkPartnerOrderStatusExist", e);
		}
		return partnerOrderStatusExist;
	}

	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String updatePartnerOrderStatus(JSONObject userJson, HttpServletRequest request) {
		Integer id = -1;	
		String status="";
		PartnerOrderStatus partnerOrderStatus=null;
		PartnerOrderStatus partnerOrderStatusOld=null;
		String replaceInOld="";
		String replaceInNew="";
		try{
		id = userJson.getInt("id");
		partnerOrderStatus=partnerOrderStatusDAO.getPartnerOrderStatusid(id);
		partnerOrderStatusOld=(PartnerOrderStatus) partnerOrderStatus.clone();
		String prtNo = userJson.getString("prtNo");
		AfPartner af=partnerManagementDAO.getPartnerByPartnerNO(prtNo);
		if(!"".equals(prtNo)){
			partnerOrderStatus.setPrtNo(af.getPrtNo());
		} else {
			partnerOrderStatus.setPrtNo(null);
		}
		
		partnerOrderStatus.setPartnerName(af.getPartnerName());
		partnerOrderStatus.setJpplStatus(userJson.getString("jpplStatus"));
		partnerOrderStatus.setPartnerStatus(userJson.getString("partnerStatus"));
		status=partnerOrderStatusDAO.updatePartnerOrderStatus(partnerOrderStatus);
		
		/*if("success".equalsIgnoreCase(status)){
			String ipAddress = request.getHeader("X-FORWARDED-FOR");
			if (ipAddress == null) {
				ipAddress = request.getRemoteAddr();
			}
			AfUserActivityLog updateUsActLog = new AfUserActivityLog();
			new LinkedHashMap<>();
			
				updateUsActLog.setUserActivity("Update Order Status");
				updateUsActLog.setUserActCreatedDateTime(new Date());
				updateUsActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
				updateUsActLog.setUserActIpVal(ipAddress);
				updateUsActLog.setUserActModule("Order Status");
				
				if(!partnerOrderStatus.getPartnerName().equalsIgnoreCase(partnerOrderStatusOld.getPartnerName())){
					replaceInOld=replaceInOld+"|Partner  :"+partnerOrderStatusOld.getPartnerName();
				    replaceInNew=replaceInNew+"|Partner  :"+partnerOrderStatus.getPartnerName();
				}
				if(!partnerOrderStatus.getPartnerStatus().equalsIgnoreCase(partnerOrderStatusOld.getPartnerStatus())){
					replaceInOld=replaceInOld+"|Partners Status:"+partnerOrderStatusOld.getPartnerStatus();
					replaceInNew=replaceInNew+"|Partners Status:"+partnerOrderStatus.getPartnerStatus();
				}
				if(!partnerOrderStatus.getJpplStatus().equalsIgnoreCase(partnerOrderStatusOld.getJpplStatus())){
					replaceInOld=replaceInOld+"|Partners Jppl Status :"+partnerOrderStatusOld.getJpplStatus();
					replaceInNew=replaceInNew+"|Partners Jppl Status :"+partnerOrderStatus.getJpplStatus();
				}
				
				updateUsActLog.setUserNewFieldValue(replaceInNew);
	    		updateUsActLog.setUserOldFieldValue(replaceInOld);
				userActLogDao.updateUserLog(updateUsActLog);
			
			
		}*/
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error("@@Exception in PartnerOrderStatusServiceImpl updatePartnerOrderStatus", e);
		}
		return status;
	}


}
