package com.af.admin.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;

import com.af.admin.beans.AdminRolesBean;
import com.af.admin.beans.AdminUserBean;
import com.af.admin.beans.AfAmzonoPartnerCategoryBean;
import com.af.admin.beans.ChangePasswordBean;
import com.af.admin.beans.KeyWordBean;
import com.af.admin.beans.PartnerBean;
import com.af.admin.beans.PartnerMappingBean;
import com.af.admin.entity.AfPartner;
import com.af.admin.entity.AfPermissionValues;



public interface PartnerManagementService {
	public String createPartner(PartnerBean partnerBean,HttpServletRequest request);
	public boolean checkPartnerID(String partnerID);
	public boolean checkPartnerName(String partnerName);
	public String deletePartner(Integer partnerID, HttpServletRequest request);
	public List<PartnerBean> getPartners();
	public List<PartnerBean> getPartners(String partnerType);
	public String updatePartner(JSONObject userJson, HttpServletRequest request);
	public String updatePartnerCategoryMap(JSONObject userMapJson, HttpServletRequest request);
	public String updateLogoPartner(JSONObject userJson, HttpServletRequest request);
	public String saveOrder(String reOrder, String prtType, String curPage, String pageLen, HttpServletRequest request);
	public boolean checkLogoPrtDisplayOrder(String prtPos, String prtType);
	public boolean checkLogoPrtTilePosition(String prtPos, String prtType);
	public List<PartnerBean> getLogoPartners();
	public List<PartnerBean> getOfflinePartner();
	public List<PartnerBean> getHostPartner();
	public List<PartnerBean> getTalkPartner();
	public List<PartnerBean> getReadPartner();
	public List<KeyWordBean> getKeywordFileName(String logoprtNo);
	public String saveLogoPartnerConfig(String tileNo, HttpServletRequest request);
	public String getLgoPrtTileSearchresult();
	public List<PartnerBean> getDefaultPartners();
	public List<PartnerBean> getDefaultAndHostPartners();
	public List<PartnerBean> getAllPartners();
	public AfPartner getPartnerByName(String partnerName);
	public boolean checkActivityID(String activityID);
	public Map<String, AfPartner> getPartnerMap();
	
	public String savePopupContentByPartner(PartnerBean partnerBean,HttpServletRequest request);
	public List<PartnerBean> getLogoPartners(String prtType);
	
	public List<Map<String, String>> getAllPrtWebservice();
	public String getPartnerMappedCategoriesByPrtNo(String prtNameCatMapping);
	public List<PartnerBean> getPartnerListPagination(int length, int start, String partnerType);
	public Long getPartnerListPaginationCount(String partnerType);
	public Boolean checkAggregateID(String aggregateid);
	public String createPartnerMapping(JSONObject createPatJson, HttpServletRequest request);
	public List<PartnerMappingBean>getPartnerMappingDetails();
	public String updatePartnerMapping(JSONObject createPrtJson, HttpServletRequest request);
	public String deletePartnerMapping(Integer id, HttpServletRequest request);
	public Boolean MappingPartnerNameExist(String patName,String partNumber);
}
