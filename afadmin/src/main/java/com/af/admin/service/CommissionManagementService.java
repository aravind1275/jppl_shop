package com.af.admin.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;

import com.af.admin.beans.CommissionBean;
import com.af.admin.beans.CommissionUploadBean;
import com.af.admin.entity.AfPartnerCommission;




public interface CommissionManagementService {
	public List<CommissionBean> getPartnersCommission(String partNo);
	public List<CommissionBean> getPartnersCommission();
	public String updateCommission(JSONObject commissionJson,HttpServletRequest request);
	public String addNewCommission(JSONObject commissionJson,HttpServletRequest request);
	
	
	public boolean checkPartnerProducts(Map<String, String> rpmap);
	public String addCommission(CommissionBean commissionBean, HttpServletRequest request);
	public String checkExistingDetailsStartDate(JSONObject commissionJson);
	public String checkExistingDetailsEditStartDate(JSONObject commissionJson);
	public String checkExistingDetailsEndDate(JSONObject commissionJson);
	public String checkExistingDetailsEditEndDate(JSONObject commissionJson);
	
	public List<CommissionBean> getPartnersCommission(String fromDate, String toDate, String partnerNo);
	public List<CommissionBean> getPartnersCommissionPagination(String fromDate, String toDate, String partnerNo,int length,int start);
	public Long getPartnersCommissionPaginationCount(String fromDate, String toDate, String partnerNo);
	
	public String saveJPMileConfiguration(String jpMile,HttpServletRequest request);
	
	public List<Map<String, String>> getPartnerCommissionWebservice();
	public String updateCommissionReport(List<CommissionUploadBean> commissionList);
	public List<CommissionBean> getCommissionExportCsvList();
}
