package com.af.admin.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.af.admin.DAO.CategoryManagementDAO;
import com.af.admin.DAO.CommonDAO;
import com.af.admin.DAO.PartnerManagementDAO;
import com.af.admin.DAO.PartnerProductDAO;
import com.af.admin.DAO.RecommendedProductDAO;
import com.af.admin.beans.PartnerCategoryBean;
import com.af.admin.beans.RecommendedProductBean;
import com.af.admin.constants.AfConstants;
import com.af.admin.entity.AfPartner;
import com.af.admin.entity.AfPartnerProduct;
import com.af.admin.entity.AfProduct;
import com.af.admin.entity.AfRecommendedProduct;
import com.af.admin.service.CommonService;
import com.af.admin.service.RecommendedProductService;
import com.af.admin.service.SendGetRequest;
import com.af.admin.utils.CommonUtil;
import com.af.admin.utils.ServiceProviderUtil;
import com.af.admin.ws.Services;



@Component
@Transactional(readOnly=true)
public class RecommendedProductServiceimpl implements RecommendedProductService {
	private Logger logger = LogManager.getLogger(RecommendedCategoryServiceimpl.class.getName());

	@Autowired
	Services services;
	
	@Autowired
	SendGetRequest sendGetRequest;
	
	@Autowired
	CommonUtil commonUtil;
	
	@Autowired
	PartnerManagementDAO partnerManagementDAO;

	@Autowired
	CategoryManagementDAO categoryManagementDao;

	@Autowired
	PartnerProductDAO partnerProductDAO;


	@Autowired
	CommonDAO commonDAO;


	@Autowired
	RecommendedProductDAO rpDAO;

	@Autowired
	CommonService commonService;

	@Override
	public List<RecommendedProductBean> getRecommendedProducts() {
		List<AfRecommendedProduct> afRPList=null;		
		List<RecommendedProductBean> rpList=new ArrayList<RecommendedProductBean>();		
		int rpStatus;
		String dateStr="";		
		Date dt=null;

		try{
			afRPList=rpDAO.getRecommendedProducts();
			for(AfRecommendedProduct afRP:afRPList){


				RecommendedProductBean rpBean=new RecommendedProductBean();

				rpBean.setRpNo(afRP.getRpNo());
				if(afRP.getAfProduct()!=null)
				{
				rpBean.setRpName(afRP.getAfProduct().getProductName());
				}
				rpBean.setPosition(afRP.getRpPosition());
				dt=afRP.getActivationDate();
				if(dt!=null && !dt.equals(""))
				{
				dateStr=ServiceProviderUtil.getInstance().getFormattedDate(dt,"dd/MM/yyyy");
				rpBean.setActivationDate(dateStr);
				}
				dt=null;
				dt=afRP.getDeactivationDate();
				if(dt!=null && !dt.equals(""))
				{
				dateStr=ServiceProviderUtil.getInstance().getFormattedDate(dt,"dd/MM/yyyy");
				rpBean.setDeactivationDate(dateStr);
				}
				rpStatus=afRP.getRpStatus();

				rpBean.setStatusCode(rpStatus);														

				if(1==rpStatus){
					rpBean.setRpStatus(AfConstants.STATUS_ACTIVE);
					rpBean.setStatusClass(AfConstants.STATUS_CLASS_GREEN);				
				}
				if(0==rpStatus){
					rpBean.setRpStatus(AfConstants.STATUS_IN_ACTIVE);
					rpBean.setStatusClass(AfConstants.STATUS_CLASS_RED);				
				}			
				rpList.add(rpBean);
			}

		}catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in RecommendedProductServiceimpl getRecommendedProducts() :"+e);
		}
		return rpList;

	}

	

	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String createRecommendedProduct(RecommendedProductBean recommendedProductBean) {
		String status;
		AfRecommendedProduct afRecommendedProduct=new AfRecommendedProduct();
		AfPartner afPartner=null;
		AfPartnerProduct afPartnerProduct=null;
		AfProduct afProduct=null;


		String partnerName="",partnerId="",activationDate="",deactivationDate="";
		Date dt=null;		
		DateFormat sourceDf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");			
		Date dt1=new Date();
		String hh_mi_ss=""+dt1;		
		String format[]=hh_mi_ss.split(" ");
		hh_mi_ss=format[3];		
		logger.info("activationDate"+activationDate+"partnerName"+partnerName+"partnerId"+partnerId);
		try{

			afProduct=partnerProductDAO.getProductByPrtPrdNO(recommendedProductBean.getPrtPrdNo().toString());
			if(afProduct!=null)
			afRecommendedProduct.setAfProduct(afProduct);
			afRecommendedProduct.setRpPosition((byte)recommendedProductBean.getPosition());

			
			activationDate=recommendedProductBean.getActivationDate();
			activationDate=activationDate+" "+hh_mi_ss;
			if(activationDate.length()>0)
				dt=sourceDf.parse(activationDate);
			afRecommendedProduct.setActivationDate(dt);
			dt = null;

			deactivationDate=recommendedProductBean.getDeactivationDate();

			if(deactivationDate != null && deactivationDate.length()>0){
				deactivationDate=deactivationDate+" "+hh_mi_ss;
				dt=sourceDf.parse(deactivationDate);
			}

			afRecommendedProduct.setDeactivationDate(dt);

			afRecommendedProduct.setRpStatus((byte)recommendedProductBean.getStatusCode());
			afRecommendedProduct.setCreatedBy(1);
			afRecommendedProduct.setCreatedTime(new Date());
			afRecommendedProduct.setStatus((byte) 1);
			status=rpDAO.createRecommendedProduct(afRecommendedProduct);

		}catch(Exception e){
			e.printStackTrace();
			status="failure";
			logger.error("@@@@ Exception in RecommendedProductServiceimpl createBanner() :"+e);
			return status;
		}
		return status;
	}	

	@Override
	public boolean checkRecommendedProduct(Map<String, String> rpmap) {
		boolean checkFlag=false;
		List<AfRecommendedProduct> afRecommendedProduct;
		try{
			
			afRecommendedProduct=rpDAO.getRecommendedProduct(rpmap);
			if(afRecommendedProduct!=null && afRecommendedProduct.size()>0)
				checkFlag=true;
			logger.debug(">>>>>>>>checkRecommendedProduct checkFlag>>>"+checkFlag);
			
		}catch(Exception e){
			logger.error("@@Exception in RecommendedProductServiceimpl checkRecommendedProduct():", e);
		}
		return checkFlag;
	}

	@Override
	public boolean checkRecommendedProductPosition(String prtPrdPos) {
		boolean checkFlag=false;
		List<AfRecommendedProduct> afRecommendedProduct;
		try{
			afRecommendedProduct=rpDAO.checkRecommendedProductPosition(prtPrdPos);
			if(afRecommendedProduct!=null && afRecommendedProduct.size()>0){
				checkFlag=true;
			}
		}catch(Exception e){
			logger.error("@@Exception in RecommendedProductServiceimpl checkRecommendedProduct():", e);
		}
		return checkFlag;
	}


	@Override
	public RecommendedProductBean getRecommendedProductByNo(String rpNo) {

		String dateStr="";
		Date dt=null;
		RecommendedProductBean recommendedProductBean = null;
		AfRecommendedProduct afRecommendedProduct =null;
		List<PartnerCategoryBean> partnerCategoryBean = null;
		try{
			afRecommendedProduct=rpDAO.getRecommendedProductByNo(rpNo);
		}catch(Exception e){
		}


		if(null!=afRecommendedProduct){
			recommendedProductBean=new RecommendedProductBean();
			recommendedProductBean.setRpNo(afRecommendedProduct.getRpNo());
			recommendedProductBean.setPrtPrdNo(afRecommendedProduct.getAfProduct().getMasterProductId());
			recommendedProductBean.setRpName(afRecommendedProduct.getAfProduct().getProductName());
			recommendedProductBean.setPosition(afRecommendedProduct.getRpPosition());	
			dt=afRecommendedProduct.getActivationDate();
			dateStr=ServiceProviderUtil.getInstance().getFormattedDate(afRecommendedProduct.getActivationDate(),"dd/MM/yyyy");
			if(dt!=null && !dt.equals(""))
				recommendedProductBean.setActivationDate(dateStr);

			dt=afRecommendedProduct.getDeactivationDate();
			if(dt!=null && !dt.equals(""))
			{
				dateStr=ServiceProviderUtil.getInstance().getFormattedDate(dt,"dd/MM/yyyy");
				recommendedProductBean.setDeactivationDate(dateStr);
			}
			recommendedProductBean.setRpStatus(afRecommendedProduct.getRpStatus()+"");

		}
		return recommendedProductBean;
	}

	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String updateRecommendedProduct(RecommendedProductBean recommendedProductBean) {
		String activationDate="",deactivationDate="",status="";
		AfRecommendedProduct afRecommendedProduct=null;
		AfPartner afPartner=null;
		AfProduct afProduct=null;
		Date dt=null;		
		DateFormat sourceDf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");			
		Date dt1=new Date();
		String hh_mi_ss=""+dt1;

		String format[]=hh_mi_ss.split(" ");
		hh_mi_ss=format[3];		

		try{
			afRecommendedProduct=rpDAO.getRecommendedProductByNo(recommendedProductBean.getRpNo()+"");

			if(null!=afRecommendedProduct){
				afProduct=partnerProductDAO.getMasterProductByPrdNO(recommendedProductBean.getPrtPrdNo().toString());
				afRecommendedProduct.setAfProduct(afProduct);
				afRecommendedProduct.setRpPosition((byte)recommendedProductBean.getPosition());		

				activationDate=recommendedProductBean.getActivationDate().split(" ")[0];
				activationDate=activationDate+" "+hh_mi_ss;
				if(activationDate.length()>0)
					dt=sourceDf.parse(activationDate);
				afRecommendedProduct.setActivationDate(dt);

				dt = null;
				deactivationDate=recommendedProductBean.getDeactivationDate().split(" ")[0];
				if(deactivationDate != null && deactivationDate.length()>0){
					deactivationDate=deactivationDate+" "+hh_mi_ss;
					dt=sourceDf.parse(deactivationDate);
				}

				afRecommendedProduct.setDeactivationDate(dt);

				afRecommendedProduct.setModifiedTime(new Date());
				afRecommendedProduct.setRpStatus((byte)Integer.parseInt(recommendedProductBean.getRpStatus()));

				try{
					status=rpDAO.updateRecommendedProduct(afRecommendedProduct);
				}catch(Exception e){
					logger.error("@@@@ Exception in RecomendedproductServiceImpl updateRecommendedProduct():"+e);
					status="failure";
					return status;
				}
				return status;

			}

		}catch(Exception e){
			logger.error("@@@@ Exception updateRecommendedProduct():"+e);
			status="failure";
			return status;
		}
		return status;
	}

	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String deleteRecommendedProduct(Integer prtNo) {
		String deleteStatus="";
		try{
			deleteStatus=rpDAO.deleteRecommendedProduct(prtNo);

		}catch(Exception e){
			logger.error("@@@@ Exception in RecomendedProductServiceImpl deletePartner() :"+e);
			deleteStatus="failure";
			return deleteStatus;
		}
		return deleteStatus;
	}

	@Override
	public Map<String, String> getProductList(String prtCatNo_,String keyword) {
		int prtCatNo;
		int catNo;
		String response="";
		long startime,endtime;
		prtCatNo = Integer.parseInt(prtCatNo_);
		Map<String, String> pList = new LinkedHashMap<String, String>();
		List<AfProduct> afpList = new ArrayList<AfProduct>();
		List<Integer> listAfCategory =new ArrayList<Integer>();
		try {
			catNo=categoryManagementDao.getPartnerCategoryByPartnerCategoryNo(prtCatNo_).getAfCategory().getCatNo();
			listAfCategory.add(catNo);
			logger.info("/getProductList listAfCategory:"+listAfCategory);
			startime=System.currentTimeMillis();
			response = services.autoSuggProductInfoService(0, listAfCategory, keyword);
			endtime=System.currentTimeMillis();
			logger.info("/getProductList   while gettring response -----------start-time----"+startime+" ms");
			logger.info("/getProductList   while gettring response -----------end-time----"+endtime+" ms");
			logger.info("/getProductList   while gettring response -----------time-diff in Milisec----"+(endtime-startime)+" ms");
			logger.info("/getProductList   while gettring response -----------time-diff in sec----"+((endtime-startime)/1000)+" sec");
			startime=System.currentTimeMillis();
			if(null!=response && !response.isEmpty() && response.length()>0)
				pList = commonUtil.getAutoSuggProductInfoList(response);
			endtime=System.currentTimeMillis();
			logger.info("/getProductList   while parsing response -----------start-time----"+startime+" ms");
			logger.info("/getProductList   while parsing response -----------end-time----"+endtime+" ms");
			logger.info("/getProductList   while parsing response -----------time-diff in Milisec----"+(endtime-startime)+" ms");
			logger.info("/getProductList   while parsing response -----------time-diff in sec----"+((endtime-startime)/1000)+" sec");
		} catch (Exception e) {
			logger.debug("/getPartnerProduct Exception:" + e);
		}
		
		return pList;
	}


}
