package com.af.admin.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.af.admin.DAO.TransactionReportDAO;
import com.af.admin.beans.TransactionReportBean;
import com.af.admin.constants.AfConstants;
import com.af.admin.entity.AfPartner;
import com.af.admin.entity.AfPartnerProduct;
import com.af.admin.entity.AfTransaction;
import com.af.admin.service.TransactionReportService;
import com.af.admin.utils.ServiceProviderUtil;

@Component
@Transactional(readOnly=true)
public class TransactionReportServiceImpl implements TransactionReportService {
	private Logger logger = LogManager.getLogger(TransactionReportServiceImpl.class.getName());
	@Autowired
	TransactionReportDAO transactionReportDAO;
	
	
	@Override
	public List<TransactionReportBean> getTransactionDetails(String trackingId) {
		List<AfTransaction> afTransactionList=null;
		List<TransactionReportBean> transactionReportBeanList=new ArrayList<TransactionReportBean>();
		List<AfPartnerProduct> afPartnerProductList = null;
		
		try{
			String dateStr = "", productId="", partnerProductName="",partnerName="",partnerCategoryName="";
			int transactionStatus;
			Date date = null;
			afTransactionList=transactionReportDAO.getTransactionDetails(trackingId);
		for(AfTransaction afTransaction:afTransactionList){
			TransactionReportBean transactionReportBean=new TransactionReportBean();
			
			dateStr = (String) (date!=null && !date.equals("")?(ServiceProviderUtil.getInstance().getFormattedDate(date,"dd/MM/yyyy")).toString():new Date());
			transactionReportBean.setTransactionDate(dateStr); 
			
			
			dateStr = (String) (date!=null && !date.equals("")?(ServiceProviderUtil.getInstance().getFormattedDate(date,"dd/MM/yyyy")).toString():new Date());
			transactionReportBean.setMilesPostingDate(dateStr);
			afPartnerProductList=transactionReportDAO.getProductNameById(productId);  
			for(AfPartnerProduct afPartnerProduct:afPartnerProductList){
				partnerProductName = ServiceProviderUtil.nullSafe(afPartnerProduct.getProductName(),"");
				partnerName 	= ServiceProviderUtil.nullSafe(afPartnerProduct.getAfPartner().getPartnerName().toString(),"");
				partnerCategoryName	= ServiceProviderUtil.nullSafe(afPartnerProduct.getAfPartnerCategory().getPartnerCategoryName().toString(),"");
			}
			
			transactionReportBean.setPartnerProductName(partnerProductName);
			transactionReportBean.setPartnerName(partnerName);
			transactionReportBean.setPartnerCategoryName(partnerCategoryName);
			
			
						transactionReportBean.setTransactionStatus(AfConstants.TRANSACTION_STATUS_REQUEST);											
						transactionReportBean.setTransactionStatus(AfConstants.TRANSACTION_STATUS_CONFIRM);	
						transactionReportBean.setTransactionStatus(AfConstants.TRANSACTION_STATUS_CANCEL);	
					
			SimpleDateFormat dateFormat = new SimpleDateFormat( "dd/MM/yyyy" );
			Calendar cal = Calendar.getInstance();
			cal.setTime( date );
			cal.add( Calendar.DATE, 70 );
			String expectedCreditDate = dateFormat.format(cal.getTime()); 
			transactionReportBean.setExpectedCreditDate(expectedCreditDate);
			transactionReportBeanList.add(transactionReportBean);
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		return transactionReportBeanList;
	}
}
