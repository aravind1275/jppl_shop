package com.af.admin.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;

import com.af.admin.beans.AdminRolesBean;
import com.af.admin.beans.AdminUserBean;
import com.af.admin.beans.ChangePasswordBean;
import com.af.admin.beans.UserBean;
import com.af.admin.entity.AfPermissionValues;



public interface UserManagementService {
	public AdminUserBean authenticateUser(AdminUserBean adminUserBean);
	public String updateAdminUser(JSONObject userJson,HttpServletRequest request);
	public String createUser(AdminUserBean adminUserBean,HttpServletRequest request);
	public boolean checkUserName(String userName);
	public String deleteUser(Integer userId,HttpServletRequest request);
	public AdminUserBean getUserDetailsByUserName(String userName);

	public List<UserBean> getAdminUsers();
	public Boolean checkRoleName(String roleName);

	public String changePassword(ChangePasswordBean changePasswordBean);
	public String createRole(AdminRolesBean adminRolesBean, HttpServletRequest request);
	
	public void updateRole(AdminRolesBean adminRolesBean, HttpServletRequest request);
	
	public String deleteRole(Integer roleId, HttpServletRequest request);
	
	public Map<AfPermissionValues, Map<Byte, String>> permissionActionsForvalues(List<AfPermissionValues> permissionValues,Map<Byte, String> permissionActions);
	
	public AdminRolesBean getRoleById(String roleId);

	public String[] getRolePermissionsById(String roleId);

}
