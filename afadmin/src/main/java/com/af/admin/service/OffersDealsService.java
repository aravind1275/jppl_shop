package com.af.admin.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;

import com.af.admin.beans.OffersDealsBean;
import com.af.admin.beans.RecommendedProductBean;
import com.af.admin.beans.offersBean;
import com.af.admin.entity.AfOfferDealLink;
import com.af.admin.entity.AfPartnerOffer;

public interface OffersDealsService {

	List<OffersDealsBean> getOffersDealsList();
	
	List<OffersDealsBean> getOffersDealsListByPrtNo(String prtNo);
	
	Map<String, String> getSearchByNameList(String prtNo, String searchByName);
	
	List<OffersDealsBean> searchOffesrDeals(OffersDealsBean offersDealsBean);
	
	public String defineOffersDeals(OffersDealsBean offersDealsBean,HttpServletRequest request);
	
	public OffersDealsBean getPartnerOfferByNO(String prtOffrNo);
	
	public String updateOffersDealsDetails(OffersDealsBean offersDealsBean, HttpServletRequest request);
	
	public boolean checkOffersDealSortOrder(String offersDealsSortOrder);
	
	public Map<String, String> getOfferNamesByPrtCatNo(String prtNo, String prtCatNo);
	
	public Map<String, String> getOfferTitle(String prtNo, String catNo);
	
	public String deleteSelectedOffer(Integer integer,HttpServletRequest request);
	
	public Integer getMaxOfferandDealsSlNo();


	public List<AfOfferDealLink> getAllOffersDealsList();

	public String addOfferDeal(String lmdId, String id);

	public String updateOffersDealsLink(JSONObject userJson, HttpServletRequest request);

	public List<AfOfferDealLink> getAllOffersDealsListcheck();

	List<offersBean> getExportOffersDealList();

	public List<AfOfferDealLink> approvedOfferDealLink();

	public List<AfOfferDealLink> pendingOfferDealLink();
 
	public List<AfOfferDealLink> newRecordOfferDealLink();

	public List<OffersDealsBean> getOffersDealsListPagination(int length, int start);

	public Long  getPartnersCommissionPaginationCount();

	public List<String> getStoreList();
	
}
