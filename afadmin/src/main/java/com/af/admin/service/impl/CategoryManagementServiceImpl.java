package com.af.admin.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.af.admin.DAO.CategoryManagementDAO;
import com.af.admin.DAO.CommonDAO;
import com.af.admin.DAO.PartnerManagementDAO;
import com.af.admin.DAO.UserActivityLogDAO;
import com.af.admin.beans.AfAmzonoPartnerCategoryBean;
import com.af.admin.beans.CategoryAttributeJSONBean;
import com.af.admin.beans.CategoryBean;
import com.af.admin.beans.CategoryJSONBean;
import com.af.admin.beans.CategoryStateJSONBean;
import com.af.admin.beans.PartnerCategoryBean;
import com.af.admin.constants.AfConstants;
import com.af.admin.constants.AfHibernateQueries;
import com.af.admin.entity.AfAmazonPartnerCategory;
import com.af.admin.entity.AfCategory;
import com.af.admin.entity.AfPartner;
import com.af.admin.entity.AfPartnerCategory;
import com.af.admin.entity.AfPriceRangeFilter;
import com.af.admin.entity.AfUserActivityLog;
import com.af.admin.service.CategoryManagementService;
import com.af.admin.service.CommonService;
import com.af.admin.utils.AdminUserActivityUtil;
import com.af.admin.utils.CommonUtil;
import com.af.admin.utils.PartnerCategoryComparator;
import com.af.admin.utils.ServiceProviderUtil;

@Component
@Transactional(readOnly = true)
public class CategoryManagementServiceImpl implements CategoryManagementService {

	private Logger logger = LogManager.getLogger(CategoryManagementServiceImpl.class.getName());

	@Autowired
	CategoryManagementDAO categorymanagementDAO;

	@Autowired
	PartnerManagementDAO partnermanagementDAO;

	@Autowired
	CommonDAO commonDAO;

	@Autowired
	CommonService commonService;

	@Autowired
	AdminUserActivityUtil adminUserActivityUtil;

	@Autowired
	UserActivityLogDAO userActLogDao;
	
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");

	@Override
	public List<CategoryBean> getCategoryList() {

		List<AfCategory> afCategoryList = null;
		List<CategoryBean> categoryList = new ArrayList<CategoryBean>();
		String dateStr = "";
		Date dt = null;
		int categoryStatusCode;
		Map<String, String> categories = new HashMap<String, String>();
		try {
			afCategoryList = categorymanagementDAO.getCategoryList();
			for (AfCategory afCategory : afCategoryList) {
				categories.put(afCategory.getCatNo() + "", afCategory.getCategoryName());
			}
			for (AfCategory afCategory : afCategoryList) {
				CategoryBean categoryBean = new CategoryBean();

				categoryBean.setCatNo((afCategory.getCatNo()));

				categoryBean.setStartValue(afCategory.getFilterStartValue());
				categoryBean.setEndValue(afCategory.getFilterEndValue());
				categoryBean.setStepValue(afCategory.getFilterStepValue());

				categoryBean.setCategoryId(afCategory.getCategoryId());
				categoryBean.setCategoryName(afCategory.getCategoryName());
				categoryBean.setPartNO(afCategory.getPrtNo() != null ? afCategory.getPrtNo().toString() : "");
				dt = afCategory.getActivationDate();
				dateStr = ServiceProviderUtil.getInstance().getFormattedDate(afCategory.getActivationDate(),
						"dd/MM/yyyy");
				if (dt != null && !dt.equals(""))
					categoryBean.setStartDate(dateStr);

				dt = afCategory.getDeactivationDate();
				dateStr = ServiceProviderUtil.getInstance().getFormattedDate(afCategory.getDeactivationDate(),
						"dd/MM/yyyy");
				if (dt != null && !dt.equals(""))
					categoryBean.setEndDate(dateStr);

				categoryStatusCode = afCategory.getCategoryStatus();

				if (1 == categoryStatusCode) {
					categoryBean.setCategorySatus(AfConstants.STATUS_ACTIVE);
					categoryBean.setCategoryStatusClass(AfConstants.STATUS_CLASS_GREEN);
					categoryBean.setCategoryStatusCode(1);
				}
				if (0 == categoryStatusCode) {
					categoryBean.setCategorySatus(AfConstants.STATUS_IN_ACTIVE);
					categoryBean.setCategoryStatusClass(AfConstants.STATUS_CLASS_RED);
					categoryBean.setCategoryStatusCode(0);
				}
				if (afCategory.getGoQuicklyCategory() != null) {
					categoryBean
							.setGoQuicklyCategory(Byte.parseByte(String.valueOf(afCategory.getGoQuicklyCategory())));
				}
				if (afCategory.getGqcDisplayOrder() != null) {
					categoryBean.setGqcDisplayOrder(Byte.parseByte(String.valueOf(afCategory.getGqcDisplayOrder())));
				}

				if (afCategory.getCategoryImageUrl() != null) {
					categoryBean.setCategoryImageUrl(afCategory.getCategoryImageUrl());
				}

				if (afCategory.getLinkedCategory() != null) {
					categoryBean.setLinkedCategory(afCategory.getLinkedCategory().replaceAll("-", ","));

					if (!afCategory.getLinkedCategory().isEmpty()) {
						String[] catNos = afCategory.getLinkedCategory().split("-");
						String name = "";
						for (int i = 0; i < catNos.length; i++) {
							name += categories.get(catNos[i] + "") + ",";
						}
						categoryBean.setLinkedCategoryName(name);
					}
				}

				if (afCategory.getBestSellingCategory() != null) {
					categoryBean.setBestSellingCategory(afCategory.getBestSellingCategory());
				}

				categoryList.add(categoryBean);
			}
		} catch (Exception e) {
			logger.error("@@@@ Exception in CategoryManagementServiceImpl at getCategoryList() :", e);
		}
		return categoryList;
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public String createCategory(CategoryBean categoryBean, HttpServletRequest request) {
		AfCategory afCategory = new AfCategory();
		String startdate = "", enddate = "";
		String status;

		Date dt = null;
		Date dt1 = new Date();
		DateFormat sourceDf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");

		String hh_mi_ss = "" + dt1;
		String format[] = hh_mi_ss.split(" ");
		hh_mi_ss = format[3];

		try {
			startdate = categoryBean.getStartDate();
			enddate = categoryBean.getEndDate();
			startdate = startdate + " " + hh_mi_ss;
			if (startdate.length() > 0)
				dt = sourceDf.parse(startdate);
			afCategory.setActivationDate(dt);

			if (enddate.trim().length() > 0) {
				enddate = enddate + " " + hh_mi_ss;
				dt = sourceDf.parse(enddate);
				afCategory.setDeactivationDate(dt);
			} else {
				afCategory.setDeactivationDate(null);
			}
			if ((categoryBean.getCategoryId() != null) && !(categoryBean.getCategoryId().trim().isEmpty())
			) {
				String condition = categoryBean.getCategoryId().trim() + "-%";
				Long rowValue = categorymanagementDAO.getCategoryRowCount(AfHibernateQueries.GET_MAX_SUB_CATEGORY_ID,
						condition);
				if (rowValue == 0) {
					afCategory.setCategoryId(categoryBean.getCategoryId().trim() + "-001");
				} else {
					int newValue = rowValue.intValue() + 1;
					if (newValue < 10) {
						afCategory
								.setCategoryId(categoryBean.getCategoryId().trim() + "-00" + String.valueOf(newValue));
					} else if (newValue < 100) {
						afCategory.setCategoryId(categoryBean.getCategoryId().trim() + "-0" + String.valueOf(newValue));
					} else if (newValue >= 100) {
						afCategory.setCategoryId(categoryBean.getCategoryId().trim() + "-" + String.valueOf(newValue));
					}
				}
				afCategory.setGqcDisplayOrder(categoryBean.getGqcSubDisplayOrder());
			} else {
				Long rowValue = categorymanagementDAO.getCategoryRowCount(AfHibernateQueries.GET_MAX_CATEGORY_ID);
				int newValue = rowValue.intValue() + 1;
				if (newValue < 10) {
					afCategory.setCategoryId("00" + String.valueOf(newValue));
				} else if (newValue < 100) {
					afCategory.setCategoryId("0" + String.valueOf(newValue));
				} else if (newValue >= 100) {
					afCategory.setCategoryId(String.valueOf(newValue));
				}
				afCategory.setGqcDisplayOrder(categoryBean.getGqcDisplayOrder());
			}

			afCategory.setCategoryName(categoryBean.getCategoryName());
			afCategory.setCategoryStatus((byte) categoryBean.getCategoryStatusCode());

			afCategory.setStatus((byte) 1);
			afCategory.setCreatedTime(new Date());
			afCategory.setCreatedBy(1);

			String startValue = categoryBean.getStartValue();
			String endValue = categoryBean.getEndValue();
			String stepValue = categoryBean.getStepValue();

			if ((startValue == null) || (startValue.trim().equalsIgnoreCase(""))) {
				startValue = "0";
			}
			if ((endValue == null) || (endValue.trim().equalsIgnoreCase(""))) {
				endValue = "0";
			}
			if ((stepValue == null) || (stepValue.trim().equalsIgnoreCase(""))) {
				stepValue = "0";
			}
			afCategory.setFilterStartValue(startValue);
			afCategory.setFilterEndValue(endValue);
			afCategory.setFilterStepValue(stepValue);

			afCategory.setGoQuicklyCategory(categoryBean.getGoQuicklyCategory());

			if (null != categoryBean.getBestSellingCategory()) {
				afCategory.setBestSellingCategory(categoryBean.getBestSellingCategory());
			}

			if (null != categoryBean.getCategoryImageUrl()) {
				afCategory.setCategoryImageUrl(categoryBean.getCategoryImageUrl());
			}

			if (null != categoryBean.getLinkedCategory()) {
				afCategory.setLinkedCategory(categoryBean.getLinkedCategory().replaceAll(",", "-"));
			}

			status = categorymanagementDAO.createCategory(afCategory);
			if ("success".equalsIgnoreCase(status)) {
				Long catNo = getLastCategoryCatNo();
				if (!(startValue.trim().equalsIgnoreCase("0")) && !(endValue.trim().equalsIgnoreCase("0"))
						&& !(stepValue.trim().equalsIgnoreCase("0")) && (catNo != null)) {

					createPriceRangeFilters(Integer.parseInt(startValue), Integer.parseInt(endValue),
							Integer.parseInt(stepValue), catNo, "AF_CATEGORY");
				}
			}

			if ("success".equalsIgnoreCase(status)) {
				AfUserActivityLog afUsActLogCat = new AfUserActivityLog();
				String ipAddress = request.getHeader("X-FORWARDED-FOR");
				if (ipAddress == null) {
					ipAddress = request.getRemoteAddr();
				}
				String statusforcat="";
				if(categoryBean.getCategoryStatusCode()==1){
					statusforcat = "Active";
				}else{
					statusforcat = "Inactive";
				}
			try {
				afUsActLogCat.setUserOldFieldValue("NA");
				afUsActLogCat.setUserNewFieldValue("Category name: "+categoryBean.getCategoryName()+" | "+"Start date : "+categoryBean.getStartDate()+" |"
				+ " filter start value: "+categoryBean.getStartValue()+" | "+"filter end value: "+categoryBean.getEndValue()+" | "+" filter step value :"+categoryBean.getStepValue()+" | "
				+"status: "+statusforcat+" | "+"Go Quickly Display Order: "+categoryBean.getGoQuicklyCategory()+" | "+"Linked Category: "+categoryBean.getLinkedCategory().replaceAll(",","-")+" | " 
				+"Category Image URL: "+categoryBean.getCategoryImageUrl()+" | "+" ");
	
				afUsActLogCat.setUserActivity("Created category");
				afUsActLogCat.setUserActCreatedDateTime(new Date());
				afUsActLogCat.setUserActIpVal(ipAddress);
				afUsActLogCat.setUserActModule("Category");
				afUsActLogCat.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
				userActLogDao.createUserActLog(afUsActLogCat);

			}catch (Exception e) {
				e.printStackTrace();
			}

			}
		} catch (Exception e) {
			status = "failure";
			logger.error("@@@@ Exception in CategoryManagementServiceImpl at createCategory()", e);
			return status;
		}
		return status;
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public String updateCategory(JSONObject categoryJson) {
		AfCategory afCategory = new AfCategory();
		String status = "", catNo = "";
		String startdate = "", enddate = "";
		Date dt = null;
		DateFormat sourceDf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		Date dt1 = new Date();
		String hh_mi_ss = "" + dt1;
		String format[] = hh_mi_ss.split(" ");
		hh_mi_ss = format[3];

		try {
			catNo = categoryJson.getString("catNo");
			afCategory = categorymanagementDAO.getCategoryByCategoryNo(catNo);
			startdate = categoryJson.getString("startDate");
			enddate = categoryJson.getString("endDate");
			if (startdate != null && !startdate.trim().isEmpty()) {
				if (startdate.length() > 0) {
					startdate = startdate + " " + hh_mi_ss;
					dt = sourceDf.parse(startdate);

				}

				afCategory.setActivationDate(dt);
			}

			if (enddate != null && !enddate.trim().isEmpty()) {
				if (enddate.length() > 0) {
					hh_mi_ss = format[3];
					enddate = enddate + " " + hh_mi_ss;
					dt = sourceDf.parse(enddate);

				}
				afCategory.setDeactivationDate(dt);
			}
			afCategory.setCategoryId(categoryJson.getString("categoryId"));
			afCategory.setCategoryName(categoryJson.getString("categoryName"));
			afCategory.setCategoryStatus((byte) categoryJson.getInt("categoryStatus"));
			afCategory.setModifiedTime(new Date());
			afCategory.setCreatedBy(1);
			status = categorymanagementDAO.updateCategory(afCategory);
		} catch (Exception e) {
			logger.error("@@@@ Exception in CategoryManagementServiceImpl at updateCategory()", e);

		}
		return status;
	}

	@Override
	public boolean checkcategoryIdExist(String categoryID) {
		boolean categoryIdExist = false;
		try {
			categoryIdExist = categorymanagementDAO.checkcategoryIdExist(categoryID);
		} catch (Exception e) {
			logger.error("@@Exception in CategoryManagementServiceImpl at checkcategoryIdExist() :", e);
		}
		return categoryIdExist;
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public String deleteCategory(Integer catNo) {
		String deleteStatus = "";
		try {
			deleteStatus = categorymanagementDAO.deleteCategory(catNo);
		} catch (Exception e) {
			logger.error("@@@@ Exception in CategoryManagementServiceImpl at deleteCategory() :", e);
		}
		return deleteStatus;
	}

	@Override
	public List<PartnerCategoryBean> getMappingDetails() {
		List<AfPartnerCategory> afCategoryList = null;
		int categoryStatusCode;
		AfPartner afpartner = null;
		try {
			List<PartnerCategoryBean> partnercategoryListBean = new ArrayList<PartnerCategoryBean>();
			afCategoryList = categorymanagementDAO.getmappingDetails();

			if (afCategoryList != null) {
				for (AfPartnerCategory afpartnerCategory : afCategoryList) {

					PartnerCategoryBean partnercategoryBean = new PartnerCategoryBean();
					if (afpartnerCategory.getAfCategory() != null) {
						partnercategoryBean.setCategoryName(afpartnerCategory.getAfCategory().getCategoryName());
						partnercategoryBean.setCatNo(afpartnerCategory.getAfCategory().getCatNo().toString());
					}
					afpartner = afpartnerCategory.getAfPartner();
					if (afpartner != null) {
						partnercategoryBean.setPartNO(afpartnerCategory.getAfPartner().getPrtNo().toString());
						partnercategoryBean.setPartnerName(afpartnerCategory.getAfPartner().getPartnerName());
					}
					partnercategoryBean.setPrtCatNo(afpartnerCategory.getPrtCatNo().toString());
                    partnercategoryBean.setPartnerCategoryId(afpartnerCategory.getPartnerCategoryId());
					partnercategoryBean.setPartnercategoryName(afpartnerCategory.getPartnerCategoryName());
					partnercategoryBean.setFilterStartValue(afpartnerCategory.getFilterStartValue() != null
							? afpartnerCategory.getFilterStartValue() : "");
					partnercategoryBean.setFilterEndValue(
							afpartnerCategory.getFilterEndValue() != null ? afpartnerCategory.getFilterEndValue() : "");
					partnercategoryBean.setFilterStepValue(afpartnerCategory.getFilterStepValue() != null
							? afpartnerCategory.getFilterStepValue() : "");

					categoryStatusCode = afpartnerCategory.getPartnerCategoryStatus();

					if (1 == categoryStatusCode) {
						partnercategoryBean.setCategorySatus(AfConstants.STATUS_ACTIVE);
						partnercategoryBean.setCategoryStatusClass(AfConstants.STATUS_CLASS_GREEN);
						partnercategoryBean.setCategoryStatusCode(categoryStatusCode);

					}
					if (0 == categoryStatusCode) {
						partnercategoryBean.setCategorySatus(AfConstants.STATUS_IN_ACTIVE);
						partnercategoryBean.setCategoryStatusClass(AfConstants.STATUS_CLASS_RED);
						partnercategoryBean.setCategoryStatusCode(categoryStatusCode);
					}

					partnercategoryBean.setDisplayOrder(afpartnerCategory.getPartnerCategoryPosition() != null
							? afpartnerCategory.getPartnerCategoryPosition().toString() : "");
					partnercategoryBean.setCategoryImageUrl(afpartnerCategory.getPartnerCategoryLogoUrl() != null
							? afpartnerCategory.getPartnerCategoryLogoUrl() : "");
					partnercategoryListBean.add(partnercategoryBean);

					PartnerCategoryComparator comparator = new PartnerCategoryComparator();
					Collections.sort(partnercategoryListBean, comparator);

				}
			}
			return partnercategoryListBean;
		} catch (Exception e) {
			logger.error("@@@@ Exception in CategoryManagementServiceImpl at getMappingDetails()  :", e);
			return null;
		}
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public List<PartnerCategoryBean> getMappingDetailsExceptSearch() {
		List<AfPartnerCategory> afCategoryList = null;
		int categoryStatusCode;
		AfPartner afpartner = null;
		try {
			List<PartnerCategoryBean> partnercategoryListBean = new ArrayList<PartnerCategoryBean>();
			afCategoryList = categorymanagementDAO.getmappingDetails();

			if (afCategoryList != null) {
				for (AfPartnerCategory afpartnerCategory : afCategoryList) {
if(afpartnerCategory.getAfPartner()!=null && afpartnerCategory.getAfPartner().getDefaultPartner()==0){


					PartnerCategoryBean partnercategoryBean = new PartnerCategoryBean();
					if (afpartnerCategory.getAfCategory() != null) {
						partnercategoryBean.setCategoryName(afpartnerCategory.getAfCategory().getCategoryName());
						partnercategoryBean.setCatNo(afpartnerCategory.getAfCategory().getCatNo().toString());
					}
					afpartner = afpartnerCategory.getAfPartner();
					if (afpartner != null) {
						partnercategoryBean.setPartNO(afpartnerCategory.getAfPartner().getPrtNo().toString());
						partnercategoryBean.setPartnerName(afpartnerCategory.getAfPartner().getPartnerName());
					}
					partnercategoryBean.setPrtCatNo(afpartnerCategory.getPrtCatNo().toString());
                    partnercategoryBean.setPartnerCategoryId(afpartnerCategory.getPartnerCategoryId());
					partnercategoryBean.setPartnercategoryName(afpartnerCategory.getPartnerCategoryName());
					partnercategoryBean.setFilterStartValue(afpartnerCategory.getFilterStartValue() != null
							? afpartnerCategory.getFilterStartValue() : "");
					partnercategoryBean.setFilterEndValue(
							afpartnerCategory.getFilterEndValue() != null ? afpartnerCategory.getFilterEndValue() : "");
					partnercategoryBean.setFilterStepValue(afpartnerCategory.getFilterStepValue() != null
							? afpartnerCategory.getFilterStepValue() : "");

					categoryStatusCode = afpartnerCategory.getPartnerCategoryStatus();

					if (1 == categoryStatusCode) {
						partnercategoryBean.setCategorySatus(AfConstants.STATUS_ACTIVE);
						partnercategoryBean.setCategoryStatusClass(AfConstants.STATUS_CLASS_GREEN);
						partnercategoryBean.setCategoryStatusCode(categoryStatusCode);

					}
					if (0 == categoryStatusCode) {
						partnercategoryBean.setCategorySatus(AfConstants.STATUS_IN_ACTIVE);
						partnercategoryBean.setCategoryStatusClass(AfConstants.STATUS_CLASS_RED);
						partnercategoryBean.setCategoryStatusCode(categoryStatusCode);
					}

					partnercategoryBean.setDisplayOrder(afpartnerCategory.getPartnerCategoryPosition() != null
							? afpartnerCategory.getPartnerCategoryPosition().toString() : "");
					partnercategoryBean.setCategoryImageUrl(afpartnerCategory.getPartnerCategoryLogoUrl() != null
							? afpartnerCategory.getPartnerCategoryLogoUrl() : "");
					partnercategoryListBean.add(partnercategoryBean);

					PartnerCategoryComparator comparator = new PartnerCategoryComparator();
					Collections.sort(partnercategoryListBean, comparator);
				}
				}
			}
			return partnercategoryListBean;
		} catch (Exception e) {
			logger.error("@@@@ Exception in CategoryManagementServiceImpl at getMappingDetails()  :", e);
			return null;
		}
	}
	
	
	
	
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public String deactivateMappedCategory(Integer partnercategoryNo, HttpServletRequest request) {
		String deleteStatus = "";
		AfPartnerCategory afPartCat = null;
		try {
			deleteStatus = categorymanagementDAO.deactivateMappedCategory(partnercategoryNo);
			afPartCat = categorymanagementDAO.deleteCategoryvalues(partnercategoryNo);
			

			String ipAddress = request.getHeader("X-FORWARDED-FOR");
			if (ipAddress == null) {
				ipAddress = request.getRemoteAddr();
			}

			AfUserActivityLog deleteUsActLog=new AfUserActivityLog();

			try {
				deleteUsActLog.setUserActivity("Delete category");
				deleteUsActLog.setUserNewFieldValue("NA");

				deleteUsActLog.setUserOldFieldValue("Category name: "+afPartCat.getPartnerCategoryName()+" |"
						+ "Partner Name: "+afPartCat.getAfPartner().getPartnerName()+" | "+"Category Id: "+afPartCat.getPartnerCategoryId()+" | " 
						+ "Start date : "+ new Date() +" | " +"status: "+afPartCat.getPartnerCategoryStatus()+" | "+" ");

				deleteUsActLog.setUserActCreatedDateTime(new Date());
				deleteUsActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
				deleteUsActLog.setUserActIpVal(ipAddress);
				deleteUsActLog.setUserActModule("Category");
				userActLogDao.updateUserLog(deleteUsActLog);
			}
			catch (Exception e){
				e.printStackTrace();
			}
		} catch (Exception e) {
			logger.error("@@@@ Exception in CategoryManagementServiceImpl at deactivateMappedCategory() :", e);
		}
		return deleteStatus;
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public String updatePartnerCategory(JSONObject categoryJson, HttpServletRequest request) {
		AfPartnerCategory afPartnerCategory = null;
		AfCategory afCategory = null;
		String status = "";
		AfPartnerCategory afPartnerCategoryOld = null;
		String replaceInOld="";
		String replaceInNew="";

		try {
			afPartnerCategory = categorymanagementDAO
					.getPartnerCategoryByPartnerCategoryNo(categoryJson.getString("partnerCatNo"));
			afPartnerCategoryOld = (AfPartnerCategory) afPartnerCategory.clone();
					
			
			if (categoryJson.getString("catNo") != null || !categoryJson.getString("catNo").isEmpty()) {

				afCategory = categorymanagementDAO.getCategoryByCategoryNo(categoryJson.getString("catNo"));
				logger.info("****************CategoryManagementServiceImpl updatePartnerCategory*******afCategory "
						+ afCategory);
			}
			if (afCategory != null)
				afPartnerCategory.setAfCategory(afCategory);
			afPartnerCategory.setPartnerCategoryName(afCategory.getCategoryName());
			afPartnerCategory.setPartnerCategoryStatus(Byte.parseByte((categoryJson.getString("partCatStatus"))));
			afPartnerCategory.setModifiedTime(new Date());
			afPartnerCategory.setModifiedBy(1);

			status = categorymanagementDAO.updatePartnerCategory(afPartnerCategory);

			if ("success".equalsIgnoreCase(status)) {
				String ipAddress = request.getHeader("X-FORWARDED-FOR");
			if (ipAddress == null) {
				ipAddress = request.getRemoteAddr();
			}
			String oldcatStat="";
			String newcatStat="";
			String oldCatName = "";
			String newCatName = "";
			if(afPartnerCategoryOld.getPartnerCategoryStatus()==1){
				oldcatStat = "Active";
			}else{
				oldcatStat = "Inactive";
			}
			if(afPartnerCategory.getPartnerCategoryStatus()==1){
				newcatStat = "Active";
			}else{
				newcatStat = "Inactive";
			}
			
			if(!afPartnerCategory.getAfCategory().getCategoryName().equals("") && afPartnerCategory.getAfCategory().getCategoryName()!=null){
				newCatName = afPartnerCategory.getAfCategory().getCategoryName();
			}else{
				newCatName = "NA";
			}
			
			if(!afPartnerCategoryOld.getAfCategory().getCategoryName().equals("") && afPartnerCategoryOld.getAfCategory().getCategoryName()!=null){
				oldCatName = afPartnerCategoryOld.getAfCategory().getCategoryName();
			}else{
				oldCatName = "NA";
			}
		
				AfUserActivityLog updateUsActLog = new AfUserActivityLog();
				try {
					updateUsActLog.setUserActivity("Update Mapped partner Category");
					updateUsActLog.setUserActCreatedDateTime(new Date());
					updateUsActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
					updateUsActLog.setUserActIpVal(ipAddress);
					updateUsActLog.setUserActModule("Category");
					
					updateUsActLog.setUserNewFieldValue("Category Name:"+afPartnerCategory.getAfCategory().getCategoryName()+" | Partner category name:"+afPartnerCategory.getPartnerCategoryName()
					+"Partner Category Status:"+newcatStat+" | "+"modifiedTime:"+new Date()+" | ");
					
					updateUsActLog.setUserOldFieldValue("Category Name:"+afPartnerCategoryOld.getAfCategory().getCategoryName()+" Partner category name:"+afPartnerCategoryOld.getPartnerCategoryName()
					+"Partner Category Status:"+oldcatStat);
					userActLogDao.updateUserLog(updateUsActLog);
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}

		} catch (Exception e) {
			logger.error("@@@@ Exception in CategoryManagementServiceImpl at updatePartnerCategory()", e);
		}
		return status;
	}
	
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public String updatePartnerCategoryMapped(JSONObject categoryJson, HttpServletRequest request) {
		AfPartnerCategory afpartcat = null;
		String status = "";
		AfPartnerCategory afCategoryOld = null;
		try {
			afpartcat = categorymanagementDAO.getPartnerCategoryByPartnerCategoryNo(categoryJson.getString("partnercatnumber"));
			afCategoryOld = (AfPartnerCategory) afpartcat.clone();
			afpartcat.setPartnerCategoryName(categoryJson.getString("partnercategoryname"));
			afpartcat.setPartnerCategoryId(categoryJson.getString("partnercategoryid"));
			afpartcat.setPartnerCategoryStatus(Byte.parseByte((categoryJson.getString("partnercatstatus"))));
			afpartcat.setModifiedTime(new Date());
			afpartcat.setModifiedBy(1);
			
			status = categorymanagementDAO.updatePartnerCategory(afpartcat);
			
			if ("success".equalsIgnoreCase(status)) {
				String ipAddress = request.getHeader("X-FORWARDED-FOR");
			if (ipAddress == null) {
				ipAddress = request.getRemoteAddr();
			}
				AfUserActivityLog updateUsActLog = new AfUserActivityLog();
				Map<String,String> objectCom = new LinkedHashMap<>();
				try {
					updateUsActLog.setUserActivity("Update Mapped partner Category");
					updateUsActLog.setUserActCreatedDateTime(new Date());
					updateUsActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
					updateUsActLog.setUserActIpVal(ipAddress);
					updateUsActLog.setUserActModule("Category");
					
					objectCom = adminUserActivityUtil.objectComparision(afpartcat, afCategoryOld);
					updateUsActLog.setUserNewFieldValue(objectCom.get(AfConstants.ACTIVITY_OLD_KEY));
					updateUsActLog.setUserOldFieldValue(objectCom.get(AfConstants.ACTIVITY_NEW_KEY));
					userActLogDao.updateUserLog(updateUsActLog);
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
			
		} catch (Exception e) {
			logger.error("@@@@ Exception in CategoryManagementServiceImpl at updatePartnerCategoryMapped()", e);
		}
		return status;
	}
	
	
	
	
	

	@Override
	public List<PartnerCategoryBean> getPartnerCategories(String partnerNo) {
		Set<AfPartnerCategory> afpartnerCategory = null;
		afpartnerCategory = new HashSet<AfPartnerCategory>();
		AfCategory category = null;
		// AfPartner partner=null;
		// partner=partnermanagementDAO.getPartnerByPartnerNO(partnerNo);
		List<PartnerCategoryBean> partnercategoryBeanlist = null;
		partnercategoryBeanlist = new ArrayList<PartnerCategoryBean>();

		int categoryStatusCode;
		List<AfPartnerCategory> afPartnerCategories = null;
		afPartnerCategories = categorymanagementDAO.getPartnerCategories(Integer.parseInt(partnerNo));
		for (AfPartnerCategory afPartnerCategory2 : afPartnerCategories) {

			/*
			 * } afpartnerCategory=partner.getAfPartnerCategories();
			 * 
			 * 
			 * 
			 * for (Iterator iterator = afpartnerCategory.iterator(); iterator
			 * .hasNext();) { AfPartnerCategory afPartnerCategory2 =
			 * (AfPartnerCategory) iterator.next();
			 */
			PartnerCategoryBean partnercategoryBean = new PartnerCategoryBean();

			partnercategoryBean.setPartNO(afPartnerCategory2.getAfPartner().getPrtNo().toString());
			partnercategoryBean.setPartnerName(afPartnerCategory2.getAfPartner().getPartnerName());
			partnercategoryBean.setPartnerCategoryId(afPartnerCategory2.getPartnerCategoryId());

			if (afPartnerCategory2.getAfCategory() != null) {
				partnercategoryBean.setCategoryName(afPartnerCategory2.getAfCategory().getCategoryName());
				partnercategoryBean.setCatNo(afPartnerCategory2.getAfCategory().getCatNo().toString());
			}
			partnercategoryBean.setPrtCatNo(afPartnerCategory2.getPrtCatNo().toString());
			partnercategoryBean.setPartnercategoryName(afPartnerCategory2.getPartnerCategoryName());
           
			/*
			 * partnercategoryBean.setDisplayOrder(afPartnerCategory2.
			 * getPartnerCategoryPosition()!=null?afPartnerCategory2.
			 * getPartnerCategoryPosition().toString():"");
			 * partnercategoryBean.setFilterStartValue(afPartnerCategory2.
			 * getFilterStartValue()!=null?afPartnerCategory2.
			 * getFilterStartValue():"");
			 * partnercategoryBean.setFilterEndValue(afPartnerCategory2.
			 * getFilterEndValue()!=null?afPartnerCategory2.getFilterEndValue():
			 * ""); partnercategoryBean.setFilterStepValue(afPartnerCategory2.
			 * getFilterStepValue()!=null?afPartnerCategory2.getFilterStepValue(
			 * ):"");
			 */

			categoryStatusCode = afPartnerCategory2.getPartnerCategoryStatus();

			if (1 == categoryStatusCode) {
				partnercategoryBean.setCategorySatus(AfConstants.STATUS_ACTIVE);
				partnercategoryBean.setCategoryStatusClass(AfConstants.STATUS_CLASS_GREEN);
				partnercategoryBean.setCategoryStatusCode(categoryStatusCode);

			}
			if (0 == categoryStatusCode) {
				partnercategoryBean.setCategorySatus(AfConstants.STATUS_IN_ACTIVE);
				partnercategoryBean.setCategoryStatusClass(AfConstants.STATUS_CLASS_RED);
				partnercategoryBean.setCategoryStatusCode(categoryStatusCode);
			}

			partnercategoryBean.setDisplayOrder(afPartnerCategory2.getPartnerCategoryPosition() != null
					? afPartnerCategory2.getPartnerCategoryPosition().toString() : "");
			partnercategoryBean.setCategoryImageUrl(afPartnerCategory2.getPartnerCategoryLogoUrl() != null
					? afPartnerCategory2.getPartnerCategoryLogoUrl() : "");

			partnercategoryBeanlist.add(partnercategoryBean);

			PartnerCategoryComparator comparator = new PartnerCategoryComparator();
			Collections.sort(partnercategoryBeanlist, comparator);
		}

		return partnercategoryBeanlist;
	}

	@Override
	public boolean checkCategoryPosition(String categoryDisplayOrder) {
		boolean categoryDisplayOrderExist = false;
		try {
			categoryDisplayOrderExist = categorymanagementDAO.checkCategoryPosition(categoryDisplayOrder);
		} catch (Exception e) {
			logger.error("@@@@Exception in CategoryManagementServiceImpl at checkCategoryPosition() :", e);
		}
		return categoryDisplayOrderExist;
	}

	public void createPriceRangeFilters(int startValue, int endValue, int stepValue, int prt_cat_no) {
		try {
			List<AfPriceRangeFilter> afRangeFilters = new ArrayList<AfPriceRangeFilter>();
			AfPriceRangeFilter afPriceRangeFilter1 = new AfPriceRangeFilter();
			afPriceRangeFilter1.setPrtCatNo(prt_cat_no);
			afPriceRangeFilter1.setPrfId("PF1");
			afPriceRangeFilter1.setCreatedTime(new Date());
			afPriceRangeFilter1.setFilterStartValue("Below");
			afPriceRangeFilter1.setFilterStepValue(stepValue + "");
			afPriceRangeFilter1.setFilterEndValue(startValue + "");
			afPriceRangeFilter1.setCreatedBy(1);
			afPriceRangeFilter1.setModifiedTime(new Date());
			afPriceRangeFilter1.setModifiedBy(1);
			afPriceRangeFilter1.setStatus((byte) 1);
			afRangeFilters.add(afPriceRangeFilter1);

			int i = 0, j = 2;
			i = startValue;
			while (i < endValue) {
				AfPriceRangeFilter afPriceRangeFilter2 = new AfPriceRangeFilter();
				startValue = i;
				i = startValue + stepValue;

				if (i < endValue) {
					afPriceRangeFilter2.setPrtCatNo(prt_cat_no);
					afPriceRangeFilter2.setPrfId("PF" + j++);
					afPriceRangeFilter2.setFilterStartValue(startValue + "");
					afPriceRangeFilter2.setFilterEndValue(i + "");
					afPriceRangeFilter2.setFilterStepValue(stepValue + "");
					afPriceRangeFilter2.setCreatedTime(new Date());
					afPriceRangeFilter2.setCreatedBy(1);
					afPriceRangeFilter2.setModifiedTime(new Date());
					afPriceRangeFilter2.setModifiedBy(1);
					afPriceRangeFilter2.setStatus((byte) 1);
					afRangeFilters.add(afPriceRangeFilter2);
				}
			}
			AfPriceRangeFilter afPriceRangeFilter3 = new AfPriceRangeFilter();
			afPriceRangeFilter3.setPrtCatNo(prt_cat_no);
			afPriceRangeFilter3.setPrfId("PF" + j);
			afPriceRangeFilter3.setCreatedTime(new Date());
			afPriceRangeFilter3.setFilterStartValue("Above");
			afPriceRangeFilter3.setFilterStepValue(stepValue + "");
			afPriceRangeFilter3.setFilterEndValue(startValue + "");
			afPriceRangeFilter3.setCreatedBy(1);
			afPriceRangeFilter3.setModifiedTime(new Date());
			afPriceRangeFilter3.setModifiedBy(1);
			afPriceRangeFilter3.setStatus((byte) 1);
			afRangeFilters.add(afPriceRangeFilter3);
			// logger.info("CategoryManagementServiceImpl afRangeFilters-size
			// :"+afRangeFilters.size());
			categorymanagementDAO.createPriceRangeFilter(afRangeFilters);
		} catch (Exception e) {
			logger.error("@@@@Exception in CategoryManagementServiceImpl at createPriceRangeFilters() :", e);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public String updateCategory(CategoryBean categoryBean, HttpServletRequest request) {
		AfCategory afCategory = new AfCategory();
		AfCategory newAfCategory = new AfCategory();
		String startdate = "", enddate = "";
		String status = "failure";
		String replaceInOld="";
		String replaceInNew="";
		Date dt = null;
		Date dt1 = new Date();
		DateFormat sourceDf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");

		String hh_mi_ss = "" + dt1;
		String format[] = hh_mi_ss.split(" ");
		hh_mi_ss = format[3];
		AfCategory afCategoryOld = null;
		
		try {
			if ((categoryBean.getCategoryNo() != null) && !(categoryBean.getCategoryNo().trim().equalsIgnoreCase(""))) {

				afCategory = categorymanagementDAO.getCategoryByCategoryNo(categoryBean.getCategoryNo());

				afCategoryOld = (AfCategory) afCategory.clone();

				boolean resultStatus = false;
				String deactivateStatus = "";

				if (afCategory != null) {

					if (afCategory.getCategoryId().trim().length() > 3) {

						newAfCategory = categorymanagementDAO.getCategoryByCategoryID(afCategory.getCategoryId().trim()
								.substring(0, (afCategory.getCategoryId().trim().length() - 4)));

						if (newAfCategory.getCategoryStatus() == 0) {
							if (categoryBean.getCategoryStatusCode() == 1) {
								// do not active the child if parent is
								// inactive.

								return "parentInactive";
							}
						}
					}
					// storing in AF_PRICE_RANGE_FILTER

					String oldStartValue = afCategory.getFilterStartValue();
					String oldEndValue = afCategory.getFilterEndValue();
					String oldStepValue = afCategory.getFilterStepValue();

					String startValue = categoryBean.getStartValue();
					String endValue = categoryBean.getEndValue();
					String stepValue = categoryBean.getStepValue();

					if ((startValue == null) || (startValue.trim().equalsIgnoreCase(""))) {
						startValue = "0";
					}
					if ((endValue == null) || (endValue.trim().equalsIgnoreCase(""))) {
						endValue = "0";
					}
					if ((stepValue == null) || (stepValue.trim().equalsIgnoreCase(""))) {
						stepValue = "0";
					}
					afCategory.setFilterStartValue(startValue);
					afCategory.setFilterEndValue(endValue);
					afCategory.setFilterStepValue(stepValue);

					// parsing date
					startdate = categoryBean.getStartDate();
					enddate = categoryBean.getEndDate();
					startdate = startdate + " " + hh_mi_ss;
					if (startdate.length() > 0)
						dt = sourceDf.parse(startdate);
					afCategory.setActivationDate(dt);

					if (enddate.trim().length() > 0) {
						enddate = enddate + " " + hh_mi_ss;
						dt = sourceDf.parse(enddate);
						afCategory.setDeactivationDate(dt);
					} else {
						afCategory.setDeactivationDate(null);
					}
					afCategory.setCategoryName(categoryBean.getCategoryName());
					afCategory.setCategoryStatus((byte) categoryBean.getCategoryStatusCode());

					afCategory.setCreatedTime(new Date());
					afCategory.setCreatedBy(1);
					afCategory.setStatus((byte) 1);

					afCategory.setGoQuicklyCategory(categoryBean.getGoQuicklyCategory());
					afCategory.setGqcDisplayOrder(categoryBean.getGqcDisplayOrder());

					// TODO: added new fileds
					if (null != categoryBean.getBestSellingCategory()) {
						afCategory.setBestSellingCategory(categoryBean.getBestSellingCategory());
					}

					if (null != categoryBean.getCategoryImageUrl()) {
						afCategory.setCategoryImageUrl(categoryBean.getCategoryImageUrl());
					}

					if (null != categoryBean.getLinkedCategory()) {
						afCategory.setLinkedCategory(categoryBean.getLinkedCategory().replaceAll(",", "-"));
					}

					status = categorymanagementDAO.createCategory(afCategory);

					if ("success".equalsIgnoreCase(status)) {

						if (startValue.equals(oldStartValue) && endValue.equals(oldEndValue)
								&& stepValue.equals(oldStepValue)) {
							// logger.error("@@@@ No update required in price
							// range filter table ....");
						} else {
							resultStatus = categorymanagementDAO.checkInPriceRangeFilter(afCategory.getCatNo(),
									AfHibernateQueries.AF_PRICE_RANGE_FILTER_BY_CAT_NO);
							if (resultStatus) {
								List<AfPriceRangeFilter> prList = categorymanagementDAO.getPriceRangeFilterList(
										afCategory.getCatNo(), AfHibernateQueries.GET_AF_PRICE_RANGE_FILTER_BY_CAT_NO);

								for (AfPriceRangeFilter afFilter : prList) {
									afFilter.setStatus((byte) 0);
									deactivateStatus = categorymanagementDAO.deactivatePriceRangeFilter(afFilter);
								}
								createPriceRangeFilters(Integer.parseInt(startValue), Integer.parseInt(endValue),
										Integer.parseInt(stepValue), afCategory.getCatNo().longValue(), "AF_CATEGORY");
							} else {
								createPriceRangeFilters(Integer.parseInt(startValue), Integer.parseInt(endValue),
										Integer.parseInt(stepValue), afCategory.getCatNo().longValue(), "AF_CATEGORY");
							}
						}

						if (categoryBean.getCategoryStatusCode() == 0) {
							List<AfCategory> afCategoryList = new ArrayList<AfCategory>();

							// get child details
							afCategoryList = categorymanagementDAO.getCategoryList(
									AfHibernateQueries.GET_CATEGORY_BY_CAT_ID, (afCategory.getCategoryId() + "-%"));

							for (AfCategory subAfCategory : afCategoryList) {
								subAfCategory.setCategoryStatus((byte) 0);
								status = categorymanagementDAO.createCategory(subAfCategory);
							}
						}
					}
					String statusInactive=	afCategoryOld.getCategoryStatus()==0?"InActive":"Active";
			     	String statusActive=   afCategory.getCategoryStatus()==0?"InActive":"Active";
					//Starts Update Category activity log
					if ("success".equalsIgnoreCase(status)) {
						String ipAddress = request.getHeader("X-FORWARDED-FOR");
					if (ipAddress == null) {
						ipAddress = request.getRemoteAddr();
					}
					String oldval="";
					String newVal="";
						AfUserActivityLog updateUsActLog = new AfUserActivityLog();
						Map<String,String> objectCom = new LinkedHashMap<>();
						try {
							updateUsActLog.setUserActivity("Update Category ");
							updateUsActLog.setUserActCreatedDateTime(new Date());
							updateUsActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
							updateUsActLog.setUserActIpVal(ipAddress);
							updateUsActLog.setUserActModule("Category ");
							/*objectCom = adminUserActivityUtil.objectComparision(afCategory, afCategoryOld);
							
							oldval =  objectCom.get(AfConstants.ACTIVITY_OLD_KEY).toString().replace("categoryStatus :1","categoryStatus :Active ").replace("categoryStatus :0", "categoryStatus :Inactive");
							newVal = objectCom.get(AfConstants.ACTIVITY_NEW_KEY).toString().replace("categoryStatus :1","categoryStatus :Active ").replace("categoryStatus :0", "categoryStatus :Inactive");;
							*/
							if(afCategory.getCategoryName()!=afCategoryOld.getCategoryName()){
								replaceInOld=replaceInOld+"|Category Name:"+afCategoryOld.getCategoryName();
							    replaceInNew=replaceInNew+"|Category Name :"+afCategory.getCategoryName();
							}
							/*if(!afCategory.d.equalsIgnoreCase(afCategoryOld.getBannerId())){
								replaceInOld=replaceInOld+"|Start Date:"+simpleDateFormat.format(afCategoryOld.getBannerId());
								replaceInNew=replaceInNew+"|Start Date:"+simpleDateFormat.format(afBanner.getBannerId());
							}
							if(!categoryBean.getStartDate().equalsIgnoreCase(categoryBean.getStartDate())){
								replaceInOld=replaceInOld+"|End Date:"+afCategoryOld.getBannerName();
								replaceInNew=replaceInNew+"|End Date:"+afBanner.getBannerName();
							}*/
							if(!afCategory.getFilterStartValue().equalsIgnoreCase(afCategoryOld.getFilterStartValue())){
								replaceInOld=replaceInOld+"|Filter Start Value:"+afCategoryOld.getFilterStartValue();
							     replaceInNew=replaceInNew+"|Filter Start Value:"+afCategory.getFilterStartValue();
							}
							if(!afCategory.getFilterEndValue().equalsIgnoreCase(afCategoryOld.getFilterEndValue())){
								replaceInOld=replaceInOld+"|Filter End Value:"+afCategoryOld.getFilterEndValue();
								replaceInNew=replaceInNew+"|Filter End Value:"+afCategory.getFilterEndValue();
							}
							if(afCategory.getCategoryStatus()!=afCategoryOld.getCategoryStatus()){
								replaceInOld=replaceInOld+"|Status:"+statusInactive;
								replaceInNew=replaceInNew+"|Status:"+statusActive;
							}
							if(!afCategory.getFilterStepValue().equalsIgnoreCase(afCategoryOld.getFilterStepValue())){
								replaceInOld=replaceInOld+"|Filter Step Value:"+afCategoryOld.getFilterStepValue();
								replaceInNew=replaceInNew+"|Filter Step Value:"+afCategory.getFilterStepValue();
							}
							
							
							if(afCategory.getGqcDisplayOrder()!=(afCategoryOld.getGqcDisplayOrder())){
								replaceInOld=replaceInOld+"|Go Quickly Display Order:"+afCategoryOld.getGqcDisplayOrder();
								replaceInNew=replaceInNew+"|Go Quickly Display Order:"+afCategory.getGqcDisplayOrder();
							}
							if(!afCategory.getLinkedCategory().equalsIgnoreCase(afCategoryOld.getLinkedCategory())){
								replaceInOld=replaceInOld+"|Linked Category:"+afCategoryOld.getLinkedCategory();
								replaceInNew=replaceInNew+"|Linked Category:"+afCategory.getLinkedCategory();
							}
							if(!afCategory.getCategoryImageUrl().equalsIgnoreCase(afCategoryOld.getCategoryImageUrl())){
								replaceInOld=replaceInOld+"|Category Image URL:"+afCategoryOld.getCategoryImageUrl();
								replaceInNew=replaceInNew+"|Category Image URL:"+afCategory.getCategoryImageUrl();
							}
							if(replaceInNew.isEmpty() && replaceInOld.isEmpty()) {
		                        replaceInOld=replaceInOld+"|ModifiedTime:"+simpleDateFormat.format(afCategoryOld.getModifiedTime());
		                        replaceInNew=replaceInNew+"|ModifiedTime:"+simpleDateFormat.format(afCategory.getModifiedTime());
		                    }
							
							updateUsActLog.setUserNewFieldValue(replaceInNew);
							updateUsActLog.setUserOldFieldValue(replaceInOld);
							userActLogDao.updateUserLog(updateUsActLog);
						}
						catch(Exception e){
							e.printStackTrace();
						}
					}//end Update Category activity log
				}
			}
		} catch (Exception e) {
			//e.printStackTrace();
			status = "failure";
			logger.error("@@@@Exception in CategoryManagementServiceImpl at updateCategory() :", e);
			return status;
		}
		return status;
	}

	/*
	 * @Override
	 * 
	 * @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	 * public List<CategoryJSONBean> createCategoryTree(List<CategoryBean>
	 * parentCatList, Map<String, List<CategoryBean>> childCatMap) {
	 * 
	 * List<CategoryJSONBean> parentCategoryList = new
	 * ArrayList<CategoryJSONBean>(); try { if(parentCatList.size() > 0) {
	 * boolean isStatePrintedYes = false; for (CategoryBean parentCategoryBean :
	 * parentCatList) { CategoryJSONBean categoryJSONBean = new
	 * CategoryJSONBean();
	 * categoryJSONBean.setText(parentCategoryBean.getCategoryName());
	 * categoryJSONBean.setId(String.valueOf(parentCategoryBean.getCategoryId()
	 * + "/" + parentCategoryBean.getCatNo() + "/0/-1"));
	 * 
	 * CategoryAttributeJSONBean attributeJSONBean = new
	 * CategoryAttributeJSONBean();
	 * attributeJSONBean.setStartDate(parentCategoryBean.getStartDate());
	 * attributeJSONBean.setEndDate(parentCategoryBean.getEndDate());
	 * attributeJSONBean.setStatus(parentCategoryBean.getCategoryStatusCode());
	 * attributeJSONBean.setCategoryId(parentCategoryBean.getCategoryId());
	 * attributeJSONBean.setCatNo(String.valueOf(parentCategoryBean.getCatNo()))
	 * ;
	 * 
	 * attributeJSONBean.setStartValue(parentCategoryBean.getStartValue());
	 * attributeJSONBean.setEndValue(parentCategoryBean.getEndValue());
	 * attributeJSONBean.setStepValue(parentCategoryBean.getStepValue());
	 * 
	 * categoryJSONBean.setA_attr(attributeJSONBean);
	 * 
	 * if(!isStatePrintedYes) { isStatePrintedYes = true; CategoryStateJSONBean
	 * categoryStateJSONBean = new CategoryStateJSONBean();
	 * categoryStateJSONBean.setSelected(true);
	 * 
	 * categoryJSONBean.setState(categoryStateJSONBean); }
	 * 
	 * if(childCatMap.containsKey(String.valueOf(parentCategoryBean.getCatNo()))
	 * ) { List<CategoryBean> childCategoryList = childCatMap.get(
	 * String.valueOf(parentCategoryBean.getCatNo())); new
	 * CommonUtil().formChildTree(categoryJSONBean, childCategoryList,
	 * parentCategoryBean.getCatNo()); // new
	 * CommonUtil().formChildTree(categoryJSONBean, childCategoryList, 0); }
	 * parentCategoryList.add(categoryJSONBean); } } } catch (Exception e) {
	 * e.printStackTrace(); } return parentCategoryList; }
	 */

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public String deleteCategory(String catNo, RedirectAttributes redirectAttrs, HttpServletRequest request) {
		AfCategory afCategory = null;
		String status = "failure";
		boolean isCatNoExistYes = false;
		List<AfCategory> categoryList = new ArrayList<AfCategory>();
		// List<String> queryStringList = new ArrayList<String>();

		try {
			// queryStringList.add(AfHibernateQueries.GET_CAT_NO_COUNT_FROM_AF_PARTNER_OFFER);
			// queryStringList.add(AfHibernateQueries.GET_CAT_NO_COUNT_FROM_AF_PARTNER_KEYWORD);
			// queryStringList.add(AfHibernateQueries.GET_CAT_NO_COUNT_FROM_AF_RECOMMENDED_CATEGORY);
			// queryStringList.add(AfHibernateQueries.GET_CAT_NO_COUNT_FROM_AF_PARTNER_CATEGORY);

			String isCatMappedOrNot = checkCategoryIsMappedOrNot(AfHibernateQueries.IS_CAT_NO_MAPPED_TO_OTHER_TABLES,
					catNo, isCatNoExistYes, redirectAttrs, "it is mapped ");

			switch (isCatMappedOrNot) {
			case "YES":
				isCatNoExistYes = true;
				break;
			case "NO":
				if (!isCatNoExistYes) {
					afCategory = categorymanagementDAO.getCategoryByCategoryNo(catNo);

					//Start Delete activity log for Category.

					String ipAddress = request.getHeader("X-FORWARDED-FOR");
					if (ipAddress == null) {
						ipAddress = request.getRemoteAddr();
					}

					AfUserActivityLog deleteUsActLog=new AfUserActivityLog();

					try {
						deleteUsActLog.setUserActivity("Delete category");
						deleteUsActLog.setUserNewFieldValue("NA");

						deleteUsActLog.setUserOldFieldValue("Category name: "+afCategory.getCategoryName()+" | "+"Start date : "+afCategory.getActivationDate()+" |"
								+ " filter start value: "+afCategory.getFilterStartValue()+" | "+"filter end value: "+afCategory.getFilterEndValue()+" | "+" filter step value :"+afCategory.getFilterStepValue()+" | "
				         +"status: "+afCategory.getCategoryStatus()+" | "+"Go Quickly Display Order: "+afCategory.getGoQuicklyCategory()+" | "+"Linked Category: "+afCategory.getLinkedCategory().replaceAll(",","-")+" | " 
				         +"Category Image URL: "+afCategory.getCategoryImageUrl()+" | "+" ");

						deleteUsActLog.setUserActCreatedDateTime(new Date());
						deleteUsActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
						deleteUsActLog.setUserActIpVal(ipAddress);
						deleteUsActLog.setUserActModule("Category");
						userActLogDao.updateUserLog(deleteUsActLog);
					}
					catch (Exception e){
						e.printStackTrace();
					}

					categoryList = categorymanagementDAO.getCategoryList(
							AfHibernateQueries.GET_ALL_SUB_CATEGORIES_BY_CAT_ID,
							afCategory.getCategoryId().trim() + "-%");

					if (categoryList.size() > 0) {
						for (AfCategory uniqueAfCategory : categoryList) {

							String isChildCatMappedOrNot = checkCategoryIsMappedOrNot(
									AfHibernateQueries.IS_CAT_NO_MAPPED_TO_OTHER_TABLES,
									String.valueOf(uniqueAfCategory.getCatNo()), isCatNoExistYes, redirectAttrs,
									"it's child " + uniqueAfCategory.getCategoryName() + " is mapped ");

							if (isChildCatMappedOrNot.equalsIgnoreCase("YES")) {
								isCatNoExistYes = true;
								break;
							}
						}
					}
				}
				break;
			case "":
				return "failure";
			default:
				break;
			}

			if (!(isCatNoExistYes) && (afCategory != null)) {
				afCategory.setCategoryId(afCategory.getCategoryId());
				afCategory.setStatus((byte) 0);
				status = categorymanagementDAO.createCategory(afCategory);
				if ("success".equalsIgnoreCase(status)) {
					List<AfCategory> afCategoryList = new ArrayList<AfCategory>();
					List<AfCategory> parentAfCategoryList = new ArrayList<AfCategory>();
					parentAfCategoryList = categorymanagementDAO
							.getCategoryList(AfHibernateQueries.GET_CATEGORY_BY_CAT_ID, afCategory.getCategoryId());
					afCategoryList = categorymanagementDAO.getCategoryList(AfHibernateQueries.GET_CATEGORY_BY_CAT_ID,
							(afCategory.getCategoryId() + "-%"));

					if (parentAfCategoryList.size() > 0)
						afCategoryList.addAll(parentAfCategoryList);

					deleteSubCategory(afCategoryList, status, false);
				}
			} else {
				status = "CatNoExist";
			}
		} catch (Exception e) {
			status = "failure";
			logger.error("@@@@Exception in CategoryManagementServiceImpl at deleteCategory()", e);
			return status;
		}
		return status;
	}

	public void createPriceRangeFilters(int startValue, int endValue, int stepValue, Long uniqueNo, String whichTable) {
		try {
			if ((startValue != 0) && (endValue != 0) && (stepValue != 0)) {
				List<AfPriceRangeFilter> afRangeFilters = new ArrayList<AfPriceRangeFilter>();
				AfPriceRangeFilter startPriceRangeFilter = new AfPriceRangeFilter();
				if ("AF_CATEGORY".equalsIgnoreCase(whichTable)) {
					startPriceRangeFilter.setCatNo((int) (long) uniqueNo);
				}
				startPriceRangeFilter.setPrtCatNo(0);
				startPriceRangeFilter.setPrfId("PF1");
				startPriceRangeFilter.setCreatedTime(new Date());
				startPriceRangeFilter.setFilterStartValue("Below");
				startPriceRangeFilter.setFilterStepValue(stepValue + "");
				startPriceRangeFilter.setFilterEndValue(startValue + "");
				startPriceRangeFilter.setCreatedBy(1);
				startPriceRangeFilter.setModifiedTime(new Date());
				startPriceRangeFilter.setModifiedBy(1);
				startPriceRangeFilter.setStatus((byte) 1);
				afRangeFilters.add(startPriceRangeFilter);

				int i = 0, j = 2;
				i = startValue;
				while (i < endValue) {
					AfPriceRangeFilter afPriceRangeFilter = new AfPriceRangeFilter();
					startValue = i;
					i = startValue + stepValue;

					if (i < endValue) {
						if ("AF_CATEGORY".equalsIgnoreCase(whichTable)) {
							afPriceRangeFilter.setCatNo((int) (long) uniqueNo);
						}
						afPriceRangeFilter.setPrtCatNo(0);
						afPriceRangeFilter.setPrfId("PF" + j++);
						afPriceRangeFilter.setFilterStartValue(startValue + "");
						afPriceRangeFilter.setFilterEndValue(i + "");
						afPriceRangeFilter.setFilterStepValue(stepValue + "");
						afPriceRangeFilter.setCreatedTime(new Date());
						afPriceRangeFilter.setCreatedBy(1);
						afPriceRangeFilter.setModifiedTime(new Date());
						afPriceRangeFilter.setModifiedBy(1);
						afPriceRangeFilter.setStatus((byte) 1);
						afRangeFilters.add(afPriceRangeFilter);
					}
				}
				AfPriceRangeFilter endPriceRangeFilter = new AfPriceRangeFilter();
				if ("AF_CATEGORY".equalsIgnoreCase(whichTable)) {
					endPriceRangeFilter.setCatNo((int) (long) uniqueNo);
				}
				endPriceRangeFilter.setPrtCatNo(0);
				endPriceRangeFilter.setPrfId("PF" + j);
				endPriceRangeFilter.setCreatedTime(new Date());
				endPriceRangeFilter.setFilterStartValue("Above");
				endPriceRangeFilter.setFilterStepValue(stepValue + "");
				endPriceRangeFilter.setFilterEndValue(startValue + "");
				endPriceRangeFilter.setCreatedBy(1);
				endPriceRangeFilter.setModifiedTime(new Date());
				endPriceRangeFilter.setModifiedBy(1);
				endPriceRangeFilter.setStatus((byte) 1);
				afRangeFilters.add(endPriceRangeFilter);

				categorymanagementDAO.createPriceRangeFilter(afRangeFilters);
			}
		} catch (Exception e) {
			logger.error("@@@@Exception in CategoryManagementServiceImpl at createPriceRangeFilters() :", e);
		}
	}

	public List<CategoryJSONBean> getCategoryTree(List<CategoryBean> categoryList, int catIdLength,
			boolean isStateSelectedYes, String catNo, String actionType) {

		List<CategoryJSONBean> parentCategoryList = new ArrayList<CategoryJSONBean>();

		CategoryBean categoryBean = new CategoryBean();
		List<Integer> childCatNos = new ArrayList<Integer>();
		for (int i = 0; i < categoryList.size(); i++) {
			categoryBean = categoryList.get(i);
			CategoryJSONBean categoryJSONBean = new CategoryJSONBean();
			if (categoryBean.getCategoryId().trim().length() == catIdLength) {
				categoryJSONBean.setText(categoryBean.getCategoryName());
				categoryJSONBean.setId(String.valueOf(categoryBean.getCategoryId()));

				CategoryAttributeJSONBean attributeJSONBean = new CategoryAttributeJSONBean();
				attributeJSONBean.setStartDate(categoryBean.getStartDate());
				attributeJSONBean.setEndDate(categoryBean.getEndDate());
				attributeJSONBean.setStatus(categoryBean.getCategoryStatusCode());
				attributeJSONBean.setCategoryId(categoryBean.getCategoryId());
				attributeJSONBean.setCatNo(String.valueOf(categoryBean.getCatNo()));

				attributeJSONBean.setStartValue(categoryBean.getStartValue());
				attributeJSONBean.setEndValue(categoryBean.getEndValue());
				attributeJSONBean.setStepValue(categoryBean.getStepValue());

				if (categoryBean.getGoQuicklyCategory() != null) {
					attributeJSONBean.setGoQuicklyCategory(String.valueOf(categoryBean.getGoQuicklyCategory()));
				} else {
					attributeJSONBean.setGoQuicklyCategory("");
				}

				if (categoryBean.getGqcDisplayOrder() != null) {
					attributeJSONBean.setGqcDisplayOrder(String.valueOf(categoryBean.getGqcDisplayOrder()));
				} else {
					attributeJSONBean.setGqcDisplayOrder("");
				}

				if (categoryBean.getCategoryImageUrl() != null) {
					attributeJSONBean.setCategoryImageUrl(String.valueOf(categoryBean.getCategoryImageUrl()));
				} else {
					attributeJSONBean.setCategoryImageUrl("");
				}

				if (categoryBean.getBestSellingCategory() != null) {
					attributeJSONBean.setBestSellingCategory(String.valueOf(categoryBean.getBestSellingCategory()));
				} else {
					attributeJSONBean.setBestSellingCategory("0");
				}

				if (categoryBean.getLinkedCategory() != null) {
					attributeJSONBean.setLinkedCategory(categoryBean.getLinkedCategory());
				} else {
					attributeJSONBean.setLinkedCategory("");
				}

				if (categoryBean.getLinkedCategoryName() != null) {
					attributeJSONBean.setLinkedCategoryName(categoryBean.getLinkedCategoryName());
				} else {
					attributeJSONBean.setLinkedCategoryName("");
				}

				categoryJSONBean.setA_attr(attributeJSONBean);

				if ((actionType.equalsIgnoreCase("")) && (isStateSelectedYes)) {
					if (isStateSelectedYes) {
						isStateSelectedYes = false;
						CategoryStateJSONBean categoryStateJSONBean = new CategoryStateJSONBean();
						categoryStateJSONBean.setSelected(true);

						categoryJSONBean.setState(categoryStateJSONBean);
					}
				} else {
					if (!"".equalsIgnoreCase(catNo)) {
						if (catNo.equalsIgnoreCase(String.valueOf(categoryBean.getCatNo()))) {
							CategoryStateJSONBean categoryStateJSONBean = new CategoryStateJSONBean();
							categoryStateJSONBean.setSelected(true);

							categoryJSONBean.setState(categoryStateJSONBean);
						}
					}
				}
				new CommonUtil().hasChild(categoryJSONBean, categoryList, (i + 1), categoryBean.getCategoryId(),
						(3 * 3 + 2), 3, childCatNos, actionType, catNo);

				parentCategoryList.add(categoryJSONBean);
			}
		}
		return parentCategoryList;
	}

	@Override
	public boolean checkcategoryNameExist(String categoryName) {
		boolean isCategoryNameExist = false;
		try {
			isCategoryNameExist = categorymanagementDAO.checkcategoryNameExist(categoryName);
			logger.debug(">>>>>>>>checkcategoryNameExist service>>>" + isCategoryNameExist);
		} catch (Exception e) {
			logger.error("@@@@Exception in CategoryManagementServiceImpl at checkcategoryNameExist() :", e);
		}
		return isCategoryNameExist;
	}

	
	
	@Override
	public boolean checkpartnerCategoryNameExist(String partcategoryName,Integer prtNo) {
		boolean isPartnerCategoryNameExist = false;
		try {
			isPartnerCategoryNameExist = categorymanagementDAO.checkpartnercategoryNameExist(partcategoryName,prtNo);
			logger.debug(">>>>>>>>checkcategoryNameExist service>>>" + isPartnerCategoryNameExist);
		} catch (Exception e) {
			logger.error("@@@@Exception in CategoryManagementServiceImpl at checkpartnerCategoryNameExist() :", e);
		}
		return isPartnerCategoryNameExist;
	}

	
	@Override
	public boolean checkpartnerCategoryIdExist(String partcategoryId) {
		
		boolean isPartnerCategoryIdExist = false;
		try {
			isPartnerCategoryIdExist = categorymanagementDAO.checkpartnercategoryIdExist(partcategoryId);
			logger.debug(">>>>>>>>checkcategoryNameExist service>>>" + isPartnerCategoryIdExist);
		} catch (Exception e) {
			logger.error("@@@@Exception in CategoryManagementServiceImpl at checkpartnerCategoryNameExist() :", e);
		}
		return isPartnerCategoryIdExist;
	}
	
	
	
	
	
	
	private void deleteSubCategory(List<AfCategory> afCategoryList, String status, boolean resultStatus) {

		try {
			if (afCategoryList.size() > 0) {
				for (AfCategory subAfCategory : afCategoryList) {
					subAfCategory.setStatus((byte) 0);
					subAfCategory.setCategoryId(subAfCategory.getCategoryId());
					status = categorymanagementDAO.createCategory(subAfCategory);

					resultStatus = false;
					resultStatus = categorymanagementDAO.checkInPriceRangeFilter(subAfCategory.getCatNo(),
							AfHibernateQueries.AF_PRICE_RANGE_FILTER_BY_CAT_NO);
					if (resultStatus) {
						List<AfPriceRangeFilter> prList = categorymanagementDAO.getPriceRangeFilterList(
								subAfCategory.getCatNo(), AfHibernateQueries.GET_AF_PRICE_RANGE_FILTER_BY_CAT_NO);

						for (AfPriceRangeFilter afFilter : prList) {
							afFilter.setStatus((byte) 0);
							categorymanagementDAO.deactivatePriceRangeFilter(afFilter);
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error("@@@@Exception in CategoryManagementServiceImpl at deleteSubCategory() :", e);
		}
	}

	@Override
	public Long getLastCategoryCatNo() {
		Long catNo = null;
		try {
			catNo = categorymanagementDAO.getLastCategoryCatNo(AfHibernateQueries.AF_CATEGORY_MAX_CAT_NO);
		} catch (Exception e) {
			logger.error("@@@@Exception in CategoryManagementServiceImpl at getLastCategoryCatNo() :", e);
		}
		return catNo;
	}

	@Override
	public boolean checkCategoryDispOrd(String displayOrder) {
		boolean checkFlag = false;

		try {
			checkFlag = categorymanagementDAO.checkCategoryDispOrd(displayOrder);
		} catch (Exception e) {
			logger.error("@@@@Exception in CategoryManagementServiceImpl at checkCategoryDispOrd() :", e);
		}

		return checkFlag;
	}
	
	private String checkCategoryIsMappedOrNot(String queryString, String catNo, boolean isCatNoExistYes,
			RedirectAttributes redirectAttrs, String msg) {
		String isCategoryMappedOrNot = "";
		try {
			isCatNoExistYes = categorymanagementDAO.checkCategoryNoExistInOtherTable(queryString, catNo);
			if (isCatNoExistYes) {
				isCategoryMappedOrNot = "YES";
				redirectAttrs.addFlashAttribute("errormessage", "The category cannot be deleted, as " + msg);
			}
			if (!isCatNoExistYes) {
				isCategoryMappedOrNot = "NO";
			}
		} catch (Exception e) {
			logger.error("@@@@Exception in CategoryManagementServiceImpl at checkCategoryIsMappedOrNot() :", e);
		}
		return isCategoryMappedOrNot;
	}

	@Override
	public AfCategory getCategoryByCategoryName(String categoryName) {
		AfCategory afCategory = new AfCategory();
		try {
			afCategory = categorymanagementDAO.getCategoryByCategoryName(categoryName);
		} catch (Exception e) {
			logger.error("@@@@Exception in CategoryManagementServiceImpl at getCategoryByName() :", e);
		}
		return afCategory;
	}

	@Override
	public String getBestSellingCategories() {
		List<String> bestSellingCategories = null;
		String bestSellingCat = "";
		try {
			bestSellingCategories = categorymanagementDAO.getBestSellingCategories();
			int i = 1;
			if (!bestSellingCategories.isEmpty())
				for (String catName : bestSellingCategories) {
					if (i == bestSellingCategories.size())
						bestSellingCat += catName;
					else
						bestSellingCat += catName + ",";
					i++;
				}
		} catch (Exception e) {
			logger.error("@@@@Exception in CategoryManagementServiceImpl at getBestSellingCategories() :", e);
		}
		return bestSellingCat;
	}

	@Override
	public Map<String, AfCategory> getCategoryMap() {

		List<AfCategory> afCategoryList = null;
		Map<String, AfCategory> categoryMap = new HashMap<String, AfCategory>();
		try {
			afCategoryList = categorymanagementDAO.getCategoryList();
			for (AfCategory afCategory : afCategoryList) {
				categoryMap.put(afCategory.getCategoryId().trim(), afCategory);
			}
		} catch (Exception e) {
			logger.error("@@@@ Exception in CategoryManagementServiceImpl at getCategoryMap() :", e);
		}
		return categoryMap;
	}

	@Override
	public String getJpplCatByPrtCategory(String prtCategory,Integer prtNo) {
		String category ="NA";
		try {
			category = categorymanagementDAO.getPrtCatByCatName(prtCategory,prtNo);
			
		} catch (Exception e) {
			logger.error("@@@@ Exception in CategoryManagementServiceImpl at getJpplCatByPrtCategory() :", e);
		}
		return category;
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public String createPartnerCategory(JSONObject userMapJson, HttpServletRequest request) {
		AfPartnerCategory  afPartCat= new AfPartnerCategory();
		String status = "failure"; 
		try {
			String partNo = userMapJson.getString("partNumber");
			String createCategoryName = userMapJson.getString("createCategoryName");
			String createCategoryId = userMapJson.getString("createCategoryId");
			String catStat = "";
			
			AfPartner afpartner = partnermanagementDAO.getPartnerByPartnerNO(partNo);
			if(afpartner!=null){
			afPartCat.setAfPartner(afpartner);
			}
			afPartCat.setPartnerCategoryName(createCategoryName);
			afPartCat.setPartnerCategoryId(createCategoryId);
			afPartCat.setPartnerCategoryStatus(Byte.parseByte((userMapJson.getString("createpartcatstatus"))));
			afPartCat.setStatus(new Byte (userMapJson.getString("mainStatus")));
			status=categorymanagementDAO.createPartnercategories(afPartCat);
			if(afPartCat.getPartnerCategoryStatus()==1){
				catStat = "Active";
			}else{
				catStat = "InActive";
			}
			if ("success".equalsIgnoreCase(status)) {
				AfUserActivityLog afUsActLogCat = new AfUserActivityLog();
				String ipAddress = request.getHeader("X-FORWARDED-FOR");
				if (ipAddress == null) {
					ipAddress = request.getRemoteAddr();
				}
			try {
				afUsActLogCat.setUserOldFieldValue("NA");
				afUsActLogCat.setUserNewFieldValue("Category name: "+afPartCat.getPartnerCategoryName()+" |"
				+ "Partner Name: "+afPartCat.getAfPartner().getPartnerName()+" | "+"Category Id: "+afPartCat.getPartnerCategoryId()+" | " 
				+ "Start date : "+ new Date() +" | " +"status: "+catStat+" | "+" ");
	
				afUsActLogCat.setUserActivity("Created category");
				afUsActLogCat.setUserActCreatedDateTime(new Date());
				afUsActLogCat.setUserActIpVal(ipAddress);
				afUsActLogCat.setUserActModule("Category");
				afUsActLogCat.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
				userActLogDao.createUserActLog(afUsActLogCat);

			}catch (Exception e) {
				e.printStackTrace();
			}
			}
			
		} catch (Exception e) {
		logger.error("@@@@ Exception in CategoryManagementServiceImpl at createPartnerCategory() :", e);
		}		
		return status;
	}

	@Override
	public List<AfAmzonoPartnerCategoryBean> getAmazonMappingDetails() {
		 
		
		List<AfAmzonoPartnerCategoryBean> afAmzonoPartnerCategoryBean=new ArrayList<>();
		try{
			List<AfAmazonPartnerCategory> afCategoryList = categorymanagementDAO.getAmazonmappingDetails();
			for(AfAmazonPartnerCategory afAmazonPartnerCategory: afCategoryList){
				AfAmzonoPartnerCategoryBean afAmzonPartnerCategoryBean = new AfAmzonoPartnerCategoryBean();
				afAmzonPartnerCategoryBean.setAmzCatNo(afAmazonPartnerCategory.getAmzCatNo());
				afAmzonPartnerCategoryBean.setAmzCategoryId(afAmazonPartnerCategory.getAmzCategoryId());
				afAmzonPartnerCategoryBean.setPrtName(afAmazonPartnerCategory.getAfPartner().getPartnerName());
				afAmzonPartnerCategoryBean.setPrtNo(afAmazonPartnerCategory.getAfPartner().getPrtNo());
				afAmzonPartnerCategoryBean.setPrtCatNo(afAmazonPartnerCategory.getAfPartnerCategory().getPrtCatNo());
				afAmzonPartnerCategoryBean.setPrtCatName(afAmazonPartnerCategory.getAfPartnerCategory().getPartnerCategoryName());
				afAmzonPartnerCategoryBean.setAmzOrderCategoryName(afAmazonPartnerCategory.getAmzCategoryName());
				int amazonStatus = afAmazonPartnerCategory.getAmazonCategoryStatus();
				
				if (1 == amazonStatus) {
					afAmzonPartnerCategoryBean.setAmazonCategoryStatus(AfConstants.STATUS_ACTIVE);
					afAmzonPartnerCategoryBean.setAmazonCategoryStatusClass(AfConstants.STATUS_CLASS_GREEN);
					afAmzonPartnerCategoryBean.setAmazonCategoryStatusCode(1);
				}
				if (0 == amazonStatus) {
					afAmzonPartnerCategoryBean.setAmazonCategoryStatus(AfConstants.STATUS_IN_ACTIVE);
					afAmzonPartnerCategoryBean.setAmazonCategoryStatusClass(AfConstants.STATUS_CLASS_RED);
					afAmzonPartnerCategoryBean.setAmazonCategoryStatusCode(0);
				}
				afAmzonoPartnerCategoryBean.add(afAmzonPartnerCategoryBean);
			}
			
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in CategoryManagementServiceImpl at getAmazonMappingDetails() :", e);
		}
		return afAmzonoPartnerCategoryBean;
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public String CreateAmazonPartner(JSONObject categoryJson,HttpServletRequest request) {
		String oldOrNewVal = "";
		String commLogStatus = "";
		String status=AfConstants.SUCCESS;
		try{
		AfAmazonPartnerCategory afAmazonPartnerCategory= new AfAmazonPartnerCategory();
		afAmazonPartnerCategory.setAfPartner(partnermanagementDAO.getPartnerByPartnerNO(categoryJson.getString("partNumber")));
		afAmazonPartnerCategory.setAfPartnerCategory(categorymanagementDAO.getAfPartnerCategories(categoryJson.getInt("amazonCategoryName")));
		afAmazonPartnerCategory.setAmzCategoryId(categoryJson.getString("amazonOrderCategoryId"));
		afAmazonPartnerCategory.setCreatedTime(new Date());
		afAmazonPartnerCategory.setAmzCategoryName(categoryJson.getString("amazonOrderCategoryName"));
		afAmazonPartnerCategory.setAmazonCategoryStatus((byte)categoryJson.getInt("mainStatus"));
		afAmazonPartnerCategory.setStatus(new Byte (categoryJson.getString("createpartcatstatus")));
		afAmazonPartnerCategory.setModifiedTime(new Date());
		afAmazonPartnerCategory.setModifiedBy(1);
		status=categorymanagementDAO.CreateAmazonPartner(afAmazonPartnerCategory);
		String status1=afAmazonPartnerCategory.getAmazonCategoryStatus()+"";
		status1 = status1.replaceAll("0","InActive");
		status1 = status1.replaceAll("1", "Active");
		if("success".equalsIgnoreCase(status)){
			String ipAddress = request.getHeader("X-FORWARDED-FOR");
			if(ipAddress == null) {
				ipAddress = request.getRemoteAddr();
			}
			ipAddress = (null != ipAddress) ? ipAddress.trim() : "";
			AfUserActivityLog afPrtActLog=new AfUserActivityLog();
			oldOrNewVal=" Partners : "+afAmazonPartnerCategory.getAfPartner().getPartnerName()+" | "+"Amazon Partner Categories : "+afAmazonPartnerCategory.getAfPartnerCategory().getPartnerCategoryName()+" | "+
					""+"Amazon Category ID: "+afAmazonPartnerCategory.getAmzCategoryId()+"|"
							+ " "+"Amazon Order Category Name: "+afAmazonPartnerCategory.getAmzCategoryName()+"|"+"Status : "+status1+" |";
								
		
					afPrtActLog.setUserNewFieldValue(oldOrNewVal);
					afPrtActLog.setUserOldFieldValue("NA");
					afPrtActLog.setUserActivity("Create Amazon Category");
					afPrtActLog.setUserActCreatedDateTime(new Date());
					afPrtActLog.setUserActIpVal(ipAddress);
					afPrtActLog.setUserActModule("Category Management");
					afPrtActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
					
					commLogStatus = userActLogDao.createUserActLog(afPrtActLog);
		}
		
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in CategoryManagementServiceImpl at CreateAmazonPartner() :", e);
			status = AfConstants.FAILURE;
			return status;
			
		}
		return status;
	}
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public String updateAmazonPartner(JSONObject categoryJson,
			HttpServletRequest request) {
		String status=AfConstants.SUCCESS;
		String replaceInOld="";
		String replaceInNew="";
		AfAmazonPartnerCategory afAmazonPartnerCategoryOld=null;
		AfAmazonPartnerCategory afAmazonPartnerCategory=null;
		try{
	    afAmazonPartnerCategory= categorymanagementDAO.getAmazonmappingDetailsById(categoryJson.getInt("id"));
		afAmazonPartnerCategoryOld=(AfAmazonPartnerCategory) afAmazonPartnerCategory.clone();
		afAmazonPartnerCategory.setAfPartner(partnermanagementDAO.getPartnerByPartnerNO(categoryJson.getString("partNumber")));
		afAmazonPartnerCategory.setAfPartnerCategory(categorymanagementDAO.getAfPartnerCategories(categoryJson.getInt("amazonCategoryName")));
		afAmazonPartnerCategory.setAmzCategoryId(categoryJson.getString("amazonOrderCategoryId"));
		afAmazonPartnerCategory.setCreatedTime(new Date());
		afAmazonPartnerCategory.setAmzCategoryName(categoryJson.getString("amazonOrderCategoryName"));
		afAmazonPartnerCategory.setAmazonCategoryStatus((byte)categoryJson.getInt("createpartcatstatus"));
		afAmazonPartnerCategory.setModifiedTime(new Date());
		afAmazonPartnerCategory.setModifiedBy(1);
		
		String statusInactive=	afAmazonPartnerCategoryOld.getAmazonCategoryStatus()==0?"InActive":"Active";
     	String statusActive=afAmazonPartnerCategory.getAmazonCategoryStatus()==0?"InActive":"Active";
     	
     	status=categorymanagementDAO.CreateAmazonPartner(afAmazonPartnerCategory);
		
		if("success".equalsIgnoreCase(status)){
			String ipAddress = request.getHeader("X-FORWARDED-FOR");
			if (ipAddress == null) {
				ipAddress = request.getRemoteAddr();
			}
			AfUserActivityLog updateUsActLog = new AfUserActivityLog();
			Map<String,String> objectCom = new LinkedHashMap<>();
			
				updateUsActLog.setUserActivity("Update Amazon Category");
				updateUsActLog.setUserActCreatedDateTime(new Date());
				updateUsActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
				updateUsActLog.setUserActIpVal(ipAddress);
				updateUsActLog.setUserActModule("Category Management");
				
				if(!afAmazonPartnerCategory.getAmzCategoryId().equalsIgnoreCase(afAmazonPartnerCategoryOld.getAmzCategoryId())){
					replaceInOld=replaceInOld+"| Amazon Category Id:"+afAmazonPartnerCategoryOld.getAmzCategoryId();
					replaceInNew=replaceInNew+"| Amazon Category Id:"+afAmazonPartnerCategory.getAmzCategoryId();
				}
				if(!afAmazonPartnerCategory.getAmzCategoryName().equalsIgnoreCase(afAmazonPartnerCategoryOld.getAmzCategoryName())){
					replaceInOld=replaceInOld+"| Amazon Order Category Name:"+afAmazonPartnerCategoryOld.getAmzCategoryName();
					replaceInNew=replaceInNew+"| Amazon Order Category Name:"+afAmazonPartnerCategory.getAmzCategoryName();
				}
				if(afAmazonPartnerCategory.getAmazonCategoryStatus()!=afAmazonPartnerCategoryOld.getAmazonCategoryStatus()){
					replaceInOld=replaceInOld+"| Amazon Category Status:"+statusInactive;
					replaceInNew=replaceInNew+"| Amazon Category Status:"+statusActive;
				}
				if(replaceInNew.isEmpty() && replaceInOld.isEmpty()) {
                    replaceInOld=replaceInOld+"| ModifiedTime:"+simpleDateFormat.format(afAmazonPartnerCategoryOld.getModifiedTime());
                    replaceInNew=replaceInNew+"| ModifiedTime:"+simpleDateFormat.format(afAmazonPartnerCategory.getModifiedTime());
                }
				updateUsActLog.setUserNewFieldValue(replaceInNew);
	    		updateUsActLog.setUserOldFieldValue(replaceInOld);
				userActLogDao.updateUserLog(updateUsActLog);
			
			
		}
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in CategoryManagementServiceImpl at CreateAmazonPartner() :", e);
			/*status = AfConstants.FAILURE;
			return status;*/
			
		}
		return status;
	}

	
}
