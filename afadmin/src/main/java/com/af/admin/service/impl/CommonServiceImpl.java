package com.af.admin.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.multipart.MultipartFile;

import com.af.admin.DAO.CategoryManagementDAO;
import com.af.admin.DAO.CommonDAO;
import com.af.admin.DAO.PartnerManagementDAO;
import com.af.admin.DAO.UserActivityLogDAO;
import com.af.admin.beans.AdminMenu;
import com.af.admin.beans.AdminRolesBean;
import com.af.admin.beans.AfAdminMenusBean;
import com.af.admin.beans.AfPermissionValuesBean;
import com.af.admin.beans.PartnerCategoryBean;
import com.af.admin.beans.SellerTextBean;
import com.af.admin.beans.SeoBean;
import com.af.admin.constants.AfConstants;
import com.af.admin.constants.AfHibernateQueries;
import com.af.admin.entity.AfAdminRoles;
import com.af.admin.entity.AfCategory;
import com.af.admin.entity.AfEnumValues;
import com.af.admin.entity.AfPartner;
import com.af.admin.entity.AfPartnerCategory;
import com.af.admin.entity.AfPartnerKeyword;
import com.af.admin.entity.AfPartnerOffer;
import com.af.admin.entity.AfPartnerProduct;
import com.af.admin.entity.AfPermissionValues;
import com.af.admin.entity.AfSeo;
import com.af.admin.entity.AfUserActivityLog;
import com.af.admin.service.CategoryManagementService;
import com.af.admin.service.CommonService;
import com.af.admin.service.OffersDealsService;
import com.af.admin.service.PartnerManagementService;
import com.af.admin.service.SendGetRequest;
import com.af.admin.utils.CommonUtil;
import com.af.admin.utils.ServiceProviderUtil;
import com.af.admin.ws.Services;
import com.opencsv.CSVWriter;


@Component
@Transactional(readOnly = true)
public class CommonServiceImpl implements CommonService {
	private Logger logger = LogManager.getLogger(CommonServiceImpl.class.getName());
	
	
	@Autowired
	Services services;
	
	@Autowired
	SendGetRequest sendGetRequest;
	
	@Autowired
	CommonUtil commonUtil;

	@Autowired
	CommonDAO commonDAO;

	@Autowired
	PartnerManagementDAO partnerManagementDAO;
	
	@Autowired
	UserActivityLogDAO userActLogDao;

	@Autowired
	CategoryManagementDAO categoryManagementDAO;

	@Autowired
	CategoryManagementService categoryManagementService;

	@Autowired
	PartnerManagementService partnerManagementService;
	
	@Autowired
	OffersDealsService offersDealsService;
	
	

	@Value("${application.af.upload.images.folder}")
	private String imagesFolderLocation;

	@Value("${application.af.upload.files.folder}")
	private String fileFolderLocation;
	
	@Value("${application.af.serverCacheUrl}")
	private String serverCacheUrl;

	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
	@Override
	public List<AdminMenu> getMenus(List<AdminMenu> adminMenus,
			Map<String, List<AdminMenu>> submenuMap, Integer userRole) {
		List<AfAdminMenusBean> listAdminMenusBeans = new ArrayList<AfAdminMenusBean>();
		int parentMenuId = 0, prevParentMenuId = 0;
		String title;
		List<AdminMenu> lisAdminMenus = null;
		List<String> permissionNames = null;
		try {
			listAdminMenusBeans = commonDAO.getMenus();
			permissionNames = getPermissionList(userRole);
			if (null == permissionNames)
				permissionNames = new ArrayList<String>();
			for (AfAdminMenusBean afAdminMenusBean : listAdminMenusBeans) {
				parentMenuId = afAdminMenusBean.getParentMenuId();
				title = afAdminMenusBean.getTitle();
				title = (null != title) ? title.trim() : "";
				if (parentMenuId == 0) {
					if (!permissionNames.contains(title))
						continue;
					AdminMenu adminMenu = new AdminMenu();
					adminMenu.setMenuId((null != afAdminMenusBean.getMenuId()) ? String.valueOf(afAdminMenusBean.getMenuId()) : "0");
					adminMenu.setTitle((null != afAdminMenusBean.getTitle()) ? afAdminMenusBean.getTitle().trim() : "");
					adminMenu.setUrl((null != afAdminMenusBean.getUrl()) ? afAdminMenusBean.getUrl().trim() : "");
					adminMenu.setParentMenuId(String.valueOf(afAdminMenusBean.getParentMenuId()));
					adminMenus.add(adminMenu);
				}

				else {
					if ((0 == prevParentMenuId)
							|| (parentMenuId != prevParentMenuId)) {
						lisAdminMenus = new ArrayList<AdminMenu>();
					}
					title = afAdminMenusBean.getTitle();
					title = (null != title) ? title.trim() : "";

					if (6 == parentMenuId && !permissionNames.contains(title)) {
						prevParentMenuId = parentMenuId;
						continue;
					}
					if (4 == parentMenuId && !permissionNames.contains(title)) {
						prevParentMenuId = parentMenuId;
						continue;
					}
					AdminMenu adminMenu = new AdminMenu();
					adminMenu.setMenuId((null != afAdminMenusBean.getMenuId()) ? String.valueOf(afAdminMenusBean.getMenuId()) : "0");
					adminMenu.setTitle((null != afAdminMenusBean.getTitle()) ? afAdminMenusBean.getTitle().trim() : "");
					adminMenu.setUrl((null != afAdminMenusBean.getUrl()) ? afAdminMenusBean.getUrl().trim() : "");
					adminMenu.setParentMenuId(String.valueOf(afAdminMenusBean.getParentMenuId()));
					lisAdminMenus.add(adminMenu);
					submenuMap.put(String.valueOf(parentMenuId), lisAdminMenus);
					prevParentMenuId = parentMenuId;
				}

			}

		} catch (Exception e) {
			logger.error("@@@@Exception in CommonServiceImpl at getMenus() :", e);
		}
		return adminMenus;
	}

	@Override
	public List<AdminRolesBean> getAdminRoles() {
		List<AfAdminRoles> afAdminRoleslist = new ArrayList<AfAdminRoles>();
		List<AdminRolesBean> liAdminRolesBeans = new ArrayList<AdminRolesBean>();
		String roleName = "", description = "";
		try {
			afAdminRoleslist = commonDAO.getAdminRoles();
			for (AfAdminRoles afAdminRoles : afAdminRoleslist) {
				AdminRolesBean adminRolesBean = new AdminRolesBean();

				roleName = afAdminRoles.getRoleName();
				roleName = (null != roleName) ? roleName.trim() : "";
				adminRolesBean.setRoleName(roleName);

				description = afAdminRoles.getDescription();
				description = (null != description) ? description.trim() : "";
				adminRolesBean.setDescription(description);

				adminRolesBean.setRoleId(afAdminRoles.getRoleId());

				liAdminRolesBeans.add(adminRolesBean);
			}
		} catch (Exception e) {
			logger.error("@@@@Exception in CommonServiceImpl at getAdminRoles() :", e);
		}
		return liAdminRolesBeans;
	}

	@Override
	public List<AfPermissionValuesBean> getPermissionValues() {
		List<AfPermissionValues> afPermissionValues = new ArrayList<AfPermissionValues>();
		List<AfPermissionValuesBean> afPermissionValuesBeans = new ArrayList<AfPermissionValuesBean>();
		try {
			afPermissionValues = commonDAO.getPermissionValues();
		} catch (Exception e) {
			logger.error("@@@@Exception in CommonServiceImpl at getMenus() :", e);
		}
		if (null != afPermissionValues) {
			for (AfPermissionValues afValues : afPermissionValues) {
				AfPermissionValuesBean afPermissionValuesBean = new AfPermissionValuesBean();
				afPermissionValuesBean.setPermissionId(afValues.getPermissionId());
				afPermissionValuesBean.setPermissionName(afValues.getPermissionName());
				afPermissionValuesBeans.add(afPermissionValuesBean);
			}
		}
		return afPermissionValuesBeans;
	}

	@Override
	public List<String> getPermissionList(Integer userRole) {
		List<String> permissionNames = null;
		try {
			permissionNames = commonDAO.getRolePermissions(userRole);
		} catch (Exception e) {
			logger.error("@@@@Exception in CommonServiceImpl at getPermissionValues() :", e);
		}
		return permissionNames;
	}

	@Override
	public String saveImage(MultipartFile file, String moduleName) {
		moduleName = (null != moduleName) ? moduleName.trim() : "";
		String destination = "", fileName = "", extension = "", serverFileName = "",originalFilename="";;
		try {
			destination = imagesFolderLocation + moduleName + "/";
			File theDirs = new File(destination);
			if (!theDirs.exists()) {
				theDirs.mkdirs();
			}
			if (!file.isEmpty()) {
				originalFilename = (null !=file.getOriginalFilename())?file.getOriginalFilename().trim():"";
				if(!originalFilename.isEmpty())
				{
				fileName= originalFilename.substring(0, originalFilename.lastIndexOf("."));
				extension = originalFilename.substring(originalFilename.lastIndexOf("."), originalFilename.length());
				fileName = fileName.replaceAll("[^\\w\\s]","").replaceAll(" ", "");
                serverFileName = fileName+System.currentTimeMillis()+extension;
                destination = destination + serverFileName;
					try {
						file.transferTo(new File(destination));
					} catch (IllegalStateException | IOException e) {
						e.printStackTrace();
					}
				}
			}
		} catch (Exception e) {
			logger.error("@@@@Exception in CommonServiceImpl at saveImage() :", e);
		}
		return serverFileName;
	}

	@Override
	public void saveExcel(String filename, MultipartFile excel,
			HttpServletRequest request) {
		try {
			File file = new File(fileFolderLocation + filename);
			FileUtils.writeByteArrayToFile(file, excel.getBytes());
		} catch (IOException e) {
			logger.error("@@@@Exception in CommonServiceImpl at saveExcel() :", e);
		}
	}

	@Override
	public Map<String, String> getPartnerProductList(String prtCatNo_, String rpName) {
		int prtCatNo;
		prtCatNo = Integer.parseInt(prtCatNo_);
		Map<String, String> ppList = new LinkedHashMap<String, String>();
		List<AfPartnerProduct> afppList = new ArrayList<AfPartnerProduct>();
		
		long startime=System.currentTimeMillis(),endtime=System.currentTimeMillis();
		String response="";
		
		startime=System.currentTimeMillis();
		response = services.PartnerProductSearchInfoService(String.valueOf(prtCatNo), rpName);			
		endtime=System.currentTimeMillis();
		logger.info("getPartnerProductList   while gettring response -----------start-time----"+startime+" ms");
		logger.info("getPartnerProductList   while gettring response -----------end-time----"+endtime+" ms");
		logger.info("getPartnerProductList   while gettring response -----------time-diff in Milisec----"+(endtime-startime)+" ms");
		logger.info("getPartnerProductList   while gettring response -----------time-diff in sec----"+((endtime-startime)/1000)+" sec");
		startime=System.currentTimeMillis();
		if(null!=response && !response.isEmpty() && response.length()>0)
			ppList = commonUtil.getPartnerProductSearchInfoList(response);
		endtime=System.currentTimeMillis();
		logger.info("getPartnerProductList   while parsing response -----------start-time----"+startime+" ms");
		logger.info("getPartnerProductList   while parsing response -----------end-time----"+endtime+" ms");
		logger.info("getPartnerProductList   while parsing response -----------time-diff in Milisec----"+(endtime-startime)+" ms");
		logger.info("getPartnerProductList   while parsing response -----------time-diff in sec----"+((endtime-startime)/1000)+" sec");
		
		
		return ppList;
	}
	
	@Override
	public HashSet<String> getBrandForParnerCategory(String prtCatNo_){
		int prtCatNo;
		prtCatNo = Integer.parseInt(prtCatNo_);
		
		HashSet<String> brandList = new HashSet<String>();
		
		List<AfPartnerProduct> afppList = new ArrayList<AfPartnerProduct>();
		
		try {
			afppList = commonDAO.getPartnerProductList(prtCatNo);
		} catch (Exception e) {
			logger.error("@@@@Exception in CommonServiceImpl at getBrandForParnerCategory() :", e);
		}
		for (AfPartnerProduct pproduct : afppList) {
			brandList.add(pproduct.getProductBrand());
		}

		return brandList;
	}
	
	@Override
	public Map<String, String> getPartnerCategoryListMap(String prtNo_) {
		int prtNo;
		prtNo = Integer.parseInt(prtNo_);
		Map<String, String> pcList = new LinkedHashMap<String, String>();
		List<AfPartnerCategory> afpcList = new ArrayList<AfPartnerCategory>();
		try {
			afpcList = commonDAO.getPartnerCategoryList(prtNo);
		} catch (Exception e) {
			logger.error("@@@@Exception in CommonServiceImpl at getPartnerCategoryListMap() :", e);
		}
		for (AfPartnerCategory pcategory : afpcList) {
			pcList.put(pcategory.getPrtCatNo().toString(),
					pcategory.getPartnerCategoryName());
		}
		return pcList;
	}

	@Override
	public List<PartnerCategoryBean> getPartnerCategoryListBean(String prtNo_) {
		int prtNo;
		prtNo = Integer.parseInt(prtNo_);
		List<PartnerCategoryBean> pcList = new ArrayList<PartnerCategoryBean>();
		List<AfPartnerCategory> afpcList = new ArrayList<AfPartnerCategory>();
		try {
			afpcList = commonDAO.getPartnerCategoryList(prtNo);
		} catch (Exception e) {
			logger.error("@@@@Exception in CommonServiceImpl at getPartnerCategoryListBean() :", e);
		}
		for (AfPartnerCategory pcategory : afpcList) {
			PartnerCategoryBean partCategoryBean = new PartnerCategoryBean();
			partCategoryBean.setPrtCatNo(pcategory.getPrtCatNo().toString());
			partCategoryBean.setPartnercategoryName(pcategory.getPartnerCategoryName());
			pcList.add(partCategoryBean);
		}
		return pcList;
	}

	@Override
	public Map<Byte, String> getEnumValues(String typeName) {
		typeName = (null != typeName) ? typeName.trim() : "";
		Map<Byte, String> enumValues = new HashMap<Byte, String>();
		List<AfEnumValues> enumList = new ArrayList<AfEnumValues>();
		try {
			enumList = commonDAO.getEnumValues(typeName);
		} catch (Exception e) {
			logger.error("@@@@Exception in CommonServiceImpl at getEnumValues() :", e);
		}
		for (AfEnumValues enumVal : enumList) {
			enumValues.put(enumVal.getIntCode(), enumVal.getValue());
		}
		return enumValues;
	}
	
	@Override
	public String getJPMilesEnumValues(String typeName) {
		typeName = (null != typeName) ? typeName.trim() : "";
		String jpMiles="";
		List<AfEnumValues> enumList = new ArrayList<AfEnumValues>();
		try {
			enumList = commonDAO.getEnumValues(typeName);
		} catch (Exception e) {
			logger.error("@@@@Exception in CommonServiceImpl at getJPMilesEnumValues() :", e);
		}
		for (AfEnumValues enumVal : enumList) {
			jpMiles = enumVal.getValue();
		}
		return jpMiles;
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public String cerateKeyWord(List<String> keyWord, String filePath,
			String prtNo, HttpServletRequest request,String prtType) {
		AfPartner afPartner;
		AfPartnerKeyword afPartnerKeyword;
		AfPartner afp;
		String status = "failure";
		List<AfPartnerKeyword> afPartnerKeywords = new ArrayList<AfPartnerKeyword>();
		try {

			afPartner = partnerManagementDAO.getPartnerByPartnerNO(prtNo);
			status = commonDAO.deleteKeyWord(Integer.parseInt(prtNo));
			for (int i = 0; i < keyWord.size(); i++) {
				String checKeyword = "";
				afPartnerKeyword = new AfPartnerKeyword();
				afPartnerKeyword.setAfPartner(afPartner);
				afPartnerKeyword.setKeyword(keyWord.get(i));
				afPartnerKeyword.setFilePath(filePath);
				checKeyword = keyWord.get(i).toString();
				checKeyword = checKeyword.replaceAll("\\s+", "");
				if (checKeyword.startsWith("-"))
					afPartnerKeyword.setKeywordStatus((byte) 0);
				else {
					afPartnerKeyword.setKeywordStatus((byte) 1);
				}
				afPartnerKeyword.setStatus((byte) 1);
				afPartnerKeyword.setCreatedTime(new Date());
				afPartnerKeyword.setActivationDate(new Date());
				afPartnerKeyword.setCreatedBy(1);
				afPartnerKeywords.add(afPartnerKeyword);
			}
			status = commonDAO.cerateKeyWord(afPartnerKeywords);
			
			if(status.equalsIgnoreCase("success")){
				
				String prtLogStatus = "";
				String partnerNameFile = "";
				
				String ipAddress = request.getHeader("X-FORWARDED-FOR");
				if (ipAddress == null) {
					ipAddress = request.getRemoteAddr();
				}
				
				AfUserActivityLog afPrtActLog=new AfUserActivityLog();
				try{
				afPrtActLog.setUserOldFieldValue("NA");
				afPrtActLog.setUserActivity("Created File Upload");
				afPrtActLog.setUserActIpVal(ipAddress);
				afPrtActLog.setUserActModule("Partner File Management");
				afPrtActLog.setUserActCreatedDateTime(new Date());
				
				afp = partnerManagementDAO.getPartnerByPartnerNO(prtNo);
				partnerNameFile=afp.getPartnerName();
				
				afPrtActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
				afPrtActLog.setUserNewFieldValue("File Path : "+filePath+" | "+"Partner Type:"+prtType+" | "
						+"Partner Name:"+afPartner.getPartnerName());
				
				prtLogStatus = userActLogDao.createUserActLog(afPrtActLog);
				}catch (Exception e) {
					logger.error("@@@@Exception in CommonServiceImpl at cerateKeyWord() in user activity Log :", e);
				}
			}
			
			
		} catch (Exception e) {
			logger.error("@@@@Exception in CommonServiceImpl at cerateKeyWord() :", e);
			return "failure";
		}
		return status;
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public String deleteFile(Integer prtNo, HttpServletRequest request) {
		String status = "failure";
		String filePath = "";
		AfPartner afp;
		String partnerNameFile = "";
		List<AfPartnerKeyword> afPartnerKeywords = new ArrayList<AfPartnerKeyword>();
		try {
			afPartnerKeywords = partnerManagementDAO.getKeywordFileName(prtNo.toString());
			if (afPartnerKeywords != null)
				filePath = afPartnerKeywords.get(0).getFilePath();
			if (filePath != null) {
				File file = new File(filePath);

				if (file.delete()) {
				} else {
				}
			}
			status = commonDAO.deleteKeyWord(prtNo);
			if(status.equalsIgnoreCase(AfConstants.SUCCESS)){
				String ipAddress = request.getHeader("X-FORWARDED-FOR");
				if (ipAddress == null) {
					ipAddress = request.getRemoteAddr();
				}
				
				ipAddress = (null != ipAddress) ? ipAddress.trim() : "";
				
				AfUserActivityLog afPrtActLog=new AfUserActivityLog();
				
				afp = partnerManagementDAO.getPartnerByPartnerNO(prtNo.toString());
				partnerNameFile=afp.getPartnerName();
			
				afPrtActLog.setUserNewFieldValue("NA");
				afPrtActLog.setUserActivity("Deleted Uploaded File");
				afPrtActLog.setUserActIpVal(ipAddress);
				afPrtActLog.setUserActModule("Partner File Management");
				afPrtActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
				afPrtActLog.setUserActCreatedDateTime(new Date());
				afPrtActLog.setUserOldFieldValue("File Path : "+filePath+" | "+"Partner Name : "+partnerNameFile);
				userActLogDao.updateUserLog(afPrtActLog);
			}
			
		} catch (Exception e) {
			logger.error("@@@@Exception in CommonServiceImpl at deleteFile() :", e);
			return "failure";
		}
		return status;
	}

	@Override
	public String getFileContent(MultipartFile file, HttpServletRequest request) {
		String content = null;
		if (!file.isEmpty()) {
			StringBuilder contentBuilder = new StringBuilder();
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(file.getInputStream()));
				String str;
				while ((str = in.readLine()) != null) {
					contentBuilder.append(str);
				}
				in.close();
			} catch (IOException e) {
				logger.error("@@@@Exception in CommonServiceImpl at getFileContent() :", e);
			}
			content = contentBuilder.toString();
		}
		return content;
	}
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void cerateOffersAndDeals(List<String> offersAndDealsList, String filePath, int NoOfcolumns, ModelMap model) {

		AfPartnerOffer afPartnerOffer;
		String status = "failure";
		Set<AfPartnerOffer> afPartnerOfferSuccessSet = new HashSet<AfPartnerOffer>();

		Date dt = null;
		DateFormat dateTimeFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
		SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd");
		
		String hh_mi_ss = new Date().toString();
		String format[] = hh_mi_ss.split(" ");
		hh_mi_ss = format[3];
		int successCount = 0, failureCount = 0;
		Integer maxCount = 0;
		boolean isRecordExist = false;

		try {
			maxCount = offersDealsService.getMaxOfferandDealsSlNo();
			maxCount= (maxCount != null) ? maxCount : 0;
			Map<String, AfCategory> afCategoryMap = categoryManagementService.getCategoryMap();
			Map<String, AfPartner> afPartnerMap   = partnerManagementService.getPartnerMap();
			
			for (int i = 1; i < offersAndDealsList.size() / NoOfcolumns; i++) {
				try {
					afPartnerOffer = new AfPartnerOffer();
					dt = null;
					status = "failure";
					isRecordExist = false;
					if( (offersAndDealsList.get(i * NoOfcolumns + 1) != null)
							&& (afPartnerMap.get(offersAndDealsList.get(i * NoOfcolumns + 1).toLowerCase().trim()) != null) ) {
						afPartnerOffer.setAfPartner(afPartnerMap.get(offersAndDealsList.get(
								i * NoOfcolumns + 1).toLowerCase().trim()));
					} else {
						failureCount ++;
						continue;
					}
					if( (offersAndDealsList.get(i * NoOfcolumns + 2) != null)
							&& (afCategoryMap.get(
									!"-".equalsIgnoreCase(String.valueOf(
											offersAndDealsList.get(i * NoOfcolumns + 2).trim().charAt(
													offersAndDealsList.get(i * NoOfcolumns + 2).trim().length() - 1)))
													? offersAndDealsList.get(i * NoOfcolumns + 2).trim() :
														offersAndDealsList.get(i * NoOfcolumns + 2).trim().replace(
																String.valueOf(offersAndDealsList.get(i * NoOfcolumns + 2).trim()
																		.charAt(offersAndDealsList.get(i * NoOfcolumns + 2).trim().length() - 1)), "")
									) != null) ) {
						afPartnerOffer.setAfCategory(afCategoryMap.get(
								!"-".equalsIgnoreCase(String.valueOf(
										offersAndDealsList.get(i * NoOfcolumns + 2).trim().charAt(
												offersAndDealsList.get(i * NoOfcolumns + 2).trim().length() - 1)))
												? offersAndDealsList.get(i * NoOfcolumns + 2).trim() :
													offersAndDealsList.get(i * NoOfcolumns + 2).trim().replace(
															String.valueOf(offersAndDealsList.get(i * NoOfcolumns + 2).trim()
																	.charAt(offersAndDealsList.get(i * NoOfcolumns + 2).trim().length() - 1)), "")));
					} else {
						failureCount ++;
						continue;
					}
					if(offersAndDealsList.get(i * NoOfcolumns + 3) != null) {
						afPartnerOffer.setOfferName(offersAndDealsList.get(i * NoOfcolumns + 3));
					} else {
						failureCount ++;
						continue;
					}
					if(offersAndDealsList.get(i * NoOfcolumns + 4) != null) {
						afPartnerOffer.setDescription(offersAndDealsList.get(i * NoOfcolumns + 4));
					} else {
						failureCount ++;
						continue;
					}
					afPartnerOffer.setOfferLandingUrl(offersAndDealsList.get(i * NoOfcolumns + 5));
					afPartnerOffer.setOfferLogoUrl(offersAndDealsList.get(i * NoOfcolumns + 6));
					afPartnerOffer.setOfferPosition(!offersAndDealsList.get(i * NoOfcolumns + 7)
							.equalsIgnoreCase("") ? Integer.parseInt(offersAndDealsList.get(i * NoOfcolumns + 7)) : null);
					afPartnerOffer.setImageAltTxt(offersAndDealsList.get(i * NoOfcolumns + 8));

					if((offersAndDealsList.get(i * NoOfcolumns + 9) + " " + hh_mi_ss).length() > 0)
						dt = dateTimeFormat.parse(offersAndDealsList.get(i * NoOfcolumns + 9) + " " + hh_mi_ss);
					afPartnerOffer.setActivationDate(dt);

					if((offersAndDealsList.get(i * NoOfcolumns + 10) + " " + hh_mi_ss).length() > 0)
						dt = dateTimeFormat.parse(offersAndDealsList.get(i * NoOfcolumns + 10) + " " + hh_mi_ss);
					afPartnerOffer.setDeactivationDate(dt);

					if(offersAndDealsList.get(i * NoOfcolumns + 11) != null) {
						afPartnerOffer.setOfferStatus(offersAndDealsList.get(
								i * NoOfcolumns + 11).trim().equalsIgnoreCase("Active") ? (byte) 1 : (byte) 0);
					} else {
						failureCount ++;
						continue;
					}
					afPartnerOffer.setOfferId("");
					afPartnerOffer.setCreatedTime(new Date());
					afPartnerOffer.setCreatedBy(1);
					afPartnerOffer.setModifiedTime(new Date());
					afPartnerOffer.setModifiedBy(1);
					afPartnerOffer.setStatus((byte) 1);

					
					StringBuffer queryBuffer = new StringBuffer(AfHibernateQueries.GET_AF_PARTNER_OFFER_BY_COND);
					
					queryBuffer.append(" and afpo.afPartner.prtNo='").append(afPartnerOffer.getAfPartner().getPrtNo()).append("'")
					.append(" and afpo.afCategory.catNo='").append(afPartnerOffer.getAfCategory().getCatNo()).append("'")
					.append(" and afpo.offerName='").append(afPartnerOffer.getOfferName()).append("'")
					.append(" and afpo.description='").append(afPartnerOffer.getDescription()).append("'")
					.append(" and afpo.offerStatus='").append(afPartnerOffer.getOfferStatus()).append("'")
					.append(" and date(afpo.activationDate) > date(sysdate()) ");
					
					List<AfPartnerOffer> afPartnerOfferList = commonDAO.getOffersAndDeals(queryBuffer.toString());
					for(AfPartnerOffer afPartnerOffers : afPartnerOfferList) {
						if( (afPartnerOffer.getOfferPosition() != null) &&
								( (afPartnerOffers.getOfferPosition() != null) && (afPartnerOffers.getOfferStatus() == 1) ) ) {
							failureCount ++;
							isRecordExist = true;
							continue;
						} else {
							afPartnerOffers.setStatus((byte)0);
							
							status = commonDAO.cerateOffersAndDeals(afPartnerOffers);
						}
					}
					if(!isRecordExist) {
						queryBuffer = new StringBuffer(AfHibernateQueries.GET_AF_PARTNER_OFFER_BY_COND)
						.append(" and afpo.afPartner.prtNo='").append(afPartnerOffer.getAfPartner().getPrtNo()).append("'")
						.append(" and afpo.afCategory.catNo='").append(afPartnerOffer.getAfCategory().getCatNo()).append("'")
						.append(" and afpo.offerName='").append(afPartnerOffer.getOfferName()).append("'")
						.append(" and afpo.description='").append(afPartnerOffer.getDescription()).append("'")
						.append(" and afpo.offerStatus='").append(afPartnerOffer.getOfferStatus()).append("'");
						
						if(afPartnerOffer.getOfferLandingUrl() != null) {
							queryBuffer.append(" and afpo.offerLandingUrl='").append(afPartnerOffer.getOfferLandingUrl()).append("'");
						} else {
							queryBuffer.append(" and afpo.offerLandingUrl is null");
						}
						if(afPartnerOffer.getOfferLogoUrl() != null) {
							queryBuffer.append(" and afpo.offerLogoUrl='").append(afPartnerOffer.getOfferLogoUrl()).append("'");
						} else {
							queryBuffer.append(" and afpo.offerLogoUrl is null");
						}
						if(afPartnerOffer.getOfferPosition() != null) {
							queryBuffer.append(" and afpo.offerPosition='").append(afPartnerOffer.getOfferPosition()).append("'");
						} else {
							queryBuffer.append(" and afpo.offerPosition is null");
						}
						if(afPartnerOffer.getImageAltTxt() != null) {
							queryBuffer.append(" and afpo.imageAltTxt='").append(afPartnerOffer.getImageAltTxt()).append("'");
						} else {
							queryBuffer.append(" and afpo.imageAltTxt is null");
						}
						queryBuffer.append(" and date(afpo.activationDate)='").append(dateFormater.format(
								afPartnerOffer.getActivationDate())).append("'");
						if(afPartnerOffer.getDeactivationDate() != null) {
							queryBuffer.append(" and date(afpo.deactivationDate)='").append(dateFormater.format(
									afPartnerOffer.getDeactivationDate())).append("'");
						} else {
							queryBuffer.append(" and afpo.deactivationDate is null");
						}
						if(afPartnerOffer.getOfferId() != null) {
							queryBuffer.append(" and afpo.offerId='").append(afPartnerOffer.getOfferId()).append("'");
						} else {
							queryBuffer.append(" and afpo.offerId is null");
						}
						afPartnerOfferList = new ArrayList<AfPartnerOffer>();
						afPartnerOfferList = commonDAO.getOffersAndDeals(queryBuffer.toString());
						
						/*if(afPartnerOfferList.isEmpty()) {
							afPartnerOfferList = new ArrayList<AfPartnerOffer>();
	
							queryBuffer = new StringBuffer(AfHibernateQueries.GET_AF_PARTNER_OFFER_BY_COND);
	
							queryBuffer.append(" and afpo.afPartner.prtNo='" + afPartnerOffer.getAfPartner().getPrtNo() + "'");
							queryBuffer.append(" and afpo.afCategory.catNo='" + afPartnerOffer.getAfCategory().getCatNo() + "'");
							queryBuffer.append(" and afpo.offerName='" + afPartnerOffer.getOfferName() + "'");
							queryBuffer.append(" and afpo.description='" + afPartnerOffer.getDescription() + "'");
							queryBuffer.append(" and afpo.offerStatus='" + afPartnerOffer.getOfferStatus() + "'");
							queryBuffer.append(" and date(afpo.activationDate) > date(sysdate()) ");
	
							afPartnerOfferList = commonDAO.getOffersAndDeals(queryBuffer.toString());

							for(AfPartnerOffer afPartnerOffers : afPartnerOfferList) {
								//Update existing records with status 0
								afPartnerOffers.setStatus((byte)0);
	
								status = commonDAO.cerateOffersAndDeals(afPartnerOffers);
							}
						}*/
						if(afPartnerOfferList.isEmpty()) {
							status = commonDAO.cerateOffersAndDeals(afPartnerOffer);
							
							if(status.equalsIgnoreCase("success")) {
								successCount ++;
								afPartnerOfferSuccessSet.add(afPartnerOffer);
							}
						} else {
							failureCount ++;
						}
					}
				} catch (Exception e) {
					logger.info("@@@@Exception in CommonServiceImpl at cerateOffersAndDeals(-, -)"
							+ " inside for loop :", e);
				}
			}
			try {
				if(afPartnerOfferSuccessSet.size() > 0) {
					filePath = filePath.replaceAll(".csv", "_success.csv");
					CSVWriter csvWriter = new CSVWriter(new FileWriter(filePath));
					List<String[]> records = new ArrayList<String[]>();
					String coulmnHeaders = "";

					for(int i = 0; i < NoOfcolumns; i ++) {
						coulmnHeaders += (!coulmnHeaders.isEmpty() ? "," : "") + offersAndDealsList.get(i);
					}
					// adding headers
					records.add(coulmnHeaders.split(","));
					

					for(AfPartnerOffer afPartnerOffers : afPartnerOfferSuccessSet) {
						maxCount++;
						records.add(new String[] { (String.valueOf(maxCount)), (afPartnerOffers.getAfPartner().getPartnerName()),
								(!afPartnerOffers.getAfCategory().getCategoryId().contains("-") ? afPartnerOffers.getAfCategory().getCategoryId()
										+ "-" : afPartnerOffers.getAfCategory().getCategoryId()), (afPartnerOffers.getOfferName()),
										(afPartnerOffers.getDescription()), (afPartnerOffers.getOfferLandingUrl()),
										(afPartnerOffers.getOfferLogoUrl()),
										(String.valueOf(afPartnerOffers.getOfferPosition() != null ? afPartnerOffers.getOfferPosition() : "")),
										(afPartnerOffers.getImageAltTxt()),
										(afPartnerOffers.getActivationDate() != null
										&& !afPartnerOffers.getActivationDate().equals("") ? ServiceProviderUtil.getInstance()
												.getFormattedDate(afPartnerOffers.getActivationDate(),"dd/MM/yyyy") : ""),
												(afPartnerOffers.getDeactivationDate() != null
												&& !afPartnerOffers.getDeactivationDate().equals("") ? ServiceProviderUtil.getInstance()
														.getFormattedDate(afPartnerOffers.getDeactivationDate(),"dd/MM/yyyy") : ""),
														((afPartnerOffers.getOfferStatus() == 1) ? "Active" : "Inactive") });
					}
					csvWriter.writeAll(records);
					csvWriter.close();
					model.put("status", +successCount+" rows uploaded successfully and "+failureCount+" rows failed to upload.");
				} else {
					model.put("status", "noFileUpdate");
				}
			} catch (Exception e) {
				e.printStackTrace();
				logger.info("@@@@Exception in CommonServiceImpl at cerateOffersAndDeals(-, -)"
						+ " outside for loop while creating CSV File:", e);
				model.put("status", "failure");
			}
		} catch (Exception e) {
			logger.error("@@@@Exception in CommonServiceImpl at cerateOffersAndDeals() :", e);
			model.put("status", "failure");
		}
	}
	@Override
	public List<SeoBean> getSEOData(){
		List<AfSeo> afSeo = new ArrayList<AfSeo>();
		List<SeoBean> seoBean = new ArrayList<SeoBean>();
		
		try{
			afSeo = commonDAO.getSEOData();
			
			if(afSeo != null){
				for(AfSeo afs : afSeo){
					SeoBean sb = new SeoBean();
					sb.setSeoPageId(afs.getSeoPageId());
					sb.setTitle(afs.getTitle());
					sb.setMetaDesc(afs.getMetaDesc());
					sb.setMetaTag(afs.getMetaTag());
					sb.setHeaderText(afs.getHeaderText());
					seoBean.add(sb);
				}
			}
		}
		catch(Exception e){
			logger.error("@@@@Exception in CommonServiceImpl at getSEOData() :", e);
		}
		
		return seoBean;
	}
	
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public String saveSEOData(SeoBean seoBean, HttpServletRequest request){
		String status = "failure";
		AfSeo afSeo = null;
		AfSeo afSeoOld = null;
		String replaceInOld="";
		String replaceInNew="";
		AfEnumValues enumList = new AfEnumValues();
		String seoValue="";
		try{
			
			afSeo = commonDAO.getSEOData(seoBean.getSeoPageId());
			afSeoOld=(AfSeo) afSeo.clone();
			if(null==afSeo || null==seoBean.getSeoPageId()){
				afSeo = new AfSeo();
				afSeo.setCreatedTime(new Date());
			}
			else{
				afSeo.setModifiedTime(new Date());
			}
			
			afSeo.setSeoPageId(seoBean.getSeoPageId());
			afSeo.setTitle(seoBean.getTitle());
			afSeo.setMetaDesc(seoBean.getMetaDesc());
			afSeo.setMetaTag(seoBean.getMetaTag());
			afSeo.setHeaderText(seoBean.getHeaderText());
			afSeo.setStatus((byte)1);
			afSeo.setCreatedBy(1);
			
			enumList = commonDAO.getEnumValues("SEO_PAGE_ID", String.valueOf(afSeo.getSeoPageId()));
			String getTitle = afSeo.getTitle().length() !=0?afSeo.getTitle():"NA";
			String getTitleOld = afSeoOld.getTitle().length() !=0?afSeoOld.getTitle():"NA";
			
			String getMetaDesc = afSeo.getMetaDesc().length() !=0?afSeo.getMetaDesc():"NA";
			String getMetaDescOld = afSeoOld.getMetaDesc().length() !=0?afSeoOld.getMetaDesc():"NA";
			
			String getMetaTag = afSeo.getMetaTag().length() !=0?afSeo.getMetaTag():"NA";
			String getMetaTagOld = afSeoOld.getMetaTag().length() !=0?afSeoOld.getMetaTag():"NA";
			
			String getHeaderText = afSeo.getHeaderText().length() !=0?afSeo.getHeaderText():"NA";
			String getHeaderTextOld = afSeoOld.getHeaderText().length() !=0?afSeoOld.getHeaderText():"NA";
			status = commonDAO.saveSEOData(afSeo);
			if("success".equalsIgnoreCase(status)){
				String ipAddress = request.getHeader("X-FORWARDED-FOR");
				if (ipAddress == null) {
					ipAddress = request.getRemoteAddr();
				}
				
				AfUserActivityLog updateUsActLog = new AfUserActivityLog();
				Map<String,String> objectCom = new LinkedHashMap<>();
				try{
					
					updateUsActLog.setUserActivity("SEO Update :"+enumList.getValue());
					updateUsActLog.setUserActCreatedDateTime(new Date());
					updateUsActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
					updateUsActLog.setUserActIpVal(ipAddress);
					updateUsActLog.setUserActModule("SEO");
					if(!getTitle.equalsIgnoreCase(getTitleOld)){
						replaceInOld=replaceInOld+"|Recommended Title:"+getTitleOld;
						replaceInNew=replaceInNew+"|Recommended Title:"+getTitle;
					}
					
					if(!getMetaDesc.equalsIgnoreCase(getMetaDescOld)){
						replaceInOld=replaceInOld+"|Recommended Meta Description:"+getMetaDescOld;
						replaceInNew=replaceInNew+"|Recommended Meta Description:"+getMetaDesc;
					}
					if(!getMetaTag.equalsIgnoreCase(getMetaTagOld)){
						replaceInOld=replaceInOld+"|Recommended Meta Tags:"+getMetaTagOld;
						replaceInNew=replaceInNew+"|Recommended Meta Tags:"+getMetaTag;
					}
					if(!getHeaderText.equalsIgnoreCase(getHeaderTextOld)){
						replaceInOld=replaceInOld+"|Recommended H1 Tags:"+getHeaderTextOld;
						replaceInNew=replaceInNew+"|Recommended H1 Tags:"+getHeaderText;
					}
					if(replaceInNew.isEmpty() && replaceInOld.isEmpty()) {
                        replaceInOld=replaceInOld+"|ModifiedTime:"+simpleDateFormat.format(afSeoOld.getModifiedTime());
                        replaceInNew=replaceInNew+"|ModifiedTime:"+simpleDateFormat.format(afSeo.getModifiedTime());
                    }
					
					updateUsActLog.setUserNewFieldValue(replaceInNew);
		    		updateUsActLog.setUserOldFieldValue(replaceInOld);
					userActLogDao.updateUserLog(updateUsActLog);
					
					
						    
				}
				catch(Exception e){
					logger.error("@@@@Exception in CommonServiceImpl at saveSEOData() :", e);
				}
			}
		}
		catch(Exception e){
			logger.error("@@@@Exception in CommonServiceImpl at saveSEOData() :", e);
		}
		
		return status;
	}

	@Override
	public String clearCache(){
		StringBuilder response = new StringBuilder();
		try{
			logger.info("*****serverCacheUrl: " + serverCacheUrl);
			String[] urlArray = serverCacheUrl.split(",");
			
			for(int i = 0; i < urlArray.length; i++){
				if(i == urlArray.length-1){
					response.append(urlArray[i] + ":	" + sendGetRequest.sendGet(urlArray[i]));
				}
				else{
					response.append(urlArray[i] + ":	" + sendGetRequest.sendGet(urlArray[i]) + "<br/>");
				}
			}
		}
		catch(Exception e){
			logger.error("@@@@ Exception in CommonServiceImpl clearCache(): ", e);
		}
		return response.toString();
	}

	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public AfEnumValues getEnumValuebyType(String TypeName) {
		AfEnumValues enamVal=null; 
		
		try{
			enamVal = commonDAO.getEnumValuebyType(TypeName);
		}
		catch(Exception e){
			logger.error("@@@@ Exception in CommonServiceImpl at getEnumValuebyType() :", e);
		}
		
		return enamVal;
	}

	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String saveSellerText(String value,HttpServletRequest request) {
		String status="failure";
		String replaceInOld="";
		String replaceInNew="";
		AfEnumValues enamVal=null;
		try{
			enamVal = getEnumValuebyType("SELLER_TEXT");
			status = commonDAO.saveSellerText(value);
			
			replaceInNew=value;
			
			if("success".equalsIgnoreCase(status)){
				replaceInOld =  enamVal.getValue();
				
				String ipAddress = request.getHeader("X-FORWARDED-FOR");
				if (ipAddress == null) {
					ipAddress = request.getRemoteAddr();
				}
				AfUserActivityLog updateUsActLog = new AfUserActivityLog();
				try {
					updateUsActLog.setUserActivity("Update Seller Text");
					updateUsActLog.setUserActCreatedDateTime(new Date());
					updateUsActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
					updateUsActLog.setUserActIpVal(ipAddress);
					updateUsActLog.setUserActModule("Seller Text");
					updateUsActLog.setUserNewFieldValue(replaceInNew);
		    		updateUsActLog.setUserOldFieldValue(replaceInOld);
		    		if(!replaceInNew.equalsIgnoreCase(replaceInOld)){
					userActLogDao.updateUserLog(updateUsActLog);
					}
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
		}
		catch(Exception e){
			logger.error("@@@@ Exception in CommonServiceImpl at saveSellerText() :", e);
		}
		
		return status;
	}
	
	@Override
	public Map<Integer, String> getBrandList( String rpName) {
		Map<Integer, String> ppList = new HashMap<Integer, String>();	
		List<AfPartnerProduct> afppList = new ArrayList<AfPartnerProduct>();
		
		long startime=System.currentTimeMillis(),endtime=System.currentTimeMillis();
		String response="";
		
		startime=System.currentTimeMillis();
		response = services.brandSearchInfoService(rpName);			
		endtime=System.currentTimeMillis();
		logger.info("getPartnerProductList   while gettring response -----------start-time----"+startime+" ms");
		logger.info("getPartnerProductList   while gettring response -----------end-time----"+endtime+" ms");
		logger.info("getPartnerProductList   while gettring response -----------time-diff in Milisec----"+(endtime-startime)+" ms");
		logger.info("getPartnerProductList   while gettring response -----------time-diff in sec----"+((endtime-startime)/1000)+" sec");
		startime=System.currentTimeMillis();
		if(null!=response && !response.isEmpty() && response.length()>0)
			ppList = commonUtil.getBrandSearchInfoList(response);
		endtime=System.currentTimeMillis();
		logger.info("getPartnerProductList   while parsing response -----------start-time----"+startime+" ms");
		logger.info("getPartnerProductList   while parsing response -----------end-time----"+endtime+" ms");
		logger.info("getPartnerProductList   while parsing response -----------time-diff in Milisec----"+(endtime-startime)+" ms");
		logger.info("getPartnerProductList   while parsing response -----------time-diff in sec----"+((endtime-startime)/1000)+" sec");
		
		return ppList;
	}
}