package com.af.admin.service.impl;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.af.admin.DAO.CategoryManagementDAO;
import com.af.admin.DAO.CommonDAO;
import com.af.admin.DAO.OffersDealsDAO;
import com.af.admin.DAO.PartnerCategoryDAO;
import com.af.admin.DAO.PartnerManagementDAO;
import com.af.admin.DAO.PartnerProductDAO;
import com.af.admin.DAO.SectionManagementDAO;
import com.af.admin.DAO.UserActivityLogDAO;
import com.af.admin.beans.HomePageSectionBean;
import com.af.admin.beans.SectionBean;
import com.af.admin.beans.SectionItemsBean;
import com.af.admin.constants.AfConstants;
import com.af.admin.entity.AfCategory;
import com.af.admin.entity.AfEnumValues;
import com.af.admin.entity.AfHomePageSection;
import com.af.admin.entity.AfPartner;
import com.af.admin.entity.AfPartnerCategory;
import com.af.admin.entity.AfPartnerOffer;
import com.af.admin.entity.AfPartnerProduct;
import com.af.admin.entity.AfProduct;
import com.af.admin.entity.AfSectionItems;
import com.af.admin.entity.AfSections;
import com.af.admin.entity.AfUserActivityLog;
import com.af.admin.service.SectionManagementService;
import com.af.admin.utils.AdminUserActivityUtil;
import com.af.admin.utils.ServiceProviderUtil;


@Component
@Transactional(readOnly=true)
public class SectionManagementServiceImpl implements SectionManagementService {
	
	private Logger logger = LogManager.getLogger(SectionManagementServiceImpl.class.getName());
	
	@Autowired
	CommonDAO commonDAO;
	
	@Autowired
	SectionManagementDAO sectionManagementDAO;
	
	@Autowired
	PartnerManagementDAO partnermanagementDAO;
	
	@Autowired
	OffersDealsDAO offersDealsDAO;
	
	@Autowired
	PartnerProductDAO partnerProductDAO;
	
	@Autowired
	CategoryManagementDAO categoryManagementDAO;
	
	@Autowired
	PartnerCategoryDAO partnerCategoryDAO;
	
	@Autowired
	AdminUserActivityUtil adminUserActivityUtil;
	
	@Autowired
	UserActivityLogDAO userActLogDao;
	
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
	@Override
	public List<SectionBean> getSections() {
		List<AfSections> afSectionList=null;
		List<SectionBean> sectionList=new ArrayList<SectionBean>();
		String dateStr="";
		Date dt=null; 
		byte sectionStatus;
		try{
			afSectionList=sectionManagementDAO.getSections();
			for(AfSections afSection:afSectionList){
				SectionBean sectionBean=new SectionBean();
				sectionBean.setSectionId(afSection.getSectId());
				sectionBean.setSectionTitle(afSection.getSectionTitle());
				sectionBean.setDisplayOrder(afSection.getSectionDispOrder());
				sectionBean.setSectionStatusCode(afSection.getSectionStatus());
				sectionStatus=afSection.getSectionStatus();
				if(1==sectionStatus){
					sectionBean.setStatus(AfConstants.STATUS_ACTIVE);
					sectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_GREEN);

				}
				if(0==sectionStatus){
					sectionBean.setStatus(AfConstants.STATUS_IN_ACTIVE);
					sectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_RED);
				}
				dt=afSection.getSectionStartDate()!=null?afSection.getSectionStartDate():new Date();
				dateStr=ServiceProviderUtil.getInstance().getFormattedDate(afSection.getSectionStartDate(),"dd/MM/yyyy");
				if(dt!=null && !dt.equals(""))
					sectionBean.setActivationDate(dateStr);
				
				dt=afSection.getSectionEndDate()!=null?afSection.getSectionEndDate():new Date();
				dateStr=ServiceProviderUtil.getInstance().getFormattedDate(afSection.getSectionEndDate(),"dd/MM/yyyy");
				if(dt!=null && !dt.equals(""))
					sectionBean.setDeactivationDate(dateStr);
				 
				sectionList.add(sectionBean);
			}
		}catch(Exception e){
			logger.error("@@@@ Exception in SectionManagementServiceImpl getSections() :",e);
		}
		return sectionList;
	}
	
	@Override
	public List<SectionBean> getSections(String sectionTitle, int sectionType) {
		List<AfSections> afSectionList=null;
		List<SectionBean> sectionList=new ArrayList<SectionBean>();
		String dateVal="", dateStr="", dateEnd="", currentDt="";
		Date dt1=null, dt2=null, stDt=null, enDt=null, curDt=null; 
		Date dt = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		int sectionStatus;
		try{
			
			afSectionList=sectionManagementDAO.getSections(sectionTitle, sectionType);
			for(AfSections afSection:afSectionList){
				SectionBean sectionBean=new SectionBean();
				sectionBean.setSectionId(afSection.getSectId());
				sectionBean.setSectionTitle(afSection.getSectionTitle());
				sectionBean.setDisplayOrder(afSection.getSectionDispOrder());
				sectionBean.setSectionStatusCode(afSection.getSectionStatus());
				sectionBean.setFooterDesc(afSection.getFooterDescription());
				sectionBean.setFooterImageUrl1(afSection.getFooterImage1());
				sectionBean.setFooterImageUrl2(afSection.getFooterImage2());
				String footerDesc=afSection.getFooterDescription();
				
				if(footerDesc != null){
					footerDesc = URLEncoder.encode(footerDesc, "UTF-8");
				}
				sectionBean.setFooterDesc(footerDesc);
				if(null!=afSection.getViewAllLink()) {
				sectionBean.setViewLink(afSection.getViewAllLink());
				}
				sectionBean.setRedirectionPage(afSection.getRedirectionPage());
				sectionStatus=afSection.getSectionStatus();
				
				dt1=afSection.getSectionStartDate()!=null?afSection.getSectionStartDate():new Date();
				dateStr=ServiceProviderUtil.getInstance().getFormattedDate(afSection.getSectionStartDate(),"dd/MM/yyyy");
				if(dt1!=null && !dt1.equals(""))
					sectionBean.setActivationDate(dateStr);
				
				dt2=afSection.getSectionEndDate()!=null?afSection.getSectionEndDate():new Date();
				dateVal=ServiceProviderUtil.getInstance().getFormattedDate(afSection.getSectionEndDate(),"dd/MM/yyyy");
				if(dt2!=null && !dt2.equals(""))
					sectionBean.setDeactivationDate(dateVal);
				
				dateStr = ServiceProviderUtil.getInstance().getFormattedDate(afSection.getSectionStartDate(),"dd/MM/yyyy");
				stDt = dateFormat.parse(dateStr);
				currentDt = ServiceProviderUtil.getInstance().getFormattedDate(dt,"dd/MM/yyyy");
				curDt = dateFormat.parse(currentDt);
				 
					dateEnd = ServiceProviderUtil.getInstance().getFormattedDate(afSection.getSectionEndDate(),"dd/MM/yyyy");
					if(!dateEnd.equals("")){
					
					enDt = dateFormat.parse(dateEnd);
				}
				
					if(1==sectionStatus){
						sectionBean.setStatus(AfConstants.STATUS_ACTIVE);
						sectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_GREEN);
						
					}
					if(0==sectionStatus){
						sectionBean.setStatus(AfConstants.STATUS_IN_ACTIVE);
						sectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_RED);
						
					}
					
					if(dateEnd.equals("")){	
						
						if(curDt.before(stDt)){
							sectionBean.setStatus(AfConstants.STATUS_IN_ACTIVE);
							sectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_RED);
						}else if(curDt.equals(stDt) ){
							if(1==sectionStatus){
							 sectionBean.setStatus(AfConstants.STATUS_ACTIVE);
							 sectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_GREEN);
							}else{
								sectionBean.setStatus(AfConstants.STATUS_IN_ACTIVE);
								sectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_RED);
							}
						}else if(curDt.after(stDt)){
							if(1==sectionStatus){
								 sectionBean.setStatus(AfConstants.STATUS_ACTIVE);
								 sectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_GREEN);
								}else{
									sectionBean.setStatus(AfConstants.STATUS_IN_ACTIVE);
									sectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_RED);
								}
						}
					
					}else{
							
							if(curDt.after(stDt) && enDt.after(curDt)){
								if(1==sectionStatus){
									sectionBean.setStatus(AfConstants.STATUS_ACTIVE);
									sectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_GREEN);
								}else{
									sectionBean.setStatus(AfConstants.STATUS_IN_ACTIVE);
									sectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_RED);
								}
							}else if(curDt.equals(stDt) && curDt.equals(enDt)){
								if(1==sectionStatus){
									sectionBean.setStatus(AfConstants.STATUS_ACTIVE);
									sectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_GREEN);
								}else{
									sectionBean.setStatus(AfConstants.STATUS_IN_ACTIVE);
									sectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_RED);
								}	
							}else if(curDt.after(stDt) && enDt.before(curDt)){
									sectionBean.setStatus(AfConstants.STATUS_IN_ACTIVE);
									sectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_RED);
							}else if(curDt.equals(stDt) && enDt.after(curDt)){
								if(1==sectionStatus){
									sectionBean.setStatus(AfConstants.STATUS_ACTIVE);
									sectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_GREEN);
								}else{
									sectionBean.setStatus(AfConstants.STATUS_IN_ACTIVE);
									sectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_RED);
								}	
							}else if(stDt.after(curDt) && stDt.equals(enDt)){
									sectionBean.setStatus(AfConstants.STATUS_IN_ACTIVE);
									sectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_RED);
							}else if(stDt.before(curDt) && enDt.before(curDt)){
								if(1==sectionStatus){
									sectionBean.setStatus(AfConstants.STATUS_ACTIVE);
									sectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_GREEN);
								}else{
									sectionBean.setStatus(AfConstants.STATUS_IN_ACTIVE);
									sectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_RED);
								}	
							}
							
						} 
				 
				sectionList.add(sectionBean);
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in SectionManagementServiceImpl getSections() :",e);
		}
		return sectionList;
	}
	
	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String updateHomePageSection(JSONObject homePageSectionJson,HttpServletRequest request) {
		Date dt=null;
		DateFormat sourceDf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		String status="failure",activationDate="",deactivationDate="",sectionId="",status_="failure", sectionName="";
		AfSections afsection = null;
		List<AfSections> afsectionlist = null;
		String replaceInNew = "";
		String replaceInOld="";
		AfSections afsectionOld=null;
		try{ 
			afsection=sectionManagementDAO.getHomePageSection(Integer.parseInt(homePageSectionJson.getString("sectionId")));
			
			sectionId=String.valueOf(afsection.getSectId());
			sectionName = afsection.getSectionTitle();
			afsectionOld=(AfSections)afsection.clone();
			if(sectionId!=homePageSectionJson.getString("sectionId"))
				afsection.setSectionTitle(homePageSectionJson.getString("sectionId"));
				afsection.setSectionTitle(homePageSectionJson.getString("sectionTitle"));
				afsection.setSectionDispOrder((byte)Integer.parseInt(homePageSectionJson.getString("displayOrder")));
				afsection.setSectionStatus((byte)Integer.parseInt(homePageSectionJson.getString("sectionStatus")));			
				afsection.setFooterDescription(homePageSectionJson.getString("footerDesc"));
				afsection.setFooterImage1(homePageSectionJson.getString("footerImageUrl1"));
				afsection.setFooterImage2(homePageSectionJson.getString("footerImageUrl2"));
				afsection.setViewAllLink((byte)Integer.parseInt(homePageSectionJson.getString("viewLink")));
				afsection.setRedirectionPage(homePageSectionJson.getString("redirectionPage"));
			activationDate=homePageSectionJson.getString("sectionStartDate");
			
			if(activationDate.length()>0 && activationDate!=null){
				Date dt1=new Date();
				String hh_mi_ss=""+dt1;
				String format[]=hh_mi_ss.split(" ");
				hh_mi_ss=format[3];
				activationDate=activationDate+" "+hh_mi_ss;
				dt=sourceDf.parse(activationDate);
				afsection.setSectionStartDate(dt);
			}
			
			deactivationDate=homePageSectionJson.getString("sectionEndDate");			
			if(deactivationDate.length()>0 && deactivationDate!=null){
				Date dt1=new Date();
				String hh_mi_ss=""+dt1;
				String format[]=hh_mi_ss.split(" ");
				hh_mi_ss=format[3];
				deactivationDate=deactivationDate+" "+hh_mi_ss;
				dt=sourceDf.parse(deactivationDate);
				afsection.setSectionEndDate(dt);
			}
			else{
				afsection.setSectionEndDate(null);
			}
			afsection.setModifiedTime(new Date());
			afsection.setModifiedBy(1);
			
			
			status=sectionManagementDAO.updateSection(afsection);
			
			
			if(!status.equalsIgnoreCase("failure")){
				status_= sectionManagementDAO.updateHomePageSectionByName(homePageSectionJson.getString("sectionTitle"), (byte)Integer.parseInt(homePageSectionJson.getString("sectionStatus")), sectionName, (byte)Integer.parseInt(homePageSectionJson.getString("displayOrder")));
			}
			if("success".equalsIgnoreCase(status)){
				String ipAddress = request.getHeader("X-FORWARDED-FOR");
				if (ipAddress == null) {
					ipAddress = request.getRemoteAddr();
				}
				AfUserActivityLog updateUsActLog = new AfUserActivityLog();
				Map<String,String> objectCom = new LinkedHashMap<>();
				try{
					updateUsActLog.setUserActivity("Update Home Page Sections Listing  ");
					updateUsActLog.setUserActCreatedDateTime(new Date());
					updateUsActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
					updateUsActLog.setUserActIpVal(ipAddress);
					updateUsActLog.setUserActModule("Home page Custom section");
					objectCom = adminUserActivityUtil.objectComparision(afsection, afsectionOld);
					replaceInOld=objectCom.get(AfConstants.ACTIVITY_OLD_KEY).toString().replace(":0 |",":InActive |").replace(":1 |", ":Active |");
		    		replaceInNew=objectCom.get(AfConstants.ACTIVITY_NEW_KEY).toString().replace(":0 |",":InActive |").replace(":1 |", ":Active |");
		    		if(replaceInNew.isEmpty() && replaceInOld.isEmpty()) {
                        replaceInOld=replaceInOld+"|ModifiedTime:"+simpleDateFormat.format(afsection.getModifiedTime());
                        replaceInNew=replaceInNew+"|ModifiedTime:"+simpleDateFormat.format(afsection.getModifiedTime());
                    }
		    		if(afsection.getViewAllLink()==0) {
		    			replaceInOld=replaceInOld.replace("redirectionPage", "");
		    		}
		    		if(afsectionOld.getViewAllLink()==0) {
		    			replaceInNew=replaceInNew.replace("redirectionPage", "");
		    		}
					updateUsActLog.setUserNewFieldValue(replaceInOld);
		    		updateUsActLog.setUserOldFieldValue(replaceInNew);
					userActLogDao.updateUserLog(updateUsActLog);
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
		}
		catch(Exception e){
			logger.error("@@@@ Exception in SectionManagementServiceImpl updateHomePageSection() :",e);
		}
		
		return status_;
	}
	
	@Override
	public boolean checksecTitle(String secTitle) {
		boolean secTitleExist=false;
		try{
			secTitleExist=sectionManagementDAO.checkforSecTitle(secTitle);
			logger.debug(">>>>>>>>checkPartnerID service>>>"+secTitleExist);
		}catch(Exception e){
			logger.error("@@Exception in SectionManagementServiceImpl checksecTitle", e);
		}
		return secTitleExist;
	}
	
	@Override
	public boolean checkDisplayOrder(String dipOrder) {
		boolean checkFlag=false;
		List<AfSections> afSections;
		try{
			afSections=sectionManagementDAO.checkDisOrder(dipOrder);
			if(afSections!=null && afSections.size()>0){
				checkFlag=true;
			}
		}catch(Exception e){
			logger.error("@@Exception in SectionManagementServiceImpl checkLogoPrtDisplayOrder():", e);
		}
		return checkFlag;
	}

	@Override
	public boolean checkSectionItemDisplayOrder(String displayOrder,String sectionId){
		boolean checkFlag=false;
		List<AfSectionItems> afSectionItems;
		try{
			afSectionItems=sectionManagementDAO.checkSectionItemDisplayOrder(displayOrder,sectionId);
			if(afSectionItems!=null && afSectionItems.size()>0){
				checkFlag=true;
			}
		}catch(Exception e){
			logger.error("@@Exception in SectionManagementServiceImpl checkSectionItemDisplayorder():", e);
		}
		return checkFlag;
	}
	
	@Override
	public boolean checkDuplicateRecord(String sectionId, String itemType, String prtNo, String catNo, String prtPrdctNo, String prtOfferNo,String brandName){
		boolean checkFlag=false;
		List<AfSectionItems> afSectionItems;
		
		try{
			afSectionItems=sectionManagementDAO.checkDuplicateRecord(sectionId, itemType, prtNo, catNo, prtPrdctNo, prtOfferNo,brandName);
			
			if(afSectionItems!=null && afSectionItems.size()>0){
				checkFlag=true;
			}
		}
		catch(Exception e){
			logger.error("@@Exception in SectionManagementServiceImpl checkDuplicateRecord():", e);
		}
		
		return checkFlag;
	}
	
	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String createSectionItems(SectionItemsBean sectionItemsBean,HttpServletRequest request){
		String status = "failure";
		AfSectionItems afSectionItems = new AfSectionItems();
		AfSections afSections = null;
		AfPartner afPartner = null;
		AfPartnerOffer afPartnerOffer = null;
		AfPartnerProduct afPartnerProduct = null;
		AfCategory afCategory = null;
		AfPartnerCategory afPartnerCategory = null;
		DateFormat sourceDf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		String hh_mi_ss=""+new Date();
		String startDate = "", endDate = "";
		Date dt=null;
		String format[]=hh_mi_ss.split(" ");
		hh_mi_ss=format[3];	
		String oldOrNewVal = "";
		String commLogStatus = "";
		List<AfEnumValues> enumList = new ArrayList<AfEnumValues>();
		
		try{
			if(sectionItemsBean.getSectionId()!=null && !sectionItemsBean.getSectionId().isEmpty()){
				afSections = sectionManagementDAO.getHomePageSection(Integer.parseInt(sectionItemsBean.getSectionId()));
			}
			System.out.println("sectionItemsBean.getPrtNo() == "+sectionItemsBean.getPrtNo());
			if(sectionItemsBean.getPrtNo()!=null && !sectionItemsBean.getPrtNo().isEmpty()){
				afPartner = partnermanagementDAO.getPartnerByPartnerNO(sectionItemsBean.getPrtNo());
			}
			
			if(sectionItemsBean.getPrtOfferNo()!=null && !sectionItemsBean.getPrtOfferNo().isEmpty()){
				afPartnerOffer = offersDealsDAO.getPartnerOfferByNO(sectionItemsBean.getPrtOfferNo());
			}
			
			if(sectionItemsBean.getPrtPrdctNo()!=null && !sectionItemsBean.getPrtPrdctNo().isEmpty()){
				afPartnerProduct = partnerProductDAO.getPartnerProductByPrtPrdNO(sectionItemsBean.getPrtPrdctNo());
			}
			
			if(sectionItemsBean.getCatNo()!=null && !sectionItemsBean.getCatNo().isEmpty()){
				afCategory = categoryManagementDAO.getCategoryByCategoryNo(sectionItemsBean.getCatNo());
			}
			
			if(sectionItemsBean.getPrtCatNo()!=null && !sectionItemsBean.getPrtCatNo().isEmpty()){
				afPartnerCategory = partnerCategoryDAO.getPartnerCategoryByPrtCatNO(sectionItemsBean.getPrtCatNo());
			}
			
			afSectionItems.setAfSections(afSections);
			if(afPartner!=null){
				afSectionItems.setAfPartner(afPartner);
			}
			afSectionItems.setAfPartnerOffer(afPartnerOffer);
			afSectionItems.setAfPartnerProduct(afPartnerProduct);
			afSectionItems.setAfCategory(afCategory);
			afSectionItems.setAfPartnerCategory(afPartnerCategory);
			System.out.println("itemtype== "+sectionItemsBean.getItemType());
			if(sectionItemsBean.getItemType()!=null && !sectionItemsBean.getItemType().isEmpty()){
			afSectionItems.setItemType((sectionItemsBean.getItemType() != null)?Byte.parseByte(sectionItemsBean.getItemType()):null);
			}
			if(sectionItemsBean.getDisplayOrder() != null) {
			afSectionItems.setDisplayOrder(Integer.parseInt(sectionItemsBean.getDisplayOrder()));
			}
			afSectionItems.setSectionItemStatus(sectionItemsBean.getSectionItemStatus());
			if(sectionItemsBean.getDisplayPrtPopup() != null) {
			afSectionItems.setDisplayPrtPopup(Byte.parseByte(sectionItemsBean.getDisplayPrtPopup()));
			}
			startDate= sectionItemsBean.getStartDate();
			if(startDate!=null && !startDate.isEmpty()){
			startDate=startDate+" "+hh_mi_ss;
			
			if(startDate.length()>0){
				dt=sourceDf.parse(startDate);
			}
			}
			afSectionItems.setStartDate(dt);
			
			dt = null;
			endDate=sectionItemsBean.getEndDate();
			if(endDate!=null && !endDate.isEmpty()) {
			endDate=endDate+" "+hh_mi_ss;
			if(endDate.length()>0){
				dt=sourceDf.parse(endDate);
			}
			}
			if(dt!=null ){
			afSectionItems.setEndDate(dt);
			}
			afSectionItems.setStatus((byte)1);
			afSectionItems.setCreatedTime(new Date());
			afSectionItems.setCreatedBy(1);
			//
			afSectionItems.setBrandName(sectionItemsBean.getBrandName());
			afSectionItems.setBrandDescription(sectionItemsBean.getBrandDescription());
			afSectionItems.setBrandImageUrl(sectionItemsBean.getBrandImageUrl());
			enumList = sectionManagementDAO.getEnumValues("ITEM_TYPE", String.valueOf(afSectionItems.getItemType()));
			for(AfEnumValues enumValue:enumList){
			  sectionItemsBean.setTypeName(enumValue.getValue());
			}
			String status1=sectionItemsBean.getSectionItemStatus()+"";
			status1 = status1.replaceAll("0","InActive");
			status1 = status1.replaceAll("1", "Active");
			
			afSectionItems.setLandingUrl(sectionItemsBean.getLandingUrl());
			afSectionItems.setTitle(sectionItemsBean.getTitle());
			afSectionItems.setBrandImageUrl(sectionItemsBean.getBrandImageUrlCustom());
			afSectionItems.setBrandDescription(sectionItemsBean.getCustomDescription());
			status = sectionManagementDAO.createSectionItems(afSectionItems);
			/*if("success".equalsIgnoreCase(status)){
				try{
					
					String ipAddress = request.getHeader("X-FORWARDED-FOR");
					if(ipAddress == null) {
						ipAddress = request.getRemoteAddr();
					}
					ipAddress = (null != ipAddress) ? ipAddress.trim() : "";
					AfUserActivityLog afPrtActLog=new AfUserActivityLog();
					if(afSectionItems.getItemType()==1){
					oldOrNewVal="Select A Section: "+afSectionItems.getAfSections().getSectionTitle()+" | "+"Item Type : "+sectionItemsBean.getTypeName()+" | "+
							""+"Select A Partner: "+afSectionItems.getAfPartner().getPartnerName()+"| "+"Partner Category: "+afSectionItems.getAfPartnerCategory().getPartnerCategoryName()+" |"
							+ ""+" Select Product : "+afSectionItems.getAfPartnerProduct().getProductName()+" |"+"Section Item Status : "+status1+" |"+"Display Order : "+afSectionItems.getDisplayOrder()+" |"
						    + ""+"Start Date : "+afSectionItems.getStartDate()+" | "+"End Date: "+afSectionItems.getEndDate()+" | ";
					}
					if(afSectionItems.getItemType()==2){
						oldOrNewVal="Select A Section: "+afSectionItems.getAfSections().getSectionTitle()+" | "+"Item Type : "+sectionItemsBean.getTypeName()+" |"
								+ ""+"Select Category: "+afCategory.getCategoryName()+" |"
								+ ""+"Section Item Status : "+status1+" |"+"Display Order : "+afSectionItems.getDisplayOrder()+" |"
							    + ""+"Start Date : "+afSectionItems.getStartDate()+" | "+"End Date: "+afSectionItems.getEndDate()+" | "; 
								
					}
					if(afSectionItems.getItemType()==3){
						oldOrNewVal="Select A Section: "+afSectionItems.getAfSections().getSectionTitle()+" | "+"Item Type : "+sectionItemsBean.getTypeName()+" | "+
								""+"Select A Partner: "+afPartner.getPartnerName()+" |"+"Section Item Status : "+status1+" |"+"Display Order : "+afSectionItems.getDisplayOrder()+" |"
							    + ""+"Start Date : "+afSectionItems.getStartDate()+" | "+"End Date: "+afSectionItems.getEndDate()+" |";
								
						}
					if(afSectionItems.getItemType()==4){
						oldOrNewVal="Select A Section: "+afSectionItems.getAfSections().getSectionTitle()+" | "+"Item Type : "+sectionItemsBean.getTypeName()+" | "+
								""+"Select A Partner: "+afPartner.getPartnerName()+"|"+"Select Offers: "+afPartnerOffer.getOfferName()+" |"
										+ ""+"Select Category: "+afSectionItems.getAfCategory().getCategoryName()+"|"
								+""+"Section Item Status : "+status1+" |"+"Display Order : "+afSectionItems.getDisplayOrder()+" |"
							    + ""+"Start Date : "+afSectionItems.getStartDate()+" | "+"End Date: "+afSectionItems.getEndDate()+" | ";
								
				
									
						}
					if(afSectionItems.getItemType()==5){
						oldOrNewVal="Select A Section: "+afSectionItems.getAfSections().getSectionTitle()+" | "+"Item Type : "+sectionItemsBean.getTypeName()+" |"
						+ ""+"Select Brand : "+afSectionItems.getBrandName()+"|"+"Brand Image: "+afSectionItems.getBrandImageUrl()+" |"+"Description: "+afSectionItems.getBrandDescription()+" |"
							+ ""+"Section Item Status : "+status1+"|"
						+ ""+"Display Order : "+afSectionItems.getDisplayOrder()+" |"+"Start Date : "+afSectionItems.getStartDate()+" | "
							+ ""+"End Date: "+afSectionItems.getEndDate()+" |";
												
						}
					if(afSectionItems.getItemType()==6){
						oldOrNewVal="Select A Section: "+afSectionItems.getAfSections().getSectionTitle()+" | "+"Item Type : "+sectionItemsBean.getTypeName()+" |"
						+ ""+"Select A Partner: "+afPartner.getPartnerName()+"|"+"Custom Page Image: "+afSectionItems.getBrandImageUrl()+" |"+"Custom Item Text: "+afSectionItems.getBrandDescription()+" |"
							+ ""+"Section Item Status : "+status1+"|"+"Display Order : "+afSectionItems.getDisplayOrder()+" |"+"Start Date : "+afSectionItems.getStartDate()+" |"+"End Date: "+afSectionItems.getEndDate()+" |"
									+ ""+"Custom Item Title: "+afSectionItems.getTitle()+" |"+"Custom Item Landing URL: "+afSectionItems.getLandingUrl()+" ";
												
						}
					
							afPrtActLog.setUserNewFieldValue(oldOrNewVal);
							afPrtActLog.setUserOldFieldValue("NA");
							afPrtActLog.setUserActivity("Create Home page Section");
							afPrtActLog.setUserActCreatedDateTime(new Date());
							afPrtActLog.setUserActIpVal(ipAddress);
							afPrtActLog.setUserActModule("Home page Custom Section");
							afPrtActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
							
							commLogStatus = userActLogDao.createUserActLog(afPrtActLog);
					
				}
				catch(Exception e){
					e.printStackTrace();
					logger.error("@@Exception in SectionManagementServiceImpl createSectionItems():", e);
				}
			}*/
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error("@@Exception in SectionManagementServiceImpl createSectionItems():", e);
		}
		
		return status;
	}
	
	@Override
	public List<SectionItemsBean> getSectionItems(String sectionId) {
		List<AfSectionItems> afSectionItemList=null;
		List<SectionItemsBean> sectionItemList=new ArrayList<SectionItemsBean>();
		List<AfEnumValues> enumList = new ArrayList<AfEnumValues>();
		AfSections afSections = null;
		int sectionItemStatus;
		String dateVal="", dateStr="", dateEnd="", currentDt="";
		Date dt1=null, dt2=null, stDt=null, enDt=null, curDt=null;
		Date dt = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		int sectionStatus;
		
		try{
			afSectionItemList=sectionManagementDAO.getSectionItems(sectionId);

			for(AfSectionItems afSectionItem:afSectionItemList){
				SectionItemsBean sectionItemsBean=new SectionItemsBean();
				sectionItemsBean.setSectionId(String.valueOf(afSectionItem.getAfSections().getSectId()));
				afSections = sectionManagementDAO.getHomePageSection(Integer.parseInt(sectionItemsBean.getSectionId()));
				sectionItemsBean.setSectionTitle(afSections.getSectionTitle());
				sectionItemsBean.setItemId(String.valueOf(afSectionItem.getItemId()));
				sectionItemsBean.setDisplayPrtPopup(afSectionItem.getDisplayPrtPopup()+"");
				enumList = sectionManagementDAO.getEnumValues("ITEM_TYPE", String.valueOf(afSectionItem.getItemType()));
			
				if(afSectionItem!=null){
					sectionItemsBean.setSectionId(afSectionItem.getAfSections().getSectId()+"");
					String itemType = afSectionItem.getItemType()+"";
					sectionItemsBean.setItemType(itemType);
					
					if(afSectionItem.getAfPartner()!=null){
						sectionItemsBean.setPrtNo(afSectionItem.getAfPartner().getPrtNo()+"");
					}
					
					if(afSectionItem.getAfCategory()!=null){
						sectionItemsBean.setCatNo(afSectionItem.getAfCategory().getCatNo()+"");
						sectionItemsBean.setCatName(afSectionItem.getAfCategory().getCategoryName()+"");
						
					}
					
					if(afSectionItem.getAfPartnerCategory() != null){
						sectionItemsBean.setPrtCatNo(afSectionItem.getAfPartnerCategory().getPrtCatNo()+"");
						sectionItemsBean.setPrtCatName(afSectionItem.getAfPartnerCategory().getPartnerCategoryName());
					}
					
					if(afSectionItem.getAfPartnerProduct()!=null){
						sectionItemsBean.setPrtPrdctNo(afSectionItem.getAfPartnerProduct().getPrtPrdNo()+"");
						sectionItemsBean.setProductName(afSectionItem.getAfPartnerProduct().getProductName()+"");
					}
					
					if(afSectionItem.getAfPartnerOffer()!=null){
						sectionItemsBean.setPrtOfferNo(afSectionItem.getAfPartnerOffer().getPrtOffrNo()+"");
						sectionItemsBean.setPrtOfferName(afSectionItem.getAfPartnerOffer().getOfferName()+"");
					}
					sectionItemsBean.setBrandNameHidden(afSectionItem.getBrandName());
					dt1=afSectionItem.getStartDate()!=null?afSectionItem.getStartDate():new Date();
					dateStr=ServiceProviderUtil.getInstance().getFormattedDate(afSectionItem.getStartDate(),"dd/MM/yyyy");
					if(dt1!=null && !dt1.equals(""))
						sectionItemsBean.setStartDate(dateStr);
					
					dt2=afSectionItem.getEndDate()!=null?afSectionItem.getEndDate():new Date();
					dateVal=ServiceProviderUtil.getInstance().getFormattedDate(afSectionItem.getEndDate(),"dd/MM/yyyy");
					if(dt2!=null && !dt2.equals("")){
						sectionItemsBean.setEndDate(dateVal);
					}
					
					for(AfEnumValues enumValue:enumList){
						sectionItemsBean.setTypeName(enumValue.getValue());
					}
					
					sectionItemsBean.setDisplayOrder(String.valueOf(afSectionItem.getDisplayOrder()));
					sectionItemsBean.setSectionItemStatus(afSectionItem.getSectionItemStatus());
					sectionItemsBean.setDisplayPrtPopup(afSectionItem.getDisplayPrtPopup()+"");
					switch(itemType){
						case AfConstants.ITEM_TYPE_PRODUCT:
							if(afSectionItem.getAfPartnerProduct() != null){
								sectionItemsBean.setTitle(afSectionItem.getAfPartnerProduct().getProductName());
							}
							break;
						case AfConstants.ITEM_TYPE_CATEGORY:
							if(afSectionItem.getAfCategory() != null){
								sectionItemsBean.setTitle(afSectionItem.getAfCategory().getCategoryName());
							}
							
							break;
						case AfConstants.ITEM_TYPE_PARTNER:
							if(afSectionItem.getAfPartner() != null){
								sectionItemsBean.setTitle(afSectionItem.getAfPartner().getPartnerName());
							}
							break;
						case AfConstants.ITEM_TYPE_OFFERS_AND_DEALS:
							if(afSectionItem.getAfPartnerOffer() != null){
								sectionItemsBean.setTitle(afSectionItem.getAfPartnerOffer().getOfferName());
							}
							break;
						case AfConstants.ITEM_TYPE_BRAND:
							if(afSectionItem.getBrandName() !=null){
								sectionItemsBean.setBrandName(afSectionItem.getBrandName());
								sectionItemsBean.setTitle(afSectionItem.getBrandName());
								sectionItemsBean.setBrandImageUrl(afSectionItem.getBrandImageUrl());
								sectionItemsBean.setBrandDescription(afSectionItem.getBrandDescription());
							}
							break;
						case AfConstants.ITEM_TYPE_CUSTOM:
						
								sectionItemsBean.setTitle(afSectionItem.getTitle());
								sectionItemsBean.setBrandDescription(afSectionItem.getBrandDescription());
								sectionItemsBean.setLandingUrl(afSectionItem.getLandingUrl());
								sectionItemsBean.setBrandImageUrl(afSectionItem.getBrandImageUrl());
						
					}

					sectionItemStatus=afSectionItem.getSectionItemStatus();
					dateStr = ServiceProviderUtil.getInstance().getFormattedDate(afSectionItem.getStartDate(),"dd/MM/yyyy");
					stDt = dateFormat.parse(dateStr);
					currentDt = ServiceProviderUtil.getInstance().getFormattedDate(dt,"dd/MM/yyyy");
					curDt = dateFormat.parse(currentDt);
					 
					dateEnd = ServiceProviderUtil.getInstance().getFormattedDate(afSectionItem.getEndDate(),"dd/MM/yyyy");
					if(!dateEnd.equals("")){
						enDt = dateFormat.parse(dateEnd);
					}

					if(1==sectionItemStatus){
						sectionItemsBean.setStatus(AfConstants.STATUS_ACTIVE);
						sectionItemsBean.setStatusCodeClass(AfConstants.STATUS_CLASS_GREEN);
						
					}
					if(0==sectionItemStatus){
						sectionItemsBean.setStatus(AfConstants.STATUS_IN_ACTIVE);
						sectionItemsBean.setStatusCodeClass(AfConstants.STATUS_CLASS_RED);
						
					}
					
					if(dateEnd.equals("")){	
						
						if(curDt.before(stDt)){
							sectionItemsBean.setStatus(AfConstants.STATUS_IN_ACTIVE);
							sectionItemsBean.setStatusCodeClass(AfConstants.STATUS_CLASS_RED);
						}else if(curDt.equals(stDt) ){
							if(1==sectionItemStatus){
								sectionItemsBean.setStatus(AfConstants.STATUS_ACTIVE);
								sectionItemsBean.setStatusCodeClass(AfConstants.STATUS_CLASS_GREEN);
							}else{
								sectionItemsBean.setStatus(AfConstants.STATUS_IN_ACTIVE);
								sectionItemsBean.setStatusCodeClass(AfConstants.STATUS_CLASS_RED);
							}
						}else if(curDt.after(stDt)){
							if(1==sectionItemStatus){
								sectionItemsBean.setStatus(AfConstants.STATUS_ACTIVE);
								sectionItemsBean.setStatusCodeClass(AfConstants.STATUS_CLASS_GREEN);
								}else{
									sectionItemsBean.setStatus(AfConstants.STATUS_IN_ACTIVE);
									sectionItemsBean.setStatusCodeClass(AfConstants.STATUS_CLASS_RED);
								}
						}
					
					}else{
							
							if(curDt.after(stDt) && enDt.after(curDt)){
								if(1==sectionItemStatus){
									sectionItemsBean.setStatus(AfConstants.STATUS_ACTIVE);
									sectionItemsBean.setStatusCodeClass(AfConstants.STATUS_CLASS_GREEN);
								}else{
									sectionItemsBean.setStatus(AfConstants.STATUS_IN_ACTIVE);
									sectionItemsBean.setStatusCodeClass(AfConstants.STATUS_CLASS_RED);
								}
							}else if(curDt.equals(stDt) && curDt.equals(enDt)){
								if(1==sectionItemStatus){
									sectionItemsBean.setStatus(AfConstants.STATUS_ACTIVE);
									sectionItemsBean.setStatusCodeClass(AfConstants.STATUS_CLASS_GREEN);
								}else{
									sectionItemsBean.setStatus(AfConstants.STATUS_IN_ACTIVE);
									sectionItemsBean.setStatusCodeClass(AfConstants.STATUS_CLASS_RED);
								}	
							}else if(curDt.after(stDt) && enDt.before(curDt)){
								sectionItemsBean.setStatus(AfConstants.STATUS_IN_ACTIVE);
								sectionItemsBean.setStatusCodeClass(AfConstants.STATUS_CLASS_RED);
							}else if(curDt.equals(stDt) && enDt.after(curDt)){
								if(1==sectionItemStatus){
									sectionItemsBean.setStatus(AfConstants.STATUS_ACTIVE);
									sectionItemsBean.setStatusCodeClass(AfConstants.STATUS_CLASS_GREEN);
								}else{
									sectionItemsBean.setStatus(AfConstants.STATUS_IN_ACTIVE);
									sectionItemsBean.setStatusCodeClass(AfConstants.STATUS_CLASS_RED);
								}	
							}else if(stDt.after(curDt) && stDt.equals(enDt)){
								sectionItemsBean.setStatus(AfConstants.STATUS_IN_ACTIVE);
								sectionItemsBean.setStatusCodeClass(AfConstants.STATUS_CLASS_RED);
							}else if(stDt.before(curDt) && enDt.before(curDt)){
								if(1==sectionItemStatus){
									sectionItemsBean.setStatus(AfConstants.STATUS_ACTIVE);
									sectionItemsBean.setStatusCodeClass(AfConstants.STATUS_CLASS_GREEN);
								}else{
									sectionItemsBean.setStatus(AfConstants.STATUS_IN_ACTIVE);
									sectionItemsBean.setStatusCodeClass(AfConstants.STATUS_CLASS_RED);
								}	
							}
							
						} 
					
					sectionItemList.add(sectionItemsBean);
				}
			}
		}catch(Exception e){
			logger.error("@@@@ Exception in SectionManagementServiceImpl getSectionItems() :",e);
		}
		return sectionItemList;
	}
	
	@Override
	public SectionItemsBean getSectionItemByItemId(String sectionItemId) {
		AfSectionItems afSectionItem=null;
		List<SectionItemsBean> sectionItemList=new ArrayList<SectionItemsBean>();
		List<AfEnumValues> enumList = new ArrayList<AfEnumValues>();
		AfSections afSections = null;
		AfPartner afPartner = null;
		AfPartnerOffer afPartnerOffer = null;
		AfProduct afProduct = null;
		AfCategory afCategory = null;
		byte sectionItemStatus;
		SectionItemsBean sectionItemsBean=new SectionItemsBean();
		String dateStr="";
		Date dt=null; 
		try{
			
			afSectionItem=sectionManagementDAO.getSectionItemByItemId(sectionItemId);
			if(afSectionItem!=null)
			{
			sectionItemsBean.setSectionId(afSectionItem.getAfSections().getSectId()+"");
			sectionItemsBean.setItemType(afSectionItem.getItemType()+"");
			if(afSectionItem.getAfPartner()!=null)
			sectionItemsBean.setPrtNo(afSectionItem.getAfPartner().getPrtNo()+"");
			
			if(afSectionItem.getAfCategory()!=null)
			{
			sectionItemsBean.setCatNo(afSectionItem.getAfCategory().getCatNo()+"");
			sectionItemsBean.setCatName(afSectionItem.getAfCategory().getCategoryName()+"");
			}
			if(afSectionItem.getAfPartnerProduct()!=null)
			{
			sectionItemsBean.setPrtPrdctNo(afSectionItem.getAfPartnerProduct().getPrtPrdNo()+"");
			sectionItemsBean.setProductName(afSectionItem.getAfPartnerProduct().getProductName());
			String prtCategoryName=null;
				prtCategoryName=afSectionItem.getAfPartnerProduct().getAfPartnerCategory().getPartnerCategoryName();
				
			sectionItemsBean.setPrtCatName(prtCategoryName+"");
			}
			if(afSectionItem.getBrandName() !=null){
				sectionItemsBean.setBrandName(afSectionItem.getBrandName());	
				sectionItemsBean.setBrandImageUrl(afSectionItem.getBrandImageUrl());
				sectionItemsBean.setBrandDescription(afSectionItem.getBrandDescription());
			}
			sectionItemsBean.setDisplayOrder(afSectionItem.getDisplayOrder()+"");
			sectionItemStatus=afSectionItem.getSectionItemStatus();
			
			if(1==sectionItemStatus){
				sectionItemsBean.setStatus(AfConstants.STATUS_ACTIVE);
				sectionItemsBean.setStatusCodeClass(AfConstants.STATUS_CLASS_GREEN);

			}
			if(0==sectionItemStatus){
				sectionItemsBean.setStatus(AfConstants.STATUS_IN_ACTIVE);
				sectionItemsBean.setStatusCodeClass(AfConstants.STATUS_CLASS_RED);
			}
			
			sectionItemsBean.setSectionItemStatus(sectionItemStatus);
			
			dt=afSectionItem.getStartDate()!=null?afSectionItem.getStartDate():new Date();
			dateStr=ServiceProviderUtil.getInstance().getFormattedDate(afSectionItem.getStartDate(),"dd/MM/yyyy");
			if(dt!=null && !dt.equals(""))
				sectionItemsBean.setStartDate(dateStr);
			
			dt=afSectionItem.getEndDate()!=null?afSectionItem.getEndDate():new Date();
			dateStr=ServiceProviderUtil.getInstance().getFormattedDate(afSectionItem.getEndDate(),"dd/MM/yyyy");
			if(dt!=null && !dt.equals(""))
				sectionItemsBean.setEndDate(dateStr);
			 
			}	
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in SectionManagementServiceImpl getSectionItemByItemId() :",e);
		}
		return sectionItemsBean;
	}
	
	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String updateSectionItems(JSONObject userJson,HttpServletRequest request){
		Date dt=null;
		DateFormat sourceDf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		
		AfPartner afPartner = null;
		AfPartnerOffer afPartnerOffer = null;
		AfPartnerProduct afPartnerProduct = null;
		AfCategory afCategory = null;
		
		
		String status="failure",activationDate="",deactivationDate="";
		AfSectionItems afSectionItems = null;
		AfSectionItems afSectionItemsOld=null;
		
		String replaceInOld="";
		String replaceInNew="";
		String prtNo="", prtPrdNo="", prtOfferNo="", sectionItemStatus="", catNo="", displayOrd="",brandName="",brandImageUrl="",brandDescription="";
		String landingUrl="",title="",text="",displaypopup="";
		try{
			afSectionItems=sectionManagementDAO.getSectionItemByItemId(userJson.getString("edit_itemId"));
			afSectionItemsOld=(AfSectionItems) afSectionItems.clone();
			prtNo=userJson.getString("edit_prtNo");
			prtPrdNo=userJson.getString("edit_prtPrdctNo");
			prtOfferNo=userJson.getString("edit_prtOfferNo");
			sectionItemStatus=userJson.getString("edit_sectionItemStatus");
			
			catNo=userJson.getString("catgry_no");
			displayOrd=userJson.getString("edit_displayOrder");
			brandName=userJson.getString("edit_brandName");
			brandImageUrl=userJson.getString("edit_brandImageUrl");
			String custombrandImageUrl=userJson.getString("edit_brandImageUrl1");
			brandDescription=userJson.getString("edit_brandDescription");

			activationDate=userJson.getString("startDate");
			
			landingUrl=userJson.getString("edit_landingUrl");
			title=userJson.getString("edit_title");
			text=userJson.getString("edit_text");
			if(activationDate.length()>0 && activationDate!=null){
				Date dt1=new Date();
				String hh_mi_ss=""+dt1;
				String format[]=hh_mi_ss.split(" ");
				hh_mi_ss=format[3];
				activationDate=activationDate+" "+hh_mi_ss;
				dt=sourceDf.parse(activationDate);
				afSectionItems.setStartDate(dt);
			}
			
			deactivationDate=userJson.getString("endDate");			
			if(deactivationDate.length()>0 && deactivationDate!=null){
				Date dt1=new Date();
				String hh_mi_ss=""+dt1;
				String format[]=hh_mi_ss.split(" ");
				hh_mi_ss=format[3];
				deactivationDate=deactivationDate+" "+hh_mi_ss;
				dt=sourceDf.parse(deactivationDate);
				afSectionItems.setEndDate(dt);
			}
			else{
				afSectionItems.setEndDate(null);
			}

			if(!prtNo.isEmpty()){
				afPartner = partnermanagementDAO.getPartnerByPartnerNO(prtNo);
			}
			
			if(!prtOfferNo.isEmpty()){
				afPartnerOffer = offersDealsDAO.getPartnerOfferByNO(prtOfferNo);
			}
			
			if(!prtPrdNo.isEmpty()){
				afPartnerProduct = partnerProductDAO.getPartnerProductByPrtPrdNO(prtPrdNo);
			}
			
			if(!catNo.isEmpty()){
				afCategory = categoryManagementDAO.getCategoryByCategoryNo(catNo);
			}
			
			afSectionItems.setAfPartner(afPartner);
			afSectionItems.setAfPartnerOffer(afPartnerOffer);
			
			afSectionItems.setAfPartnerProduct(afPartnerProduct);
			afSectionItems.setAfCategory(afCategory);
			
			afSectionItems.setDisplayOrder((displayOrd != null)?Integer.parseInt(displayOrd):null);
			afSectionItems.setSectionItemStatus(Byte.parseByte(sectionItemStatus));
			
			afSectionItems.setStatus((byte)1);
			afSectionItems.setModifiedTime(new Date());
			afSectionItems.setModifiedBy(1);
			
			afSectionItems.setBrandName(brandName);
			if(afSectionItems.getItemType()==5){
				afSectionItems.setBrandImageUrl(brandImageUrl);
			}
			if(afSectionItems.getItemType()==6){
				afSectionItems.setBrandImageUrl(custombrandImageUrl);
			}
			afSectionItems.setBrandDescription(brandDescription);
			
			afSectionItems.setLandingUrl(landingUrl);
			afSectionItems.setTitle(title);
			afSectionItems.setBrandDescription(text);
			afSectionItems.setDisplayPrtPopup(Byte.parseByte(userJson.getString(("displayPrtPopup"))));
			
			
			String statusInactive=	afSectionItemsOld.getSectionItemStatus()==0?"InActive":"Active";
	     	String statusActive=afSectionItems.getSectionItemStatus()==0?"InActive":"Active";
			
			status = sectionManagementDAO.createSectionItems(afSectionItems);
			if("success".equalsIgnoreCase(status)){
				String ipAddress = request.getHeader("X-FORWARDED-FOR");
				if (ipAddress == null) {
					ipAddress = request.getRemoteAddr();
				}
				AfUserActivityLog updateUsActLog = new AfUserActivityLog();
				Map<String,String> objectCom = new LinkedHashMap<>();
				try{
					updateUsActLog.setUserActivity("Update Section Wise Listing ");
					updateUsActLog.setUserActCreatedDateTime(new Date());
					updateUsActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
					updateUsActLog.setUserActIpVal(ipAddress);
					updateUsActLog.setUserActModule("Home page Custom section");
					if(afSectionItems.getItemType()==1){
					if(!afSectionItems.getAfPartner().getPartnerName().equalsIgnoreCase(afSectionItemsOld.getAfPartner().getPartnerName())){
						replaceInOld=replaceInOld+"|Partner Name :"+afSectionItemsOld.getAfPartner().getPartnerName();
					    replaceInNew=replaceInNew+"|Partner Name :"+afSectionItems.getAfPartner().getPartnerName();
					}
					if(!afSectionItems.getAfPartnerCategory().getPartnerCategoryName().equalsIgnoreCase(afSectionItemsOld.getAfPartnerCategory().getPartnerCategoryName())){
						replaceInOld=replaceInOld+"|Partner Category :"+afSectionItemsOld.getAfPartnerCategory().getPartnerCategoryName();
					    replaceInNew=replaceInNew+"|Partner Category :"+afSectionItems.getAfPartnerCategory().getPartnerCategoryName();
					}
					if(!afSectionItems.getAfPartnerProduct().getProductName().equalsIgnoreCase(afSectionItemsOld.getAfPartnerProduct().getProductName())){
						replaceInOld=replaceInOld+"|Select Product :"+afSectionItemsOld.getAfPartnerProduct().getProductName();
					    replaceInNew=replaceInNew+"|Select Product :"+afSectionItems.getAfPartnerProduct().getProductName();
					}
					
					
					
				}
					if(afSectionItems.getItemType()==2){
						if(!afSectionItems.getAfCategory().getCategoryName().equalsIgnoreCase(afSectionItemsOld.getAfCategory().getCategoryName())){
							replaceInOld=replaceInOld+"|Category Name :"+afSectionItemsOld.getAfCategory().getCategoryName();
						    replaceInNew=replaceInNew+"|Category Name :"+afSectionItems.getAfCategory().getCategoryName();
						}
						
						
				}
					if(afSectionItems.getItemType()==3){
						if(!afSectionItems.getAfPartner().getPartnerName().equalsIgnoreCase(afSectionItemsOld.getAfPartner().getPartnerName())){
							replaceInOld=replaceInOld+"|Partner Name :"+afSectionItemsOld.getAfPartner().getPartnerName();
						    replaceInNew=replaceInNew+"|Partner Name :"+afSectionItems.getAfPartner().getPartnerName();
						}
						
						
				}
					
					if(afSectionItems.getItemType()==4){
						if(!afSectionItems.getAfPartner().getPartnerName().equalsIgnoreCase(afSectionItemsOld.getAfPartner().getPartnerName())){
							replaceInOld=replaceInOld+"|Partner Name :"+afSectionItemsOld.getAfPartner().getPartnerName();
						    replaceInNew=replaceInNew+"|Partner Name :"+afSectionItems.getAfPartner().getPartnerName();
						}
						if(!afSectionItems.getAfPartnerOffer().getOfferName().equalsIgnoreCase(afSectionItemsOld.getAfPartnerOffer().getOfferName())){
							replaceInOld=replaceInOld+"|Select Offers:"+afSectionItemsOld.getAfPartnerOffer().getOfferName();
							replaceInNew=replaceInNew+"|Select Offers:"+afSectionItems.getAfPartnerOffer().getOfferName();
						}
						
						if(!afSectionItems.getAfCategory().getCategoryName().equalsIgnoreCase(afSectionItemsOld.getAfCategory().getCategoryName())){
							replaceInOld=replaceInOld+"|Category Name:"+(afSectionItemsOld.getBrandDescription());
							replaceInNew=replaceInNew+"|Category Name:"+(afSectionItems.getBrandDescription());
						}
						
				}
					
					if(afSectionItems.getItemType()==5){
						if(!afSectionItems.getBrandName().equalsIgnoreCase(afSectionItemsOld.getBrandName())){
							replaceInOld=replaceInOld+"|Select Brand :"+afSectionItemsOld.getBrandName();
						    replaceInNew=replaceInNew+"|Select Brand :"+afSectionItems.getBrandName();
						}
						if(afSectionItems.getBrandDescription()!=afSectionItemsOld.getBrandDescription()){
							replaceInOld=replaceInOld+"|Brand description:"+(afSectionItemsOld.getBrandDescription());
							replaceInNew=replaceInNew+"|Brand description:"+(afSectionItems.getBrandDescription());
						}
						if(afSectionItems.getBrandImageUrl()!=afSectionItemsOld.getBrandImageUrl()){
							replaceInOld=replaceInOld+"|Brand Image:"+(afSectionItemsOld.getBrandImageUrl());
							replaceInNew=replaceInNew+"|Brand Image:"+(afSectionItems.getBrandImageUrl());
						}
				}
					
					if(afSectionItems.getItemType()==6){
						if(!afSectionItems.getAfPartner().getPartnerName().equalsIgnoreCase(afSectionItemsOld.getAfPartner().getPartnerName())){
							replaceInOld=replaceInOld+"|Partner Name :"+afSectionItemsOld.getAfPartner().getPartnerName();
						    replaceInNew=replaceInNew+"|Partner Name :"+afSectionItems.getAfPartner().getPartnerName();
						}
						if(!afSectionItems.getTitle().equalsIgnoreCase(afSectionItemsOld.getTitle())){
							replaceInOld=replaceInOld+"|Custom Item Title :"+afSectionItemsOld.getTitle();
						    replaceInNew=replaceInNew+"|Custom Item Title :"+afSectionItems.getTitle();
						}
						if(!afSectionItems.getBrandDescription().equalsIgnoreCase(afSectionItemsOld.getBrandDescription())){
							replaceInOld=replaceInOld+"|Custom Item Text:"+(afSectionItemsOld.getBrandDescription());
							replaceInNew=replaceInNew+"|Custom Item Text:"+(afSectionItems.getBrandDescription());
						}
						if(!afSectionItems.getBrandImageUrl().equalsIgnoreCase(afSectionItemsOld.getBrandImageUrl())){
							replaceInOld=replaceInOld+"|Custom Image:"+(afSectionItemsOld.getBrandImageUrl());
							replaceInNew=replaceInNew+"|Custom Image:"+(afSectionItems.getBrandImageUrl());
						}
						if(!afSectionItems.getLandingUrl().equalsIgnoreCase(afSectionItemsOld.getLandingUrl())){
							replaceInOld=replaceInOld+"|Custom Item Landing Url :"+(afSectionItemsOld.getLandingUrl());
							replaceInNew=replaceInNew+"|Custom Item Landing Url:"+(afSectionItems.getLandingUrl());
						}
				}
					
					if(afSectionItems.getSectionItemStatus()!=afSectionItemsOld.getSectionItemStatus()){
						replaceInOld=replaceInOld+"|Section Item Status:"+statusInactive;
						replaceInNew=replaceInNew+"|Section Item Status:"+statusActive;
					}
					if(afSectionItems.getDisplayOrder()!=afSectionItemsOld.getDisplayOrder()){
						replaceInOld=replaceInOld+"|Display Order:"+(afSectionItemsOld.getDisplayOrder());
						replaceInNew=replaceInNew+"|Display Order:"+(afSectionItems.getDisplayOrder());
					}
					if(afSectionItems.getEndDate()!=afSectionItemsOld.getEndDate()){
						replaceInOld=replaceInOld+"|Deactivation Date:"+(afSectionItemsOld.getEndDate());
						replaceInNew=replaceInNew+"|Deactivation Date:"+(afSectionItems.getEndDate());
					}
//					if(replaceInNew.isEmpty() && replaceInOld.isEmpty() && afSectionItemsOld!=null) {
//                        replaceInOld=replaceInOld+"|ModifiedTime:"+simpleDateFormat.format(afSectionItemsOld.getModifiedTime());
//                        replaceInNew=replaceInNew+"|ModifiedTime:"+simpleDateFormat.format(afSectionItems.getModifiedTime());
//                    }
					updateUsActLog.setUserNewFieldValue(replaceInNew);
		    		updateUsActLog.setUserOldFieldValue(replaceInOld);
					userActLogDao.updateUserLog(updateUsActLog);
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in SectionManagementServiceImpl updateSectionItems() :", e);
		}

		return status;
	}

	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String deleteItem(String itemId,HttpServletRequest request) {
		AfSectionItems afSectionItems = null;
		String status="failure";
		String oldOrNewVal = "";
		List<AfEnumValues> enumList = new ArrayList<AfEnumValues>();
	
		try{
			if(!itemId.isEmpty())
			{
			afSectionItems=sectionManagementDAO.getSectionItemByItemId(itemId.trim());
			enumList = sectionManagementDAO.getEnumValues("ITEM_TYPE", String.valueOf(afSectionItems.getItemType()));
			SectionItemsBean sectionItemsBean1=new SectionItemsBean();
			for(AfEnumValues enumValue:enumList){
			  sectionItemsBean1.setTypeName(enumValue.getValue());
				
			}
			afSectionItems.setSectionItemStatus((byte)0);
			afSectionItems.setStatus((byte)0);
			afSectionItems.setModifiedTime(new Date());
			afSectionItems.setModifiedBy(1);
			
			String status1=afSectionItems.getSectionItemStatus()+"";
			status1 = status1.replaceAll("0","InActive");
			status1 = status1.replaceAll("1", "Active");
			
				String ipAddress = request.getHeader("X-FORWARDED-FOR");
				if (ipAddress == null) {
					ipAddress = request.getRemoteAddr();
				}
				AfUserActivityLog updateUsActLog = new AfUserActivityLog();
				try{
				
					oldOrNewVal="Select A Section: "+afSectionItems.getAfSections().getSectionTitle()+" | "+"Item Type : "+sectionItemsBean1.getTypeName()+" | "+
                    " |"+"Section Item Status : "+status1+" |"+"Display Order : "+afSectionItems.getDisplayOrder()+" |";
					updateUsActLog.setUserActivity("Delete Section Wise Listing ");
			    	updateUsActLog.setUserActCreatedDateTime(new Date());
			    	updateUsActLog.setUserNewFieldValue("NA");
			    	updateUsActLog.setUserActIpVal(ipAddress);
			    	updateUsActLog.setUserActModule("Home page Custom Section");
			    	updateUsActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
			    	updateUsActLog.setUserOldFieldValue(oldOrNewVal);
			    	userActLogDao.updateUserLog(updateUsActLog);
			    	status = sectionManagementDAO.createSectionItems(afSectionItems);
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return status;
	}
	
	@Override
	public List<HomePageSectionBean> getHomePageSections() {
		List<AfHomePageSection> afHomeSectionList=null;
		List<HomePageSectionBean> homeSectionList=new ArrayList<>();
		byte status;
		AfSections afSection=null;
		String dateVal="", dateStr="", dateEnd="", currentDt="";
		Date dt1=null, dt2=null, stDt=null, enDt=null, curDt=null; 
		Date dt = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		try{
			afHomeSectionList=sectionManagementDAO.getHomePageSections();
			for(AfHomePageSection afHomeSection:afHomeSectionList){
				HomePageSectionBean homePageSectionBean=new HomePageSectionBean();
				homePageSectionBean.setSectionId(afHomeSection.getSectionId());
				homePageSectionBean.setSectionName(afHomeSection.getSectionName());
				homePageSectionBean.setSectionPosition(afHomeSection.getSectionPosition());
				homePageSectionBean.setLoggedInStatus(afHomeSection.getLoggedInStatus());
				homePageSectionBean.setNonLoggedInstatus(afHomeSection.getNonLoggedInStatus());
				homePageSectionBean.setStatus(afHomeSection.getStatus());
				homePageSectionBean.setSectionCode(afHomeSection.getSectionCode());
				status=afHomeSection.getStatus();
				if(1==status){
					homePageSectionBean.setStatusValue(AfConstants.STATUS_ACTIVE);
					homePageSectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_GREEN);
				}
				if(0==status){
					homePageSectionBean.setStatusValue(AfConstants.STATUS_IN_ACTIVE);
					homePageSectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_RED);
				}
				afSection=sectionManagementDAO.getAfSectionByTitle(afHomeSection.getSectionName());
				if (afSection != null) {
					dt1 = afSection.getSectionStartDate() != null ? afSection.getSectionStartDate() : new Date();
					dateStr = ServiceProviderUtil.getInstance().getFormattedDate(afSection.getSectionStartDate(),
							"dd/MM/yyyy");
					dt2 = afSection.getSectionEndDate() != null ? afSection.getSectionEndDate() : new Date();
					dateVal = ServiceProviderUtil.getInstance().getFormattedDate(afSection.getSectionEndDate(),
							"dd/MM/yyyy");
					dateStr = ServiceProviderUtil.getInstance().getFormattedDate(afSection.getSectionStartDate(),
							"dd/MM/yyyy");
					stDt = dateFormat.parse(dateStr);
					currentDt = ServiceProviderUtil.getInstance().getFormattedDate(dt, "dd/MM/yyyy");
					curDt = dateFormat.parse(currentDt);

					dateEnd = ServiceProviderUtil.getInstance().getFormattedDate(afSection.getSectionEndDate(),
							"dd/MM/yyyy");
					if (!dateEnd.equals("")) {

						enDt = dateFormat.parse(dateEnd);
					}
					if (dateEnd.equals("")) {

						if (curDt.before(stDt)) {
							homePageSectionBean.setStatusValue(AfConstants.STATUS_IN_ACTIVE);
							homePageSectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_RED);
						} else if (curDt.equals(stDt)) {
							if (1 == status) {
								homePageSectionBean.setStatusValue(AfConstants.STATUS_ACTIVE);
								homePageSectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_GREEN);
							} else {
								homePageSectionBean.setStatusValue(AfConstants.STATUS_IN_ACTIVE);
								homePageSectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_RED);
							}
						} else if (curDt.after(stDt)) {
							if (1 == status) {
								homePageSectionBean.setStatusValue(AfConstants.STATUS_ACTIVE);
								homePageSectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_GREEN);
							} else {
								homePageSectionBean.setStatusValue(AfConstants.STATUS_IN_ACTIVE);
								homePageSectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_RED);
							}
						}

					} else {

						if (curDt.after(stDt) && enDt.after(curDt)) {
							if (1 == status) {
								homePageSectionBean.setStatusValue(AfConstants.STATUS_ACTIVE);
								homePageSectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_GREEN);
							} else {
								homePageSectionBean.setStatusValue(AfConstants.STATUS_IN_ACTIVE);
								homePageSectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_RED);
							}
						} else if (curDt.equals(stDt) && curDt.equals(enDt)) {
							if (1 == status) {
								homePageSectionBean.setStatusValue(AfConstants.STATUS_ACTIVE);
								homePageSectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_GREEN);
							} else {
								homePageSectionBean.setStatusValue(AfConstants.STATUS_IN_ACTIVE);
								homePageSectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_RED);
							}
						} else if (curDt.after(stDt) && enDt.before(curDt)) {
							homePageSectionBean.setStatusValue(AfConstants.STATUS_IN_ACTIVE);
							homePageSectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_RED);
						} else if (curDt.equals(stDt) && enDt.after(curDt)) {
							if (1 == status) {
								homePageSectionBean.setStatusValue(AfConstants.STATUS_ACTIVE);
								homePageSectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_GREEN);
							} else {
								homePageSectionBean.setStatusValue(AfConstants.STATUS_IN_ACTIVE);
								homePageSectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_RED);
							}
						} else if (stDt.after(curDt) && stDt.equals(enDt)) {
							homePageSectionBean.setStatusValue(AfConstants.STATUS_IN_ACTIVE);
							homePageSectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_RED);
						} else if (stDt.before(curDt) && enDt.before(curDt)) {
							if (1 == status) {
								homePageSectionBean.setStatusValue(AfConstants.STATUS_ACTIVE);
								homePageSectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_GREEN);
							} else {
								homePageSectionBean.setStatusValue(AfConstants.STATUS_IN_ACTIVE);
								homePageSectionBean.setStatusCodeClass(AfConstants.STATUS_CLASS_RED);
							}
						}
					}
					
				}
				
				
				
				
				homeSectionList.add(homePageSectionBean);
			}
		}catch(Exception e){
			logger.error("@@@@ Exception in SectionManagementServiceImpl getSections() :",e);
		}
		return homeSectionList;
	}
	
	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String homeSectionSaveOrder(String reOrder,HttpServletRequest request) {
		StringTokenizer reOrderTkn = null, reOrderTkn_ = null ;		
		Integer secNo = -1;
		int sectionPosition=-1, part=0;
		String status="", temp = "";
		List<AfHomePageSection> afHomeSectionList = null;
		List<Integer> sectionlist = new ArrayList<Integer>();
		List<Integer> secDisOrdlist = new ArrayList<Integer>();
		int secdisOrd=0;
		AfSections afSections=null;
		AfHomePageSection afHomePageSectionOld=null;
		
		String replaceInOld="";
		String replaceInNew="";
		try{
			
			afHomeSectionList = sectionManagementDAO.getHomePageSections();
			
			for(AfHomePageSection afHomeSection : afHomeSectionList){
				sectionPosition =afHomeSection.getSectionPosition();	
				secDisOrdlist.add(sectionPosition);
				}
			reOrderTkn = new StringTokenizer(reOrder,",");
			while(reOrderTkn.hasMoreTokens()){				
				reOrderTkn_ = new StringTokenizer(reOrderTkn.nextToken(),".");
				temp = reOrderTkn_.nextToken();
				secNo = (int)Integer.parseInt(reOrderTkn_.nextToken());
				sectionlist.add(secNo);
			}		
			secdisOrd=0;
			if(secDisOrdlist.size() == sectionlist.size()){
			
				for(int i=1, j=1; i<=secDisOrdlist.size() && j<=sectionlist.size();i++,j++){
					secdisOrd = secDisOrdlist.get(i-1);
					AfHomePageSection afHomePageSection=sectionManagementDAO.getHomePageSectionById(sectionlist.get(j-1));				
					afHomePageSectionOld=(AfHomePageSection) afHomePageSection.clone();
					afHomePageSection.setSectionPosition((byte)secdisOrd);
					
					afSections=sectionManagementDAO.getAfSectionByTitle(afHomePageSection.getSectionName());
					afSections.setSectionDispOrder((byte)secdisOrd);
					status=sectionManagementDAO.updateSection(afHomePageSection);
					sectionManagementDAO.updateSection(afSections);
					 if("success".equalsIgnoreCase(status)){
						 String ipAddress = request.getHeader("X-FORWARDED-FOR");
							if (ipAddress == null) {
								ipAddress = request.getRemoteAddr();
							}
							AfUserActivityLog updateUsActLog = new AfUserActivityLog();
							Map<String,String> objectCom = new LinkedHashMap<>();
							try{
								updateUsActLog.setUserActivity("Update Section");
								updateUsActLog.setUserActCreatedDateTime(new Date());
								updateUsActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
								updateUsActLog.setUserActIpVal(ipAddress);
								updateUsActLog.setUserActModule("HomePage Section Order");
								
								objectCom = adminUserActivityUtil.objectComparision(afHomePageSection, afHomePageSectionOld);
								replaceInOld=objectCom.get(AfConstants.ACTIVITY_OLD_KEY).toString().replace(":0 |",":InActive |").replace(":1 |", ":Active |");
					    		replaceInNew=objectCom.get(AfConstants.ACTIVITY_NEW_KEY).toString().replace(":0 |",":InActive |").replace(":1 |", ":Active |");
					    		if(replaceInOld!=replaceInNew){
								updateUsActLog.setUserNewFieldValue(replaceInNew);
					    		updateUsActLog.setUserOldFieldValue(replaceInOld);
								userActLogDao.updateUserLog(updateUsActLog);
					    		}
							}
							catch(Exception e){
								e.printStackTrace();
							}
					 }
					
					}
			}
		}
		catch(Exception e){
			logger.error("@@@@ Exception in SectionManagementServiceImpl homeSectionSaveOrder() :"+e);
		}
		
		return status;
	}
	
	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public String updateHomePageSections(JSONObject homePageSectionJson,HttpServletRequest request) {
		String status="failure";
		AfHomePageSection afHomePageSection=null;
		AfHomePageSection afHomePageSectionOld=null;
		int sectionId;
		String sectionTitle;
		AfSections afSections=null;
		String replaceInOld="";
		String replaceInNew="";
		try {
			byte loggedInStatus = (byte)homePageSectionJson.getInt("loggedInStatus");
			byte nonLoggedInStatus = (byte)homePageSectionJson.getInt("nonLoggedInStatus");
			sectionId=homePageSectionJson.getInt("sectionId");
			sectionTitle=homePageSectionJson.getString("sectionTitle");
			afHomePageSection=sectionManagementDAO.getHomePageSectionById(sectionId);
			afHomePageSectionOld=(AfHomePageSection) afHomePageSection.clone();
			afSections=sectionManagementDAO.getAfSectionByTitle(sectionTitle);
			
			afHomePageSection.setLoggedInStatus((byte)homePageSectionJson.getInt("loggedInStatus"));
			afHomePageSection.setNonLoggedInStatus((byte)homePageSectionJson.getInt("nonLoggedInStatus"));

			if(loggedInStatus == 0 && nonLoggedInStatus==0){
				afHomePageSection.setStatus((byte) 0);
				if(afSections !=null)
					afSections.setSectionStatus((byte) 0);
			}else{
				afHomePageSection.setStatus((byte) 1);
				if(afSections !=null)
					afSections.setSectionStatus((byte) 1);

			}
			afHomePageSection.setModifiedTime(new Date());
			afHomePageSection.setModifiedBy(1);
			sectionManagementDAO.updateSection(afSections);
			status=sectionManagementDAO.updateSection(afHomePageSection);
			
			if("success".equalsIgnoreCase(status)){
				String ipAddress = request.getHeader("X-FORWARDED-FOR");
				if (ipAddress == null) {
					ipAddress = request.getRemoteAddr();
				}
				AfUserActivityLog updateUsActLog = new AfUserActivityLog();
				
				try{
					updateUsActLog.setUserActivity(homePageSectionJson.getString("sectionTitle"));
					updateUsActLog.setUserActCreatedDateTime(new Date());
					updateUsActLog.setUserActCreatedBy((String) request.getSession().getAttribute("userIdSession"));
					updateUsActLog.setUserActIpVal(ipAddress);
					//
					String LoggedInStatus=afHomePageSectionOld.getLoggedInStatus()==0?"No":"Yes";
					String NonLoggedInStatus=afHomePageSectionOld.getNonLoggedInStatus()==0?"No":"Yes";
					String LoggedInStatus1= afHomePageSection.getLoggedInStatus()==0?"No":"Yes";
					String NonLoggedInStatus1= afHomePageSection.getNonLoggedInStatus()==0?"No":"Yes";
					//
					updateUsActLog.setUserActModule("HomePage Section Order");
					if(afHomePageSection.getLoggedInStatus() != afHomePageSectionOld.getLoggedInStatus()){
						replaceInOld=" LoggedInStatus: "+LoggedInStatus;
						replaceInNew=" LoggedInStatus:"+LoggedInStatus1;
					}
					if(afHomePageSection.getNonLoggedInStatus() != afHomePageSectionOld.getNonLoggedInStatus()){
						replaceInOld=replaceInOld+" |NonLoggedInStatus: "+NonLoggedInStatus;
						replaceInNew=replaceInNew+" | NonLoggedInStatus:"+NonLoggedInStatus1;
					}
					if(replaceInNew.isEmpty() && replaceInOld.isEmpty()) {
                        replaceInOld=replaceInOld+"|ModifiedTime:"+simpleDateFormat.format(afHomePageSectionOld.getModifiedTime());
                        replaceInNew=replaceInNew+"|ModifiedTime:"+simpleDateFormat.format(afHomePageSection.getModifiedTime());
                    }
					
					updateUsActLog.setUserNewFieldValue(replaceInNew);
		    		updateUsActLog.setUserOldFieldValue(replaceInOld);
					userActLogDao.updateUserLog(updateUsActLog);
				    
		    		
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
			
		} catch (JSONException | CloneNotSupportedException e) {
			e.printStackTrace();
			logger.error("@@@@ Exception in SectionManagementServiceImpl updateHomePageSections() :"+e);

		}
		
		return status;
	}
}