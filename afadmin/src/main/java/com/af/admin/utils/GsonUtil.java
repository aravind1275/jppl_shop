package com.af.admin.utils;

import com.google.gson.Gson;

public class GsonUtil {
	public static String toJson(Object obj) {
		String str = new Gson().toJson(obj);
		return str;
	}
}
