package com.af.admin.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;



public class ServiceProviderUtil {
	
	private Logger logger = LogManager.getLogger(ServiceProviderUtil.class.getName());
	
	private static ServiceProviderUtil serviceProviderUtil;
	
	private ServiceProviderUtil(){

	}
	public static ServiceProviderUtil getInstance(){
		if(serviceProviderUtil==null){
			serviceProviderUtil=new ServiceProviderUtil();  
		}
		return serviceProviderUtil;
	}
	public String getFormattedDate(Date date,String format){
		String dateStr=null;

		try{
			if(format==null){
				format="dd-MM-yyyy";
			}
			if(format.equals("")){
				format="dd-MM-yyyy";
			}
			DateFormat df = new SimpleDateFormat(format);
			if(date!=null){
				dateStr=df.format(date);
			}else{
				dateStr="";
			}
		}catch(Exception e){
			logger.error("Exception in ServiceProviderUtils getFormattedDate", e);
		}
		return dateStr;
	}

	public Date getDateFromString(String date,String format){
		Date dateStr=null;

		try{
			if(format==null){
				format="dd-MM-yyyy";
			}
			if(format.equals("")){
				format="dd-MM-yyyy";
			}
			DateFormat df = new SimpleDateFormat(format);
			if(date!=null && !date.trim().isEmpty()){
				dateStr=df.parse(date);
			}else{

			}
		}catch(Exception e){
			logger.error("Exception in ServiceProviderUtils getDateFromString", e);
		}
		return dateStr;
	}
	public static Date trim(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.MILLISECOND, 0);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.HOUR, 23);
		return calendar.getTime();
	}

	public static String nullSafe(String givenString, String defaultString) {
		return givenString!=null && givenString.length() > 0?givenString:defaultString;
	}
	public int daysBetween(Date d1, Date d2) {
		return (int)( (d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
	}
	
	public static String checkNullSafeWithObject(Object givenObj, String defaultString){
		return ((givenObj!=null && givenObj.toString().trim().length() > 0) ? givenObj.toString() : defaultString);
	}
}

