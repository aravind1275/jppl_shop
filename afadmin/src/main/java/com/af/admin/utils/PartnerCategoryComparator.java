package com.af.admin.utils;

import java.util.Comparator;

import com.af.admin.beans.PartnerCategoryBean;

public class PartnerCategoryComparator implements Comparator {

	@Override
	public int compare(Object o1, Object o2) {
		PartnerCategoryBean pc1=(PartnerCategoryBean)o1;
		PartnerCategoryBean pc2=(PartnerCategoryBean)o2;
		int a=Integer.parseInt(pc1.getPrtCatNo());
		int b=Integer.parseInt(pc2.getPrtCatNo());
		if(a <  b)
			return -1;
	    if(a == b) 
	    	  return 0;
	    return 1;
	}
}