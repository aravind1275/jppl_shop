package com.af.admin.utils;



import java.lang.reflect.Field;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.af.admin.DAO.CommonDAO;
import com.af.admin.constants.AfConstants;
import org.apache.logging.log4j.LogManager;




@Component
@Transactional(readOnly=true)
public class AdminUserActivityUtil {
	
	private static Logger logger = LogManager.getLogger(AdminUserActivityUtil.class.getName());
	
	
	@Autowired
	CommonDAO commonDAO;
	
	public Map<String,String> objectComparision(Object objOne,Object objTwo){
		 Field [] attributes=objOne.getClass().getDeclaredFields();
		 String propertyName;
		 String oldFieldValues="";
		 String newFieldValues="";
		
		 Map<String,String> objectCom=new LinkedHashMap<>();
		 for (Field field : attributes) {
			 propertyName=field.getName();
			 if("serialVersionUID".equals(propertyName))continue;
			 try {
				Object obj1= PropertyUtils.getSimpleProperty(objOne, propertyName);
				Object obj2= PropertyUtils.getSimpleProperty(objTwo, propertyName);
				String oldDate="";
				String newDate="";
				if (Date.class.isAssignableFrom(field.getType())) {
                      if(null!=obj1)oldDate= CommonUtil.getDate((Date)obj1);
                      if(null!=obj2)newDate= CommonUtil.getDate((Date)obj2);

                      oldDate=(null!=oldDate)?oldDate.trim():"";
                      newDate=(null!=newDate)?newDate.trim():"";
                      
                      if(!oldDate.equals(newDate)){
	                      oldFieldValues=oldFieldValues+((oldFieldValues.length()>0)?" | ":"")+propertyName+" :"+oldDate;	
	  					  newFieldValues=newFieldValues+((newFieldValues.length()>0)?" | ":"")+propertyName+" :"+newDate;
                      }
          		 		logger.info("inside oldFieldValues "+oldFieldValues+" newFieldValues "+newFieldValues);

                      continue;
				}//if it is date data type comparision

				
				if(null != obj1 && null != obj2 && !obj1.equals(obj2) && !(Date.class.isAssignableFrom(field.getType()))){
					
				if(null != obj1 && null != obj2 && !obj1.equals(obj2) && !(Date.class.isAssignableFrom(field.getType()))){					
					oldFieldValues=oldFieldValues+((oldFieldValues.length()>0)?" | ":"")+propertyName+" :"+obj1;	
					newFieldValues=newFieldValues+((newFieldValues.length()>0)?" | ":"")+propertyName+" :"+obj2;
				}
				}
			} catch (Exception e) {
				logger.error("@@@@ Exception in AdminUserActivityUtil objectComparision :"+e);
			}
			
		 }//for each
		 
		 	logger.info("outside oldFieldValues "+oldFieldValues+" newFieldValues "+newFieldValues);
			objectCom.put(AfConstants.ACTIVITY_OLD_KEY, oldFieldValues);
			objectCom.put(AfConstants.ACTIVITY_NEW_KEY, newFieldValues);
		
		return objectCom;
	}
	
	
	public static String getIpAddress(HttpServletRequest request){
		String ipAddress = request.getHeader("X-FORWARDED-FOR");  
	    if (ipAddress == null) {  
		   ipAddress = request.getRemoteAddr();  
	     }
	    ipAddress=(null!=ipAddress)?ipAddress.trim():"";
	 return ipAddress;
	}
	
}

