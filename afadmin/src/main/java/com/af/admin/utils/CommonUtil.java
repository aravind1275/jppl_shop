/**
 * 
 */
package com.af.admin.utils;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;

import com.af.admin.beans.CategoryAttributeJSONBean;
import com.af.admin.beans.CategoryBean;
import com.af.admin.beans.CategoryJSONBean;
import com.af.admin.beans.CategoryStateJSONBean;
import com.af.admin.beans.PartnerCategoryBean;
import com.af.admin.pojo.autosugg.AutoSuggestProducts;
import com.af.admin.pojo.search.SearchPartnerProducts;
import com.af.admin.service.CategoryManagementService;
import com.af.admin.service.CommonService;
import com.af.admin.service.PartnerManagementService;
import com.akamai.edgegrid.signer.ClientCredential;
import com.akamai.edgegrid.signer.exceptions.RequestSigningException;
import com.akamai.edgegrid.signer.googlehttpclient.GoogleHttpClientEdgeGridRequestSigner;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.http.ByteArrayContent;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.apache.ApacheHttpTransport;
import com.google.api.client.util.Maps;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * @author jogeswarsahu
 *
 */
@Component
@Transactional(readOnly = true)
public class CommonUtil {

	private Logger logger = LogManager.getLogger(CommonUtil.class.getName());

	@Autowired
	PartnerManagementService partnermanagementService;

	@Autowired
	CategoryManagementService categoryManagementService;

	@Autowired
	CommonService commonService;


	/*public void formChildTree(CategoryJSONBean categoryJSONBean, List<CategoryBean> list, int parentCategoryNo) {
		List<CategoryJSONBean> catJsonList= new ArrayList<CategoryJSONBean>();
		for(CategoryBean categoryBean : list){
			CategoryJSONBean catJSONBean = new CategoryJSONBean();
			if(parentCategoryNo == categoryBean.getParentCatNo()) {
				catJSONBean.setId(String.valueOf(categoryBean.getCatNo() + "/" +
						categoryBean.getChCatNo() + "/" + categoryBean.getChCatNo()));
				catJSONBean.setText(categoryBean.getCategoryName());

				CategoryAttributeJSONBean attributeJSONBean = new CategoryAttributeJSONBean();
				attributeJSONBean.setStartDate(categoryBean.getStartDate());
				attributeJSONBean.setEndDate(categoryBean.getEndDate());
				attributeJSONBean.setStatus(categoryBean.getCategoryStatusCode());
				attributeJSONBean.setCategoryId(categoryBean.getCategoryId());
				attributeJSONBean.setCatNo(String.valueOf(categoryBean.getCatNo()));

				attributeJSONBean.setStartValue(categoryBean.getStartValue());
				attributeJSONBean.setEndValue(categoryBean.getEndValue());
				attributeJSONBean.setStepValue(categoryBean.getStepValue());

				catJSONBean.setA_attr(attributeJSONBean);

				if(categoryBean.getHasChild() == (byte)1) {
					formChildTree(catJSONBean, list, categoryBean.getCatNo());
				}
				catJsonList.add(catJSONBean);
			}
		}
		categoryJSONBean.setChildren(catJsonList);
	 }*/

	/*public void formChildTree(CategoryJSONBean categoryJSONBean, List<CategoryBean> list, int parentCategoryNo) {
		List<CategoryJSONBean> catJsonList= new ArrayList<CategoryJSONBean>();
		for(CategoryBean categoryBean: list){
			CategoryJSONBean catJSONBean = new CategoryJSONBean();
			if(parentCategoryNo == categoryBean.getParentCatNo()){
				catJSONBean.setId(String.valueOf(categoryBean.getCatNo() + "/" +
						categoryBean.getChCatNo() + "/" + categoryBean.getChCatNo()));
				catJSONBean.setText(categoryBean.getSubCategoryName());

				CategoryAttributeJSONBean attributeJSONBean = new CategoryAttributeJSONBean();
				attributeJSONBean.setStartDate(categoryBean.getSubStartDate());
				attributeJSONBean.setEndDate(categoryBean.getSubEndDate());
				attributeJSONBean.setStatus(categoryBean.getChildCategoryStatusCode());
				attributeJSONBean.setCategoryId(categoryBean.getSubCategoryId());
				attributeJSONBean.setCatNo(String.valueOf(categoryBean.getChCatNo()));

				attributeJSONBean.setStartValue(categoryBean.getStartValue());
				attributeJSONBean.setEndValue(categoryBean.getEndValue());
				attributeJSONBean.setStepValue(categoryBean.getStepValue());

				catJSONBean.setA_attr(attributeJSONBean);

				if(categoryBean.getHasChild() == (byte)1) {
					formChildTree(catJSONBean, list, categoryBean.getChCatNo());
				}
				catJsonList.add(catJSONBean);
			}
		}
		categoryJSONBean.setChildren(catJsonList);
	}*/

	public void hasChild(CategoryJSONBean categoryJSONBean, List<CategoryBean> categoryList,
			int index, String categoryId, int idLength, int count, List<Integer> childCatNos,
			String actionType, String catNo) {

		List<CategoryJSONBean> catJsonList= new ArrayList<CategoryJSONBean>();
		CategoryBean categoryBean = new CategoryBean();
		count = count + 1;
		try {
			for(int i = index; i < categoryList.size(); i ++) {
				categoryBean = categoryList.get(i);
				if(categoryBean.getCategoryId().trim().length() != 3) {
					if( (categoryBean.getCategoryId().startsWith(categoryId))
							&& !(childCatNos.contains(categoryBean.getCatNo())) ) {

						CategoryJSONBean catJSONBean = new CategoryJSONBean();
						catJSONBean.setText(categoryBean.getCategoryName());
						catJSONBean.setId(categoryBean.getCategoryId());

						CategoryAttributeJSONBean attributeJSONBean = new CategoryAttributeJSONBean();
						attributeJSONBean.setStartDate(categoryBean.getStartDate());
						attributeJSONBean.setEndDate(categoryBean.getEndDate());
						attributeJSONBean.setStatus(categoryBean.getCategoryStatusCode());
						attributeJSONBean.setCategoryId(categoryBean.getCategoryId());
						attributeJSONBean.setCatNo(String.valueOf(categoryBean.getCatNo()));
						attributeJSONBean.setStartValue(categoryBean.getStartValue());
						attributeJSONBean.setEndValue(categoryBean.getEndValue());
						attributeJSONBean.setStepValue(categoryBean.getStepValue());

						attributeJSONBean.setGoQuicklyCategory(String.valueOf(
								categoryBean.getGoQuicklyCategory() != null ? categoryBean.getGoQuicklyCategory() : ""));
						attributeJSONBean.setGqcDisplayOrder(String.valueOf(
								categoryBean.getGqcDisplayOrder() != null ? categoryBean.getGqcDisplayOrder() : ""));
						attributeJSONBean.setCategoryImageUrl(String.valueOf(
								categoryBean.getCategoryImageUrl() != null ? categoryBean.getCategoryImageUrl() : ""));
						attributeJSONBean.setBestSellingCategory(String.valueOf(
								categoryBean.getBestSellingCategory() != null ? categoryBean.getBestSellingCategory() : "0"));
						attributeJSONBean.setLinkedCategory(
								categoryBean.getLinkedCategory() != null ? categoryBean.getLinkedCategory() : "");
						attributeJSONBean.setLinkedCategoryName(
								categoryBean.getLinkedCategoryName() != null ? categoryBean.getLinkedCategoryName() : "");

						catJSONBean.setA_attr(attributeJSONBean);

						if(!actionType.equalsIgnoreCase("")){
							if(!"".equalsIgnoreCase(catNo)) {
								if(catNo.equalsIgnoreCase(String.valueOf(categoryBean.getCatNo()))) {
									CategoryStateJSONBean categoryStateJSONBean = new CategoryStateJSONBean();
									categoryStateJSONBean.setSelected(true);

									catJSONBean.setState(categoryStateJSONBean);
								}
							}
						}
						childCatNos.add(categoryBean.getCatNo());

						try {
							if(categoryList.get(i + 1).getCategoryId().trim().length() == idLength) {
								hasChild(catJSONBean, categoryList, (i + 1), categoryBean.getCategoryId(),
										((3 * count) + ((count - 1))), count, childCatNos, actionType, catNo);
							}
						} catch (Exception e) {
						}
						catJsonList.add(catJSONBean);
					}
				}
			}
			categoryJSONBean.setChildren(catJsonList);
		} catch (Exception e) {
			logger.error("Exception in CommonUtil hasChild() " + e);
		}
	}

	public void getCategoryDetails(ModelMap model, HttpServletRequest request,
			String catNo, String actionType) {

		try {
			model.addAttribute("categoryBean", new CategoryBean());
			model.addAttribute("categoryBeanId", new CategoryBean());
			model.addAttribute("partnercategoryBean", new PartnerCategoryBean());
			model.addAttribute("categoryList", categoryManagementService.getCategoryList());
			model.addAttribute("partnerList",  partnermanagementService.getAllPartners());

			Map<Byte, String> getEnumValues = commonService.getEnumValues("STATUS");

			model.addAttribute("categoryStatus",  getEnumValues);
			model.addAttribute("categoryStatus_", getEnumValues);
			model.addAttribute("prtCatStatus", getEnumValues);

			List<PartnerCategoryBean> list = new ArrayList<PartnerCategoryBean>();
			List<PartnerCategoryBean> listexcpSearch = new ArrayList<PartnerCategoryBean>();
			list = categoryManagementService.getMappingDetails();
			listexcpSearch = categoryManagementService.getMappingDetailsExceptSearch();
			model.addAttribute("mappingDetailsList", list);
			model.addAttribute("mappingDetailsListCat", listexcpSearch);
			model.addAttribute("mapCategoryTab", false);
			model.addAttribute("mapCategoryUpdateTab", false);

			getDefaultCategoryTree(request, catNo, actionType);

		} catch (Exception e) {
			logger.error(" Exception in CommonUtil getCategoryDetails()" + e);
		}
	}

	public void getDefaultCategoryTree(HttpServletRequest request, String catNo, String actionType) {
		try {
			List<CategoryBean> categoryList =  categoryManagementService.getCategoryList();
			List<CategoryJSONBean> parentCategoryList = categoryManagementService.getCategoryTree(categoryList, 3, true, catNo, actionType);
			request.setAttribute("categoriesList", GsonUtil.toJson(parentCategoryList));
		} catch (Exception e) {
			logger.error(" Exception in CommonUtil getDefaultCategoryTree()" + e);
		}
	}

	public Map<String, String> getAutoSuggProductInfoList(String response) {
		Map<String, String> aspList = new HashMap<String, String>();		
		try {	

			ObjectMapper mapper = new ObjectMapper();
			try {
				// Convert JSON string to Object
				AutoSuggestProducts autoSuggestProducts = mapper.readValue(response, AutoSuggestProducts.class);
				for(com.af.admin.pojo.autosugg.Doc doc:autoSuggestProducts.getResponse().getDocs())				 
					aspList.put(doc.getMASTERPRODUCTID()+"", doc.getPRODUCTNAME().replace("(", "").replace(")", ""));
			} catch (JsonGenerationException e)
			{ e.printStackTrace(); }
			catch (JsonMappingException e)
			{ e.printStackTrace(); }
			catch (IOException e)
			{ e.printStackTrace(); 
			}			

		}catch (Exception e1) {
			logger.error("/getAutoSuggProductInfoList afadmin Exception - e1...:" , e1);
			e1.printStackTrace();
		}
		logger.info("/getAutoSuggProductInfoList - afadmin - aspList-size:" + aspList.size());
		return aspList;
	}

	public Set<String> listOfFilesInFolder(final File folder) {
		Set<String> filesList = new HashSet<String>();
		File[] listOfFiles=null;
		try {
			listOfFiles=folder.listFiles();
			if(listOfFiles!=null)
			for (final File fileEntry : listOfFiles) {
				if (!fileEntry.isDirectory()) {
					//listFilesForFolder(fileEntry);
					filesList.add(fileEntry.getName().contains("success") ? fileEntry.getName() : "");
				}
			}
		} catch (Exception e) {
			logger.error("Exception in CommonUtil listOfFilesInFolder(-)" , e);
			e.printStackTrace();
		}
		return filesList;
	}

	public Map<String, String> getPartnerProductSearchInfoList(String response) {
		Map<String, String> ppList = new HashMap<String, String>();		
		try {	

			ObjectMapper mapper = new ObjectMapper();
			try {
				// Convert JSON string to Object
				SearchPartnerProducts searchPartnerProducts = mapper.readValue(response, SearchPartnerProducts.class);
				for(com.af.admin.pojo.search.Doc doc:searchPartnerProducts.getResponse().getDocs())				 
					ppList.put(doc.getPRTPRDNO()+"", doc.getPRODUCTNAME());//.replace("(", "").replace(")", "")
			} catch (JsonGenerationException e)
			{ e.printStackTrace(); }
			catch (JsonMappingException e)
			{ e.printStackTrace(); }
			catch (IOException e)
			{ e.printStackTrace(); 
			}			

		}catch (Exception e1) {
			logger.error("/getPartnerProductSearchInfoList afadmin Exception - e1...:" , e1);
			e1.printStackTrace();
		}
		
		logger.info("/getPartnerProductSearchInfoList - afadmin - ppList-size:" + ppList.size());
		return ppList;

	}
	
	public static String getStringValue(Object obj){
		return (obj != null)?obj.toString():"";
	}
	public Map<Integer,String> getBrandSearchInfoList(String response) {
		Map<Integer, String> items = new HashMap<Integer, String>();	
				JsonParser jsonParser = new JsonParser();
				JsonElement element = jsonParser.parse(response);
				JsonObject json=element.getAsJsonObject();
				JsonObject json1=(JsonObject) json.get("facet_counts");
				JsonObject json2= (JsonObject) json1.get("facet_fields");
				String je=json2.get("PRODUCT_BRAND").toString();
				Pattern p = Pattern.compile("\"([^\"]*)\"");
				Matcher m = p.matcher(je);
				int count=1;
				while (m.find()) {	
				 // items.add(m.group(1));
					items.put(count, m.group(1));
					count++;
				}
		logger.info("/getBrandSearchInfoList - afadmin - branditems-size:" + items.size());
		return items;

	}
	
	public static String getDate(Date date){
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		return sdf.format(date);
	}	
	
	private static final String HTTPS = "https";

	public void akamaiPurge() throws IOException{

System.out.println("-------------");
logger.info("---------");
//
//	       ClientCredential credential = ClientCredential.builder()
//	               .accessToken("akab-x4wlppiecvifn6sd-s55fiy5ifcuo3qdy")
//	               .clientToken("akab-fze7y5lmjwosbhka-sgdpswuk6aoxkguf")
//	               .clientSecret("r+PeZ2/neBRj+1CxKvMKg9WvcTf+JiHirRUDBJnAlAs=")
//	               .host("akab-5fl66ukjkmqnydng-kn5bsnzjkueyi6kt.purge.akamaiapis.net")
//	               .build();
//
//
//	       ArrayList<String> arrayList = new ArrayList<>();
//
//	       // You can add multiple urls.
//	       arrayList.add("https://vmspreprod.jetprivilege.com/");
//
//	       HttpResponse response = invalidateUrls(credential, arrayList, "staging");
//	       logger.info("parseAsString== "+response.parseAsString());
//	       logger.info("getStatusCode== "+response.getStatusCode());
	   
	}
	public  HttpResponse invalidateUrls(ClientCredential clientCredential, List<String> urls, String network) {

	       HttpTransport httpTransport = new ApacheHttpTransport();
	       HttpRequestFactory requestFactory = httpTransport.createRequestFactory();

	       HttpRequest request = null;

	       try {

	           // This is fast purge approach
	           URI uri = new URI(HTTPS, "api.ccu.akamai.com", "/ccu/v3/delete/url/" + network, null, null);

	           String requestBody = getStringRequestBody(urls);

	           request = requestFactory.buildPostRequest(new GenericUrl(uri), ByteArrayContent.fromString("application/json", requestBody));
	           GoogleHttpClientEdgeGridRequestSigner requestSigner = new GoogleHttpClientEdgeGridRequestSigner(clientCredential);
	           requestSigner.sign(request);

	           return request.execute();

	       } catch (IOException e) {
	           logger.error("IOException in Akamai Utility", e);
	       } catch (RequestSigningException e) {
	        logger.error("RequestSigningException in Akamai Utility", e);
	       } catch (URISyntaxException e) {
	        logger.error("UriSyntaxException in Akamai Utility", e);
	       }

	       return null;
	   }

	   public static String getStringRequestBody(List<String> urls) {

	       Map<String, List<String>> akamaiRequestMap = Maps.newHashMap();
	       akamaiRequestMap.put("objects", urls);

	       return new Gson().toJson(akamaiRequestMap);
	   }
}
