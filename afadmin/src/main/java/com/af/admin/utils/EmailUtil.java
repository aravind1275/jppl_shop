package com.af.admin.utils;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailParseException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

@Component
public class EmailUtil {
	@Autowired
	private JavaMailSender mailSender;

	public void sendMail(String from, String to, String subject, String msg) {
		try {
			MimeMessage Mimemessage = mailSender.createMimeMessage();
			MimeMessageHelper message = new MimeMessageHelper(Mimemessage, true);
			message.setFrom(from);
			message.setTo(to);
			message.setSubject(subject);
			message.setText(msg);
			mailSender.send(Mimemessage);

		} catch (MessagingException e) {
			throw new MailParseException(e);
		}


	}
}
