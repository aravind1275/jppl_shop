package com.af.admin.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.af.admin.beans.EmailManagementBean;
import com.af.admin.service.CommonService;
import com.af.admin.service.EmailManagementService;
import com.af.admin.utils.CommonUtil;

@Controller
public class EmailManagementController {

	private Logger logger = LogManager.getLogger(EmailManagementController.class.getName());

	@Autowired
	CommonService commonService;

	@Autowired
	EmailManagementService emailManagementService;
	
	@Autowired
	CommonUtil commonUtil;

	private String emailManagementBeanStr = "emailManagementBean";
	
	@RequestMapping("/emailManagement")
	public String emailManagement(ModelMap model) {
		List<EmailManagementBean> emailBeanList = null;
		try {
			emailBeanList = emailManagementService.getAllEmail();
		} catch (Exception e) {
			logger.error("@@@@ Exception in EmailManagementController at emailManagement() :", e);
		}
		model.addAttribute("emailBeanList", emailBeanList);
		model.addAttribute(emailManagementBeanStr, new EmailManagementBean());
		
		return "emailManagement";
	}

	@RequestMapping("/addEmail")
	public String addEmail(ModelMap model, HttpServletRequest request) {
		model.addAttribute(emailManagementBeanStr, new EmailManagementBean());
		model.addAttribute("emailStatus", commonService.getEnumValues("STATUS"));
		request.setAttribute("parentMenuId", "2");
		request.setAttribute("menuId", "21");
		return "addEmail";
	}

	@RequestMapping(value = "/addEmail", method = RequestMethod.POST)
	public String addContentSubmit(ModelMap model, @ModelAttribute("emailManagementBean") EmailManagementBean emailManagementBean,
			HttpServletRequest request) {
		emailManagementBean.setUserId((Integer) request.getSession().getAttribute("userId"));
		try {
			emailManagementService.addEmail(emailManagementBean);
			/*try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}
*/
		} catch (Exception e) {
			logger.error("@@@@ Exception in EmailManagementController at addContentSubmit() :", e);
		}
		return "redirect:/admin/emailManagement";
	}

	@RequestMapping(value = "/updateEmail", method = RequestMethod.GET)
	public String updateContent(HttpServletRequest request, ModelMap model) {
		model.addAttribute("emailStatus", commonService.getEnumValues("STATUS"));
		try {
			int emid = Integer.parseInt(request.getParameter("emId"));
			EmailManagementBean emailManagementBean = emailManagementService.getEmailById(emid);
			model.addAttribute("emailManagementBean", emailManagementBean);
		} catch (Exception e) {
			logger.error("@@@@ Exception in EmailManagementController at updateContent() :", e);
		}
		request.setAttribute("parentMenuId", "2");
		request.setAttribute("menuId", "21");
		return "updateEmail";
	}

	@RequestMapping(value = "/updateEmail", method = RequestMethod.POST)
	public String updateEmailSubmit(ModelMap model, @ModelAttribute("emailManagementBean") EmailManagementBean emailManagementBean, HttpServletRequest request, RedirectAttributes redirectAttrs) {
		String status = "";
		try {
			status = emailManagementService.updateEmail(emailManagementBean);
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}
		} catch (Exception e) {
			logger.error("@@@@ Exception in EmailManagementController at updateEmailSubmit() :", e);
		}
		if (status.equalsIgnoreCase("success")) {
			redirectAttrs.addFlashAttribute("successMsg", "Email Template Updated Successfully");
		}
		return "redirect:/admin/emailManagement";
	}

	@RequestMapping(value = "/saveUploadContent", method = RequestMethod.POST)
	public @ResponseBody String saveFile(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
		String responseString = "";
		try {
			responseString = commonService.getFileContent(file, request);
		} catch (Exception e) {
			logger.error("@@@@ Exception in EmailManagementController at saveFile() :", e);
		}
		return responseString;
	}

	@RequestMapping("/checkContentTypeExist")
	public @ResponseBody String contentTypeExist(@RequestParam("contentType") String contentType) {
		return emailManagementService.emailTypeExist(contentType);
	}
}
