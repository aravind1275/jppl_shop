
package com.af.admin.controller;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.af.admin.beans.BannerBean;
import com.af.admin.beans.OffersDealsBean;
import com.af.admin.constants.AfConstants;
import com.af.admin.service.BannerManagementService;
import com.af.admin.service.CommonService;
import com.af.admin.service.PartnerManagementService;
import com.af.admin.utils.CommonUtil;
import com.af.admin.utils.GsonUtil;
import java.io.IOException;
import org.json.JSONArray;

@Controller
public class BannerManagementController {
	private Logger logger = LogManager.getLogger(BannerManagementController.class.getName());
	
	@Autowired
	BannerManagementService bannerManagementService;
	
	@Autowired
	CommonService commonService; 
	
	@Autowired
	PartnerManagementService partnermanagementService;
	
	@Autowired
	CommonUtil commonUtil;
	
	@RequestMapping(value="/bannerManagement",method=RequestMethod.GET)
	public String bannerManagementPage(ModelMap model,HttpServletRequest request){
		String updateMessage=request.getParameter("updateMessage");
		String deleteBnrMsg=request.getParameter("deleteBnrMsg");
		Map<String, List<BannerBean>> bannerList = null;
		bannerList = bannerManagementService.getBanners();
		model.addAttribute("bannerBean",new BannerBean());
		model.addAttribute("bannerSections", commonService.getEnumValues("BANNER_SECTIONS"));
		model.addAttribute("bannerStatus", commonService.getEnumValues("STATUS"));
		model.addAttribute("cBannerList", bannerList.get("cbannerlist"));
		model.addAttribute("sBannerList", bannerList.get("sbannerlist"));
		model.addAttribute("partnerList", partnermanagementService.getPartners());
		
		if(updateMessage!=null)
		{
			if(updateMessage.equalsIgnoreCase(AfConstants.SUCCESS)){
			model.addAttribute(AfConstants.SUCCESS_MESSAGE, "Banner Updated Successfully");
			}
			else if(updateMessage.equalsIgnoreCase(AfConstants.FAILURE)){
			model.addAttribute(AfConstants.ERROR_MESSAGE, "Banner Could  Not Be Updated,Error Occured");
		}
		}
		
		if(deleteBnrMsg!=null)
		{
			if(deleteBnrMsg.equalsIgnoreCase("success")){
			model.addAttribute("message", "Banner Deleted Successfully");
			}
		else if(deleteBnrMsg.equalsIgnoreCase(AfConstants.FAILURE))
			model.addAttribute("errormessage", "Banner Could Not be Deleted! Error Occured");
		}
		
		
		return "bannerManagement";
	}
	
	@RequestMapping(value="/bannerManagement",method=RequestMethod.POST)
	public String bannerManagementPage(@ModelAttribute("bannerBean") BannerBean bannerBean,ModelMap model,RedirectAttributes redirectAttrs,HttpServletRequest request){
	
		String status;
		logger.info("bannerBean.bannerimageurl:"+bannerBean.getBannerImageUrl());
		status=bannerManagementService.createBanner(bannerBean, request);
		/*try {
			commonUtil.akamaiPurge();
		} catch (IOException e) {
			logger.error("Akamai Exception ",e);
		}*/
		model.addAttribute("bannerList", bannerManagementService.getBanners());		
		if(status.equalsIgnoreCase("success"))
			redirectAttrs.addFlashAttribute("message", "Banner Created Successfully");
		else
			redirectAttrs.addFlashAttribute("errormessage", "Banner Could not be Created ");

		return "redirect:/admin/bannerManagement";
	}
	
	@RequestMapping(value="/updateBanner",method=RequestMethod.POST)
	public @ResponseBody String updateBanner(@RequestParam("json") String json,HttpServletRequest request){
		String status=AfConstants.FAILURE;
		try {
			JSONObject userJson=new JSONObject(json);			
			status=bannerManagementService.updateBanner(userJson,request);
			/*try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}*/
		} catch (Exception e) {
			logger.error("@@@@ Exception in BannerManagementController at updateBanner() :", e);
		}
		return status;
	}
	
	@RequestMapping(value="/saveImages",method=RequestMethod.POST)
	public @ResponseBody String saveImagePage(@RequestParam("file") MultipartFile file,@RequestParam("type") String type,HttpServletRequest request){
		String responseString="";
		try{
				responseString=commonService.saveImage(file,type);						
		}catch(Exception e){
			logger.error("@@@@ Exception in BannerManagementController at saveImagePage() :", e);
		}
		logger.debug("/saveImages POST responseString:"+responseString);
		return responseString;
	}
	
	@RequestMapping(value="/deleteBanner")
	public @ResponseBody String deleteUser(@RequestParam("bnrNo") Integer bnrNo,HttpServletRequest request){
		String deleteStatus="";
		try{
			deleteStatus=bannerManagementService.deleteBanner(bnrNo,request);
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in BannerManagementController at deleteUser() :", e);
			deleteStatus="failure";
			return deleteStatus;
		}
		return deleteStatus;
		
	}
	
	@RequestMapping(value="/bannerSaveOrder",method=RequestMethod.POST)
	public @ResponseBody String saveOrder(@RequestParam("reorder") String reorder,HttpServletRequest request){
		
		String status="failure";
		try {
			logger.error("reorder"+reorder);
                       // System.out.println("----------->"+reorder);
			status=bannerManagementService.saveOrder(reorder,request);
		} catch (Exception e) {
			logger.error("@@@@ Exception in BannerManagementController at saveOrder() :", e);
		}
		return status;
	}
	
	@RequestMapping(value="/addBanerCancel",method=RequestMethod.GET)
	public @ResponseBody String addPartnerCancel(ModelMap model){
		return "admin/bannerManagement";
	}
	
	@RequestMapping("/checkBannerID")
	@ResponseBody
	public String checkBannerID(@RequestParam("bannerId") String bannerid) {
		Boolean bannerIDExist = false;
		String bannerId = bannerid;
		bannerId = (null != bannerId) ? bannerId.trim() : "";
		if (bannerId.length() > 0) {
			bannerIDExist = bannerManagementService.checkBannerID(bannerId);
		}
		if (bannerIDExist)
			return "exist";
		else
			return "";
	}
	
	@RequestMapping("/checkUtmParameter")
	@ResponseBody
	public String checkUtmParameter(@RequestParam("json") String json) throws JSONException {
		Boolean bannerIDExist = false;
		JSONObject userJson=new JSONObject(json);
		
			bannerIDExist = bannerManagementService.checkUtmParameter(userJson);
		
		if (bannerIDExist)
			return "exist";
		else
			return "";
	}
	
	@RequestMapping(value = "/BannerPagination", method = RequestMethod.GET)
	public @ResponseBody String getBannerListPagination(@ModelAttribute("bannerBean") BannerBean bannerBean, 
			ModelMap model, HttpServletRequest request,@RequestParam("length") int length,@RequestParam("start") int start,@RequestParam("draw") int draw) throws JSONException {
		String response="";
		String jsonResponseStart = "{\"data\":";
		String jsonResponseEnd = "}";
		 List<BannerBean> bannerBeanList = null;
		 Long bannerBeanListTotal = null;
                try{
//                    System.out.println("Length "+length+"  start "+start+"draw"+draw);
			bannerBeanList=bannerManagementService.getBannerListPagination(length,start);
			bannerBeanListTotal = bannerManagementService.getBannerListPaginationCount();
//                        System.out.println("===================" +bannerBeanList.get(0).toString());
                        
                        
		}catch(Exception e){
			logger.error("@@@@ Exception in BannerManagementController at getBannerListPagination() :", e);
		}
                
               
//                   System.out.println("-----------bannerBeanListTotal--->"+jsArray);
//                   String str=jsonResponseStart+GsonUtil.toJson(bannerBeanList)+",\"draw\":"+draw+",\"recordsTotal\":"+bannerBeanListTotal+",\"recordsFiltered\":"+bannerBeanListTotal+jsonResponseEnd;
                   
                   
		return jsonResponseStart+GsonUtil.toJson(bannerBeanList)+",\"draw\":"+draw+",\"recordsTotal\":"+bannerBeanListTotal+",\"recordsFiltered\":"+bannerBeanListTotal+jsonResponseEnd;
}

}
