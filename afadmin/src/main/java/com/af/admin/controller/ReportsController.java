package com.af.admin.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.af.admin.DAO.ReportsDAO;
import com.af.admin.beans.BuisnessReportsBean;
import com.af.admin.beans.InvalidCsvReprot;
import com.af.admin.beans.JPMilesStatusUpdateBean;
import com.af.admin.beans.zeroCsvReport;
import com.af.admin.constants.AfConstants;
import com.af.admin.service.PartnerManagementService;
import com.af.admin.service.ReportsService;
import com.af.admin.utils.GsonUtil;
import com.af.admin.utils.ServiceProviderUtil;

@Controller
public class ReportsController {
	
	 private static final int BUFFER_SIZE = 4096;


	 
	private Logger logger = LogManager.getLogger(ReportsController.class.getName());

	@Autowired
	ReportsService reportsService;

	@Autowired
	PartnerManagementService partnermanagementService;
	
	@Autowired
	ReportsDAO reportsDAO;

	@Value("${application.af.upload.files.folder}")
	private String fileFolderLocation;
	
	@Value("${application.af.report_download}")
	private String reportFileLocation;
	
	@RequestMapping(value = "/businessReport", method = RequestMethod.POST)
	public String getBusinessReportWithJPNumber(@RequestParam("fromDate") String fromDate,
			@RequestParam("toDate") String toDate, @RequestParam("partnerName") String prtNo,
			@RequestParam("jpNumber") String jpNumber, String status, ModelMap model,
			@RequestParam("validTypeRadio") String validTypeRadio) {
//		System.out.println("entered business report for post request");
		List<String> statusList = new ArrayList<>(
				Arrays.asList("All",AfConstants.JP_NUMBER_MISSING,AfConstants.REFERENCE_NUMBER_MISSING,AfConstants.JP_AND_REFERENCE_NUMBER_MISSING,AfConstants.TENTATIVE, AfConstants.PROCESSED, AfConstants.FAILED,AfConstants.OTHERS));
		List<BuisnessReportsBean> businessReportList = reportsService.getBuisnessReportsList(fromDate, toDate, prtNo,
				jpNumber, status, validTypeRadio,10,0);
		
		model.addAttribute("partnerList", partnermanagementService.getAllPartners());
		model.addAttribute("bussnrptList",businessReportList);
		model.addAttribute("fromDate", fromDate);
		model.addAttribute("toDate", toDate);
		model.addAttribute("prtNo", prtNo);
		model.addAttribute("jpNumber", jpNumber);
		model.addAttribute("status",status);
		model.addAttribute("statusList",statusList);
		model.addAttribute("hiddenVal", validTypeRadio);
		model.addAttribute("length", 10);
		
		List<Object[]> reportObject= reportsDAO.getShopJPReports(fromDate, toDate, prtNo, jpNumber, status, validTypeRadio);		
		model.addAttribute("count", reportObject == null?0:reportObject.size());
		
		
		if (businessReportList == null || businessReportList.isEmpty()) {
			model.addAttribute(AfConstants.SUCCESS_MESSAGE, "No Records Found");
		}

		return "businessReport";
	}
	@RequestMapping(value = "/serverPagination", method = RequestMethod.GET)
	public @ResponseBody String getBusinessReportWithJPNumbers(@RequestParam("fromDate") String fromDate,
			@RequestParam("toDate") String toDate, @RequestParam("partnerName") String prtNo,
			@RequestParam("jpNumber") String jpNumber, String status, ModelMap model,
			@RequestParam("length") int length,@RequestParam("start") int start,@RequestParam("draw") int draw,
			@RequestParam("validTypeRadio") String validTypeRadio,HttpServletRequest request,
			@RequestParam("reportType") String reportType) {
//		System.out.println("entered for ajax server pagination");
		String paginRespnse="";
		String paginRespnse1="";
		try {
		List<String> statusList = new ArrayList<>(
				Arrays.asList("All",AfConstants.JP_NUMBER_MISSING,AfConstants.REFERENCE_NUMBER_MISSING,AfConstants.JP_AND_REFERENCE_NUMBER_MISSING ,AfConstants.TENTATIVE, AfConstants.PROCESSED, AfConstants.FAILED,AfConstants.OTHERS));
		List<BuisnessReportsBean> reportList = new ArrayList<>();
		if(reportType.equalsIgnoreCase("business")){
			reportList=reportsService.getBuisnessReportsList(fromDate, toDate, prtNo,jpNumber, status, validTypeRadio,length,start);
		}
		if(reportType.equalsIgnoreCase("finance")){
			reportList=reportsService.getFinanceReportsList(fromDate, toDate, prtNo, jpNumber, status, validTypeRadio, length, start);
		}
		if(reportType.equalsIgnoreCase("serviceCenter")){
			reportList=reportsService.getServiceCenterReportsList(fromDate, toDate, prtNo, jpNumber, status, validTypeRadio, length, start);
		}
		if(reportType.equalsIgnoreCase("analytics")){
			reportList=reportsService.getAnalyticsReportsList(fromDate, toDate, prtNo, jpNumber, status, validTypeRadio, length, start);
		}
		if(reportType.equalsIgnoreCase("invalid")){
			reportList=reportsService.getInvalidReportsList(fromDate, toDate, prtNo,jpNumber,length,start);
		}		
		if(reportType.equalsIgnoreCase("jpplStatus")){
			reportList=reportsService.getJPMilesStatusUpdateWithJPNumber(fromDate, toDate, prtNo,jpNumber, status, validTypeRadio,length,start);
		}
		String jsonResponseStart = "{\"data\":";
		String jsonResponseEnd = "}";
		
		model.addAttribute("partnerList", partnermanagementService.getAllPartners());
		model.addAttribute("bussnrptList", reportList);
		model.addAttribute("fromDate", fromDate);
		model.addAttribute("toDate", toDate);
		model.addAttribute("prtNo", prtNo);
		model.addAttribute("jpNumber", jpNumber);
		model.addAttribute("status", status);
		model.addAttribute("statusList", statusList);
		model.addAttribute("hiddenVal", validTypeRadio);
		model.addAttribute("length", length);
		List<Object[]> reportObject= reportsDAO.getShopJPReports(fromDate, toDate, prtNo, jpNumber, status, validTypeRadio);
		
		model.addAttribute("count", reportObject.size());
		model.addAttribute("end", start+length);
		if (reportList == null || reportList.isEmpty()) {
			model.addAttribute(AfConstants.SUCCESS_MESSAGE, "No Records Found");
		}
		paginRespnse=jsonResponseStart+GsonUtil.toJson(reportList)+",\"draw\":"+draw+",\"recordsTotal\":"+reportObject.size()+",\"recordsFiltered\":"+reportObject.size()+jsonResponseEnd;
	    JSONObject jsonObject=new JSONObject(paginRespnse);
	    paginRespnse1=""+jsonObject;
		}catch (Exception e) {
			
			logger.error("Exception In Pagination  "+e);
			return "";
			
			// TODO: handle exception
		}
//		return jsonResponseStart+GsonUtil.toJson(reportList)+",\"draw\":"+draw+",\"recordsTotal\":"+reportObject.size()+",\"recordsFiltered\":"+reportObject.size()+jsonResponseEnd;
	
	   return paginRespnse1;
	}

	@RequestMapping(value = "/businessReport", method = RequestMethod.GET)
	public String getBusinessReportWithJPNumberPage(ModelMap model, HttpServletRequest request) {
//		System.out.println("entered business report for get request");
		List<String> statusList = new ArrayList<>(
				Arrays.asList("All",AfConstants.JP_NUMBER_MISSING,AfConstants.REFERENCE_NUMBER_MISSING,AfConstants.JP_AND_REFERENCE_NUMBER_MISSING, AfConstants.TENTATIVE, AfConstants.PROCESSED, AfConstants.FAILED,AfConstants.OTHERS));
		try {
			model.addAttribute("partnerList", partnermanagementService.getAllPartners());
			model.addAttribute("statusList", statusList);
			model.addAttribute("length", 10);
			model.addAttribute("pageNo", 0);
		} catch (Exception e) {
			logger.error("@@@@ Exception in ReportController at getBusinessReportWithJPNumberPage() :", e);
		
		}
		
		return "businessReport";
	}

	@RequestMapping(value = "/financeReport", method = RequestMethod.POST)
	public String getFinanceReportWithJPNumber(@RequestParam("fromDate") String fromDate,
			@RequestParam("toDate") String toDate, @RequestParam("partnerName") String prtNo,
			@RequestParam("jpNumber") String jpNumber, String status, ModelMap model,
			@RequestParam("validTypeRadio") String validTypeRadio) {
		List<BuisnessReportsBean> financeReportList = reportsService.getFinanceReportsList(fromDate, toDate, prtNo,
				jpNumber, status, validTypeRadio, 10, 0);
		List<String> statusList = new ArrayList<>(
				Arrays.asList("All", AfConstants.JP_NUMBER_MISSING,AfConstants.REFERENCE_NUMBER_MISSING,AfConstants.JP_AND_REFERENCE_NUMBER_MISSING ,AfConstants.TENTATIVE, AfConstants.PROCESSED, AfConstants.FAILED,AfConstants.OTHERS));
		model.addAttribute("partnerList", partnermanagementService.getAllPartners());
		model.addAttribute("financerptlist",financeReportList);
		
		model.addAttribute("fromDate", fromDate);
		model.addAttribute("toDate", toDate);
		model.addAttribute("prtNo", prtNo);
		model.addAttribute("jpNumber", jpNumber);
		model.addAttribute("status",status);
		model.addAttribute("statusList",statusList);
		model.addAttribute("hiddenVal", validTypeRadio);
		

		if (financeReportList == null || financeReportList.isEmpty()) {
			model.addAttribute(AfConstants.SUCCESS_MESSAGE, "No Records Found");
		}
		
		return "financeReport";
	}
	
	@RequestMapping(value="/financeReport", method=RequestMethod.GET)
	public String getFinanceReportPage(ModelMap model, HttpServletRequest request) {
		List<String> statusList = new ArrayList<>(
				Arrays.asList("All", AfConstants.JP_NUMBER_MISSING,AfConstants.REFERENCE_NUMBER_MISSING,AfConstants.JP_AND_REFERENCE_NUMBER_MISSING, AfConstants.TENTATIVE, AfConstants.PROCESSED, AfConstants.FAILED,AfConstants.OTHERS));
		try {
			model.addAttribute("partnerList", partnermanagementService.getAllPartners());
			model.addAttribute("statusList", statusList);
		} catch (Exception e) {
			logger.error("@@@@ Exception in ReportController at getFinanceReportPage() :", e);
		}
		return "financeReport";
	}

	@RequestMapping(value = "/analyticsReport", method = RequestMethod.POST)
	public String getAnalyticsReportWithJPNumber(@RequestParam("fromDate") String fromDate,
			@RequestParam("toDate") String toDate, @RequestParam("partnerName") String prtNo,
			@RequestParam("jpNumber") String jpNumber, String status, ModelMap model,
			@RequestParam("validTypeRadio") String validTypeRadio) {
		List<BuisnessReportsBean> analyticsReportlist = reportsService.getAnalyticsReportsList(fromDate, toDate, prtNo,
				jpNumber, status, validTypeRadio, 10, 0);
		List<String> statusList = new ArrayList<>(Arrays.asList("All",AfConstants.JP_NUMBER_MISSING,AfConstants.REFERENCE_NUMBER_MISSING,AfConstants.JP_AND_REFERENCE_NUMBER_MISSING, AfConstants.TENTATIVE, AfConstants.PROCESSED, AfConstants.FAILED,AfConstants.OTHERS));
		model.addAttribute("partnerList", partnermanagementService.getAllPartners());
		model.addAttribute("analyticrptlist",analyticsReportlist);
		
		model.addAttribute("fromDate", fromDate);
		model.addAttribute("toDate", toDate);
		model.addAttribute("prtNo", prtNo);
		model.addAttribute("jpNumber", jpNumber);
		model.addAttribute("status",status);
		model.addAttribute("statusList",statusList);
		model.addAttribute("hiddenVal", validTypeRadio);

		if (analyticsReportlist == null || analyticsReportlist.isEmpty()) {
			model.addAttribute(AfConstants.SUCCESS_MESSAGE, "No Records Found");
		}
		
		return "analyticsReport";
	}
	
	@RequestMapping(value="/analyticsReport", method=RequestMethod.GET)
	public String getAnalyticsReportPage(ModelMap model, HttpServletRequest request) {
		List<String> statusList = new ArrayList<>(Arrays.asList("All",AfConstants.JP_NUMBER_MISSING,AfConstants.REFERENCE_NUMBER_MISSING,AfConstants.JP_AND_REFERENCE_NUMBER_MISSING, AfConstants.TENTATIVE, AfConstants.PROCESSED, AfConstants.FAILED,AfConstants.OTHERS));
		try{
			model.addAttribute("partnerList", partnermanagementService.getAllPartners());
			model.addAttribute("statusList", statusList);
		} catch (Exception e) {
			logger.error("@@@@ Exception in ReportController at getAnalyticsReportPage() :", e);
		}
		return "analyticsReport";
	}

	@RequestMapping(value = "/serviceCenterReport", method = RequestMethod.POST)
	public String getServiceCenterReportJPNumber(@RequestParam("fromDate") String fromDate,
			@RequestParam("toDate") String toDate, @RequestParam("partnerName") String prtNo,
			@RequestParam("jpNumber") String jpNumber, String status, ModelMap model,
			@RequestParam("validTypeRadio") String validTypeRadio) {
		List<BuisnessReportsBean> serviceCenterReportlist = reportsService.getServiceCenterReportsList(fromDate, toDate,
				prtNo, jpNumber, status, validTypeRadio, 10, 0);
		List<String> statusList = new ArrayList<>(Arrays.asList("All",AfConstants.JP_NUMBER_MISSING,AfConstants.REFERENCE_NUMBER_MISSING,AfConstants.JP_AND_REFERENCE_NUMBER_MISSING, AfConstants.TENTATIVE, AfConstants.PROCESSED, AfConstants.FAILED,AfConstants.OTHERS));
		model.addAttribute("partnerList", partnermanagementService.getAllPartners());
		model.addAttribute("serviceCenterrptlist",serviceCenterReportlist);
		
		model.addAttribute("fromDate", fromDate);
		model.addAttribute("toDate", toDate);
		model.addAttribute("prtNo", prtNo);
		model.addAttribute("jpNumber", jpNumber);
		model.addAttribute("status",status);
		model.addAttribute("statusList",statusList);
		model.addAttribute("hiddenVal", validTypeRadio);

		if (serviceCenterReportlist == null || serviceCenterReportlist.isEmpty()) {
			model.addAttribute(AfConstants.SUCCESS_MESSAGE, "No Records Found");
		}
		
		return "serviceCenterReport";
	}
	
	@RequestMapping(value="/serviceCenterReport", method=RequestMethod.GET)
	public String geServiceCenterReportPage(ModelMap model, HttpServletRequest request) {
		List<String> statusList = new ArrayList<>(Arrays.asList("All",AfConstants.JP_NUMBER_MISSING,AfConstants.REFERENCE_NUMBER_MISSING,AfConstants.JP_AND_REFERENCE_NUMBER_MISSING, AfConstants.TENTATIVE, AfConstants.PROCESSED, AfConstants.FAILED,AfConstants.OTHERS));
		try{
			model.addAttribute("partnerList", partnermanagementService.getAllPartners());
			model.addAttribute("statusList", statusList);
		} catch (Exception e) {
			logger.error("@@@@ Exception in ReportController at geServiceCenterReportPage() :", e);
		
		}
		return "serviceCenterReport";
	}

	@RequestMapping(value = "/newProductsReport", method = RequestMethod.POST)
	public String getNewProductsReportPage(@ModelAttribute("newProductsReport") BuisnessReportsBean newProductsReport,
			ModelMap model) {

		String fromDate = newProductsReport.getFromDate();
		String toDate = newProductsReport.getToDate();

		Date startDate = ServiceProviderUtil.getInstance().getDateFromString(fromDate, AfConstants.DATE_FORMAT);
		Date endDate = ServiceProviderUtil.getInstance().getDateFromString(toDate, AfConstants.DATE_FORMAT);

		int daysDiff = ServiceProviderUtil.getInstance().daysBetween(endDate, startDate) + 1;

		if (daysDiff < 32) {
			List<BuisnessReportsBean> newProductsReportlist = reportsService
					.getNewProductsReportList(newProductsReport);

			model.addAttribute("newProductsReport", newProductsReport);
			model.addAttribute("newProductsReportlist", newProductsReportlist);
			model.addAttribute("partnerList", reportsService.getPartnersByProducts());
			model.addAttribute("brandList", reportsService.getBrandsByProducts(toDate, fromDate));

			if (newProductsReportlist == null || newProductsReportlist.isEmpty()) {
				model.addAttribute(AfConstants.SUCCESS_MESSAGE, "No Records Found");
			}
		} else {
			model.addAttribute(AfConstants.SUCCESS_MESSAGE, "Provided date duration is more than 31");
			model.addAttribute("newProductsReport", newProductsReport);
			model.addAttribute("partnerList", reportsService.getPartnersByProducts());

			String firstDate = ServiceProviderUtil.getInstance().getFormattedDate(new Date(), AfConstants.DATE_FORMAT);
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -30);
			String secondDate = ServiceProviderUtil.getInstance().getFormattedDate(cal.getTime(),
					AfConstants.DATE_FORMAT);

			model.addAttribute("brandList", reportsService.getBrandsByProducts(secondDate, firstDate));
		}

		return "newProductsReport";
	}

	@RequestMapping(value = "/mappedProductReport", method = RequestMethod.GET)
	public String getMappedProductReportPage(ModelMap model, HttpServletRequest request) {

		try {
			List<BuisnessReportsBean> mappedProductsReportList = reportsService
					.getMappedProductsReportList(new BuisnessReportsBean());

			model.addAttribute("mappedProductsReportList", mappedProductsReportList);
			model.addAttribute("mappedProductsReport", new BuisnessReportsBean());
		} catch (Exception e) {
			logger.error("@@@@ Exception in ReportController at getMappedProductReportPage() :", e);
		}
		return "mappedProductsReport";
	}

	@RequestMapping(value = "/mappedProductReport", method = RequestMethod.POST)
	public String getMappedProductReportPage(
			@ModelAttribute("mappedProductsReport") BuisnessReportsBean mappedProductsReport, ModelMap model) {

		List<BuisnessReportsBean> mappedProductsReportList = reportsService
				.getMappedProductsReportList(mappedProductsReport);

		model.addAttribute("mappedProductsReport", mappedProductsReport);
		model.addAttribute("mappedProductsReportList", mappedProductsReportList);

		if (mappedProductsReportList == null || mappedProductsReportList.isEmpty()) {
			model.addAttribute("message", "No Records Found");
		}

		return "mappedProductsReport";
	}

	@RequestMapping(value = "/dailyMilesStatusReport", method = RequestMethod.GET)
	public String getDailyMilesReportPage(ModelMap model, HttpServletRequest request) {
		try {
			model.addAttribute("partnerList", partnermanagementService.getAllPartners());
		} catch (Exception e) {
			logger.error("@@@@ Exception in ReportController at geServiceCenterReportPage() :", e);

		}
		return "dailyMilesStatusReport";
	}

	@RequestMapping(value = "/dailyMilesStatusReport", method = RequestMethod.POST)
	public String getDailyMilesReportPage(@RequestParam("fromDate") String fromDate,
			@RequestParam("toDate") String toDate, @RequestParam("partnerName") String prtNo,
			@RequestParam("jpNumber") String jpNumber, String status, ModelMap model) {
		try {
			List<BuisnessReportsBean> dailyMilesReportList = reportsService.getdailyMilesReportsList(fromDate, toDate,
					prtNo, jpNumber);
			model.addAttribute("partnerList", partnermanagementService.getAllPartners());
			model.addAttribute("dailyMilesReportList", dailyMilesReportList);

			model.addAttribute("fromDate", fromDate);
			model.addAttribute("toDate", toDate);
			model.addAttribute("prtNo", prtNo);
			model.addAttribute("jpNumber", jpNumber);
			model.addAttribute("status", status);

			if (dailyMilesReportList == null || dailyMilesReportList.isEmpty()) {
				model.addAttribute("message", "No Records Found");
			}
		} catch (Exception e) {
			logger.error("@@@@ Exception in ReportController at getDailyMilesReportPage() :", e);
		}
		return "dailyMilesStatusReport";
	}

	@RequestMapping("/downloadFile")
	public void downloadXmlFile(@RequestParam("orderId") String orderId, @RequestParam("type") String type,@RequestParam("isXML") Boolean isXml,
			HttpServletResponse response) {
		if ("1".equals(type)) {
				response.setContentType("application/xml");
				response.setHeader("Content-Disposition", "attachment; filename=dailyRequest.xml");
		}
		if ("2".equals(type)){
			if(isXml==false){
				response.setContentType("text/html");
				response.setHeader("Content-Disposition", "attachment; filename=dailyResponse.txt");

			}else{
				response.setContentType("application/xml");
				response.setHeader("Content-Disposition", "attachment; filename=dailyResponse.xml");
			}
		}
		if ("3".equals(type)) {
			response.setContentType("application/xml");
			response.setHeader("Content-Disposition", "attachment; filename=queingRequest.xml");
		}
		if ("4".equals(type)) {
			if(isXml==false){
				response.setContentType("text/html");
				response.setHeader("Content-Disposition", "attachment; filename=queingResponse.txt");

			}else{
				response.setContentType("application/xml");
				response.setHeader("Content-Disposition", "attachment; filename=queingResponse.xml");
			}
		}
		String downloadText = "";
		try {
			downloadText = reportsService.downloadXmlFile(orderId, type);
			if (downloadText != null) {
				ServletOutputStream outStream = response.getOutputStream();
				outStream.println(downloadText);
				outStream.flush();
				outStream.close();
			}
		} catch (Exception e) {
			logger.error("@@@@ Exception in ReportController downloadXmlFile() :", e);
		}

	}


	@RequestMapping(value = "/zeroJpMilesReport", method = RequestMethod.GET)
	public String getZeroJpMilesReportPage(ModelMap model, HttpServletRequest request) {
		List<String> statusList = new ArrayList<>(
				Arrays.asList("All", AfConstants.JP_NUMBER_MISSING,AfConstants.REFERENCE_NUMBER_MISSING,AfConstants.JP_AND_REFERENCE_NUMBER_MISSING,AfConstants.TENTATIVE, AfConstants.PROCESSED, AfConstants.FAILED ,AfConstants.OTHERS));
		try {
			model.addAttribute("statusList", statusList);
			model.addAttribute("partnerList", partnermanagementService.getAllPartners());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("@@@@ Exception in ReportController getZeroJpMilesReportPage() :", e);
		}

		return "zeroJpMilesReport";
	}

	@RequestMapping(value = "/zeroJpMilesReport", method = RequestMethod.POST)
	public String getZeroJpMilesReport(@RequestParam("fromDate") String fromDate, @RequestParam("toDate") String toDate,
			@RequestParam("partnerName") String prtNo, @RequestParam("jpNumber") String jpNumber, String status,
			ModelMap model, @RequestParam("validTypeRadio") String validTypeRadio) {
		List<String> statusList = new ArrayList<>(
				Arrays.asList("All", AfConstants.JP_NUMBER_MISSING,AfConstants.REFERENCE_NUMBER_MISSING,AfConstants.JP_AND_REFERENCE_NUMBER_MISSING,AfConstants.TENTATIVE, AfConstants.PROCESSED, AfConstants.FAILED,AfConstants.OTHERS));
		List<BuisnessReportsBean> zeroJpMilesReport = reportsService.getZeroJpMilesReport(fromDate, toDate, prtNo,
				jpNumber, status, validTypeRadio);
		
		
		try {
			model.addAttribute("zeroJpMilesReport", zeroJpMilesReport);
			model.addAttribute("partnerList", partnermanagementService.getAllPartners());
			model.addAttribute("fromDate", fromDate);
			model.addAttribute("toDate", toDate);
			model.addAttribute("prtNo", prtNo);
			model.addAttribute("jpNumber", jpNumber);
			model.addAttribute("status", status);
			model.addAttribute("statusList", statusList);
			model.addAttribute("hiddenVal", validTypeRadio);

			if (zeroJpMilesReport == null || zeroJpMilesReport.isEmpty()) {
				model.addAttribute(AfConstants.SUCCESS_MESSAGE, "No Records Found");
			}
		} catch (Exception e) {
			
			e.printStackTrace();
		}

		return "zeroJpMilesReport";
	}

	@RequestMapping(value = "/uploadZeroJpmilesFile", method = RequestMethod.POST)
	public String uploadFileHandler(@RequestParam("filename") String name,
			@RequestParam("uploadCSVFile") MultipartFile file, HttpServletRequest request, ModelMap model) {
		String message = "";
		String fileName = "";
		try {
			fileName = new String(file.getBytes());
			String[] lines = fileName.split("\\r?\\n");
			List<zeroCsvReport> reportList = new ArrayList<>();
			zeroCsvReport zeroCsvReport;

			for (int i = 1; i < lines.length; i++) {
				String lines1[] = lines[i].split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
				zeroCsvReport = new zeroCsvReport();

				
				zeroCsvReport.setTransactionId(lines1[1].replaceAll("^\"|\"$", ""));
				zeroCsvReport.setAfffiliateOrderId(lines1[2].replaceAll("^\"|\"$", ""));
				zeroCsvReport.setBaseJpmiles(lines1[3].replaceAll("^\"|\"$", ""));
				zeroCsvReport.setEmailStatus(lines1[4].replaceAll("^\"|\"$", ""));
				zeroCsvReport.setSmsStatus(lines1[5].replaceAll("^\"|\"$", ""));
				if(lines1.length>6){
				zeroCsvReport.setBonusJpmiles(lines1[6].replaceAll("^\"|\"$", ""));
				}
				reportList.add(zeroCsvReport);
				
			}
		
			message = reportsService.updateCsvReport(reportList);
			List<String> statusList = new ArrayList<>(
					Arrays.asList("All", AfConstants.TENTATIVE, AfConstants.PROCESSED, AfConstants.FAILED));
			model.addAttribute("partnerList", partnermanagementService.getAllPartners());
			model.addAttribute("statusList", statusList);
			String mess[] = message.split(",");		
			if (mess[0].equalsIgnoreCase("success")) {
				model.addAttribute(AfConstants.SUCCESS_MESSAGE, "File Uploaded Successfully");
			} else if (mess[0].equalsIgnoreCase("failure")) {
				model.addAttribute(AfConstants.ERROR_MESSAGE, "File Upload Failed");
			}
			 else if (mess[0].equalsIgnoreCase(AfConstants.EMAIL_SMS_ERROR)) {
					model.addAttribute(AfConstants.ERROR_MESSAGE, "Email or Sms status are non supporting"+mess[1]);
				}
			 else if (mess[0].equalsIgnoreCase(AfConstants.ORDER_TRANS_ERROR)) {
					model.addAttribute(AfConstants.ERROR_MESSAGE, "Transaction id and Order id are not correct");
				}
		} catch (Exception e) {
			model.addAttribute(AfConstants.ERROR_MESSAGE, "File Upload Failed");
			e.printStackTrace();
			logger.error("@@@@ Exception in ReportController uploadFileHandler() :", e);
		}

		return "zeroJpMilesReport";
	}

	@RequestMapping(value = "/uploadZeroJpmilesFile", method = RequestMethod.GET)
	public String uploadFileHandlerPage(ModelMap model, HttpServletRequest request) {
		List<String> statusList = new ArrayList<>(
				Arrays.asList("All", AfConstants.TENTATIVE, AfConstants.PROCESSED, AfConstants.FAILED));
		try {
			model.addAttribute("statusList", statusList);
			model.addAttribute("partnerList", partnermanagementService.getAllPartners());
		} catch (Exception e) {
			logger.error("@@@@ Exception in ReportController getZeroJpMilesReportPage() :", e);
		}

		return "zeroJpMilesReport";

	}
	
	@RequestMapping(value= "/queingLogicReport",  method= RequestMethod.GET)
	public String getqueingLogicReport(ModelMap model) {

		try {
			model.addAttribute("partnerList", partnermanagementService.getAllPartners());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("@@@Exception in ReportController getqueingLogicReport():", e);
		}
		return "queingLogicReport";
	}

	@RequestMapping(value= "/queingLogicReport", method=RequestMethod.POST)
	public String queingLogicReportPage(@RequestParam("fromDate") String fromDate, @RequestParam("toDate") 
			String toDate, @RequestParam("partnerName") String prtNo,@RequestParam("jpNumber") String jpNumber , ModelMap model){

		try {
			List<BuisnessReportsBean> queingreportBeanList = reportsService.getqueingLogicReportList(fromDate, toDate, prtNo, jpNumber);
			model.addAttribute("partnerList", partnermanagementService.getAllPartners());
			model.addAttribute("queingreportBeanList", queingreportBeanList);
			model.addAttribute("fromDate", fromDate);
			model.addAttribute("toDate", toDate);
			model.addAttribute("prtNo", prtNo);
			model.addAttribute("jpNumber", jpNumber);

			if (queingreportBeanList==null || queingreportBeanList.isEmpty()) {
				model.addAttribute("message", "No records found");
			}
		} catch(Exception e) {
			logger.error("@@@Exception inReportsController getqueingLogicReport():", e);
		}

		return "queingLogicReport";
	}
	
	
	@RequestMapping(value = "/exportBusinessCsv", method = RequestMethod.GET)
	public void exportBusinessCsv(@RequestParam("fromDate") String fromDate,
			@RequestParam("toDate") String toDate, @RequestParam("partnerName") String prtNo,
			@RequestParam("jpNumber") String jpNumber, String status, ModelMap model,
			@RequestParam("validTypeRadio") String validTypeRadio,HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		 String fileName = "BusinessReport_"+new SimpleDateFormat("ddMMyyyyHHmmss'.csv'").format(new Date());
		 String CSV_LOCATION = reportFileLocation+"/"+fileName;
		 String COMMA_DELIMITER = ",";
		 String NEW_LINE_SEPARATOR = "\n";
		 String FILE_HEADER = "JP_NUMBER,Reference Number,Affiliate order number,Partner Order Id, order Date,Merchant,partner Category,Jppl Category,Product,price,Partner Commission rate,partner commission Amount,jppl commission Amount,% jppl Commission,% of commission given to member,tax%,No. of jpmiles,"
		 		+ "expected date of credit,jpmiles credited,jpmiles declined,status,source,medium,campaign,member/guest,transaction vai";
		 
		 FileWriter fileWriter = null;
//		System.out.println("insdei report");

		 try { 
			 List<BuisnessReportsBean> reportList = reportsService.getExportBuisnessReportsList(fromDate, toDate, prtNo,
						jpNumber, status, validTypeRadio);
//			 System.out.println("report list in controller --------->"+reportList.size());
			 fileWriter = new FileWriter(CSV_LOCATION);
			 fileWriter.append(FILE_HEADER.toString());
			 for(BuisnessReportsBean buisnessReportsBean: reportList){
				 
//				 System.out.println("jpNumber ---> "+buisnessReportsBean.getJpNumber());
//				 System.out.println("getTransactionId ---> "+buisnessReportsBean.getTransactionId());
//				 System.out.println("getPartnerItemId ---> "+buisnessReportsBean.getPartnerItemId());
//				 System.out.println("getPartnerOrderId ---> "+buisnessReportsBean.getPartnerOrderId());
//				 System.out.println("getFormatedTransactionDate ---> "+buisnessReportsBean.getFormatedTransactionDate());
//				 System.out.println("getPartnerName ---> "+buisnessReportsBean.getPartnerName());
//				 System.out.println("getPartnerCategoryName ---> "+buisnessReportsBean.getPartnerCategoryName());
//				 System.out.println("getJpplCategoryName ---> "+buisnessReportsBean.getJpplCategoryName());
//				 System.out.println("getProductName ---> "+buisnessReportsBean.getProductName());
//				 System.out.println("getPrice ---> "+buisnessReportsBean.getPrice());
//				 System.out.println("getCommissionRate ---> "+buisnessReportsBean.getCommissionRate());
//				 System.out.println("getCommAmount ---> "+buisnessReportsBean.getCommAmount());
//				 System.out.println("getJpplCommAmount ---> "+buisnessReportsBean.getJpplCommAmount());
//				 System.out.println("getCommissionRate ---> "+buisnessReportsBean.getCommissionRate());
//				 System.out.println("getMemberShare ---> "+buisnessReportsBean.getMemberShare());
//				 System.out.println("getTax ---> "+buisnessReportsBean.getTax());
//				 System.out.println("getJpmilesPurchase().toString() ---> "+buisnessReportsBean.getJpmilesPurchase().toString());
//				 System.out.println("getFormatedExpectedDateOfCredit ---> "+buisnessReportsBean.getFormatedExpectedDateOfCredit());
//				 System.out.println("getJpmilesToCredit().toString() ---> "+buisnessReportsBean.getJpmilesToCredit().toString());
//				 System.out.println("getJpmilesCancel ---> "+buisnessReportsBean.getJpmilesCancel());
//				 System.out.println("getPurchase_status ---> "+buisnessReportsBean.getPurchase_status());
//				 System.out.println("getMedium ---> "+buisnessReportsBean.getMedium());
//				 System.out.println("getCampaign ---> "+buisnessReportsBean.getCampaign());
//				 System.out.println("getMember_or_guest ---> "+buisnessReportsBean.getMember_or_guest());
//				 System.out.println("getTransactionVia ---> "+buisnessReportsBean.getTransactionVia());
				 
				 
				 
				 
			 fileWriter.append(NEW_LINE_SEPARATOR);
			 fileWriter.append("\""+buisnessReportsBean.getJpNumber()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getTransactionId()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getPartnerItemId()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
             fileWriter.append("\""+buisnessReportsBean.getPartnerOrderId()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getFormatedTransactionDate()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getPartnerName()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getPartnerCategoryName()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getJpplCategoryName()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getProductName()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getPrice()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getCommissionRate()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getCommAmount()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getJpplCommAmount()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getCommissionRate()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getMemberShare()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getTax()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getJpmilesPurchase().toString()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getFormatedExpectedDateOfCredit()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getJpmilesToCredit().toString()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getJpmilesCancel().toString()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getPurchase_status()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getSource()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getMedium()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getCampaign()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getMember_or_guest()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getTransactionVia()+"\"");			
			 fileWriter.append(COMMA_DELIMITER);
			 }


			 	
    
		 }catch (Exception e) {
			 logger.error("@@@Exception in ReportsController exportBusinessCsv():", e);
		}
		 finally {
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				logger.error("@@@Exception in ReportsController exportBusinessCsv():", e);
			}
		}
		   File downloadFile = new File(CSV_LOCATION);
	        FileInputStream inputStream = new FileInputStream(downloadFile);
	         
	        
	           String  mimeType = "application/octet-stream";
	        
	        response.setContentType(mimeType);
	        response.setContentLength((int) downloadFile.length());
	 
	        
	        String headerKey = "Content-Disposition";
	        String headerValue = String.format("attachment; filename=\"%s\"",
	                downloadFile.getName());
	        response.setHeader(headerKey, headerValue);
	 
	       
	        OutputStream outStream = response.getOutputStream();
	 
	        byte[] buffer = new byte[BUFFER_SIZE];
	        int bytesRead = -1;
	 
	        
	        while ((bytesRead = inputStream.read(buffer)) != -1) {
	            outStream.write(buffer, 0, bytesRead);
	        }
	 
	        inputStream.close();
	        outStream.close();

		
	}
	@RequestMapping(value = "/exportFinanceCsv", method = RequestMethod.GET)
	public void exportFinanceCsv(@RequestParam("fromDate") String fromDate,
			@RequestParam("toDate") String toDate, @RequestParam("partnerName") String prtNo,
			@RequestParam("jpNumber") String jpNumber, String status, ModelMap model,
			@RequestParam("validTypeRadio") String validTypeRadio,HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		
		 String fileName = "FinanceReport_"+new SimpleDateFormat("ddMMyyyyHHmmss'.csv'").format(new Date());
		 String CSV_LOCATION = reportFileLocation+"/"+fileName;
		 String COMMA_DELIMITER = ",";
		 String NEW_LINE_SEPARATOR = "\n";
		 String FILE_HEADER = "Jp Number,Affiliate order number,% jppl Commission,% of commission given to member,Partner Commission Rate,"
		 		+ "Partner commission Amount,Jppl commission Amount,tax %,Jp Miles Credited,"
		 		+ "Status,Source,Medium,Campaign,Member/Guest,Transaction vai";
		 
		 FileWriter fileWriter = null;
		

		 try { 
			 List<BuisnessReportsBean> reportList = reportsService.getExportFinanceReportsList(fromDate, toDate, prtNo,
						jpNumber, status, validTypeRadio);	
			 fileWriter = new FileWriter(CSV_LOCATION);
			 fileWriter.append(FILE_HEADER.toString());
			 for(BuisnessReportsBean buisnessReportsBean: reportList){
			 fileWriter.append(NEW_LINE_SEPARATOR);
			 fileWriter.append("\""+buisnessReportsBean.getJpNumber()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 
			 fileWriter.append("\""+buisnessReportsBean.getPartnerItemId()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getCommissionRate()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getMemberShare()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getCommissionRate()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getCommAmount()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getJpplCommAmount()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getTax()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getJpmilesToCredit().toString()+"\"");
			 fileWriter.append(COMMA_DELIMITER);			 
			 fileWriter.append("\""+buisnessReportsBean.getPurchase_status()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getSource()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getMedium()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getCampaign()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getMember_or_guest()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getTransactionVia()+"\"");			
			 fileWriter.append(COMMA_DELIMITER);
			 
			 }


			 	
    
		 }catch (Exception e) {
			 logger.error("@@@Exception in ReportsController exportFinanceCsv():", e);
		}
		 finally {
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				
				 logger.error("@@@Exception in ReportsController exportFinanceCsv():", e);
			}
		}
		 File downloadFile = new File(CSV_LOCATION);
	        FileInputStream inputStream = new FileInputStream(downloadFile);
	         
	        
	           String  mimeType = "application/octet-stream";
	       
	        response.setContentType(mimeType);
	        response.setContentLength((int) downloadFile.length());
	 
	       
	        String headerKey = "Content-Disposition";
	        String headerValue = String.format("attachment; filename=\"%s\"",
	                downloadFile.getName());
	        response.setHeader(headerKey, headerValue);
	 
	       
	        OutputStream outStream = response.getOutputStream();
	 
	        byte[] buffer = new byte[BUFFER_SIZE];
	        int bytesRead = -1;
	 
	       
	        while ((bytesRead = inputStream.read(buffer)) != -1) {
	            outStream.write(buffer, 0, bytesRead);
	        }
	 
	        inputStream.close();
	        outStream.close();

		
	}
	
	
	@RequestMapping(value = "/exportAnalyticsCsv", method = RequestMethod.GET)
	public void exportAnalyticsCsv(@RequestParam("fromDate") String fromDate,
			@RequestParam("toDate") String toDate, @RequestParam("partnerName") String prtNo,
			@RequestParam("jpNumber") String jpNumber, String status, ModelMap model,
			@RequestParam("validTypeRadio") String validTypeRadio,HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		
		String fileName = "AnalyticsReport_"+new SimpleDateFormat("ddMMyyyyHHmmss'.csv'").format(new Date());
		 String CSV_LOCATION = reportFileLocation+"/"+fileName;
		 String COMMA_DELIMITER = ",";
		 String NEW_LINE_SEPARATOR = "\n";
		 String FILE_HEADER = "Jp Number,Reference Number,Affiliate order number,order Date,Merchant,partner Category,Jppl Category,Product,price,No of JpMiles,Partner Commission Rate,Partner Commission Amount,Jppl Commission Amount,"+
		  "expected date of credit,jpmiles credited,jpmiles declined,status,source,medium,campaign,member/guest,transaction vai";
		 
		 FileWriter fileWriter = null;
		

		 try { 
			 List<BuisnessReportsBean> reportList = reportsService.getExportAnalyticsReportsList(fromDate, toDate, prtNo,
						jpNumber, status, validTypeRadio);	
			 fileWriter = new FileWriter(CSV_LOCATION);
			 fileWriter.append(FILE_HEADER.toString());
			 for(BuisnessReportsBean buisnessReportsBean: reportList){
			 fileWriter.append(NEW_LINE_SEPARATOR);
			 fileWriter.append("\""+buisnessReportsBean.getJpNumber()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getTransactionId()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getPartnerItemId()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getFormatedTransactionDate()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getPartnerName()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getPartnerCategoryName()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getJpplCategoryName()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getProductName()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getPrice()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getJpmilesPurchase().toString()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getRatePerMile()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getCommAmount()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getJpplCommAmount()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getFormatedExpectedDateOfCredit()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getJpmilesToCredit().toString()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getJpmilesCancel().toString()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getPurchase_status()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getSource()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getMedium()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getCampaign()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getMember_or_guest()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getTransactionVia()+"\"");			
			 fileWriter.append(COMMA_DELIMITER);
			 } 
		 }catch (Exception e) {
			 logger.error("@@@Exception in ReportsController exportAnalyticsCsv():", e);
		}
		 finally {
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				logger.error("@@@Exception in ReportsController exportAnalyticsCsv():", e);
			}
		}
		 File downloadFile = new File(CSV_LOCATION);
	        FileInputStream inputStream = new FileInputStream(downloadFile);
	         
	       
	           String  mimeType = "application/octet-stream";

	        response.setContentType(mimeType);
	        response.setContentLength((int) downloadFile.length());
	 
	        String headerKey = "Content-Disposition";
	        String headerValue = String.format("attachment; filename=\"%s\"",
	                downloadFile.getName());
	        response.setHeader(headerKey, headerValue);
	 
	        OutputStream outStream = response.getOutputStream();
	 
	        byte[] buffer = new byte[BUFFER_SIZE];
	        int bytesRead = -1;
	 
	        while ((bytesRead = inputStream.read(buffer)) != -1) {
	            outStream.write(buffer, 0, bytesRead);
	        }
	 
	        inputStream.close();
	        outStream.close();

		
	}
	
	
	
	@RequestMapping(value = "/exportServiceCenterCsv", method = RequestMethod.GET)
	public void exportCsv(@RequestParam("fromDate") String fromDate,
			@RequestParam("toDate") String toDate, @RequestParam("partnerName") String prtNo,
			@RequestParam("jpNumber") String jpNumber, String status, ModelMap model,
			@RequestParam("validTypeRadio") String validTypeRadio,HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		
		String fileName = "ServiceCenterReport_"+new SimpleDateFormat("ddMMyyyyHHmmss'.csv'").format(new Date());
		 String CSV_LOCATION = reportFileLocation+"/"+fileName;
		 String COMMA_DELIMITER = ",";
		 String NEW_LINE_SEPARATOR = "\n";
		 String FILE_HEADER = "JP_NUMBER,Reference Number,Affiliate order number,Order Date,Merchant,Partner Category,Jppl Category,Product,Price,No. Of JpMiles,Partner Commission Rate,Partner Commission Amount,Jppl Commission Amount,"
		 		+ "expected date of credit,jpmiles credited,jpmiles declined,status,source,medium,campaign,member/guest,transaction vai";
		 
		 FileWriter fileWriter = null;
		

		 try { 
			 List<BuisnessReportsBean> reportList = reportsService.getExportServiceCenterReportsList(fromDate, toDate, prtNo,
						jpNumber, status, validTypeRadio);	
			 fileWriter = new FileWriter(CSV_LOCATION);
			 fileWriter.append(FILE_HEADER.toString());
			 for(BuisnessReportsBean buisnessReportsBean: reportList){
			 fileWriter.append(NEW_LINE_SEPARATOR);
			 fileWriter.append("\""+buisnessReportsBean.getJpNumber()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getTransactionId()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getPartnerItemId()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getFormatedTransactionDate()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getPartnerName()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getPartnerCategoryName()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getJpplCategoryName()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getProductName()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getPrice()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getCommissionRate()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getCommAmount()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getJpplCommAmount()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getCommissionRate()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getMemberShare()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getTax()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getJpmilesPurchase().toString()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getFormatedExpectedDateOfCredit()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getJpmilesToCredit().toString()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getJpmilesCancel().toString()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getPurchase_status()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getSource()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getMedium()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getCampaign()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getMember_or_guest()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getTransactionVia()+"\"");			
			 fileWriter.append(COMMA_DELIMITER);
			 }


			 	
    
		 }catch (Exception e) {
			 logger.error("@@@Exception in ReportsController ServiceCenter():", e);
		}
		 finally {
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				logger.error("@@@Exception in ReportsController ServiceCenter():", e);
			}
		}
		 File downloadFile = new File(CSV_LOCATION);
	        FileInputStream inputStream = new FileInputStream(downloadFile);
	         
	           String  mimeType = "application/octet-stream";
	 
	        response.setContentType(mimeType);
	        response.setContentLength((int) downloadFile.length());
	 
	        String headerKey = "Content-Disposition";
	        String headerValue = String.format("attachment; filename=\"%s\"",
	                downloadFile.getName());
	        response.setHeader(headerKey, headerValue);
	 
	        OutputStream outStream = response.getOutputStream();
	 
	        byte[] buffer = new byte[BUFFER_SIZE];
	        int bytesRead = -1;
	 
	        while ((bytesRead = inputStream.read(buffer)) != -1) {
	            outStream.write(buffer, 0, bytesRead);
	        }
	 
	        inputStream.close();
	        outStream.close();

		
	}
	
	
	@RequestMapping(value = "/invalidReport", method = RequestMethod.GET)
	public String getInvalidReport(ModelMap model, HttpServletRequest request) {
		
		try {
			model.addAttribute("partnerList", partnermanagementService.getAllPartners());
			model.addAttribute("length", 10);
			model.addAttribute("pageNo", 0);
		} catch (Exception e) {
			logger.error("@@@@ Exception in ReportController at getInvalidReport() :", e);
		
		}
		
		return "invalidReport";
	}
	
	@RequestMapping(value = "/invalidReport", method = RequestMethod.POST)
	public String getInvalidReportWithJPNumber(@RequestParam("fromDate") String fromDate,
			@RequestParam("toDate") String toDate, @RequestParam("partnerName") String prtNo,
			@RequestParam("jpNumber") String jpNumber, ModelMap model) {
		List<BuisnessReportsBean> invalidReportList = reportsService.getInvalidReportsList(fromDate, toDate, prtNo,
				jpNumber, 10, 0);
	
		model.addAttribute("partnerList", partnermanagementService.getAllPartners());
		model.addAttribute("invalidReportList",invalidReportList);
		
		model.addAttribute("fromDate", fromDate);
		model.addAttribute("toDate", toDate);
		model.addAttribute("prtNo", prtNo);
		model.addAttribute("jpNumber", jpNumber);
	

		if (invalidReportList == null || invalidReportList.isEmpty()) {
			model.addAttribute(AfConstants.SUCCESS_MESSAGE, "No Records Found");
		}
		
		return "invalidReport";
	}
	
	
	@RequestMapping(value = "/exportInvalidCsv", method = RequestMethod.GET)
	public void exportInvalidCsv(@RequestParam("fromDate") String fromDate,
			@RequestParam("toDate") String toDate, @RequestParam("partnerName") String prtNo,
			@RequestParam("jpNumber") String jpNumber, ModelMap model,HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		 String fileName = "Invalid_Report_"+new SimpleDateFormat("ddMMyyyyHHmmss'.csv'").format(new Date());
		 String CSV_LOCATION = reportFileLocation+"/"+fileName;
		 String COMMA_DELIMITER = ",";
		 String NEW_LINE_SEPARATOR = "\n";
		 String FILE_HEADER = "JP_NUMBER,Reference Number,Affiliate order number,order Date,partner Name,partner Category,Product Id,Product Name,message,Tracking Id";
		 
		 FileWriter fileWriter = null;
		

		 try { 
			 List<BuisnessReportsBean> reportList = reportsService.getExportInvalidReportsList(fromDate, toDate, prtNo,
						jpNumber);	
			 fileWriter = new FileWriter(CSV_LOCATION);
			 fileWriter.append(FILE_HEADER.toString());
			 for(BuisnessReportsBean buisnessReportsBean: reportList){
			 fileWriter.append(NEW_LINE_SEPARATOR);
			 fileWriter.append("\""+buisnessReportsBean.getJpNumber()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getTransactionId()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getPartnerItemId()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getFormatedTransactionDate()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getPartnerName()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getPartnerCategoryName()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getPartnerProductId()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getProductName()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getMessage()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getTrackingId()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			
			 }


			 	
    
		 }catch (Exception e) {
			 logger.error("@@@Exception in ReportsController exportBusinessCsv():", e);
		}
		 finally {
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				logger.error("@@@Exception in ReportsController exportBusinessCsv():", e);
			}
		}
		   File downloadFile = new File(CSV_LOCATION);
	        FileInputStream inputStream = new FileInputStream(downloadFile);
	         
	        
	           String  mimeType = "application/octet-stream";
	        
	        response.setContentType(mimeType);
	        response.setContentLength((int) downloadFile.length());
	 
	        
	        String headerKey = "Content-Disposition";
	        String headerValue = String.format("attachment; filename=\"%s\"",
	                downloadFile.getName());
	        response.setHeader(headerKey, headerValue);
	 
	       
	        OutputStream outStream = response.getOutputStream();
	 
	        byte[] buffer = new byte[BUFFER_SIZE];
	        int bytesRead = -1;
	 
	        
	        while ((bytesRead = inputStream.read(buffer)) != -1) {
	            outStream.write(buffer, 0, bytesRead);
	        }
	 
	        inputStream.close();
	        outStream.close();

		
	}
	@RequestMapping(value = "/serverPaginationInvalid", method = RequestMethod.GET)
	public @ResponseBody String getInvalidReportWithJPNumbers(@RequestParam("fromDate") String fromDate,
			@RequestParam("toDate") String toDate, @RequestParam("partnerName") String prtNo,
			@RequestParam("jpNumber") String jpNumber, String status, ModelMap model,
			@RequestParam("length") int length,@RequestParam("start") int start,@RequestParam("draw") int draw,HttpServletRequest request,
			@RequestParam("reportType") String reportType) {
		
		List<BuisnessReportsBean> reportList = new ArrayList<>();
		if(reportType.equalsIgnoreCase("invalid")){
			reportList=reportsService.getInvalidReportsList(fromDate, toDate, prtNo,jpNumber,length,start);
		}		
		String jsonResponseStart = "{\"data\":";
		String jsonResponseEnd = "}";
		
		model.addAttribute("partnerList", partnermanagementService.getAllPartners());
		model.addAttribute("bussnrptList", reportList);
		model.addAttribute("fromDate", fromDate);
		model.addAttribute("toDate", toDate);
		model.addAttribute("prtNo", prtNo);
		model.addAttribute("jpNumber", jpNumber);
		model.addAttribute("length", length);
		List<Object[]> reportObject= reportsDAO.getShopInValidReports(fromDate, toDate, prtNo, jpNumber);
		
		model.addAttribute("count", reportObject.size());
		model.addAttribute("end", start+length);
		if (reportList == null || reportList.isEmpty()) {
			model.addAttribute(AfConstants.SUCCESS_MESSAGE, "No Records Found");
		}
		return jsonResponseStart+GsonUtil.toJson(reportList)+",\"draw\":"+draw+",\"recordsTotal\":"+reportObject.size()+",\"recordsFiltered\":"+reportObject.size()+jsonResponseEnd;
	}
	
	@RequestMapping(value = "/uploadInvalidFile", method = RequestMethod.GET)
	public String getuploadInvalidFile(ModelMap model, HttpServletRequest request) {
		
		try {
			model.addAttribute("partnerList", partnermanagementService.getAllPartners());
		} catch (Exception e) {
			logger.error("@@@@ Exception in ReportController getuploadInvalidFile() :", e);
		}

		return "invalidReport";

	}
	
	@RequestMapping(value = "/uploadInvalidFile", method = RequestMethod.POST)
	public String uploadInvalidFilePage(@RequestParam("filename") String name,
			@RequestParam("uploadCSVFile") MultipartFile file, HttpServletRequest request, ModelMap model) {
		String message = "";
		String fileName = "";
		try {
			fileName = new String(file.getBytes());
			String[] lines = fileName.split("\\r?\\n");
			List<InvalidCsvReprot> reportList = new ArrayList<>();
			InvalidCsvReprot invalidCsvReprot = null;

			for (int i = 1; i < lines.length; i++) {
				String lines1[] = lines[i].split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
				invalidCsvReprot = new InvalidCsvReprot();

				
				invalidCsvReprot.setTransactionId(lines1[1].replaceAll("^\"|\"$", ""));
				invalidCsvReprot.setAfffiliateOrderId(lines1[2].replaceAll("^\"|\"$", ""));
				invalidCsvReprot.setProductName(lines1[7].replaceAll("^\"|\"$", ""));
				invalidCsvReprot.setPartnerCategoryName(lines1[5].replaceAll("^\"|\"$", ""));
				invalidCsvReprot.setPartnerName(lines1[4].replaceAll("^\"|\"$", ""));
				//invalidCsvReprot.setTrackingId(lines1[6].replaceAll("^\"|\"$", ""));
				
				reportList.add(invalidCsvReprot);
				
			}
		
			message = reportsService.updateInvalidCsvReport(reportList);
			model.addAttribute("partnerList", partnermanagementService.getAllPartners());
			String[] mess = message.split(" ");	
			if (message.equalsIgnoreCase("success")) {
				model.addAttribute(AfConstants.SUCCESS_MESSAGE, "File Uploaded Successfully");
			} else if (message.equalsIgnoreCase("failure")) {
				model.addAttribute(AfConstants.ERROR_MESSAGE, "File Upload Failed");
			}
			else if (mess[0].equalsIgnoreCase("failure")) {
				model.addAttribute(AfConstants.ERROR_MESSAGE,"Row " +mess[1]+"is not updated");
			}
		} catch (Exception e) {
			logger.error("@@@@ Exception in @ReportController uploadFileHandler() :", e);
		}

		return "invalidReport";
	}

	@RequestMapping(value = "/jpMilesStatusUpdate", method = RequestMethod.GET)
	public String getJPMilesStatusUpdate(ModelMap model, HttpServletRequest request) {
		String updateMsg = request.getParameter("updateMsg");
		List<String> statusList = new ArrayList<>(
				Arrays.asList("All", AfConstants.TENTATIVE, AfConstants.PROCESSED, AfConstants.FAILED));
		try {
			model.addAttribute("partnerList", partnermanagementService.getAllPartners());
			model.addAttribute("statusList", statusList);
			model.addAttribute("length", 10);
			model.addAttribute("pageNo", 0);
			
			if(updateMsg != null) {
				if(updateMsg.equalsIgnoreCase(AfConstants.SUCCESS))
					model.addAttribute(AfConstants.SUCCESS_MESSAGE, "Status updated Successfully");
				else if(updateMsg.equalsIgnoreCase(AfConstants.FAILURE))
					model.addAttribute(AfConstants.ERROR_MESSAGE, "Status Could not be updated");
				}
		
		} catch (Exception e) {
			logger.error("@@@@ Exception in @ReportController at getJPMilesStatusUpdate() :", e);
		
		}
		return "jpMilesStatusUpdate";
	}

	

	@RequestMapping(value = "/jpMilesStatusUpdate", method = RequestMethod.POST)
	public String getJPMilesStatusUpdateWithJPNumber(@RequestParam("fromDate") String fromDate,
			@RequestParam("toDate") String toDate, @RequestParam("partnerName") String prtNo,
			@RequestParam("jpNumber") String jpNumber, String status, ModelMap model,
			@RequestParam("validTypeRadio") String validTypeRadio) {
		List<String> statusList = new ArrayList<>(
				Arrays.asList("All", AfConstants.TENTATIVE, AfConstants.PROCESSED, AfConstants.FAILED));
		List<BuisnessReportsBean> jpMilesStatusUpdateBeanList = reportsService.getJPMilesStatusUpdateWithJPNumber(fromDate, toDate, prtNo,
				jpNumber, status, validTypeRadio,10,0);
		
		model.addAttribute("partnerList", partnermanagementService.getAllPartners());
		model.addAttribute("jpMilesStatusUpdateList",jpMilesStatusUpdateBeanList);
		model.addAttribute("fromDate", fromDate);
		model.addAttribute("toDate", toDate);
		model.addAttribute("prtNo", prtNo);
		model.addAttribute("jpNumber", jpNumber);
		model.addAttribute("status",status);
		model.addAttribute("statusList",statusList);
		model.addAttribute("hiddenVal", validTypeRadio);
		model.addAttribute("length", 10);
		
		List<Object[]> reportObject= reportsDAO.getJPMilesStatusReports(fromDate, toDate, prtNo, jpNumber, status, validTypeRadio);		
		model.addAttribute("count", reportObject == null?0:reportObject.size());
		
		
		if (jpMilesStatusUpdateBeanList == null || jpMilesStatusUpdateBeanList.isEmpty()) {
			model.addAttribute(AfConstants.SUCCESS_MESSAGE, "No Records Found");
		}

		return "jpMilesStatusUpdate";
	}
	@RequestMapping(value = "/jpplCreditUpdate", method = RequestMethod.POST)
	public @ResponseBody String jpplCreditUpdate(@RequestParam("json") String json,ModelMap model,HttpServletRequest request,RedirectAttributes redirectAttrs) {
		String status = "";
		try{
			JSONObject userJson=new JSONObject(json);			
 			status=reportsService.jpplCreditUpdate(userJson,request);
			}
		    catch(Exception e){
		    	e.printStackTrace();
			 logger.error("@@@@ Exception in ReportController at jpplCreditUpdate() :", e);
		}
		return status;
		
	}
	
	
	@RequestMapping(value = "/exportAllBusinessCsv", method = RequestMethod.GET)
public void exportAllBusinessCsv(@RequestParam("fromDate") String fromDate,@RequestParam("toDate") String toDate,
			@RequestParam("jpNumber") String jpNumber, String status,@RequestParam("validTypeRadio") String validTypeRadio,@RequestParam("reportType") String reportType,
			ModelMap model,HttpServletRequest request, HttpServletResponse response) throws IOException {
		
//		String prtNo = "1";
//		System.out.println("-----report type ------------>"+reportType);
		 String fileName = reportType+new SimpleDateFormat("ddMMyyyyHHmmss'.csv'").format(new Date());
		 String CSV_LOCATION = reportFileLocation+"/"+fileName;
		 String COMMA_DELIMITER = ",";
		 String NEW_LINE_SEPARATOR = "\n";
		 String FILE_HEADER = "JP_NUMBER,Reference Number,Affiliate order number,Partner Order Id, order Date,Merchant,partner Category,Jppl Category,Product,price,Partner Commission rate,partner commission Amount,jppl commission Amount,% jppl Commission,% of commission given to member,tax%,No. of jpmiles,"
		 		+ "expected date of credit,jpmiles credited,jpmiles declined,status,source,medium,campaign,member/guest,transaction vai";
		 
		 FileWriter fileWriter = null;
		

		 try { 
			 	
			 List<BuisnessReportsBean> reportList = reportsService.getAllExportBuisnessReportsList(fromDate, toDate,jpNumber, status, validTypeRadio);

			 for(BuisnessReportsBean buisnessReportsBeanTemp: reportList)
			 {
				 String jpNumberTemp = buisnessReportsBeanTemp.getJpNumber();
				 if(jpNumberTemp.equals("No data found")) {
					 fileWriter = new FileWriter(CSV_LOCATION);
					 fileWriter.append(FILE_HEADER.toString());
					 break;
				 }
				 else {
					 fileWriter = new FileWriter(CSV_LOCATION);
					 fileWriter.append(FILE_HEADER.toString());
					 for(BuisnessReportsBean buisnessReportsBean: reportList){
					 fileWriter.append(NEW_LINE_SEPARATOR);
					 fileWriter.append("\""+buisnessReportsBean.getJpNumber()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getTransactionId()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getPartnerItemId()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
		             fileWriter.append("\""+buisnessReportsBean.getPartnerOrderId()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getFormatedTransactionDate()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getPartnerName()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getPartnerCategoryName()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getJpplCategoryName()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getProductName()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getPrice()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getCommissionRate()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getCommAmount()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getJpplCommAmount()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getCommissionRate()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getMemberShare()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getTax()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getJpmilesPurchase().toString()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getFormatedExpectedDateOfCredit()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getJpmilesToCredit().toString()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getJpmilesCancel().toString()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getPurchase_status()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getSource()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getMedium()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getCampaign()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getMember_or_guest()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getTransactionVia()+"\"");			
					 fileWriter.append(COMMA_DELIMITER); 
					 }
					break; 
				 }
			 }
			 

		 }catch (Exception e) {
			 logger.error("@@@Exception in ReportsController All  exportBusinessCsv():", e);
		}
		 finally {
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				logger.error("@@@Exception in ReportsController  ALL exportBusinessCsv():", e);
			}
		}
		 
		 try {
			 
		   File downloadFile = new File(CSV_LOCATION);
	        FileInputStream inputStream = new FileInputStream(downloadFile);
	        
	        
	           String  mimeType = "application/octet-stream";
	        
	        response.setContentType(mimeType);
	        response.setContentLength((int) downloadFile.length());
	       
	        
	        String headerKey = "Content-Disposition";
	        String headerValue = String.format("attachment; filename=\"%s\"",downloadFile.getName());
	       
	        response.setHeader(headerKey, headerValue);
	 
	        OutputStream outStream = response.getOutputStream();
	   	 
	        byte[] buffer = new byte[BUFFER_SIZE];
	        int bytesRead = -1;
	 
	        
	        while ((bytesRead = inputStream.read(buffer)) != -1) {
	        	
	            outStream.write(buffer, 0, bytesRead);
	        }
	 
	        inputStream.close();
	        outStream.close();
	        
		 }
		 catch(Exception e) {
			 logger.error("@@@Exception in ReportsController ALLL exportBusinessCsv():", e);
//			 e.printStackTrace();			 
		 }
		
	}
	
	
	@RequestMapping(value = "/exportAllServiceCenterCsv", method = RequestMethod.GET)
	public void exportAllServiceCenterCsv(@RequestParam("fromDate") String fromDate,
			@RequestParam("toDate") String toDate,
			@RequestParam("jpNumber") String jpNumber, String status, ModelMap model,
			@RequestParam("validTypeRadio") String validTypeRadio,HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		
		String fileName = "ServiceAllCenterReport_"+new SimpleDateFormat("ddMMyyyyHHmmss'.csv'").format(new Date());
		 String CSV_LOCATION = reportFileLocation+"/"+fileName;
		 String COMMA_DELIMITER = ",";
		 String NEW_LINE_SEPARATOR = "\n";
		 String FILE_HEADER = "JP_NUMBER,Reference Number,Affiliate order number,Order Date,Merchant,Partner Category,Jppl Category,Product,Price,No. Of JpMiles,Partner Commission Rate,Partner Commission Amount,Jppl Commission Amount,"
		 		+ "expected date of credit,jpmiles credited,jpmiles declined,status,source,medium,campaign,member/guest,transaction vai";
		 
		 FileWriter fileWriter = null;

		 try { 
			 List<BuisnessReportsBean> reportList = reportsService.getAllExportServiceCenterReportsList(fromDate, toDate,jpNumber, status, validTypeRadio);				 
			 
			 for(BuisnessReportsBean buisnessReportsBeanTemp: reportList)
			 {
				 String jpNumberTemp = buisnessReportsBeanTemp.getJpNumber();
				 if(jpNumberTemp.equals("No data found")) {
					
					 fileWriter = new FileWriter(CSV_LOCATION);
					 fileWriter.append(FILE_HEADER.toString());
					 
					 break;
				 }
				 
				 else {
								
					 fileWriter = new FileWriter(CSV_LOCATION);
					

					 fileWriter.append(FILE_HEADER.toString());
					 for(BuisnessReportsBean buisnessReportsBean: reportList){
					 fileWriter.append(NEW_LINE_SEPARATOR);
					 fileWriter.append("\""+buisnessReportsBean.getJpNumber()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getTransactionId()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getPartnerItemId()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getFormatedTransactionDate()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getPartnerName()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getPartnerCategoryName()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getJpplCategoryName()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getProductName()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getPrice()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getCommissionRate()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getCommAmount()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getJpplCommAmount()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getCommissionRate()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getMemberShare()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getTax()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getJpmilesPurchase().toString()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getFormatedExpectedDateOfCredit()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getJpmilesToCredit().toString()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getJpmilesCancel().toString()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getPurchase_status()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getSource()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getMedium()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getCampaign()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getMember_or_guest()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getTransactionVia()+"\"");			
					 fileWriter.append(COMMA_DELIMITER);
				 }
			}
			 
			 }


			 	
    
		 }catch (Exception e) {
			 logger.error("@@@Exception in ReportsController ServiceCenter():", e);
		}
		 finally {
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				logger.error("@@@Exception in ReportsController ServiceCenter():", e);
			}
		}
		 File downloadFile = new File(CSV_LOCATION);
	        FileInputStream inputStream = new FileInputStream(downloadFile);
	         
	           String  mimeType = "application/octet-stream";
	 
	        response.setContentType(mimeType);
	        response.setContentLength((int) downloadFile.length());
	 
	        String headerKey = "Content-Disposition";
	        String headerValue = String.format("attachment; filename=\"%s\"",
	                downloadFile.getName());
	        response.setHeader(headerKey, headerValue);
	 
	        OutputStream outStream = response.getOutputStream();
	 
	        byte[] buffer = new byte[BUFFER_SIZE];
	        int bytesRead = -1;
	 
	        while ((bytesRead = inputStream.read(buffer)) != -1) {
	            outStream.write(buffer, 0, bytesRead);
	        }
	 
	        inputStream.close();
	        outStream.close();

		
	}

	
	
	@RequestMapping(value = "/exportAllFinanceCsv", method = RequestMethod.GET)
	public void exportAllFinanceCsv(@RequestParam("fromDate") String fromDate,
			@RequestParam("toDate") String toDate,
			@RequestParam("jpNumber") String jpNumber, String status, ModelMap model,
			@RequestParam("validTypeRadio") String validTypeRadio,HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		
		 String fileName = "FinanceAllReport_"+new SimpleDateFormat("ddMMyyyyHHmmss'.csv'").format(new Date());
		 String CSV_LOCATION = reportFileLocation+"/"+fileName;
		 String COMMA_DELIMITER = ",";
		 String NEW_LINE_SEPARATOR = "\n";
		 String FILE_HEADER = "Jp Number,Affiliate order number,% jppl Commission,% of commission given to member,Partner Commission Rate,"
		 		+ "Partner commission Amount,Jppl commission Amount,tax %,Jp Miles Credited,"
		 		+ "Status,Source,Medium,Campaign,Member/Guest,Transaction vai";
		 
		 FileWriter fileWriter = null;
		

		 try { 
			 List<BuisnessReportsBean> reportList = reportsService.getAllExportFinanceReportsList(fromDate, toDate,
						jpNumber, status, validTypeRadio);	
			 
			 
			 for(BuisnessReportsBean buisnessReportsBeanTemp: reportList)
			 {
				 String jpNumberTemp = buisnessReportsBeanTemp.getJpNumber();
				 if(jpNumberTemp.equals("No data found")) {
					
					 fileWriter = new FileWriter(CSV_LOCATION);
					 fileWriter.append(FILE_HEADER.toString());
					 
					 break;
				 }
			 
				 else {
					
					 
					 fileWriter = new FileWriter(CSV_LOCATION);
					 fileWriter.append(FILE_HEADER.toString());
					 for(BuisnessReportsBean buisnessReportsBean: reportList){
					 fileWriter.append(NEW_LINE_SEPARATOR);
					 fileWriter.append("\""+buisnessReportsBean.getJpNumber()+"\"");
					 fileWriter.append(COMMA_DELIMITER);					 
					 fileWriter.append("\""+buisnessReportsBean.getPartnerItemId()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getCommissionRate()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getMemberShare()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getCommissionRate()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getCommAmount()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getJpplCommAmount()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getTax()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getJpmilesToCredit().toString()+"\"");
					 fileWriter.append(COMMA_DELIMITER);			 
					 fileWriter.append("\""+buisnessReportsBean.getPurchase_status()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getSource()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getMedium()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getCampaign()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getMember_or_guest()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getTransactionVia()+"\"");			
					 fileWriter.append(COMMA_DELIMITER);					 
				 }			 
			 }
		}

			 	
    
		 }catch (Exception e) {
			 logger.error("@@@Exception in ReportsController exportFinanceCsv():", e);
		}
		 finally {
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				
				 logger.error("@@@Exception in ReportsController exportFinanceCsv():", e);
			}
		}
		 File downloadFile = new File(CSV_LOCATION);
	        FileInputStream inputStream = new FileInputStream(downloadFile);
	         
	        
	           String  mimeType = "application/octet-stream";
	       
	        response.setContentType(mimeType);
	        response.setContentLength((int) downloadFile.length());
	 
	       
	        String headerKey = "Content-Disposition";
	        String headerValue = String.format("attachment; filename=\"%s\"",
	                downloadFile.getName());
	        response.setHeader(headerKey, headerValue);
	 
	       
	        OutputStream outStream = response.getOutputStream();
	 
	        byte[] buffer = new byte[BUFFER_SIZE];
	        int bytesRead = -1;
	 
	       
	        while ((bytesRead = inputStream.read(buffer)) != -1) {
	            outStream.write(buffer, 0, bytesRead);
	        }
	 
	        inputStream.close();
	        outStream.close();

		
	}
	
	
	@RequestMapping(value = "/exportAllzeroJpMilesReport", method = RequestMethod.GET)
	public void exportAllZeroJpMilesReportCsv(@RequestParam("fromDate") String fromDate,
			@RequestParam("toDate") String toDate,
			@RequestParam("jpNumber") String jpNumber, String status, ModelMap model,
			@RequestParam("validTypeRadio") String validTypeRadio,HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		
		 String fileName = "ZeroJpMilesAllReport_"+new SimpleDateFormat("ddMMyyyyHHmmss'.csv'").format(new Date());
		 String CSV_LOCATION = reportFileLocation+"/"+fileName;
		 String COMMA_DELIMITER = ",";
		 String NEW_LINE_SEPARATOR = "\n";
		 String FILE_HEADER = "Jp Number,Reference Number,Affiliate Order Number,Base Jp miles,Email Status,Sms Status,Bonus Jp miles";
		 
		 FileWriter fileWriter = null;
		

		 try { 
			 List<BuisnessReportsBean> reportList = reportsService.getAllZeroJpMilesReport(fromDate, toDate,
						jpNumber, status, validTypeRadio);	
			 
			 
			 for(BuisnessReportsBean buisnessReportsBeanTemp: reportList)
			 {
				 String jpNumberTemp = buisnessReportsBeanTemp.getJpNumber();
				 if(jpNumberTemp.equals("No data found")) {
					
					 fileWriter = new FileWriter(CSV_LOCATION);
					 fileWriter.append(FILE_HEADER.toString());
					
					 break;
				 }
			 
				 else {
										 
					 fileWriter = new FileWriter(CSV_LOCATION);
					 fileWriter.append(FILE_HEADER.toString());
					 for(BuisnessReportsBean buisnessReportsBean: reportList){
					 fileWriter.append(NEW_LINE_SEPARATOR);					 				 
					 fileWriter.append("\""+buisnessReportsBean.getJpNumber()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getTransactionId()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getPartnerItemId()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getJpmilesPurchase()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getEmailStatus()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getSmsStatus()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 fileWriter.append("\""+buisnessReportsBean.getBonusJpmiles()+"\"");
					 fileWriter.append(COMMA_DELIMITER);
					 }		
				}
		}

			 	
    
		 }catch (Exception e) {
			 logger.error("@@@Exception in ReportsController exportFinanceCsv():", e);
		}
		 finally {
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				
				 logger.error("@@@Exception in ReportsController exportFinanceCsv():", e);
			}
		}
		 File downloadFile = new File(CSV_LOCATION);
	        FileInputStream inputStream = new FileInputStream(downloadFile);
	         
	        
	           String  mimeType = "application/octet-stream";
	       
	        response.setContentType(mimeType);
	        response.setContentLength((int) downloadFile.length());
	 
	       
	        String headerKey = "Content-Disposition";
	        String headerValue = String.format("attachment; filename=\"%s\"",
	                downloadFile.getName());
	        response.setHeader(headerKey, headerValue);
	 
	       
	        OutputStream outStream = response.getOutputStream();
	 
	        byte[] buffer = new byte[BUFFER_SIZE];
	        int bytesRead = -1;
	 
	       
	        while ((bytesRead = inputStream.read(buffer)) != -1) {
	            outStream.write(buffer, 0, bytesRead);
	        }
	 
	        inputStream.close();
	        outStream.close();

		
	}
	
	
	@RequestMapping(value = "/exportAllAnalyticsCsv", method = RequestMethod.GET)
	public void exportAllAnalyticsCsv(@RequestParam("fromDate") String fromDate,
			@RequestParam("toDate") String toDate,
			@RequestParam("jpNumber") String jpNumber, String status, ModelMap model,
			@RequestParam("validTypeRadio") String validTypeRadio,HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		
		String fileName = "AnalyticsAllReport_"+new SimpleDateFormat("ddMMyyyyHHmmss'.csv'").format(new Date());
		 String CSV_LOCATION = reportFileLocation+"/"+fileName;
		 String COMMA_DELIMITER = ",";
		 String NEW_LINE_SEPARATOR = "\n";
		 String FILE_HEADER = "Jp Number,Reference Number,Affiliate order number,order Date,Merchant,partner Category,Jppl Category,Product,price,No of JpMiles,Partner Commission Rate,Partner Commission Amount,Jppl Commission Amount,"+
		  "expected date of credit,jpmiles credited,jpmiles declined,status,source,medium,campaign,member/guest,transaction vai";
		 
		 FileWriter fileWriter = null;
		

		 try { 
			 List<BuisnessReportsBean> reportList = reportsService.getAllExportAnalyticsReportsList(fromDate, toDate,
						jpNumber, status, validTypeRadio);	
			 fileWriter = new FileWriter(CSV_LOCATION);
			 fileWriter.append(FILE_HEADER.toString());
			 for(BuisnessReportsBean buisnessReportsBean: reportList){
				 
				 
				 String jpNumberTemp = buisnessReportsBean.getJpNumber();
				 
			if(jpNumberTemp.equals("")) {
					
					 fileWriter = new FileWriter(CSV_LOCATION);
					 fileWriter.append(FILE_HEADER.toString());
					 
					 break;
		 		}
				
			else {				 
				
			 fileWriter.append(NEW_LINE_SEPARATOR);
			 fileWriter.append("\""+buisnessReportsBean.getJpNumber()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getTransactionId()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getPartnerItemId()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getFormatedTransactionDate()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getPartnerName()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getPartnerCategoryName()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getJpplCategoryName()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getProductName()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getPrice()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getJpmilesPurchase().toString()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getRatePerMile()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getCommAmount()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getJpplCommAmount()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getFormatedExpectedDateOfCredit()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getJpmilesToCredit().toString()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getJpmilesCancel().toString()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getPurchase_status()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getSource()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getMedium()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getCampaign()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getMember_or_guest()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+buisnessReportsBean.getTransactionVia()+"\"");			
			 fileWriter.append(COMMA_DELIMITER);
			 } 
			 }
		 }catch (Exception e) {
			 logger.error("@@@Exception in ReportsController exportAnalyticsCsv():", e);
		}
		 finally {
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				logger.error("@@@Exception in ReportsController exportAnalyticsCsv():", e);
			}
		}
		 File downloadFile = new File(CSV_LOCATION);
	        FileInputStream inputStream = new FileInputStream(downloadFile);
	         
	       
	           String  mimeType = "application/octet-stream";

	        response.setContentType(mimeType);
	        response.setContentLength((int) downloadFile.length());
	 
	        String headerKey = "Content-Disposition";
	        String headerValue = String.format("attachment; filename=\"%s\"",
	                downloadFile.getName());
	        response.setHeader(headerKey, headerValue);
	 
	        OutputStream outStream = response.getOutputStream();
	 
	        byte[] buffer = new byte[BUFFER_SIZE];
	        int bytesRead = -1;
	 
	        while ((bytesRead = inputStream.read(buffer)) != -1) {
	            outStream.write(buffer, 0, bytesRead);
	        }
	 
	        inputStream.close();
	        outStream.close();

		
	}
	
	
	
	
	
}