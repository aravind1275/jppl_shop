package com.af.admin.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.af.admin.beans.PartnerOrderStatusBean;
import com.af.admin.constants.AfConstants;
import com.af.admin.service.PartnerManagementService;
import com.af.admin.service.PartnerOrderStatusService;
import com.af.admin.utils.CommonUtil;

@Controller
public class PartnerOrderStatusController {
	
	
	@Autowired
	PartnerOrderStatusService partnerOrderStatusService;
	
	@Autowired
	PartnerManagementService partnermanagementService;
	
	@Autowired
	CommonUtil commonUtil;

	private Logger logger = LogManager.getLogger(PartnerOrderStatusController.class.getName());
	
	@RequestMapping(value="/orderStatus",method=RequestMethod.GET)
	public String partnerOrderStatus(ModelMap model,HttpServletRequest request ,PartnerOrderStatusBean partnerOrderStatusBean){
		try{
			String updateMessage=request.getParameter("updateMessage");
			model.addAttribute("partnerOrderStatusBean", new PartnerOrderStatusBean());
			model.addAttribute("partnerList", partnermanagementService.getPartners());
			model.addAttribute("partnerStatusListAll",partnerOrderStatusService.getPartnersStatus("All"));
			model.addAttribute("partnerStatusList",partnerOrderStatusService.getPartnersStatus(""));
			String jpplStatus = request.getParameter("jpplStatus");
			jpplStatus = (null != jpplStatus) ? jpplStatus.trim() : "";
			partnerOrderStatusBean.setJpplStatus(jpplStatus);
			model.addAttribute("partnerOrderStatusBean", partnerOrderStatusBean);
			
			if(updateMessage!=null)
			{
				if(updateMessage.equalsIgnoreCase(AfConstants.SUCCESS))
				model.addAttribute(AfConstants.SUCCESS_MESSAGE, "partner Order  Updated Successfully");
		
				else if(updateMessage.equalsIgnoreCase(AfConstants.FAILURE))
				model.addAttribute(AfConstants.ERROR_MESSAGE, "partner Order Could  Not Be Updated,Error Occured");
			}
		}
		catch(Exception e){
			logger.error("@@@@ Exception in PartnerOrderStatusController at partnerOrderStatus() ", e);
		}
		return "partnerOrderStatus";
		
	}
	
	
	@RequestMapping(value="/orderStatus",method=RequestMethod.POST)
	public String savePartnerOrderStatus(@ModelAttribute("partnerOrderStatusBean") PartnerOrderStatusBean partnerOrderStatusBean,ModelMap model,RedirectAttributes redirectAttrs,HttpServletRequest request){
	
		String status = null;
		status=partnerOrderStatusService.savePartnerOrderStatus(partnerOrderStatusBean, request);
		/*try {
			commonUtil.akamaiPurge();
		} catch (IOException e) {
			logger.error("Akamai Exception ",e);
		}*/	
		if(status.equalsIgnoreCase("success"))
			redirectAttrs.addFlashAttribute("message", "partnerOrderStatus Save Successfully");
		else
			redirectAttrs.addFlashAttribute("errormessage", "partnerOrderStatus Could not be Save ");

		return "redirect:/admin/orderStatus";
	}
	
	@RequestMapping(value="/addPartnerOrderStatusCancel",method=RequestMethod.GET)
	public @ResponseBody String addPartnerOrderStatusCancel(ModelMap model){
		return "admin/orderStatus";
	}
	
	
	@RequestMapping("/checkPartnerOrderStatusExist")
	@ResponseBody
	public String checkPartnerOrderStatusExist(@RequestParam("prtNo") String prtno,@RequestParam("partnerStatus") String partnerstatus,@RequestParam("jpplStatus") String jpplstatus) {
		Boolean partnerOrderStatusExist = false;
		String prtNo = prtno;
		String partnerStatus=partnerstatus;
		String jpplStatus=jpplstatus;
		
		prtNo = (null != prtNo) ? prtNo.trim() : "";
		partnerStatus = (null != partnerStatus) ? partnerStatus.trim() : "";
		jpplStatus = (null != jpplStatus) ? jpplStatus.trim() : "";
		if (prtNo.length() > 0 && partnerStatus.length() > 0 && jpplStatus.length() > 0 ) {
			partnerOrderStatusExist = partnerOrderStatusService.checkPartnerOrderStatusExist(prtNo,partnerStatus,jpplStatus);
		}
		if (partnerOrderStatusExist)
			return "exist";
		else
			return "";
	}
	
	@RequestMapping(value="/updatePartnerOrderStatus", method=RequestMethod.POST)
	public @ResponseBody String updatePartnerOrderStatus(@RequestParam("json") String json,HttpServletRequest request){
		String status=AfConstants.FAILURE;
		try {
			JSONObject userJson=new JSONObject(json);			
			status=partnerOrderStatusService.updatePartnerOrderStatus(userJson,request);
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}
		} catch (Exception e) {
			logger.error("@@@@ Exception in PartnerOrderStatusController at updatePartnerOrderStatus() :", e);
		}
		return status;
	}
	
	}


