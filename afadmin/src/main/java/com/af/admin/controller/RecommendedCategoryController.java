package com.af.admin.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.af.admin.beans.RecommendedCategoryBean;
import com.af.admin.service.CommonService;
import com.af.admin.service.PartnerManagementService;
import com.af.admin.service.RecommendedCategoryService;
import com.af.admin.utils.CommonUtil;

@Controller
public class RecommendedCategoryController {
	private Logger logger = LogManager.getLogger(RecommendedCategoryController.class.getName());

	@Autowired
	RecommendedCategoryService rcService;

	@Autowired
	PartnerManagementService partnermanagementService;

	@Autowired
	CommonService commonService;
	
	@Autowired
	CommonUtil commonUtil;

	@RequestMapping(value="/recommendedCategories",method=RequestMethod.GET)
	public String recommendedCategories(ModelMap model,HttpServletRequest request){
		String deleteMsg=request.getParameter("deleteMsg");
		model.addAttribute("recommendedCategoryBean_",new RecommendedCategoryBean());
		model.addAttribute("recommendedCategoryBean",new RecommendedCategoryBean());			
		model.addAttribute("rcStatus", commonService.getEnumValues("STATUS"));
		model.addAttribute("rcList", rcService.getRecommendedCategories());
		model.addAttribute("partnerList", partnermanagementService.getPartners());
		if(deleteMsg!=null)
		{
			if(deleteMsg.equalsIgnoreCase("success"))
				model.addAttribute("message", "Recommended Category Deleted Successfully");

			else if(deleteMsg.equalsIgnoreCase("failure"))
				model.addAttribute("errormessage", "Recommended Category Could not be Deleted! Error Occurred");
		}
		commonUtil.getDefaultCategoryTree(request, "", "");

		return "recommendedCategories";
	}

	@RequestMapping(value="/getPartnerCategory",method=RequestMethod.GET)
	public @ResponseBody Map<String, String> getPartnerCategoryList(HttpServletRequest request){
		Map<String, String> pcList=null;		
		String prtNo=request.getParameter("prtNo");
		logger.debug("/getPartnerCategory GET prtNo:"+prtNo);
		pcList=commonService.getPartnerCategoryListMap(prtNo);		
		return pcList;
	}

	@RequestMapping(value="/recommendedCategories",method=RequestMethod.POST)
	public String createRecommendedCategories(@ModelAttribute("recommendedCategoryBean") RecommendedCategoryBean recommendedCategoryBean,ModelMap model,RedirectAttributes redirectAttrs){
		String status;
		try{
			status=rcService.createRecommendedCategory(recommendedCategoryBean);
		/*	try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}*/
			if(status.equalsIgnoreCase("success"))
				redirectAttrs.addFlashAttribute("message", "Recommended Category Created Successfully");
			else
				redirectAttrs.addFlashAttribute("errormessage", "Recommended Category Could not be Created ");

		}catch(Exception e)
		{
			logger.debug("@@@@ Exception in RecommendedCategoryController at createRecommendedCategories() :", e);
		}

		return "redirect:/admin/recommendedCategories";
	}

	@RequestMapping(value="/recommendedCategoriesFilter",method=RequestMethod.POST)
	public String recommendedCategories(@ModelAttribute("recommendedCategoryBean_") RecommendedCategoryBean recommendedCategoryBean,ModelMap model){
		String prtNo = recommendedCategoryBean.getPrtNo().toString();
		model.addAttribute("recommendedCategoryBean_",new RecommendedCategoryBean());
		model.addAttribute("recommendedCategoryBean",new RecommendedCategoryBean());
		model.addAttribute("rcStatus", commonService.getEnumValues("STATUS"));
		model.addAttribute("rcList", rcService.getRecommendedCategories(prtNo));		
		model.addAttribute("partnerList", partnermanagementService.getPartners());
		recommendedCategoryBean.setPrtNo(Integer.parseInt(prtNo));
		model.addAttribute("recommendedCategoryBean_",recommendedCategoryBean);
		return "recommendedCategories";
	}

	@RequestMapping("/checkRecommendedCategory")
	public @ResponseBody String checkRecommendedCategory(HttpServletRequest request){
		Map<String, String> rcmap = new HashMap<String, String>();
		boolean checkFlag=false;
		String prtNo=request.getParameter("prtNo");
		String prtCatNo=request.getParameter("prtCatNo");		
		logger.debug("/checkRecommendedCategory request-val:"+prtNo+"#"+prtCatNo);
		if(prtNo.length()>0 && prtCatNo.length()>0){
			rcmap.put("prtNo", prtNo);
			rcmap.put("prtCatNo", prtCatNo);
			checkFlag=rcService.checkRecommendedCategory(rcmap);
			if(checkFlag) return "exist"; else return "";
		}else
			return "";		
	}

	@RequestMapping(value="/updateRecommendedCategory")
	public String updateRecommendedCategoryGet( ModelMap model, HttpServletRequest request){
		RecommendedCategoryBean recommendedCategoryBean=null;
		try{
			String rpNo=request.getParameter("rcNo");			
			model.addAttribute("rcStatus", commonService.getEnumValues("STATUS"));				
			model.addAttribute("partnerList", partnermanagementService.getPartners());

			recommendedCategoryBean=rcService.getRecommendedCategoryByNo(rpNo);
			model.addAttribute("recommendedCategoryBean", recommendedCategoryBean);

			commonUtil.getDefaultCategoryTree(request, "", "");
		}
		catch(Exception e){
			logger.debug("@@@@ Exception in RecommendedCategoryController at updateRecommendedCategoryGet() :", e);
		}
		return "updateRecommendedCategory";
	}


	@RequestMapping(value="/updateRecommendedCategory", method=RequestMethod.POST)
	public String updateRecommendedCategoryPost(@ModelAttribute RecommendedCategoryBean recommendedCategoryBean,RedirectAttributes redirectAttrs,HttpServletRequest request){

		logger.debug("/updateRecommendedCategory POST starts...");
		String status;
		try {

			status=rcService.updateRecommendedCategory(recommendedCategoryBean);
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}
			if(status.equalsIgnoreCase("success"))
				redirectAttrs.addFlashAttribute("message", "Recommended Category Updated Successfully");
			else
				redirectAttrs.addFlashAttribute("errormessage", "Recommended Category Could not be Updated ");

		} catch (Exception e) {
			logger.debug("@@@@ Exception in RecommendedCategoryController at updateRecommendedCategoryPost() :", e);
		}
		commonUtil.getDefaultCategoryTree(request, "", "");

		return "redirect:/admin/recommendedCategories";
	}

	@RequestMapping(value="/deleteRecommendedCategory")
	public @ResponseBody String deleteRecommendedCategory(@RequestParam("rcNo") String rcNo){
		String deleteStatus="";
		try{			
			deleteStatus = rcService.deleteRecommendedCategory(new Integer(rcNo));
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}
			logger.debug(deleteStatus);
		}catch(Exception e){
			logger.error("@@@@ Exception in RecommendedCategoryController at deleteRecommendedCategory() :", e);
		}
		return deleteStatus;

	}

	@RequestMapping("/checkRecommendedCategoryPosition")
	public @ResponseBody String checkRecommendedCategoryPosition(HttpServletRequest request){
		boolean checkFlag=false;
		String recommendedCategoryPosition=request.getParameter("recommendedCategoryPosition");
		logger.debug("/checkRecommendedProductPosition request-val:" + recommendedCategoryPosition);
		if(recommendedCategoryPosition.length()>0){
			checkFlag=rcService.checkRecommendedCategoryPosition(recommendedCategoryPosition);
			if(checkFlag) return "exist"; else return "";
		}else
			return "";	
	}

	@RequestMapping("/checkRcCatNo")
	public @ResponseBody String checkRcCatNo(HttpServletRequest request){
		boolean checkFlag=false;
		String rcCatNo=request.getParameter("rcCatNo");
		logger.debug("/checkRecommendedProductPosition request-val:" + rcCatNo);
		if(rcCatNo.length()>0){
			checkFlag=rcService.checkRcCatNo(rcCatNo);
			if(checkFlag) return "exist"; else return "";
		}else
			return "";	
	}


}
