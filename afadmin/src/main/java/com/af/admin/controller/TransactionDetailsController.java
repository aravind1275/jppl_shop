package com.af.admin.controller;

import com.af.admin.beans.BuisnessReportsBean;
import com.af.admin.beans.ExportBean;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.af.admin.beans.TransactionDetailsBean;
import com.af.admin.service.TransactionDetailsService;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Value;






@Controller
public class TransactionDetailsController {
	private Logger logger = LogManager.getLogger(TransactionDetailsController.class.getName());
	
	@Autowired
	TransactionDetailsService transactionDetailsService; 
	
        @Value("${application.af.report_download}")
	private String reportFileLocation;
        
        private static final int BUFFER_SIZE = 4096;
	
	@RequestMapping(value="/transactionDetails", method=RequestMethod.GET)
	public String getTransactionDetailsPage(ModelMap model) {
		model.addAttribute("transactionDetailsBean",new TransactionDetailsBean());
		return "transactionDetails";
	}
        
        
        
        
        @RequestMapping(value = "/exportTransactionCsv", method = RequestMethod.GET)
	public void exportTransactionCsv(@RequestParam("fromDate") String fromDate,
			@RequestParam("toDate") String toDate,
			@RequestParam("inputMemberId") String inputMemberId, ModelMap model,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		 String fileName = "TransactionReport_"+new SimpleDateFormat("ddMMyyyyHHmmss'.csv'").format(new Date());
		 String CSV_LOCATION = reportFileLocation+"/"+fileName;
		 String COMMA_DELIMITER = ",";
		 String NEW_LINE_SEPARATOR = "\n";
		 String FILE_HEADER = "Transaction Date,Partner,Category,Product,Transaction Amount,No.of JP Miles,Expected Date of Credit,Date Credited,Status";
		 		
		 
		 FileWriter fileWriter = null;
		 List<TransactionDetailsBean> transactionListList = null;
		

		 try { 
			 if(fromDate.length() == 0)
				{
					System.out.println("inside if");
					transactionListList = transactionDetailsService.getTransactionDetails(inputMemberId);
				}
				else
				{
					System.out.println("inside else");
					transactionListList = transactionDetailsService.getTransactionDateDetails(inputMemberId, fromDate, toDate);
				}
				                 System.out.println("records in controller.........."+transactionListList.size());			
			 fileWriter = new FileWriter(CSV_LOCATION);
			 fileWriter.append(FILE_HEADER.toString());
			 for(TransactionDetailsBean exportBean: transactionListList){
			 fileWriter.append(NEW_LINE_SEPARATOR);
			 //fileWriter.append("\""+transactionDetailsBean.get+"\"");
                            
//			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+exportBean.getTransactionDate()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+exportBean.getPartnerName()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+exportBean.getPartnerCategoryName()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+exportBean.getPartnerProductName()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+exportBean.getAmount()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+exportBean.getMiles()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
                       fileWriter.append("\""+exportBean.getExpectedCreditDate()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+exportBean.getMilesPostingDate()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 fileWriter.append("\""+exportBean.getProductStatus()+"\"");
			 fileWriter.append(COMMA_DELIMITER);
			 
		}
	}	
    
		 catch (Exception e) {
			 logger.error("@@@Exception in ReportsController exportBusinessCsv():", e);
			 e.printStackTrace();
		}
		 finally {
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				logger.error("@@@Exception in ReportsController exportBusinessCsv():", e);
				e.printStackTrace();
			}
		}
		   File downloadFile = new File(CSV_LOCATION);
	        FileInputStream inputStream = new FileInputStream(downloadFile);
	         
	        
	           String  mimeType = "application/octet-stream";
	        
	        response.setContentType(mimeType);
	        response.setContentLength((int) downloadFile.length());
	 
	        
	        String headerKey = "Content-Disposition";
	        String headerValue = String.format("attachment; filename=\"%s\"",
	                downloadFile.getName());
	        response.setHeader(headerKey, headerValue);
	 
	       
	        OutputStream outStream = response.getOutputStream();
	 
	        byte[] buffer = new byte[BUFFER_SIZE];
	        int bytesRead = -1;
	 
	        
	        while ((bytesRead = inputStream.read(buffer)) != -1) {
	            outStream.write(buffer, 0, bytesRead);
	        }
	 
	        inputStream.close();
	        outStream.close();
 }
		
	
        
        
        
      
        
	@RequestMapping(value="/transactionDetailsList", method=RequestMethod.POST)
	public String getTransactionDetailsList(@RequestParam("inputMemberId") String inputMemberId, ModelMap model,@RequestParam("fromDate") String fromDate,@RequestParam("toDate") String toDate) {
		inputMemberId = (null!=inputMemberId && inputMemberId.length() > 0)?inputMemberId.trim():"";
		logger.info("/transactionDetailsList "+inputMemberId);
		model.addAttribute("transactionDetailsBean",new TransactionDetailsBean());
		
		System.out.println("From date----->"+fromDate.length());
		System.out.println("to date-------->"+toDate.length());
		
		List<TransactionDetailsBean> transactionListList = null;
		
		if(fromDate.length() == 0)
		{
			System.out.println("inside if");
			transactionListList = transactionDetailsService.getTransactionDetails(inputMemberId);
		}
		else
		{
			System.out.println("inside else");
			transactionListList = transactionDetailsService.getTransactionDateDetails(inputMemberId, fromDate, toDate);
		}
		
		
		if(transactionListList.isEmpty()){
			model.addAttribute("message", "Data Not Available");
		}
		
		model.addAttribute("inputMemberId", inputMemberId);
                model.addAttribute("fromDate", fromDate);
               model.addAttribute("toDate", toDate);
		model.addAttribute("transactionDetailsList", transactionListList);
		return "transactionDetails";
	}   
		
}  
