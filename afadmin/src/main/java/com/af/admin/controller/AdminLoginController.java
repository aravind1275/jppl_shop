package com.af.admin.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.af.admin.beans.AdminUserBean;
import com.af.admin.service.UserManagementService;

@Controller

@RequestMapping("/login")
public class AdminLoginController {
	
	private Logger logger = LogManager.getLogger(AdminLoginController.class.getName());
	
	@Autowired
	UserManagementService userManagementService;
	@RequestMapping(method=RequestMethod.GET)
	public String adminLoginpage(ModelMap model,HttpServletRequest request){
		model.addAttribute("adminUserBean", new AdminUserBean());
		if(request.getSession()!=null){
		request.getSession().invalidate();
		}
		return "admin/login/adminLogin";
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public String adminLogindetailsCheck(@ModelAttribute("adminUserBean") AdminUserBean adminUserBean,HttpServletRequest request,ModelMap model){
		AdminUserBean userBean=null;
		String errorMsg="";
		try{
			
			request.getSession().setAttribute("userIdSession", adminUserBean.getUserName());
			userBean=userManagementService.authenticateUser(adminUserBean);
		}
		catch(Exception e)
		{
			logger.error("@@@@ Exception in AdminLoginController at adminLogindetailsCheck() :", e);
		}
		if(null!=userBean){
			errorMsg=userBean.getErrorMsg();
			errorMsg=(null!=errorMsg)?errorMsg.trim():"";
			if(errorMsg.length()>0){
				model.addAttribute("errorMsg", errorMsg);
				return "admin/login/adminLogin";
			}
			else
			{
			request.getSession().setAttribute("activeUser", userBean);
			}
		}
		else
		{		
			model.addAttribute("errorMsg", "User is not Exist");
			return "admin/login/adminLogin";
		}		

		return "redirect:/admin/dashboard";

	}

}
