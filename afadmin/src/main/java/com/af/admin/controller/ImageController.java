package com.af.admin.controller;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ImageController {
	private Logger logger = LogManager.getLogger(ImageController.class.getName());
	
    @Value("${application.afadmin_serverUrl}")
    private String afAdminUrl;

    @RequestMapping(value = "/image/{folder}/{imageParam:.+}", method = {RequestMethod.GET})
    private String getImageUrl(@PathVariable String folder, @PathVariable String imageParam, ModelMap model, HttpServletRequest request, HttpServletResponse response) {
        HttpURLConnection conn = null;
        try {
        	logger.info("image path in portal: /image/"+folder+"/"+imageParam);
            URL url = new URL(afAdminUrl + "image/" + folder + "/" + imageParam);
//            URL url = new URL("https://s3.amazonaws.com/app-res-uploads-uat/extension/images/icon/rentcar1530105578120.png");
            logger.info("new url: " + url);
            
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "image/jpeg");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            InputStream in = conn.getInputStream();
            OutputStream out = response.getOutputStream();
            copy(in, out);
            out.flush();
            out.close();

        } catch (Exception e) {
        	logger.info("@@@Exception in ImageController at getImageUrl(): ", e);
        }
        return null;
    }

    protected static long copy(InputStream input, OutputStream output)
            throws IOException {
        byte[] buffer = new byte[12288]; // 12K
        long count = 0L;
        int n = 0;
        while (-1 != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
            count += n;
        }
        return count;
    }

}