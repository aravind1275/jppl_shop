package com.af.admin.controller;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.af.admin.constants.AfConstants;
import com.af.admin.entity.AfEnumValues;
import com.af.admin.service.CommonService;
import com.af.admin.utils.CommonUtil;




@Controller	
public class SellerTextController {
	private Logger logger = LogManager.getLogger(SellerTextController.class.getName());
	
	@Autowired
	CommonService commonService;
	
	@Autowired
	CommonUtil commonUtil;
	
	@RequestMapping(value="/sellerText",method=RequestMethod.GET)
	public String getSellerText(ModelMap model){
		AfEnumValues enamVal=null;
		try{
		enamVal = commonService.getEnumValuebyType("SELLER_TEXT");
		model.addAttribute("sellerText",enamVal.getValue());
		}catch (Exception e) {
			logger.error("@@@@ Exception in SellerTextController at getSellerText() :", e);
		}
		return "sellerText";
		
	}
	
	@RequestMapping(value = "/sellerText", method = RequestMethod.POST)
	public String updateSellerText(@RequestParam("sellerText") String sellerText, HttpServletRequest request, ModelMap model) {
		String status = "failure";

		try {
			status = commonService.saveSellerText(sellerText,request);
			/*try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}*/
		} catch (Exception e) {
			logger.error("@@@@ Exception in SellerTextController at updateSellerText() :", e);
		}

		if (status.equalsIgnoreCase(AfConstants.SUCCESS)) {
			model.addAttribute("message", "Seller Text updated Successfully");
		} else if (status.equalsIgnoreCase(AfConstants.FAILURE)) {
			model.addAttribute("errormessage", "Error in updating eller Text");
		} 
		AfEnumValues enamVal=null;
		try {
			enamVal = commonService.getEnumValuebyType("SELLER_TEXT");
			model.addAttribute("sellerText",enamVal.getValue());
		} catch (Exception e) {
			logger.error("@@@@ Exception in SellerTextController at updateSellerText() retrive :", e);
		}
		return "sellerText";
	}
}
