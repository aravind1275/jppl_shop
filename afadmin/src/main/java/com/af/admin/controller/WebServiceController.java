package com.af.admin.controller;

import org.springframework.stereotype.Controller;
//import com.af.admin.pojo.autosugg.Response;
//import com.google.api.client.http.HttpResponse;
//import static com.sun.xml.internal.ws.api.message.Packet.Status.Request;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;
//import javax.net.ssl.HttpsURLConnection;
//import javax.net.ssl.SSLSocketFactory;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.net.URL;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyFactory;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertPath;
import java.security.cert.CertPathValidator;
import java.security.cert.CertPathValidatorException;
import java.security.cert.CertPathValidatorResult;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.PKIXCertPathValidatorResult;
import java.security.cert.PKIXParameters;
import java.security.cert.TrustAnchor;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.X509EncodedKeySpec;


@Controller
public class WebServiceController {

	private static Logger logger = LogManager.getLogger(WebServiceController.class.getName());
	
	@RequestMapping(value= {"/getJPNumber"},method=RequestMethod.POST, produces={"application/json"}) 
    public @ResponseBody String getJpnumber(@RequestParam("mobileNumber") String mobileNumber) {
 	   System.out.println("-------------mobilenumber----------->"+mobileNumber);
//         String mobileNum=mobileNumber;
         String  Json="";
         String apiurl="https://api.jetprivilege.com/api/experience/member-profile/v2/fetch-profile?MobileNumber="+mobileNumber;
             try {
            	 sslCertificate();
                 URL url = new URL (apiurl);
                String encoding = Base64.getEncoder().encodeToString(("WW4PMR1YJoIurRCVeje2yDXrQtRhziVb:HkquOnbNU9Qe9J4I5aATW4mtygoJOKD3").getBytes());
//                 sslManager();
                 HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                 connection.setRequestMethod("GET");
                 connection.setDoOutput(true);
                 
                 connection.setRequestProperty  ("Authorization", "Basic " + encoding);
                 connection.setRequestProperty  ("partnerID", "JP Hotel platform");
                 
                 InputStream content = (InputStream)connection.getInputStream();
                 BufferedReader in   = 
                     new BufferedReader (new InputStreamReader(content));
                 String line;
                 String resposne="";
                 while ((line = in.readLine()) != null) {
//                     System.out.println("response---->"+line);
                   
                     resposne = resposne + line;
                     
                 }
                 logger.info("mobile Number Json  : "+resposne);
                 System.out.println("------------>"+resposne);
                 
                 JSONObject object=new JSONObject(resposne);
                 String res=""+object.get("ResponseStatus");
                 JSONObject object1=new JSONObject(res);
               
                  String resststus=""+object1.get("ResponseFlag");
                  String errormessage="";
           
                 
                 if("300043".equalsIgnoreCase(""+object1.get("ResponseCode"))) {
                 	errormessage="Multiple member accounts are linked to this mobile number, Request you to log in using your JetPrivilege membership number";
                 }else if("300044".equalsIgnoreCase(""+object1.get("ResponseCode"))) {
                 	
                 	errormessage="Sorry, we couldn\'t find a JetPrivilege account linked to the entered mobile number. You can also log in using your membership account number.";
                 }else {
                 if(resststus.equalsIgnoreCase("false")) {
              		errormessage=""+object1.getString("ResponseMessage");   
                 }
                 
                 	
                 }
                 if(resststus.equalsIgnoreCase("true")) {
              	  
              	  Json="{\"JPNumber\":\""+object.getString("JPNumber")+"\",\"ResponseFlag\":"+object1.get("ResponseFlag")+",\"ResponseMessage\":\""+errormessage+"\"}";
        
                }else {
             
              	  Json="{\"JPNumber\":"+"null"+",\"ResponseFlag\":"+object1.get("ResponseFlag")+",\"ResponseMessage\":\""+errormessage+"\"}";
                  }
                 
                   logger.info("mobile Number Json  : "+Json);
         
         }catch(Exception ex){
         	
         ex.printStackTrace();
         logger.error("mobile Number Api Response Error  : "+ex);
         }
         
      return Json;
 }
	
	public void sslCertificate() {
		
		
		TrustManager[] trustAllCerts = new TrustManager[]{
				new X509TrustManager() {
				    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				        return null;
				    }
				    public void checkClientTrusted(
				        java.security.cert.X509Certificate[] certs, String authType) {
				    }
				    public void checkServerTrusted(
				        java.security.cert.X509Certificate[] certs, String authType) {
				    }
				}};

				   try {
				    SSLContext sc = SSLContext.getInstance("SSL");
				    sc.init(null, trustAllCerts, new java.security.SecureRandom());
				    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
				    } catch (Exception e) {
				    	
				    	logger.error("Error in Certification Code "+e);
				    }
		
	}
	

	
}
