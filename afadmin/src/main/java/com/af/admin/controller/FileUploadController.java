package com.af.admin.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.af.admin.DAO.PartnerManagementDAO;
import com.af.admin.beans.OffersDealsBean;
import com.af.admin.beans.PartnerBean;
import com.af.admin.constants.AfConstants;
import com.af.admin.service.CommonService;
import com.af.admin.service.PartnerManagementService;
import com.opencsv.CSVReader;

/**
 * Handles requests for the application file upload requests
 */
@Controller
public class FileUploadController {
	@Autowired
	CommonService commonService;
	
	@Autowired
	PartnerManagementDAO partnerManagementDAO;
	
	@Autowired
	PartnerManagementService partnerManagementService;
	
	private static final Logger logger = LoggerFactory.getLogger(FileUploadController.class);
	
	@Value("${application.af.upload.files.folder}")
	private String fileFolderLocation;
	
	@Value("${application.af.upload.images.folder}")
	private String imagesFolderLocation;

	
	/**
	 * Upload single file using Spring Controller
	 */
	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
	public String  uploadFileHandler(@ModelAttribute("partnerBean")PartnerBean partnerBean,@RequestParam("filename") String name,
			@RequestParam("uploadCSVFile") MultipartFile file,HttpServletRequest request,@RequestParam("logoPrtNo") String prtNo,@RequestParam("prtType") String prtType,ModelMap model) {
		String[] nextLine;
		String status="";
		String partnerName=prtNo;
		String filePath="";
		List<String> keywords=new ArrayList<>();
		Set<String> keywordSet=new HashSet<>();
		name = name+"_"+new Timestamp(System.currentTimeMillis()).getTime()+".csv";
			if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();
				
				// Creating the directory to store file
				String rootPath = fileFolderLocation;
				logger.info("root Path"+rootPath);
				logger.info("name "+name);
				logger.info("partnername "+partnerName);
				
				logger.info("fileFolderLocation "+fileFolderLocation);
				File dir = new File(rootPath+"keywords"+ File.separator +partnerName);
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(dir+ File.separator + name);
				filePath=dir+ File.separator + name;
				logger.info("File Path "+filePath);
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));    
				stream.write(bytes);
				stream.close();

				logger.info("Server File Location=" + serverFile.getAbsolutePath());
					//read file
			        //CSVReader(fileReader, ';', '\'', 1) means
			        //using separator ; and using single quote ' . Skip first line when read
			 
			        FileReader fileReader = new FileReader(serverFile);
			        CSVReader reader = new CSVReader(fileReader);//, ';', '\'', 1);
			        
			            while ((nextLine = reader.readNext()) != null) {
			            	logger.info("content : ");
			                for(int i=0;i<nextLine.length;i++){
			                	logger.info("content : "+nextLine[i]);
			                	nextLine[i]=nextLine[i].replaceFirst("\\s*","");
			                	logger.info("content : "+nextLine[i]);
			                	if(	nextLine[i].isEmpty()|| nextLine[i]==null)
			                		continue;
			                   keywordSet.add(nextLine[i]);
			                    
			                }
			            }
			            keywords.addAll(keywordSet);
			            status=commonService.cerateKeyWord(keywords, filePath, prtNo, request, prtType);
			            partnerBean.setPrtNo(Integer.parseInt(prtNo));
						model.addAttribute("keywordsFile",partnerManagementService.getKeywordFileName(prtNo));
						model.addAttribute("logoPartnerList",partnerManagementService.getLogoPartners());
						model.addAttribute("partnerBean", partnerBean);
						model.addAttribute("logoprtNo", prtNo);
						model.addAttribute("prttype", prtType);
						logger.info("lOGO Partner No "+prtNo+">>prtType>>"+prtType);
						model.addAttribute("mappingKeyWordTab",true);
						if(status.equalsIgnoreCase("success"))
							model.addAttribute(AfConstants.SUCCESS_MESSAGE, "File Uploaded Successfully");
						else if(status.equalsIgnoreCase("failure"))
								model.addAttribute(AfConstants.ERROR_MESSAGE, "File Upload Failed");
						request.setAttribute("configTileNo",partnerManagementService.getLgoPrtTileSearchresult());
			            return "partnerManagement";  
			} catch (Exception e) {
				logger.error("@@@@ Exception in FileUploadController at uploadFileHandler() :", e);
				return "You failed to upload " + name + " => " + e.getMessage();
				
			}  
		} else {
			return "You failed to upload " + name
					+ " because the file was empty.";
		}
	}
	
	@RequestMapping(value="/deleteFile")
	@ResponseBody
	public String deleteFile(@RequestParam("prtNo") String prtNo, HttpServletRequest request){
		
		String deleteStatus="";
		try{
			logger.debug("deletePartner partner No"+prtNo);
			deleteStatus=commonService.deleteFile(new Integer(prtNo), request);
					logger.debug(deleteStatus);
		}catch(Exception e){
				logger.error("@@@@ Exception in FileUploadController at deleteFile() :", e);
		}
		return deleteStatus;
		
	}

	/**
	 * Upload multiple file using Spring Controller
	 */
	@RequestMapping(value = "/uploadMultipleFile", method = RequestMethod.POST)
	public @ResponseBody
	String uploadMultipleFileHandler(@RequestParam("name") String[] names,
			@RequestParam("file") MultipartFile[] files) {

		if (files.length != names.length)
			return "Mandatory information missing";

		String message = "";
		for (int i = 0; i < files.length; i++) {
			MultipartFile file = files[i];
			String name = names[i];
			try {
				byte[] bytes = file.getBytes();

				// Creating the directory to store file
				String rootPath = System.getProperty("catalina.home");
				File dir = new File(rootPath + File.separator + "tmpFiles");
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(dir.getAbsolutePath()
						+ File.separator + name);
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				logger.info("Server File Location="
						+ serverFile.getAbsolutePath());

				message = message + "You successfully uploaded file=" + name
						+ "<br />";
			} catch (Exception e) {
				logger.error("@@@@ Exception in FileUploadController at uploadMultipleFileHandler() :", e);
				return "You failed to upload " + name + " => " + e.getMessage();
			}
		}
		return message;
	}
	
	/**
	 * Upload single file using Spring Controller
	 */
	@SuppressWarnings("resource")
	@RequestMapping(value = "/offersAndDealsFileUpload", method = RequestMethod.POST)
	public String  offersAndDealsFileUpload(@ModelAttribute("offersDealsBean")OffersDealsBean offersDealsBean,
			@RequestParam("filename") String name, @RequestParam("uploadCSVFile") MultipartFile file,
			HttpServletRequest request, ModelMap model) {

		String[] nextLine;
		String filePath = "";
		List<String> offersAndDealsList = new ArrayList<String>();
		name += "_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + ".csv";
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();

				logger.info("offersAndDeals file name: " + name);
				logger.info("offersAndDeals fileFolderLocation: " + fileFolderLocation);

				// Creating the directory to store file
				File dir = new File(fileFolderLocation + "offersAndDeals" + File.separator);
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(dir + File.separator + name);
				filePath = dir + File.separator + name;
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));    
				stream.write(bytes);
				stream.close();

				logger.info("Server File Location=" + serverFile.getAbsolutePath());

				//start reading file
				FileReader fileReader = new FileReader(serverFile);
				CSVReader reader = new CSVReader(fileReader);

				while ((nextLine = reader.readNext()) != null) {
					for(int i = 0; i < nextLine.length; i++) {
						nextLine[i] = nextLine[i].replaceFirst("\\s*", ""); //removing whitespace from start
						if(	(nextLine[i] != null) || !(nextLine[i].trim().isEmpty()) ) {
							offersAndDealsList.add(nextLine[i]);
						}
					}
				}
				if(offersAndDealsList.size() > 1)
					commonService.cerateOffersAndDeals(offersAndDealsList, filePath, 12, model);
				else
					model.put("status", "emptyFile" + file.getOriginalFilename());

				return "redirect:/admin/offersDeals?" + URLEncoder.encode(String.valueOf(model.get("status")), "UTF-8");

			} catch (Exception e) {
				logger.error("@@@@ Exception in FileUploadController at uploadFileHandler() :", e);
				e.printStackTrace();
				return "redirect:/admin/offersDeals?status=failure";
			}
		} else {
			return "redirect:/admin/offersDeals?status=Failed to upload, File is empty";
		}
	}
}
