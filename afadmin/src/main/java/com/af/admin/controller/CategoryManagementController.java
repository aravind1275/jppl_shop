package com.af.admin.controller;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.exception.ConstraintViolationException;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.af.admin.beans.AfAmzonoPartnerCategoryBean;
import com.af.admin.beans.CategoryBean;
import com.af.admin.beans.CommissionBean;
import com.af.admin.beans.PartnerCategoryBean;
import com.af.admin.constants.AfConstants;
import com.af.admin.service.CategoryManagementService;
import com.af.admin.service.CommonService;
import com.af.admin.service.PartnerManagementService;
import com.af.admin.utils.CommonUtil;
import com.af.admin.utils.GsonUtil;

@Controller
public class CategoryManagementController {
	private Logger logger = LogManager.getLogger(CategoryManagementController.class.getName());

	@Autowired
	CategoryManagementService categoryManagementService;

	@Autowired
	PartnerManagementService partnermanagementService;

	@Autowired
	CommonUtil commonUtil;
	
	@Autowired
	CommonService commonService;
	

	private String catNoStr = "catNo";
	private String existStr = "exist";
	private String statusStr = "STATUS";

	@RequestMapping(value="/category",method=RequestMethod.GET)
	public String categoryManagement(ModelMap model,HttpServletRequest request){
		String catNo  = (String) model.get(catNoStr);

        String statuscatMapSearch = "";
        String statuscatMapOther = "";
        String daletecatMapOther = "";
		if( (catNo != null) && !(catNo.equalsIgnoreCase("")) ) {
			commonUtil.getCategoryDetails(model, request, catNo, "createOrUpdate");
		} else {
			commonUtil.getCategoryDetails(model, request, "", "");
		}
		String status ="";
		model.addAttribute("amazonMappingDetailsList", categoryManagementService.getAmazonMappingDetails());
		model.addAttribute("createAmzonPartnerCatForm",new AfAmzonoPartnerCategoryBean());
		daletecatMapOther = request.getParameter("deletePrtCatMsgOther");
		
		if(daletecatMapOther!=null) {
			model.addAttribute("mapCategoryUpdateTab", true);
			if(daletecatMapOther.equalsIgnoreCase(AfConstants.SUCCESS))
				model.addAttribute(AfConstants.SUCCESS_MESSAGE, "Partner Category Deactivated Successfully");

			else if(daletecatMapOther.equalsIgnoreCase(AfConstants.FAILURE))
				model.addAttribute(AfConstants.ERROR_MESSAGE, "Partner Category Cannot be Deactivated! Error Occured");
		}
		
		String status1 = request.getParameter("updateAmazonMessage")==null?"":request.getParameter("updateAmazonMessage");
		if(status1 != null && !status1.equalsIgnoreCase("")){
		if(status1.equalsIgnoreCase(AfConstants.SUCCESS)){
			model.addAttribute(AfConstants.SUCCESS_MESSAGE, " Added Successfully updated");

			}else if(status1.equalsIgnoreCase(AfConstants.FAILURE)){
				model.addAttribute(AfConstants.ERROR_MESSAGE, "Error: Could not be updated ");

			}
		model.addAttribute("updateamazonCategory", true);
		}
		
		
		
		status =request.getParameter("updateMessage")==null?"":request.getParameter("updateMessage");
		if(status != null && !status.equalsIgnoreCase("")){

		if(status.equalsIgnoreCase(AfConstants.SUCCESS)){
		model.addAttribute(AfConstants.SUCCESS_MESSAGE, "Category Created Successfully");

		}else if(status.equalsIgnoreCase(AfConstants.FAILURE)){
			model.addAttribute(AfConstants.ERROR_MESSAGE, "Error: Category Could not be Created ");

		}
		model.addAttribute("mapCategoryUpdateTab", true);
		}
		
		statuscatMapOther = request.getParameter("updateMessagecategory1")==null?"":request.getParameter("updateMessagecategory1");
		if(statuscatMapOther != null && !statuscatMapOther.equalsIgnoreCase("")){
		if(statuscatMapOther.equalsIgnoreCase(AfConstants.SUCCESS)){
			model.addAttribute(AfConstants.SUCCESS_MESSAGE, "Category Updates Successfully");

			}else if(statuscatMapOther.equalsIgnoreCase(AfConstants.FAILURE)){
				model.addAttribute(AfConstants.ERROR_MESSAGE, "Error: Category Could not be Updated ");

			}
		model.addAttribute("mapCategoryUpdateTab", true);
		}
		statuscatMapSearch = request.getParameter("updateMessagecategory")==null?"":request.getParameter("updateMessagecategory");
		if(statuscatMapSearch != null && !statuscatMapSearch.equalsIgnoreCase("")){
		if(statuscatMapSearch.equalsIgnoreCase(AfConstants.SUCCESS)){
			model.addAttribute(AfConstants.SUCCESS_MESSAGE, "Category Updates Successfully");

			}else if(statuscatMapSearch.equalsIgnoreCase(AfConstants.FAILURE)){
				model.addAttribute(AfConstants.ERROR_MESSAGE, "Error: Category Could not be Updated ");

			}
		model.addAttribute("mapCategoryTab", true);
		}
		model.addAttribute("bestSellingCategories", categoryManagementService.getBestSellingCategories());

		
		return "categoryManagement";
	}

	@RequestMapping(value = "/category", method = RequestMethod.POST)
	public  String addCategory(@ModelAttribute("categoryBean") CategoryBean categoryBean,
			BindingResult result, RedirectAttributes redirectAttrs, HttpServletRequest request) {
		String status;
		String catNo = "";
//                System.out.println("cat--add--post");
		try {
			if(categoryBean.getGoQuicklyCategory() == null){
				categoryBean.setGoQuicklyCategory((byte)0);
			}
			if(categoryBean.getBestSellingCategory() == null){
				categoryBean.setBestSellingCategory((byte)0);
			}

			if("UpdateCategory".equalsIgnoreCase(categoryBean.getAction())) {
				status = categoryManagementService.updateCategory(categoryBean, request);
				try {
					commonUtil.akamaiPurge();
				} catch (IOException e) {
					logger.error("Akamai Exception ",e);
				}
				if( (categoryBean.getCategoryNo() != null) &&
						!(categoryBean.getCategoryNo().trim().equalsIgnoreCase("")) ) {
					catNo = categoryBean.getCategoryNo();
				}

				if(status.equalsIgnoreCase(AfConstants.SUCCESS)) {
					redirectAttrs.addFlashAttribute(AfConstants.SUCCESS_MESSAGE, "Category Updated Successfully");
				} else if(status.equalsIgnoreCase("parentInactive")) {
					redirectAttrs.addFlashAttribute(AfConstants.ERROR_MESSAGE, "Category Could not be Updated, as it's parent category is Inactive ");
				} else {
					redirectAttrs.addFlashAttribute(AfConstants.ERROR_MESSAGE, "Category Could not be Updated ");
				}
			} else {
				status = categoryManagementService.createCategory(categoryBean, request);
				/*try {
					commonUtil.akamaiPurge();
				} catch (IOException e) {
					logger.error("Akamai Exception ",e);
				}*/
				Long categoryNo = categoryManagementService.getLastCategoryCatNo();
				if(categoryNo != null) {
					catNo = String.valueOf(categoryNo);
				}
				if(status.equalsIgnoreCase(AfConstants.SUCCESS))
					redirectAttrs.addFlashAttribute(AfConstants.SUCCESS_MESSAGE, "Category Created Successfully");
				else
					redirectAttrs.addFlashAttribute(AfConstants.ERROR_MESSAGE, "Category Could not be Created ");
			}
			redirectAttrs.addFlashAttribute(catNoStr, catNo);
		} catch(Exception e) {
			logger.error("@@@@ Exception in CategoryManagementController at addCategory() :", e);
		}
		return "redirect:/admin/category";
	}

	@RequestMapping(value="/deleteCategory")
	public @ResponseBody String deleteCategory(@RequestParam("catNo") String catNo){
		String deleteStatus = "";
		try {
			deleteStatus = categoryManagementService.deleteCategory(new Integer(catNo));
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}
			logger.debug(deleteStatus);
		} catch(Exception e) {
			logger.error("@@@@ Exception in CategoryManagementController at deleteCategory() :", e);
		}
		return deleteStatus;
	}

	
	@RequestMapping(value = "/createPartnerCategory", method = RequestMethod.POST)
	@ResponseBody
	public String createPartnerCategory(@RequestParam("json") String json, HttpServletRequest request, RedirectAttributes redirectAttrs) {
		String status = "failure";
		try {
			JSONObject createCatJson = new JSONObject(json);
			
			status =categoryManagementService.createPartnerCategory(createCatJson, request);
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}
			if(status.equalsIgnoreCase(AfConstants.SUCCESS)) {
				redirectAttrs.addFlashAttribute(AfConstants.SUCCESS_MESSAGE, "Category Created Successfully");
			} 
			else{
				redirectAttrs.addFlashAttribute(AfConstants.ERROR_MESSAGE,"Error: Category could'nt be created." );
			}
		} catch (JSONException e) {
			logger.error("@@@@ Exception in PartnerManagementController at createPartnerCategory() :", e);
		}
	return status;
			
	}
	
	
	@RequestMapping(value="/updatecategoryLogo",method=RequestMethod.POST)
	public @ResponseBody String saveImagePage(@RequestParam("file") MultipartFile file,@RequestParam("type") String type,HttpServletRequest request){
		String responseString="";
		try{

			responseString=commonService.saveImage(file,type);

		}catch(Exception e){
			logger.error("@@@@ Exception in CategoryManagementController at saveImagePage() :", e);
		}
		logger.debug("/saveImages POST responseString:"+responseString);
		return responseString;
	}

	@RequestMapping(value="/updateCategory",method=RequestMethod.POST)
	public @ResponseBody String updateCategory(@RequestParam("json") String json){

		String status=AfConstants.FAILURE;
		try {
			JSONObject userJson=new JSONObject(json);
			status=categoryManagementService.updateCategory(userJson);
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}
		} catch (Exception e) {
			logger.error("@@@@ Exception in CategoryManagementController at updateCategory() :", e);
		}

		return status;
	}

	@RequestMapping("/checkcategoryId")
	public @ResponseBody String checkcategoryId(@RequestParam("categoryId") String categoryId){
		Boolean categoryIdExist=false;
		String catId = (null!=categoryId)?categoryId.trim():"";
		
		if(catId.length()>0){
			categoryIdExist=categoryManagementService.checkcategoryIdExist(catId);
		}
		if(categoryIdExist)
			return existStr;
		else return "";
	}

	@RequestMapping(value="/mapPartnerCategories",method=RequestMethod.POST)
	public String mapCategories(@ModelAttribute("partnercategoryBean") PartnerCategoryBean partnercategoryBean,
			ModelMap model, RedirectAttributes redirectAttrs, HttpServletRequest request) {
		List<PartnerCategoryBean> list=new ArrayList<>();
		String prtNo=partnercategoryBean.getPartNO().trim();

		try{
			if(prtNo!=null && !prtNo.trim().isEmpty()){
				list=categoryManagementService.getPartnerCategories(partnercategoryBean.getPartNO());
			}
			
			if(prtNo == null || prtNo.trim().isEmpty()){
				list=categoryManagementService.getMappingDetails();
			}
			
			model.addAttribute("categoryBean",new CategoryBean());
			model.addAttribute("categoryList", categoryManagementService.getCategoryList());
			model.addAttribute("partnerList", partnermanagementService.getAllPartners());
			model.addAttribute("categoryStatus", commonService.getEnumValues(statusStr));
			model.addAttribute("categoryStatus_", commonService.getEnumValues(statusStr));
			model.addAttribute("prtCatStatus", commonService.getEnumValues(statusStr));
			model.addAttribute("mappingDetailsList", list);
			partnercategoryBean.setPartNO(partnercategoryBean.getPartNO());
			model.addAttribute("partnercategoryBean",partnercategoryBean);
			model.addAttribute("mapCategoryTab", true);
			model.addAttribute("categoryBeanId",new CategoryBean());
			model.addAttribute("createAmzonPartnerCatForm",new AfAmzonoPartnerCategoryBean());
			model.addAttribute("mappingDetailsListCat", categoryManagementService.getMappingDetailsExceptSearch());

			commonUtil.getDefaultCategoryTree(request, "", "");
		}catch(Exception e){
			logger.error("@@@@ Exception in CategoryManagementController at mapCategories() :", e);
		}
		return "categoryManagement";
	}

	@RequestMapping(value="/getMappingDetails",method=RequestMethod.GET)
	public @ResponseBody String mappingDetails(ModelMap model){
		List<PartnerCategoryBean> list=new ArrayList<>();
		try{
			list=categoryManagementService.getMappingDetails();
		}catch(Exception e){
			logger.error("@@@@ Exception in CategoryManagementController at mappingDetails() :", e);
		}


		return GsonUtil.toJson(list);
	}

	@RequestMapping(value="/deactivateMappedCategory")
	public @ResponseBody String deleteMappedCategory(@RequestParam("prtCatNo") String prtCatNo, HttpServletRequest request){
		String deleteStatus="";
		try{

			deleteStatus=categoryManagementService.deactivateMappedCategory(new Integer(prtCatNo), request);	
			logger.debug(deleteStatus);
		}catch(Exception e){
			logger.error("@@@@ Exception in CategoryManagementController at deleteMappedCategory() :", e);
		}
		return deleteStatus;

	}

	@RequestMapping(value="/addCategoryCancel",method=RequestMethod.GET)
	public @ResponseBody String addPartnerCancel(ModelMap model){
		return "admin/category";
	}

	@RequestMapping(value="/updateMappedCategory",method=RequestMethod.POST)
	public @ResponseBody String updatePartnerCategory(@RequestParam("json") String json,ModelMap model,RedirectAttributes redirectAttrs, HttpServletRequest request){
		String status=AfConstants.FAILURE;
		try {
			JSONObject userJson=new JSONObject(json);
			status=categoryManagementService.updatePartnerCategory(userJson, request);
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}
		}catch (ConstraintViolationException c) { 
			logger.error("@@@@ Exception in CategoryManagementController at updatePartnerCategory() :", c);
		}
		catch (Exception e) {
			logger.error("Error >>>>>>>>>>>>", e);
		}

		return status;
	}
	
	@RequestMapping(value="/updatePartnerCategoryMapped",method=RequestMethod.POST)
	public @ResponseBody String updatePartnerCategoryMapped(@RequestParam("json") String json,ModelMap model,RedirectAttributes redirectAttrs, HttpServletRequest request){
		String status=AfConstants.FAILURE;
		try {
			JSONObject userJson=new JSONObject(json);
			status=categoryManagementService.updatePartnerCategoryMapped(userJson, request);
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}
		}catch (ConstraintViolationException c) { 
			logger.error("@@@@ Exception in CategoryManagementController at updatePartnerCategoryMapped() :", c);
		}
		catch (Exception e) {
			logger.error("Error >>>>>>>>>>>>", e);
		}

		return status;
	}
	
	
	
	

	@RequestMapping(value="/updatePartnerCategoryCancel",method=RequestMethod.GET)
	public @ResponseBody String updatePartnerCategoryCancel(ModelMap model){
		return AfConstants.SUCCESS;
	}

	@RequestMapping(value="/updatePartnerCategory")
	public  String updatePartnerCategory(@ModelAttribute("partnercategoryBean") PartnerCategoryBean partnercategoryBean,ModelMap model,HttpServletRequest request){
		try{
			String updateMessage=request.getParameter("updateMessage");
			String deletePrtCatMsg=request.getParameter("deletePrtCatMsg");
			String prtNo=request.getParameter("prtNo")!=null?request.getParameter("prtNo"):"";
			model.addAttribute("categoryBean",new CategoryBean());
			model.addAttribute("partnercategoryBean",new PartnerCategoryBean());
			model.addAttribute("categoryList", categoryManagementService.getCategoryList());
			model.addAttribute("partnerList", partnermanagementService.getAllPartners());
			model.addAttribute("categoryStatus", commonService.getEnumValues(statusStr));
			model.addAttribute("categoryStatus_", commonService.getEnumValues(statusStr));
			model.addAttribute("prtCatStatus", commonService.getEnumValues(statusStr));
			model.addAttribute("prtCatupdateStatus", commonService.getEnumValues(statusStr));
			
			partnercategoryBean.setPartNO(prtNo);
			model.addAttribute("partnercategoryBean",partnercategoryBean);

			List<PartnerCategoryBean> list=null;
			
			if("".equals(prtNo)){
				list = categoryManagementService.getMappingDetails();
			}
			else{
				list=categoryManagementService.getPartnerCategories(prtNo);
			}

			model.addAttribute("mappingDetailsList",list);
			model.addAttribute("mapCategoryTab", true);
			model.addAttribute("mappingDetailsListCat", list);
			model.addAttribute("mapCategoryUpdateTab", true);

			if(updateMessage!=null) {
				if(updateMessage.equalsIgnoreCase(AfConstants.SUCCESS))
					model.addAttribute(AfConstants.SUCCESS_MESSAGE, "Partner Category Updated Successfully");

				else if(updateMessage.equalsIgnoreCase(AfConstants.FAILURE))
					model.addAttribute(AfConstants.ERROR_MESSAGE, "Partner Category Not Updated,Error Occured");
			}

			if(deletePrtCatMsg!=null) {
				
				if(deletePrtCatMsg.equalsIgnoreCase(AfConstants.SUCCESS))
					model.addAttribute(AfConstants.SUCCESS_MESSAGE, "Partner Category Deactivated Successfully");

				else if(deletePrtCatMsg.equalsIgnoreCase(AfConstants.FAILURE))
					model.addAttribute(AfConstants.ERROR_MESSAGE, "Partner Category Cannot be Deactivated! Error Occured");
			}

			model.addAttribute("categoryBeanId",new CategoryBean());

			commonUtil.getDefaultCategoryTree(request, "", "");
		}catch(Exception e){
			logger.error("@@@@ Exception in CategoryManagementController at updatePartnerCategory() :", e);	
		}
		return "categoryManagement";
	}

	@RequestMapping("/checkCategoryDisplayOrder")
	public @ResponseBody String checkCategoryDisplayOrder(HttpServletRequest request){
		boolean checkFlag=false;
		String categoryDisplayOrder = request.getParameter("categoryDisplayOrder");

		if(categoryDisplayOrder.length()>0){
			checkFlag=categoryManagementService.checkCategoryPosition(categoryDisplayOrder);
			if(checkFlag) return existStr; else return "";
		}
		else{
			return "";	
		}
	}

	@RequestMapping(value="/deleteCategory", method = RequestMethod.POST)
	public String deleteCategory(ModelMap model, HttpServletRequest request, RedirectAttributes redirectAttrs){
		try {
			String catNo = request.getParameter(catNoStr);
			String status = null;
			if( (null != catNo) && !(catNo.trim().equalsIgnoreCase("")) ) {
				status = categoryManagementService.deleteCategory(catNo, redirectAttrs, request);
				try {
					commonUtil.akamaiPurge();
				} catch (IOException e) {
					logger.error("Akamai Exception ",e);
				}
				if(status.equalsIgnoreCase(AfConstants.SUCCESS)) {
					redirectAttrs.addFlashAttribute(AfConstants.SUCCESS_MESSAGE, "Category Deleted Successfully");
				} else if(status.equalsIgnoreCase(AfConstants.FAILURE)) {
					redirectAttrs.addFlashAttribute(AfConstants.ERROR_MESSAGE, "Category Cannot be Deleted! Error Occured");
				}
			}
		} catch (Exception e) {
			redirectAttrs.addFlashAttribute("errormessage", "Category Cannot be Deleted! Error Occured");
			logger.error("@@@@ Exception in CategoryManagementController at deleteCategory()", e);	
		}
		return "redirect:/admin/category";
	}

	@RequestMapping("/checkCategoryName")
	public @ResponseBody String checkcategoryName(@RequestParam("categoryName") String categoryName){
		Boolean isCategoryNameExist = false;
		String catName = (null != categoryName) ? categoryName.trim() : "";
		
		if(catName.length() > 0) {
			isCategoryNameExist = categoryManagementService.checkcategoryNameExist(catName);
		}
		if(isCategoryNameExist)
			return existStr;
		else return "";
	}
	
	
	@RequestMapping("/checkCreatePartnerCategoryName")
	public @ResponseBody String checkCreatePartnercategoryName(@RequestParam("createCategoryName") String createCategoryName,@RequestParam("prtNo") Integer prtNo){
		Boolean isCategoryNameExist = false;
		String catName = (null != createCategoryName) ? createCategoryName.trim() : "";
		if(catName.length() > 0) {
			isCategoryNameExist = categoryManagementService.checkpartnerCategoryNameExist(catName,prtNo);
		}
		if(isCategoryNameExist)
			return existStr;
		else return "";
	}
	
	
 
	@RequestMapping("/checkCreatePartnerCategoryId")
	public @ResponseBody String checkCreatePartnercategoryId(@RequestParam("createCategoryId") String createCategoryId){
		Boolean isCategoryIdExist = false;
		String catId = (null != createCategoryId) ? createCategoryId.trim() : "";
		if(catId.length() > 0) {
			isCategoryIdExist = categoryManagementService.checkpartnerCategoryIdExist(catId);
		}
		if(isCategoryIdExist)
			return existStr;
		else return "";
	}
	
	

	@RequestMapping("/checkCategoryDispOrd")
	public @ResponseBody String checkCategoryDispOrd(HttpServletRequest request){
		boolean checkFlag = false;
		String displayOrder = request.getParameter("displayOrder");
		displayOrder = (null != displayOrder) ? displayOrder.trim() : "";
		
		if (displayOrder.length() > 0) {
			checkFlag = categoryManagementService.checkCategoryDispOrd(displayOrder);
		}
		
		if(checkFlag){
			return existStr;
		}
		else{
			return "";
		}
	}

	@RequestMapping(value="/mapCreatePartnerCategories",method=RequestMethod.POST)
	public  String mapCreatePartnerCategories(@ModelAttribute("partnercategoryBean") PartnerCategoryBean partnercategoryBean,
			ModelMap model,
			RedirectAttributes redirectAttrs,
			HttpServletRequest request,
			@RequestParam("partnameup") String partnameup,
			@RequestParam("partnertypeup") String partnertypeup) {
		try {
			List<PartnerCategoryBean> list=new ArrayList<>();
			List<PartnerCategoryBean> list2=new ArrayList<>();
		
			if(partnameup!=null && !partnameup.trim().isEmpty()){
				list=categoryManagementService.getPartnerCategories(partnameup);
			}
			
			if(partnameup == null || partnameup.trim().isEmpty()){
				list=categoryManagementService.getMappingDetails();
			}
			model.addAttribute("categoryBeanId",new CategoryBean());
			model.addAttribute("categoryBean",new CategoryBean());
			partnercategoryBean = new PartnerCategoryBean();
			partnercategoryBean.setPartNO(partnercategoryBean.getPartNO());
			model.addAttribute("partnercategoryBean",partnercategoryBean);
			model.addAttribute("mappingDetailsListCat", list);
			model.addAttribute("categoryupdateStatus", commonService.getEnumValues(statusStr));
			model.addAttribute("categoryupdateStatus_", commonService.getEnumValues(statusStr));
			model.addAttribute("prtCatStatus", commonService.getEnumValues(statusStr));
			model.addAttribute("categoryStatus", commonService.getEnumValues(statusStr));
			model.addAttribute("categoryList", categoryManagementService.getCategoryList());
			model.addAttribute("partnerList", partnermanagementService.getAllPartners());
			model.addAttribute("mapCategoryUpdateTab", true);
			model.addAttribute("mappingDetailsList", categoryManagementService.getMappingDetails());
			model.addAttribute("createAmzonPartnerCatForm",new AfAmzonoPartnerCategoryBean());
			commonUtil.getDefaultCategoryTree(request, "", "");
			model.addAttribute("partnertypeup", partnertypeup);
			model.addAttribute("partnameup", partnameup);
		} catch (Exception e) {
			logger.error("@@@@ Exception in CategoryManagementController at mapCreatePartnerCategories()", e);	
		}
	return "categoryManagement";
	}
	
	@RequestMapping(value="/mapAmazonPartnerCategories",method=RequestMethod.GET)
	public String amzonPartnerCategory(ModelMap model,@ModelAttribute("afAmzonoPartnerCategoryBean") AfAmzonoPartnerCategoryBean afAmzonoPartnerCategoryBean,HttpServletRequest request){
		model.addAttribute("afAmzonoPartnerCategoryBean",afAmzonoPartnerCategoryBean);
		model.addAttribute("amazonMappingDetailsList", categoryManagementService.getAmazonMappingDetails());
		model.addAttribute("categoryList", categoryManagementService.getCategoryList());
		model.addAttribute("partnerList", partnermanagementService.getAllPartners());
		model.addAttribute("createAmzonPartnerCatForm",new AfAmzonoPartnerCategoryBean());
		model.addAttribute("updateamazonCategory", true);
	return "categoryManagement";
	}
	
	
	@RequestMapping(value = "/createAmazonPartnerCategory", method = RequestMethod.POST)
	public @ResponseBody String createAmazonPartner(@RequestParam("json") String json, ModelMap model,
			RedirectAttributes redirectAttrs, HttpServletRequest request) {
		String status="";
		try {
			JSONObject createCatJson = new JSONObject(json);

			status = categoryManagementService.CreateAmazonPartner(createCatJson, request);
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}
		} catch (Exception e) {
			
			logger.error("@@@@ Exception in CategoryManagementController at CreateAmazonPartner() :", e);
		}

		return status;
	}
	
	
	@RequestMapping(value = "/updateAmazonCategory", method = RequestMethod.POST)
	public @ResponseBody String updateAmazonPartner(@RequestParam("json") String json, ModelMap model,
			RedirectAttributes redirectAttrs, HttpServletRequest request) {
		String status="";
		try {
			JSONObject createCatJson = new JSONObject(json);

			status = categoryManagementService.updateAmazonPartner(createCatJson, request);
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}

		} catch (Exception e) {
			
			logger.error("@@@@ Exception in CategoryManagementController at CreateAmazonPartner() :", e);
		}

		return status;
	}
	
	
	
	
}