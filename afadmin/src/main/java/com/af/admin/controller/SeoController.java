package com.af.admin.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.af.admin.beans.SeoBean;
import com.af.admin.service.CommonService;
import com.af.admin.utils.CommonUtil;


@Controller
public class SeoController {
	
	private Logger logger = LogManager.getLogger(SeoController.class.getName());
	
	@Autowired
	CommonService commonService;
	
	@Autowired
	CommonUtil commonUtil;
	
	@RequestMapping(value="/seo",method=RequestMethod.GET)
	public String getSEOData(ModelMap model, HttpServletRequest request) {
		model.addAttribute("seoPageId", commonService.getEnumValues("SEO_PAGE_ID"));
		model.addAttribute("seoBean", new SeoBean());
		model.addAttribute("seoData", commonService.getSEOData());
		return "seo";
	}

	@RequestMapping(value="/seo",method=RequestMethod.POST)
	public String saveSEOData(@ModelAttribute("seoBean") SeoBean seoBean, ModelMap model, HttpServletRequest request) {
		String status = "failure";
		
		try{
			status = commonService.saveSEOData(seoBean,request);
			/*try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}*/
		}
		catch(Exception e){
			logger.error("@@@@ Exception in SEOController at saveSEOData() :", e);
		}
		
		model.addAttribute("seoPageId", commonService.getEnumValues("SEO_PAGE_ID"));
		model.addAttribute("seoBean", new SeoBean());
		model.addAttribute("seoData", commonService.getSEOData());
		model.addAttribute("selectedPage", null!=seoBean.getSeoPageId()?seoBean.getSeoPageId():"");
		
		if(status.equalsIgnoreCase("success")){
			model.addAttribute("message", "SEO data added successfully");
		}
		else{
			model.addAttribute("errorMessage", "SEO data Could not be added");
		}
		
		return "seo";
	}
	
}
