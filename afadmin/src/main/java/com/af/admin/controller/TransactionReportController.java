package com.af.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.af.admin.beans.TransactionReportBean;
import com.af.admin.service.CommonService;
import com.af.admin.service.TransactionReportService;

@Controller
public class TransactionReportController {

	@Autowired
	TransactionReportService transactionReportService;    
	  
	@RequestMapping(value="/transactionReport", method=RequestMethod.GET)
	public String getTransactionReportPage(ModelMap model) {
		model.addAttribute("transactionReportBean",new TransactionReportBean());
		return "transactionReport";
	}
	
	@RequestMapping(value="/transactionList", method=RequestMethod.POST)
	public String getTransactionList(@RequestParam("inputMemberId") String inputMemberId, ModelMap model) {
		inputMemberId = (null!=inputMemberId && inputMemberId.length() > 0)?inputMemberId.trim():"";
		model.addAttribute("transactionReportBean",new TransactionReportBean());
		model.addAttribute("transactionList", transactionReportService.getTransactionDetails(inputMemberId));
		return "transactionReport";
	}
		
}  
