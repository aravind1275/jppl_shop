package com.af.admin.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.af.admin.beans.PartnerBean;
import com.af.admin.service.PartnerManagementService;
import com.af.admin.utils.CommonUtil;

@Controller
public class PopupContentController {

	private Logger logger = LogManager.getLogger(SeoController.class.getName());

	@Autowired
	PartnerManagementService partnerManagementService;
	
	@Autowired
	CommonUtil commonUtil;

	@RequestMapping(value = "/partnerPopupContent", method = RequestMethod.GET)
	public String getPopupContent(ModelMap model, HttpServletRequest request) {
		
		model.addAttribute("partnerPopupContent", new PartnerBean());
		model.addAttribute("prtList", partnerManagementService.getAllPartners());
		return "popupContent";
	}

	@RequestMapping(value = "/partnerPopupContent", method = RequestMethod.POST)
	public String setPopupContent(@ModelAttribute("partnerPopupContent") PartnerBean partnerBean, ModelMap model,
			HttpServletRequest request) {
		
		String status = "failure";

		try {
			status = partnerManagementService.savePopupContentByPartner(partnerBean,request);
			/*try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}*/
		} catch (Exception e) {
			logger.error("@@@@ Exception in PopupContentController at setPopupContent() :", e);
		}
		model.addAttribute("partnerPopupContent", new PartnerBean());
		model.addAttribute("prtList", partnerManagementService.getAllPartners());
		model.addAttribute("selectedPartner", null != partnerBean.getPrtNo() ? partnerBean.getPrtNo() : "");

		if (status.equalsIgnoreCase("success")) {
			model.addAttribute("message", "Popup Content data added successfully");
		} else {
			model.addAttribute("errorMessage", "Popup Content data Could not be added");
		}

		return "popupContent";
	}
}
