package com.af.admin.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.af.admin.service.CommonService;

@Controller
public class CacheController {

	private Logger logger = LogManager.getLogger(CacheController.class.getName());
	
	@Autowired
	CommonService commonService;
	
	@RequestMapping(value="/cache", method=RequestMethod.GET)
	public String cache(ModelMap model){
		return "cache";
	}
	
	@RequestMapping(value="/clearCache")
	public @ResponseBody String clearCache(){
		String response = "failure";
		try{
			response = commonService.clearCache();
		}
		catch(Exception e){
			logger.error("@@@@ Exception in CacheController clearCache(): ", e);
		}
		return response;
	}
}
