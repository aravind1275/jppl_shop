package com.af.admin.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.af.admin.beans.AdminRolesBean;
import com.af.admin.beans.AdminUserBean;
import com.af.admin.beans.AfPermissionValuesBean;
import com.af.admin.beans.UserBean;
import com.af.admin.constants.AfConstants;
import com.af.admin.entity.AfPermissionValues;
import com.af.admin.service.CommonService;
import com.af.admin.service.UserManagementService;
import com.af.admin.utils.CommonUtil;


@Controller
public class UserManagementController {
	
	private Logger logger = LogManager.getLogger(UserManagementController.class.getName());
	
	@Autowired
	UserManagementService userManagementService;
	
	@Autowired
	CommonService commonService;
	
	@Autowired
	CommonUtil commonUtil;
	
	@RequestMapping(value="/userManagement",method=RequestMethod.GET)
	public String userManagementPage(ModelMap model,HttpServletRequest request){
		String updateMessage=request.getParameter("updateMessage");
		List<UserBean> listUserBeans=new ArrayList<UserBean>();
		List<AdminRolesBean>  listAdminRolesBeans=null;
		Map<Byte, String> statusValues=new HashMap<Byte, String>();
		try{
			listUserBeans=userManagementService.getAdminUsers();
			listAdminRolesBeans=commonService.getAdminRoles();
			statusValues=commonService.getEnumValues("STATUS");
			
		}catch(Exception e){
			logger.error("@@@@ Exception in UserManagementController at userManagementPage() :", e);
		}
		model.addAttribute("userList", listUserBeans);
		model.addAttribute("roles", listAdminRolesBeans);
		model.addAttribute("statusValues",statusValues);
		if(updateMessage!=null)
		{
			if(updateMessage.equalsIgnoreCase(AfConstants.SUCCESS))
			model.addAttribute(AfConstants.SUCCESS_MESSAGE, "User Updated Successfully");
	
			else if(updateMessage.equalsIgnoreCase(AfConstants.FAILURE))
			model.addAttribute(AfConstants.ERROR_MESSAGE, "User Could  Not Be Updated,Error Occured");
		}
		return "userManagement";
	}
	
	@RequestMapping(value="/createUser",method=RequestMethod.GET)
	public String userCreatePage(ModelMap model){
		List<AdminRolesBean>  listAdminRolesBeans=new ArrayList<AdminRolesBean>();
		Map<Byte, String> statusValues=new HashMap<Byte, String>();
		try{
			listAdminRolesBeans=commonService.getAdminRoles();
			statusValues=commonService.getEnumValues("STATUS");
		}catch(Exception e){
			logger.error("@@@@ Exception in UserManagementController at userCreatePage() :", e);
		}
		model.addAttribute("roles", listAdminRolesBeans);
		model.addAttribute("statusValues",statusValues);
		model.addAttribute("adminUserBean",new AdminUserBean());
		return "addUser";
	}
	
	
	@RequestMapping(value="/createUser",method=RequestMethod.POST)
	public String userManagementPage(@ModelAttribute("adminUserBean") AdminUserBean adminUserBean,ModelMap model,RedirectAttributes redirectAttrs,HttpServletRequest request){
		String status;
		try{
			adminUserBean.setUserId((Integer)request.getSession().getAttribute("userId"));
			status=userManagementService.createUser(adminUserBean,request);
			/*try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}*/
			if(status.equalsIgnoreCase(AfConstants.SUCCESS))
			redirectAttrs.addFlashAttribute(AfConstants.SUCCESS_MESSAGE, "User Created Successfully");
			else if(status.equalsIgnoreCase(AfConstants.FAILURE))
				redirectAttrs.addFlashAttribute(AfConstants.ERROR_MESSAGE, "User Could Not Be Created!Error Occured");

		}catch(Exception e){
			logger.error("@@@@ Exception in UserManagementController at userManagementPage() :", e);
		}
		
		return "redirect:/admin/userManagement";
	}
	
	@RequestMapping(value="/updateUser",method=RequestMethod.POST)
	public @ResponseBody String updateUser(@RequestParam("json") String json,HttpServletRequest request,RedirectAttributes redirectAttrs){
		
		String status="failure";
		try {
			JSONObject userJson=new JSONObject(json);
		
			status=userManagementService.updateAdminUser(userJson,request);
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}
		} catch (Exception e) {
			logger.error("@@@@ Exception in UserManagementController at updateUser() :", e);
		}
		return status;
	}
	
	@RequestMapping("/checkUserName")
	public @ResponseBody String checkUserNameCall(@RequestParam("userName") String userName){
		
		Boolean userNameExist=false;
		userName=(null!=userName)?userName.trim():"";
		if(userName.length()>0){
			userNameExist=userManagementService.checkUserName(userName);
		}
		if(userNameExist)return "exist";
		else return "";
	}
	
	@RequestMapping("/checkRoleName")
	public @ResponseBody String checkRoleNameCall(@RequestParam("roleName") String roleName){
		
		Boolean userRoleExist=false;
		roleName=(null!=roleName)?roleName.trim():"";
		if(roleName.length()>0){
			userRoleExist=userManagementService.checkRoleName(roleName);
		}
		if(userRoleExist)return "exist";
		else return "";
	}
	
	@RequestMapping(value="/deleteUser")
	public @ResponseBody String deleteUser(@RequestParam("userId") Integer userId,HttpServletRequest request){
		
		String deleteStatus="";
		try{
			deleteStatus=userManagementService.deleteUser(userId,request);
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}
			logger.debug(deleteStatus);
		}catch(Exception e){
			logger.error("@@@@ Exception in UserManagementController at deleteUser() :", e);
		}
		return deleteStatus;
		
	}
	
	@RequestMapping(value="/roleManagement",method=RequestMethod.GET)
	public String roleManagementPage(ModelMap model,HttpServletRequest request){
		String updateMessage=request.getParameter("updateMessage");
		List<AdminRolesBean> listAdminRolesBeans=new ArrayList<AdminRolesBean>();
		try{
			listAdminRolesBeans = commonService.getAdminRoles();
			if(updateMessage!=null)
			{
				if(updateMessage.equalsIgnoreCase(AfConstants.SUCCESS))
				model.addAttribute(AfConstants.SUCCESS_MESSAGE, "Role Updated Successfully");
		
				else if(updateMessage.equalsIgnoreCase(AfConstants.FAILURE))
				model.addAttribute(AfConstants.ERROR_MESSAGE, "User Could  Not Be Updated,Error Occured");
			}
		}catch(Exception e){
			logger.error("@@@@ Exception in UserManagementController at roleManagementPage() :", e);
		}
		model.addAttribute("roles", listAdminRolesBeans);	
		return "roleManagement";
	}
	
	@RequestMapping(value="/createRole",method=RequestMethod.GET)
	public String createRolePage(ModelMap model){
		List<AfPermissionValuesBean> permissionValues=new ArrayList<AfPermissionValuesBean>();
		try{
			permissionValues=commonService.getPermissionValues();
		}catch(Exception e){
			logger.error("@@@@ Exception in UserManagementController at createRolePage() :", e);
		}
		model.addAttribute("permissionValues", permissionValues);
		model.addAttribute("adminRolesBean",new AdminRolesBean());
		return "createRole";
	}
	
	@RequestMapping(value="/createRole",method=RequestMethod.POST)
	public String createRolePage(@ModelAttribute("adminRolesBean") AdminRolesBean adminRolesBean,HttpServletRequest request,RedirectAttributes redirectAttrs){
		String status;
		try{
			adminRolesBean.setUserId((Integer)request.getSession().getAttribute("userId"));
			status=userManagementService.createRole(adminRolesBean, request);
			/*try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}*/
			if(status.equalsIgnoreCase("success"))
				redirectAttrs.addFlashAttribute("message", "Role Created Successfully");
			else if(status.equalsIgnoreCase("failure"))
				redirectAttrs.addFlashAttribute("errormessage", "Role Could Not Be Created!Error Occurred");				}catch(Exception e){
			logger.error("@@@@ Exception in UserManagementController at createRolePage() :", e);
		}
		return "redirect:/admin/roleManagement";
	}
	
	@RequestMapping(value="/deleteRole")
	public @ResponseBody String deleteRolePage(@RequestParam("roleId") Integer roleId, HttpServletRequest request, RedirectAttributes redirectAttrs){
		
		String deleteStatus="";
		try{
			deleteStatus=userManagementService.deleteRole(roleId, request);
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}
			redirectAttrs.addFlashAttribute("message", "Role Updated Successfully");
			logger.debug(deleteStatus);
		}catch(Exception e){
			logger.error("@@@@ Exception in UserManagementController at deleteRolePage() :", e);
		}
		return "redirect:/admin/roleManagement";
	}
	
	@RequestMapping(value="/updateRole")
	public String updateRole(@RequestParam("roleId") String roleId,ModelMap model){
		List<AfPermissionValuesBean> permissionValues=new ArrayList<AfPermissionValuesBean>();
		AdminRolesBean adminRolesBean=null;
		try{
			permissionValues=commonService.getPermissionValues();
			adminRolesBean=userManagementService.getRoleById(roleId);
		}catch(Exception e){
			logger.error("@@@@ Exception in UserManagementController at updateRole() :", e);
		}
		model.addAttribute("permissionValues", permissionValues);
		
		if(null==adminRolesBean)
			model.addAttribute("adminRolesBean",new AdminRolesBean());
		else model.addAttribute("adminRolesBean",adminRolesBean);
		
		return "updateRole";
	}
	
	
	@RequestMapping(value="/updateRole",method=RequestMethod.POST)
	public String updateRole(@ModelAttribute("adminRolesBean") AdminRolesBean adminRolesBean,HttpServletRequest request,RedirectAttributes redirectAttrs){
		adminRolesBean.setUserId((Integer)request.getSession().getAttribute("userId"));
		try{
			userManagementService.updateRole(adminRolesBean, request);
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}
			redirectAttrs.addFlashAttribute("message", "Role Updated Successfully");
		}catch(Exception e){
			logger.error("@@@@ Exception in UserManagementController at updateRole() :", e);
		}
		return "redirect:/admin/roleManagement";
	}
	
	@RequestMapping(value="/viewRole")
	public String viewRole(@RequestParam("roleId") String roleId,ModelMap model){
		List<AfPermissionValues> permissionValues=null;
		Map<AfPermissionValues, Map<Byte, String>> permissionActionsForvalues=null;
		Map<Byte, String> permissionActions=null;
		AdminRolesBean adminRolesBean=null;
		try{
			
			permissionActions=commonService.getEnumValues("PERMISSION_ACTIONS");
			permissionActionsForvalues=userManagementService.permissionActionsForvalues(permissionValues,permissionActions);
			adminRolesBean=userManagementService.getRoleById(roleId);
			
		}catch(Exception e){
			logger.error("@@@@ Exception in UserManagementController at viewRole() :", e);
		}
		
		model.addAttribute("permissionActionsForvalues", permissionActionsForvalues);
		if(null==adminRolesBean)model.addAttribute("adminRolesBean",new AdminRolesBean());
		else model.addAttribute("adminRolesBean",adminRolesBean);
		
		return "viewRole";
	}
	
	
	
	@RequestMapping("/getRolePermissionsById")
	public @ResponseBody String[] getLinkQuestionsByTitle(@RequestParam("roleId") String roleId){
		String[] rolePermissions=userManagementService.getRolePermissionsById(roleId);
		
		return rolePermissions;
		
	}
}
