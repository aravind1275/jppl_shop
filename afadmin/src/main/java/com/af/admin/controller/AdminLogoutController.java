package com.af.admin.controller;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
@Controller
@RequestMapping("/logout")
public class AdminLogoutController {
	@RequestMapping(method=RequestMethod.GET)
	public String logoutPage(HttpServletRequest request){
		if(request.getSession()!=null){
			request.getSession().invalidate();
		}
		return "redirect:/";
	}
}
