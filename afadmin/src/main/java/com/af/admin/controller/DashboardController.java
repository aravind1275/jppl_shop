package com.af.admin.controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.af.admin.service.CommonService;

@Controller
public class DashboardController {
	private Logger logger = LogManager.getLogger(DashboardController.class.getName());
	
	@Autowired
	CommonService commonService;
	
	@RequestMapping(value="/dashboard",method=RequestMethod.GET)
	public String dashboardPage(ModelMap model){
		logger.debug("inside dashboard page");
		return "dashboard";
		
	}
}