package com.af.admin.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.af.admin.beans.AfNotificationBean;
import com.af.admin.constants.AfConstants;
import com.af.admin.service.CommonService;
import com.af.admin.service.NotificationService;
import com.af.admin.service.PartnerManagementService;
import com.af.admin.utils.CommonUtil;



@Controller
public class NotificationController {
	
	@Autowired
	CommonService commonService;

	@Autowired
	NotificationService notificationService;
	
	@Autowired
	PartnerManagementService partnermanagementService;
	
	@Autowired
	CommonUtil commonUtil;

	
	private Logger logger = LogManager.getLogger(NotificationController.class.getName());
			
	@RequestMapping(value="/notification",method=RequestMethod.GET)
	public String notification(ModelMap model,HttpServletRequest request){
		String updateMessage=request.getParameter("updateMessage");
		String deleteBnrMsg=request.getParameter("deleteBnrMsg");
		model.addAttribute("notificatioList", "");
		model.addAttribute("notificationBean", new AfNotificationBean());
		try{
			model.addAttribute("notificationList", notificationService.getNotificationList());
			model.addAttribute("partnerList", partnermanagementService.getPartners());
			model.addAttribute("notificationStatus", commonService.getEnumValues("STATUS"));
			if(updateMessage!=null)
			{
				if(updateMessage.equalsIgnoreCase(AfConstants.SUCCESS))
				model.addAttribute(AfConstants.SUCCESS_MESSAGE, "Notification Updated Successfully");
		
				else if(updateMessage.equalsIgnoreCase(AfConstants.FAILURE))
				model.addAttribute(AfConstants.ERROR_MESSAGE, "Notification Could  Not Be Updated,Error Occured");
			}
			
			if(deleteBnrMsg!=null)
			{
				if(deleteBnrMsg.equalsIgnoreCase("success"))
				model.addAttribute("message", "Notification Deleted Successfully");
		
			else if(deleteBnrMsg.equalsIgnoreCase(AfConstants.FAILURE))
				model.addAttribute("errormessage", "Notification Could Not be Deleted! Error Occured");
			}
		}
		catch(Exception e){
			logger.error("@@@@ Exception in NotificationController at notification() ", e);
		}
		
		return "notification";
	
	}
	@RequestMapping(value="/notification",method=RequestMethod.POST)
	public String NotificationCreate(@ModelAttribute("notificationBean") AfNotificationBean notificationBean,ModelMap model,RedirectAttributes redirectAttrs,HttpServletRequest request){
	
		String status;
		status=notificationService.createNotification(notificationBean, request);
		/*try {
			commonUtil.akamaiPurge();
		} catch (IOException e) {
			logger.error("Akamai Exception ",e);
		}*/
		model.addAttribute("notificationList", notificationService.getNotificationList());		
		if(status.equalsIgnoreCase("success"))
			redirectAttrs.addFlashAttribute("message", "Notification Created Successfully");
		else
			redirectAttrs.addFlashAttribute("errormessage", "Notification Could not be Created ");

		return "redirect:/admin/notification";
	}
	@RequestMapping(value="/deleteNotification")
	public @ResponseBody String deleteNotification(@RequestParam("notificationNo") Integer notificationNo,HttpServletRequest request){
		String deleteStatus="";
		try{
			deleteStatus=notificationService.deleteNotification(notificationNo,request);
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}
		}catch(Exception e){
			
			logger.error("@@@@ Exception in NotificationController at deleteNotification() :", e);
			deleteStatus="failure";
			return deleteStatus;
		}
		return deleteStatus;
		
	}
	@RequestMapping(value="/updateNotification", method=RequestMethod.POST)
	public @ResponseBody String updateNotification(@RequestParam("json") String json,HttpServletRequest request){
		String status=AfConstants.FAILURE;
		try {
			JSONObject userJson=new JSONObject(json);			
			status=notificationService.updateNotification(userJson,request);
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}
		} catch (Exception e) {
			logger.error("@@@@ Exception in NotificationController at updateNotification() :", e);
		}
		return status;
	}
	@RequestMapping(value="/addNotificationCancel",method=RequestMethod.GET)
	public @ResponseBody String addPartnerCancel(ModelMap model){
		return "admin/notification";
	}
	
	@RequestMapping("/checkNotificationID")
	@ResponseBody
	public String checkNotificationID(@RequestParam("notificationId") String notificationid) {
		Boolean notificationIDExist = false;
		String notificationId = notificationid;
		notificationId = (null != notificationId) ? notificationId.trim() : "";
		if (notificationId.length() > 0) {
			notificationIDExist = notificationService.checkNotificationID(notificationId);
		}
		if (notificationIDExist)
			return "exist";
		else
			return "";
	}
	
	
}
