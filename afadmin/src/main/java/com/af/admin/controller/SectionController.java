package com.af.admin.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.af.admin.beans.HomePageSectionBean;
import com.af.admin.beans.SectionBean;
import com.af.admin.beans.SectionItemsBean;
import com.af.admin.constants.AfConstants;
import com.af.admin.service.CommonService;
import com.af.admin.service.PartnerManagementService;
import com.af.admin.service.SectionManagementService;
import com.af.admin.utils.CommonUtil;

@Controller
public class SectionController {
	
	private Logger logger = LogManager.getLogger(SectionController.class.getName());

	@Autowired
	SectionManagementService sectionManagementService;
	
	@Autowired
	CommonService commonService;
	
	@Autowired
	CommonUtil commonUtil;
	
	@Autowired
	PartnerManagementService partnermanagementService;
	
	@RequestMapping(value = "/homePageSections", method = RequestMethod.GET)
	public String getSections(SectionBean sectionBean, ModelMap model, HttpServletRequest request, RedirectAttributes redirectAttrs) {
		String updateMessage = request.getParameter("updateMessage");
		String sectionId = request.getParameter("sectionId");
		String itemTab = request.getParameter("itemTab");
		String deleteMsg = request.getParameter("deleteMsg");
		List<SectionItemsBean> sectionItemList = null;
                System.out.println("-------->"+sectionId);
		if(sectionId!=null && !sectionId.isEmpty() && !sectionId.equalsIgnoreCase("null"))
		{
			sectionItemList =  sectionManagementService.getSectionItems(sectionId);
			model.addAttribute("sectionItemList", sectionItemList);
		}
	      
		List<SectionBean> sectionList =  sectionManagementService.getSections("All", 1);
		
		model.addAttribute("sectionList", sectionList);
		model.addAttribute("sectionItemTab", false);
		if (sectionList == null || sectionList.isEmpty()) {
			model.addAttribute("SectionMessage", "No Section Found");
		}
		
		if(itemTab != null && itemTab.equalsIgnoreCase("1")){
			model.addAttribute("sectionItemTab", true);
		}
		else{
			model.addAttribute("sectionItemTab", false);
		}
		
		if (deleteMsg != null) {
			if (deleteMsg.equalsIgnoreCase(AfConstants.SUCCESS))
				model.addAttribute("message", "Item deleted Successfully");

			else if (deleteMsg.equalsIgnoreCase(AfConstants.FAILURE))
				model.addAttribute("errormessage",
						"Item Could not be Deleted");
		}
		model.addAttribute("sectionItemStatus", commonService.getEnumValues("STATUS"));
		model.addAttribute("itemType", commonService.getEnumValues("ITEM_TYPE"));
		model.addAttribute("SectionStatus",commonService.getEnumValues("STATUS"));
		model.addAttribute("partnerList", partnermanagementService.getPartners());
		model.addAttribute("sectionItems", new SectionItemsBean());
		
		commonUtil.getDefaultCategoryTree(request, "", "");

		if (updateMessage != null) {
			if (updateMessage.equalsIgnoreCase(AfConstants.SUCCESS)) {
				model.addAttribute(AfConstants.SUCCESS_MESSAGE,"Section Updated Successfully");
			}
			else if (updateMessage.equalsIgnoreCase(AfConstants.FAILURE))
				model.addAttribute(AfConstants.ERROR_MESSAGE,"Section Not Updated,Error Occured");
		}
		
		return "pageSections";
	}
	
	@RequestMapping(value = "/homePageSections", method = RequestMethod.POST)
	public String homePageSectionsListing(@ModelAttribute("sectionItems") SectionItemsBean sectionItemsBean, ModelMap model, HttpServletRequest request, RedirectAttributes redirectAttrs) {
		String status = "failure";
		String sectionId = request.getParameter("sectionId");
		SectionBean sectionBean=new SectionBean();
		
		try{
//			status = sectionManagementService.createSectionItems(sectionItemsBean,request);
			/*try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}*/
			logger.info("homePageSectionsListing create status: " + status);
			
			sectionId = (null != sectionId) ? sectionId.trim() : "";
			if(sectionId.isEmpty())
				sectionId = "0";
			sectionBean.setSectionId(Integer.parseInt(sectionId));
			 
			List<SectionBean> sectionList =  sectionManagementService.getSections("All", 1);
			commonUtil.getDefaultCategoryTree(request, "", "");
			model.addAttribute("sectionList", sectionList);
			model.addAttribute("itemType", commonService.getEnumValues("ITEM_TYPE"));
			model.addAttribute("sectionItemStatus", commonService.getEnumValues("STATUS"));
			model.addAttribute("SectionStatus",commonService.getEnumValues("STATUS"));
			model.addAttribute("partnerList", partnermanagementService.getAllPartners());
			model.addAttribute("sectionItems", new SectionItemsBean());
			sectionBean.setSectionId(Integer.parseInt(sectionId));
			model.addAttribute("sectionBean",sectionBean );
                        
                        System.out.println("-------section id"+sectionId);
			List<SectionItemsBean> sectionItemList =  sectionManagementService.getSectionItems(sectionId);
			model.addAttribute("sectionItemList", sectionItemList);
			
			if (sectionItemList == null || sectionItemList.isEmpty()) {
				model.addAttribute("sectionItemMessage", "No Section Items Found");
			}
			
			if (sectionList == null || sectionList.isEmpty()) {
				model.addAttribute("SectionMessage", "No Section Found");
			}
			model.addAttribute("sectionItemTab", true);
		}
		catch(Exception e){
			logger.error("@@@@ Exception in SectionManagementController at createSectionItems() :"+e);
		}
		return "pageSections";
	}
	
	@RequestMapping(value = "/createSectionItems", method = RequestMethod.GET)
	public String getcreateSectionItems(@ModelAttribute("sectionItems") SectionItemsBean sectionItemsBean, ModelMap model, HttpServletRequest request, RedirectAttributes redirectAttrs) {
		String status = "failure";
		String sectionId = request.getParameter("sectionId");
		SectionBean sectionBean=new SectionBean();
		
		try{
			status = sectionManagementService.createSectionItems(sectionItemsBean,request);
			/*try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}*/
			logger.info("homePageSectionsListing create status: " + status);
			
			sectionId = (null != sectionId) ? sectionId.trim() : "";
			if(sectionId.isEmpty())
				sectionId = "0";
			sectionBean.setSectionId(Integer.parseInt(sectionId));
			 
			List<SectionBean> sectionList =  sectionManagementService.getSections("All", 1);
			commonUtil.getDefaultCategoryTree(request, "", "");
			model.addAttribute("sectionList", sectionList);
			model.addAttribute("itemType", commonService.getEnumValues("ITEM_TYPE"));
			model.addAttribute("sectionItemStatus", commonService.getEnumValues("STATUS"));
			model.addAttribute("SectionStatus",commonService.getEnumValues("STATUS"));
			model.addAttribute("partnerList", partnermanagementService.getAllPartners());
			model.addAttribute("sectionItems", new SectionItemsBean());
			sectionBean.setSectionId(Integer.parseInt(sectionId));
			model.addAttribute("sectionBean",sectionBean );
			List<SectionItemsBean> sectionItemList =  sectionManagementService.getSectionItems(sectionId);
			model.addAttribute("sectionItemList", sectionItemList);
			
			if (sectionItemList == null || sectionItemList.isEmpty()) {
				model.addAttribute("sectionItemMessage", "No Section Items Found");
			}
			
			if (sectionList == null || sectionList.isEmpty()) {
				model.addAttribute("SectionMessage", "No Section Found");
			}
			model.addAttribute("sectionItemTab", true);
		}
		catch(Exception e){
			logger.error("@@@@ Exception in SectionManagementController at createSectionItems() :"+e);
		}
		return "pageSections";
	}
	
	@RequestMapping(value = "/createSectionItems", method = RequestMethod.POST)
	public String createSectionItems(@ModelAttribute("sectionItems") SectionItemsBean sectionItemsBean, ModelMap model, HttpServletRequest request, RedirectAttributes redirectAttrs) {
		String status = "failure";
		
		try{
			status = sectionManagementService.createSectionItems(sectionItemsBean,request);
			/*try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}*/
			if(status.equalsIgnoreCase("success")){
				model.addAttribute(AfConstants.SUCCESS_MESSAGE,"Section Items Created Successfully");
			}
			else if(status.equalsIgnoreCase("failure")){
				model.addAttribute(AfConstants.ERROR_MESSAGE,"Section Items Not Created, Error Occured");
			}
			
			List<SectionBean> sectionList =  sectionManagementService.getSections("All", 1);
			commonUtil.getDefaultCategoryTree(request, "", "");
			model.addAttribute("sectionList", sectionList);
			model.addAttribute("itemType", commonService.getEnumValues("ITEM_TYPE"));
			model.addAttribute("sectionItemStatus", commonService.getEnumValues("STATUS"));
			model.addAttribute("partnerList", partnermanagementService.getPartners());
			model.addAttribute("sectionItems", new SectionItemsBean());
			model.addAttribute("sectionBean", new SectionBean());
		}
		catch(Exception e){
			logger.error("@@@@ Exception in SectionManagementController at createSectionItems() :"+ e);
		}
		return "pageSections";
	}
	
	@RequestMapping(value = "/updateSection", method = RequestMethod.POST)
	@ResponseBody
	public String updateSection(@RequestParam("json") String json,HttpServletRequest  request) {
		String status = "failure";
		try {

			JSONObject sectionJson = new JSONObject(json);
			status = sectionManagementService.updateHomePageSection(sectionJson,request);
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}
		} catch (Exception e) {
			logger.error("@@@@ Exception in SectionManagementController at updateSection() :"+ e);
		}

		return status;
	}
	
	@RequestMapping("/checkSectionTitle")
	@ResponseBody
	public String checkSectionTitle(@RequestParam("sectionTitle") String secTitle) {
		Boolean secTitleExist = false;
		secTitle = (null != secTitle) ? secTitle.trim() : "";
		if (!secTitle.isEmpty()) {
			secTitleExist = sectionManagementService.checksecTitle(secTitle);
		}
		if (secTitleExist)
			return "exist";
		else
			return "";
	}
	
	@RequestMapping("/checkDispOrd")
	public @ResponseBody String checkLogoPrtDisplayOrder(
			HttpServletRequest request) {
		boolean checkFlag = false;
		String displayOrder = request.getParameter("displayOrder");
		if (displayOrder.length() > 0) {
			checkFlag = sectionManagementService.checkDisplayOrder(displayOrder);

			if (checkFlag)
				return "exist";
			else
				return "";
		} else
			return "";
	}
	
	@RequestMapping("/checkSectionItemDisplayOrder")
	public @ResponseBody String checkSectionItemDisplayOrder(HttpServletRequest request) {
		boolean checkFlag = false;
		String displayOrder = request.getParameter("displayOrder");
		String sectionId = request.getParameter("sectionId");
		
		if (displayOrder.length() > 0) {
			checkFlag = sectionManagementService.checkSectionItemDisplayOrder(displayOrder,sectionId);
			
			if (checkFlag)
				return "exist";
			else
				return "";
		} else
			return "";
	}
	
	@RequestMapping("/checkDuplicateRecord")
	public @ResponseBody String checkDuplicateRecord(HttpServletRequest request) {
		boolean checkFlag = false;
		String sectionId = request.getParameter("sectionId");
		String itemType = request.getParameter("itemType");
		String prtNo = request.getParameter("prtNo");
		String catNo = request.getParameter("catNo");
		String prtPrdctNo = request.getParameter("prtPrdctNo");
		String prtOfferNo = request.getParameter("prtOfferNo");
		String brandName =request.getParameter("brandName");
		
		logger.info("sectionId: " + sectionId);
		logger.info("itemType: " + itemType);
		logger.info("catNo: " + catNo);
		logger.info("prtPrdctNo: " + prtPrdctNo);
		logger.info("prtOfferNo: " + prtOfferNo);
		logger.info("brandName: " + brandName);
		
		checkFlag = sectionManagementService.checkDuplicateRecord(sectionId, itemType, prtNo, catNo, prtPrdctNo, prtOfferNo,brandName);
			
		if (checkFlag){
			logger.info("exist");
			return "exist";
		}
		else{
			logger.info(" notexist");
			return "";
		}
	}

	@RequestMapping(value = "/updateSectionItems", method = RequestMethod.POST)
	@ResponseBody
	public String updateSectionItems(@RequestParam("json") String json,HttpServletRequest request) {

		String status = "failure";
		try {
			JSONObject userJson = new JSONObject(json);
			status = sectionManagementService.updateSectionItems(userJson,request);
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}
		} catch (Exception e) {
			logger.info("@@@@ Exception in SectionController at updateSectionItems() :"+ e);
		}
		return status;
	}
	
	@RequestMapping(value = "/deleteItem", method = RequestMethod.GET)
	public @ResponseBody String deleteItem(String itemId,HttpServletRequest request) {
		String status = "failure";
		
		try {
			status = sectionManagementService.deleteItem(itemId,request);
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}
		}
		catch (Exception e) {
			logger.error("@@@@ Exception in SectionController at updateSectionItems() :", e);
		}
		
		return status;
	}
	
	@RequestMapping(value = "/homePageSectionOrder", method = RequestMethod.GET)
	public String getSectionOrder(HomePageSectionBean homePageSectionBean, ModelMap model, HttpServletRequest request, RedirectAttributes redirectAttrs) {
		String updateMessage = request.getParameter("updateMessage");
		List<HomePageSectionBean> homeSectionList = null;
		 
			homeSectionList =  sectionManagementService.getHomePageSections();
			model.addAttribute("homeSectionList", homeSectionList);
			model.addAttribute("SectionStatus",commonService.getEnumValues("STATUS"));

		
		if (homeSectionList == null || homeSectionList.isEmpty()) {
			model.addAttribute("SectionMessage", "No Section Found");
		}

		if (updateMessage != null) {
			if (updateMessage.equalsIgnoreCase(AfConstants.SUCCESS)) {
				model.addAttribute(AfConstants.SUCCESS_MESSAGE,"Section Updated Successfully");
			}
			else if (updateMessage.equalsIgnoreCase(AfConstants.FAILURE))
				model.addAttribute(AfConstants.ERROR_MESSAGE,"Section Not Updated,Error Occured");
		}
		
		return "pageSectionOrder";
	}
	
	@RequestMapping(value = "/homeSectionDisplaySaveOrder", method = RequestMethod.POST)
	@ResponseBody
	public String homeSectionDisplaySaveOrder(@RequestParam("reorder") String reorder,HttpServletRequest request) {

		String status = "failure";
		try {
			logger.error("reorder" + reorder);
			status = sectionManagementService.homeSectionSaveOrder(reorder,request);
		} catch (Exception e) {
			logger.error("@@@@ Exception in SectionController at homeSectionDisplaySaveOrder() :", e);
		}
		return status;
	}
	
	@RequestMapping(value = "/getBrandName", method = RequestMethod.GET)
	public @ResponseBody Map<Integer,String> getBrandNameList(HttpServletRequest request) {
		
		Map<Integer,String> ppList = new HashMap<>();
		String brName = request.getParameter("brName");
		try {
			ppList = commonService.getBrandList(brName);
			logger.info("getBrandNameList  "+ppList.size());
		} catch (Exception e) {
			logger.error("@@@@ Exception in SectionController at getBrandNameList() :", e);

		}
		return ppList;
	}
	
	@RequestMapping(value = "/updateHomePageSections", method = RequestMethod.POST)
	@ResponseBody
	public String updateHomePageSection(@RequestParam("json") String json,HttpServletRequest request) {
		String status = "failure";
		try {
			JSONObject userJson = new JSONObject(json);
			status = sectionManagementService.updateHomePageSections(userJson,request);
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("@@@@ Exception in SectionController at updateHomePageSection() :"+ e);
		}
		return status;
	}
	
}
