package com.af.admin.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.af.admin.beans.PartnerBean;
import com.af.admin.beans.PartnerCategoryBean;
import com.af.admin.beans.RecommendedProductBean;
import com.af.admin.service.CommonService;
import com.af.admin.service.PartnerManagementService;
import com.af.admin.service.RecommendedProductService;
import com.af.admin.utils.CommonUtil;
import com.af.admin.utils.GsonUtil;

@Controller
public class RecommendedProductController {
	private Logger logger = LogManager.getLogger(RecommendedProductController.class.getName());
	
	@Autowired
	RecommendedProductService rpService;
	
	@Autowired
	PartnerManagementService partnermanagementService;
	
	@Autowired
	CommonService commonService; 
	
	@Autowired
	CommonUtil commonUtil;
	
	@RequestMapping(value="/recommendedProducts",method=RequestMethod.GET)
	public String recommendedProductsPage(ModelMap model,HttpServletRequest request){
		String deleteMsg=request.getParameter("deleteMsg");
		String prtNo=request.getParameter("prtNo");
		RecommendedProductBean rpBean= new RecommendedProductBean();
		model.addAttribute("rpList", rpService.getRecommendedProducts());
		model.addAttribute("recommendedProductBean_",rpBean);
		model.addAttribute("recommendedProductBean",new RecommendedProductBean());
		model.addAttribute("rpStatus", commonService.getEnumValues("STATUS"));		
		
		model.addAttribute("partnerList", partnermanagementService.getPartners());
		if(deleteMsg!=null)
		{
			if(deleteMsg.equalsIgnoreCase("success"))
				model.addAttribute("message", "Recommended Product Deleted Successfully");
		
			else if(deleteMsg.equalsIgnoreCase("failure"))
				model.addAttribute("errormessage", "Recommended Product Could not be Deleted! Error Occurred");
		}
		return "recommendedProducts";
	}
	
	@RequestMapping(value="/getPartnerCategories",method=RequestMethod.GET)
	public @ResponseBody String getPartnerCategories(@RequestParam("prtNo") String prtNo,ModelMap model){
		prtNo=(null!=prtNo)?prtNo.trim():"";
		List<PartnerCategoryBean> list=new ArrayList<PartnerCategoryBean>();		
		if(prtNo.length()>0){			
			list=commonService.getPartnerCategoryListBean(prtNo);
		}
		
		return GsonUtil.toJson(list) ;
	}
	
	@RequestMapping(value="/getPartnerProduct",method=RequestMethod.GET)
	public @ResponseBody Map<String, String> getPartnerProductList(HttpServletRequest request){
		Map<String, String> ppList=null;		
		String prtCatNo=request.getParameter("prtCatNo");
		String keyword=request.getParameter("keyword");
		logger.info("/getPartnerProduct GET prtCatNo:"+prtCatNo);
		logger.info("/getPartnerProduct GET keyword:"+keyword);
		ppList=rpService.getProductList(prtCatNo, keyword);	
		logger.info("/getPartnerProduct GET ppList-size:"+ppList.size());
		return ppList;
	}
	
	@RequestMapping(value="/recommendedProducts",method=RequestMethod.POST)
	public String createRecommendedProducts(@ModelAttribute("recommendedProductBean") RecommendedProductBean recommendedProductBean,ModelMap model,RedirectAttributes redirectAttrs){
		String status;
		try{
		status=rpService.createRecommendedProduct(recommendedProductBean);	
		/*try {
			commonUtil.akamaiPurge();
		} catch (IOException e) {
			logger.error("Akamai Exception ",e);
		}*/
		if(status.equalsIgnoreCase("success"))
			redirectAttrs.addFlashAttribute("message", "Recommended Product Created Successfully");
		else
			redirectAttrs.addFlashAttribute("errormessage", "Recommended Product Could not be Created ");
		
		}
		catch(Exception e){
			logger.debug("@@@@ Exception in RecommendedProductController at createRecommendedProducts() :", e);
			
		}
		return "redirect:/admin/recommendedProducts";
	}
	
	@RequestMapping(value="/recommendedProductsFilter",method=RequestMethod.GET)
	public String recommendedProductsFilter(@ModelAttribute("recommendedProductBean_") RecommendedProductBean recommendedProductBean,ModelMap model){
		return "redirect:/admin/recommendedProducts";
	}
	
	@RequestMapping("/checkRecommendedProduct")
	public @ResponseBody String checkRecommendedProduct(HttpServletRequest request){
		Map<String, String> rpmap = new HashMap<String, String>();
		boolean checkFlag=false;
		String prtPrdNo=request.getParameter("prtPrdNo");		
		logger.debug("/checkRecommendedProduct request-val: "+prtPrdNo);
		if( prtPrdNo.length()>0){
			rpmap.put("prtPrdNo", prtPrdNo);
			checkFlag=rpService.checkRecommendedProduct(rpmap);
			if(checkFlag) return "exist"; else return "";
		}else
			return "";		
	}
	
	@RequestMapping("/checkRecommendedProductPosition")
	public @ResponseBody String checkRecommendedProductPosition(HttpServletRequest request){
		boolean checkFlag=false;
		String prtPrdPos=request.getParameter("prtPrdPos");
		logger.debug("/checkRecommendedProductPosition request-val:" + prtPrdPos);
		if(prtPrdPos.length()>0){
			checkFlag=rpService.checkRecommendedProductPosition(prtPrdPos);
			if(checkFlag) return "exist"; else return "";
		}else
			return "";	
	}
	
	@RequestMapping(value="/updateRecommendedProduct")
	public String updateRecommendedProductGet(ModelMap model, HttpServletRequest request){
		RecommendedProductBean recommendedProductBean = null;
		String rpNo=request.getParameter("rpNo");			
		model.addAttribute("rpStatus", commonService.getEnumValues("STATUS"));				
		model.addAttribute("partnerList", partnermanagementService.getPartners());		
		recommendedProductBean=rpService.getRecommendedProductByNo(rpNo);
		model.addAttribute("partnerCatList", recommendedProductBean.getPartnerCategoryBean());
        model.addAttribute("recommendedProductBean", recommendedProductBean);
	
        return "updateRecommendedProduct";
	}
	
	
	@RequestMapping(value="/updateRecommendedProduct", method=RequestMethod.POST)
	public String updateRecommendedProductPost(@ModelAttribute RecommendedProductBean recommendedProductBean,RedirectAttributes redirectAttrs,ModelMap model){
		String status;
		try {
		status=rpService.updateRecommendedProduct(recommendedProductBean);
		try {
			commonUtil.akamaiPurge();
		} catch (IOException e) {
			logger.error("Akamai Exception ",e);
		}
		if(status.equalsIgnoreCase("success")){
			model.addAttribute("message", "Recommended Product Updated Successfully");
		}
		else{
			model.addAttribute("message", "Recommended Product Could not be Updated");
		}
		model.addAttribute("rpList", rpService.getRecommendedProducts());
		model.addAttribute("recommendedProductBean_",recommendedProductBean);
		model.addAttribute("recommendedProductBean",new RecommendedProductBean());
		model.addAttribute("rpStatus", commonService.getEnumValues("STATUS"));		
		
		model.addAttribute("partnerList", partnermanagementService.getPartners());
	
		} catch (Exception e) {
		logger.debug("@@@@ Exception in RecommendedProductController at updateRecommendedProductPost() :", e);
	}
			
		return "recommendedProducts";
	}
	
	@RequestMapping(value="/deleteRecommendedProduct")
	public @ResponseBody String deleteRecommendedProduct(@RequestParam("rpNo") String rpNo){
		String deleteStatus="";
		try{			
			deleteStatus=rpService.deleteRecommendedProduct(new Integer(rpNo));
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}
			logger.debug(deleteStatus);
		}catch(Exception e){
			logger.error("@@@@ Exception in RecommendedProductController at deleteRecommendedProduct() :", e);
		}
		return deleteStatus;
		
	}
	
	

}
