package com.af.admin.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import java.net.URLDecoder;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.af.admin.beans.AfAmzonoPartnerCategoryBean;
import com.af.admin.beans.CategoryBean;
import com.af.admin.beans.CommissionBean;
import com.af.admin.beans.KeyWordBean;
import com.af.admin.beans.PartnerBean;
import com.af.admin.beans.PartnerCategoryBean;
import com.af.admin.beans.PartnerMappingBean;
import com.af.admin.constants.AfConstants;
import com.af.admin.service.CategoryManagementService;
import com.af.admin.service.CommissionManagementService;
import com.af.admin.service.CommonService;
import com.af.admin.service.OffersDealsService;
import com.af.admin.service.PartnerManagementService;
import com.af.admin.utils.CommonUtil;
import com.af.admin.utils.GsonUtil;
import com.af.admin.utils.ServiceProviderUtil;

/**
 *  PartnerManagementController.java
 *  Functionalities Used
 *  partnerPage() method using for getting list of partners.
 */

/**
 * File Name: PartnerManagementController.java
 * 
 * @Description:This class is for Partner Management
 * @author jppl Created: August 10, 2016 Modification History: $Log$:
 */
@Controller
public class PartnerManagementController {
	private Logger logger = LogManager.getLogger(PartnerManagementController.class.getName());

	@Autowired
	PartnerManagementService partnerManagementService;

	@Autowired
	CommissionManagementService commissionManagementService;
	
	@Autowired
	CommonService commonService;
	
	@Autowired
	CategoryManagementService categoryManagementService;
	
	@Autowired
	CommonUtil commonUtil;
	
	@Autowired
	OffersDealsService offersDealsService;
	
	private String existStr = "exist";
	
	/**
	 * @param request
	 * @param model
	 * @param redirectAttrs
	 * @return String :for displaying partnerManagement Page
	 * @Description:This method returning Default partner Management page
	 */
	@RequestMapping(value = "/partner", method = RequestMethod.GET)
	public String partnerPage(PartnerBean partnerBean, ModelMap model,
			HttpServletRequest request, RedirectAttributes redirectAttrs) {
		
//		System.out.println("partner get");
		
		String updateMessage = request.getParameter("updateMessage");
		String catUpdateMessage = request.getParameter("catUpdateMessage");
		String patUpdateMessage = request.getParameter("patUpdateMessage");
		String patCreateMessage = request.getParameter("patCreateMessage");
		String deletePrtMsg = request.getParameter("deletePrtMsg");
		String prtdeletePrtMsg = request.getParameter("prtdeletePrtMsg");
		String deleteFileMsg = request.getParameter("deleteFileMsg");
		String logoprtNo = request.getParameter("logoprtNo");
		String partnerType = request.getParameter("partnerType");
		String prttype = request.getParameter("prttype");
		partnerType = (null != partnerType) ? partnerType.trim() : "";
		//		+ partnerType+">>prttype>>"+prttype);

                
//                System.out.println("partner get------->");    
		partnerBean.setPartnerType(partnerType);
		model.addAttribute("partnerBean", partnerBean);
		if (partnerType.equalsIgnoreCase("logo"))
			model.addAttribute("activateLogoPartners", true);
		else if (partnerType.equalsIgnoreCase("host")
				|| partnerType.equalsIgnoreCase("default") 
				|| partnerType.equalsIgnoreCase("read") || partnerType.equalsIgnoreCase("talk"))
			model.addAttribute("activateLogoPartners", false);
		else if (partnerType.equalsIgnoreCase("offline"))
			model.addAttribute("activateOffLinePartners", true);
		
		if (partnerType == null || partnerType.isEmpty()){
			model.addAttribute("partnerList",
					partnerManagementService.getPartnerListPagination(10,0,"All"));
		}
		else {
			List<PartnerBean> partnerList = partnerManagementService.getPartnerListPagination(10,0,partnerType);
			model.addAttribute("partnerList", partnerList);

			if (partnerList == null || partnerList.size() <= 0) {
				model.addAttribute("partnerMessage", "No Partners Found");
			}
		}

		model.addAttribute("partnerStatus",
				commonService.getEnumValues("STATUS"));
		if (updateMessage != null) {
			if (updateMessage.equalsIgnoreCase(AfConstants.SUCCESS)) {
				
				model.addAttribute(AfConstants.SUCCESS_MESSAGE,
						"Partner Updated Successfully");

			}

			else if (updateMessage.equalsIgnoreCase(AfConstants.FAILURE))
				model.addAttribute(AfConstants.ERROR_MESSAGE,
						"Partner Not Updated,Error Occured");
		}
		
		if (catUpdateMessage != null) {
			model.addAttribute("CategoryTextTab", true);
			if (catUpdateMessage.equalsIgnoreCase(AfConstants.SUCCESS)) {
				
				model.addAttribute(AfConstants.SUCCESS_MESSAGE,
						"Partner Updated Successfully");

			}

			else if (catUpdateMessage.equalsIgnoreCase(AfConstants.FAILURE))
				model.addAttribute(AfConstants.ERROR_MESSAGE,
						"Partner Not Updated,Error Occured");
		}
		
		if (patCreateMessage != null) {
			model.addAttribute("CategoryTextTab1", true);
			if (patCreateMessage.equalsIgnoreCase(AfConstants.SUCCESS)) {
				model.addAttribute(AfConstants.SUCCESS_MESSAGE,
						"Partner Created Successfully");

			}
			else if (patCreateMessage.equalsIgnoreCase(AfConstants.FAILURE))
				model.addAttribute(AfConstants.ERROR_MESSAGE,
						"Partner Not Created,Error Occured");
		}
		
		if (patUpdateMessage != null) {
			model.addAttribute("CategoryTextTab1", true);
			if (patUpdateMessage.equalsIgnoreCase(AfConstants.SUCCESS)) {
				model.addAttribute(AfConstants.SUCCESS_MESSAGE,
						"Partner Updated Successfully");

			}
			else if (patUpdateMessage.equalsIgnoreCase(AfConstants.FAILURE))
				model.addAttribute(AfConstants.ERROR_MESSAGE,
						"Partner Not Updated,Error Occured");
		}
		if (prtdeletePrtMsg != null) {
			model.addAttribute("CategoryTextTab1", true);
			if (prtdeletePrtMsg.equalsIgnoreCase(AfConstants.SUCCESS))
				model.addAttribute(AfConstants.SUCCESS_MESSAGE, "Partner deleted Successfully");

			else if (prtdeletePrtMsg.equalsIgnoreCase(AfConstants.FAILURE))
				model.addAttribute(AfConstants.ERROR_MESSAGE,
						"Partner Could not be Deleted");
		}
		
		
		
		if (deletePrtMsg != null) {
			if (deletePrtMsg.equalsIgnoreCase(AfConstants.SUCCESS))
				model.addAttribute("message", "Partner deleted Successfully");

			else if (deletePrtMsg.equalsIgnoreCase(AfConstants.FAILURE))
				model.addAttribute("errormessage",
						"Partner Could not be Deleted");
		}
		if (deleteFileMsg != null) {
			if (deleteFileMsg.equalsIgnoreCase(AfConstants.SUCCESS)) {
				model.addAttribute("message", "File deleted Successfully");
				model.addAttribute("mappingKeyWordTab", true);
				if (logoprtNo != null)
					model.addAttribute("logoprtNo", logoprtNo);
					model.addAttribute("prttype", prttype);
			} else if (deleteFileMsg.equalsIgnoreCase(AfConstants.FAILURE)) {
				model.addAttribute("errormessage", "File Could not be Deleted");
				model.addAttribute("mappingKeyWordTab", true);
				if (logoprtNo != null)
					model.addAttribute("logoprtNo", logoprtNo);
					model.addAttribute("prttype", prttype);

			}
		}
		model.addAttribute("createPartnerMappingForm",new PartnerMappingBean());
	    model.addAttribute("afPartnerMappingList", partnerManagementService.getPartnerMappingDetails());
		request.setAttribute("configTileNo",partnerManagementService.getLgoPrtTileSearchresult());
		model.addAttribute("logoPartnerList",partnerManagementService.getLogoPartners());	
		model.addAttribute("offersLinkList", offersDealsService.getStoreList());
		
		return "partnerManagement";
	}

	/**
	 * @param partnerBean
	 * @param model
	 * @param redirectAttrs
	 * @return String :for displaying partnerManagement Page
	 * @Description:This method Adds Partnervz
	 */
	@RequestMapping(value = "/partner", method = RequestMethod.POST)
	public String addPartner(
			@ModelAttribute("partnerBean") PartnerBean partnerBean,
			ModelMap model, RedirectAttributes redirectAttrs,HttpServletRequest request) {
		String status;
		try {
//			System.out.println("Enter in /partner Post");
			status = partnerManagementService.createPartner(partnerBean,request);
//			System.out.println("status-->"+status);
		      try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}
			model.addAttribute("partnerStatus",
					commonService.getEnumValues("STATUS"));
			if (status.equalsIgnoreCase(AfConstants.SUCCESS))
				redirectAttrs.addFlashAttribute("message",
						"Partner Created Successfully");
			else
				redirectAttrs.addFlashAttribute("errormessage",
						"Partner Could not be Created ");

		} catch (Exception e) {
			logger.error("@@@@ Exception in PartnerManagementController at addPartner() :", e);
		}
		return "redirect:/admin/partner";
	}

	@RequestMapping("/checkLogoPrtDispOrd")
	public @ResponseBody String checkLogoPrtDisplayOrder(HttpServletRequest request) {
		boolean checkFlag = false;
		String prtPos = request.getParameter("prtPos");
		String prtType = request.getParameter("prtType");
		logger.debug("/checkLogoPrtDisplayOrder request-val:" + prtPos +">>prtType>>"+prtType);
		if (prtPos.length() > 0) {
			checkFlag = partnerManagementService.checkLogoPrtDisplayOrder(prtPos, prtType);

			if (checkFlag)
				return "exist";
			else
				return "";
		} else
			return "";
	}

	@RequestMapping("/checkLogoPrtTilePos")
	public @ResponseBody String checkLogoPrtTilePosition(
			HttpServletRequest request) {
		boolean checkFlag = false;
		String prtPos = request.getParameter("prtPos");
		String prtType = request.getParameter("prtType");
		logger.debug("/checkLogoPrtTilePosition request-val:" + prtPos +">>prtType>>"+prtType);
		if (prtPos.length() > 0) {
			checkFlag = partnerManagementService.checkLogoPrtTilePosition(prtPos, prtType);

			if (checkFlag)
				return "exist";
			else
				return "";
		} else
			return "";
	}

	/**
	 * @param json
	 * @return String :returns status to ajax call
	 * @Description:This method Updates Partner
	 */
	@RequestMapping(value = "/updatePartner", method = RequestMethod.POST)
	@ResponseBody
	public String updatePartner(@RequestParam("json") String json, HttpServletRequest request) {
		String status = "failure";
		try {
			JSONObject userJson = new JSONObject(json);
			  logger.info("partner updated Data Json format "+userJson);
			  
			  
//			System.out.println("inside upadate Partner");
			status = partnerManagementService.updatePartner(userJson, request);
//			try {
//				commonUtil.akamaiPurge();
//			} catch (IOException e) {
//				logger.error("Akamai Exception ",e);
//			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("@@@@ Exception in PartnerManagementController at updatePartner() :", e);
		}

		return status;
	}

	
	@RequestMapping(value = "/updatePartnerCategoryMap", method = RequestMethod.POST)
	@ResponseBody
	public String updatePartnerCategoryMap(@RequestParam("json") String json, HttpServletRequest request) {
		String status = "failure";
		try {
			JSONObject userMapJson = new JSONObject(json);
			status = partnerManagementService.updatePartnerCategoryMap(userMapJson, request);
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}
			
		} catch (JSONException e) {
			logger.error("@@@@ Exception in PartnerManagementController at updatePartnerCategoryMap() :", e);
		}
	return status;
			
	}
	
	@RequestMapping(value = "/updateLogoPartner", method = RequestMethod.POST)
	@ResponseBody
	public String updateLogoPartner(@RequestParam("json") String json, HttpServletRequest request) {
		String status = "failure";
		try {
			JSONObject userJson = new JSONObject(json);
//			System.out.println("inside update logo partner ");
			status = partnerManagementService.updateLogoPartner(userJson, request);
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}

		} catch (Exception e) {
			logger.error("@@@@ Exception in PartnerManagementController at updateLogoPartner() :", e);
		}

		return status;
	}
	/**
	 * @param file
	 * @param type
	 * @param request
	 * @return String :responseString i.e filename to ajax call
	 * @Description:This method Updates Partner
	 */

	@RequestMapping(value = "/updatepartnerLogo", method = RequestMethod.POST)
	@ResponseBody
	public String saveImagePage(@RequestParam("file") MultipartFile file,
			@RequestParam("type") String type, HttpServletRequest request) {
		String responseString = "";
		try {
//			System.out.println("inside update partner logo post");
			responseString = commonService.saveImage(file, type);
			
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}
		} catch (Exception e) {
			logger.error("@@@@ Exception in PartnerManagementController at saveImagePage() :", e);
		}
		logger.debug("/saveImages POST responseString:" + responseString);
		return responseString;
	}

	/**
	 * @param prtNo
	 * @return String :deleteStatus to ajax call
	 * @Description:This method Deletes Partner
	 */
	@RequestMapping(value = "/deletePartner")
	@ResponseBody
	public String deletePartner(@RequestParam("prtNo") String prtNo, HttpServletRequest request) {

		String deleteStatus = "";
		
		try {
			logger.debug("deletePartner partner No" + prtNo);
			deleteStatus = partnerManagementService.deletePartner(new Integer(prtNo), request);
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}
			logger.debug(deleteStatus);
		} catch (Exception e) {
			logger.error("@@@@ Exception in PartnerManagementController at deletePartner() :", e);
		}
		return deleteStatus;

	}

	/**
	 * @param partnerid
	 * @return String :status to ajax call
	 * @Description:This method Checks Partner Id Exists or Not
	 */
	@RequestMapping("/checkPartnerID")
	@ResponseBody
	public String checkPartnerID(@RequestParam("partnerId") String partnerid) {
		Boolean partnerIDExist = false;
		String partnerId = partnerid;
		partnerId = (null != partnerId) ? partnerId.trim() : "";
		if (partnerId.length() > 0) {
			partnerIDExist = partnerManagementService.checkPartnerID(partnerId);
		}
		if (partnerIDExist)
			return "exist";
		else
			return "";
	}

	/**
	 * @param partnername
	 * @return String :status to ajax call
	 * @Description:This method Deletes Checks Partner Name Exists or Not
	 */
	@RequestMapping("/checkPartnerName")
	@ResponseBody
	public String checkPartnerName(
			@RequestParam("partnerName") String partnername) {
		Boolean partnerNameExist = false;
		String partnerName = partnername;
		partnerName=URLDecoder.decode(partnerName);		
		partnerName = (null != partnerName) ? partnerName.trim() : "";
		if (partnerName.length() > 0) {
			partnerNameExist = partnerManagementService
					.checkPartnerName(partnerName);
		}
		if (partnerNameExist)
			return "exist";
		else
			return "";
	}

	/**
	 * @param reorder
	 * @return String :status to ajax call
	 * @Description:This method Deletes Checks Saves the Order in DB
	 */
	@RequestMapping(value = "/partnerSaveOrder", method = RequestMethod.POST)
	@ResponseBody
	public String saveOrder(@RequestParam("reorder") String reorder, @RequestParam("prttype") String prtType, @RequestParam("curPage") String curPage, @RequestParam("pageLen") String pageLen, HttpServletRequest request) {

		String status = "failure";
//		System.out.println("pageLen -> "+pageLen+" curPage -> "+curPage);
		try {
			logger.error("reorder" + reorder +">>prttype>>"+prtType);
			status = partnerManagementService.saveOrder(reorder, prtType,curPage, pageLen, request);
			
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}
		} catch (Exception e) {
			logger.error("@@@@ Exception in PartnerManagementController at saveOrder() :", e);
		}
		return status;
	}

	/**
	 * @param model
	 * @return String :Default Partner Management Page
	 * @Description:This method Cancel the adding of partner on click of cancel
	 */
	@RequestMapping(value = "/addPartnerCancel", method = RequestMethod.GET)
	@ResponseBody
	public String addPartnerCancel(ModelMap model) {
		return "/admin/partner";
	}

	@RequestMapping(value = "/getKeywordFileName", method = RequestMethod.POST)
	public String getKeywordFileName(
			@ModelAttribute("partnerBean") PartnerBean partnerBean,
			@RequestParam("logoprtNo") String logoprtNo,
			@RequestParam("prttype") String prttype,
			@ModelAttribute("keywordBean") KeyWordBean keyWordBean,
			ModelMap model, RedirectAttributes redirectAttrs) {
		String status = "";
                
//                System.out.println("entered in getkeywordfile name ");

		try {
			if (logoprtNo != null && logoprtNo.length() > 0) {
				partnerBean.setPrtNo(Integer.parseInt(logoprtNo));
				model.addAttribute("keywordsFile",partnerManagementService.getKeywordFileName(logoprtNo));
			}

			model.addAttribute("logoPartnerList",partnerManagementService.getLogoPartners());
			model.addAttribute("partnerBean", partnerBean);
			model.addAttribute("logoprtNo", logoprtNo);
			model.addAttribute("prttype", prttype);
			model.addAttribute("mappingKeyWordTab", true);
			model.addAttribute("createPartnerMappingForm",new PartnerMappingBean());
		} catch (Exception e) {
			logger.error("@@@@ Exception in PartnerManagementController at getKeywordFileName() :", e);
		}
		return "partnerManagement";
	}

	
	
	
	@RequestMapping(value = "/getKeywordFileName", method = RequestMethod.GET)
	public String getKeywordFileName(
			ModelMap model, RedirectAttributes redirectAttrs) {
		String status = "";

		try {
				
				model.addAttribute("keywordsFile","");

			model.addAttribute("logoPartnerList",partnerManagementService.getLogoPartners());
			model.addAttribute("partnerBean", new PartnerBean());
			model.addAttribute("logoprtNo", 0);
			model.addAttribute("prttype", "");
			model.addAttribute("mappingKeyWordTab", true);
			model.addAttribute("createPartnerMappingForm",new PartnerMappingBean());
		} catch (Exception e) {
			logger.error("@@@@ Exception in PartnerManagementController at getKeywordFileName() :", e);
		}
		return "partnerManagement";
	}
	
	
	
	@RequestMapping(value = "/configureLogoPartner", method = RequestMethod.POST)
	public String addLogoPartnerConfig(@RequestParam("configTileNo") String tileNo, HttpServletRequest request, ModelMap model) {
		String status = "failure";

		try {
			status = partnerManagementService.saveLogoPartnerConfig(tileNo, request);
		} catch (Exception e) {
			logger.error("@@@@ Exception in PartnerManagementController at addLogoPartnerConfig() :", e);
		}

		if (status.equalsIgnoreCase(AfConstants.SUCCESS)) {
			model.addAttribute(AfConstants.SUCCESS_MESSAGE,
					"Configuration added successfully");
		} else if (status.equalsIgnoreCase(AfConstants.FAILURE)) {
			model.addAttribute(AfConstants.ERROR_MESSAGE,
					"Error in updating config");
		}
		model.addAttribute("partnerBean", new PartnerBean());
		model.addAttribute("mappingKeyWordTab", false);
		model.addAttribute("logoPartnerConfigurationTab", true);
		request.setAttribute("configTileNo",partnerManagementService.getLgoPrtTileSearchresult());
		model.addAttribute("logoPartnerList",partnerManagementService.getLogoPartners());
		
		return "partnerManagement";
	}
	
	/**
	 * @param activityid
	 * @return String :status to ajax call
	 * @Description:This method Checks Activity Id Exists or Not
	 */
	@RequestMapping("/checkActivityID")
	@ResponseBody
	public String checkActivityID(@RequestParam("activityId") String activityid) {
		Boolean activityIDExist = false;
		String activityId = activityid;
		activityId = (null != activityId) ? activityId.trim() : "";
		if (activityId.length() > 0) {
			activityIDExist = partnerManagementService.checkActivityID(activityid);
		}
		if (activityIDExist)
			return "exist";
		else
			return "";
	}
	
	@RequestMapping(value="/getOffLinePartner",method=RequestMethod.GET)
	public @ResponseBody String getOffLinePartner(@RequestParam("prtType") String prtType,ModelMap model){
		prtType=(null!=prtType)?prtType.trim():"";
		List<PartnerBean> list=new ArrayList<>();		
		if(prtType.length()>0){		
			list=partnerManagementService.getLogoPartners(prtType);
		}
		
		return GsonUtil.toJson(list) ;
	}
	
	@RequestMapping(value="/getCategoryMappingPartner",method=RequestMethod.GET)
	public @ResponseBody String getCategoryMappingPartner(@RequestParam("prtType") String prtType,ModelMap model){
		prtType=(null!=prtType)?prtType.trim():"";
		String partType = " ";
		List<PartnerBean> list=new ArrayList<>();		
		if(prtType.length()>0){		
		
			if(prtType.equals("logo")){
	        list=partnerManagementService.getLogoPartners();
	        partType = "logo";
			}
			if(prtType.equals("default")){
				list=partnerManagementService.getDefaultPartners();
				partType = "default";
			}
			if(prtType.equals("offline")){
				list=partnerManagementService.getOfflinePartner();
				partType = "offline";
			}
			if(prtType.equals("host")){
				list=partnerManagementService.getHostPartner();
				partType = "host";
			}
			if(prtType.equals("talk")){
				list=partnerManagementService.getTalkPartner();
				partType = "talk";
		    }
			if(prtType.equals("read")){
				list=partnerManagementService.getReadPartner();
				partType = "read";
		    }
			model.addAttribute("partType", partType);
		}
		return GsonUtil.toJson(list) ;
	}
	
	@RequestMapping(value = "/getPartnerByCategory", method = RequestMethod.GET)
	public @ResponseBody String getPartnerByCategory(
			@RequestParam("prtNameCatMapping") String prtNameCatMapping,
			ModelMap model) {
		String status = "";
		List<PartnerCategoryBean> list2 = new ArrayList<>();
		String dd = "";
		try {
			model.addAttribute("partnerByCat",new PartnerCategoryBean());
			 list2 = categoryManagementService.getPartnerCategories(prtNameCatMapping);
			 int count=0;
			 for(PartnerCategoryBean ss : list2){
				 if(count == 0){
				 dd=dd+ss.getCategoryName();
				 }
				 else{
					 dd = dd+", "+ss.getCategoryName();
				 }
				 count++;
			 }
			 
		} catch (Exception e) {
			logger.error("@@@@ Exception in PartnerManagementController at getPartnerByCategory() :", e);
		}
		return dd ;
	}

	@RequestMapping(value = "/getCategoryTextMapped", method = RequestMethod.GET)
	public @ResponseBody String getCategoryTextMapped(
			@RequestParam("prtNameCatMapping") String prtNameCatMapping,
			ModelMap model) {
		
		String textMapCat = "";
		try {
			
			textMapCat  = partnerManagementService.getPartnerMappedCategoriesByPrtNo(prtNameCatMapping);
			 int count=0;
			 
			 
		} catch (Exception e) {
			logger.error("@@@@ Exception in PartnerManagementController at getCategoryTextMapped() :", e);
		}
		return textMapCat ;
	}
	
	
	@RequestMapping(value="/getAllPrtWebservice",method=RequestMethod.GET)
	public @ResponseBody String getAllPrtWebservice(){
		String jsonResponseStart = "{\"response\":{";
		String jsonResponsePrtStart = "\"partners\":";
		String jsonResponsePrtCmmStart = "\"prtCmm\":";
		String jsonResponseEnd = "}}";
		String response = "";
		
		List<Map<String, String>> afPartnerMapList = null;
		List<Map<String, String>> afPartnerCmmMapList = null;
		String prtJson = "";
		String prtCommJson = "";
		
		try{
			afPartnerMapList = partnerManagementService.getAllPrtWebservice();
			afPartnerCmmMapList = commissionManagementService.getPartnerCommissionWebservice();
			
			
			if(!afPartnerMapList.isEmpty()){
				prtJson = CommonUtil.getStringValue(new JSONArray(afPartnerMapList));
				response = jsonResponseStart+jsonResponsePrtStart+prtJson;
			}
			
			if(!afPartnerCmmMapList.isEmpty()){
				prtCommJson= CommonUtil.getStringValue(new JSONArray(afPartnerCmmMapList));
				response += ","+jsonResponsePrtCmmStart+prtCommJson+jsonResponseEnd;
			}else{
				response = response+jsonResponseEnd;
			}
		}
		catch(Exception e){
			logger.error("@@@@ Exception in PartnerManagementController at getAllPrtWebservice() :", e);
		}
		return response;
	}
	
	
	@RequestMapping(value = "/partnerServerPagination", method = RequestMethod.GET)
	public @ResponseBody String getPartnerList(@ModelAttribute("partnerBean") PartnerBean partnerBean, 
			ModelMap model, HttpServletRequest request,@RequestParam("length") int length,@RequestParam("start") int start,@RequestParam("draw") int draw ,@RequestParam("partnerType") String partnerType) {
		String response="";
		String jsonResponseStart = "{\"data\":";
		String jsonResponseEnd = "}";
		String responseJson="";
		String responseJson1="";
		 List<PartnerBean> partnerBeanList = null;
		 Long partnerBeanListTotal = null;
		try{
			partnerBeanList=partnerManagementService.getPartnerListPagination(length,start,partnerType);
			partnerBeanListTotal = partnerManagementService.getPartnerListPaginationCount(partnerType);
			responseJson=jsonResponseStart+GsonUtil.toJson(partnerBeanList)+",\"draw\":"+draw+",\"recordsTotal\":"+partnerBeanListTotal+",\"recordsFiltered\":"+partnerBeanListTotal+jsonResponseEnd;
		
		   JSONObject object=new JSONObject(responseJson);
		   responseJson1=""+object;
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in PartnerManagementController at partnerServerPagination() :", e);
		
		  return "";
		}
//		return jsonResponseStart+GsonUtil.toJson(partnerBeanList)+",\"draw\":"+draw+",\"recordsTotal\":"+partnerBeanListTotal+",\"recordsFiltered\":"+partnerBeanListTotal+jsonResponseEnd;

	   return responseJson1;
	}

	@RequestMapping("/checkAggregateID")
	@ResponseBody
	public String checkAggregateID(@RequestParam("aggregateId") String aggregateid) {
		Boolean aggregateIDExist = false;
		String aggregateId = aggregateid;
		aggregateId = (null != aggregateId) ? aggregateId.trim() : "";
		if (aggregateId.length() > 0) {
			aggregateIDExist = partnerManagementService.checkAggregateID(aggregateid);
		}
		if (aggregateIDExist)
			return "exist";
		else
			return "";
	}
	@RequestMapping(value = "/createPartnerMapping", method = RequestMethod.POST)
	@ResponseBody
	public String createPartnerMapping(@RequestParam("json") String json, HttpServletRequest request, RedirectAttributes redirectAttrs) {
		String status = AfConstants.SUCCESS;
		try {
			JSONObject createPatJson = new JSONObject(json);
			
			status =partnerManagementService.createPartnerMapping(createPatJson, request);
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}
			/*if(status.equalsIgnoreCase(AfConstants.SUCCESS)) {
				redirectAttrs.addFlashAttribute(AfConstants.SUCCESS_MESSAGE, "Partner Created Successfully");
			} 
			else{
				redirectAttrs.addFlashAttribute(AfConstants.ERROR_MESSAGE,"Error: Partner could'nt be created." );
			}*/
		} catch (JSONException e) {
			logger.error("@@@@ Exception in PartnerManagementController at createPartnerMapping() :", e);
		}
	return status;
			
	}
	@RequestMapping(value="/afPartnerMapping",method=RequestMethod.GET)
	public String afPartnerMappingList(ModelMap model,@ModelAttribute("partnerMappingBean") PartnerMappingBean partnerMappingBean,HttpServletRequest request){
		try {
			model.addAttribute("partnerMappingBean",partnerMappingBean);
			model.addAttribute("partnerList", partnerManagementService.getAllPartners());
			model.addAttribute("afPartnerMappingList", partnerManagementService.getPartnerMappingDetails());
			model.addAttribute("offersLinkList", offersDealsService.getStoreList());
			model.addAttribute("CategoryTextTab1", true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	return "partnerManagement";
	}
	
	@RequestMapping(value = "/updatePartnerMapping", method = RequestMethod.POST)
	public @ResponseBody String updatePartnerMapping(@RequestParam("json") String json, ModelMap model,
			RedirectAttributes redirectAttrs, HttpServletRequest request) {
		String status="";
		try {
			JSONObject createPrtJson = new JSONObject(json);

			status = partnerManagementService.updatePartnerMapping(createPrtJson, request);
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}

		} catch (Exception e) {
			logger.error("@@@@ Exception in PartnerManagementController at updatePartnerMapping() :", e);
		}

		return status;
	}
	
	
	@RequestMapping(value="/deletePartnerMapping")
	public @ResponseBody String deletePartnerMapping(@RequestParam("id") Integer id,HttpServletRequest request){
		String deleteStatus="";
		try{
			deleteStatus=partnerManagementService.deletePartnerMapping(id,request);
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in PartnerManagementController at deletePartnerMapping() :", e);
			return deleteStatus;
		}
		return deleteStatus;
	}
	
	@RequestMapping("/checkCreateMappingDuplicate")
	public @ResponseBody String checkCreateMappingDuplicate(@RequestParam("createMappingPartnerName") String createMappingPartnerName,@RequestParam("partNumber") String partNumber){
		Boolean isPartnerNameExist = false;
		String patName = (null != createMappingPartnerName) ? createMappingPartnerName.trim() : "";
		if(patName.length() > 0) {
			isPartnerNameExist = partnerManagementService.MappingPartnerNameExist(patName,partNumber);
		}
		if(isPartnerNameExist)
			return existStr;
		else return "";
	}
	@RequestMapping(value="/addPartnerMappingCancel",method=RequestMethod.GET)
	public @ResponseBody String addPartnerMappingCancel(ModelMap model){
		return "admin/partner";
	}
	
}