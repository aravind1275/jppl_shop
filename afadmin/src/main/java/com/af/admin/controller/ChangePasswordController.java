package com.af.admin.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.af.admin.beans.AdminUserBean;
import com.af.admin.beans.ChangePasswordBean;
import com.af.admin.service.UserManagementService;



@Controller
@RequestMapping("/changePassword")
public class ChangePasswordController {

	@Autowired
	UserManagementService userManagementService;

	@RequestMapping(method=RequestMethod.GET)
	public String changePasswordPage(ModelMap model,HttpServletRequest request) {
		model.addAttribute("changePasswordBean", new ChangePasswordBean());
		model.addAttribute("adminUserBean",request.getSession().getAttribute("activeUser"));
		return "changePassword";
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public String changePasswordSubmitPage(@ModelAttribute("changePasswordBean") ChangePasswordBean changePasswordBean,RedirectAttributes redirectAttrs,ModelMap model,HttpServletRequest request) {
		String message="";
		message=userManagementService.changePassword(changePasswordBean);
		message=(null!=message)?message.trim():"";
		
		if(message.length()>0){
			redirectAttrs.addFlashAttribute("successMessage", message);
			AdminUserBean adminUserBean=(AdminUserBean)request.getSession().getAttribute("activeUser");
			adminUserBean.setPassword(changePasswordBean.getNewPassword());
			request.getSession().setAttribute("activeUser", adminUserBean);
			
		}
		else {
			model.addAttribute("errorMsg", "Change of Password Failed");
			return "changePassword";
		}
		
		return "redirect:/admin/dashboard";
	}
}
