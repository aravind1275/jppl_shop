package com.af.admin.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.af.admin.DAO.CommissionManagementDAO;
import com.af.admin.beans.CommissionBean;
import com.af.admin.beans.CommissionUploadBean;
import com.af.admin.beans.PartnerCategoryBean;
import com.af.admin.beans.offersBean;
import com.af.admin.constants.AfConstants;
import com.af.admin.service.CategoryManagementService;
import com.af.admin.service.CommissionManagementService;
import com.af.admin.service.CommonService;
import com.af.admin.service.PartnerManagementService;
import com.af.admin.service.RecommendedProductService;
import com.af.admin.utils.CommonUtil;
import com.af.admin.utils.GsonUtil;
import com.af.admin.utils.ServiceProviderUtil;

@Controller
public class CommissionManagementController {
	private static final int BUFFER_SIZE = 4096;
	private Logger logger = LogManager.getLogger(CommissionManagementController.class.getName());

	@Autowired
	CommissionManagementService commissionmanagementservice;

	@Autowired
	CategoryManagementService categoryManagementService;

	@Autowired
	PartnerManagementService partnermanagementService;

	@Autowired
	RecommendedProductService rpService;

	@Autowired
	CommonService commonService;
	
	@Autowired
	CommonUtil commonUtil;
	
	@Autowired
	CommissionManagementDAO commisionManagementDao;
	
	@Value("${application.af.report_download}")
	private String reportFileLocation;

	private String statusStr = "STATUS";

	private String partnercategoryBeanStr = "partnercategoryBean";

	private String commissionStatusStr = "commissionStatus";

	private String prtCatNoStr = "prtCatNo";

	@RequestMapping(value = "/commission", method = RequestMethod.GET)
	public String commissionManagement(CommissionBean viewCommissionBean, ModelMap model, HttpServletRequest request) {
		String updateMessage = request.getParameter("updateMessage");
		String addComMsg = request.getParameter("addComMsg");
       List<CommissionBean> emptyBean = new ArrayList<>();
		try {
			String partNo = ServiceProviderUtil.nullSafe(request.getParameter("partNo"), "");
			String fromDate = ServiceProviderUtil.nullSafe(request.getParameter("fromDate"), "");
			String toDate = ServiceProviderUtil.nullSafe(request.getParameter("toDate"), "");

			/*model.addAttribute("commissionList",
					commissionmanagementservice.getPartnersCommission(fromDate, toDate, partNo));*/
			model.addAttribute("commissionList",
					emptyBean);

			model.addAttribute("commissionbean", new CommissionBean());
			model.addAttribute(partnercategoryBeanStr, new PartnerCategoryBean());
			model.addAttribute("partnerList", partnermanagementService.getAllPartners());

			model.addAttribute(commissionStatusStr, commonService.getEnumValues(statusStr));
			model.addAttribute("configJPMiles", commonService.getJPMilesEnumValues("OFFERED_JPMILES"));
			viewCommissionBean.setPartNo(partNo);

			viewCommissionBean.setActivationDate(fromDate);
			viewCommissionBean.setDeactivationDate(toDate);

			model.addAttribute("viewCommissionBean", viewCommissionBean);

			if (updateMessage != null) {
				if (updateMessage.equalsIgnoreCase(AfConstants.SUCCESS))
					model.addAttribute(AfConstants.SUCCESS_MESSAGE, "Commission Updated Successfully");

				else if (updateMessage.equalsIgnoreCase(AfConstants.FAILURE))
					model.addAttribute(AfConstants.ERROR_MESSAGE, "Commission could not be updated,Error Occured");
			}

			if (addComMsg != null) {
				if (addComMsg.equalsIgnoreCase(AfConstants.SUCCESS))
					model.addAttribute(AfConstants.SUCCESS_MESSAGE, "New Commission Details Added Successfully");

				else if (addComMsg.equalsIgnoreCase(AfConstants.FAILURE))
					model.addAttribute(AfConstants.ERROR_MESSAGE,
							"New Commission Details Cannot be Deleted! Error Occured");
			}

		} catch (Exception e) {
			logger.error("@@@@ Exception in CommissionManagementController at commissionManagement() :", e);
		}
		return "commissionManagement";
	}

	@RequestMapping(value = "/selectPartnerCategoires", method = RequestMethod.GET)
	public @ResponseBody String selectPartnerCategories(@RequestParam("partNo") String partNo, ModelMap model) {
		String prtNo = (null != partNo) ? partNo.trim() : "";
		List<PartnerCategoryBean> list = new ArrayList<>();

		if (prtNo.length() > 0) {
			model.addAttribute(partnercategoryBeanStr, new PartnerCategoryBean());
			list = commonService.getPartnerCategoryListBean(prtNo);
		}
		return GsonUtil.toJson(list);
	}

	@RequestMapping(value = "/updateCommission", method = RequestMethod.POST)
	public @ResponseBody String updateCommission(@RequestParam("json") String json,HttpServletRequest request) {
		String status = AfConstants.FAILURE;
		try {
			JSONObject commissionJson = new JSONObject(json);
			status = commissionmanagementservice.updateCommission(commissionJson,request);
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}
			logger.debug(" CommissionManagementController status" + status);
		} catch (Exception e) {
			logger.error("@@@@ Exception in CommissionManagementController at updateCommission() :", e);
		}
		return status;
	}

	@RequestMapping(value = "/addCommission", method = RequestMethod.POST)
	public String addCommission(@ModelAttribute("commissionBean") CommissionBean commissionBean, ModelMap model,
			RedirectAttributes redirectAttrs, HttpServletRequest request) {
		String status;
		try {
			status = commissionmanagementservice.addCommission(commissionBean, request);
			/*try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}*/
			model.addAttribute(commissionStatusStr, commonService.getEnumValues(statusStr));

			if (status.equalsIgnoreCase("success")) {
				redirectAttrs.addFlashAttribute("message", "Commission Added Successfully");
			} else {
				redirectAttrs.addFlashAttribute("errormessage", "Commission Could not be Added");
			}
		} catch (Exception e) {
			logger.error("@@@@ Exception in CommissionManagementController at addCommission() :", e);
		}

		return "redirect:/admin/commission";
	}

	@RequestMapping(value = "/addCommissionCancel", method = RequestMethod.GET)
	public @ResponseBody String addPartnerCancel(ModelMap model) {
		return "admin/commission";
	}

	@RequestMapping(value = "/checkExistingDetailsStartDate", method = RequestMethod.POST)
	public @ResponseBody String checkExistingDetailsStartDate(@RequestParam("json") String json,
			RedirectAttributes redirectAttrs) {
		String status = AfConstants.FAILURE;
		try {
			JSONObject commissionJson = new JSONObject(json);
			status = commissionmanagementservice.checkExistingDetailsStartDate(commissionJson);
		} catch (Exception e) {
			logger.error("@@@@ Exception in CommissionManagementController at checkExistingDetailsStartDate() :", e);
		}

		return status;
	}

	@RequestMapping(value = "/checkExistingDetailsEditStartDate", method = RequestMethod.POST)
	public @ResponseBody String checkExistingDetailsEditStartDate(@RequestParam("json") String json,
			RedirectAttributes redirectAttrs) {
		String status = AfConstants.FAILURE;

		try {
			JSONObject commissionJson = new JSONObject(json);
			status = commissionmanagementservice.checkExistingDetailsEditStartDate(commissionJson);
		} catch (Exception e) {
			logger.error("@@@@ Exception in CommissionManagementController at checkExistingDetailsEditStartDate() :",
					e);
		}

		return status;
	}

	@RequestMapping(value = "/checkExistingDetailsEndDate", method = RequestMethod.POST)
	public @ResponseBody String checkExistingDetailsEndDate(@RequestParam("json") String json,
			RedirectAttributes redirectAttrs) {
		String status = AfConstants.FAILURE;

		try {
			JSONObject commissionJson = new JSONObject(json);
			status = commissionmanagementservice.checkExistingDetailsEndDate(commissionJson);

		} catch (Exception e) {
			logger.error("@@@@ Exception in CommissionManagementController at checkExistingDetailsEndDate() :", e);
		}

		return status;
	}

	@RequestMapping(value = "/checkExistingDetailsForEditEndDate", method = RequestMethod.POST)
	public @ResponseBody String checkExistingDetailsForEditEndDate(@RequestParam("json") String json,
			RedirectAttributes redirectAttrs) {
		String status = AfConstants.FAILURE;

		try {
			JSONObject commissionJson = new JSONObject(json);
			status = commissionmanagementservice.checkExistingDetailsEditEndDate(commissionJson);
		} catch (Exception e) {
			logger.error("@@@@ Exception in CommissionManagementController at checkExistingDetailsForEditEndDate() :",
					e);
		}

		return status;
	}

	@RequestMapping(value = "/addNewCommission", method = RequestMethod.POST)
	public @ResponseBody String addNewCommission(@RequestParam("json") String json ,HttpServletRequest request) {
		String status = AfConstants.FAILURE;
		try {
			JSONObject commissionJson = new JSONObject(json);
			status = commissionmanagementservice.addNewCommission(commissionJson,request);
		/*	try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}*/
		} catch (Exception e) {
			logger.error("@@@@ Exception in CommissionManagementController at addNewCommission() :", e);
		}

		return status;
	}

	@RequestMapping(value = "/getPartnerProducts", method = RequestMethod.GET)
	public @ResponseBody Map<String, String> getPartnerProductList(HttpServletRequest request) {
		Map<String, String> ppList = null;
		String prtCatNo = request.getParameter(prtCatNoStr);
		String rpName = request.getParameter("rpName");
		ppList = commonService.getPartnerProductList(prtCatNo, rpName);
		return ppList;
	}

	@RequestMapping(value = "/getBrandForParnerCategory", method = RequestMethod.GET)
	public @ResponseBody Set<String> getBrandForParnerCategory(HttpServletRequest request) {
		String prtCatNo = request.getParameter(prtCatNoStr);
		Set<String> brandList = null;
		brandList = commonService.getBrandForParnerCategory(prtCatNo);
		return brandList;
	}

	@RequestMapping("/checkPartnerProducts")
	public @ResponseBody String checkPartnerProducts(HttpServletRequest request) {
		Map<String, String> rpmap = new HashMap<>();
		boolean checkFlag = false;
		String prtNo = request.getParameter("prtNo");
		String prtPrdNo = request.getParameter("prtPrdNo");
		String prtCatNo = request.getParameter(prtCatNoStr);

		logger.debug("/checkPartnerProducts request-val:" + prtNo + "#" + prtPrdNo + "#" + prtCatNo);

		if (prtNo.length() > 0 && prtPrdNo.length() > 0) {
			rpmap.put("prtNo", prtNo);
			rpmap.put("prtPrdNo", prtPrdNo);
			rpmap.put(prtCatNoStr, prtCatNo);
			checkFlag = commissionmanagementservice.checkPartnerProducts(rpmap);
			if (checkFlag)
				return "exist";
			else
				return "";
		} else
			return "";
	}

	@RequestMapping(value = "/ConfingureJPMile", method = RequestMethod.POST)
	public String addJPMileConfig(@RequestParam("jpMileConfig") String jpMile, HttpServletRequest request,
			ModelMap model) {
		String status = AfConstants.FAILURE;

		try {
			status = commissionmanagementservice.saveJPMileConfiguration(jpMile,request);
		} catch (Exception e) {
			logger.error("@@@@ Exception in commissionManagementController at addJPMileConfig() :", e);
		}

		if (status != null) {
			if (status.equalsIgnoreCase("success"))
				model.addAttribute("message", "JPMiles Updated Successfully");

			else if (status.equalsIgnoreCase("failure"))
				model.addAttribute("errormessage", "JPMiles could not be updated,Error Occured");
		}

		model.addAttribute("commissionbean", new CommissionBean());
		model.addAttribute(partnercategoryBeanStr, new PartnerCategoryBean());
		model.addAttribute("partnerList", partnermanagementService.getAllPartners());
		model.addAttribute(commissionStatusStr, commonService.getEnumValues(statusStr));
		model.addAttribute("viewCommissionBean", new CommissionBean());
		model.addAttribute("commissionList", commissionmanagementservice.getPartnersCommission("", "", ""));
		model.addAttribute("addCommission_", false);
		model.addAttribute("addJPMiles_", true);
		request.setAttribute("configJPMiles", commonService.getJPMilesEnumValues("OFFERED_JPMILES"));

		return "commissionManagement";
	}

	@RequestMapping(value = "/uploadCommissionFile", method = RequestMethod.GET)
	public String commissionFileUpload(CommissionBean viewCommissionBean, ModelMap model, HttpServletRequest request) {

		try {
			model.addAttribute("partnerList", partnermanagementService.getAllPartners());
		} catch (Exception e) {
			logger.error("@@@@ Exception in CommissionManagementController at commissionManagement() :", e);
		}
		return "commissionManagement";

	}

	@RequestMapping(value = "/uploadCommissionFile", method = RequestMethod.POST)
	public String commissionFileUpload(@RequestParam("filename") String name,
			@RequestParam("uploadCSVFile") MultipartFile file, HttpServletRequest request, ModelMap model) {
		String message = "";
		String fileName = "";
		try {
			fileName = new String(file.getBytes());
			String[] lines = fileName.split("\\r?\\n");
			logger.info("In CommisionFile Upload uploaded file is  :::" + fileName);
			List<CommissionUploadBean> commissionList = new ArrayList<>();
			CommissionUploadBean commissionBean;
			for (int i = 1; i < lines.length; i++) {
				String lines1[]= lines[i].split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
				
				commissionBean = new CommissionUploadBean();
				if(lines1.length==0){
					continue;
				}
				commissionBean.setPartnerName(lines1[0].replaceAll("^\"|\"$", ""));
				commissionBean.setPrtCategoryName(lines1[1].replaceAll("^\"|\"$", ""));
				commissionBean.setPrtProductName(lines1[2].replaceAll("^\"|\"$", ""));
				commissionBean.setCategoryPath(lines1[3].replaceAll("^\"|\"$", ""));
				commissionBean.setMaximumJpMilesEarned(lines1[4].replaceAll("^\"|\"$", ""));
				if(lines1[5].replaceAll("^\"|\"$", "").equalsIgnoreCase("Flat Amount")) {
					commissionBean.setPartnerCommisionType("2");
				} else {
					commissionBean.setPartnerCommisionType("1");
				}
				commissionBean.setCommisionValue(lines1[6].replaceAll("^\"|\"$", ""));
				commissionBean.setJpMilesCommsion(lines1[7].replaceAll("^\"|\"$", ""));
				if(lines1[8].replaceAll("^\"|\"$", "").equalsIgnoreCase("Flat Amount")) {
					
					commissionBean.setBonusJpMilesCommisionType("2");
				} else {
					commissionBean.setBonusJpMilesCommisionType("1");
				}
				commissionBean.setBonusJpMilesCommisionValue(lines1[9].replaceAll("^\"|\"$", ""));
				commissionBean.setBonusPartnerCode(lines1[10].replaceAll("^\"|\"$", ""));
				commissionBean.setBonusActivateCode(lines1[11].replaceAll("^\"|\"$", ""));
				commissionBean.setLowerValueRange(lines1[12].replaceAll("^\"|\"$", ""));
				commissionBean.setUpperValueRange(lines1[13].replaceAll("^\"|\"$", ""));
				commissionBean.setActivationDate(lines1[14].replaceAll("^\"|\"$", ""));
				commissionBean.setDeactivationDate(lines1[15].replaceAll("^\"|\"$", ""));
				commissionBean.setCommisionStatus(lines1[16].replaceAll("^\"|\"$", ""));
				commissionBean.setTransactionStatus(lines1[17].replaceAll("^\"|\"$", ""));

				commissionList.add(commissionBean);
				logger.info("Added file content to commisionList");
			}
			message = commissionmanagementservice.updateCommissionReport(commissionList);
			
			String[] err=message.split(" ");
			model.addAttribute("uploadCommission", true); 

			model.addAttribute("partnerList", partnermanagementService.getAllPartners());
			model.addAttribute("commissionbean", new CommissionBean());
			model.addAttribute("viewCommissionBean", new CommissionBean());

			if (message.equalsIgnoreCase("success")) {
				model.addAttribute(AfConstants.SUCCESS_MESSAGE, "File Uploaded Successfully");
			} else if (message.equalsIgnoreCase("failure")) {
				model.addAttribute(AfConstants.ERROR_MESSAGE, "File Upload Failed");

			} else if (message.equalsIgnoreCase(AfConstants.EMAIL_SMS_ERROR)) {
				model.addAttribute(AfConstants.ERROR_MESSAGE, "Mandatory Fields error");

			} else if (message.equalsIgnoreCase(commissionList.size()+"")) {
				model.addAttribute(AfConstants.ERROR_MESSAGE, commissionList.size()+"Start or End Date error");
			} else if(err[0].equalsIgnoreCase("failure")){
				model.addAttribute(AfConstants.ERROR_MESSAGE, "row " +err[1]+" not updated");

			}

		} catch (Exception e) {
			model.addAttribute(AfConstants.ERROR_MESSAGE, "File Upload Failed");
			logger.error("@@@Exception in CommissionManagementController commissionFileUpload():", e);
		}

		return "commissionManagement";
	}
	
	
	@RequestMapping(value = "/commissionServer", method = RequestMethod.GET)
	public @ResponseBody String getCommisssionList(@ModelAttribute("commissionBean") CommissionBean commissionBean, 
			ModelMap model, HttpServletRequest request,@RequestParam("length") int length,@RequestParam("start") int start,@RequestParam("draw") int draw) {
		String response="";
		String jsonResponseStart = "{\"data\":";
		String jsonResponseEnd = "}";
		String partNo = ServiceProviderUtil.nullSafe(request.getParameter("partNo"), "");
		String fromDate = ServiceProviderUtil.nullSafe(request.getParameter("fromDate"), "");
		String toDate = ServiceProviderUtil.nullSafe(request.getParameter("toDate"), "");
		 List<CommissionBean> commisssionList = null;
		 Long commisssionListTotal = null;
		try{
			commisssionList=commissionmanagementservice.getPartnersCommissionPagination(fromDate, toDate, partNo,length,start);
			commisssionListTotal = commissionmanagementservice.getPartnersCommissionPaginationCount(fromDate, toDate, partNo);
		}
                
		catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in CommissionManagementController at getCommisssionList() :", e);
		}
//                System.out.println("--------->"+commisssionListTotal);
		//return commisssionList;
		return jsonResponseStart+GsonUtil.toJson(commisssionList)+",\"draw\":"+draw+",\"recordsTotal\":"+commisssionListTotal+",\"recordsFiltered\":"+commisssionListTotal+jsonResponseEnd;
}
	
	
	@RequestMapping(value = "/commissionExportCsv ", method = RequestMethod.GET)
	public void exportCommissionExportCsv (ModelMap model,HttpServletRequest request, HttpServletResponse response) throws IOException {
		 String fileName = "Commisiion_"+new SimpleDateFormat("ddMMyyyyHHmmss'.csv'").format(new Date());
		 String CSV_LOCATION = reportFileLocation+"/"+fileName;
		 String COMMA_DELIMITER = ",";
		 String NEW_LINE_SEPARATOR = "\n";
		 String FILE_HEADER = "Partner Name,Partner Category Name,Product Name,Commission Type,Transaction Type,Partner Commission,JP Miles,Lower Value,Upper Value,ActivateDate,DeactivateDate,Status";
		 
		 FileWriter fileWriter = null;
		 try{
			 List<CommissionBean> reportList = commissionmanagementservice.getCommissionExportCsvList();	
			 fileWriter = new FileWriter(CSV_LOCATION);
			 fileWriter.append(FILE_HEADER.toString());
			 for(CommissionBean commissionBean: reportList){
				 fileWriter.append(NEW_LINE_SEPARATOR);
				 fileWriter.append("\""+commissionBean.getPartnerName()+"\"");
				 fileWriter.append(COMMA_DELIMITER);
				 fileWriter.append("\""+commissionBean.getPrtCategoryName()+"\"");
				 fileWriter.append(COMMA_DELIMITER);
				 fileWriter.append("\""+commissionBean.getPrtProductName()+"\"");
				 fileWriter.append(COMMA_DELIMITER);
				 fileWriter.append("\""+commissionBean.getCommisionType()+"\"");
				 fileWriter.append(COMMA_DELIMITER);
				 fileWriter.append("\""+commissionBean.getTransactionStatus()+"\"");
				 fileWriter.append(COMMA_DELIMITER);
				 fileWriter.append("\""+commissionBean.getCommissionRate()+"\"");
				 fileWriter.append(COMMA_DELIMITER);
				 fileWriter.append("\""+commissionBean.getMemberShare()+"\"");
				 fileWriter.append(COMMA_DELIMITER);
				 fileWriter.append("\""+commissionBean.getLowerValue()+"\"");
				 fileWriter.append(COMMA_DELIMITER);
				 fileWriter.append("\""+commissionBean.getUpperValue()+"\"");
				 fileWriter.append(COMMA_DELIMITER);
				 fileWriter.append("\""+commissionBean.getActivationDate()+"\"");
				 fileWriter.append(COMMA_DELIMITER);
				 fileWriter.append("\""+commissionBean.getDeactivationDate()+"\"");
				 fileWriter.append(COMMA_DELIMITER);
				 fileWriter.append("\""+commissionBean.getCommissionSatus()+"\"");
				 fileWriter.append(COMMA_DELIMITER);
			 }
		 }
		 catch(Exception e){
			 e.printStackTrace();
			 logger.error("@@@@ Exception in CommissionManagementController at exportcommissionExportCsv() :", e);
		 }
		 finally {
				try {
					fileWriter.flush();
					fileWriter.close();
				} catch (IOException e) {
					logger.error("@@@Exception in CommissionManagementController exportcommissionExportCsv():", e);
				}
			}
		 
		 File downloadFile = new File(CSV_LOCATION);
	        FileInputStream inputStream = new FileInputStream(downloadFile);
	         
	        
	           String  mimeType = "application/octet-stream";
	        
	        response.setContentType(mimeType);
	        response.setContentLength((int) downloadFile.length());
	 
	        
	        String headerKey = "Content-Disposition";
	        String headerValue = String.format("attachment; filename=\"%s\"",
	                downloadFile.getName());
	        response.setHeader(headerKey, headerValue);
	 
	       
	        OutputStream outStream = response.getOutputStream();
	 
	        byte[] buffer = new byte[BUFFER_SIZE];
	        int bytesRead = -1;
	 
	        
	        while ((bytesRead = inputStream.read(buffer)) != -1) {
	            outStream.write(buffer, 0, bytesRead);
	        }
	 
	        inputStream.close();
	        outStream.close();
	}
	
	
	
}