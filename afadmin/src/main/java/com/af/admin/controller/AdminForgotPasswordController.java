package com.af.admin.controller;

import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.af.admin.beans.AdminUserBean;
import com.af.admin.service.UserManagementService;
import com.af.admin.utils.EmailUtil;
@Controller
@RequestMapping("/adminForgotPassword")
public class AdminForgotPasswordController {
	private Logger logger = LogManager.getLogger(AdminForgotPasswordController.class.getName());
	@Autowired
	EmailUtil emailUtil;
	
	@Autowired
	UserManagementService userManagementService;
	
	@Value("${application.mail.from}")
	private String from;
	
	@Value("${application.jpplip.application_URL}")
	private String loginUrl;
	
	@RequestMapping(method=RequestMethod.GET)
	public String forgotPasswordPage(){
		return "admin/login/forgotPassword";	
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public String sendEmail(HttpServletRequest request,ModelMap model,RedirectAttributes redirectAttrs){
	AdminUserBean bean=null;
	String userName=request.getParameter("userName");
	try{
		bean=userManagementService.getUserDetailsByUserName(userName);
	
		if(null!=bean){
			emailUtil.sendMail(from.trim(), userName.trim() ,  "Forgot Password", "Hi your password is "+bean.getPassword() );
			redirectAttrs.addFlashAttribute("successMessage", "Password Successfully send to ur Email ID");
	
			if(request.getSession()!=null){
				request.getSession().invalidate();
			}
		}
		else{
			model.addAttribute("errorMsg", "User is not Exist");
			return "admin/login/forgotPassword";
		}
	}catch(Exception e){
		logger.error("@@@@ Exception in AdminForgotPasswordController at sendEmail() :", e);
	}

	return "redirect:"+loginUrl;
	}
}
