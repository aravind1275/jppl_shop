package com.af.admin.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.af.admin.beans.AdminRolesBean;
import com.af.admin.beans.BuisnessReportsBean;
import com.af.admin.beans.CommissionBean;
import com.af.admin.beans.OffersAndDealsJson;
import com.af.admin.beans.OffersDealsBean;
import com.af.admin.beans.PartnerCategoryBean;
import com.af.admin.beans.offersBean;
import com.af.admin.constants.AfConstants;
import com.af.admin.entity.AfOfferDealLink;
import com.af.admin.entity.AfPartnerOffer;
import com.af.admin.service.CommonService;
import com.af.admin.service.OffersDealsService;
import com.af.admin.service.PartnerManagementService;
import com.af.admin.utils.CommonUtil;
import com.af.admin.utils.GsonUtil;
import com.af.admin.utils.ServiceProviderUtil;

@Controller
public class OffersDealsController {
	 private static final int BUFFER_SIZE = 4096;
	 
	private Logger logger = LogManager.getLogger(OffersDealsController.class.getName());


	@Autowired
	PartnerManagementService partnermanagementService;

	@Autowired
	OffersDealsService offersDealsService;

	@Autowired
	CommonService commonService;

	@Autowired
	CommonUtil commonUtil;
	
	
	@Value("${application.af.upload.files.folder}")
	private String fileFolderLocation;
	
	@Value("${application.af.report_download}")
	private String reportFileLocation;
	
	private String offersDealsBeanStr = "offersDealsBean";
	
	private String offersDealsListStr = "offersDealsList";
	
	private String partnerListStr = "partnerList";
	
	private String offersStatusStr = "offersStatus";
	
	private String allOffersDealsListStr = "allOffersDealsList";
	
	@RequestMapping(value = "offersDeals", method = RequestMethod.GET)
	public String getOffersDealsPage(ModelMap model, HttpServletRequest request) {
		String deleteMsg = request.getParameter("deleteMsg");
		String message;
		String updateMsg;
		try {
			model.addAttribute("offersDealsBean_", new OffersDealsBean());
			model.addAttribute(offersDealsBeanStr, new OffersDealsBean());
			model.addAttribute(offersDealsListStr, offersDealsService.getOffersDealsList());
			model.addAttribute(partnerListStr, partnermanagementService.getAllPartners());
			model.addAttribute(offersStatusStr, commonService.getEnumValues(AfConstants.STATUS));
			model.addAttribute("offersBean",new offersBean());
			
			if(deleteMsg != null) {
				if(deleteMsg.equalsIgnoreCase(AfConstants.SUCCESS))
					model.addAttribute(AfConstants.SUCCESS_MESSAGE, "Offers and Deals Deactivated Successfully");
				else if(deleteMsg.equalsIgnoreCase(AfConstants.FAILURE))
					model.addAttribute(AfConstants.ERROR_MESSAGE, "Offers and Deals Could not be Deactivated! Error Occurred");
			}
			message = request.getParameter("status") != null ? URLDecoder.decode(request.getParameter("status"), "UTF-8") : null;
			if(message != null) {
				if(message.contains("successfully"))
					model.addAttribute(AfConstants.SUCCESS_MESSAGE, message);
				else if(message.equalsIgnoreCase(AfConstants.FAILURE))
					model.addAttribute(AfConstants.ERROR_MESSAGE, "File Upload Failed");
				else if(message.contains("emptyFile"))
					model.addAttribute(AfConstants.ERROR_MESSAGE, "You failed to upload '"
									+ message.substring(9) + "' because the file is empty.");
				else if(message.contains("upload,"))
					model.addAttribute(AfConstants.ERROR_MESSAGE, "Failed to upload, File is empty");
				else if(message.contains("noFileUpdate"))
					model.addAttribute(AfConstants.ERROR_MESSAGE, "Failed to upload, all duplicate records found");
			}
			
			
			updateMsg = request.getParameter("updateMsg");
			if(updateMsg != null) {
			if(updateMsg.equalsIgnoreCase(AfConstants.SUCCESS))
				model.addAttribute(AfConstants.SUCCESS_MESSAGE, "Offers and Deals updated Successfully");
			else if(updateMsg.equalsIgnoreCase(AfConstants.FAILURE))
				model.addAttribute(AfConstants.ERROR_MESSAGE, "Offers and Deals Could not be updated");
			}
			commonUtil.getDefaultCategoryTree(request, "", "");
			
			Set<String> filesList =  commonUtil.listOfFilesInFolder(new File(
					fileFolderLocation + File.separator + "offersAndDeals" + File.separator));
			
			filesList.removeAll(Collections.singleton(""));
			model.addAttribute("offersDealsFilePath", fileFolderLocation + "offersAndDeals" + File.separator);
			model.addAttribute("uploadSuccessFiles", filesList);
		} catch(Exception e) {
			logger.error("@@@@ Exception in OffersDealsController at getOffersDealsPage() :", e);
		}
		return "offersDeals";
	}

	@RequestMapping(value="/getSearchByNameList",method=RequestMethod.GET)
	public @ResponseBody Map<String, String> getSearchByNameList(HttpServletRequest request){
		Map<String, String> ppList = null;		
		try {
			String prtNo 	= request.getParameter("prtNo");
			String searchByName = request.getParameter("searchByName");
			ppList = offersDealsService.getSearchByNameList(prtNo,searchByName);
		} catch(Exception e) {
			logger.error("@@@@ Exception in OffersDealsController at getSearchByNameList() :", e);
		}
		return ppList;
	}

	@RequestMapping(value="searchOfferDeals", method = RequestMethod.GET)
	public String searchOfferDeals(ModelMap model, HttpServletRequest request) {
		List<OffersDealsBean> searchOffersDealslist = new ArrayList<>();
		try {
			OffersDealsBean offersDealsBean = new OffersDealsBean();

			String requestPrtNo = (String) model.get("prtNo");
			String requestSearchByName = (String) model.get("searchByName");
			String requestSearchByValue = (String) model.get("searchByValue");
			String requestCatNo = (String) model.get("catNo");

			String deleteMsg = request.getParameter("deleteMsg");

			if( (deleteMsg != null) && !(deleteMsg.equalsIgnoreCase("")) ) {
				requestPrtNo = request.getParameter("prtNo");
				requestSearchByName = request.getParameter("searchByName");
				requestSearchByValue = request.getParameter("searchByValue");
				requestCatNo = request.getParameter("catNo");

				if(deleteMsg != null) {
					if(deleteMsg.equalsIgnoreCase(AfConstants.SUCCESS))
						model.addAttribute(AfConstants.SUCCESS_MESSAGE, "Offers and Deals Deactivated Successfully");
					else if(deleteMsg.equalsIgnoreCase(AfConstants.FAILURE))
						model.addAttribute(AfConstants.ERROR_MESSAGE, "Offers and Deals Could not be Deactivated! Error Occurred");
				}
			}
			if( requestPrtNo != null && !(requestPrtNo.equalsIgnoreCase("")) ) {
				offersDealsBean.setPrtNo(Integer.parseInt(requestPrtNo));
			}
			offersDealsBean.setSearchByName(requestSearchByName);
			offersDealsBean.setSearchByValue(requestSearchByValue);
			offersDealsBean.setCatNo(requestCatNo);

			if( requestPrtNo != null && !(requestPrtNo.equalsIgnoreCase(""))
					&& requestSearchByName != null && !(requestSearchByName.equalsIgnoreCase(""))
					&& requestSearchByValue != null && !(requestSearchByValue.equalsIgnoreCase("")) ) {

				searchOffersDealslist = offersDealsService.searchOffesrDeals(offersDealsBean);
				model.addAttribute(offersDealsListStr, searchOffersDealslist);
			} else {
				model.addAttribute(offersDealsListStr, offersDealsService.getOffersDealsList());
			}

			model.addAttribute("offersDealsBean_", offersDealsBean);
			model.addAttribute(offersDealsBeanStr, new OffersDealsBean());
			model.addAttribute(partnerListStr, partnermanagementService.getAllPartners());
			model.addAttribute(offersStatusStr, commonService.getEnumValues(AfConstants.STATUS));

			commonUtil.getDefaultCategoryTree(request, "", "");
		}
		catch(Exception e) {
			logger.error("@@@@ Exception in OffersDealsController at searchOfferDeals() GET:", e);
		}
		return "offersDeals";
	}

	@RequestMapping(value="searchOfferDeals", method = RequestMethod.POST)
	public String searchOfferDeals(@ModelAttribute("offersDealsBean_") OffersDealsBean offersDealsBean,
			ModelMap model, HttpServletRequest request) {
			try {
//                            System.out.println("offersDealsBean.getPrtNo()  "+offersDealsBean.getPrtNo());
			if((offersDealsBean.getPrtNo() != null) || !(offersDealsBean.getSearchByName().equals("")) || !(offersDealsBean.getSearchByValue().equals("")) ) {
//                                 System.out.println("inside list----------------");
				model.addAttribute(offersDealsListStr, offersDealsService.searchOffesrDeals(offersDealsBean));
			} else {
				model.addAttribute(offersDealsListStr, offersDealsService.getOffersDealsList());
			}
			model.addAttribute("requestPrtNo", offersDealsBean.getPrtNo());
			model.addAttribute("requestSearchByName", offersDealsBean.getSearchByName());
			model.addAttribute("requestSearchByValue", offersDealsBean.getSearchByValue());
			model.addAttribute("requestCatNo", offersDealsBean.getCatNo());

			model.addAttribute(offersDealsBeanStr, new OffersDealsBean());
			model.addAttribute(partnerListStr, partnermanagementService.getAllPartners());
			model.addAttribute(offersStatusStr, commonService.getEnumValues(AfConstants.STATUS));

			commonUtil.getDefaultCategoryTree(request, "", "");
		}
		catch(Exception e) {
			logger.error("@@@@ Exception in OffersDealsController at searchOfferDeals() :", e);
		}
		return "offersDeals";
	}

	@RequestMapping(value="/getODPartnerCategories",method=RequestMethod.GET)
	public @ResponseBody String getPartnerCategories(@RequestParam("prtNo") String prtNo,ModelMap model){
		prtNo = (null!=prtNo)?prtNo.trim():"";
		List<PartnerCategoryBean> list = new ArrayList<>();		
		if(prtNo.length()>0){			
			list=commonService.getPartnerCategoryListBean(prtNo);
		}
		return GsonUtil.toJson(list) ;
	}

	@RequestMapping(value="/offersDeals",method=RequestMethod.POST)
	public String defineOffersDeals(@ModelAttribute("offersDealsBean") OffersDealsBean offersDealsBean,
			ModelMap model, RedirectAttributes redirectAttrs, HttpServletRequest request){
		String status;
		try {
			status = offersDealsService.defineOffersDeals(offersDealsBean,request);
			/*try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}*/
			if(status.equalsIgnoreCase(AfConstants.SUCCESS))
				redirectAttrs.addFlashAttribute(AfConstants.SUCCESS_MESSAGE, "Offers and Deals Created Successfully");
			else
				redirectAttrs.addFlashAttribute(AfConstants.ERROR_MESSAGE, "Offers and Deals Could not be Created ");

			commonUtil.getDefaultCategoryTree(request, "", "");
		} catch(Exception e) {
			logger.error("@@@@ Exception in OffersDealsController at defineOffersDeals() :", e);
		}
		return "redirect:/admin/offersDeals";
	}

	@RequestMapping(value="/updateOffersDeals")
	public String updateOffersDealsGet(ModelMap model, HttpServletRequest request){
		OffersDealsBean OffersDealsBean = null;
		try{
			String prtOffrNo = request.getParameter("prtOffrNo");

			model.addAttribute(offersStatusStr, commonService.getEnumValues(AfConstants.STATUS));
			model.addAttribute(partnerListStr, partnermanagementService.getAllPartners());
			OffersDealsBean = offersDealsService.getPartnerOfferByNO(prtOffrNo);
			model.addAttribute("partnerCatNo", OffersDealsBean.getPrtCatNo());
			model.addAttribute("partnerCatList", OffersDealsBean.getPartnerCategoryBean());
			model.addAttribute(offersDealsBeanStr, OffersDealsBean);
			model.addAttribute("offerCategoryName", OffersDealsBean.getCategoryName());
			model.addAttribute("offerAltTxt", OffersDealsBean.getAltTxt());
			model.addAttribute("imageurloffer", OffersDealsBean.getOfferImageUrl());
			model.addAttribute("requestPrtNo", request.getParameter("requestPrtNo"));
			model.addAttribute("requestSearchByName", request.getParameter("requestSearchByName"));
			model.addAttribute("requestSearchByValue", request.getParameter("requestSearchByValue"));
			model.addAttribute("requestCatNo", request.getParameter("requestCatNo"));

			commonUtil.getDefaultCategoryTree(request, "", "");
		}catch(Exception e){
			logger.error("@@@@ Exception in OffersDealsController at updateOffersDealsGet() :", e);
		}
		return "updateOffersDeals";
	}

	@RequestMapping(value = "/updateOffersDeals", method = RequestMethod.POST)
	public String updateOffersDetailsPost(@ModelAttribute("offersDealsBean") OffersDealsBean offersDealsBean,
			RedirectAttributes redirectAttrs, HttpServletRequest request){

		String status;
		try {

			redirectAttrs.addFlashAttribute("catNo", offersDealsBean.getRequestCatNo());

			status = offersDealsService.updateOffersDealsDetails(offersDealsBean,request);
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}
			if(status.equalsIgnoreCase(AfConstants.SUCCESS))
				redirectAttrs.addFlashAttribute(AfConstants.SUCCESS_MESSAGE, "Offers and Deals Updated Successfully");
			else
				redirectAttrs.addFlashAttribute(AfConstants.ERROR_MESSAGE, "Offers and Deals Could not be Updated ");

			commonUtil.getDefaultCategoryTree(request, "", "");
		} catch(Exception e) {
			logger.error("@@@@ Exception in OffersDealsController at updateOffersDetailsPost() :", e);
		}
		return "redirect:/admin/searchOfferDeals";
	}

	@RequestMapping(value="/getOffersNames",method=RequestMethod.GET)
	public @ResponseBody Map<String, String> getOffersNames(HttpServletRequest request){
		Map<String, String> ppList = null;
		try{	
			String prtNo	=	request.getParameter("prtNo");
			String prtCatNo	=	request.getParameter("prtCatNo");
			logger.debug("/getOffersNames prtNo:"+prtNo+"--prtCatNo"+prtCatNo);
			ppList = offersDealsService.getOfferNamesByPrtCatNo(prtNo,prtCatNo);
		}catch(Exception e){
			logger.error("@@@@ Exception in OffersDealsController at getOffersNames() :", e);
		}
		return ppList;
	}

	@RequestMapping(value="/deleteSelectedOffer")
	public @ResponseBody String deleteSelectedOffer(@RequestParam("prtOffrNo") String prtOffrNo,HttpServletRequest request){
		String deleteStatus = "";
		try{			
			deleteStatus = offersDealsService.deleteSelectedOffer(new Integer(prtOffrNo),request);
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}
			logger.debug(deleteStatus);
		}catch(Exception e){
			e.printStackTrace();
			logger.debug("@@@@ Exception in OffersDealsController at deleteSelectedOffer() :", e);
		}
		return deleteStatus;
	}

	@RequestMapping("/checkOffersDealSortOrder")
	public @ResponseBody String checkOffersDealSortOrder(HttpServletRequest request){
		boolean checkFlag=false;
		String offersDealsSortOrder=request.getParameter("offersDealsSortOrder");
		if(offersDealsSortOrder.length()>0){
			checkFlag=offersDealsService.checkOffersDealSortOrder(offersDealsSortOrder);
			if(checkFlag) return "exist"; else return "";
		}else
			return "";	
	}
	
	@RequestMapping(value="/getOfferTitle",method=RequestMethod.GET)
	public @ResponseBody Map<String, String> getOfferTitle(HttpServletRequest request){
		Map<String, String> ppList = null;
		try{	
			String prtNo	=	request.getParameter("prtNo");
			String catNo	=	request.getParameter("catNo");
			logger.info("/getOfferTitle prtNo: " + prtNo + " catNo: " + catNo);
			ppList = offersDealsService.getOfferTitle(prtNo, catNo);
		}catch(Exception e){
			logger.error("@@@@ Exception in OffersDealsController at getOfferTitle() :", e);
		}
		return ppList;
	}
	
	@RequestMapping(value = "/allOffersDeals", method = RequestMethod.GET)
	public @ResponseBody List<AfOfferDealLink> getAllOffersDealsPage(@ModelAttribute("offersBean") offersBean offersBean,ModelMap model, HttpServletRequest request) {
		String response="";
		String jsonResponseStart = "{\"data\":";
		String jsonResponseEnd = "}";
		try{
				response=jsonResponseStart+GsonUtil.toJson(offersDealsService.getAllOffersDealsList())+jsonResponseEnd;
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in OffersDealsController at getAllOffersDealsPage() :", e);
		}
		
		return offersDealsService.getAllOffersDealsList();
	}
	@RequestMapping(value = "/allOffersDealscheck", method = RequestMethod.GET)
	public @ResponseBody List<AfOfferDealLink> getAllOffersDealsPagecheck(@ModelAttribute("offersBean") offersBean offersBean, ModelMap model, HttpServletRequest request) {
		String response="";
		String jsonResponseStart = "{\"data\":";
		String jsonResponseEnd = "}";
		try{
				//response=jsonResponseStart+GsonUtil.toJson(offersDealsService.getAllOffersDealsListcheck())+jsonResponseEnd;
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in OffersDealsController at getAllOffersDealsPage() :", e);
		}
		
		return offersDealsService.getAllOffersDealsListcheck();
		
	}
	@RequestMapping(value = "/updateOffersDealsLink", method = RequestMethod.POST)
	public @ResponseBody String updateOffersDealsLink(@RequestParam("json") String json,HttpServletRequest request,RedirectAttributes redirectAttrs){
		String status=AfConstants.FAILURE;
		try {
			JSONObject userJson=new JSONObject(json);			
 			status=offersDealsService.updateOffersDealsLink(userJson,request);
			try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("@@@@ Exception in OffersDealsController at updateNotification() :", e);
		}
		return status;
	}	
	
	@RequestMapping(value = "/addOffersDeals", method = RequestMethod.POST)
	public @ResponseBody String addOffersDealsCheck(ModelMap model,@RequestParam("lmdId") String lmdId,@RequestParam("id") String Id,HttpServletRequest request,RedirectAttributes redirectAttrs) {
		String status = "";
		try{
			status=offersDealsService.addOfferDeal(lmdId,Id);		
			/*try {
				commonUtil.akamaiPurge();
			} catch (IOException e) {
				logger.error("Akamai Exception ",e);
			}*/
			}catch(Exception e){
			logger.error("@@@@ Exception in OffersDealsController at addOffersDealsCheck() :", e);
		}
		return status;
		
	}
	
	@RequestMapping(value = "/exportOfferDealLinkCsv", method = RequestMethod.GET)
	public void exportOfferDealLinkCsv(ModelMap model,HttpServletRequest request, HttpServletResponse response) throws IOException {
		 String fileName = "OfferDealLink_"+new SimpleDateFormat("ddMMyyyyHHmmss'.csv'").format(new Date());
		 String CSV_LOCATION = reportFileLocation+"/"+fileName;
		 String COMMA_DELIMITER = ",";
		 String NEW_LINE_SEPARATOR = "\n";
		 String FILE_HEADER = "Lmd Id,Partner Name,Offer Text,Title,Description,Categories,Start_Date,End_Date,Status";
		 
		 FileWriter fileWriter = null;
		 try{
			 List<offersBean> reportList = offersDealsService.getExportOffersDealList();	
			 fileWriter = new FileWriter(CSV_LOCATION);
			 fileWriter.append(FILE_HEADER.toString());
			 for(offersBean offersBean: reportList){
				 fileWriter.append(NEW_LINE_SEPARATOR);
				 fileWriter.append("\""+offersBean.getLmd_id()+"\"");
				 fileWriter.append(COMMA_DELIMITER);
				 fileWriter.append("\""+offersBean.getStore()+"\"");
				 fileWriter.append(COMMA_DELIMITER);
				 fileWriter.append("\""+offersBean.getOffer_text()+"\"");
				 fileWriter.append(COMMA_DELIMITER);
				 fileWriter.append("\""+offersBean.getOffers__title()+"\"");
				 fileWriter.append(COMMA_DELIMITER);
				 fileWriter.append("\""+offersBean.getOffers__categories()+"\"");
				 fileWriter.append(COMMA_DELIMITER);
				 fileWriter.append("\""+offersBean.getOffers__description()+"\"");
				 fileWriter.append(COMMA_DELIMITER);
				 fileWriter.append("\""+offersBean.getOffers__start_date()+"\"");
				 fileWriter.append(COMMA_DELIMITER);
				 fileWriter.append("\""+offersBean.getOffers__end_date()+"\"");
				 fileWriter.append(COMMA_DELIMITER);
				 fileWriter.append("\""+offersBean.getStatus()+"\"");
				 fileWriter.append(COMMA_DELIMITER);
			 }
		 }
		 catch(Exception e){
			 e.printStackTrace();
			 logger.error("@@@@ Exception in OffersDealsController at exportOfferDealLinkCsv() :", e);
		 }
		 finally {
				try {
					fileWriter.flush();
					fileWriter.close();
				} catch (IOException e) {
					logger.error("@@@Exception in OffersDealsController exportOfferDealLinkCsv():", e);
				}
			}
		 
		 File downloadFile = new File(CSV_LOCATION);
	        FileInputStream inputStream = new FileInputStream(downloadFile);
	         
	        
	           String  mimeType = "application/octet-stream";
	        
	        response.setContentType(mimeType);
	        response.setContentLength((int) downloadFile.length());
	 
	        
	        String headerKey = "Content-Disposition";
	        String headerValue = String.format("attachment; filename=\"%s\"",
	                downloadFile.getName());
	        response.setHeader(headerKey, headerValue);
	 
	       
	        OutputStream outStream = response.getOutputStream();
	 
	        byte[] buffer = new byte[BUFFER_SIZE];
	        int bytesRead = -1;
	 
	        
	        while ((bytesRead = inputStream.read(buffer)) != -1) {
	            outStream.write(buffer, 0, bytesRead);
	        }
	 
	        inputStream.close();
	        outStream.close();
	}
	
	
	@RequestMapping(value = "/approvedOfferDealLink", method = RequestMethod.GET)
	public @ResponseBody List<AfOfferDealLink> approvedOfferDealLink(ModelMap model,HttpServletRequest request, HttpServletResponse response) throws IOException {
		List<AfOfferDealLink> offersBean=null;
		try{
			offersBean=offersDealsService.approvedOfferDealLink();
		}
		catch(Exception e){
			logger.error("@@@Exception in OffersDealsController approvedOfferDealLink():", e);
		}
		return offersBean;
	}
	
	@RequestMapping(value = "/pendingOfferDealLink", method = RequestMethod.GET)
	public @ResponseBody List<AfOfferDealLink> pendingOfferDealLink(ModelMap model,HttpServletRequest request, HttpServletResponse response) throws IOException {
		List<AfOfferDealLink> afOfferDealLink=null;
		try{
			afOfferDealLink=offersDealsService.pendingOfferDealLink();
		}
		catch(Exception e){
			logger.error("@@@Exception in OffersDealsController pendingOfferDealLink():", e);
		}
		return afOfferDealLink;
	}
	
	@RequestMapping(value = "/newRecordOfferDealLink", method = RequestMethod.GET)
	public @ResponseBody List<AfOfferDealLink> newRecordOfferDealLink(ModelMap model,HttpServletRequest request, HttpServletResponse response) throws IOException {
		List<AfOfferDealLink> afOfferDealLink=null;
		try{
			afOfferDealLink=offersDealsService.newRecordOfferDealLink();
		}
		catch(Exception e){
			logger.error("@@@Exception in OffersDealsController pendingOfferDealLink():", e);
		}
		return afOfferDealLink;
	}
	
	@RequestMapping(value = "/OffersDealPagination", method = RequestMethod.GET)
	public @ResponseBody String getoffersDealsBeanList(@ModelAttribute("offersDealsBean") OffersDealsBean offersDealsBean, 
			ModelMap model, HttpServletRequest request,@RequestParam("length") int length,@RequestParam("start") int start,@RequestParam("draw") int draw) {
		String response="";
		String jsonResponseStart = "{\"data\":";
		String jsonResponseEnd = "}";
		 List<OffersDealsBean> offersDealsBeanList = null;
		 Long offersDealsBeanListTotal = null;
		try{
			offersDealsBeanList=offersDealsService.getOffersDealsListPagination(length,start);
			offersDealsBeanListTotal = offersDealsService.getPartnersCommissionPaginationCount();
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error("@@@@ Exception in OffersDealsController at getoffersDealsBeanList() :", e);
		}
//                System.out.println("------------>"+offersDealsBeanListTotal);
		//return commisssionList;
		return jsonResponseStart+GsonUtil.toJson(offersDealsBeanList)+",\"draw\":"+draw+",\"recordsTotal\":"+offersDealsBeanListTotal+",\"recordsFiltered\":"+offersDealsBeanListTotal+jsonResponseEnd;
}
	
	
	
  }
