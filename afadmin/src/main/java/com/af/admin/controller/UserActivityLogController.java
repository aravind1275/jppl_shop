package com.af.admin.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.af.admin.beans.UserActivityLogBean;
import com.af.admin.service.UserActivityLogService;


@Controller
public class UserActivityLogController {
	
	private Logger logger = LogManager.getLogger(ReportsController.class.getName());

	@Autowired
	UserActivityLogService userActLogService;
	
	
	
	@RequestMapping(value="/userActivityLog",method=RequestMethod.GET)
	public String getUserActivityLog(HttpServletRequest request, ModelMap model) {

		
	    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
	    String CurrentDay = formatter.format(date);
       
   	   try{ 
    List<UserActivityLogBean> userActivityLogList = userActLogService.getUserActLogListDefault(CurrentDay);
    model.addAttribute("userActivityLogList", userActivityLogList);
   	   }catch (Exception e) {
   		logger.error("@@@@ Exception in UserActivityLogController at getUserActivityLog() :", e);
	}
		
		return "userActivityLog";
	}
	
	
    @RequestMapping(value="userActivityLog", method=RequestMethod.POST)
    public String getUserActivityWorkLogByDate(@RequestParam("userActFromDate") String userActFromDate,
    		@RequestParam("userActTodate") String userActTodate, ModelMap model){
    	try{
    	List<UserActivityLogBean> userActivityLogList = userActLogService.getUserActLogList(userActFromDate, userActTodate);
    	
    	model.addAttribute("userActFromDate", userActFromDate);
    	model.addAttribute("userActTodate", userActTodate);
    	model.addAttribute("userActivityLogList", userActivityLogList);
    	
    	if(userActivityLogList == null || userActivityLogList.size() == 0)
		{
			model.addAttribute("message", "No Records Found");
		}
    	}catch (Exception e) {
    		logger.error("@@@@ Exception in UserActivityLogController at getUserActivityWorkLogByDate() :", e);
		}
    	return "userActivityLog";
    }
	
	

}
