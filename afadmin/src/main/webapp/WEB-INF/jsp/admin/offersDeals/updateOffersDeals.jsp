<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
	<div class='container'>
	<!-- Select Category Modal Start -->
	<div id="categoriesModal" class="modal fade" role="dialog">	
		<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Select Category</h4>
			</div><!--header-->
			<div class="modal-body">
				<div class="select_category"></div>
			</div><!---body--->
			<div class="modal-footer">
			</div>
		</div><!---modal content--->
		</div>
	</div>
	<!-- Select Category Modal End -->         
            <section class="insurance_management">
                <div id ="breadcrumb">
                    <ul>
                        <li>Home</li>
						<li>Content Management</li>
						<li>Offers & Deals</li>
                    </ul>
                </div>  <!---breadcrumb--->
				<div class="section_heading">
					<h5>Offers & Deals</h5> 
				</div><!--- section heading ---->
				<div class="management"><!---User Management--->
                    <ul class="nav nav-tabs">
                        <li><a href="${contextPath}/admin/offersDeals">Offers & Deals</a></li>
                        <li class="active"><a data-toggle="tab" href="#sectionB">Update Offers & Deals</a></li>
                    </ul>
                    <form:form autocomplete="off" commandName="offersDealsBean" enctype="multipart/form-data">
					<form:input type="hidden" path="getCategoryList" id="getCategoryList" value="${requestScope.categoriesList}" />
					<form:input type="hidden" path="catNo" id="catNoID" />
					<form:input type="hidden" path="prtCatNo" id="prtCatNo" value="${partnerCatNo}" />
					
					<form:input type="hidden" path="requestPrtNo" id="requestPrtNo" value="${requestPrtNo}" />
					<form:input type="hidden" path="requestSearchByName" id="requestSearchByName" value="${requestSearchByName}" />
					<form:input type="hidden" path="requestSearchByValue" id="requestSearchByValue" value="${requestSearchByValue}" />
					<form:input type="hidden" path="requestCatNo" id="requestCatNo" value="${requestCatNo}" />
					
					<div id="sectionB" class="tab-pane fade submenu in active" ><!-----Define Recommended starts here---->
						<div class="row pad_top">
							<div class="col-md-3">
								<label>Select A Partner</label><span class="mandatory_star">*</span>
								<fieldset class="infield">
									<form:select path="prtNo" id="prtNo" class="select_option">
											<form:option value="">-Select Partner-</form:option>
											<c:forEach items="${partnerList}" var="partner">
	               					     	 <li>
	             					     	 <form:option value="${partner.prtNo}" label="${partner.partnerName}" />
	             							</li>
	             						</c:forEach>
										</form:select>
								</fieldset>	
								<input type="hidden" name="prtNo_" id="prtNo_"/>
							</div>
							<%-- <div class="col-md-4">
								<label>Partner Category</label><span class="mandatory_star">*</span>
								<fieldset class="infield">
									<form:select path="prtCatNo" id="prtCatNo" class="select_option">
             							<form:option value="">-Select Category-</form:option>
	             							<c:forEach items="${partnerCatList}" var="partnerCategory">
	             					     	 <li>
	             					      		<form:option value="${partnerCategory.prtCatNo}" label="${partnerCategory.partnercategoryName}" />
	             							</li>
	             							</c:forEach> 
	             				</form:select>	
							</fieldset>	
							<input type="hidden" name="prtCatNo_" id="prtCatNo_"/>
							</div> --%>
							<div class="col-md-3">
								<label>Select Offer Category</label><span class="mandatory_star">*</span>
								<input type="text" value="${offerCategoryName}" class="form-control" id="categorySelected" readonly />	
								<!-- <div id="categorySelected-error" style="font:13px;color:red !important;">Please enter Offer Category</div> -->
								<a href="javascript:void(0);" class="selectCategory"  data-toggle="modal" data-target="#categoriesModal">Click here to View Categories</a>
							</div>
							<div class="col-md-3">
								<label>Offers Name/Title</label><span class="mandatory_star">*</span>
								<form:input type='text'  id="offerName" maxlength="50" path="offerName" class="form-control" />
								<input type="hidden" name="offerName_" id="offerName_"/>
								<form:input type="hidden" path="prtOffrNo" id="prtOffrNo"/>
								<input type="hidden" name="prtOffrNo_" id="prtOffrNo_"/>	
							</div>
							<div class="col-md-3">
								<label>Offer Description</label><span class="mandatory_star">*</span>
								<form:input type='text'  id="offerDesc" path="offerDesc" class="form-control" maxlength="100"/>
								<input type="hidden" name="offerDesc_" id="offerDesc_"/>
							</div>
						</div><!--- row --->
						<div class="row pad_top">
							<div class="col-md-3">
								<label>Landing URL</label><span class="mandatory_star">*</span>
								<form:input type='text' maxlength="5000" id="landingUrl" path="landingUrl" class="form-control" />
							</div>
							<%-- <div class="col-md-3">
								<label>Image URL</label><span class="mandatory_star">*</span>
							<form:input type='text' maxlength="250" id="imageUrl" path="imageUrl" class="form-control" />
							</div> --%>
							<div class="col-md-3">
								<label>Sort Order</label>
								<form:input type='text' id="sortOrder" path="sortOrder" class="form-control" maxlength="3"/>
								<input type="hidden" name="sortOrderHidden" id="sortOrderHidden"/>
							</div>
							<div class="col-md-3">
								<label>Alt Text</label><span class="mandatory_star">*</span>
								<form:input type='text' value="${offerAltTxt}" id="altTxt" path="altTxt" class="form-control" maxlength="100"/>
							</div>
							
						</div> <!------  row  ------>
						
						<div class="row pad_top">
							<div class="col-md-4">
								<label>Image URL</label><br>
								<form:input id="uploadFile3" class="upload_file"
									placeholder="Choose File"  path="imageUrl" disabled="true"/>
								<div class="fileUpload btn padTop_Btm">
									<span class="bg_w_button">Browse</span>
									<input id="imageUrl2" name="imageUrl2" type="file"
										class="upload newBannerImageUpload bwrsBtnAddPar" />
								</div>
							</div>

							<div class="col-md-4 image_wrap image_wrap1 noMar-top">
								<img id="offerImage_2" src="${imageurloffer}"
									style="max-height: 100%; max-width: 100%;" />
							</div>
							<div class="col-md-4">
								<input class="bg_g_button" type="button" value="Upload"
									onclick="uploadImage2('imageUrl2','image','offerImageUrl2')" />
								<form:hidden path="imageUrl" id="offerImageUrl2" disabled="disabled" />
								<div id="newBannerError" class="customErrorMessageDisp error"></div>
							</div>
						</div>
						<div class="row pad_top pad_bottom">
							<div class="col-md-3">
								<label>Offer Start Date</label><span class="mandatory_star">*</span>
								<div class="pick_date">
									<form:input type="text" class="form-control startDate" id="datepicker1" path="offerStartDate" readonly="true"/>
								</div>
							</div><!--- datepicker --->
							<div class="col-md-3">
								<label>Offer End Date</label>
								<div class="pick_date">
									<form:input type="text" class="form-control endDate" id="datepicker2" path="offerEndDate" />
								</div>
							</div><!--- datepicker --->
							<div class="col-md-6">
								<label>Offers & Deals Status</label><span class="mandatory_star">*</span>
								<div class="half-col last">
									<ul class="list-inline">
										<c:forEach items="${offersStatus}" var="status">
										<li>
										<label>
											<form:radiobutton path="offersStatus" class="css-checkbox" id ="${status.key}" value="${status.key}" />
											<label for="${status.key}" class="css-label radGroup2">${status.value}</label>
										</label>
										</li>
									</c:forEach>
								</ul>
								</div>
                            </div><!--- radio button --->
						</div> <!------  row  ------> 
						<div class="row pad_top pad_bottom">
						</div> <!------  row  ------>
						<div class="row btn_save pull-right">
							
							<a  href="${contextPath}/admin/offersDeals"><input class="bg_g_button" type="button" value="Cancel" /></a>
							<button type="button" class="bg_g_button" onClick="checkOfferCategory(); $(this).closest('form').submit();">Update</button>
						</div><!------ button ------>
					</div><!----section B---->
					</form:form>
                </div>	<!---- user managemnt----->	 
            </section><!-----section---->            
        </div><!-----container---->


<link rel="stylesheet" href="${contextPath}/static/css/jstree-3.0.1/themes/default/style.min.css">
<script type="text/javascript">
$(document).on('click','.selectCategory',function(){
	var json = $("#getCategoryList").val();
// 	console.log(json);
	var content = JSON.parse(json);	
	$('.select_category').jstree({
			'core' : {
				'data' : content,
				 "check_callback" : true,
			}
	});
});
$(document).on('click','.select_category .jstree-container-ul li a',function(){
	var selected_categoryValue = $('.select_category').jstree().get_selected(true)[0].text;
	$('#categorySelected').val(selected_categoryValue);
	$('#catNoID').val($(".jstree-clicked").attr("catno"));
	checkOfferCategory();
	$('.close').click();
});

$('#categorySelected-error').hide();
function checkOfferCategory(){
	var category = $("#categorySelected").val();
	if(category != ""){
		$('#categorySelected-error').hide();
	} else {
		$('#categorySelected-error').show();
	}
}

var sortOrderExist=false;
var uploadFlag = false;
$('#sectionA').addClass('in active');
$('#sectionB').addClass('active');

$.validator.addMethod("alpha", function(value, element) {
	var re = /^[-./#&+\w\s+\w\']*$/;
    if (re.test(value)) {
    	return true;
    } else {
    	return false;
    }
});

$(document).ready(function () {	
	$("#sortOrderHidden").val($("#sortOrder").val());
	
	 $("#offersDealsBean").validate({
	    ignore:[],
	 	rules: {
	 		prtNo:{
	 			required: true,	 			
	        },
	        categorySelected:{
				required: true,
			},
	        prtCatNo:{
		 		required: true,	 			
		    },
		    altTxt:{
				required: true,
				alpha: true,
				noSpace:true
			},
	 		offerName:{
	 			required: true,
	 			rangelength:[1,100],
	 			noSpace:true
	         },
	         offerDesc:{
		 	    required: true,
				alpha: true,
		 	    rangelength:[1,100],
		 	    noSpace:true
		     },
		     landingUrl:{
			 	required: true,
			 	rangelength:[1,5000],
			 	url:true
			 },
			 imageUrl2:{
				 extension : "png|jpg|jpeg|gif"
			 },
			 offerImageUrl2 : {
					required : true,
					rangelength : [ 1, 250 ]
				}, 
	         sortOrder:{
	 			required: false,	 
	 			number :true,
	 			rangelength:[1, 3],
	 			sortOrderNotZero:true,
	 			duplicatercSortOrder:true,
				sortOrderLessThan100 : true
	 		},	 		
	 		offerStartDate:{
	 			required: true,
	 			startDate:true
	 		},
	 		offerEndDate:{
	 			required: false,
	 			endDate:true,
	 		},
	 	 },

	  messages: {
	
	    prtNo:{
	 		required:"Please select the Partner",	 		
	 	},
	 	prtCatNo:{
	 		required:"Please select the Partner Category",	 		
	 	},
	 	altTxt:{
			required:"Please enter alt text",
			alpha:"Please Enter Valid Special Charaters (Only -.#&')",
			noSpace : "Please enter a valid alt text"
		},
	    offerName:{
			required:"Please enter Offer Name",
			rangelength:"Maximum 100 characters permitted",
			noSpace : "Please enter a valid Offer Name"
	 	},  
	 	offerDesc:{
	 		required:"Please enter Offer Description",
			alpha:"Please Enter Valid Special Charaters (Only -.#&')",
	 		rangelength:"Maximum 100 characters permitted",
	 		noSpace : "Please enter a valid Offer Description"
	 	},
	 	landingUrl:{
	 		required:"Please enter Offer landing Url",
	 	},
	 	imageUrl2:{
	 		extension : "Invalid file format. Only jpg|jpeg|png|gif files are allowed."
	 	},
	 	offerImageUrl2 : {
			required : "Please Upload Offers Image",
			rangelength : "Maximum 100 characters permitted"
		}, 
	 	sortOrder:{
	 		required: "Please enter Sort Order",
	 		rangelength:"Maximum 3 digits permitted",
	 		number :"Please enter numeric data",
	 		sortOrderNotZero:"Sort Order cannot be 0",
	 		duplicatercSortOrder:"Sort Order already exists",
			sortOrderLessThan100: "Sort Order must be less than or equal to 100"
	 	},	 	
	 	offerStartDate:{
	 		required: "Please Select Offer Start Date",
	 		startDate: "* Start Date must be less than or equal to End date"
	 		
	 	},
	 	offerEndDate:{
	 		required: "Please Select Offer End Date",
	 			endDate: "* End Date must be greater than or equal to Start date"
	 		
	 	},		 		 	
	  },
	  errorElement: "div",
	  errorPlacement: function(error, element) {
		    if ($(element).hasClass("form-control startDate")) {
		        error.insertAfter($(element).closest("div"));
		    } else if ($(element).hasClass("form-control endDate")){
		     error.insertAfter($(element).closest("div"));
		    } else {
		        error.insertAfter(element);
		    }
		},
	  submitHandler: function (form) {
		  var category = $("#categorySelected").val();
			if(category != ""){
				$('#categorySelected-error').hide();
			 	form.submit();
			} else {
				$('#categorySelected-error').show();
			}
	 },
	 });  
	 
	 /* for submit the form on key press enter*/
	 $(document).keypress(function(event){
		    if(event.keyCode == 13){
			    $("#offersDealsBean").submit(); 
		    }
		});
	 

	 $.validator.addMethod("endDate", function(value, element) {
		var startDate = getFormattedDate($('#datepicker1').val());
		var endDate = getFormattedDate($('#datepicker2').val());
		if(endDate=='Invalid Date')
			return true;
		if (endDate >= startDate)
			return true;
		if (endDate <= startDate)
			return false;
	});
	 
	 $.validator.addMethod("startDate", function(value, element) {
			var startDate = getFormattedDate($('#datepicker1').val());
			var endDate = getFormattedDate($('#datepicker2').val());
				if(endDate=='Invalid Date')
					return true;
				if (startDate <= endDate)
					return true;
				if (startDate >= endDate)
					return false;
	});
	 
$.validator.addMethod('duplicatercSortOrder', function(value, element) {
	return !sortOrderExist;
});

$.validator.addMethod('sortOrderNotZero', function(value, element) {
	var isValid = false;
	var sortOrder = $("#sortOrder").val();
	
	if((sortOrder != "") && (sortOrder == 0)){
		isValid = true;
	}
	return !isValid; 
});
$.validator.addMethod('sortOrderLessThan100', function(value, element) {
	var isValid = false;
	var sortOrder = $("#sortOrder").val();
	
	if(sortOrder > 100){
		isValid = true;
	}
	return !isValid;
});
$.validator.addMethod("noSpace", function(value, element) { 
    return value == '' || value.trim().length != 0;  
  });
});	

$("#prtNo").on( "selectmenuchange", function(){ 
	var obj=null;		
	var prtNo = $("#prtNo option:selected").val();  
	$("#offerName").val('');
	$("#offersDealsBean").data('validator').resetForm();  
 	$.get("${contextPath}/admin/getODPartnerCategories?prtNo="+prtNo, function(data){
	 var obj = jQuery.parseJSON(data);
	$('#prtCatNo').empty();
	 $('#prtCatNo').append("<option value=''>Select Category</option>"); 
     for(var i = 0; i < obj.length; i++){
        var value = obj[i];
       var newOption = $("<option value="+value.prtCatNo+">"+value.partnercategoryName+"</option>");
       $('#prtCatNo').append(newOption);	       
     }	   
     	$( "#prtCatNo" ).selectmenu( "refresh" );
     	$("#offersDealsBean").validate().element('#prtNo');
});
}); 


$( "#prtCatNo" ).on( "selectmenuselect", function(){
	var prtNo = $("#prtNo option:selected").val();
	  if(prtNo==''){
		  alert("Please select the partner");
		  $("#offerName").val('');
		  $( "#prtNo-button" ).focus();
		  return false;
	  }			 
	});


$( "#offerName" ).click(function() {
	var prtCatNo = $("#prtCatNo option:selected").val();	
	$("#offersDealsBean").data('validator').resetForm();
	  if(prtCatNo==''){
		  alert("Please select the partner category");
		  $("#offerName").val('');
		  $( "#prtCatNo-button" ).focus();
		  return false;
	  }			 
	});	

$('#datepicker1').on('change', function () {
	$("#offersDealsBean").validate().element('#datepicker1');
	//TODO:AW-284
	var endDate = getFormattedDate($('#datepicker2').val());
	
	if(endDate != 'Invalid Date'){
		$("#offersDealsBean").validate().element('#datepicker2');
	}
}); 

$('#datepicker2').on('change', function () {
	$("#offersDealsBean").validate().element('#datepicker2');
	//TODO:AW-284
	$("#offersDealsBean").validate().element('#datepicker1');
}); 
	
$("#prtCatNo").on("selectmenuchange", function(){
	$("#offersDealsBean").validate().element('#prtCatNo');
});	

$('#sortOrder').on('keyup', function () {
	var offersDealsSortOrder = $("#sortOrder").val();
	var offersDealsSortOrderHidden = $("#sortOrderHidden").val();
	sortOrderExist=false;
	
	if(offersDealsSortOrder != offersDealsSortOrderHidden && offersDealsSortOrder != 0){
		$.get("${contextPath}/admin/checkOffersDealSortOrder?offersDealsSortOrder="+offersDealsSortOrder, function(data,status) {
			if(status=="success" && data!=''){
				sortOrderExist=true;
			}
			else{
				sortOrderExist=false;
			}
			$("#offersDealsBean").validate().element('#sortOrder');
		});
	}
	else{
		
		$("#offersDealsBean").validate().element('#sortOrder');
	}
});

function getFormattedDate(date){
	var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
	return new Date(date.replace(pattern,'$3-$2-$1'));
}


function uploadImage2(id, type, url) {
		/* if ($("#offersDealsBean").validate().element("#imageUrl")) { */
			var formData = new FormData();
			formData.append('file', $("#" + id)[0].files[0]);

			/* if ($("#offersDealsBean").validate().element("#" + id)) { */
			
				$.ajax({
					url : "${contextPath}/admin/saveImages?type=" + type,
					data : formData,
					dataType : 'text',
					processData : false,
					contentType : false,
					type : 'POST',

					success : function(response) {
						uploadclick = false;
						$('#uploadFile3').val(response);
						var newImageUrl = '${contextPath}' + '/admin/images/'
								+ type + "/" + response;
						$('#offerImage_2').attr('src', newImageUrl);
						$("#offersDealsBean").validate().element("#" + url);
						$('#offerImageUrl2').val(response);
						//$("#offersDealsBean").validate().element("#imageUrl");
					}
				});
		/* 	}
		} */

	}
  $('#imageUrl2').on('change', function() {
		$('#uploadFile3').val($(this).val());
	}); 
  
  
 /*  $.validator.addMethod('filesize', function(value, element, param) {
		return this.optional(element) || (element.files[0].size <= param)
	}, 'File size should not be more than 1MB');
 */
	/* $.validator.addMethod('imageDimention', function(value, element) {
		var fileUpload = $("#imageUrl")[0];
		alert(fileUpload)
		var bannerType = $("#offersDealsBean input[type='radio']:checked").val();
		checkFileUpload1(fileUpload, bannerType, "newBannerError");
		return uploadFlag;
	}, ''); */
	
	
	$(function() {
	$("#offersDealsBean").bind(
			"change",
			function() {
				//Get reference of FileUpload.
				var fileUpload = $("#imageUrl2")[0];
				var bannerType = $(
						"#offersDealsBean input[type='radio']:checked").val();
				checkFileUpload1(fileUpload, bannerType, "newBannerError");
				if (!uploadFlag)
					return false;
			});
});
	
	function checkFileUpload1(fileUpload, Type, Status) {
		//Check whether the file is valid Image.
		var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:\().+(\)\.def\().+(\])+(.jpg|.jpeg|.png|.gif)$");
		if (regex.test(fileUpload.value.toLowerCase())) {
			notificationEditFlag = true;
			if (typeof (fileUpload.files) != "undefined") {
				var reader = new FileReader();
				reader.readAsDataURL(fileUpload.files[0]);

				reader.onload = function(e) {
					var image = new Image();
					image.src = e.target.result;

					image.onload = function() {

						var height = this.height;
						var width = this.width;
						var errorText = "";
						uploadFlag = false;

						$("#" + Status).css({
							"display" : ""
						});
						$("#" + Status).html("<p>" + errorText + "</p>");

						if (errorText != "") {
							$("#imageUrl2-error").css({
								"display" : "none"
							});
						}
						return uploadFlag;
					};
				};
			}
		} else {
			uploadFlag = false;
			notificationEditFlag = false;
		}
	}
</script>


