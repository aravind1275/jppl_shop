<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:set var="contextPath" value="${pageContext.servletContext.contextPath}"  scope="application"/>
<script>
	function changeURL(){
			history.pushState({}, '',"${contextPath}/admin/offersDeals");
    }
</script>
<!-- Modal contet for delete-->
		<div id="myModalDelete" class="modal fade" role="dialog">	
			<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Are You Sure?</h4>
				</div><!--header-->
				<div class="modal-body">
					<p>
						<img src="${contextPath}/static/img/info_icon1.png"></img>
						<span id="deleteodId" class="bannerNameClass"></span> 
					</p>
				</div><!---body--->
				<div class="modal-footer">
					<button id="btnD" class="bg_g_button" data-dismiss="modal">Deactivate</button>
				</div>
			</div><!---modal content--->
			</div>
		</div>	
		<!-- Select Category Modal Start -->
		<div id="categoriesModal" class="modal fade" role="dialog">	
			<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Select Category</h4>
				</div><!--header-->
				<div class="modal-body">
					<div class="select_category"></div>
				</div><!---body--->
				<div class="modal-footer">
				</div>
			</div><!---modal content--->
			</div>
		</div>
		<!-- Select Category Modal End -->
		
		<!-- Select Category Modal Start -->
		<div id="categoriesModal1" class="modal fade" role="dialog">	
			<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Select Category</h4>
				</div><!--header-->
				<div class="modal-body">
					<div class="select_category1"></div>
				</div><!---body--->
				<div class="modal-footer">
				</div>
			</div><!---modal content--->
			</div>
		</div>
		<!-- Select Category Modal End -->
		<!-- Modal shilpi  for edit-dealsoffer-details-->
 <div id="myModal1" class="modal fade" role="dialog">
	<form:form autocomplete="off" id="editOffersDealBean" commandName="offersDealsBean">
	<form:input type="hidden" path="catNo" id="catNoID1" />
       <form:input type="hidden" path="getCategoryList" id="getCategoryList1" value="${requestScope.categoriesList}" />
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" id="cancelbtnD" class="closeOff"
						data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Edit OffersDealBean Details</h4>
				</div>
				<!----modal-header--->
				<div class="modal-body1">
					<div class="row pad_top">
						<div class="col-md-6 user_details">
							<label>LmdId</label>
						<input type="text" class="form-control " name="edit_lmdid" maxlength="50" id="edit_lmdid" readonly="readonly">
						<input type="hidden" id="edit_lmdidHidden" />
						</div>

						<div class="col-md-6 user_details">
							<label>Store</label>
							<input type="text" class="form-control " name="edit_store" maxlength="300" id="edit_store" readonly="readonly">
							
						</div>
						
							<div class="col-md-6">
									<label>Select A Partner</label><span class="mandatory_star">*</span>
									<fieldset class="infield">
										<form:select path="prtNo" id="prtNo" class="select_option">          
											<form:option value="">Select Partner</form:option>
             	 					 <c:forEach items="${partnerList}" var="partner">
	             					      <li>
	             					      <form:option value="${partner.prtNo}" label="${partner.partnerName}" />
	             						</li>
	             					</c:forEach> 
										</form:select>
									</fieldset>	
								</div>
						
					</div>
					<!----row--->
					<div class="row pad_top">
						<div class="col-md-6 user_details">
							<label>Text</label>
							<input type="text" class="form-control " name="edit_text" maxlength="3000" id="edit_text">
						</div>
						

						<div class="col-md-6 user_details">
							<label>Title</label>
							<input type="text" class="form-control " name="edit_title" id="edit_title">
						</div>
					</div>
					<div class="row pad_top">
					<div class="col-md-6 user_details">
							<label>Description</label>
							<input type="text" class="form-control " name="edit_description" id="edit_description" maxlength="3000">
							 
						</div>
						<div class="col-md-6 user_details">
							<label>category</label>
							<input type="text" class="form-control " name="edit_category" maxlength="3000" id="edit_category" readonly="readonly">
						</div>
						
						<div class="col-md-6">
									<label>Select Offer Category</label><span class="mandatory_star">*</span>
									<input type="text" class="form-control" id="categorySelected1" readonly />	
									<div id="categorySelected-error1" style="font:13px;color:red !important;">Please enter Offer Category</div>
									<a href="javascript:void(0);" class="selectCategory1"  data-toggle="modal" data-target="#categoriesModal1">Click here to View Categories</a>
								</div>
					</div>
					<div class="row pad_top">
						<div class="col-md-6 browse_btn">
							<label>Image</label><br>
                            <input id="edit_imageurl" class="upload_file upload_file_style"
								placeholder="Choose File" disabled="disabled" /> <input
								type="hidden" id="edit_imageurlHidden" />
							<div class="fileUpload btn padTop_Btm">
								<span class="bg_w_button">Browse</span> <input
									name="edit_image" id="edit_image" type="file"
									class="upload upload_btn_modal editBannerImageUpload bwrsBtnAddPar" />
							</div>
						</div>

						<div class="col-md-4 image_wrap image_wrap1"
							style="width: 250px; height: 115px; margin-top: 10px;">
							<img id="edit_image_"  src=""
								style="max-height: 100%; max-width: 100%;">
						</div>
						<div class="col-md-1 mar_top">
							<input class="bg_g_button" type="button" value="Upload"
								onclick="editUploadImage('edit_image','image','edit_imageurl')" />
							<input type="hidden" name="editImageUrl"
								id="editImageUrl" />
							<div id="editBannerError" class="customErrorMessageDisp error"></div>
						</div>
					</div>
					<!------------- row ----------->
					<div class="row pad_top">
						<div class="col-md-4 user_details">
							<label>Start Date</label>
							 <div class="pick_date">
							 	<input type="text" class="form-control datepicker" id="editFromDate" name="editFromDate" readonly="readonly"/>
							</div> 
						</div>
						<div class="col-md-4 user_details">
							<label>End Date</label>
						   	<div class="pick_date">
								<input  type="text" class="form-control datepicker" id="editToDate" name="editToDate" readonly="readonly" />
							</div>
						</div>
					</div>
				  </div>
				<div class="row btn_save align_right">
					<input class="bg_g_button" type="button" value="Update" id="updateOffersDealsLink"/>
				</div>
				<!-------------button----------->
			</div>
		</div>
	</form:form>
</div> 
        <div class='container'>
            
            <section class="insurance_management offersDeals">
                <div id ="breadcrumb">
                    <ul>
                        <li>Home</li>
						<li>Content Management</li>
						<li>Offers & Deals</li>
                    </ul>
                </div>  <!---breadcrumb--->
                <c:if test="${fn:length(message) gt 0}"><script>changeURL();</script><div class="alertMessage" style="color:green;font-weight: bold;"  ><c:out value="${message}"></c:out></div> </c:if>
	  			<c:if test="${fn:length(errormessage) gt 0}"><script>changeURL();</script><div class="alertMessage" style="color:red;font-weight: bold;"  ><c:out value="${errormessage}"></c:out></div> </c:if>
				<div class="section_heading">
					<h5>Offers & Deals</h5> 
				</div><!--- section heading ---->
				<div class="management"><!---User Management--->
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#sectionA">Offers & Deals</a></li>
                        <li><a data-toggle="tab" href="#sectionB">Define Offers & Deals</a></li>
                        <li><a data-toggle="tab" href="#sectionC">Upload Offers & Deals</a></li>
                        <li><a data-toggle="tab" href="#sectionD"> Offers & Deals Link</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="sectionA" class="tab-pane fade in active">
                        
                            <nav class="navbar navbar-default" role="navigation" id="submenu">
                             <span id="requiredField" style="color:red;font-weight: bold;display:none;">Please select Partner or Offer</span>
                           
                            <form:form autocomplete="off" action="searchOfferDeals" id="offersDealsBean_" commandName="offersDealsBean_" >
							<div class="row pad_top offers_deal">
								<div class="col-md-4">
									<label>Select A Partner</label>
									<fieldset class="infield">
										<form:select name="speed" path="prtNo" id="prtNo_" class="select_option">
											<form:option value="">--Select Partner--</form:option>
											<c:forEach items="${partnerList}" var="partner">
	               					     	 <li>
	             					     	 <form:option value="${partner.prtNo}" label="${partner.partnerName}" />
	             							</li>
	             						</c:forEach>
										</form:select>
									</fieldset>	
								</div>
								<div class="col-md-4">
									<label>Search By</label>
									<fieldset class="infield">
										<form:select name="speed" path="searchByName" id="searchByName" class="select_option">
											<form:option value="" selected="selected">--Select SearchBy--</form:option>
											<form:option value="OffrName">Offer Name/Title</form:option>
	             							<form:option value="OffrDesc">Offer Description</form:option>
	             					 		<form:option value="CatName">Category Name</form:option>
										</form:select>
									</fieldset>	
								</div>
								<div class="col-md-2 ">
									<label>Enter Value</label>
									<form:input type='text'  id="searchByValue" path="searchByValue" class="form-control" placeholder=" What's your Wish?" />
									<form:input type="hidden" path="catNo" id="offercatNo"/>	
	             					<form:input type="hidden" path="prtOffrNo" id="prtOffrNo"/>	
								</div>
								<div class="col-md-2 mar_top">
									<button type="button" id="serachBtn" class="bg_g_button">Search</button>
								</div>
                            </div><!--- row --->
                            
                            <div class="row pad_top offers_deal">
                            	<div class="col-md-12">
		                            <c:if test="${empty offersDealsList}">
			                            <div class="alertMessage" style="color:green;font-weight:bold;">
			                            	No Offers & Deals Found
			                            </div>
		                            </c:if>
	                            </div>
	                        </div> 
                            
                            </form:form>
                            
                          <%--   <c:if test="${not empty offersDealsList}"> --%>
                            
                            <table id="viewOffersTable" class=" dataTables partner_info_table table-bordered" cellspacing="0" cellpadding="0" width="98%">
								<thead>
									<tr>
									    <th>Offer No</th>
										<th>Offer Name/Title</th>
										<th>Offer Description</th>
										<th>Category Name</th>
										<th>Sort Order</th>
										<th>Offer Start Date</th>
										<th>Offer End Date</th>  
										<th>Status</th>
										<th class="width_section">Action</th>
									</tr>
								</thead><!----thead--->
								<%-- <tbody>
									<c:forEach items="${offersDealsList}" var="offer" varStatus="i">
									<tr class="admin" id="1.${offer.prtOffrNo}" data-id="${offer.prtOffrNo}" data-offername="${offer.offerName}">
										<td class="slno_width">${i.count}</td>									
										<td class="td_width">${offer.offerName}</td>
										<td class="td_width">${offer.offerDesc}</td>
										<td class="td_width">${offer.categoryName}</td>
										<td class="td_width">${offer.sortOrder}</td>
										<td class="td_width">${offer.offerStartDate}</td>
										<td class="td_width">${offer.offerEndDate}</td>
 										<td class="${offer.offersStatusClass} td_width">${offer.offersStatus}</td>
										<td>
											<a class="editOffer" href="javascript:void(0);">
												<img class="edit_wraper" src="${contextPath}/static/img/edit_icon.png">
											</a>
											<span class="delete btnDelete" data-target="#myModalDelete" id="deleteOffersDeals">
												<img src="${contextPath}/static/img/delete_icon1.png"  class="fa fa-trash fa-lg"></img>
											</span>
										</td>
									</tr> 
									</c:forEach>
								</tbody><!---tbody---> --%>
							</table><!---table-->	
						
                            </nav>
                        </div><!----section A---->
                        
                        <!-- Modal content for Create New Offers & Deals Tab----------------------------------->
                        <div id="sectionB" class="tab-pane fade submenu" >
                        <form:form autocomplete="off" action="offersDeals" commandName="offersDealsBean">
                        <form:input type="hidden" path="catNo" id="catNoID" />
                        <form:input type="hidden" path="getCategoryList" id="getCategoryList" value="${requestScope.categoriesList}" />
                            <div class="row pad_top">
								<div class="col-md-3">
									<label>Select A Partner</label><span class="mandatory_star">*</span>
									<fieldset class="infield">
										<form:select path="prtNo" id="prtNo" class="select_option">          
											<form:option value="">Select Partner</form:option>
             	 					 <c:forEach items="${partnerList}" var="partner">
	             					      <li>
	             					      <form:option value="${partner.prtNo}" label="${partner.partnerName}" />
	             						</li>
	             					</c:forEach> 
										</form:select>
									</fieldset>	
								</div>
								<div class="col-md-3">
									<label>Select Offer Category</label><span class="mandatory_star">*</span>
									<input type="text" class="form-control" id="categorySelected" readonly />	
									<div id="categorySelected-error" style="font:13px;color:red !important;">Please enter Offer Category</div>
									<a href="javascript:void(0);" class="selectCategory"  data-toggle="modal" data-target="#categoriesModal">Click here to View Categories</a>
								</div>
								<div class="col-md-3">
									<label>Offers Name/Title</label><span class="mandatory_star">*</span>
									<form:input type='text' id="offerName" path="offerName" maxlength="50" placeholder=" Enter your Offer Name!" class="form-control" />
									<form:input type="hidden" path="prtOffrNo" id="prtOffrNo"/>
								</div>
								<div class="col-md-3">
									<label>Offer Description</label><span class="mandatory_star">*</span>
									<form:input type='text' id="offerDesc" path="offerDesc"	class="form-control" maxlength="100"/>
								</div>
                            </div><!--- row --->
                            
							<div class="row pad_top">
								<div class="col-md-3">
									<label>Landing URL</label><span class="mandatory_star">*</span>
									<form:input type='text' maxlength="5000" id="landingUrl" path="landingUrl"	class="form-control" />									
								</div>
								
								<div class="col-md-3">
									<label>Sort Order</label>
									<form:input type='text' id="sortOrder" path="sortOrder" class="form-control" maxlength="3"/>
								</div>
								<div class="col-md-3">
									<label>Alt Text</label><span class="mandatory_star">*</span>
									<form:input type='text' id="altTxt" path="altTxt" class="form-control" maxlength="100"/>
								</div>
							</div> <!------  row  ------>
							
							<div class="row pad_top">
							<div class="col-md-4">
								<label>Image URL</label><br>
								<input id="uploadFile" class="upload_file"
									placeholder="Choose File" disabled="disabled" />
								<div class="fileUpload btn padTop_Btm">
									<span class="bg_w_button">Browse</span>
									<input id="imageUrl1" name="imageUrl1" type="file"
										class="upload newBannerImageUpload bwrsBtnAddPar" />
								</div>
							</div>

							<div class="col-md-4 image_wrap image_wrap1 noMar-top">
								<img id="offerImage_" src=""
									style="max-height: 100%; max-width: 100%;" />
							</div>
							<div class="col-md-4">
								<input class="bg_g_button" type="button" value="Upload"
									onclick="uploadImage('imageUrl1','image','offerImageUrl')" />
								<form:hidden path="imageUrl" id="offerImageUrl" />
								<div id="newBannerError" class="customErrorMessageDisp error"></div>
							</div>
						</div>
							<div class="row pad_top pad_bottom">
								<div class="col-md-3">
									<label>Offer Start Date</label><span class="mandatory_star">*</span>
								<div class="pick_date offerDeals">
										<form:input type="text" class="form-control startDate" id="datepicker1" path="offerStartDate" readonly="true"/>
										<!--img src="./img/2016-06-23.png" alt="" ></img-->
									</div>
								</div>
								<div class="col-md-3">
									<label>Offer End Date</label>
									<div class="pick_date">
										<form:input type="text" class="form-control endDate" id="datepicker2" path="offerEndDate" readonly="true"/>
									</div>
								</div><!--- datepicker --->
								<div class="col-md-6">
									<label>Offers & Deals Status</label><span class="mandatory_star">*</span>
									<div class="half-col last">
										<ul class="list-inline">
											<c:forEach items="${offersStatus}" var="status">
											<li>
											<label>
												<form:radiobutton path="offersStatusCode" class="css-checkbox" id ="${status.key}" value="${status.key}" />
												<label for="${status.key}" class="css-label radGroup2">${status.value}</label>
											</label>
											</li>
										</c:forEach>
									</ul>
									</div>
                                </div><!--- radio button --->
							</div> <!------  row  ------>
							<div class="row pad_top pad_bottom">
							</div> <!------  row  ------>
							
                            <div class="row btn_save pull-right">
								<a  href="${contextPath}/admin/offersDeals"><button type="button" class="bg_g_button" id="cancelbtnD">Cancel</button></a>
								<button type="button" class="bg_g_button" onClick="checkOfferCategory(); $(this).closest('form').submit();">Save</button>
							</div><!------ button ------>
							</form:form>
                        </div><!----section B---->
                        
                        <!----section C starts---->
						<div id="sectionC" class="tab-pane fade submenu mapKeyword">
							<div class="row pad_top">
								<form id="uploadCSVForm" name="uploadCSVForm" method="POST" action="offersAndDealsFileUpload" enctype="multipart/form-data">
									<div class="row">
										<div class="col-md-3 upload_keyword">
											<label>Upload Offers & Deals File</label><span class="mandatory_star">*</span>
											<div class="fileUpload btn ">
												<input name="uploadFile2" id="uploadFile2" class="upload_file"
												 placeholder=".csv file only" disabled="disabled" />
												<span class="bg_w_button">Browse</span>
												<input type="file" name="uploadCSVFile" id="uploadCSVFile" class="upload uploadfile_button"/>
												<input type="hidden" id="filename" name="filename" value="offersAndDeals"/>
											</div>
											<!-- <div class="fileUpload btn ">
												<input class="bg_g_button" type="submit" value="Upload" />
											</div> -->
										</div>
										<div class="col-md-5">
										<div style="opacity:0;">empty div</div>
											<div class="fileUpload btn ">
												<input class="bg_g_button" type="submit" id="UploadFile" value="Upload" />
											</div>
										</div>
									</div>
								</form>
							</div>
							<!--- row --->
							<table class=" partner_info_table table-bordered" cellspacing="0" cellpadding="0" width="98%" id="table-1">
								<thead>
									<tr>
										<th style="width:10%;">Sl No.</th>
										<th style="width:80%;">Uploaded Files</th>
										<!-- <th>Date</th>
										<th class="width_section">Action</th> -->
									</tr>
								</thead>
								<!----thead--->
								<tbody>
									<c:forEach items="${uploadSuccessFiles}" var="uploadSuccessFile" varStatus="i">
										<tr class="admin" id="${i.count}" data-filepath="${offersDealsFilePath}">
											<td>${i.count}</td>
											<td>
												<a class="dwldFile" href="/afadmin/admin/download?filePath=${offersDealsFilePath}${uploadSuccessFile}">
													${uploadSuccessFile}</a>
											</td>
											<!-- <td class="td_width"></td> -->
											<%-- <td class="td_width">
												<span class="delete btnDelete" data-target="#deleteFileModal" id="deleteFile">
													<img src="${contextPath}/static/img/delete_icon1.png" class="fa fa-trash fa-lg"></img>
												</span>
											</td> --%>
										</tr>
									</c:forEach>
								</tbody><!---tbody--->
							</table><!---table-->
						</div><!----section C Ends---->
						
						<!----section D Start---->
                          	<div id="sectionD" class="tab-pane fade submenu mapKeyword">
                          	  <form:form autocomplete="off" action="allOffersDeals" id="offersBean" commandName="offersBean">
                          	 
                          	 <div class="row pad_top export_btn padTop pull-left owd" id="radioType">
                          	 
                          	  		  <input type="radio" id="newRecord" name="radioButton" value="newRecord" />
                                      <label for="newRecord">New Record</label>
                                      
                                      <input type="radio" id="approved" name="radioButton" value="approved" />   
                                      <label for="approved">Approved</label>
                                     
                                      <input type="radio" id="pending" name="radioButton" value="pending" />   
                                     <label for="pending">Pending</label>
                                    
                              </div>
                             <div class="row btn_save pull-right">
								<button type="button" class="bg_g_button" id="all"  >Fetch All Record</button>
								<!--  <button type="button" class="bg_g_button" id="one" >Fetch New Record</button>  -->
								  <button type="button" class="bg_g_button" id="add" >Add</button> 
							<a  href="/afadmin/admin/exportOfferDealLinkCsv " class="bg_w_button odexp" id="export">export to csv</a>
							</div>
                            <table id="dtable" class=" table-bordered" width="100%">
								<thead>
									<tr>
										<th>Active</th>
										<th>partnerMapName</th>
										<th>lmd_id </th>
										<th>Store</th>
										<th>Text</th>
										<th>Title</th>
										<th>Description </th>
										<th>Categories </th>  
										<th>Start_date</th>
										<th>End_date</th>
										<th>Status</th>
									   <th>Action</th> 
									</tr>
								</thead>
								
							</table>
							
							
							<table id="dtable1" class=" table-bordered" width="100%">
								<thead>
									<tr>
										<th>Active</th>
										<th>partnerMapName</th>
										<th>lmd_id </th>
										<th>Store</th>
										<th>Text</th>
										<th>Title</th>
										<th>Description </th>
										<th>Categories </th>  
										<th>Start_date</th>
										<th>End_date</th>
										<th>Status</th>
									   <th>Action</th> 
									</tr>
								</thead>
								
							</table>
							<table id="dtableApproved" class=" table-bordered" width="100%">
								<thead>
									<tr>
										<th>Lmd ID</th>
										<th>Store</th>
										<th>Title</th>
										<th>Description</th>
										<th>Categories</th>  
										<th>Start_date</th>
										<th>End_date</th>
										<th>Status</th>
									</tr>
								</thead>
								
							</table>
								<table id="dtable3" class=" table-bordered" width="100%">
								<thead>
									<tr>
									 <th>Active</th>
										<th>lmd_id </th>
										<th>Store</th>
										<th>Text</th>
										<th>Title</th>
										<th>Description </th>
										<th>Categories </th>  
										<th>Start_date</th>
										<th>End_date</th>
										<th>Status</th>
									   <th>Action</th>
									</tr>
								</thead>
								
							</table>
							<table id="dtableNewRecord" class=" table-bordered" width="100%">
								<thead>
									<tr>
										<th>Active</th>
										<th>lmd_id </th>
										<th>Store</th>
										<th>Text</th>
										<th>Title</th>
										<th>Description </th>
										<th>Categories </th>  
										<th>Start_date</th>
										<th>End_date</th>
										<th>Status</th>
									   <th>Action</th>
									</tr>
								</thead>
								
							</table>
							
							</form:form>
                          	</div>
                          	
                          	
                          	
                    </div><!-----tab-content---->
                </div>	<!---- user managemnt----->
            </section><!-----section---->
            
        </div><!-----container---->

<link rel="stylesheet" href="${contextPath}/static/css/jstree-3.0.1/themes/default/style.min.css">
<script type="text/javascript">
$("#uploadCSVForm").validate({
	ignore:[],	
	rules: {
		uploadCSVFile:{
			required: true,
			extension: "csv"
		}
	},
	messages: {
		uploadCSVFile:{
			required: "Please upload csv file",
			extension: "File format shoud be .csv"
		}
	},
	errorElement: "div",
	errorPlacement: function(error, element) {
		if ($(element).hasClass("uploadBtn2")){
	    	error.insertAfter($(element).closest("div"));
		} else {
	    	error.insertAfter(element);
		}
	},
	submitHandler: function (form) {
		form.submit();
	},
});
$('#uploadCSVFile').on('change', function() {
	//var filename = $(this).val();
	$('#uploadFile2').val($(this).val());

	$("#uploadCSVForm").validate().element('#uploadCSVFile');
});
$(document).on('click','.selectCategory',function(){
	var json = $("#getCategoryList").val();
// 	console.log(json);
	var content = JSON.parse(json);	
	$('.select_category').jstree({
			'core' : {
				'data' : content,
				 "check_callback" : true,
			}
	});
});

$(document).on('click','.selectCategory1',function(){
	var json = $("#getCategoryList1").val();
// 	console.log(json);
	var content = JSON.parse(json);	
	$('.select_category1').jstree({
			'core' : {
				'data' : content,
				 "check_callback" : true,
			}
	});
});
$(document).on('click','.select_category .jstree-container-ul li a',function(){

	var selected_categoryValue = $('.select_category').jstree().get_selected(true)[0].text;
	$('#categorySelected').val(selected_categoryValue);
	$('#catNoID').val($(".jstree-clicked").attr("catno"));
	checkOfferCategory();
	$('.close').click();
});
$(document).on('click','.select_category1 .jstree-container-ul li a',function(){

	var selected_categoryValue = $('.select_category1').jstree().get_selected(true)[0].text;
	$('#categorySelected1').val(selected_categoryValue);
	$('#catNoID1').val($(".jstree-clicked").attr("catno"));
	checkOfferCategory1();
	$('.close').click();
});
</script>

<!-- Variables -->
<script type="text/javascript">
var sortOrderExist=false;
var searchByValueInvalid=false;
var uploadFlag = false;
$('#categorySelected-error').hide();
$('#categorySelected-error1').hide();

function checkOfferCategory(){
	var category = $("#categorySelected").val();
	if(category != ""){
		$('#categorySelected-error').hide();
	} else {
		$('#categorySelected-error').show();
	}
}
function checkOfferCategory1(){
	var category = $("#categorySelected1").val();
	if(category != ""){
		$('#categorySelected-error1').hide();
	} else {
		$('#categorySelected-error1').show();
	}
}
$(function() {
	$("#editOffersDealBean").bind(
			"change",
			function() {
				//Get reference of FileUpload.
				var fileUpload = $("#edit_image")[0];
				var Type = $(
						"#editOffersDealBean input[type='radio']:checked")
						.val();
				checkFileUpload(fileUpload, Type, "editBannerError");
				if (!uploadFlag)
					return false;
			});
});
//shilpi
function editUploadImage(id, type, url) {
	if ($("#editOffersDealBean").validate().element("#edit_image")) {
		var formData = new FormData();
		formData.append('file', $("#" + id)[0].files[0]);

		if ($("#editOffersDealBean").validate().element("#" + id)) {
			$.ajax({
				url : "${contextPath}/admin/saveImages?type=" + type,
				data : formData,
				dataType : 'text',
				processData : false,
				contentType : false,
				type : 'POST',

				success : function(response) {
					$('#' + url).val(response);
					var newImageUrl = '${contextPath}' + '/admin/images/'
							+ type + "/" + response;
					$('#edit_imageurlHidden').val(
							$("#edit_imageurl").val());
					$('#edit_image_').prop('src', newImageUrl);
					$("#editOffersDealBean").validate().element("#" + url);
				}
			});
		}
	}
}
function checkFileUpload(fileUpload, Type, Status) {
	//Check whether the file is valid Image.
	var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:\().+(\)\.def\().+(\])+(.jpg|.jpeg|.png|.gif)$");

	if (regex.test(fileUpload.value.toLowerCase())) {
		notificationEditFlag = true;
		//Check whether HTML5 is supported.
		if (typeof (fileUpload.files) != "undefined") {
			//Initiate the FileReader object.
			var reader = new FileReader();
			//Read the contents of Image File.
			reader.readAsDataURL(fileUpload.files[0]);

			reader.onload = function(e) {
				//Initiate the JavaScript Image object.
				var image = new Image();
				//Set the Base64 string return from FileReader as source.
				image.src = e.target.result;

				image.onload = function() {

					//Determine the Height and Width.
					var height = this.height;
					var width = this.width;

					var errorText = "";
					uploadFlag = false;

					/* if (Type == 0) {
						if (width == 1366 && height == 438) {
							uploadFlag = true;
						} else {
							uploadFlag = false;
							errorText = "Height and Width must be 438 and 1366 for Notification Image.";
						}
					} 
					else {
						if (width == 1366 && height == 438) {
							uploadFlag = true;
						} else {
							uploadFlag = false;
							errorText = "Height and Width must be 438 and 1366 for Notification Image.";
						}
					}  */

					$("#" + Status).css({
						"display" : ""
					});
					$("#" + Status).html("<p>" + errorText + "</p>");

					if (errorText != "") {
						$("#bannerImageUrl-error").css({
							"display" : "none"
						});
					}
					return uploadFlag;
				};
			};
		}
	} else {
		notificationEditFlag = false; 
		uploadFlag = false;
	}
}




</script>



<!-- Form Validation -->
<script type="text/javascript">
$("#serachBtn").on('click',function(){
	/*var	prtNo=$("#prtNo_").val();
	var	searchByName=$("#searchByName").val();
	var	searchByValue=$("#searchByValue").val();
	
	if(prtNo == "" && searchByName == "" && searchByValue == ""){
		 $("#requiredField").css('display', 'block');
		//alert("Please select Partner or Offer");
	}
	else{
		$("#offersDealsBean_" ).submit();
	}*/
	
	$("#offersDealsBean_" ).submit();
	
});
/* $("#all").on('click',function(){
	$("#offersBean" ).submit();
	
}); */
$(document).ready(function () {
	var d = new Date(); // for now
	var h = d.getHours();
	h = (h < 10) ? ("0" + h) : h ;
	var m = d.getMinutes();
	m = (m < 10) ? ("0" + m) : m ;
	var s = d.getSeconds();
	s = (s < 10) ? ("0" + s) : s ;
	var month = d.getMonth()+1;
	var day = d.getDate();
	var year = d.getFullYear();
	datetext = day+"-"+month+"-"+year+"_"+h+":"+m+":"+s;
	
	jQuery.extend( jQuery.fn.dataTableExt.oSort, {
	    "non-empty-string-asc": function (str1, str2) {
	    	str1 = Number($.trim(str1));
	    	str2 = Number($.trim(str2));
	        if(str1 == null || str1 == "")
	            return 1;
	        if(str1 == null || str2 == "")
	            return -1;
	        return ((str1 < str2) ? -1 : ((str1 > str2) ? 1 : 0));
	    },
	    "non-empty-string-desc": function (str1, str2) {
	    	str1 = Number($.trim(str1));
	    	str2 = Number($.trim(str2));
	        if(str1 == null || str1 == "")
	            return 1;
	        if(str1 == null || str2 == "")
	            return -1;
	        return ((str1 < str2) ? 1 : ((str1 > str2) ? -1 : 0));
	    }
	} );
	
	/* $('.dataTables').DataTable( {
         "paging": true,
         "searching": false,
         "lengthChange": false,
         "info":false,
         "iDisplayLength" : 100,
         "order": [[ 0, "asc" ]],
         "dom": 'Blfrtip',
         columnDefs: [
              {type: 'non-empty-string', targets: 4} // define 'name' column as non-empty-string type
         ],
     	 "searching": false,
		 "buttons": [
              { extend: 'excel', text:'export to excel',title:'view_Offers_and_deals_'+datetext},
         ],
     }); */
	
	$("#offersDealsBean_").validate({
		ignore:[],
		rules: {
			prtNo:{
				requiredPrtNo: true,	 			
			},
			searchByName:{
				requiredName: true,	 			
			},
			searchByValue:{
				requiredValue: true,
				searchByValueInvalid:true,
			} 
		},
		messages: {
			prtNo:{
				requiredPrtNo:"Please select the Partner",	 		
			},		    
			searchByName:{
				requiredName: "Please select Search By Name",	 		
			},
			searchByValue:{
				requiredValue:"Please Enter Offer Name/Offer Description/Category Name",
				searchByValueInvalid:"Invalid Search, Please select from dropdown",
			}
		},
		errorElement: "div",
		submitHandler: function (form) {		  
			form.submit();
		}
	});
});

$.validator.addMethod("alpha", function(value, element) {
	var re = /^[-./#&+\w\s+\w\']*$/;
    if (re.test(value)) {
    	return true;
    } else {
    	return false;
    }
});
	
$(document).ready(function () {
	
	$("#offersDealsBean").validate({
		ignore:[],
		rules: {
			prtNo:{
				required: true,	 			
			},
			prtCatNo:{
				required: true,	 			
			},
			offerName:{
				required: true,
				rangelength:[1,100],
				noSpace:true
			},
			categorySelected:{
				required: true,
			},
			offerDesc:{
				required: true,
				rangelength:[1,100],
				alpha: true,
				noSpace:true
			},
			landingUrl:{
				required: true,
				rangelength:[1, 5000],
				url:true
			},
			imageUrl1:{
				extension : "png|jpg|jpeg|gif",
			},
			 offerImageUrl : {
				required : true,
				rangelength : [ 1, 250 ]
			}, 
			sortOrder:{
				required: false,	
				number : true,
				rangelength:[1, 3],
				sortOrderNotZero:true,
				duplicatercSortOrder:true,
				sortOrderLessThan100 : true
			},
			altTxt:{
				required: true,
				alpha: true,
				noSpace:true
			},
			offerStartDate:{
				required: true,
				startDate: true
			},
			offerEndDate:{
				required: false,
				endDate:true
			}
		},
		messages: {
			prtNo:{
				required:"Please select the Partner",	 		
			},
			prtCatNo:{
				required:"Please select the Partner Category",	 		
			},
			offerName:{
				required:"Please enter Offer Name",
				rangelength:"Maximum 100 characters permitted",
				AlphaNumeric:"Special charachters are not allowed",
				noSpace : "Please enter a valid Offer Name"
			},
			offerDesc:{
				required:"Please enter Offer Description",
				rangelength:"Maximum 100 characters permitted",
				alpha:"Please Enter Valid Special Charaters (Only -.#&')",
				noSpace : "Please enter a valid  Offer Description"
			},
			altTxt:{
				required:"Please enter alt text",
				alpha:"Please Enter Valid Special Charaters (Only -.#&')",
				noSpace : "Please enter a valid alt text"
			},
			landingUrl:{
				required:"Please enter Offer Landing Url",
			},
			imageUrl1:{
				extension : "Invalid file format. Only jpg|jpeg|png|gif files are allowed."
			},
			offerImageUrl : {
				required : "Please Upload Offers Image",
				rangelength : "Maximum 100 characters permitted"
			}, 
			sortOrder:{
				required: "Please enter Sort Order",
				rangelength:"Maximum 3 digits permitted",
				number : "Please enter numeric data",
				sortOrderNotZero:"Sort Order cannot be 0",
				duplicatercSortOrder:"Sort Order already exists",
				sortOrderLessThan100: "Sort Order must be less than or equal to 100"
			},	 	
			offerStartDate:{
				required: "Please select Offer Start Date",
				startDate: "* Start Date must be less than or equal to End date"
			},
			offerEndDate:{
				required: "Please select Offer End Date",
				endDate: "* End Date must be greater than or equal to Start date"
			}		 		 	
		},
		errorElement: "div",
		errorPlacement: function(error, element) {
			if ($(element).hasClass("form-control startDate")) {
				error.insertAfter($(element).closest("div"));
			} 
			else if ($(element).hasClass("form-control endDate")){
				error.insertAfter($(element).closest("div"));
			}
			else {
				error.insertAfter(element);
			}
		},
		submitHandler: function (form) {
			form.submit();
		}
	});
});

</script>

<!-- Events -->
<script type="text/javascript">
/* $('span#deleteOffersDeals').on('click', function () { */
	$('#viewOffersTable').on('click', 'a.delete', function (e) {	 
		var checka = bannerPagination.row( $(this).closest('tr')).data();
	  	id = checka.id;
	var id = bannerPagination.row( $(this).closest('tr')).data().prtOffrNo;
	offerName=bannerPagination.row( $(this).closest('tr')).data().offerName;	
	$('#deleteodId').text(offerName+" "+ " Offer will be deleted. Do you want to continue?");
	$('#myModalDelete').data('id', id).modal('show');
});
	var id='', offerName='',prtOffrNo='';
/* $('.editOffer').on('click', function(){ */
	 $('#viewOffersTable').on('click', 'a.editOffer', function (e) {
	var checka = bannerPagination.row( $(this).closest('tr')).data();
  	id = checka.id;
	prtOffrNo =  bannerPagination.row( $(this).closest('tr')).data().prtOffrNo;
	var requestPrtNo = "${requestPrtNo}";
	var requestSearchByName = "${requestSearchByName}";
	var requestSearchByValue = "${requestSearchByValue}";
	var requestCatNo = "${requestCatNo}";
	
	var offerUpdateData = {"prtOffrNo" : prtOffrNo, "requestPrtNo" : requestPrtNo, "requestSearchByName" : requestSearchByName, "requestSearchByValue" : requestSearchByValue, "requestCatNo" : requestCatNo};
	//alert("${contextPath}/admin/updateOffersDeals?"+$.param(offerUpdateData));
	window.location.href= "${contextPath}/admin/updateOffersDeals?"+$.param(offerUpdateData);
});

$('#btnD').click(function () {
	var checka = bannerPagination.row( $(this).closest('tr')).data();

	var id = $('#myModalDelete').data('id');
	var prtNo = $('#prtNo_').val();
	var searchByName = $('#searchByName').val();
	var searchByValue = $('#searchByValue').val();
	var catNo = $('#offercatNo').val();
	
	$.get("${contextPath}/admin/deleteSelectedOffer?prtOffrNo="+id , function(status) {		
		if(status=="success" ){
			$('[id=' + id + ']').remove();
			$('#myModalDelete').modal('hide');
			window.location.href="${contextPath}/admin/searchOfferDeals?deleteMsg="+status+"&prtNo="+prtNo+"&searchByName="+searchByName+"&searchByValue="+searchByValue+"&catNo="+catNo;	
		}
		if(status=="failure" ){
			$('[id=' + id + ']').remove();
			$('#myModalDelete').modal('hide');
			window.location.href="${contextPath}/admin/searchOfferDeals?deleteMsg="+status+"&prtNo="+prtNo+"&searchByName="+searchByName+"&searchByValue="+searchByValue+"&catNo="+catNo;
		}
	});
});

$('#searchByValue').on('change', function () {
	var searchByName = $("#searchByName").val();
	var prtOffrNo = $("#prtOffrNo").val();
	
	if(searchByName != ""){
		if(prtOffrNo == ""){
			searchByValueInvalid = true;
		}
		else{
			searchByValueInvalid = false;
		}
	}
	
	$("#offersDealsBean_").validate().element('#searchByValue');
});

$("#searchByName").on( "selectmenuchange", function(){ 
	$("#requiredField").css('display', 'none');
	$("#searchByValue").val('');
	$("#prtOffrNo").val('');
	var prtNo = $("#prtNo_ option:selected").val(); 
	var searchByName = $("#searchByName option:selected").val();
	
	if(searchByName == ""){
		searchByValueInvalid = false;
		$("#offersDealsBean_").validate().element('#searchByValue');
	}
	$("#offersDealsBean_").validate().element('#searchByName');
});

$( "#searchByValue" ).click(function() {
	var searchByName = $("#searchByName option:selected").val();
	var prtNo = $("#prtNo_ option:selected").val(); 
	
	$("#prtOffrNo").val('');
	$("#searchByValue").val('');
	
	if(searchByName==''){
		alert("Please select the search By");
		$("#searchByName" ).focus();
		return false;
	}
	else{
		$("#searchByValue").autocomplete({
			minLength: 1,
			source: function(request, response) {    	  
				$.ajax({
					dataType: "json",
					type: "GET",
					xhrFields: {
						withCredentials: true
					},
					crossDomain: true,
					cache: true,
					data: {
						keyword: request.term
					}, 
					url: "${contextPath}/admin/getSearchByNameList?prtNo="+prtNo+"&searchByName="+searchByName,
					success: function(data){ 
						var array= $.map( data, function(key,value) {
							return {                    	  
								label:key,
								value:value    
							}
						});            	  
						response($.ui.autocomplete.filter(array, request.term));
					},
					error: function (data) {
					}
				})
			},
			focus: function( event, ui ) {
				 event.preventDefault();
			},
			select: function(event, ui) {
				
				$("#prtOffrNo").val(ui.item.value);
				$("#searchByValue").val(ui.item.label);
				$("#offercatNo").val(ui.item.value);
				searchByValueInvalid = false;
				$("#offersDealsBean_").validate().element('#searchByValue');
				return false;
			}
		});
	}
	
});

$("#prtNo_").on( "selectmenuchange", function(){
	$("#requiredField").css('display', 'none');
	$("#searchByName").val('');
	$("#searchByValue").val('');
	$( "#searchByName" ).selectmenu( "refresh",true );
	$("#offersDealsBean_").validate().element('#prtNo');
});
	
$('#sortOrder').on('keyup', function () {
	var offersDealsSortOrder = $("#sortOrder").val();
	sortOrderExist=false;
	
	if(offersDealsSortOrder == 0){
		$("#offersDealsBean").validate().element('#sortOrder');
	}
	else{
		$.get("${contextPath}/admin/checkOffersDealSortOrder?offersDealsSortOrder="+offersDealsSortOrder, function(data,status) {
			if(status=="success" && data!=''){
				sortOrderExist=true;
			}
			else{
				sortOrderExist=false;
			}
			$("#offersDealsBean").validate().element('#sortOrder');
		});
	}
	
	
});

$("#prtNo").on( "selectmenuchange", function(){ 
	var obj=null;					
	var prtNo = $("#prtNo option:selected").val();
	
	$.get("${contextPath}/admin/getODPartnerCategories?prtNo="+prtNo, function(data){
		var obj = jQuery.parseJSON(data);			
		$('#prtCatNo').empty();
		$('#prtCatNo').append("<option value=''>-Select Category-</option>");
		
		for(var i = 0; i < obj.length; i++){
			var value = obj[i];
			var newOption = $("<option value="+value.prtCatNo+">"+value.partnercategoryName+"</option>");
			$('#prtCatNo').append(newOption);	       
		}
		
		$( "#prtCatNo" ).selectmenu( "refresh" );
		$("#offersDealsBean").validate().element('#prtNo');
	});   
}); 

  
$( "#prtCatNo" ).on( "selectmenuselect", function(){
	var prtNo = $("#prtNo option:selected").val();
	$("#offerName").val('');
	
	if(prtNo==''){
		alert("Please select the partner");
		$("#offerName").val('');
		$( "#prtNo-button" ).focus();
		return false;  
	}			 
});

$( "#offerName" ).click(function() {
	/* var prtCatNo = $("#prtCatNo option:selected").val();
	 if(prtCatNo==''){
		alert("Please select the partner category");
		$("#offerName").val('');
		$( "#prtCatNo-button" ).focus();
		return false;
	} */
	var category = $("#categorySelected").val();
	if(category==''){
		alert("Please select the offer category");
		$("#offerName").val('');
		$( "#categorySelected" ).focus();
		return false;
	}
});

$('#datepicker1').on('change', function () {
	$("#offersDealsBean").validate().element('#datepicker1');
	//TODO:AW-284
	var endDate = getFormattedDate($('#datepicker2').val());
	
	if(endDate != 'Invalid Date'){
		$("#offersDealsBean").validate().element('#datepicker2');
	}
}); 

$('#datepicker2').on('change', function () {
	$("#offersDealsBean").validate().element('#datepicker2');
	//TODO:AW-284
	$("#offersDealsBean").validate().element('#datepicker1');
});

$("#prtCatNo").on("selectmenuchange", function(){
	$("#offersDealsBean").validate().element('#prtCatNo');
});
	
</script>

<!-- Validation Methods -->
<script type="text/javascript">
$.validator.addMethod('requiredPrtNo', function(value, element) {
	return true;
});

$.validator.addMethod('requiredName', function(value, element) {
	var isValid = false;
	var searchByName=$("#searchByName").val();
	var searchByValue=$("#searchByValue").val();
	
	if(searchByName == "" && searchByValue == ""){
		isValid = true;
	}
	else if(searchByName != ""){
		isValid = true;
	}
	return isValid;
});

$.validator.addMethod('requiredValue', function(value, element) {
	var isValid = false;
	var searchByName=$("#searchByName").val();
	var searchByValue=$("#searchByValue").val();
	
	if(searchByName == "" && searchByValue == ""){
		isValid = true;
	}
	else if(searchByName != "" && searchByValue != ""){
		isValid = true;
	}
	
	return isValid;
});

$.validator.addMethod('searchByValueInvalid', function(value, element) {
	return !searchByValueInvalid;
});

$.validator.addMethod("endDate", function(value, element) {
	var startDate = getFormattedDate($('#datepicker1').val());
	var endDate = getFormattedDate($('#datepicker2').val());
	
	if(endDate=='Invalid Date')
		return true;
	if (endDate >= startDate)
		return true;
	if (endDate <= startDate)
		return false;
});
	 
$.validator.addMethod("startDate", function(value, element) {
	var startDate = getFormattedDate($('#datepicker1').val());
	var endDate = getFormattedDate($('#datepicker2').val());
	
	if(endDate=='Invalid Date')
		return true;
	if (startDate <= endDate)
		return true;
	if (startDate >= endDate)
		return false;
});	
	 
$.validator.addMethod('duplicatercSortOrder', function(value, element) {
	return !sortOrderExist;
});

$.validator.addMethod('sortOrderNotZero', function(value, element) {
	var isValid = false;
	var sortOrder = $("#sortOrder").val();
	if((sortOrder != "") && (sortOrder == 0)){
		isValid = true;
	}
	return !isValid; 
});
$.validator.addMethod('sortOrderLessThan100', function(value, element) {
	var isValid = false;
	var sortOrder = $("#sortOrder").val();
	
	if(sortOrder > 100){
		isValid = true;
	}
	return !isValid;
});
$.validator.addMethod("noSpace", function(value, element) { 
    return value == '' || value.trim().length != 0;  
  });







var table;
var id='',lmdId='',store='', offerText='', title='', description='', categories='', image_url='', startDate='', endDate='';
var partnerMapName='';
//Edit record
  $('#dtable1').on('click', 'a.editOfferDeal', function (e) {
	  var checka = table.row( $(this).closest('tr')).data();
	   id = checka.id;
	   var partnerMapName = table1.row( $(this).closest('tr')).data().partnerMapName;
	   var lmdid = table.row( $(this).closest('tr')).data().lmdId;
		var store =table.row( $(this).closest('tr')).data().store;
		var text = table.row( $(this).closest('tr')).data().offerText;
		var title = table.row( $(this).closest('tr')).data().title;
		var discription =table.row( $(this).closest('tr')).data().description;
		var category =table.row( $(this).closest('tr')).data().categories;
		var imageurl = table.row( $(this).closest('tr')).data().image_url;
		var editfromdate = table.row( $(this).closest('tr')).data().startDate;
		var edittodate =table.row( $(this).closest('tr')).data().endDate;
		 $('#edit_lmdid').val(lmdid);
		 $('#edit_store').val(store);
		 $('#prtNo').val(partnerMapName).selectmenu("refresh");
			$('#edit_text').val(text);
			$('#edit_title').val(title);
			$('#edit_description').val(discription);
			//$('#edit_image_').prop("src",imageurl);
			var newImageUrl;
			if(imageurl==null){
  				$('#edit_image_').prop("src",imageurl);
  			}
  			//$('#edit_image_').prop("src",imageurl);
  			else{
			if(imageurl.startsWith("http") || imageurl.startsWith("https")){
				newImageUrl = imageurl;
			}else{
			newImageUrl = '${contextPath}' + '/admin/images/image/' + imageurl;
			} 
			$('#edit_image_').prop("src",newImageUrl);
  			}
			$('#edit_category').val(category);
			$('#edit_imageurl').val(imageurl);
			$('#edit_imageurlHidden').val(imageurl);
			$('#editFromDate').val(editfromdate);
			$('#editToDate').val(edittodate);
	 table = $('#dtable').DataTable();
       e.preventDefault();
      $('#myModal1').data('id', id).modal('show');
    
  } );
  
  var table1;
  var id='',lmdId='',store='', offerText='', title='', description='', categories='', image_url='', startDate='', endDate='';
  var partnerMapName='';var newImageUrl;
  //Edit record
    $('#dtable').on('click', 'a.editOfferDeal', function (e) {
  	  var checka = table1.row( $(this).closest('tr')).data();
  	   id = checka.id;
  	    var partnerMapName = table1.row( $(this).closest('tr')).data().partnerMapName;
  	    var lmdid = table1.row( $(this).closest('tr')).data().lmdId;
  		var store =table1.row( $(this).closest('tr')).data().store;
  		var text = table1.row( $(this).closest('tr')).data().offerText;
  		var title = table1.row( $(this).closest('tr')).data().title;
  		var discription =table1.row( $(this).closest('tr')).data().description;
  		var category =table1.row( $(this).closest('tr')).data().categories;
  		var imageurl = table1.row( $(this).closest('tr')).data().image_url;
  		var editfromdate = table1.row( $(this).closest('tr')).data().startDate;
  		var edittodate =table1.row( $(this).closest('tr')).data().endDate;
  		 $('#edit_lmdid').val(lmdid);
  		 $('#edit_store').val(store);
  		  $('#prtNo').val(partnerMapName).selectmenu("refresh");
  			$('#edit_text').val(text);
  			$('#edit_title').val(title);
  			$('#edit_description').val(discription);
  			if(imageurl==null){
  				$('#edit_image_').prop("src",imageurl);
  			}
  			//$('#edit_image_').prop("src",imageurl);
  			else{
			if(imageurl.startsWith("http") || imageurl.startsWith("https")){
				newImageUrl = imageurl;
			}else{
			newImageUrl = '${contextPath}' + '/admin/images/image/' + imageurl;
			} 
			$('#edit_image_').prop("src",newImageUrl);
  			}
  			//$('#edit_image_').prop("src",newImageUrl);
  			$('#edit_category').val(category);
  			$('#edit_imageurl').val(imageurl);
  			$('#edit_imageurlHidden').val(imageurl);
  			$('#editFromDate').val(editfromdate);
  			$('#editToDate').val(edittodate);
  	 table1 = $('#dtable1').DataTable();
  
         e.preventDefault();
        $('#myModal1').data('id', id).modal('show');
      
    } );
    
  
  $('#updateOffersDealsLink').on('click',function(){
  	 var json = {};
  	var prtNo= $("#prtNo").val();
  	if(prtNo==''){
  		prtNo = $('#edit_store').val();
  	}
  	var catvalue = $("#categorySelected1").val();
  	if(catvalue == ""){
  		catvalue = $("#edit_category").val();
  	}
  	    json["id"] = id;
  		json["lmdId"] = $("#edit_lmdid").val();
  		json["store"] = prtNo;
  		json["offerText"] = $("#edit_text").val();
  		json["title"] = $("#edit_title").val();
  		json["description"] = $("#edit_description").val();
  		json["categories"] = catvalue;
  		json["editImageUrl"] = $('#edit_imageurlHidden').val();
  		json["startDate"] = $("#editFromDate").val();
  		json["endDate"] = $("#editToDate").val();
  		var formData = new FormData()
  		formData.append('json', JSON.stringify(json));
    $.ajax({
  	url : "${contextPath}/admin/updateOffersDealsLink",
  	data : formData,
  	dataType : 'text',
  	processData : false,
  	contentType : false,
  	type : 'POST',
  	success : function(response) {
  		$('#myModal1').modal('hide'); 
		window.location.href = "${contextPath}/admin/offersDeals?updateMsg="+response;
  	},
  	error: function (response) {
  		 window.location.href="${contextPath}/admin/offersDeals?updateMsg=failure";

  		           	}
  });

  });
  var table;
  $('#all').on('click',function(){
	  $('#dtable_wrapper').css('display','none');
	  $('#dtableApproved_wrapper').css('display','none');
	  $('#dtable3_wrapper').css('display','none');
	  $('#dtableNewRecord_wrapper').css('display','none');
	  $("#dtableApproved").hide();
	  $("#dtable3").hide();
	  $("#dtableNewRecord").hide();
	  $("#dtable1").show();
	
	   table= $('#dtable1').DataTable( {
		  "sAjaxDataProp":"",
		  "lengthChange": false,
	      "iDisplayLength" : 10,
	      "retrieve": true,
	      "ordering": true,
	      "scrollX": true, 
	      "bProcessing": true,
	      "searching": true,
		  "ajax":"${contextPath}/admin/allOffersDeals",
		  "type": 'GET',  
		  "columns": [
			   {
	                "data":  "ida",
	                render: function ( data, type, row ) {
	                    if ( type === 'display' ) {
	                        return '<input type="checkbox" class="editor-active">';
	                    }
	                    return data;
	                },
			  }, 
			  
			    { "data": "partnerMapName" },
		        { "data": "lmdId" },
		        { "data": "store" },
		        { "data": "offerText" },
		        { "data": "title" }, 
		        { "data": "description" },
		        { "data": "categories" },
		        { "data": "startDate" },
		        { "data": "endDate" },
		        { "data": "status" },
		        {
	                "data": "null",
	                className: "center",
	                defaultContent: '<a href="" class="editOfferDeal"><img class="edit_wraper" src="${contextPath}/static/img/edit_icon.png"></a>'
	            }
		     
		    ] ,
		    "columnDefs": [
		    	 {
		                "targets": [1],
		                "visible": false,
		                "searchable": false
		            },
		            { "defaultContent": "-", "targets": "_all" }]
		   /*  buttons: [
                { extend: 'csv',   text:'export to csv',  title:'OfferDealLink_'+datetext},
             ] */
		  
		}); 
  });
  
  var table1;
  $( window ).on( "load", function() {

	  $('#dtable1').css('display','none');
	  $('#dtableApproved').css('display','none');
	  $('#dtable3').css('display','none');
	  $('#dtableNewRecord').css('display','none');
	   table1= $('#dtable').DataTable( {
		   
		
		  "sAjaxDataProp":"",
		  "lengthChange": false,
	      "iDisplayLength" : 10,
	      "retrieve": true,
	      "ordering": true,
	      "scrollX": true, 
	      "bProcessing": true,
	      "searching": true,
		  "ajax":"${contextPath}/admin/allOffersDealscheck",
		  "type": 'GET',
		  "columns": [
			   {
	                "data":  "ida",
	                render: function ( data, type, row ) {
	                    if ( type === 'display' ) {
	                        return '<input type="checkbox" class="editor-active">';
	                    }
	                    return data;
	                },
			  }, 
			  
			    { "data": "partnerMapName" },
		        { "data": "lmdId" },
		        { "data": "store" },
		        { "data": "offerText" },
		        { "data": "title" }, 
		        { "data": "description" },
		        { "data": "categories" },
		        { "data": "startDate" },
		        { "data": "endDate" },
		        { "data": "status" },
		        {
	                "data": "null",
	                className: "center",
	                defaultContent: '<a href="" class="editOfferDeal"><img class="edit_wraper" src="${contextPath}/static/img/edit_icon.png"></a>'
	            }
		     
		    ]  ,
		  "columnDefs": [
			  {
	                "targets": [1],
	                "visible": false,
	                "searchable": false
	            },
			  { "defaultContent": "-", "targets": "_all" }]
		}); 
	 
  });
  
  
  var ida ='';
  var ida1='';
  var id='';
  $("#dtable").on('change',"tbody td input[type='checkbox']",function(e){
	 
	  var checka = table1.row( $(this).closest('tr')).data();
	  var data = new Array();
	  var ida1;
	   ida = checka.lmdId;
	   id=checka.id;
	 $("input:checked").each(function() {
		 ida1=  data.push($(this).val());
	
        });
	   
	   $('#add').on('click',function(){
			var formData = new FormData()
	  $.ajax({
			url : "${contextPath}/admin/addOffersDeals?lmdId="+ida+"&id="+id,
			data : formData,
			dataType : 'text',
			processData : false,
			contentType : false,
			type : 'POST',

			success : function(response) {
				window.location.href = "${contextPath}/admin/offersDeals?updateMsg="+response;
			},
			error: function (response) {
			
				 window.location.href="${contextPath}/admin/offersDeals?updateMsg=failure";

				           	}
		});
	 
	  });
	});
 
  
  var ida ='';
  var ida1='';
  var id='';
  $("#dtable1").on('change',"tbody td input[type='checkbox']",function(e){
	
	  var checka = table.row( $(this).closest('tr')).data();
	  var data = new Array();
	  var ida1;
	   ida = checka.lmdId;
	   id=checka.id;
	  
	 $("input:checked").each(function() {
		 ida1=  data.push($(this).val());
	
        });
	   
	   $('#add').on('click',function(){
			var formData = new FormData()
	  $.ajax({
			url : "${contextPath}/admin/addOffersDeals?lmdId="+ida+"&id="+id,
			data : formData,
			dataType : 'text',
			processData : false,
			contentType : false,
			type : 'POST',

			success : function(response) {
				window.location.href = "${contextPath}/admin/offersDeals?updateMsg="+response;
			},
			error: function (response) {
			
				 window.location.href="${contextPath}/admin/offersDeals?updateMsg=failure";

				           	}
		});
	 
	  });
	});
  
  $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){ 
	  $($.fn.dataTable.tables(true)).DataTable() 
	  .columns.adjust();
	   });
 $('#cancelbtnD').click(function() {
		window.location.href = "${contextPath}/admin/offersDeals";
	}); 
  
  
  
</script>

<!-- Methods -->
<script type="text/javascript">
function getFormattedDate(date){
	var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
	return new Date(date.replace(pattern,'$3-$2-$1'));
}

$("#editFromDate").datepicker({
	dateFormat : 'dd/mm/yy',
	showOn : "button",
	buttonImage : "./static/img/2016-06-23.png",
	buttonImageOnly : true,
	minDate: new Date(),
	 /* onClose : function(selectedDate) {
		$("#editToDate").datepicker("option", "minDate", selectedDate);
	},  */
	buttonText : "Select date"
});

$("#editToDate").datepicker({
	dateFormat : 'dd/mm/yy',
	showOn : "button",
	buttonImage : "./static/img/2016-06-23.png",
	buttonImageOnly : true,
	minDate: new Date(),
	buttonText : "Select date",
	beforeShow : end_Range
});
function end_Range() {
	if ($("#editFromDate").datepicker("getDate") == null) {
		$("#editFromDate").focus();
	} else {
		// set minimum date range
		$("#editToDate").datepicker("option", "minDate",
				new Date($("#editFromDate").datepicker("getDate")));
	}
	;
};

$("#categoriesModal1").on("hidden.bs.modal", function () {
	$('#myModal1').css({
        overflow: 'auto',
        
    });
});

$("#myModal1").on('shown.bs.modal', function (){
	
	$('body,html').css({
        overflow: 'hidden',
        
    });
});
$("#myModal1").on("hidden.bs.modal", function () {
	$('body,html').css({
        overflow: 'auto',
        
    });
});
var table2 ;
$('#approved').on('click',function(){
	$('#dtableApproved').DataTable().destroy();
	$("#dtable1").hide();
	$("#dtableApproved").show();
	 $('#dtable_wrapper').css('display','none');
	 $('#dtable3_wrapper').css('display','none');
	 $('#dtableNewRecord_wrapper').css('display','none');
	  
	   table2= $('#dtableApproved').DataTable( {
		  "sAjaxDataProp":"",
		  "lengthChange": false,
	      "iDisplayLength" : 10,
	      "retrieve": true,
	      "ordering": true,
	      "scrollX": true, 
	      "bProcessing": true,
	      "searching": true,
		  "ajax":"${contextPath}/admin/approvedOfferDealLink",
		  "type": 'GET',  
		  "columns": [
			 
		        { "data": "lmdId" },
		        { "data": "store" },
		        { "data": "title" }, 
		        { "data": "description" },
		        { "data": "categories" },
		        { "data": "startDate" },
		        { "data": "endDate" },
		        { "data": "status" },
		     
		    ] ,
		    "columnDefs": [{ "defaultContent": "-", "targets": "_all" }]
		  
		}); 
});
var table3;
 $('#pending').on('click',function(){
	try{
	  $('#dtable3').DataTable().destroy();
	  $("#dtable3").show();
	  $("#dtable1").hide();
	  
	 $('#dtable_wrapper').css('display','none');
	 $('#dtableApproved_wrapper').css('display','none');
	 $('#dtableNewRecord_wrapper').css('display','none');
	 
	  table3= $('#dtable3').DataTable( {
		  "sAjaxDataProp":"",
		  "lengthChange": false,
	      "iDisplayLength" : 10,
	      "retrieve": true,
	      "ordering": true,
	      "scrollX": true, 
	      "bProcessing": true,
	      "searching": true,
		  "ajax":"${contextPath}/admin/pendingOfferDealLink",
		  "type": 'GET',  
		  "columns": [
			  {
	                "data":  "ida",
	                render: function ( data, type, row ) {
	                    if ( type === 'display' ) {
	                        return '<input type="checkbox" class="editor-active">';
	                    }
	                    return data;
	                },
			  }, 
			 
			   { "data": "lmdId" },
		        { "data": "store" },
		        { "data": "offerText" },
		        { "data": "title" }, 
		        { "data": "description" },
		        { "data": "categories" },
		        { "data": "startDate" },
		        { "data": "endDate" },
		        { "data": "status" },
		        {
	                "data": "null",
	                className: "center",
	                defaultContent: '<a href="" class="pendingOfferDeal"><img class="edit_wraper" src="${contextPath}/static/img/edit_icon.png"></a>'
	            }
		     
		    ] ,
		    "columnDefs": [{ "defaultContent": "-", "targets": "_all" }]
		  
		}); 
	}catch(err){
		
	}
}); 
 var table4;
 $('#newRecord').on('click',function(){
	 $('#dtableNewRecord').DataTable().destroy();
	 $('#dtable_wrapper').css('display','none');
	 $('#dtableApproved_wrapper').css('display','none');
	 $('#dtable3_wrapper').css('display','none');
	 $("#dtable1").hide();
	 $("#dtableNewRecord").show();
	  table4= $('#dtableNewRecord').DataTable( {
		  "sAjaxDataProp":"",
		  "lengthChange": false,
	      "iDisplayLength" : 10,
	      "retrieve": true,
	      "ordering": true,
	      "scrollX": true, 
	      "bProcessing": true,
	      "searching": true,
		  "ajax":"${contextPath}/admin/newRecordOfferDealLink",
		  "type": 'GET',  
		  "columns": [
			  {
	                "data":  "ida",
	                render: function ( data, type, row ) {
	                    if ( type === 'display' ) {
	                        return '<input type="checkbox" class="editor-active">';
	                    }
	                    return data;
	                },
			  }, 
			 
			   { "data": "lmdId" },
		        { "data": "store" },
		        { "data": "offerText" },
		        { "data": "title" }, 
		        { "data": "description" },
		        { "data": "categories" },
		        { "data": "startDate" },
		        { "data": "endDate" },
		        { "data": "status" },
		        {
	                "data": "null",
	                className: "center",
	                defaultContent: '<a href="" class="newOfferDeal"><img class="edit_wraper" src="${contextPath}/static/img/edit_icon.png"></a>'
	            }
		     
		    ] ,
		    "columnDefs": [{ "defaultContent": "-", "targets": "_all" }]
		  
		}); 
	
}); 
 $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){ 
	 $($.fn.dataTable.tables(true)).DataTable() 
	 .columns.adjust();
	  });
 
 var table3;
 var id='',lmdId='',store='', offerText='', title='', description='', categories='', image_url='', startDate='', endDate='';
 //Edit pending record
    $('#dtable3').on('click', 'a.pendingOfferDeal', function (e) {
 	  var checka = table3.row( $(this).closest('tr')).data();
 	   id = checka.id;
 	   var lmdid = table3.row( $(this).closest('tr')).data().lmdId;
 		var store =table3.row( $(this).closest('tr')).data().store;
 		var text = table3.row( $(this).closest('tr')).data().offerText;
 		var title = table3.row( $(this).closest('tr')).data().title;
 		var discription =table3.row( $(this).closest('tr')).data().description;
 		var category =table3.row( $(this).closest('tr')).data().categories;
 		var imageurl = table3.row( $(this).closest('tr')).data().image_url;
 		var editfromdate = table3.row( $(this).closest('tr')).data().startDate;
 		var edittodate =table3.row( $(this).closest('tr')).data().endDate;
 		 $('#edit_lmdid').val(lmdid);
 		 $('#edit_store').val(store);
 			$('#edit_text').val(text);
 			$('#edit_title').val(title);
 			$('#edit_description').val(discription);
 			var newImageUrl;
 			if(imageurl==null){
  				$('#edit_image_').prop("src",imageurl);
  			}
  			else{
			if(imageurl.startsWith("http") || imageurl.startsWith("https")){
				newImageUrl = imageurl;
			}else{
			newImageUrl = '${contextPath}' + '/admin/images/image/' + imageurl;
			} 
			$('#edit_image_').prop("src",newImageUrl);
  			}
 			//$('#edit_image_').prop("src",imageurl);
 			$('#edit_category').val(category);
 			$('#edit_imageurl').val(imageurl);
 			$('#edit_imageurlHidden').val(imageurl);
 			$('#editFromDate').val(editfromdate);
 			$('#editToDate').val(edittodate);
 			table3 = $('#dtable3').DataTable();
        e.preventDefault();
       $('#myModal1').data('id', id).modal('show');
     
   } ); 
 
    var id='',lmdId='',store='', offerText='', title='', description='', categories='', image_url='', startDate='', endDate='';
    //Edit New record
       $('#dtableNewRecord').on('click', 'a.newOfferDeal', function (e) {
    	  var checka = table4.row( $(this).closest('tr')).data();
    	   id = checka.id;
    	   var lmdid = table4.row( $(this).closest('tr')).data().lmdId;
    		var store =table4.row( $(this).closest('tr')).data().store;
    		var text = table4.row( $(this).closest('tr')).data().offerText;
    		var title = table4.row( $(this).closest('tr')).data().title;
    		var discription =table4.row( $(this).closest('tr')).data().description;
    		var category =table4.row( $(this).closest('tr')).data().categories;
    		var imageurl = table4.row( $(this).closest('tr')).data().image_url;
    		var editfromdate = table4.row( $(this).closest('tr')).data().startDate;
    		var edittodate =table4.row( $(this).closest('tr')).data().endDate;
    		 $('#edit_lmdid').val(lmdid);
    		 $('#edit_store').val(store);
    			$('#edit_text').val(text);
    			$('#edit_title').val(title);
    			$('#edit_description').val(discription);
    			//$('#edit_image_').prop("src",imageurl);
    			var newImageUrl;
    			if(imageurl.startsWith("http") || imageurl.startsWith("https")){
    				newImageUrl = imageurl;
    			}else{
    			newImageUrl = '${contextPath}' + '/admin/images/image/' + imageurl;
    			}
    			$('#edit_image_').prop("src",newImageUrl);
    			$('#edit_category').val(category);
    			$('#edit_imageurl').val(imageurl);
    			$('#edit_imageurlHidden').val(imageurl);
    			$('#editFromDate').val(editfromdate);
    			$('#editToDate').val(edittodate);
    			table4 = $('#dtable4').DataTable();
           e.preventDefault();
          $('#myModal1').data('id', id).modal('show');
        
      } ); 
 
       var ida ='';
       var ida1='';
       var id='';
       $("#dtable3").on('change',"tbody td input[type='checkbox']",function(e){
     	 
     	  var checka = table3.row( $(this).closest('tr')).data();
     	  var data = new Array();
     	  var ida1;
     	   ida = checka.lmdId;
     	   id=checka.id;
     	 $("input:checked").each(function() {
     		 ida1=  data.push($(this).val());
     	
             });
     	   
     	   $('#add').on('click',function(){
     			var formData = new FormData()
     	  $.ajax({
     			url : "${contextPath}/admin/addOffersDeals?lmdId="+ida+"&id="+id,
     			data : formData,
     			dataType : 'text',
     			processData : false,
     			contentType : false,
     			type : 'POST',

     			success : function(response) {
     				window.location.href = "${contextPath}/admin/offersDeals?updateMsg="+response;
     			},
     			error: function (response) {
     			
     				 window.location.href="${contextPath}/admin/offersDeals?updateMsg=failure";

     				           	}
     		});
     	 
     	  });
     	});
       
       var ida ='';
       var ida1='';
       var id='';
       $("#dtableNewRecord").on('change',"tbody td input[type='checkbox']",function(e){
     	  var checka = table4.row( $(this).closest('tr')).data();
     	  var data = new Array();
     	  var ida1;
     	   ida = checka.lmdId;
     	   id=checka.id;
     	 $("input:checked").each(function() {
     		 ida1=  data.push($(this).val());
     	
             });
     	   
     	   $('#add').on('click',function(){
     			var formData = new FormData()
     	  $.ajax({
     			url : "${contextPath}/admin/addOffersDeals?lmdId="+ida+"&id="+id,
     			data : formData,
     			dataType : 'text',
     			processData : false,
     			contentType : false,
     			type : 'POST',

     			success : function(response) {
     				window.location.href = "${contextPath}/admin/offersDeals?updateMsg="+response;
     			},
     			error: function (response) {
     			
     				 window.location.href="${contextPath}/admin/offersDeals?updateMsg=failure";

     				           	}
     		});
     	 
     	  });
     	});
       
      /*  Pagination OffersDeal */
     var bannerPagination;
      $(document).ready(function() {
    	  bannerPagination= $('#viewOffersTable').DataTable( {
							      "iDisplayLength" : 10,
							      "ordering": false,
							      "bPaginate": true,
							      "bProcessing": true,
							      "searching": false,
							     // "scrollX": true, 
							      "serverSide": true,
							  "ajax":"${contextPath}/admin/OffersDealPagination",
							  "type": 'GET',
							  "columns": [
								    { "data": "prtOffrNo"},
							        { "data": "offerName"},
							        { "data": "offerDesc" }, 
							        { "data": "categoryName" },
							        { "data": "sortOrder" },
							        { "data": "offerStartDate" },
							        { "data": "offerEndDate" },
							        { "data": "offersStatus" },
							  
							        {
						                "data": "null",
						                className: "center",
						                defaultContent: '<a class="editOffer" ><img class="edit_wraper" src="${contextPath}/static/img/edit_icon.png"></a> '
						                +'<a class="delete btnDelete" data-target="#myModalDelete" id="deleteOffersDeals"><img src="${contextPath}/static/img/delete_icon1.png"  class="fa fa-trash fa-lg"></img></a>'
						            }  
							        
							  
							    ]  ,
							  "columnDefs": [
								  {
						                "targets": [ 0 ],
						                "visible": false,
						                "searchable": false
						            },
								  { "defaultContent": "-", "targets": "_all" }
								  ],
								  "createdRow": function( row, data, dataIndex){
									  console.log(JSON.stringify(data));
			                            if(data.offersStatus ==='Active' ){
			                            
			                            	$('td', row).eq(6).css('color', 'Green');
			                            }
			                            else {
			                            	$('td', row).eq(6).css('color', 'Red');
			                            }     } 
							});
     //  $('#serachBtn').trigger("click");
      });

      
      
      
      function uploadImage(id, type, url) {
  		/* if ($("#offersDealsBean").validate().element("#imageUrl")) { */
  			var formData = new FormData();
  			formData.append('file', $("#" + id)[0].files[0]);

  			/* if ($("#offersDealsBean").validate().element("#" + id)) { */
  			
  				$.ajax({
  					url : "${contextPath}/admin/saveImages?type=" + type,
  					data : formData,
  					dataType : 'text',
  					processData : false,
  					contentType : false,
  					type : 'POST',

  					success : function(response) {
  						uploadclick = false;
  						$('#uploadFile').val(response);
  						var newImageUrl = '${contextPath}' + '/admin/images/'
  								+ type + "/" + response;
  						$('#offerImage_').attr('src', newImageUrl);
  						$("#offersDealsBean").validate().element("#" + url);
  						$('#offerImageUrl').val(response);
  						$("#offersDealsBean").validate().element("#imageUrl1");
  					}
  				});
  		/* 	}
  		} */

  	}
      $('#imageUrl1').on('change', function() {
  		$('#uploadFile').val($(this).val());
  	}); 
      
      
     /*  $.validator.addMethod('filesize', function(value, element, param) {
  		return this.optional(element) || (element.files[0].size <= param)
  	},  *//* 'File size should not be more than 1MB'); */

  	/* $.validator.addMethod('imageDimention', function(value, element) {
  		var fileUpload = $("#imageUrl")[0];
  		alert(fileUpload)
  		var bannerType = $("#offersDealsBean input[type='radio']:checked").val();
  		checkFileUpload1(fileUpload, bannerType, "newBannerError");
  		return uploadFlag;
  	}, ''); */
  	
  	
  	$(function() {
		$("#offersDealsBean").bind(
				"change",
				function() {
					//Get reference of FileUpload.
					var fileUpload = $("#imageUrl1")[0];
					var bannerType = $(
							"#offersDealsBean input[type='radio']:checked").val();
					checkFileUpload1(fileUpload, bannerType, "newBannerError");
					if (!uploadFlag)
						return false;
				});
	});
  	
  	function checkFileUpload1(fileUpload, Type, Status) {
  		//Check whether the file is valid Image.
  		var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:\().+(\)\.def\().+(\])+(.jpg|.jpeg|.png|.gif)$");
  		if (regex.test(fileUpload.value.toLowerCase())) {
  			notificationEditFlag = true;
  			if (typeof (fileUpload.files) != "undefined") {
  				var reader = new FileReader();
  				reader.readAsDataURL(fileUpload.files[0]);

  				reader.onload = function(e) {
  					var image = new Image();
  					image.src = e.target.result;

  					image.onload = function() {

  						//Determine the Height and Width.
  						var height = this.height;
  						var width = this.width;

  						var errorText = "";
  						uploadFlag = false;
  						/* if (Type == 0) {
							if (width == 1366 && height == 438) {
								uploadFlag = true;
							} else {
								uploadFlag = false;
								errorText = "Height and Width must be 438 and 1366 for Offer Image.";
							}
						} 
						else {
							if (width == 1366 && height == 438) {
								uploadFlag = true;
							} else {
								uploadFlag = false;
								errorText = "Height and Width must be 438 and 1366 for Offer Image.";
							}
						}  */

  						$("#" + Status).css({
  							"display" : ""
  						});
  						$("#" + Status).html("<p>" + errorText + "</p>");

  						if (errorText != "") {
  							$("#ImageUrl1-error").css({
  								"display" : "none"
  							});
  						}
  						return uploadFlag;
  					};
  				};
  			}
  		} else {
  			uploadFlag = false;
  			notificationEditFlag = false;
  		}
  	}
</script>
