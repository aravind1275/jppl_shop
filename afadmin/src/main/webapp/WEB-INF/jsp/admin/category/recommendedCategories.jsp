<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!-- Modal -->
		<div id="myModalDelete" class="modal fade" role="dialog">	
			<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Are You Sure?</h4>
				</div><!--header-->
				<div class="modal-body">
					<p><img src="${contextPath}/static/img/info_icon1.png"></img>
					<span id="deletercId"></span>
				</div><!---body--->
				<div class="modal-footer">
					<button id="btnD" class="bg_g_button" data-dismiss="modal">Delete</button>
				</div>
			</div><!---modal content--->
			</div>
		</div>	
		
		
		<!-- Select Category Modal Start -->
		<div id="categoriesModal" class="modal fade" role="dialog">	
			<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Select Category</h4>
				</div><!--header-->
				<div class="modal-body">
					<div class="select_category"></div>
				</div><!---body--->
				<div class="modal-footer">
				</div>
			</div><!---modal content--->
			</div>
		</div>
		<!-- Select Category Modal End -->
        <div class='container'>
            
            <section class="insurance_management">
                <div id ="breadcrumb">
                    <ul>
                        <li>Home</li>
						<li>Content Management</li>
						<li>Recommended Categories</li>
                    </ul>
                </div>  <!---breadcrumb--->
                <c:if test="${fn:length(message) gt 0}"><div class="alertMessage" style="color:green;font-weight: bold;"  ><c:out value="${message}"></c:out></div> </c:if>
	 			 <c:if test="${fn:length(errormessage) gt 0}"><div class="alertMessage" style="color:red;font-weight: bold;"  ><c:out value="${errormessage}"></c:out></div> </c:if>
				<div class="section_heading">
					<h5>Recommended Categories</h5> 
				</div><!--- section heading ---->
				<div class="management"><!---User Management--->
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#sectionA">Recommended Categories</a></li>
                        <li><a data-toggle="tab" href="#sectionB">Define Recommended Categories</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="sectionA" class="tab-pane fade in active">
                        <nav class="navbar navbar-default" role="navigation" id="submenu">
                       <%--  <form:form autocomplete="off" action="recommendedCategoriesFilter" commandName="recommendedCategoryBean_" enctype="multipart/form-data">
                                <!-----Recommended Categories starts here---->
							<div class="row pad_top">
								<div class="col-md-4">
									<label>Select A Partner</label>
									<fieldset class="infield">
										<form:select path="prtNo" id="prtNo_" class="select_option">             		
             					 	<form:option value="">--Select Partner--</form:option>
             	 					 <c:forEach items="${partnerList}" var="partner">
	             					      <li>
	             					      <form:option value="${partner.prtNo}" label="${partner.partnerName}" />
	             						</li>
	             					</c:forEach> 
	             				</form:select>
									</fieldset>	
								</div>
								</div><!--- row --->
								 </form:form>
                            <c:if test="${not empty rcList}"> --%>
                            <table class="partner_info_table table-bordered" cellspacing="0" cellpadding="0" width="98%">
								<thead>
									<tr>
										<th class="name">Sl No.</th>
										<!-- <th>Category ID</th> -->
										<th>Category Name</th>
										<th>Sort Order</th>
										<th>Category Start Date</th>
										<th>Category End Date</th>
										<th>Status</th>
										<th class="width_section">Action</th>
									</tr>
								</thead><!----thead--->
								<tbody>
								<c:forEach items="${rcList}" var="rcategory" varStatus="i">
									<tr class="admin" data-id="${rcategory.rcNo}"
									 data-catno="${rcategory.catNo}"
								<%-- 	 data-rcategoryid="${rcategory.rcId}" --%>
										data-rcategoryname="${rcategory.rcName}">
										<td class="slno_width">${i.count}</td>
										<%-- <td class="td_width">${rcategory.rcId}</td> --%>
										<td class="td_width">${rcategory.rcName}</td>
										<c:choose>
  											<c:when test="${rcategory.position ne 0}">
												<td class="td_width">${rcategory.position}</td>
											</c:when>
											<c:otherwise>
												<td class="td_width"></td>
											</c:otherwise>
										</c:choose>
										<td class="td_width">${rcategory.activationDate}</td>
										<td class="td_width">${rcategory.deactivationDate}</td>									
										<td class="${rcategory.statusClass} td_width">${rcategory.rcStatus}</td>		
										<td class="td_width">
											<a href="${contextPath}/admin/updateRecommendedCategory?rcNo=${rcategory.rcNo}">
												<img class="edit_wraper" src="${contextPath}/static/img/edit_icon.png">
											</a>
											<span class="delete btnDelete" data-target="#myModalDelete" id="deleteRecommendedCategory">
												<img src="${contextPath}/static/img/delete_icon1.png"  class="fa fa-trash fa-lg"></img>
											</span>
										</td>
									</tr> 
									</c:forEach>
								</tbody><!---tbody--->
							</table><!---table-->
							<%-- </c:if>	 --%>
                            </nav>
                        </div><!----section A---->
                        <div id="sectionB" class="tab-pane fade submenu" ><!-----Define Recommendation starts here---->
                        <form:form autocomplete="off" action="recommendedCategories" commandName="recommendedCategoryBean" enctype="multipart/form-data">
                          <form:input type="hidden" path="categoryList" id="getCategoryList" value="${requestScope.categoriesList}" />
                            <div class="row pad_top">
                           	 <div class="col-md-4">
									<label>Select Recommended Category</label><span class="mandatory_star">*</span>
									<input type="text" class="form-control" id="categorySelected" readonly="true">	
									<form:hidden path="catNo" class="form-control" id="catNo"/>	
									<a href="javascript:void(0);" class="selectCategory"  data-toggle="modal" data-target="#categoriesModal">Click here to View Categories</a>
							</div>
								<%-- <div class="col-md-4">
									<label>Select A Partner</label><span class="mandatory_star">*</span>
									<fieldset class="infield">
										<form:select path="prtNo" id="prtNo" class="select_option">             		
             					 			<form:option value="">--Select Partner--</form:option>
             	 					 		<c:forEach items="${partnerList}" var="partner">
	             					      <li>
	             					      <form:option value="${partner.prtNo}" label="${partner.partnerName}" />
	             						</li>
	             					</c:forEach> 
	             				</form:select>
									</fieldset>	
								</div> --%>
								
								<%--<div class="col-md-4">
									<label>Category Landing URL</label><span class="mandatory_star">*</span>
									<form:input type='text' maxlength="200"  id="rcLandingUrl" path="rcLandingUrl" class="form-control" />									
								</div>
								
								 <div class="col-md-4">
									<label>Recommended Category Name</label><span class="mandatory_star">*</span>
									<input type='text'  id="rcName" name="rcName" placeholder=" What's your Wish?" class="form-control"/>
											<form:input type="hidden" path="prtCatNo" id="prtCatNo"/>
								</div> --%>
								
                                <div class="col-md-4">
									<label>Recommended Category Position</label><!-- <span class="mandatory_star">*</span> -->
									<form:input type='text' name="position" id="position" path="position" class="form-control" maxlength="3"/>
								</div>
                            </div><!--- row --->
                            <div class="row pad_top">
								<div class="col-md-4">
									<label>Category Start Date</label><span class="mandatory_star">*</span>
									<div class="pick_date">
										<form:input  class="form-control startDate" id='datepicker11' path="activationDate"/>
									</div>
								</div>
								<div class="col-md-4">
									<label>Category End Date</label>
									<div class="pick_date">
										<form:input  class="form-control endDate" id='datepicker12' path="deactivationDate"/>
									</div>
								</div>
								
							</div> <!-------  row  ------>
							<div class="row pad_top pad_bottom">
								<div class="col-md-4">
									<label>Category Image URL</label><span class="mandatory_star">*</span>
									<form:input type='text' id="rcImageUrl"  path="rcImageUrl" class="form-control" />
								</div>
								<div class="col-md-4">
									<label>Alternative Image Text</label><span class="mandatory_star">*</span>
									<form:input type='text' id="rcImageDesc" path="rcImageDesc" class="form-control"/>
								</div>
								<div class="col-md-4">
                                    <div class='text top' >
                                        <label>Recommended Category Status</label><span class="mandatory_star">*</span>
                                        <div class="half-col last">
                                            <ul class="list-inline">
                                               <c:forEach items="${rcStatus}" var="status">
								<li>
								<label>
								<form:radiobutton path="statusCode" class="css-checkbox" id ="${status.key}" value="${status.key}" />
								<label for="${status.key}" class="css-label radGroup2">${status.value}</label>
								</label>
								</li>
								</c:forEach>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
							</div> <!-----  row  ------>
                            <div class="row btn_save pull-right">
								<a  href="${contextPath}/admin/recommendedCategories"><button type="button" class="bg_g_button" id="cancelbtnD">Cancel</button></a>
								<button type="button" class="bg_g_button"
							onClick="$(this).closest('form').submit();">Save</button>
							</div><!---- button ------>
							</form:form>
                        </div><!----section B---->
                       
                    </div><!-----tab-content---->
                </div>	<!---- user managemnt----->	
            </section><!---- section----->	
            
        </div>

<!-- Variables -->
<script type="text/javascript">
var rcPositionExist = false;
var rcCatExist = false;
var rcNameExist=false;
var rcNameInvalid=false;
</script>

<!-- Form Validation -->
<script type="text/javascript">
$(document).ready(function () {
	
	jQuery.extend( jQuery.fn.dataTableExt.oSort, {
	    "non-empty-string-asc": function (str1, str2) {
	    	str1 = Number($.trim(str1));
	    	str2 = Number($.trim(str2));
	        if(str1 == null || str1 == "")
	            return 1;
	        if(str1 == null || str2 == "")
	            return -1;
	        return ((str1 < str2) ? -1 : ((str1 > str2) ? 1 : 0));
	    },
	    "non-empty-string-desc": function (str1, str2) {
	    	str1 = Number($.trim(str1));
	    	str2 = Number($.trim(str2));
	        if(str1 == null || str1 == "")
	            return 1;
	        if(str1 == null || str2 == "")
	            return -1;
	        return ((str1 < str2) ? 1 : ((str1 > str2) ? -1 : 0));
	    }
	});

	$('.partner_info_table').DataTable( {
	     "paging": true,
	     "searching": false,
	     "lengthChange": false,
	     "info":false,
	     "iDisplayLength" : 100,
	     "order": [[ 2, "asc" ]],
	     columnDefs: [
	          {type: 'non-empty-string', targets: 2} // define 'name' column as non-empty-string type
	     ],
	 });
	
	/*$('.partner_info_table').DataTable( {
        "paging": true,
         "searching": false,
         "lengthChange": false,
         "info":false,
         "order": [[ 2, "asc" ]]
     });*/
	$('#datepicker11').prop('readonly', 'readonly');
	$('#datepicker12').prop('readonly', 'readonly');
	
	$("#recommendedCategoryBean").validate({
		ignore:[],
		rules: {		
			 catNo:{
				required: true,
				duplicateCatNo:true
			},	         
			/* rcName:{
				required: true,
				rangelength:[1,100],
				rcNameInvalid:true,
				duplicatercName:true
			}, */
			position:{
				required: false,	 
				digits:true,
				positionNotZero:true,
				duplicatercPosition:true,
				positionLessThan100 : true
			},	 		
			activationDate:{
				required: true,
				startDate: true
			},
			deactivationDate:{
		
				endDate:true
			},
			/* rcLandingUrl:{
				required: true,
				rangelength:[1,200],
				url:true
			},*/
			rcImageUrl:{
				required: true,
				rangelength:[1,2000],
				url:true
			},
			rcImageDesc:{
				required: true,
				rangelength:[1,100]
			} 
		},
		messages: {		  
			catNo:{
				required:"Please select the Category",
				duplicateCatNo:"Category already exists"
			},	 	
			/* rcName:{
				required:"Please enter Recommended Category Name",
				rangelength:"Maximum 100 characters permitted",
				duplicatercName:"Recommended Category already exists",
				rcNameInvalid:"Invalid Category, Please select from dropdown"
			}, */
			position:{
				required: "Please enter Position",	
				digits:"Please enter numeric data",
				positionNotZero:"Position cannot be 0",
				duplicatercPosition:"Recommended Category position already exists",
				positionLessThan100: "Recommended Category position must be less than or equal to 100"
			},	 	
			activationDate:{
				required: "Please select Category Start Date",
				startDate: "* Start Date must be less than or equal to End date"
			},
			deactivationDate:{
				endDate: "* End Date must be greater than or equal to Start date"
			},
			/* rcLandingUrl:{
				required:"Please enter the Category landing URL" 		
			}, */
			rcImageUrl:{
				required:"Please enter the image URL" ,
				rangelength:"Image Url length too large"
			},
			 rcImageDesc:{
				required:"Please enter the image text",
				rangelength:"Maximum 100 characters permitted"
			}
		},
		errorElement: "div",
		errorPlacement: function(error, element) {
			if ($(element).hasClass("form-control startDate")) {
				error.insertAfter($(element).closest("div"));
			}
			else if ($(element).hasClass("form-control endDate")){
				error.insertAfter($(element).closest("div"));
			}
			else {
				error.insertAfter(element);
			}
		},
		submitHandler: function (form) {
			if($("#position").val() == ""){ $("#position").val(0);}
			form.submit();
		}
	});
});
</script>

<!-- Events -->
<script type="text/javascript">
$('span#deleteRecommendedCategory').on('click', function () {
	var id = $(this).closest('tr').data('id');
	var rcid= $(this).closest('tr').data('rcategoryid');
	var rcname= $(this).closest('tr').data('rcategoryname');
	
	$('#deletercId').text(rcname+" "+ " Recommended Category will be deleted. Do you want to continue?");
	$('#myModalDelete').data('id', id).modal('show');
});

$('#btnD').click(function () {
	var id = $('#myModalDelete').data('id');
	
	$.get("${contextPath}/admin/deleteRecommendedCategory?rcNo="+id , function(status) {		
		if(status=="success" ){
			$('[data-id=' + id + ']').remove();
			$('#myModalDelete').modal('hide');
			window.location.href="${contextPath}/admin/recommendedCategories?deleteMsg="+status;	
		}
		if(status=="failure" ){
			$('[data-id=' + id + ']').remove();
			$('#myModalDelete').modal('hide');
			window.location.href="${contextPath}/admin/recommendedCategories?deleteMsg="+status;		
		}
	});
});	

$('#position').on('keyup', function () {
	var recommendedCategoryPosition = $("#position").val();
	rcPositionExist = false;
	
	if(recommendedCategoryPosition == 0){
		$("#recommendedCategoryBean").validate().element('#position');
	}
	else{
		$.get("${contextPath}/admin/checkRecommendedCategoryPosition?recommendedCategoryPosition="+recommendedCategoryPosition, function(data,status) {
			if(status=="success" && data!=''){
				rcPositionExist=true;
			}
			else{
				rcPositionExist=false;
			}
			$("#recommendedCategoryBean").validate().element('#position');
		});
	}
	
});

$('#datepicker11').on('change', function () {
	$("#recommendedCategoryBean").validate().element('#datepicker11');
	//TODO:AW-284
	var endDate = getFormattedDate($('#datepicker12').val());
	
	if(endDate != 'Invalid Date'){
		$("#recommendedCategoryBean").validate().element('#datepicker12');
	}
}); 

$('#datepicker12').on('change', function () {
	$("#recommendedCategoryBean").validate().element('#datepicker12');
	//TODO:AW-284
	$("#recommendedCategoryBean").validate().element('#datepicker11');
});

/* $( "#rcName" ).click(function() {
	var prtNo = $("#prtNo option:selected").val();
	$("#rcName").val('');
	$("#prtCatNo").val('');
	
	if(prtNo==''){
		alert("Please select the Partner");
		$("#rcName").val('');
		$( "#prtNo-button" ).focus();
		return false;
	}			 
}); */

/* $("#prtNo").on( "selectmenuchange", function(){ 
	var prtNo = $("#prtNo option:selected").val(); 
	$("#rcName").val('');
	
	$( "#rcName" ).autocomplete({
		minLength: 1,
		source: function(request, response) {    	  
			$.ajax({
				dataType: "json",
				type: "GET",
				xhrFields: {
					withCredentials: true
				},
				crossDomain: true,
				cache: true,
				data: {
					keyword: request.term
				}, 
				url: "${contextPath}/admin/getPartnerCategory?prtNo="+prtNo,
				success: function(data){            	  
					var array= $.map( data, function(key,value) {
						return {                    	  
							label:key,
							value:value    
						}
					});            	  
					response($.ui.autocomplete.filter(array, request.term));
				},
				error: function (data) {
				}
			})
		},
		focus: function( event, ui ) {
			 event.preventDefault();
		},
		select: function(event, ui) {    	  
			$("#prtCatNo").val(ui.item.value); 
			$("#rcName").val(ui.item.label);
			return false;
		}
	});
	$("#recommendedCategoryBean").validate().element('#prtNo');
});	  */

/* $("#prtNo_").on( "selectmenuchange", function(){
	if ($('#prtNo_').val() != ''){
		$(this).closest('form').trigger('submit');
	}
}); */
	 
/* $("#rcName").focusout(function(){
	var prtNo=$("#prtNo").val();
	var prtCatNo=$("#prtCatNo").val();
	
	if(prtCatNo==''){				
		rcNameInvalid=true;
		$("#recommendedCategoryBean").validate().element('#rcName');
	}
	else{
		$.get("${contextPath}/admin/checkRecommendedCategory?prtNo="+prtNo+"&prtCatNo="+prtCatNo, function(data,status) {
			if(status=="success" && data!=''){
				rcNameExist=true;	
			}
			else{
				rcNameExist=false;
			}
			rcNameInvalid=false;
			$("#recommendedCategoryBean").validate().element('#rcName');			
		});				
	}
}); */


</script>

<!-- Validation Methods -->
<script type="text/javascript">
$.validator.addMethod('duplicatercPosition', function(value, element) {
	return !rcPositionExist;
});
$.validator.addMethod('duplicateCatNo', function(value, element) {
	return !rcCatExist;
});

$.validator.addMethod('duplicatercName', function(value, element) {
	return !rcNameExist;
});

$.validator.addMethod('positionNotZero', function(value, element) {
	var isValid = false;
	var rcPosition = $("#position").val();
	
	if((rcPosition != "") && (rcPosition == 0)){
		isValid = true;
	}
	return !isValid; 
});
$.validator.addMethod('positionLessThan100', function(value, element) {
	var isValid = false;
	var recommendedCategoryPosition = $("#position").val();
	
	if(recommendedCategoryPosition > 100){
		isValid = true;
	}
	return !isValid;
});
$.validator.addMethod('rcNameInvalid', function(value, element) {
	return !rcNameInvalid;
});
$.validator.addMethod("endDate", function(value, element) {
	var startDate = getFormattedDate($('#datepicker11').val());
	var endDate = getFormattedDate($('#datepicker12').val());
	
	if(endDate=='Invalid Date')
		return true;
	if (endDate >= startDate)
		return true;
	if (endDate <= startDate)
		return false;
});  

$.validator.addMethod("startDate", function(value, element) {
	var startDate = getFormattedDate($('#datepicker11').val());
	var endDate = getFormattedDate($('#datepicker12').val());
	
	if(endDate=='Invalid Date')
		return true;
	if (startDate <= endDate)
		return true;
	if (startDate >= endDate)
		return false;
});
</script>

<!-- Methods -->
<script type="text/javascript">
function getFormattedDate(date){
	var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
	return new Date(date.replace(pattern,'$3-$2-$1'));
}
</script>		


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />
<script type="text/javascript">
$(document).on('click','.selectCategory',function(){
	var content = $("#getCategoryList").val();
	var con = JSON.parse(content);
	$('.select_category').jstree({
			'core' : {
				'data' : con,
				 "check_callback" : true,
			}
		});
});
$(document).on('click','.select_category .jstree-container-ul li a',function(){
	selected_categoryValue = $('.select_category').jstree().get_selected(true)[0].text;
// 	console.log(selected_categoryValue);

var catno = $(".jstree-clicked").attr("catno");
$("#catNo").val(catno);
$('#categorySelected').val(selected_categoryValue);
var rcCatNo = catno;//$("#catNo").val();
rcCatExist = false;
	$.get("${contextPath}/admin/checkRcCatNo?rcCatNo="+rcCatNo, function(data,status) {
		if(status=="success" && data!=''){
			rcCatExist=true;
		}
		else{
			rcCatExist=false;
		}
		$("#recommendedCategoryBean").validate().element('#catNo');
	});
$('.close').click();
});
</script>