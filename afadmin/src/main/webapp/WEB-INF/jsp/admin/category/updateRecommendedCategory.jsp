<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!-- Select Category Modal Start -->
		<div id="categoriesModal" class="modal fade" role="dialog">	
			<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Select Category</h4>
				</div><!--header-->
				<div class="modal-body">
					<div class="select_category"></div>
				</div><!---body--->
				<div class="modal-footer">
				</div>
			</div><!---modal content--->
			</div>
		</div>
		<!-- Select Category Modal End -->
                               
	<div class='container'>            
            <section class="insurance_management">
                <div id ="breadcrumb">
                    <ul>
                        <li>Home</li>
						<li>Content Management</li>
						<li>Recommended Categories</li>
                    </ul>
                </div>  <!---breadcrumb--->
				<div class="section_heading">
					<h5>Recommended Categories</h5> 
				</div><!--- section heading ---->
				<div class="management"><!---User Management--->
                    <ul class="nav nav-tabs">
                        <li><a  href="${contextPath}/admin/recommendedCategories">Recommended Categories</a></li>
                        <li class="active"><a data-toggle="tab" href="#sectionB">Update Recommendation</a></li>
                    </ul>
                        <div id="sectionB" class="tab-pane fade submenu  in active" ><!-----Define Recommended starts here---->
                        <form:form autocomplete="off" commandName="recommendedCategoryBean" enctype="multipart/form-data">
                            <form:input type="hidden" path="categoryList" id="categoryList" value="${requestScope.categoriesList}" />
                           
                            <div class="row pad_top">
								<%-- <div class="col-md-4">
									<label>Select A Partner</label><span class="mandatory_star">*</span>
									<fieldset class="infield">
										<form:select path="prtNo" id="prtNo" class="select_option">             		
             					 <form:option value="">--Select--</form:option>
             	 					 <c:forEach items="${partnerList}" var="partner">
	             					      <li>
	             					      <form:option value="${partner.prtNo}" label="${partner.partnerName}" />
	             						</li>
	             					</c:forEach> 
	             				</form:select>	
									</fieldset>	
									<input type="hidden" name="prtNo_" id="prtNo_"/>
								</div> --%>
                                <div class="col-md-4">
									<label>Recommended Category  Name</label><span class="mandatory_star">*</span>
									<form:input type='text'  id="categorySelected" path="rcName" readonly="true" class="form-control" />
									<form:input type="hidden" path="catNo" id="catNo"/>
									<a href="javascript:void(0);" class="selectCategory"  data-toggle="modal" data-target="#categoriesModal">Click here to View Categories</a>
								</div>
								<!--	<div class="col-md-4">
									<label>Category Landing URL</label><span class="mandatory_star">*</span>
									<form:input type='text'  id="rcLandingUrl" maxlength="200" path="rcLandingUrl" class="form-control" />
								</div> -->
								<div class="col-md-4">
									<label>Position</label><!-- <span class="mandatory_star">*</span> -->
									<form:input type='text' name="position" id="position" path="position" class="form-control" maxlength="3"/>
									<input type="hidden" name="positionHidden" id="positionHidden"/>	
								</div>
                            </div><!--- row --->
                            <div class="row pad_top">
								
								<div class="col-md-4">
									<label>Category Start Date</label><span class="mandatory_star">*</span>
									<div class="pick_date">
										<form:input  class="form-control startDate" id='datepicker11' path="activationDate"/>
									</div>
								</div><!--- datepicker --->
								<div class="col-md-4">
									<label>Category End Date</label><!-- <span class="mandatory_star">*</span> -->
									<div class="pick_date">
										<form:input  class="form-control endDate" id='datepicker12' path="deactivationDate"/>
									</div>
								</div><!--- datepicker --->
							
									
								</div> <!------  row  ------>
							<div class="row pad_top pad_bottom">
								<div class="col-md-4">
									<label>Category Image URL</label><span class="mandatory_star">*</span>
									<form:input type='text' id="rcImageUrl"  path="rcImageUrl" class="form-control" />	
								</div>
								<div class="col-md-4">
									<label>Alternative Image Text</label><span class="mandatory_star">*</span>
									<form:input type='text' id="rcImageDesc" path="rcImageDesc" class="form-control" />
								</div>
								<div class="col-md-4">
									<label>Recommended Product Status</label><span class="mandatory_star">*</span>
									<div class="half-col last">
										<ul class="list-inline">
											<c:forEach items="${rcStatus}" var="status">
								<li>
								<label>
								<form:radiobutton path="rcStatus" class="css-checkbox" id ="${status.key}" value="${status.key}" />
								<label for="${status.key}" class="css-label radGroup2">${status.value}</label>
								</label>
								</li>
								</c:forEach>
										</ul>
									</div>
                                </div><!--- radio button --->
							</div> <!------  row  ------>
                            <div class="row btn_save pull-right">
								<a  href="${contextPath}/admin/recommendedCategories">
								<button type="button" class="bg_g_button">Cancel</button></a>
								<button type="button" class="bg_g_button"
							onClick="$(this).closest('form').submit();">Update</button>
							</div><!------ button ------>
							</form:form>
                        </div><!----section B---->
                        
                </div>	<!---- user managemnt----->	 
            </section><!-----section---->
            
        </div><!-----container---->

<!-- Variables -->
<script type="text/javascript">
var rcPositionExist = false;
var rcNameExist=false;
var rcNameInvalid=false;
var rcCatExist = false;
</script>

<!-- Form Validation -->
<script type="text/javascript">
$(document).ready(function () {	
	$("#positionHidden").val($("#position").val());
	$('#datepicker11').prop('readonly', 'readonly');
/* 	$('#datepicker12').prop('readonly', 'readonly'); */
		
	$("#recommendedCategoryBean").validate({
		ignore:[],
		rules: {	 		
			/* prtNo:{
				required: true,	 			
			}, */
			catNo:{
				required: true,
				duplicateCatNo:true
			},	
			rcName:{
				required: true,
				rangelength:[1,100]
				/* rcNameInvalid:true,
				duplicatercName:true, */
			},
			position:{
				required: false,	
				digits:true,
				positionNotZero:true,
				duplicatercPosition:true,
				positionLessThan100 : true
			},	 		
			activationDate:{
				required: true,
				startDate: true
			},
			deactivationDate:{
			/* 	required: true, */
				endDate:true
			},
			/*rcLandingUrl:{
				required: true,
				rangelength:[1,200],
				url:true
			}, */
			rcImageUrl:{
				required: true,
				rangelength:[1,2000],
				url:true
			},
			rcImageDesc:{
				required: true,
				rangelength:[1,100]
			}
		},
		messages: {	
			catNo:{
				required:"Please select the Category",
				duplicateCatNo:"Category already exists"
			},	
			rcName:{
				required:"Please enter Recommended Category Name",
				rangelength:"Maximum 100 characters permitted"
				/* duplicatercName:"Recommended Category already exists",
				rcNameInvalid:"Invalid Category, Please select from dropdown"
 */			},
			position:{
				required: "Please enter Position",
				digits:"Please enter numeric data",
				positionNotZero:"Position cannot be 0",
				duplicatercPosition:"Recommended Category position already exists",
				positionLessThan100: "Recommended Category position must be less than or equal to 100"
			},	 	
			activationDate:{
				required: "Please select Category Start Date",
				startDate: "* Start Date must be less than or equal to End date"
			},
			deactivationDate:{
				/* required: "Please select Category End Date", */
				endDate: "* End Date must be greater than or equal to Start date"		
			},
			/*rcLandingUrl:{
				required:"Please enter the image landing URL",	 		
			},*/
			rcImageUrl:{
				required:"Please enter the image URL",
				rangelength:"Image Url Length too large"
			},
			 rcImageDesc:{
				required:"Please enter the image text",	
				rangelength:"Maximum 100 characters permitted"
			}
		},
		errorElement: "div",
		errorPlacement: function(error, element) {
			if ($(element).hasClass("form-control startDate")) {
				error.insertAfter($(element).closest("div"));
			}
			else if ($(element).hasClass("form-control endDate")){
				error.insertAfter($(element).closest("div"));
			}
			else {
				error.insertAfter(element);
			}
		},
		submitHandler: function (form) {
			if($("#position").val() == ""){ $("#position").val(0);}
			form.submit();
		}
	});
		 
	/* for submit the form on key press enter*/
	$(document).keypress(function(event){
		if(event.keyCode == 13){
			$("#recommendedCategoryBean").submit();
		}
	});	
});	
</script>

<!-- Events -->
<script type="text/javascript">
$('#sectionA').addClass('in active');
$('#sectionB').addClass('active');

//-----Default Auto Complete starts
/* var prtNo = $("#prtNo option:selected").val();
$("#prtNo_").val(prtNo);
$("#prtCatNo_").val($("#prtCatNo").val());
$("#rcName_").val($("#rcName").val());

$( "#rcName" ).autocomplete({
	minLength: 2,
	source: function(request, response) {    	  
		$.ajax({
			dataType: "json",
			type: "GET",
			xhrFields: {
				withCredentials: true
			},
			crossDomain: true,
			cache: true,
			data: {
				keyword: request.term
			}, 
			url: "${contextPath}/admin/getPartnerCategory?prtNo="+prtNo,
			success: function(data){           	  
				var array= $.map( data, function(key,value) {
					return {                    	  
						label:key,
						value:value    
					}
				});            	  
				response($.ui.autocomplete.filter(array, request.term));
			},
			error: function (data) {
			}
		})
	},
	select: function(event, ui) {   	  
		$("#prtCatNo").val(ui.item.value); 
		$("#rcName").val(ui.item.label);
		return false;
	}
}); */
//-----Default Auto Complete ends

/* $("#rcName").focusout(function(){
	var prtNo=$("#prtNo").val();
	var prtCatNo=$("#prtCatNo").val();
	var prtCatNo_=$("#prtCatNo_").val();
	
	if(prtCatNo==''){				
		rcNameInvalid=true;
		$("#recommendedCategoryBean").validate().element('#rcName');
	}
	else{
		if(prtCatNo!=prtCatNo_){				
			$.get("${contextPath}/admin/checkRecommendedCategory?prtNo="+prtNo+"&prtCatNo="+prtCatNo, function(data,status) {
				if(status=="success" && data!=''){
					rcNameExist=true;	
				}
				else{
					rcNameExist=false;					
				}					
				rcNameInvalid=false;					
				$("#recommendedCategoryBean").validate().element('#rcName');					
			});
		}
		else{				
			$("#rcName").val($("#rcName_").val());
			rcNameExist=false;
			$("#recommendedCategoryBean").validate().element('#rcName');
		}
	}
}); */

/* $("#prtNo").on( "selectmenuchange", function(){	
	var prtNo = $("#prtNo option:selected").val();  
	$("#rcName").val(''); 
	var prtNo_ = $('#prtNo_').val();
	var rcName = $('#rcName').val();
	
	if ((rcName == '') && (prtNo == prtNo_)) {
		$("#rcName").val($("#rcName_").val());
		$("#prtCatNo").val($("#prtCatNo_").val());
		rcNameInvalid=false;
		$("#recommendedCategoryBean").validate().element('#rcName');       
	} */
	 
	/* $( "#rcName" ).autocomplete({
		minLength: 2,
		source: function(request, response) {    	  
			$.ajax({
				dataType: "json",
				type: "GET",
				xhrFields: {
					withCredentials: true
				},
				crossDomain: true,
				cache: true,
				data: {
					keyword: request.term
				}, 
				url: "${contextPath}/admin/getPartnerCategory?prtNo="+prtNo,
				success: function(data){            	  
					var array= $.map( data, function(key,value) {
						return {                    	  
							label:key,
							value:value    
						}
					});            	  
					response($.ui.autocomplete.filter(array, request.term));
				},
				error: function (data) {
				}
			})
		},
		select: function(event, ui) {    	  
			$("#prtCatNo").val(ui.item.value); 
			$("#rcName").val(ui.item.label);
			return false;
		}
	});
	$("#recommendedCategoryBean").validate().element('#prtNo');
}); */
 
 $( "#rcLandingUrl" ).click(function() {
	 $( "#rcLandingUrl" ).val('');
				 
});

$('#position').on('keyup', function () {
	var recommendedCategoryPosition = $("#position").val();
	var rcPrdPosHidden = $("#positionHidden").val();
	rcPositionExist = false;
	
	if(recommendedCategoryPosition != rcPrdPosHidden && recommendedCategoryPosition != 0){
		$.get("${contextPath}/admin/checkRecommendedCategoryPosition?recommendedCategoryPosition="+recommendedCategoryPosition, function(data,status) {
			if(status=="success" && data!=''){
				rcPositionExist=true;
			}
			else{
				rcPositionExist=false;
			}
			$("#recommendedCategoryBean").validate().element('#position');
		});
	}
	else{
		$("#recommendedCategoryBean").validate().element('#position');
	}
});

$('#datepicker11').on('change', function () {
	$("#recommendedCategoryBean").validate().element('#datepicker11');
	//TODO:AW-284
	var endDate = getFormattedDate($('#datepicker12').val());
	
	if(endDate != 'Invalid Date'){
		$("#recommendedCategoryBean").validate().element('#datepicker12');
	}
}); 

$('#datepicker12').on('change', function () {
	$("#recommendedCategoryBean").validate().element('#datepicker12');
	//TODO:AW-284
	$("#recommendedCategoryBean").validate().element('#datepicker11');
}); 
</script>

<!-- Validation Methods -->
<script type="text/javascript">
$.validator.addMethod('duplicatercPosition', function(value, element) {
	return !rcPositionExist;
});

$.validator.addMethod('positionNotZero', function(value, element) {
	var isValid = false;
	var rcPosition = $("#position").val();
	
	if((rcPosition != "") && (rcPosition == 0)){
		isValid = true;
	}
	
	return !isValid; 
});
$.validator.addMethod('positionLessThan100', function(value, element) {
	var isValid = false;
	var recommendedCategoryPosition = $("#position").val();
	
	if(recommendedCategoryPosition > 100){
		isValid = true;
	}
	return !isValid;
});
$.validator.addMethod('duplicatercName', function(value, element) {
	return !rcNameExist;
});
$.validator.addMethod('rcNameInvalid', function(value, element) {
	return !rcNameInvalid;
});
$.validator.addMethod("AlphaNumeric", function(value, element) {
	return this.optional(element) || /^[a-zA-Z0-9\s\.]+$/i.test(value);
});
$.validator.addMethod("endDate", function(value, element) {
	var startDate = getFormattedDate($('#datepicker11').val());
	var endDate = getFormattedDate($('#datepicker12').val());
	
	if(endDate=='Invalid Date')
		return true;
	if (endDate >= startDate)
		return true;
	if (endDate <= startDate)  
		return false;
});
$.validator.addMethod("startDate", function(value, element) {
	var startDate = getFormattedDate($('#datepicker11').val());
	var endDate = getFormattedDate($('#datepicker12').val());
	
	if(endDate=='Invalid Date')
		return true;
	if (startDate <= endDate)
		return true;
	if (startDate >= endDate)
		return false;
});
$.validator.addMethod('duplicateCatNo', function(value, element) {
	return !rcCatExist;
});
</script>

<!-- Methods -->
<script type="text/javascript">
function getFormattedDate(date){
	var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
	return new Date(date.replace(pattern,'$3-$2-$1'));
}
</script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />
<script type="text/javascript">
var catNoOld=$("#catNo").val();
$(document).on('click','.selectCategory',function(){
	var content = $("#categoryList").val();
	var con = JSON.parse(content);
	$('.select_category').jstree({
			'core' : {
				'data' : con,
				 "check_callback" : true,
			}
		});
});
$(document).on('click','.select_category .jstree-container-ul li a',function(){
	selected_categoryValue = $('.select_category').jstree().get_selected(true)[0].text;
// 	console.log(selected_categoryValue);

	var catno = $(".jstree-clicked").attr("catno");
	$('#categorySelected').val(selected_categoryValue);
	/* if(catNoOld!==catno)
	{ */
	 var rcCatNo = catno;//$("#catNo").val();
	 $("#catNo").val(catno);
	rcCatExist = false;
		$.get("${contextPath}/admin/checkRcCatNo?rcCatNo="+rcCatNo, function(data,status) {
			if(status=="success" && data!=''){
				rcCatExist=true;
			}
			else{
				rcCatExist=false;
			}
			$("#recommendedCategoryBean").validate().element('#catNo');
		});
	 
	/* } */
	/* else{
		 $("#catNo").val(catNoOld);
	} */
	$('.close').click();
});
</script>