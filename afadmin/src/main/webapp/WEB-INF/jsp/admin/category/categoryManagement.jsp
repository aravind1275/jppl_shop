<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<script>
	function changeURL(){
                	history.pushState({}, '',"${contextPath}/admin/categoryManagement");
        }
</script>

<c:set var="contextPath" value="${pageContext.servletContext.contextPath}"  scope="application"/>
<script>
	function changeURL(){
                	history.pushState({}, '',"${contextPath}/admin/categoryManagement");
        }
</script>

<div id="categoriesModal" class="modal fade" role="dialog">	
			<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Select Category</h4>
				</div><!--header-->
				<div class="modal-body">
					<div class="select_category"></div>
				</div><!---body--->
				<div class="modal-footer">
				</div>
			</div><!---modal content--->
			</div>
		</div>


<!-- Modal for delete -->
		<div id="myModalCategory" class="modal fade" role="dialog">	
			<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Are You Sure?</h4>
				</div><!--header-->
				<div class="modal-body">
					<p>
							<img src="${contextPath}/static/img/info_icon1.png"></img>
							<span id="deleteCategoryId"> Category will be deleted. Are you sure?</span> 
					</p>
				</div><!---body--->
				<div class="modal-footer">
					<button id="categoryDeleteID" class="bg_g_button" data-dismiss="modal">Delete</button>
				</div>
			</div><!---modal content--->
			</div>
		</div>	
<!-- Modal for delete -->
		<div id="myModala" class="modal fade" role="dialog">
			<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Are You Sure?</h4>
				</div><!--header-->
				<div class="modal-body">
					<p>
							<img src="${contextPath}/static/img/info_icon1.png"></img>
							<span id="deleteCategoryId"> category will be Deactivated.Are you sure?</span> 
					</p>
				</div><!---body--->
				<div class="modal-footer">
					<button id="btnDelteYes" class="bg_g_button" data-dismiss="modal">Delete</button>
				</div>
			</div><!---modal content--->
			</div>
		</div>	
		
		
		<!-- Modal for delete -->
		<div id="myModalC" class="modal fade" role="dialog">	
			<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Are You Sure?</h4>
				</div><!--header-->
				<div class="modal-body">
					<p>
							<img src="${contextPath}/static/img/info_icon1.png"></img>
							<span id="deleteMappedCategoryId"> category will be Deactivated.Are you sure?</span> 
					</p>
				</div><!---body--->
				<div class="modal-footer">
					<button id="btnD1" class="bg_g_button" data-dismiss="modal">Deactivate</button>
				</div>
			</div><!---modal content--->
			</div>
		</div>	
		
		
		<div id="myModalCUpdate" class="modal fade" role="dialog">	
			<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Are You Sure?</h4>
				</div><!--header-->
				<div class="modal-body">
					<p>
							<img src="${contextPath}/static/img/info_icon1.png"></img>
							<span id="deleteMappedUpdateCategoryId"> category will be Deactivated.Are you sure?</span> 
					</p>
				</div><!---body--->
				<div class="modal-footer">
					<button id="btnDelUpdate" class="bg_g_button" data-dismiss="modal">Deactivate</button>
				</div>
			</div><!---modal content--->
			</div>
		</div>	
		
		
		
		
		<!-- Modal  for edit-category-details-->
        <div id="myModal1" class="modal fade" role="dialog">
        <form:form autocomplete="off"  id="editCategoryBean" 
					commandName="editCategoryBean">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" id="cancelbtnD1_" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Edit Category Details</h4>
                    </div><!----modal-header--->
                    <div class="modal-body1">
                        <div class="row pad_top" >
                            <div class="col-md-4 user_details">
                                <label>Category ID</label>
                               <input type="text" minlength="1" maxlength="50" class="form-control " name="edit_categoryid" id="edit_categoryid">
								<!-- TODO:Check for existing category -->
								<input type="hidden" id="edit_categoryidHidden"/>
                            </div>
                            <div class="col-md-4 user_details">
                                <label>Category Name</label>
                               <input type="text" maxlength="50" class="form-control " name="edit_categoryname" id="edit_categoryname">
                            </div>
                            <div class="col-md-4">
								<label>Start Date</label>
								<div class="pick_date">
									<input type="text" class="form-control startDate" name="datepicker11"  id="datepicker11" />
									
									<!--img src="./img/2016-06-23.png" alt="" ></img-->
								</div>
								<input type="hidden" id="selectStartdate_" name="selectStartdate_"/>
							</div>
                        </div><!----row--->
                        <div class="row pad_top">
							
							<div class="col-md-4">
								<label>End Date</label>
								<div class="pick_date">
									<input type="text" class="form-control endDate" name="datepicker12" id="datepicker12" />
									
									<!--img src="./img/2016-06-23.png" alt="" ></img-->
								</div>
								<input type="hidden" id="selectEnddate_" name="selectEnddate_"/>
							</div>
							<div class="col-md-6">
								<label>Status</label>
								<div class="half-col last">
									<ul class="list-inline">
										<c:forEach items="${categoryStatus}" var="status">
														<li><label> 														
														<input type="radio"	name="edit_categorystatus" class="css-checkbox"
																id="edit_categorystatus_${status.key}" value="${status.key}"  />
																
																<label for="edit_categorystatus_${status.key}" class="css-label radGroup2">${status.value}</label>														 																
														</label></li>
													</c:forEach>
									</ul>
								</div>
							</div>
						</div> <!------------- row ----------->
						<div class="row btn_save">
							<input class="bg_g_button" type="button" id="updateCategory" value="Update" />
						</div><!-------------button----------->
                    </div>
                </div>
            </div>
            </form:form>
        </div>
        
		<div id="existing_category_view" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content for view details-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Existing Cartegory Details</h4>
                    </div><!----modal---header--->
                    <div class="modal-body2">
                        <div class="row">
                            <div class='col-md-6 user_details' >
                                <label>Category Id</label>
                               <p id="categoryid" class="bannerNameClass"></p>
                            </div>
                            <div class='col-md-6 user_details ' >
                                <label>Category Name</label>
                               <p id="categoryname"></p>
                            </div>
                        </div><!----row--->
                        <div class="row">
                            <div class='col-md-6 user_details' >
                                <label>Start Date</label>
                               <p id="startdate"></p>
                            </div>
                            <div class=' col-md-6 user_details ' >
                                <label>End Date</label>
                               <p id="enddate"></p>
                            </div>
                        </div><!----row--->	
						<div class="row">
							<div class='col-md-6 user_details' >
                                <label>Category Status</label>
                                <p id="categorystatus"></p>
                            </div>
                        </div><!----row--->
                    </div><!----modal-body---->
                </div>
            </div>
        </div>
        
        <!-- Modal content for view Mapped Category Details----------------------------------->
        <div id="myModalA" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content for view details-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Mapped Category Details</h4>
                    </div><!----modal---header--->
                    <div class="modal-body2">
                        <div class="row">
                            <div class='col-md-6 user_details' >
                                <label>Partner Name</label>
                               <p id="partnername_"></p>
                            </div>
                            <div class='col-md-6 user_details ' >
                                <label>Category Name</label>
                             <p id="categoryname_"></p>
                            </div>
                        
						
                            <div class='col-md-6 user_details' >
                                <label>Partner Category Name</label>
                               <p id="partnerCategoryname_"></p>
                            </div>
                           </div><!----row--->
                   
                    </div><!----modal-body---->
                </div>
            </div>
        </div>
        <!-- jagjeet -->
        <div id="myModalAUpdate" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content for view details-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Mapped Category Details</h4>
                    </div><!----modal---header--->
                    <div class="modal-body2">
                        <div class="row">
                            <div class='col-md-6 user_details' >
                                <label>Partner Name</label>
                               <p id="partnername_update"></p>
                            </div>
                            <div class='col-md-6 user_details ' >
                                <label>Category ID</label>
                             <p id="partnerCategoryId"></p>
                            </div>
                        
						
                            <div class='col-md-6 user_details' >
                                <label>Partner Category Name</label>
                               <p id="partnerCategoryname_update"></p>
                            </div>
                           </div><!----row--->
                   
                    </div><!----modal-body---->
                </div>
            </div>
        </div>
        
        
        
        
        
        
        
		 <!-- Partner Cartegory modal Edit details----------------------------------->
        <div id="Partner_category" class="modal fade" role="dialog">
        <form:form autocomplete="off"  id="editmapcategoryBean" 
					commandName="editmapcategoryBean">
		<div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" id="cancelbtnD2_" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Edit mapped Category Details</h4>
                    </div><!----modal-header--->
                    <div class="modal-body1">
                        <div class="row pad_top" >
                            <div class="col-md-4 user_details">
                                <label>Partner Name</label>
                                 <input type="text" maxlength="25"
												class="form-control " name="edit_partnername_" id="edit_partnername_" readonly>
												<input type="hidden" maxlength="11"
												class="form-control " name="edit_partnerno_" id="edit_partnerno_" readonly>
                            </div>
                            <div class="col-md-4 user_details">
                                <label>Partner Category Name</label>
                                <input type="text" maxlength="150"
												class="form-control " name="edit_partnercategoryname_" id="edit_partnercategoryname_" readonly>
                            </div>
                            
								<div class="col-md-4">
									<div class='text top' >
									<label>Partner Category Status</label>
									<div class="half-col last">
										<ul class="list-inline">
											<c:forEach items="${prtCatStatus}" var="status">
														<li><label> 
														<input type="radio"	name="edit_partcategorystatus" class="css-checkbox"	id="edit_partcategorystatus_${status.key}" value="${status.key}"/>
																<label for="edit_partcategorystatus_${status.key}" class="css-label radGroup2">
																	${status.value}
																</label>
													
														</label></li>
											</c:forEach>
										</ul>
									</div>
								</div>
								</div>
								
                            </div><!--- row --->
                            <div class="row pad_top">
                            <div class="col-md-6">
                          	  <label>Category Name</label>									
								<input type="text" class="form-control" name="categoryNo_" id="categoryName_details" readonly />	
								<input type="hidden" class="form-control" name="catgry_no" id="catgry_no" />	
							
								<a href="javascript:void(0);" class="selectCategory selectCategoryLink">Click here to View Categories</a>
						
                            </div>
                            	<div class="col-md-6 category_list_divDetails">
										<strong>Category List</strong>
										<div class="category_list_details">
										</div>
									</div>
								
                       		 </div><!----row--->
                      	 
						
					</div> <!----  row  --->
					<div class="row btn_save align_right" >
						<input class="bg_g_button" type="button"  id="updateMappedCategory" value="Update" />
					</div><!-------- button ------>
                </div>
            </div>
            </form:form>
        </div>
        
        <!-- jagjeet update category -->
        
        
        <div id="Partner_categoryUpdate" class="modal fade" role="dialog">
        <form:form autocomplete="off"  id="editmapcategoryBeanUpdate" 
					commandName="editmapcategoryBeanUpdate">
		<div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" id="cancelbtnD2Up_" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Edit mapped Category Details</h4>
                    </div><!----modal-header--->
                    <div class="modal-body1">
                        <div class="row pad_top" >
                            <div class="col-md-4 user_details">
                                <label>Partner Name</label>
                                 <input type="text" maxlength="25"
												class="form-control " name="edit_partnername_update" id="edit_partnername_update" readonly>
												<input type="hidden" maxlength="11"
												class="form-control " name="edit_partnerno_update" id="edit_partnerno_update" readonly>
                            </div> 
                            <!-- edit_partnercategoryname_update edit_partnercategoryId_update Partner_categoryUpdate -->
                            <div class="col-md-4 user_details">
                                <label>Partner Category Name</label>
                                <input type="text" maxlength="150"
												class="form-control " name="edit_partnercategoryname_update" id="edit_partnercategoryname_update">
                            </div>
                            
								<div class="col-md-4">
									<div class='text top' >
									<label>Partner Category Status</label>
									<div class="half-col last">
										<ul class="list-inline">
											<c:forEach items="${prtCatStatus}" var="status">
														<li><label> 
														<input type="radio"	name="edit_partcategorystatusupdate" class="css-checkbox"	id="edit_partcategorystatus_update_${status.key}" value="${status.key}"/>
																<label for="edit_partcategorystatus_update_${status.key}" class="css-label radGroup2">
																	${status.value}
																</label>
													
														</label></li>
											</c:forEach>
										</ul>
									</div>
								</div>
								</div>
								
                            </div>
                             <div class="row pad_top" >
                             <div class="col-md-4">
                             <label>Partner Category ID</label>
                             <input type="text" maxlength="150"
												class="form-control " name="edit_partnercategoryId_update" id="edit_partnercategoryId_update">
                             </div>
                             </div><!--- row --->
                            
                      	 
						
					</div> <!----  row  --->
					<div class="row btn_save align_right" >
						<input class="bg_g_button" type="button"  id="updateMappedCategoryPart" value="Update" />
					</div><!-------- button ------>
                </div>
            </div>
            </form:form>
        </div>
        
        
  <!-- completes -->       
        
        
        
        <div class='container'>            
            <section class="insurance_management">
                <div id ="breadcrumb">
                    <ul>
                        <li>Home</li>
						<li>Configuration</li>
						<li>Category Management</li>
                    </ul>
                </div>  <!---breadcrumb--->
				<div class="section_heading">
				<c:if test="${fn:length(message) gt 0}"><script>changeURL();</script><div class="alertMessage" style="color:green;font-weight: bold;"><c:out value="${message}"></c:out></div></c:if>
	  			<c:if test="${fn:length(errormessage) gt 0}"><script>changeURL();</script><div class="alertMessage" style="color:red;font-weight: bold;"><c:out value="${errormessage}"></c:out></div> </c:if>
				<input type="hidden" name="bsCatNames" id="bsCatNames" value="${bestSellingCategories}" />
				<h5>Category Management</h5> 
				</div><!--- section heading ---->
				<div class="management"><!---Category Management--->
                    <ul class="nav nav-tabs">
                       <!-- <li class="active" id="existingCategories" ><a data-toggle="tab" href="#sectionA">Existing Category List</a></li> -->
                        <li class="active" id="createCategory" ><a data-toggle="tab" href="#sectionB">Category</a></li>
						<li  id="mapCategory" ><a data-toggle="tab" href="#sectionC">Mapping Partner Categories</a></li>
						<li  id="UpdatePartnerCategory" ><a data-toggle="tab" href="#sectionE">Partner Categories Listing</a></li>
						<li  id="CreatePartnerCategory" ><a data-toggle="tab" href="#sectionD">Create Partner Categories</a></li>
                       <li  id="amazonPartnerCategory1" ><a data-toggle="tab" href="#sectionF">Amazon Partner Categories</a></li>
						<li  id="amazonCreateCategory" ><a data-toggle="tab" href="#sectionG">Create Amazon Partner Categories</a></li>
                    </ul>
                    <div class="tab-content">
                       
                        
                        <!---- section B start ---->
                        <div id="sectionB" class="tab-pane fade in active" ><!-----create category starts here---->
	                        <form:form autocomplete="off" id="categoryBean" commandName="categoryBean">
	                        <form:input type="hidden" path="getCategoryList" id="getCategoryList" value="${requestScope.categoriesList}" />
	                        <form:input type="hidden" path="action" id="actionID" value="" />
	                        <form:input type="hidden" path="categoryId" id="addCategoryId" value="" />
	                        <form:input type="hidden" path="categoryNo" id="addcategoryNoId" value="" />
	                        
		                        <div class="row">
		                        	<div class="col-md-3 category_list_div">
										<strong>Category List</strong>
										<div class="category_list">
										</div>
									</div>
									<div class="col-md-9 category_details">									 
										<ul class="nav nav-tabs">
											<li class="active viewCategory" id="viewCategory"><a data-toggle="tab" onclick="viewCategory(this)">View Category</a> </li>
											<li class="addCategory" id="addCategory"><a data-toggle="tab" onclick="addCategory(this)">Add Category</a> </li>
											<li class="addSubCategory" id="addSubCategory"><a data-toggle="tab" onclick="addSubCategory(this)">Add Sub Category</a></li>
										</ul>
										
										<div id="view_category">
			                            	<div class="row pad_top">
					                            <div class='col-md-4 user_details' >
					                                <label>Category Type</label>
					                               <p id="categoryTypeID_"></p>
					                            </div>
<!-- 					                            <div class='col-md-4 user_details' > -->
<!-- 					                                <label>Category ID</label> -->
<!-- 					                               <p id="categoriesID_"></p> -->
<!-- 					                            </div> -->
					                            
					                            <div class='col-md-4 user_details ' >
					                                <label>Category Name</label>
					                             <p id="categorynameID_"></p>
					                            </div>
					                            <div class='col-md-4 user_details' >
					                                <label>Status</label>
					                               <p class="green" id="activeStatusID_"></p>
					                               <p class="red" id="inactiveStatusID_"></p>
					                            </div>
					                        </div><!----row--->
					                         <div class="row pad_top">
			                            		<div class="col-md-4">
													<label>Filter Start Value</label>
													<p id="startValueID_"></p>	
												</div>
			                            		<div class="col-md-4">
													<label>Filter End Value</label>
													<p id="endValueID_"></p>	
												</div>
			                            		<div class="col-md-4">
													<label>Filter Step Value</label>
													<p id="stepValueID_"></p>	
												</div>
			                            	</div>
			                            	<div class="row pad_top pad_bottom">
					                            <div class='col-md-4 user_details' >
					                                <label>Start Date</label>
					                                <p id="startDateID_"></p>
					                            </div>
					                            <div class='col-md-8 user_details ' >
					                                <label>End Date</label>
					                             <p id="endDateID_"></p>
					                            </div>
<!-- 												<div class='col-md-4 user_details' > -->
<!-- 					                                <label>Status</label> -->
<!-- 					                               <p class="green" id="activeStatusID_"></p> -->
<!-- 					                               <p class="red" id="inactiveStatusID_"></p> -->
<!-- 					                            </div> -->
					                        </div><!----row--->
					                        <div class='col-md-4 user_details ' ></div>
					                        <div class='col-md-4 user_details ' ></div>
					                        <div class='col-md-4 user_details ' >
					                            <a class="dt-button export-excel-categroy buttons-excel buttons-html5" tabindex="0">
													<span>export to excel</span>
												</a>
				                            </div>
					                        <div></div>
										</div>
										
										<!-- Add Category Start  -->
										<div id="add_category">
				                            <div class="row pad_top">
				                                <%-- <div class="col-md-4">
													<label>Category ID</label><span class="mandatory_star">*</span>
													<form:input minlength="1" maxlength="50" type='text' path="categoryId" class="form-control categoryId_edit" />	
												</div> --%>
				                                <div class="col-md-4">
													<label>Category Name</label><span class="mandatory_star">*</span>
													<form:input type='text' path="categoryName" class="form-control categoryName_edit" />
													<input type="hidden" name="categoryNameHidden" id="categoryNameHidden" />
												</div>
				                                <div class="col-md-4">
													<label>Start Date</label><span class="mandatory_star">*</span>
													<div class="pick_date">
													<form:input  class="form-control startDate" id='datepicker1' path="startDate" readonly="true"/>
														<!--img src="./img/2016-06-23.png" alt="" ></img-->
													</div>
												</div>
												<div class="col-md-4">
													<label>End Date</label>
													<div class="pick_date">
														<form:input  class="form-control endDate" id='datepicker2' path="endDate" readonly="true"/>
														<!--img src="./img/2016-06-23.png" alt="" ></img-->
													</div>
												</div>
				                            </div>
			                            	<div class="row pad_top">
			                            		<div class="col-md-4">
													<label>Filter Start Value</label>
													<form:input minlength="1" maxlength="6" type='text' path="startValue" id="startValueID" class="form-control" />	
												</div>
			                            		<div class="col-md-4">
													<label>Filter End Value</label>
													<form:input minlength="1" maxlength="6" type='text' path="endValue" id="endValueID" class="form-control" />	
												</div>
			                            		<div class="col-md-4">
													<label>Filter Step Value</label>
													<form:input minlength="1" maxlength="6" type='text' path="stepValue" id="stepValueID" class="form-control" />	
												</div>
			                            	</div>
				                            <div class="row pad_top">
				                            	<div class="col-md-4">
				                                    <div class='text top' >
				                                        <label>Status</label><span class="mandatory_star">*</span>
				                                        <div class="half-col last">
				                                            <ul class="list-inline">
				                                               <c:forEach items="${categoryStatus}" var="status">
																<li>
																<label>
																<form:radiobutton path="categoryStatusCode" class="css-checkbox" id ="${status.key}" value="${status.key}" />
																<label for="${status.key}" class="css-label radGroup2">${status.value}</label>
																</label>
																</li>
																</c:forEach>
				                                            </ul>
				                                        </div>
				                                    </div>
				                                </div>
				                                
				                                <div class="col-md-4">
													<form:checkbox path="goQuicklyCategory" id="goQuicklyCategory" value="0" class="css-checkbox checkBoxMargin" />
													<label>Go Quickly Category</label>
												</div>
												<div class="col-md-4">
													<label>Go Quickly Display Order</label><span class="gqDisplayOrder mandatory_star">*</span>
													<form:input type="text" maxlength="2" path="gqcDisplayOrder" name="gqcDisplayOrder" id="gqcDisplayOrder" class="form-control sort_order"/>
													<input type="hidden" name="gqcDisplayOrderHidden" id="gqcDisplayOrderHidden" />
												</div>
				                                <div class="col-md-4" >
				                                	<div id="categoriesDivID">
														<label>Category ID</label>
														<input type='text' id="categoriesID" class="form-control" readonly/>
													</div>
												</div>
											</div>
											
											<!-- ****************************** -->
											<div class="row pad_top">
												<%-- <div class="col-md-4">
                          	  						<label>Category Image URL</label><span class="mandatory_star">*</span>							
													<form:input type="text" class="form-control" path="categoryImageUrl" name="categoryImageUrl" id="categoryImageUrl" />		
                            					</div> --%>
                            					
			                            		<div class="col-md-4">
													<form:checkbox path="bestSellingCategory" id="bestSellingCategory" name="bestSellingCategory" value="0" class="css-checkbox checkBoxMargin" />
													<input type="hidden" id="bestSellingCategoryHidden" />
													<label>Top Selling Category</label>
												</div>
												
												<div class="col-md-4">
                          	  						<label>Linked Category</label>								
													<input type="text" class="form-control" name="linkedCategoryName" id="linkedCategoryName" readonly />		
													<form:hidden path="linkedCategory" id="linkedCategory" />
													<a href="javascript:void(0);" class="selectLinkedCategory selectCategoryLink">Click here to View Categories</a>
                            					</div>
			                            	</div>
			                            	
			                            	<div class="row pad_top pad_bottom">
												<div class="col-md-4">
													<label>Category Image URL</label>
													<input id="uploadFile" class="upload_file" placeholder="Choose File" disabled="disabled" />
													<div class="fileUpload btn padTop_Btm">
														<span class="bg_w_button">Browse</span>
														<!-- TODO: Banner image placement -->
														<input id="categoryImage" name="categoryImage" type="file" class="upload categoryImageUpload bwrsBtnAddPar"  />
													</div>
												</div><!---- browse button --->
				                                <div class="col-md-4 image_wrap image_wrap1 noMar-top">
				                                     <img id="categoryImage_" src="" style="max-height: 100%; max-width: 100%;"/>
				                                </div>
												<div class="col-md-2">
													<input class="bg_g_button" type="button" value="Upload" onclick="uploadImage('categoryImage','categoryImageUrl')"/>
													<form:hidden path="categoryImageUrl" id="categoryImageUrl"/>
													<!-- <div id="newBannerError" class="customErrorMessageDisp error"></div> -->
												</div>		
											</div>
			                            	
											<!-- ************************************ -->
											
											<div class="row btn_save pull-right" id="categoryButtonID">
												<input class="bg_g_button" type="button" id="cancelbtnD" value="Cancel" />
												<input class="bg_g_button" type="button" onClick="$(this).closest('form').submit();" value="Save" />
											</div>
										</div>
	                                </div>
	                                
								</div>
	                            <div class="row btn_save pull-right" id="categoryButtonID">
								</div><!---- button ------->
		                    </form:form>
                        </div><!---- section B end ---->
                       
						<div id="sectionC" class="tab-pane fade in "><!---- partner category starts here ---->
                            <nav class="navbar navbar-default" role="navigation" id="submenu">
							<h5 class="table_heading">Mapping Partner Category Details</h5><!---- table header ---->
							<form:form autocomplete="off" id="mapcategoryBean" action="mapPartnerCategories"
							commandName="partnercategoryBean" >
				
							 <div class="row pad_top">
								<div class="col-md-4">
									<label>Partner</label>
									<fieldset class="infield">
										<form:select path="partNO" id="partnerNo" class="select_option">
             							 <form:option value="">--Select--</form:option>
             	 						<c:forEach items="${partnerList}" var="partner">
	             					      <li>
	             					      <form:option value="${partner.prtNo}" label="${partner.partnerName}" />
	             						</li>
	             						</c:forEach>
	             					</form:select>
									</fieldset>	
								</div>
                            </div> <!--- row --->
                            <table class=" partner_info_table table-bordered" cellspacing="0" cellpadding="0" width="98%">
								<thead>
									<tr>
										<th class="name">Sl No.</th>
										<th>Partner Name</th>
										<th>Category Name</th>
										<th>Partner Category Name</th>
										<th>Status</th>
										<th class="width_section">Action</th>
									</tr>
								</thead><!----thead--->
								<tbody>
								<c:forEach items="${mappingDetailsList}" var="mappingdetails" varStatus="i">
							
									<tr class="admin"
									data-id="${mappingdetails.prtCatNo}" data-categoryid_="${mappingdetails.categoryId}"
							        data-category_partnerno_="${mappingdetails.partNO}"
									data-category_categoryno_="${mappingdetails.catNo}"
									data-category_partnercategoryname_="${mappingdetails.partnercategoryName}"
									data-category_partnername_="${mappingdetails.partnerName}"
									data-category_partnerid_="${mappingdetails.partnerID}"
									data-categoryname_="${mappingdetails.categoryName}"
									data-categorystatus_="${mappingdetails.categorySatus}" 
								   	data-categorystatuscode_="${mappingdetails.categoryStatusCode}" 
									data-categoryimageurl_="${mappingdetails.categoryImageUrl}"
									data-category_partnercategoryno_="${mappingdetails.prtCatNo}"
									data-categorystatusclass_="${mappingdetails.categoryStatusClass}">
										<td>${i.count}</td>
										<td>${mappingdetails.partnerName}</td>
										<td>${mappingdetails.categoryName}</td>
										<td>${mappingdetails.partnercategoryName}</td>
										<td class="${mappingdetails.categoryStatusClass} td_width">${mappingdetails.categorySatus}</td>
										<td>
											<span href="javascript:void(0);" class="view_mapped_details" data-target="#myModalA"><img src="${contextPath}/static/img/View_details_icon1.png"></img></span>
											
											<span class="editmappedcategory"  data-target="#Partner_category">
												<img src="${contextPath}/static/img/edit_icon.png" class="edit_wraper" ></img>
											</span>
											<c:if test="${mappingdetails.categoryStatusCode eq 1}"> 
											<span  id="deleteMappedCategory" data-target="#myModalC">
												<img src="${contextPath}/static/img/deactivate_icon.png"  class="fa fa-trash fa-lg"></img>
											</span>
											</c:if>
											<c:if test="${mappingdetails.categoryStatusCode eq 0}"> 
											<span  id="deleteMappedCategory" data-target="#myModalC">
												<img src="${contextPath}/static/img/deactivate_icon.png"></img>
											</span>
											</c:if>
										</td>
									
									</tr> 
									</c:forEach>
								</tbody><!---tbody--->
							</table><!---table-->	
                           </form:form>
                           <div class="row btn_save pull-right">
								<input class="bg_g_button" type="button" id="cancelbtnD1" value="Cancel" />
								
							</div>
                            </nav>
                         </div><!----section C ----> 
                         <div id="sectionD" class="tab-pane fade in ">
                         
                          <form:form autocomplete="off" method="GET" id="createPartnerCatForm"
								commandName="createPartnerCatForm" >
                         
                          <div class="row pad_top">
								<div class="col-md-4">
								<label>Select Partner Type</label>
								<select name="partnerType" id="partnerType" class="select_option">
											<option value="">---Select---</option>
  											<option value="host">Host Partner</option>
  											<option value="default">Search Partner</option>
  											<option value="logo">Logo Partner</option>
  											<option value="offline">Offline Partner</option>
  											<option value="talk">Talk Partner</ption>
  											<option value="read">Read Partner</option>
 								</select>
								
								</div>
						 </div>
					 
						  <div class="row pad_top">
								<div class="col-md-4">
								<label>Select Partner</label>
								<select name="partName" id="partName" class="select_option">
											<option value="">---Select---</option>
 								</select>
								</div>
						 </div>
						 
						 <div class="row pad_top">
								<div class="col-md-4">
								<label>Category Name</label><span class="mandatory_star">*</span>
								<input type="text" class="form-control" id="createCategoryName" name="createCategoryName"/>
								</div>
								
								<div class="col-md-4">
								<label>Category ID</label><span class="mandatory_star">*</span>
								<input type="text" class="form-control" id="createCategoryId" name="createCategoryId"/>
								</div>
								
								<div class="col-md-4">
									<div class='text top' >
									<label>Status</label><span class="mandatory_star">*</span>
									<div class="half-col last">
										<ul class="list-inline">
											<c:forEach items="${categoryStatus}" var="status">
														<li><label> 
														<input type="radio"	name="partcategorystatusupdate" class="css-checkbox"	id="partcategorystatus_update_${status.key}" value="${status.key}"/>
																<label for="partcategorystatus_update_${status.key}" class="css-label radGroup2">
																	${status.value}
																</label>
													
														</label></li>
											</c:forEach>
										</ul>
									</div>
								</div>
								</div>
								
								
								
						 </div>
						 
						 <div class="row btn_save pull-right">
						   <input class="bg_g_button" type="button" value="Cancel" id="cancelCreatePartCat" />
						   <button type="button" class="bg_g_button" id="createPartnerSave">Save</button>
						   </div>
						 </form:form>
                         </div> <!----section D ---->
                         
                         <div id="sectionE" class="tab-pane fade in ">
							<nav class="navbar navbar-default" role="navigation">
							<h5 class="table_heading">Partner Category Details</h5><!---- table header ---->
							<form:form autocomplete="off" id="mapPartnerCategoryBean" action="mapCreatePartnerCategories"
							method="POST" >
                         <div class="row pad_top">
								 <div class="col-md-4">
								<label>Select Partner Type</label>
								<select id="partnerTypeUp"  class="select_option">
											<option value="">---Select---</option>
  											<option value="host">Host Partner</option>
  											<option value="default">Search Partner</option>
  											<option value="logo">Logo Partner</option>
  											<option value="offline">Offline Partner</option>
  											<option value="talk">Talk Partner</option>
  											<option value="read">Read Partner</option>
 								</select>
 								<c:if test="${mapCategoryUpdateTab==true}">
		 								<input type="hidden" id="hiddenPrtType" value="${partnertypeup}">
		 								</c:if>
								</div> 
								
								<%-- <label>Select Partner</label>
								  <select path="partNO" id="partNameUp" class="select_option">
											<option value="">---Select---</option>
	 								</select> --%>
 							<div class="col-md-4">	
						<label>Select Partner</label>
						
							<select path="partNO" id="partNameUp" class="select_option">
								<option value="">-Select Partner-</option>
								<c:forEach items="${partnerList}" var="partner">
									<c:choose>
										<c:when test="${partner.prtNo eq partnameup}">
											<option value="${partner.prtNo}" selected >${partner.partnerName}</option>
										</c:when>
										<c:otherwise>
											<option value="${partner.prtNo}" >${partner.partnerName}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
							<c:if test="${mapCategoryUpdateTab==true}">
		 									<input type="hidden" id="hiddenPrtNo" value="${partnameup}">
		 								</c:if>  
					        </div>
 					</div><!-- ------row------ -->
	
	<table class=" partner_info_table table-bordered" cellspacing="0" cellpadding="0" width="98%">	
	<thead>
			<tr>
				<th class="name">Sl No.</th>
				<th>Partner Name</th>
				<th>Partner category Name</th>
				<th>Partner Category ID</th>
				<th>Status</th>
				<th class="width_section">Action</th>
			</tr>
	</thead><!----thead--->	
	<tbody>
								<c:forEach items="${mappingDetailsListCat}" var="mappingdetailscat" varStatus="i">
							
									<tr class="admin"
									data-idup="${mappingdetailscat.prtCatNo}" 
									data-categoryupdateid_="${mappingdetailscat.categoryId}"
									data-category_partnernomap_="${mappingdetailscat.partNO}"
									data-categoryupdateno="${mappingdetailscat.catNo}"
									data-category_nameupdate_="${mappingdetailscat.categoryName}"
									data-category_updatepartnername_="${mappingdetailscat.partnerName}"
									data-category_updatepartnercategoryname_="${mappingdetailscat.partnercategoryName}"
									data-category_updatepartnerid_="${mappingdetailscat.partnerID}"
									data-category_updateid="${mappingdetailscat.categoryId}"
								    data-categoryupdatename_="${mappingdetailscat.categoryName}"
									data-categoryupdatestatus_="${mappingdetailscat.categorySatus}" 
								   	data-categoryupdatestatuscode_="${mappingdetailscat.categoryStatusCode}" 
								   	data-part_category_updateid="${mappingdetailscat.partnerCategoryId}"
								   	data-partcategorynumber="${mappingdetailscat.prtCatNo}"
									data-categoryupdatestatusclass_="${mappingdetailscat.categoryStatusClass}">
										<td>${i.count}</td>
										<td>${mappingdetailscat.partnerName}</td>
										<td>${mappingdetailscat.partnercategoryName}</td>
										<td>${mappingdetailscat.partnerCategoryId}</td>
										<td class="${mappingdetailscat.categoryStatusClass} td_width">${mappingdetailscat.categorySatus}</td>
										<td>
											<span href="javascript:void(0);" class="view_mapped_detailsUpdate" data-target="#myModalAUpdate"><img src="${contextPath}/static/img/View_details_icon1.png"></img></span>
											
											<span class="editmappedcategoryUpdate"  data-target="#Partner_categoryUpdate">
												<img src="${contextPath}/static/img/edit_icon.png" class="edit_wraper" ></img>
											</span>
											<c:if test="${mappingdetailscat.categoryStatusCode eq 1}"> 
											<span  id=deleteMappedUpdateCategory data-target="#myModalCUpdate">
												<img src="${contextPath}/static/img/deactivate_icon.png"  class="fa fa-trash fa-lg"></img>
											</span>
											</c:if>
											<c:if test="${mappingdetailscat.categoryStatusCode eq 0}"> 
											<span  id="deleteMappedUpdateCategory" data-target="#myModalCUpdate">
												<img src="${contextPath}/static/img/deactivate_icon.png"></img>
											</span>
											</c:if>
										</td>
									
									</tr> 
									</c:forEach>
								</tbody><!---tbody--->					
	</table></form:form>	</nav>  					
                         </div><!----section E ---->
                         <!--Section F starts  -->
                        <div id="sectionF" class="tab-pane fade in "><!---- partner category starts here ---->
                            <nav class="navbar navbar-default" role="navigation" id="submenu">
							<h5 class="table_heading">Mapping Amazon Category Details</h5><!---- table header ---->
							<form:form autocomplete="off" id="afAmzonoPartnerCategoryBean" action="mapAmazonPartnerCategories"
							commandName="afAmzonoPartnerCategoryBean" >
							
                            <table class=" partner_info_table table-bordered" cellspacing="0" cellpadding="0" width="98%">
								<thead>
									<tr>
										<th class="name">Sl No.</th>
										<th>Partner Name </th>
										<th>Amazon Partner Category </th>
										<th>Amazon Order Category </th>
										<th>Status</th>
										<th class="width_section">Action</th>
									</tr>
								</thead><!----thead--->
								<tbody>
								<c:forEach items="${amazonMappingDetailsList}" var="mappingdetails" varStatus="i">
									<tr class="admin" 
									data-id="${mappingdetails.amzCatNo}" 
									data-prtname="${mappingdetails.prtName}"
									data-prtno="${mappingdetails.prtNo}"
									data-prtcatno="${mappingdetails.prtCatNo}"
									data-prtcatname="${mappingdetails.prtCatName}"
									data-amazoncategoryid="${mappingdetails.amzCategoryId}"
									data-category_amzcategoryname_="${mappingdetails.amzOrderCategoryName}"
									data-amazoncategorystatus="${mappingdetails.amazonCategoryStatusCode}">
										<td>${i.count}</td>
										<td>${mappingdetails.prtName}</td>
										<td>${mappingdetails.prtCatName}</td>
										<td>${mappingdetails.amzOrderCategoryName}</td>
										<td class="${mappingdetails.amazonCategoryStatusClass} td_width">${mappingdetails.amazonCategoryStatus}</td>
										
										<td>											
											<span class="editmappedcategoryAmazon"  data-target="#editAmazonCategory">
												<img src="${contextPath}/static/img/edit_icon.png" class="edit_wraper" ></img>
											</span>
										</td>
									
									</tr> 
									</c:forEach>
								</tbody><!---tbody--->
							</table><!---table-->	
                           </form:form>
                          
                            </nav>
                         </div>
                         
                         <!--section F ends  -->
                         
                         
                       
                       
                       <div id="sectionG" class="tab-pane fade in ">
                         
                          <form:form autocomplete="off" method="GET" id="createAmzonPartnerCatForm"
								commandName="createAmzonPartnerCatForm" >
                            <nav class="navbar navbar-default" role="navigation" id="submenu">
						 <div class="row pad_top">
								<div class="col-md-4">
								<label for="country">Partners</label><span
									class="mandatory_star">*</span>
								<form:select path="prtNo" name="amazonPrtNo" id="amazonPrtNo"
									class="select_option">
									<form:option value="">--Select--</form:option>
									<c:forEach items="${partnerList}" var="partner">
										<li><form:option value="${partner.prtNo}"
												label="${partner.partnerName}" /></li>
									</c:forEach>
								</form:select>
							</div>
							
								<div class="col-md-4">
								<label for="state">Amazon Partner Categories</label>
								<!-- <span class="mandatory_star">*</span> -->
								<form:select path="prtCatNo" name="amazonPartnerCategory"
									id="amazonPartnerCategory" class="select_option">
									<form:option value="">--Select--</form:option>
								</form:select>
							</div>
                            
						 </div> 
						   <div class="row pad_top">
								<div class="col-md-4">
								<label>Amazon Category ID</label><span class="mandatory_star">*</span>
								<input type="text" class="form-control" id="amazonOrderCategoryId" name="amazonOrderCategoryId"/>
								</div>
                       		 </div><!----row--->
						 <div class="row pad_top">
								<div class="col-md-4">
								<label>Amazon Order Category </label><span class="mandatory_star">*</span>
								<input type="text" class="form-control" id="amazonOrderCategoryName" name="amazonOrderCategoryName"/>
								</div>
                       		 </div>
						 <div class="row pad_top">
								<div class="col-md-4">
									<div class='text top' >
									<label>Status</label><span class="mandatory_star">*</span>
									<div class="half-col last">
										<ul class="list-inline"> 
											<c:forEach items="${categoryStatus}" var="status">
														<li><label> 
														<input type="radio"	name="partcategorystatusupdate1" class="css-checkbox"	id="partcategorystatus_updated_${status.key}" value="${status.key}"/>
																<label for="partcategorystatus_updated_${status.key}" class="css-label radGroup2">
																	${status.value}
																</label>
													
														</label></li>
											</c:forEach>
										</ul>
									</div>
								</div>
								</div>
								
								
								
						 </div>
						 
						 <div class="row btn_save pull-right">
						   <input class="bg_g_button" type="button" value="Cancel" id="cancelCreatePartCat" />
						   <button type="button" class="bg_g_button" id="createAmazonPartnerSave">Save</button>
						   </div>
						   </nav>
						 </form:form>
                         </div> <!----section G ---->
                         
                        
                       
                    </div><!-----tab-content---->
                </div>	<!---- user managemnt----->	 
            </section><!----section ---->  
        </div><!----container ---->
        
    <!-- Display sub category start  -->
	<div id="add_subCategory" class="hidden">
		<form:form autocomplete="off" name="categoryBeanId" id="categoryBeanId" commandName="categoryBeanId">
			<form:input type="hidden" path="categoryId" id="categoryId_" />
				<div class="row pad_top">
					<div class="col-md-4">
						<label>Category Name</label><span class="mandatory_star">*</span>
						<form:input maxlength="50" type='text' path="parentCategoryName" class="form-control categoryName" placeholder="Select Category From List" disabled="true"/>
					</div>
		            <div class="col-md-4">
						<label>Sub Category Name</label><span class="mandatory_star">*</span>
						<form:input type='text'  path="categoryName" id="categoryName" class="form-control" />
					</div>
					<div class="col-md-4">
					</div>
					<%-- <div class="col-md-4">
						<label>Sub Category ID</label><span class="mandatory_star">*</span>
						<form:input minlength="1" maxlength="50" type='text' path="categoryId" id="categoryId" class="form-control categoryId" />	
					</div> --%>
		        </div>
		        <div class="row pad_top">
					<div class="col-md-4">
						<label>Filter Start Value</label>
						<form:input minlength="1" maxlength="6" type='text' path="startValue" id="subStartValueID" class="form-control categoryId_edit" />	
					</div>
					<div class="col-md-4">
						<label>Filter End Value</label>
						<form:input minlength="1" maxlength="6" type='text' path="endValue" id="subEndValueID" class="form-control categoryId_edit" />	
					</div>
					<div class="col-md-4">
						<label>Filter Step Value</label>
						<form:input minlength="1" maxlength="6" type='text' path="stepValue" id="subStepValueID" class="form-control categoryId_edit" />	
					</div>
				</div>
				
				<div class="row pad_top">
					<div class="col-md-4">
						<label>Start Date</label><span class="mandatory_star">*</span>
						<div class="pick_date">
							<form:input type="text" class="datePicker form-control startDate" id="startDate1" path="startDate" readonly="true"/>
							<!--img src="./img/2016-06-23.png" alt="" ></img-->
						</div>
					</div>
					<div class="col-md-4">
						<label>End Date</label>
						<div class="pick_date">
							<form:input type="text" class="datePicker form-control endDate" id="endDate2" path="endDate" readonly="true"/>
						</div>
					</div>
					
					<div class="col-md-4">
		                <div class='text top' >
		                    <label>Status</label><span class="mandatory_star">*</span>
		                    <div class="half-col last">
		                        <ul class="list-inline">
		                            <c:forEach items="${categoryStatus}" var="status">
										<li>
										<label>
											<form:radiobutton path="categoryStatusCode" class="css-checkbox" id ="${status.key}" value="${status.key}" />
											<label for="${status.key}" class="css-label radGroup2">${status.value}</label>
										</label>
										</li>
									</c:forEach>
		                        </ul>
		                    </div>
		                </div>
		            </div>
					<%-- <div class="col-md-4">
                  		<label>Category Image URL</label><span class="mandatory_star">*</span>
						<form:input type="text" class="form-control" path="categoryImageUrl" name="sub_categoryImageUrl" id="sub_categoryImageUrl" />		
                    </div> --%>
				</div>
		        <div class="row pad_top">
					<div class="col-md-4">
						<form:checkbox path="goQuicklyCategory" id="goQuicklySubCategory" value="0" class="css-checkbox checkBoxMargin" />
						<label>Go Quickly Category</label>
					</div>
					<div class="col-md-4">
						<label>Go Quickly Display Order</label><span class="gqSubDisplayOrder mandatory_star">*</span>
						<form:input type="text" maxlength="2" path="gqcSubDisplayOrder" name="gqcSubDisplayOrder" id="gqcSubDisplayOrder" class="form-control sort_order"/>
					</div>
				</div>
				
				<!-- ****************************** -->
				<div class="row pad_top pad_bottom">			
					<div class="col-md-4">
						<form:checkbox path="bestSellingCategory" id="sub_bestSellingCategory" name="sub_bestSellingCategory" value="0" class="css-checkbox checkBoxMargin" />
						<label>Top Selling Category</label>
					</div>
				
					<div class="col-md-4">
						<label>Linked Category</label>
						<input type="text" class="form-control" name="sub_linkedCategoryName" id="sub_linkedCategoryName" readonly />		
						<form:hidden path="linkedCategory" id="sub_linkedCategory" />
						<a href="javascript:void(0);" class="sub_selectLinkedCategory selectCategoryLink">Click here to View Categories</a>
					</div>
				</div>
				
				<div class="row pad_top pad_bottom">
					<div class="col-md-4">
						<label>Category Image URL</label>
						<input id="uploadFile_" class="upload_file" placeholder="Choose File" disabled="disabled" />
						<div class="fileUpload btn padTop_Btm">
							<span class="bg_w_button">Browse</span>
							<!-- TODO: Banner image placement -->
							<input id="sub_categoryImage" name="sub_categoryImage" type="file" class="upload categoryImageUpload bwrsBtnAddPar"  />
						</div>
					</div><!---- browse button --->
                    <div class="col-md-4 image_wrap image_wrap1 noMar-top">
                         <img id="sub_categoryImage_" src="" style="max-height: 100%; max-width: 100%;"/>
                    </div>
					<div class="col-md-2">
						<input class="bg_g_button" type="button" value="Upload" onclick="sub_uploadImage('sub_categoryImage','sub_categoryImageUrl')"/>
						<form:hidden path="categoryImageUrl" id="sub_categoryImageUrl"/>
					</div>		
				</div>
				<!-- ************************************ -->
				
			<div class="row btn_save pull-right" id="categoryButtonsID">
				<input class="bg_g_button" type="button" id="subCatCancelbtnD" value="Cancel"/>
				<input class="bg_g_button" type="button" onClick="subCatValidationRule(); $(this).closest('form').submit();" value="Save" />
			</div><!---- button ------->
		</form:form>
	</div>
	<!-- Display sub category end  -->



<!-- ***********************Popup To Select Linked Categories************************** -->
<div id="viewCategoriesModel" class="modal fade" role="dialog">	
			<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Select Linked Category</h4>
				</div><!--header-->
				<div class="modal-body">
					<div class="row pad_top pad_bottom10" >
						<div class="col-md-3">
	                        <a href="javascript:void(0);" class="expandAll selectCategoryLink">Expand All</a>
						</div>
						<div class="col-md-3">
	                        <a href="javascript:void(0);" class="collapseAll selectCategoryLink">Collapse All</a>
						</div>
					</div>
					<div class="row pad_top linkedCategoryPop">
						<div class="col-md-1">
	                        <div class="select_linked_category">
							</div>
						</div>
					</div><!---body--->
					<div class="modal-footer">
						<button id="addLinkedCategory" class="bg_g_button" data-dismiss="modal">Save</button>
					</div>
				</div><!---modal content--->
			</div>
		</div>
	</div>
	
	<div id="viewSubCategoriesModel" class="modal fade" role="dialog">	
			<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Select Linked Category</h4>
				</div><!--header-->
				<div class="modal-body">
					<div class="row pad_top pad_bottom10" >
						<div class="col-md-3">
	                        <a href="javascript:void(0);" class="sub_expandAll selectCategoryLink">Expand All</a>
						</div>
						<div class="col-md-3">
	                        <a href="javascript:void(0);" class="sub_collapseAll selectCategoryLink">Collapse All</a>
						</div>
					</div>
					<div class="row pad_top linkedCategoryPop">
						<div class="col-md-1">
	                        <div class="sub_select_linked_category">
							</div>
						</div>
					</div><!---body--->
					<div class="modal-footer">
						<button id="sub_addLinkedCategory" class="bg_g_button" data-dismiss="modal">Save</button>
					</div>
				</div><!---modal content--->
			</div>
		</div>	
	</div>
	<!--Edit Amazon  -->
	<div id="editAmazonCategory" class="modal fade" role="dialog">
        <form:form autocomplete="off"  id="editAmazonCategoryBean" 
					>
		<div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content"> 
                    <div class="modal-header">
                        <button type="button" id="cancelbtnDAmzon" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Edit Amazon Category Details</h4>
                    </div><!----modal-header--->
                    <div class="modal-body1">
                        <div class="row pad_top" >
                            <div class="col-md-4 user_details">
                                <label>Partner Name</label>
                                 <input type="text" maxlength="25"
												class="form-control " name="edit_amazonpartnername_" id="edit_amazonpartnername_" readonly>
												<input type="hidden" maxlength="11"
												class="form-control " name="edit_amazonpartnerno_" id="edit_amzonpartnerno_" readonly>
                            </div>
                            <div class="col-md-4 user_details">
                                <label>Amazon Category Name</label>
                                <input type="text" maxlength="150"
												class="form-control " name="edit_amazonpartnercategoryname_" id="edit_amazonpartnercategoryname_" readonly>
                            </div>
                            <input type="hidden" id="id">
                            <input type="hidden" id="amazonPrtNo">
                              <input type="hidden" id="amazonPrtCatNo">
								<div class="col-md-4">
									<div class='text top' >
									<label>Amazon Category Status</label>
									<div class="half-col last">
										<ul class="list-inline">
											<c:forEach items="${prtCatStatus}" var="status">
														<li><label> 
														<input type="radio"	name="edit_amazonpartcategorystatus" class="css-checkbox"	id="edit_amazonpartcategorystatus_${status.key}" value="${status.key}"/>
																<label for="edit_amazonpartcategorystatus_${status.key}" class="css-label radGroup2">
																	${status.value}
																</label>
													
														</label></li>
											</c:forEach>
										</ul>
									</div>
								</div>
								</div>
								
                            </div><!--- row --->
                            <div class="row pad_top">
                            <div class="col-md-6">
                          	  <label>Amazon Order Category Name</label>									
								<input type="text" class="form-control" name="amazonorderCategoryName" id="amazonorderCategoryName" />	
								
						
                            </div>
                            
                            <div class="col-md-6">
                          	  <label>Amazon Category Id</label>									
								<input type="text" class="form-control" name="amazonCategoryId" id="amazonCategoryId" />	
								
						
                            </div>
                            	
								
                       		 </div><!----row--->
                      	 
						
					</div> <!----  row  --->
					<div class="row btn_save align_right" >
						<input class="bg_g_button" type="button"  id="updateamazonCategory" value="Update" />
					</div><!-------- button ------>
                </div>
            </div>
            </form:form>
        </div>
	
	<!--  -->
<!-- ************************************************* -->


<link rel="stylesheet" href="${contextPath}/static/css/jstree-3.0.1/themes/default/style.min.css">
<script>
	var upArrow = '<div class="arrow-up"></div>';
	var subCategory = $('#add_subCategory').html();
	
	function getSelectedTree() {
		var id = $('.category_list').jstree().get_selected(true)[0].id;
		//var id = $('.category_list').jstree("get_selected")[0].id;
		if((id.split("-").length) > 1) {
			$("#categoryTypeID_").html("Child");
		} else {
			$("#categoryTypeID_").html("Parent");
		}
// 		$("#categoriesID_").html($('.category_list .jstree-clicked').attr("categoryid"));
		$("#categorynameID_").html($('.category_list').jstree().get_selected(true)[0].text);
		//$("#categorynameID_").html($('.category_list').jstree("get_selected")[0].text);
		$("#startDateID_").html($('.category_list .jstree-clicked').attr("startdate"));
		
		var startValuesID = $('.category_list .jstree-clicked').attr("startvalue");
		if((startValuesID != null) && !(startValuesID  === undefined) && (startValuesID != "0")) {
			$("#startValueID_").html(startValuesID);
		} else {
			$("#startValueID_").html("");
		}
		var endValuesID = $('.category_list .jstree-clicked').attr("endvalue");
		if((endValuesID != null) && !(endValuesID  === undefined) && (endValuesID != "0")) {
			$("#endValueID_").html(endValuesID);
		} else {
			$("#endValueID_").html("");
		}
		var stepValuesID = $('.category_list .jstree-clicked').attr("stepvalue");
		if((stepValuesID != null) && !(stepValuesID  === undefined) && (stepValuesID != "0") ) {
			$("#stepValueID_").html(stepValuesID);
		} else {
			$("#stepValueID_").html("");
		}
		var endDates = $('.category_list .jstree-clicked').attr("enddate");
		if((endDates != null) && !(endDates  === undefined)) {
			$("#endDateID_").html(endDates);
		} else {
			$("#endDateID_").html("");
		}
		if($('.category_list .jstree-clicked').attr("status") == 1) {
			$("#activeStatusID_").html("Active");
			$("#inactiveStatusID_").html("");
		} else {
			$("#inactiveStatusID_").html("Inactive");
			$("#activeStatusID_").html("");
		}
	}
	
	$('.export-excel-categroy').click(function() {
		var data = JSON.parse($("#getCategoryList").val());
		if (data == '')
			return;
		sortCategoryJSON(data, 'text', true);
		CategoryJSONToExcelExporter(data, "Category List Report_" + datetext, true);
// 		CategoryJSONToExcelConvertor(data, "Category List Report_" + datetext, true);
	});
	
	function createCategoryList(content){
// 		console.log(content);
		var con = JSON.parse(content);
		
		$('.category_list').jstree({
			'core' : {
				'data' : con
			},
			"plugins" : [ "sort" ]
		});
		
		$('.category_list_details').jstree({
			'core' : {
				'data' : con
			},
			"plugins" : [ "sort" ]
		});
		
		$('.select_linked_category').jstree({
			'core' : {
				'data' : con
			},
			"plugins" : ["checkbox", "sort"],
			"checkbox": {"three_state" : false}
		});
		
		$('.sub_select_linked_category').jstree({
			'core' : {
				'data' : con
			},
			"plugins" : ["checkbox", "sort"],
			"checkbox": {"three_state" : false}
		});
		
		stateChange();
	}
	
	/* ********************************************************** */
	var linked_catNos= new Array();
	
	$(".selectLinkedCategory").on('click',function() {
		$('.select_linked_category').jstree(true).deselect_all();
		$('#viewCategoriesModel').modal('show');
		
		if($("#linkedCategory").val() == ""){
			linked_catNos=[];
		}
		
		$('.select_linked_category').jstree().open_all();
		var selectedCatNo = $("#linkedCategory").val().split(',');
		$('.select_linked_category .jstree-container-ul li a').each( function() {
			if(selectedCatNo != "" && $.inArray($(this).attr("catno"), selectedCatNo) >= 0){
				$(".select_linked_category").jstree("check_node", "#"+$(this).attr("id"));
			}
		});
		$('.select_linked_category').jstree().close_all();
	});
	
	$(".expandAll").on('click',function() {
		$('.select_linked_category').jstree(true).open_all();
	});
	
	$(".collapseAll").on('click',function() {
		$('.select_linked_category').jstree(true).close_all();
	});

	$(document).on('click','.select_linked_category .jstree-container-ul li a',function(){

		var selectedCat = $(".select_linked_category").jstree('get_selected');
		
		if(selectedCat.length <= 3){
			
			if($.inArray($(this).attr("catno"), linked_catNos) < 0){
				linked_catNos.push($(this).attr("catno"));
			}
			else{
				var index = linked_catNos.indexOf($(this).attr("catno"));
				if(index >= 0){
					linked_catNos.splice(index, 1);
				}
			}
		}
		else{
			$(".select_linked_category").jstree("uncheck_node", "#"+$(this).attr("id"));
			alert("You can select max 3 categories");
		}		
	});

	$("#addLinkedCategory").on("click", function(){
		var selected_categoryValue="";

		for(var i = 0; i < $('.select_linked_category').jstree().get_selected(true).length; i++){
			selected_categoryValue += $('.select_linked_category').jstree().get_selected(true)[i].text+",";
		}
		
		$("#linkedCategoryName").val(selected_categoryValue);
		$("#linkedCategory").val(linked_catNos.toString());
	});
	
	$("#bestSellingCategory").on('change', function() {
		if($("#bestSellingCategoryHidden").val() == "1"){
			if($(this).is(":checked")) {
		    	$('#bestSellingCategory').val(1);
		    }
			else{
				$('#bestSellingCategory').val(0);
			}
		}
		else{
			var bsCatNames = $("#bsCatNames").val().split(',');
			if(bsCatNames.length < 3){
				if($(this).is(":checked")) {
			    	$('#bestSellingCategory').val(1);
			    }
				else{
					$('#bestSellingCategory').val(0);
				}
			}
			else{
				$("#bestSellingCategory").prop("checked",false);
				alert("You can set max 3 best selling categories");
			}
		}
		
	});
	
	/* $('#categoryImage').on('change', function() {
		$('#uploadFile').val($(this).val());
		$("#categoryBean").validate().element("#categoryImage");
	}); */
	
    $("#categoryImage").bind("change", function () {    	
    	$('#uploadFile').val($(this).val());
        //var fileUpload = $("#categoryImage")[0];
        //checkFileUpload(fileUpload);
    });
	
	function uploadImage(id,url){
		if($("#uploadFile").val() != "" && $("#categoryBean").validate().element("#categoryImage")){
	       /*  if(!uploadFlag){
	        	return;
	        } */
			
			var formData = new FormData();
			formData.append('file',$("#"+id)[0].files[0]);
			
			if($("#categoryBean").validate().element("#"+id)){		
				$.ajax({
					url: "${contextPath}/admin/saveImages?type=category",
					data: formData,
					dataType: 'text',
					processData: false,
					contentType: false,
					type: 'POST',
					
					success: function (response) {
						$('#uploadFile').val(response);			 
						var newImageUrl='${contextPath}'+'/admin/images/category/'+response;
						$('#categoryImage_').attr('src',newImageUrl);
						$('#categoryImageUrl').val(response);	
						$("#categoryBean").validate().element("#categoryImageUrl");
					}
				});		
			}
		}
	}
		
	/* ********************************************************** */
	
 	$(document).on('click','#subCatCancelbtnD',function() {
		$.get("${contextPath}/admin/addCategoryCancel", function(data, status) {
			if (status == "success") {
				window.location.replace("${contextPath}/" + data);
			}
		});
	});
 	$(document).on('click','#categoryDeleteID',function() {
		var id = $('.category_list').jstree().get_selected(true)[0].id;
		var catNo = $('.category_list .jstree-clicked').attr("catno");
		
		$.redirect('${contextPath}/admin/deleteCategory', {'catNo': catNo});
	});
 	/*setTimeout(function(){
 		 
 	}, 800);*/
 	
 	function stateChange() {
 	    setTimeout(function () {
 	    	getSelectedTree();
 	    }, 800);
 	}
 	
	$(function() {
			/* $(document).on('click','.jstree-icon',function(){
				var JSTreeClass = ($(".jstree-open").attr("class"));
				if( (JSTreeClass != null) && !(JSTreeClass  === undefined) &&
						(JSTreeClass == "jstree-node jstree-open") ) {
				}
			}); */
			$('.category_list_divDetails').css('display','none');
			$(document).on('click','.selectCategory',function(){
				$('.category_list_divDetails').css('display','block');
			});
			$('#categoriesDivID').hide();
			$('#add_category').hide();
			$(".category_list").bind("select_node.jstree", function (e, data) {
		    	return data.instance.toggle_node(data.node);
			});
			
			var json = $("#getCategoryList").val();
			createCategoryList(json);
			$('.category_details .nav-tabs li.active a').after(upArrow);
			
			$(document).on('click','.category_list .jstree-container-ul li a',function(){
				
				/* var center = $(".category_list").height()/2;
				alert(center);
			    var top = $(this).offset().top ;
			    if (top > center){
			        $(".category_list").scrollTop(top-center);
			    } */
				$('.icons').remove();
				var selected_categoryValue = $('.category_list').jstree().get_selected(true)[0].text;
				
				// view category start
// 				$("#categoriesID_").html($(this).attr("categoryid"));
				$("#categorynameID_").html(selected_categoryValue);
				$("#startDateID_").html($(this).attr("startdate"));
				var endDates = $(this).attr("enddate");
				if((endDates != null) && !(endDates  === undefined)) {
					$("#endDateID_").html(endDates);
				} else {
					$("#endDateID_").html("");
				}
				var startValuesID = $(this).attr("startvalue");
				if((startValuesID != null) && !(startValuesID  === undefined) && (startValuesID != "0")) {
					$("#startValueID_").html(startValuesID);
				} else {
					$("#startValueID_").html("");
				}
				var endValuesID = $(this).attr("endvalue");
				if((endValuesID != null) && !(endValuesID  === undefined) && (endValuesID != "0")) {
					$("#endValueID_").html(endValuesID);
				} else {
					$("#endValueID_").html("");
				}
				var stepValuesID = $(this).attr("stepvalue");
				if((stepValuesID != null) && !(stepValuesID  === undefined) && (stepValuesID != "0")) {
					$("#stepValueID_").html(stepValuesID);
				} else {
					$("#stepValueID_").html("");
				}
				if($(this).attr("status") == 1) {
					$("#activeStatusID_").html("Active");
					$("#inactiveStatusID_").html("");
				} else {
					$("#inactiveStatusID_").html("Inactive");
					$("#activeStatusID_").html("");
				}
				var id = $('.category_list').jstree().get_selected(true)[0].id;
				if((id.split("-").length) > 1) {
					$("#categoryTypeID_").html("Child");
				} else {
					$("#categoryTypeID_").html("Parent");
				}
				// view category end
				
				$('#viewCategory a').attr( "data-toggle", "tab");
				$('#addCategory a').attr( "data-toggle", "tab");
				$('#addSubCategory a').attr( "data-toggle", "tab");
				
				$('#viewCategory').removeClass( "disabled");
				$('#addSubCategory').removeClass( "disabled");
				$('.addCategory a').text("Add Category");
				
				$('.btn_save input:nth-child(2)').val("Save");
				$(".categoryId_edit").prop('disabled', false);
				
				$(this).after('<i class="fa fa-pencil icons editCategory"></i><i class="fa fa-close icons deleteContent" data-toggle="modal" data-target="#myModalCategory"></i>');
				$('#actionID').val("");
				$('#addCategoryId').val("");
				$('#addcategoryNoId').val("");
				$('.categoryName').val(selected_categoryValue);
				$('.categoryId_edit').val("");
				$('.categoryName_edit').val("");
				$('#datepicker1').val("");
				$('#datepicker2').val("");
				$('#addSubCategory a').attr( "onclick", "addSubCategory(this)");
				$('#viewCategory a').attr( "onclick", "viewCategory(this)");
				$('#addCategory a').attr( "onclick", "addCategory(this)");
				$("#datepicker2").attr("readonly", true);
				
				$("#goQuicklyCategory").prop('checked', false);
				$('#goQuicklyCategory').val(0);
				$("#gqcDisplayOrder").prop('readonly',true);
				$("#gqcDisplayOrder").val('');
				$("#gqcDisplayOrderHidden").val('');
				$(".gqDisplayOrder").hide();
				$("#categoryNameHidden").val('');
				
				$("#startValueID").val('');
				$("#endValueID").val('');
				$("#stepValueID").val('');
				$("#bestSellingCategory").val('0');
				$("#bestSellingCategoryHidden").val('0');
				$("#bestSellingCategory").attr("checked", false);
				$("#linkedCategoryName").val('');
				$("#categoryImage_").attr("src","");
				$("#uploadFile").val('');
				$("#categoryImageUrl").val('');
				$("#linkedCategory").val('');
				
				$("#categoryBean").validate().resetForm();
				
				setValidationRuleForCategoryDisplayOrder(false);

				if ($('.addSubCategory').hasClass('active') ) {
					$("#categoryId_").val(id);
				}
			});
	});
	
	$(document).on('click','.category_list_details .jstree-container-ul li a',function(){
		var selected_categoryValue = $('.category_list_details').jstree().get_selected(true)[0].text;
		$('#categoryName_details').val(selected_categoryValue);
		$('#catNoID').val($(".jstree-clicked").attr("catno"));
		$('#catgry_no').val($(".jstree-clicked").attr("catno"));
		
		checkCategory();
	});
	
	$(document).on('click','.select_category .jstree-container-ul li a',function(){
		var selected_categoryValue = $('.select_category').jstree().get_selected(true)[0].text;
		$('#categoryNo_').val(selected_categoryValue);
		$('#catNoID').val($(".jstree-clicked").attr("catno"));
		checkCategory();
		$('.close').click();
	});
	function checkCategory(){
		var category = $("#categoryNo_").val();
		if(category != ""){
			$('#categoryNo_-error').hide();
		} else {
			$('#categoryNo_-error').show();
		}
	}
	function viewCategory(param){
		$('.category_details #subCategory').remove();
		$('#add_category').hide();
		$('#view_category').show();
		$('.viewCategory').addClass("active");
		$('.addCategory').removeClass("active");
		$('.addSubCategory').removeClass("active");
		
		$(".categoryId_edit").prop('disabled', false);
		appendArrow(param);
	}
	function addCategory(param){
		$('.category_details #subCategory').remove();
		$('#view_category').hide();
		$('#add_category').show();
		$('.addSubCategory').removeClass("active");
		$(".categoryId_edit").prop('disabled', false);
		appendArrow(param);
	}
	function appendArrow(param){
		$('#addCategoryId').val("");
		$('.arrow-up').remove();
		$(param).after(upArrow);
	}
	function sub_uploadImage(id,url){
		if($("#uploadFile_").val() != "" && $("#categoryBeanId").validate().element("#sub_categoryImage")){
			/* if(!uploadFlag){
	        	return;
	        } */
	        
			var formData = new FormData();
			formData.append('file',$("#"+id)[0].files[0]);
			
			if($("#categoryBeanId").validate().element("#"+id)){		
				$.ajax({
					url: "${contextPath}/admin/saveImages?type=category",
					data: formData,
					dataType: 'text',
					processData: false,
					contentType: false,
					type: 'POST',
					
					success: function (response) {
						$('#uploadFile_').val(response);			 
						var newImageUrl='${contextPath}'+'/admin/images/category/'+response;
						$('#sub_categoryImage_').attr('src',newImageUrl);
						$('#sub_categoryImageUrl').val(response);	
						$("#categoryBeanId").validate().element("#sub_categoryImageUrl");
					}
				});		
			}
		}
	}
	function subCatValidationRule(){
		var flag = false;
		
		var startValue=$("#subStartValueID").val();
		var endValue=$("#subEndValueID").val();
		var stepValue=$("#subStepValueID").val();
		
		if(endValue != "" || startValue != "" || stepValue != ""){
			flag = true;
		}
		$('#subStartValueID').rules('add', {
			required: flag,
			number: true,
			subStartValueNotZero: true
		});
		$('#subEndValueID').rules('add', {
			required: flag,
			number: true,
			greaterThanSubFilter:true,
			subEndValueNotZero: true
		});
		$('#subStepValueID').rules('add', {
			required: flag,
			number: true,
			greaterThanSubEndValue : true,
			subStepValueNotZero: true
		});
		$("#categoryBeanId").validate().element('#subStartValueID');
		$("#categoryBeanId").validate().element('#subEndValueID');
		$("#categoryBeanId").validate().element('#subStepValueID');
	}
	function addSubCategory(param){
		if (! $('.addSubCategory').hasClass('active') ) {
			$('#add_category').hide();
			$('#view_category').hide();
			$('#subCategory').remove();
			$(param).removeClass();
			$(param).addClass('addSubCategory active');
			$('.category_details ul').first().after('<div id="subCategory">'+subCategory+'</div>');
			
			appendArrow(param);
			
			var id = $('.category_list').jstree().get_selected(true)[0].id;
			$("#categoryId_").val(id);
			$( "#startDate1" ).datepicker({
			    dateFormat: 'dd/mm/yy',
			    minDate: '0',
				showOn: "button",
				buttonImage: "./static/img/2016-06-23.png",
				buttonImageOnly: true,
				buttonText: "Select date"
			});
			$( "#endDate2" ).datepicker({
			    dateFormat: 'dd/mm/yy',
			    minDate: '0',
				showOn: "button",
				buttonImage: "./static/img/2016-06-23.png",
				buttonImageOnly: true,
				buttonText: "Select date"
			});
			$.validator.addMethod("subStartValueNotZero", function(value, element) {
				var startValue = $("#subStartValueID").val();
				if(startValue == '0'){
					return false;
				} else {
					return true;
				}
			});
			$.validator.addMethod("subEndValueNotZero", function(value, element) {
				var endValue = $("#subEndValueID").val();
				if(endValue == '0'){
					return false;
				} else {
					return true;
				}
			});
			$.validator.addMethod("subStepValueNotZero", function(value, element) {
				var stepValue=$("#subStepValueID").val();
				if(stepValue == '0'){
					return false;
				} else {
					return true;
				}
				return isValueZero;
			});
			
			$.validator.addMethod('gqcSubDisplayOrderNotZero', function(value, element) {
				var subDisplayOrder = parseInt($('#gqcSubDisplayOrder').val());
				
				if(subDisplayOrder == 0){
					return false;
				}
				else{
					return true;
				}
			});
			
			$("#categoryBeanId").validate({
				onkeyup : true,
				ignore : [],
				rules : {
					categoryName : {
						required : true,
						AlphaNumericOnly : true,
						dupliCatecategoryName : true,
						rangelength : [1, 50]
					},
					startValue : {
						required : false,
						number: true,
						subStartValueNotZero : false
					},
					endValue : {
						required : false,
						number: true,
						greaterThanSubFilter : false,
						subEndValueNotZero: false
					},
					stepValue : {
						required : false,
						number: true,
						greaterThanSubEndValue : false,
						subStepValueNotZero : false
					},
					startDate : {
						required : true,
						subdateHigherThanToday : true
					},
					endDate : {
						required : false,
						greaterThanSubEndDate : true,
						subdateHigherThanToday : true
					},
					gqcSubDisplayOrder : {
						required: true,
						digits: true,
						gqcSubDisplayOrderNotZero: true,
						dupCategorySortOrder: true
					},
					sub_categoryImage : {
						extension : "png|jpg|jpeg|gif",
						//filesize : 1000000,
					},
					sub_categoryImageUrl : {
						rangelength : [1, 250]
					}
				},
				messages : {
					categoryName : {
						required : "Please enter category Name",
						minlength : "Please enter atleast 3 characters",
						AlphaNumericOnly : "AlphaNumeric only(No special characters allowed)",
						dupliCatecategoryName : "Sub Category Name already exists",
						rangelength : "Maximum 50 characters permitted"
					},
					startValue: {
						required : "Please enter start value",
						number: "Please enter number only",
						subStartValueNotZero: "Value should not be 0"
					},
					endValue : {
						required : "Please enter end value",
						number: "Please enter number only",
						greaterThanSubFilter:"Filter end Value should be greater than Start value",
						subEndValueNotZero: "Value should not be 0"
					},
					stepValue : {
						required : "Please enter step value",
						number: "Please enter number only",
						greaterThanSubEndValue : "Filter Step Value should be lesser than end value",
						subStepValueNotZero: "Value should not be 0"
					},
					startDate : {
						required : "Start date cannot be empty",
						subdateHigherThanToday : "Start date must be less than or equal End date "
					},
					endDate : {
						required : "End date cannot be empty",
						greaterThanSubEndDate : "End date must be greater than or equal to Start date ",
						subdateHigherThanToday : "Start date must be greater than or equal End date "
					},
					gqcSubDisplayOrder : {
						required: "Please enter category sort order",
						digits: "Please enter numeric data",
						gqcSubDisplayOrderNotZero: "Display order should not be 0",
						dupCategorySortOrder: "Category sort order already exists"
					},
					sub_categoryImage : {
						extension:"Invalid file format. Only jpg|jpeg|png|gif files are allowed",
						//filesize : "File size must be less than 1MB",
					},
					sub_categoryImageUrl : {
						rangelength : "Maximum 250 characters permitted"
					},
				},
				errorElement : "div",
				errorPlacement : function(error, element) {
					if ($(element).hasClass("form-control startDate")) {
						error.insertAfter($(element).closest("div"));
					} else if ($(element).hasClass("form-control endDate")) {
						error.insertAfter($(element).closest("div"));
					} else if ($(element).hasClass("categoryImageUpload")) {
						error.insertAfter($(element).closest("div"));
					}
					else {
						error.insertAfter(element);
					}
				},
				submitHandler : function(form) {
					if($("#gqcSubDisplayOrder").val() == ""){
						$("#gqcSubDisplayOrder").val('0');
					}
					form.submit();
				},
			});
			
			$(document).ready(function () {
				 //For Sub Category
				 if(!$("#goQuicklySubCategory").is(":checked")) {
					 $("#gqcSubDisplayOrder").prop('readonly',true);
					 $(".gqSubDisplayOrder").hide();
					 setValidationRuleForSubCategoryDisplayOrder(false);
				 }
			});
			
//*************************************sub cat
	var sub_linked_catNos = [];
	
	$(".sub_selectLinkedCategory").on('click',function() {
		$('.sub_select_linked_category').jstree(true).deselect_all();
		$('#viewSubCategoriesModel').modal('show');
		
		if($("#sub_linkedCategory").val() == ""){
			sub_linked_catNos=[];
		}
		
		$('.sub_select_linked_category').jstree().open_all();
		var selectedCatNo = $("#sub_linkedCategory").val().split(',');
		$('.sub_select_linked_category .jstree-container-ul li a').each( function() {
			if(selectedCatNo != "" && $.inArray($(this).attr("catno"), selectedCatNo) >= 0){
				$(".sub_select_linked_category").jstree("check_node", "#"+$(this).attr("id"));
			}
		});
		$('.sub_select_linked_category').jstree().close_all();
	});

	$(".sub_expandAll").on('click',function() {
		$('.sub_select_linked_category').jstree(true).open_all();
	});
	
	$(".sub_collapseAll").on('click',function() {
		$('.sub_select_linked_category').jstree(true).close_all();
	});
	
	$(document).on('click','.sub_select_linked_category .jstree-container-ul li a',function(){

		var selectedCat = $(".sub_select_linked_category").jstree('get_selected');
		
		if(selectedCat.length <= 3){
			
			if($.inArray($(this).attr("catno"), sub_linked_catNos) < 0){
				sub_linked_catNos.push($(this).attr("catno"));
			}
			else{
				var index = sub_linked_catNos.indexOf($(this).attr("catno"));
				if(index >= 0){
					sub_linked_catNos.splice(index, 1);
				}
			}
		}
		else{
			$(".sub_select_linked_category").jstree("uncheck_node", "#"+$(this).attr("id"));
			alert("You can select max 3 categories");
		}
	});

	$("#sub_addLinkedCategory").on("click", function(){
		var selected_categoryValue="";

		for(var i = 0; i < $('.sub_select_linked_category').jstree().get_selected(true).length; i++){
			selected_categoryValue += $('.sub_select_linked_category').jstree().get_selected(true)[i].text+",";
		}
		
		$("#sub_linkedCategoryName").val(selected_categoryValue);
		$("#sub_linkedCategory").val(sub_linked_catNos.toString());
	});
	
	$("#sub_bestSellingCategory").on('change', function() {
		if($(this).is(":checked")) {
	    	$('#sub_bestSellingCategory').val(1);
	    }
		else{
			$('#sub_bestSellingCategory').val(0);
		}
	});

	$('#sub_categoryImage').on('change', function() {
		$('#uploadFile_').val($(this).val());
		$("#categoryBeanId").validate().element("#sub_categoryImage");
	});
	
	$("#sub_categoryImage").bind("change", function () {    	
    	$('#uploadFile_').val($(this).val());
       /*  var fileUpload = $("#sub_categoryImage")[0];
        checkFileUpload(fileUpload); */
    });
//*************************************
			
			$("#goQuicklySubCategory").on('change', function() {
				$('#gqcSubDisplayOrder').val('');
				
			    if($(this).is(":checked")) {
			    	$('#goQuicklySubCategory').val(1);
			    	$("#gqcSubDisplayOrder").prop('readonly',false);
			    	$(".gqSubDisplayOrder").show();
			    	
			    	setValidationRuleForSubCategoryDisplayOrder(true);
			    }
			    else{
			    	$('#goQuicklySubCategory').val(0);
			    	$("#gqcSubDisplayOrder").prop('readonly',true);
			    	$(".gqSubDisplayOrder").hide();
			    	
			    	setValidationRuleForSubCategoryDisplayOrder(false);
			    }
			});
			
			$("#gqcSubDisplayOrder").on('blur', function() {
				var gqcSubDisplayOrder = parseInt($("#gqcSubDisplayOrder").val());
				dupCategorySortOrder=false;

				if(gqcSubDisplayOrder == 0){
					$("#categoryBeanId").validate().element("#gqcSubDisplayOrder");
				}
				else{
					$.get("${contextPath}/admin/checkCategoryDispOrd?displayOrder="+gqcSubDisplayOrder, function(data,status) {
						if(status=="success" && data!=''){
							dupCategorySortOrder=true;	
						}
						else{
							dupCategorySortOrder=false;
						}
						$("#categoryBeanId").validate().element('#gqcSubDisplayOrder');				
					});
				}
			});
			
			$("#categoryName").change(function() {
				var categoryName = $('#categoryName').val();
				$.get("${contextPath}/admin/checkCategoryName?categoryName=" + categoryName, function(data, status) {
					if (status == "success" && data != '') {
						categoryNameExist = true;
					} else {
						categoryNameExist = false;
					}
					$("#categoryBeanId").validate().element('#categoryName');
				});
			});
			
			
			$.validator.addMethod('dupliCatecategoryName', function(value, element) {
				if (categoryNameExist) {
					return false;
				} else {
					return true;
				}
			});
			
			$.validator.addMethod('duplicatecategoryNamecreate', function(value, element) {
				if (partnerCategoryNameExist) {
					return false;
				} else {
					return true;
				}
			});
			
			
			
			
			$.validator.addMethod("subdateHigherThanToday", function(value, element) {
				var startDate = getFormattedDate($('#startDate1').val());
				var endDate   = getFormattedDate($('#endDate2').val());
				if (endDate == 'Invalid Date')
					return true;
				if (endDate !== 'Invalid Date') {
					if (startDate <= endDate)
						return true;
					if (startDate >= endDate)
						return false;
				}
			});
			$.validator.addMethod("greaterThanSubEndDate", function(value, element) {
				var startDate = getFormattedDate($('#startDate1').val());
				var endDate   = getFormattedDate($('#endDate2').val());
				if (endDate == 'Invalid Date')
					return true;
				if (endDate >= startDate)
					return true;
				if (endDate <= startDate)
					return false;
			});
			$("#startDate1").on('change', function() {
				$("#categoryBeanId").validate().element("#startDate1");
				var endDate = getFormattedDate($('#endDate2').val());
				if (endDate != 'Invalid Date') {
					$("#categoryBeanId").validate().element("#endDate2");
				}
			});
			$("#endDate2").on('change', function() {
				$("#categoryBeanId").validate().element("#endDate2");
				$("#categoryBeanId").validate().element("#startDate1");
			});
			var selected_categoryValue = $('.category_list').jstree().get_selected(true)[0].text;
			$('.categoryName').val(selected_categoryValue);
		}
	}
	$(document).on('click', '.editCategory', function(){
		var selected_categoryId    = $('.category_list').jstree().get_selected(true)[0].id;
		var selected_categoryValue = $('.category_list').jstree().get_selected(true)[0].text;
		
		$('#viewCategory a').removeAttr( "data-toggle");
		$('#viewCategory a').removeAttr( "onclick");
		$('#viewCategory').addClass( "disabled");
		$('#viewCategory').removeClass( "active");
		
		$('#addCategory a').removeAttr( "data-toggle");
		$('#addCategory a').removeAttr( "onclick");
		$('#addCategory').addClass( "disabled");
		
		$('#addSubCategory a').removeAttr( "data-toggle");
		$('#addSubCategory a').removeAttr( "onclick");
		$('#addSubCategory').addClass( "disabled");
		$('#addSubCategory').removeClass( "active");
		
		$('.addCategory a').attr("data-toggle");
		$('.addCategory').removeClass("disabled");
		$('.addCategory').addClass("active");
		
		$('#view_category').hide();
		$('#add_category').show();
		$('#subCategory, .arrow-up').remove();
		$('.category_details .nav-tabs li.active a').after(upArrow);
		
		$('.addCategory a').text("Edit Category");
		$('.btn_save input:nth-child(2)').val("Update");
		
		//setting values to particular fields
		$('#actionID').val("UpdateCategory");
// 		$('#categoriesDivID').show();
		
		$('#categoriesID').val($(this).prev().attr("categoryid"));
		$('#addCategoryId').val($(this).prev().attr("categoryid"));
		$('#addcategoryNoId').val($(this).prev().attr("catno"));
// 		$('.categoryId_edit').val($(".jstree-clicked").attr("categoryid"));
		$(".categoryId_edit").prop('disabled',true);
		$('.categoryName_edit').val(selected_categoryValue);
		$("#categoryNameHidden").val(selected_categoryValue);
		$('#datepicker1').val($(this).prev().attr("startdate"));
		$("#datepicker2").attr("readonly", false);
		$('#datepicker2').val($(this).prev().attr("enddate"));
// 		$('#categoryNoID').val($(".jstree-clicked").attr("catno"));
		var status = $(this).prev().attr("status");
		if(status == 0) {
			$('#0').trigger('click');
			$('#1').removeAttr('checked');
		} else {
			$('#1').trigger('click');
			$('#0').removeAttr('checked');
		}
		
		var startValuesID = $(this).prev().attr("startvalue");
		if((startValuesID != null) && !(startValuesID  === undefined) && (startValuesID != "0") ) {
			$("#startValueID").val(startValuesID);
		} else {
			$("#startValueID").val("");
		}
		var endValuesID = $(this).prev().attr("endvalue");
		if((endValuesID != null) && !(endValuesID  === undefined) && (endValuesID != "0") ) {
			$("#endValueID").val(endValuesID);
		} else {
			$("#endValueID").val("");
		}
		var stepValuesID = $(this).prev().attr("stepvalue");
		if((stepValuesID != null) && !(stepValuesID  === undefined) && (stepValuesID != "0") ) {
			$("#stepValueID").val(stepValuesID);
		} else {
			$("#stepValueID").val("");
		}
		
		var goQuicklyCategory = $(this).prev().attr("goquicklycategory");
		$('#goQuicklyCategory').val(goQuicklyCategory);
		
		var gqcDisplayorder = $(this).prev().attr("gqcdisplayorder");
		
		if(gqcDisplayorder == '0'){
			gqcDisplayorder = '';
		}
		
		$("#gqcDisplayOrder").val(gqcDisplayorder);
		$("#gqcDisplayOrderHidden").val(gqcDisplayorder);
		
		if(goQuicklyCategory == 1) {
			$("#goQuicklyCategory").prop('checked', true);
			$("#gqcDisplayOrder").prop('readonly',false);
			setValidationRuleForCategoryDisplayOrder(true);
			$(".gqDisplayOrder").show();
		} else {
			$( "#goQuicklyCategory").prop('checked', false);
			$("#gqcDisplayOrder").prop('readonly',true);
			$(".gqDisplayOrder").hide();
			setValidationRuleForCategoryDisplayOrder(false);
		}
		
		
		//**************************
		var categoryImageUrl = $(this).prev().attr("categoryimageurl");
		var linkedCategory = $(this).prev().attr("linkedcategory");
		var bestSellingCategory = $(this).prev().attr("bestsellingcategory");
		$("#bestSellingCategoryHidden").val(bestSellingCategory);
		var linkedCategoryName = $(this).prev().attr("linkedCategoryName");
		linked_catNos = linkedCategory.split(",");
		
		$('#categoryImage_').attr('src','${contextPath}'+'/admin/images/category/'+categoryImageUrl);
		$('#categoryImageUrl').val(categoryImageUrl);
		$('#uploadFile').val(categoryImageUrl);	
		$("#linkedCategory").val(linkedCategory);
		
		$("#bestSellingCategory").val(bestSellingCategory);
		$("#linkedCategoryName").val(linkedCategoryName);
		
		if(bestSellingCategory == "1"){
			$("#bestSellingCategory").prop('checked', true);
		}
		else{
			$("#bestSellingCategory").prop('checked', false);
		}
		//**************************
		$("#categoryBean").data('validator').resetForm();
	});
</script>
<script type="text/javascript">
	var categoryIDExist = false;
	var categoryNameExist = false;
	var partnerCategoryNameExist = false;
	var partnerCategoryEditUpNameExist = false;
	var partnerCategoryEditUpIdExist = false;
	var partnerCategoryIdExist = false;
	var categoryDisplayOrderExist = false;
	var dupCategorySortOrder = false;

	var uploadFlag = false;
	var mapcategorytab = false;
	var updateamazonCategory=false;
	$('a.biurl').on('click', function() {
		var id = $(this).closest('tr').data('id');
		var categoryimageurl = $(this).closest('tr').data('categoryimageurl_');
		var imageUrl = '${contextPath}' + '/admin/images/category/' + categoryimageurl;
		$('#categoryimage_').prop('src', imageUrl);
		$('#categoryimageurl').text(categoryimageurl);
		$('#myModal1_').data('id', id).modal('show');
	});
	
	$(document).ready(function() {
	
		<c:if test="${mapCategoryUpdateTab==true}">
		$('#existingCategories').removeClass('active');
		$('#createCategory').removeClass('active');
		$('#sectionA').removeClass('in active');
		$('#sectionB').removeClass('in active');
		$('#createCategory').removeClass('active');
		$('#mapCategory').removeClass('active');
		$('#sectionC').removeClass('in active');
		$('#sectionE').addClass('active');
		//$('#sectionF').addClass('in active');
		//$('#sectionG').removeClass('in active');
		//alert("insideghh");
		$('#UpdatePartnerCategory').addClass('active');
		
	    $('#amazonPartnerCategory1').removeClass('active');
		
		var partNameUp=$('#hiddenPrtNo').val();
		var partnerTypeUp = $('#hiddenPrtType').val();
		//alert("partNameUp "+partNameUp);
		$('#partnerTypeUp').val(partnerTypeUp);
		$("#partnerTypeUp").trigger("selectmenuchange", ["refresh", partNameUp]);
		
		
		history.pushState({}, '',"${contextPath}/admin/category");
	</c:if>
	
	<c:if test="${updateamazonCategory==true}">
	$('#existingCategories').removeClass('active');
	$('#createCategory').removeClass('active');
	$('#sectionA').removeClass('in active');
	$('#sectionB').removeClass('in active');
	$('#mapCategory').removeClass('active');
	$('#sectionC').removeClass('in active');
	$('#sectionF').addClass('in active');
	$('#sectionE').removeClass('active');
	$('#UpdatePartnerCategory').removeClass('in active');
	$('#amazonPartnerCategory1').addClass('active');
	
	
	history.pushState({}, '',"${contextPath}/admin/category");
</c:if>
	
	
		<c:if test="${mapCategoryTab==true}">
		    $('#existingCategories').removeClass('active');
			$('#createCategory').removeClass('active');
			$('#sectionA').removeClass('in active');
			$('#sectionB').removeClass('in active');
			$('#mapCategory').addClass('active');
			$('#sectionC').addClass('in active');
			$('#sectionE').removeClass('in active');
			$('#sectionF').removeClass('active');
			$('#UpdatePartnerCategory').removeClass('active');
			$('#amazonPartnerCategory1').removeClass('active');
			$('#sectionG').removeClass('in active');
			history.pushState({}, '',"${contextPath}/admin/category");
		</c:if>
			
		$.validator.addMethod("dropdowncheck", function(value, element) {
			if (value == "")
				return false;
			else
				return true;
		});
		$("#categoryBean").validate({
			onkeyup : true
		});
		$("#categoryBeanId").validate({
			onkeyup : true
		});
		$("#mapcategoryBean").validate({
			onkeyup : true
		});
		$("#editCategoryBean").validate({
			onkeyup : true
		});
		
		
		$('input:radio[name=partcategorystatusupdate][value=1]').attr('checked', true);
		$('input:radio[name=partcategorystatusupdate1][value=1]').attr('checked', true);
		
		
	});
	var currentDate = $.datepicker.formatDate('dd/mm/yy', new Date());
	if ("${categoryBean.categoryId}" != '') {
		var startDate = $('#datepicker1').val();
		if (getFormattedDate(startDate) < getFormattedDate(currentDate)) {
			$('#datepicker1').attr("disabled", "true");
		}
	}
	var fullDate = new Date();
	var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '0' + (fullDate.getMonth() + 1);
	var today = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();

	$.validator.addMethod("dateHigherThanToday1", function(value, element) {
		var startDate = getFormattedDate($('#datepicker1').val());
		var endDate = getFormattedDate($('#datepicker2').val());
		if (endDate == 'Invalid Date')
			return true;
		if (endDate !== 'Invalid Date') {
			if (startDate <= endDate)
				return true;
			if (startDate >= endDate)
				return false;
		}
	});
	$.validator.addMethod("greaterThanEndDate1", function(value, element) {
		var startDate = getFormattedDate($('#datepicker1').val());
		var endDate = getFormattedDate($('#datepicker2').val());
		if (endDate == 'Invalid Date')
			return true;
		if (endDate >= startDate)
			return true;
		if (endDate <= startDate)
			return false;
	});
	$.validator.addMethod("AlphabetsOnly", function(value, element) {
		return this.optional(element) || /^[a-z][a-z@\s]*$/i.test(value);
	});
	$.validator.addMethod("AlphaNumericOnly", function(value, element) {
		return this.optional(element) || /^[a-zA-Z0-9\s]*$/i.test(value);
	});
	$.validator.addMethod('duplicatecategoryId', function(value, element) {
		if (categoryIDExist) {
			return false;
		} else {
			return true;
		}
	});
	
	$("#categoryId").change(function() {
		var categoryId = $('#categoryId').val();
		$.get("${contextPath}/admin/checkcategoryId?categoryId=" + categoryId, function(data, status) {
			if (status == "success" && data != '') {
				categoryIDExist = true;
			} else {
				categoryIDExist = false;
			}
			$("#categoryBean").validate().element('#categoryId');
			
		});
	});
	$("#categoryName").change(function() {
		//alert("calling check val");
		var categoryName = $('#categoryName').val();
		var categoryNameHidden = $("#categoryNameHidden").val();
		categoryNameExist = false;
		
		if(categoryName == '' || categoryName == categoryNameHidden){
			$("#categoryBean").validate().element('#categoryName');
		}
		else{
			$.get("${contextPath}/admin/checkCategoryName?categoryName=" + categoryName, function(data, status) {
				if (status == "success" && data != '') {
					categoryNameExist = true;
				} else {
					categoryNameExist = false;
				}
				$("#categoryBean").validate().element('#categoryName');
			});
		}
	});
	
	$("#createCategoryName").change(function() {
		var createCategoryName = $('#createCategoryName').val();
		var prtNo = $('#partName').val();
		
		partnerCategoryNameExist = false;
	
			$.get("${contextPath}/admin/checkCreatePartnerCategoryName?createCategoryName=" + createCategoryName+"&prtNo="+prtNo, function(data, status) {
				if (status == "success" && data != '') {
					partnerCategoryNameExist = true;
				} else {
					partnerCategoryNameExist = false;
				}
				$("#createPartnerCatForm").validate().element('#createCategoryName');
			});
		
	});
	
	$("#createCategoryId").change(function() {
		var createCategoryId = $('#createCategoryId').val();
		partnerCategoryIdExist = false;
	
	
			$.get("${contextPath}/admin/checkCreatePartnerCategoryId?createCategoryId=" + createCategoryId, function(data, status) {
				if (status == "success" && data != '') {
					partnerCategoryIdExist = true;
				} else {
					partnerCategoryIdExist = false;
				}
				$("#createPartnerCatForm").validate().element('#createCategoryId');
			});
		
	});
	
	$("#edit_partnercategoryname_update").change(function() {
		//alert()
		var createCategoryName = $('#edit_partnercategoryname_update').val();
		var prtNo = $("#edit_partnerno_update").val();
		partnerCategoryEditUpNameExist = false;
	
	
			$.get("${contextPath}/admin/checkCreatePartnerCategoryName?createCategoryName=" + createCategoryName+"&prtNo="+prtNo, function(data, status) {
				if (status == "success" && data != '') {
					partnerCategoryEditUpNameExist = true;
				} else {
					partnerCategoryEditUpNameExist = false;
				}
				$("#editmapcategoryBeanUpdate").validate().element('#edit_partnercategoryname_update');
			});
		
	});
	
	
	$("#edit_partnercategoryId_update").change(function() {
		//alert()
		var createCategoryId = $('#edit_partnercategoryId_update').val();
		partnerCategoryEditUpIdExist = false;
	
	
			$.get("${contextPath}/admin/checkCreatePartnerCategoryId?createCategoryId=" + createCategoryId, function(data, status) {
				if (status == "success" && data != '') {
					partnerCategoryEditUpIdExist = true;
				} else {
					partnerCategoryEditUpIdExist = false;
				}
				$("#editmapcategoryBeanUpdate").validate().element('#edit_partnercategoryId_update');
			});
		
	});
	
	
	$("#categoryName").change(function() {
	
		var categoryName = $('#categoryName').val();
		var categoryNameHidden = $("#categoryNameHidden").val();
		categoryNameExist = false;
		
		if(categoryName == '' || categoryName == categoryNameHidden){
			$("#categoryBean").validate().element('#categoryName');
		}
		else{
			$.get("${contextPath}/admin/checkCategoryName?categoryName=" + categoryName, function(data, status) {
				if (status == "success" && data != '') {
					categoryNameExist = true;
				} else {
					categoryNameExist = false;
				}
				$("#categoryBean").validate().element('#categoryName');
			});
		}
	});
	
	
	
	$("#startValueID").change(function() {
		changeValidationRule();
	});
	
	$("#endValueID").change(function() {
		changeValidationRule();
	});
		
	$("#stepValueID").change(function() {
		changeValidationRule();
	});
	function changeValidationRule(){
		var flag = false;
		
		var startValue=$("#startValueID").val();
		var endValue=$("#endValueID").val();
		var stepValue=$("#stepValueID").val();
		
		if(endValue != "" || startValue != "" || stepValue != ""){
			flag = true;
		}
		
		$('#startValueID').rules('add', {
			required: flag,
			number: true,
			startValueNotZero: true
		});
		$('#endValueID').rules('add', {
			required: flag,
			number: true,
			greaterThanFilter: true,
			endValueNotZero: true
		});
		$('#stepValueID').rules('add', {
			required: flag,
			number: true,
			greaterThanEndValue : true,
			stepValueNotZero: true
		});
		if(flag == false){
			$("#categoryBean").validate().element('#startValueID');
			$("#categoryBean").validate().element('#endValueID');
			$("#categoryBean").validate().element('#stepValueID');
		}
	}
	$.validator.addMethod("startValueNotZero", function(value, element) {
		var startValue = $("#startValueID").val();
		if(startValue == '0'){
			return false;
		} else {
			return true;
		}
	});
	$.validator.addMethod("endValueNotZero", function(value, element) {
		var endValue = $("#endValueID").val();
		if(endValue == '0'){
			return false;
		} else {
			return true;
		}
	});
	$.validator.addMethod("stepValueNotZero", function(value, element) {
		var stepValue=$("#stepValueID").val();
		if(stepValue == '0'){
			return false;
		} else {
			return true;
		}
		return isValueZero;
	});
	$.validator.addMethod("greaterThanEndValue", function(value, element) {
		var stepValue = parseInt($("#stepValueID").val());
		var endValue = parseInt($("#endValueID").val());
		var flag = true;
		if(parseInt(endValue) < parseInt(stepValue)){
			flag = false;
		}
		if(stepValue == '0') {
			$.validator.addMethod("stepValueNotZero", function(value, element) {
				var stepValue=$("#stepValueID").val();
				if(stepValue == '0'){
					return false;
				} else {
					return true;
				}
			});
		}
		return flag;
	});
	$.validator.addMethod("greaterThanFilter", function(value, element) {
		var startValue=parseInt($("#startValueID").val());
		var endValue=parseInt($("#endValueID").val());
		var flag = true;
		if(startValue > endValue){
			flag = false;
		}
		return flag;
	});
	$.validator.addMethod("editGreaterThanFilter", function(value, element) {
		var startValue=parseInt($("#startValueID").val());
		var endValue=parseInt($("#endValueID").val());
		
		var flag = true;
		if(startValue > endValue){
			flag = false;
		}
		return flag;
	});
	
	$.validator.addMethod('dupliCatecategoryName', function(value, element) {
		if (categoryNameExist) {
			return false;
		} else {
			return true;
		}
	});
	
	
	$.validator.addMethod('duplicatecategoryNamecreate', function(value, element) {
		if (partnerCategoryNameExist) {
			return false;
		} else {
			return true;
		}
	});
	
	
	$.validator.addMethod('duplicatecategoryIdcreate', function(value, element) {
		if (partnerCategoryIdExist) {
			return false;
		} else {
			return true;
		}
	});
	
	//duplicateeditupdatecatname edit_partnercategoryname_update editmapcategoryBeanUpdate
	$.validator.addMethod('duplicateeditupdatecatname', function(value, element) {
		if (partnerCategoryEditUpNameExist) {
			return false;
		} else {
			return true;
		}
	});
	//duplicateeditupdatecatid edit_partnercategoryId_update partnerCategoryEditUpIdExist
	$.validator.addMethod('duplicateeditupdatecatid', function(value, element) {
		if (partnerCategoryEditUpIdExist) {
			return false;
		} else {
			return true;
		}
	});
	
	
	$.validator.addMethod("dupCategorySortOrder", function(value, element) {
		return !dupCategorySortOrder;
	});
	
	$.validator.addMethod('gqcDisplayOrderNotZero', function(value, element) {
		var displayOrder = parseInt($('#gqcDisplayOrder').val());
		
		if(displayOrder == 0){
			return false;
		}
		else{
			return true;
		}
	});
	
	$(document).ready(function(){
		$('#createPartnerCatForm').validate({
		
			ignore:[],	
			rules: {
				partnerType:{
					required: true
				}, 
				partName:{
					required: true
				},
				createCategoryName:{
					required: true,
					duplicatecategoryNamecreate : true
				},
				createCategoryId:{
					required: true,
					//AlphaNumericOnly : true,
					duplicatecategoryIdcreate : true
				}
				
			}, 
			messages: {
				partnerType:{
					 required:"Please Select Partner Type."
				 },
				 partName:{
					 required:"Please Select Partner Name."
				 },
				 createCategoryName:{
					 required:"Category Name column cannot be left Blank.",
					 duplicatecategoryNamecreate : "Partner Category Name already exists"
				 },
				 createCategoryId:{
				// AlphaNumericOnly : "AlphaNumeric only(No special characters allowed)",
				 required: "Create Category Id cannot be left Blank.",
				 duplicatecategoryIdcreate : "Partner Category Id already exist"
				 }
			},
			errorElement: "div",
			errorPlacement: function(error, element) {
			if ($(element).hasClass("uploadBtn2")){
		    	error.insertAfter($(element).closest("div"));
			}
			else {
		    	error.insertAfter(element);
			}

			},
		});
	});
	
	
	
	
	$("#categoryBean").validate({
				onkeyup : true,
				ignore : [],
				rules : {
					categoryName : {
						required : true,
						AlphaNumericOnly : true,
						dupliCatecategoryName : true,
						rangelength : [1, 50]
					},
					categoryImage : {
						extension : "png|jpg|jpeg|gif",
						//filesize : 1000000,
					},
					categoryImageUrl : {
						rangelength : [1, 250]
					},
					startValue : {
						required : false,
						number: true,
						startValueNotZero : false
					},
					endValue : {
						required : false,
						number: true,
						greaterThanFilter : false,
						endValueNotZero : false
					},
					stepValue : {
						required : false,
						number : true,
						greaterThanEndValue : false,
						stepValueNotZero : false
					},
					startDate : {
						required : true,
						dateHigherThanToday1 : true
					},
					endDate : {
						required : false,
						greaterThanEndDate1 : true,
						dateHigherThanToday1 : true
					},
					gqcDisplayOrder : {
						required: true,
						digits: true,
						gqcDisplayOrderNotZero: true,
						dupCategorySortOrder: true
					}
				},
				messages : {
					categoryName : {
						required : "Please enter category Name",
						minlength : "Please enter atleast 3 characters",
						AlphaNumericOnly : "AlphaNumeric only(No special characters allowed)",
						dupliCatecategoryName : "Category Name already exists",
						rangelength : "Maximum 50 characters permitted"
					},
					categoryImage : {
						extension:"Invalid file format. Only jpg|jpeg|png|gif files are allowed",
						//filesize : "File size must be less than 1MB",
					},
					categoryImageUrl : {
						rangelength : "Maximum 250 characters permitted"
					},
					startValue: {
						required : "Please enter start value",
						number: "Please enter number only",
						startValueNotZero: "Value should not be 0"
					},
					endValue : {
						required : "Please enter end value",
						number: "Please enter number only",
						greaterThanFilter:"Filter end Value should be greater than Start value",
						endValueNotZero: "Value should not be 0"
					},
					stepValue : {
						required : "Please enter step value",
						number: "Please enter number only",
						greaterThanEndValue : "Filter Step Value should be lesser than end value",
						stepValueNotZero: "Value should not be 0"
					},
					startDate : {
						required : "Start date cannot be empty",
						dateHigherThanToday1 : "Start date must be less than or equal End date "
					},
					endDate : {
						required : "End date cannot be empty",
						greaterThanEndDate1 : "End date must be greater than or equal to Start date ",
						dateHigherThanToday1 : "Start date must be greater than or equal End date "
					},
					gqcDisplayOrder : {
						required: "Please enter category sort order",
						digits: "Please enter numeric data",
						gqcDisplayOrderNotZero: "Display order should not be zero",
						dupCategorySortOrder: "Category sort order already exists"
					}
				},
				errorElement : "div",
				errorPlacement : function(error, element) {
					if ($(element).hasClass("form-control startDate")) {
						error.insertAfter($(element).closest("div"));
					} else if ($(element).hasClass("form-control endDate")) {
						error.insertAfter($(element).closest("div"));
					}else if ($(element).hasClass("categoryImageUpload")) {
						error.insertAfter($(element).closest("div"));
					}
					else {
						error.insertAfter(element);
					}
				},
				submitHandler : function(form) {
					if($("#gqcDisplayOrder").val() == ""){
						$("#gqcDisplayOrder").val('0');
					}
					form.submit();
				},
	});
	$('#partnerNo').on('selectmenuchange', function() {
		var partnerno = $(this).closest('tr').data('category_partnerno_');
		$(this).closest('form').trigger('submit');
	});
	
	/* $('#partNameUp').on('selectmenuchange', function() {
		var partnernomap = $(this).closest('tr').data('category_partnernomap_');
		$(this).closest('form').trigger('submit');
	}); */
	
	$('#partNameUp').on('selectmenuchange', function() {
		var partnameup=$('#partNameUp').val() ;
		//alert(partnameup);
		var partnertypeup=$('#partnerTypeUp').val();
		$.redirect("${contextPath}/admin/mapCreatePartnerCategories", {'partnameup': partnameup, 'partnertypeup': partnertypeup});				
	});
	
	
	$('span#deleteCategory').on('click', function() {
		var id = $(this).closest('tr').data('id');
		var categoryId = $(this).closest('tr').data('categoryid');
		var categoryName = $(this).closest('tr').data('categoryname');
		$('#deleteCategoryId').text(categoryName + " " + " Category will be Deactivated. Do you want to continue?");
		$('#myModala').data('id', id).modal('show');
	});

	$('#btnDelteYes').click(function() {
		var id = $('#myModala').data('id');
		$.get("${contextPath}/admin/deleteCategory?catNo=" + id,
			function(status) {
				if (status == "success") {
					$('[data-id=' + id + ']').remove();
					$('#myModala').modal('hide');
					window.location.href = "${contextPath}/admin/category?deleteCatMsg=" + status;
				}
				if (status == "failure") {
					$('[data-id=' + id + ']').remove();
					$('#myModala').modal('hide');
					window.location.href = "${contextPath}/admin/category?deleteCatMsg=" + status;
				}
			});
	});
	$('span.view_details').on('click', function() {
		var id = $(this).closest('tr').data('id');
		var categoryid = $(this).closest('tr').data('categoryid');
		var categoryname = $(this).closest('tr').data('categoryname');
		var partnername = $(this).closest('tr').data('category_partnername');
		var startdate = $(this).closest('tr').data('startdate');
		var enddate = $(this).closest('tr').data('enddate');

		var categorystatus = $(this).closest('tr').data('categorystatus');
		var categorystatuscode = $(this).closest('tr').data('categorystatuscode');
		var categorystatusclass = $(this).closest('tr').data('categorystatusclass');

		$('#categoryid').text(categoryid);
		$('#categoryname').text(categoryname);
		$('#startdate').text(startdate);
		$('#enddate').text(enddate);

		$('#partnername').text(partnername);
		$('#categorystatus').html("<font class="+categorystatusclass+">" + categorystatus);
		$('#existing_category_view').data('id', id).modal('show');
	});
	$('#cancelbtnD').click(function() {
		$.get("${contextPath}/admin/addCategoryCancel", function(data, status) {
			if (status == "success") {
				window.location.replace("${contextPath}/" + data);
			}
		});
	});
	function uploadcategoryImage(id, type, url) {
		var formData = new FormData();
		formData.append('file', $("#" + id)[0].files[0]);
		if ($("#categoryBean").validate().element("#" + id)) {
			$.ajax({
				url : "${contextPath}/admin/updatecategoryLogo?type=" + type,
				data : formData,
				dataType : 'text',
				processData : false,
				contentType : false,
				type : 'POST',
				success : function(response) {
					$('#' + url).val(response);
					var newImageUrl = '${contextPath}' + '/admin/images/' + type + "/" + response;
					$('#categoryImage_').prop('src', newImageUrl);
					$("#categoryBean").validate().element("#" + url);
				}
			});
		}
	}
	
	$(document).ready(function () {
		//For Category 
		if(!$("#goQuicklyCategory").is(":checked")) {
			 $("#gqcDisplayOrder").prop('readonly',true);
			 $(".gqDisplayOrder").hide();
			 setValidationRuleForCategoryDisplayOrder(false);
		 }
	});
	
	$("#goQuicklyCategory").on('change', function() {
		
	    if($(this).is(":checked")) {
	    	$('#goQuicklyCategory').val(1);
	    	$("#gqcDisplayOrder").prop('readonly',false);
	    	$(".gqDisplayOrder").show();
	    	$('#gqcDisplayOrder').val($("#gqcDisplayOrderHidden").val());
	    	setValidationRuleForCategoryDisplayOrder(true);
	    }
	    else{
	    	$('#goQuicklyCategory').val(0);
	    	$("#gqcDisplayOrder").prop('readonly',true);
	    	$(".gqDisplayOrder").hide();
	    	$('#gqcDisplayOrder').val('');
	    	setValidationRuleForCategoryDisplayOrder(false);
	    }
	});
	
	$("#gqcDisplayOrder").on('blur', function() {
		var gqcDisplayOrder = parseInt($("#gqcDisplayOrder").val());
		var gqcDisplayOrderHidden = parseInt($("#gqcDisplayOrderHidden").val());
		dupCategorySortOrder=false;

		if(gqcDisplayOrder == 0 || gqcDisplayOrder == gqcDisplayOrderHidden || $("#gqcDisplayOrder").val() == ''){
			$("#categoryBean").validate().element("#gqcDisplayOrder");
		}
		else{
			$.get("${contextPath}/admin/checkCategoryDispOrd?displayOrder="+gqcDisplayOrder, function(data,status) {
				if(status=="success" && data!=''){
					dupCategorySortOrder=true;	
				}
				else{
					dupCategorySortOrder=false;
				}
				$("#categoryBean").validate().element('#gqcDisplayOrder');				
			});
		}
	});
	
	function setValidationRuleForCategoryDisplayOrder(mandatory){
		$('#gqcDisplayOrder').rules('add', {
			required: mandatory,
			digits: true,
			dupCategorySortOrder: true
		});
		
	    if(!mandatory){
	    	dupCategorySortOrder=false;
	    	$("#categoryBean").validate().element("#gqcDisplayOrder");	
	    }
	}
	
	$("#datepicker1").on('change', function() {
		$("#categoryBean").validate().element("#datepicker1");
		var endDate = getFormattedDate($('#datepicker2').val());

		if (endDate != 'Invalid Date') {
			$("#categoryBean").validate().element("#datepicker2");
		}
	});
	
	$("#datepicker2").on('change', function() {
		$("#categoryBean").validate().element("#datepicker2");
		$("#categoryBean").validate().element("#datepicker1");
	});
	$("#datepicker11").on('change', function() {
		$("#editCategoryBean").validate().element("#datepicker11");
		//TODO:AW-284
		var endDate = getFormattedDate($('#datepicker12').val());
		if (endDate != 'Invalid Date') {
			$("#editCategoryBean").validate().element("#datepicker12");
		}
	});
	$("#datepicker12").on('change', function() {
		$("#editCategoryBean").validate().element("#datepicker12");
		$("#editCategoryBean").validate().element("#datepicker11");
	});
</script>

<script>
var currentDate = $.datepicker.formatDate('dd/mm/yy', new Date());
if("${editCategoryBean.edit_categoryid}"!=''){
	var startDate = $('#datepicker11').val();
	if(getFormattedDate(startDate) < getFormattedDate(currentDate)){
		
		$('#datepicker11').attr("disabled","true");
	}
}

var fullDate = new Date();
var twoDigitMonth = ((fullDate.getMonth().length+1) === 1)? (fullDate.getMonth()+1) : '0' + (fullDate.getMonth()+1);
var today = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();

$.validator.addMethod("dateHigherThanToday", function(value, element) {
	var startDate = getFormattedDate($('#datepicker11').val());
	if(startDate=='Invalid Date')
		startDate=getFormattedDate($('#datepicker11').val());
	var endDate = getFormattedDate($('#datepicker12').val());
	
	if(endDate=='Invalid Date')
		endDate=getFormattedDate($('#datepicker12').val());
	if(endDate!==null){
		if (startDate <= endDate)
			return true;
		if (startDate >= endDate)
			return false;
	}
});
$.validator.addMethod("greaterThanEndDate", function(value, element) {
	var startDate = getFormattedDate($('#datepicker11').val());
	if(startDate=='Invalid Date')
		startDate=getFormattedDate($('#datepicker11').val());
	var endDate = getFormattedDate($('#datepicker12').val());
	if(endDate=='Invalid Date')
		endDate=getFormattedDate($('#datepicker12').val());
	if (endDate >= startDate)
		return true;
	if (endDate <= startDate)
		return false;
});

$.validator.addMethod('filesize', function (value, element, param) {	    	
    return this.optional(element) || (element.files[0].size <= param)
}, 'File size must be less than 1MB');
$("#edit_categoryid").change(function(){
	var categoryId=$('#edit_categoryid').val();
	//TODO:To check existing categoryId
	var categoryIdHidden = $('#edit_categoryidHidden').val();
	
	if(categoryId != categoryIdHidden){
		$.get("${contextPath}/admin/checkcategoryId?categoryId="+categoryId, function(data,status) {
			if(status=="success" && data!=''){
				categoryIDExist=true;	
			} else {
				
				categoryIDExist=false;
			}
			$("#editCategoryBean").validate().element('#edit_categoryid');
		});
	} else {
		categoryIDExist=false;
	}
});
$("#editCategoryBean").validate({
	onkeyup: true,
	ignore:[],
   	rules: {
   		edit_categoryid:{
			required: true,
			duplicatecategoryId:true,
			AlphaNumericOnly: true
		},
		edit_categoryname:{
			required: true,
			AlphaNumericOnly: true
		},
		datepicker11:{
    		required:true,
    		dateHigherThanToday:true
    	},
    	datepicker12:{
    		required:true,
    		greaterThanEndDate:true
    	}
 	},
 	messages: {
	 	edit_categoryid:{
			required: "Please enter Category ID",
			duplicatecategoryId:"Category ID already exists",
			AlphaNumericOnly:"AlphaNumeric only(No special characters allowed)"
		},
		edit_categoryname:{
			required:"Please enter category Name",
			AlphaNumericOnly:"AlphaNumeric only(No special characters allowed)"
		},
		editCategoryImage:{
			extension:"Invalid file format. Only jpg|jpeg|png|gif files are allowed."
		},
		edit_categoryimageurl:{
	    	 required:"Category Image Url field should not be empty",
			 rangelength:"Maximum 100 characters permitted"
	    },
	    datepicker11:{
			required:"Please Select Start Date",
			dateHigherThanToday:"Start Date must be less than or equal to End date"
			
		},
		datepicker12:{
			required:"Please Select End Date",
			greaterThanEndDate:"End Date must be greater than or equal to Start Date"
		}
 	},
 	errorElement: "div",
 	errorPlacement: function(error, element) {
     	if ($(element).hasClass("form-control startDate")) {
         	error.insertAfter($(element).closest("div"));
     	} else if ($(element).hasClass("form-control endDate")){
      		error.insertAfter($(element).closest("div"));
     	} else {
         	error.insertAfter(element);
     	}
 	},
 	submitHandler: function (form) {
		form.submit();
	},
});
$('span.editmappedcategoryAmazon').on('click', function () {
	 
 	var id = $(this).closest('tr').data('id');
 	    
 	var amazonPartnerName = $(this).closest('tr').data('prtname');
	var amzonCategoryName = $(this).closest('tr').data('prtcatname');
	var amazonOrderCategoryName= $(this).closest('tr').data('category_amzcategoryname_');
	var amazoncategoryId= $(this).closest('tr').data('amazoncategoryid');
	var amazoncatStatus =  $(this).closest('tr').data('amazoncategorystatus');
	var amazonprtno =  $(this).closest('tr').data('prtno');

	var amazonprtcatno =  $(this).closest('tr').data('prtcatno');

	$('#id').val(id);

	$('#edit_amazonpartnername_').val(amazonPartnerName);
	$('#edit_amazonpartnercategoryname_').val(amzonCategoryName);
	$('#amazonorderCategoryName').val(amazonOrderCategoryName);
	$('#amazonCategoryId').val(amazoncategoryId);
	$('#amazonPrtNo').val(amazonprtno);
	$('#amazonPrtCatNo').val(amazonprtcatno);
	$('#edit_amazonpartcategorystatus_'+amazoncatStatus).prop("checked",true);	
	
	$('#editAmazonCategory').data('id',id).modal('show');
	/* $('#edit_categoryidHidden').val(categoryid); */
});
$('#updateamazonCategory').click(function () {
	var json={};
	json["id"]=$('#id').val();
	var createpartcatstatus = $('input[name=edit_amazonpartcategorystatus]:checked').val();
	json["partNumber"]=$('#amazonPrtNo').val();
	json["amazonCategoryName"]=$("#amazonPrtCatNo").val();
	json["amazonOrderCategoryId"]=$("#amazonCategoryId").val();
	json["createpartcatstatus"]=createpartcatstatus;
	json["amazonOrderCategoryName"]=$("#amazonorderCategoryName").val();
	var formData = new FormData();
	formData.append('json',JSON.stringify(json));
	if($("#editAmazonCategoryBean").validate().element('#amazonorderCategoryName')
			&& $("#editAmazonCategoryBean").validate().element('#amazonCategoryId')
			){
	$.ajax({
		  url: "${contextPath}/admin/updateAmazonCategory",
		  data: formData,
		  dataType: 'text',
		  processData: false,
		  contentType: false,
		  type: 'POST',
		  success: function (response) {
			  $('#editAmazonCategory').modal('hide');
			  window.location.href="${contextPath}/admin/category?updateAmazonMessage="+response;
		  },
		  error: function (response) {
			  $('#editAmazonCategory').modal('hide');
			  window.location.href="${contextPath}/admin/category?updateAmazonMessage=failure";
     	  }
	});
	}
});
$('span.editcategory').on('click', function () {
 	var id = $(this).closest('tr').data('id');
 	var categoryid = $(this).closest('tr').data('categoryid');
	var categoryname = $(this).closest('tr').data('categoryname');
	var startdate= $(this).closest('tr').data('startdate');
	var enddate= $(this).closest('tr').data('enddate');
	var categorystatus= $(this).closest('tr').data('categorystatus');
	var categorystatuscode= $(this).closest('tr').data('categorystatuscode');
	var categorystatusclass= $(this).closest('tr').data('categorystatusclass');

	$('#edit_categoryid').val(categoryid);
	$('#edit_categoryname').val(categoryname);
	$('#datepicker11').val(startdate);
	$('#datepicker12').val(enddate);
	$('#edit_categorystatus_'+categorystatuscode).prop("checked",true);	
	$('#myModal1').data('id',id).modal('show');
	$('#edit_categoryidHidden').val(categoryid);
});
$('#updateCategory').click(function () {
	 	var id = $('#myModal1').data('id');
		var startdate,enddate;
	  	var json={};
		json["catNo"]=id;
		json["categoryId"]=$("#edit_categoryid").val();
		json["categoryName"]=$("#edit_categoryname").val();
	
		startdate=$("#datepicker11").val();
		if(startdate=='')
			startdate=$("#datepicker11").val();
		
		enddate=$("#datepicker12").val();
		if(enddate=='')
			enddate=$("#datepicker12").val();
		json["startDate"]=startdate;
		json["endDate"]=enddate;
		json["categoryStatus"]=$('input[name=edit_categorystatus]:checked').val();$( "#startDate" ).datepicker({
	        dateFormat: 'dd/mm/yy',
	        minDate: '0',
			showOn: "button",
			buttonImage: "./static/img/2016-06-23.png",
			buttonImageOnly: true,
			buttonText: "Select date"
		});
		$( "#endDate" ).datepicker({
	        dateFormat: 'dd/mm/yy',
	        minDate: '0',
			showOn: "button",
			buttonImage: "./static/img/2016-06-23.png",
			buttonImageOnly: true,
			buttonText: "Select date"
		});
		var formData = new FormData();
		formData.append('json',JSON.stringify(json));	
		if($("#editCategoryBean").validate().element('#edit_categoryid')
		 && $("#editCategoryBean").validate().element('#edit_categoryname')
		 && $("#editCategoryBean").validate().element('#datepicker11')
		 && $("#editCategoryBean").validate().element('#datepicker12')){		
			
			$.ajax({
				  url: "${contextPath}/admin/updateCategory",
				  data: formData,
				  dataType: 'text',
				  processData: false,
				  contentType: false,
				  type: 'POST',
				  success: function (response) {
					  $('#myModal1').modal('hide');
					  window.location.href="${contextPath}/admin/category?updateMessage="+response;
				  },
				  error: function (response) {
					  $('#myModal1').modal('hide');
					  window.location.href="${contextPath}/admin/category?updateMessage=failure";
	           	  }
			});
		}
	});
	
	
	
$('#createPartnerSave').click(function () {
	var partNumber = $("#partName option:selected"). val();
	var createCategoryName = $('#createCategoryName').val();
	var createCategoryId = $('#createCategoryId').val();
	var createpartcatstatus = 1;
	//statusforcat
	var mainStatus = $('input[name=partcategorystatusupdate]:checked').val();
	var json={};
	json["partNumber"]=partNumber;
	json["createCategoryName"]=createCategoryName;
	json["createCategoryId"]=createCategoryId;
	json["mainStatus"] = mainStatus;
	json["createpartcatstatus"] = createpartcatstatus;
	var formData = new FormData();
	formData.append('json',JSON.stringify(json));
	
	if($("#createPartnerCatForm").validate().element('#partnerType')
			&& $("#createPartnerCatForm").validate().element('#partName') 
			&& $("#createPartnerCatForm").validate().element('#createCategoryName')
			&& $("#createPartnerCatForm").validate().element('#createCategoryId') 
			){
	$.ajax({      
		  url: "${contextPath}/admin/createPartnerCategory",
		  data: formData,
		  dataType: 'text',
		  processData: false,
		  contentType: false,
		  type: 'POST',
		  success: function (response) {
		  window.location.href="${contextPath}/admin/category?updateMessage="+response;
		  },
		  error: function (response) {
			  window.location.href="${contextPath}/admin/category?updateMessage=failure";
      	 }
		 });
	}
});
	
$('#createAmazonPartnerSave').click(function () {
	var amazonPrtNo = $("#amazonPrtNo"). val();
	var amazonCategoryName = $('#amazonPartnerCategory').val();
	var amazonOrderCategoryId = $('#amazonOrderCategoryId').val();
	var amazonOrderCategoryName = $('#amazonOrderCategoryName').val();
    var createpartcatstatus = 1;
	//statusforcat
	var mainStatus = $('input[name=partcategorystatusupdate1]:checked').val();
	var json={};
	json["partNumber"]=amazonPrtNo;
	json["amazonCategoryName"]=amazonCategoryName;
	json["amazonOrderCategoryId"]=amazonOrderCategoryId;
	json["amazonOrderCategoryName"]=amazonOrderCategoryName;
	json["createpartcatstatus"] = createpartcatstatus;
	json["mainStatus"] = mainStatus;
	var formData = new FormData();
	formData.append('json',JSON.stringify(json));
	
	if($('#createAmzonPartnerCatForm').validate().element("#amazonPrtNo") &&
			$('#createAmzonPartnerCatForm').validate().element("#amazonPartnerCategory") &&
			$('#createAmzonPartnerCatForm').validate().element("#amazonOrderCategoryId") &&
			$('#createAmzonPartnerCatForm').validate().element("#amazonOrderCategoryName")){
	$.ajax({      
		  url: "${contextPath}/admin/createAmazonPartnerCategory",
		  data: formData,
		  dataType: 'text',
		  processData: false,
		  contentType: false,
		  type: 'POST',
		  success: function (response) {
		  window.location.href="${contextPath}/admin/category?updateAmazonMessage="+response;
		  },
		  error: function (response) {
			  window.location.href="${contextPath}/admin/category?updateAmazonMessage=failure";
      	 }
		 });
	}
}); 
$('#amazonPrtNo').on(
		'selectmenuchange',
		function() {
			var obj = null;
			var partnerNo = $("#amazonPrtNo option:selected").val();
			$.get("${contextPath}/admin/selectPartnerCategoires?partNo="
					+ partnerNo, function(data) {
				var obj = jQuery.parseJSON(data);
				$('#amazonPartnerCategory').empty();
				$('#amazonPartnerCategory').append(
						"<option value=''>Select</option>");

				for (var i = 0; i < obj.length; i++) {
					var value = obj[i];
					var newOption = $("<option value="+value.prtCatNo+">"
							+ value.partnercategoryName + "</option>");
					$('#amazonPartnerCategory').append(newOption);
				}

				$("#amazonPartnerCategory").selectmenu("refresh");
			});
		});

$("#partnerCategory").on("selectmenuselect", function() {
	var prtNo = $("#partnerNo option:selected").val();
	$("#rpName").val('');
	$("#prtProductNo").val('');
	$("#datepicker1").trigger("change");
	if (prtNo == '') {
		alert("Please select the Partner");
		$("#rpName").val('');
		$("#partnerNo-button").focus();
		return false;
	}
});

	
	function editUploadImage(id,type,url){
		//var fileUpload = $("#editCategoryImage")[0];			 
	   // checkFileUpload(fileUpload);
	   // if(!uploadFlag) return false;
	   	var formData = new FormData();
		formData.append('file',$("#"+id)[0].files[0]);	
		$.ajax({
			url: "${contextPath}/admin/updatecategoryLogo?type="+type,
		  	data: formData,
		  	dataType:'text',
		  	processData: false,
		  	contentType: false,
		  	type: 'POST',
		  	success: function (response) {
			  	$('#'+url).val(response);			 
			  	var newImageUrl='${contextPath}'+'/admin/images/'+type+"/"+response;			 
			  	$('#editCategoryImage_').prop('src',newImageUrl);
			  	$("#editmapcategoryBean").validate().element("#"+url);
		  	}
		});			
	}
	
	
	$("#partnerType").on( "selectmenuchange", function(event, type, mapCat){
		var obj=null;
		var prtType = $("#partnerType").val();
		$.get("${contextPath}/admin/getCategoryMappingPartner?prtType="+prtType, function(data){
			var obj = jQuery.parseJSON(data);
			$('#partName').empty();
			$('#partName').append("<option value=''>-Select-</option>");
			
			for(var i = 0; i < obj.length; i++){
				var value = obj[i];
				var newOption = $("<option value="+value.prtNo+">"+value.partnerName+"</option>");
				$('#partName').append(newOption);	       
			}
			$( "#partnerType" ).selectmenu( "refresh" );
			
			 if(type == "refresh"){
				$("#partnerType").selectmenu("refresh");
				$('#partName').val(mapCat);
			}else{
				$("#partName").selectmenu( "refresh" );
				$("#createCategoryName").val('');
			} 
		});
		 
	});
	
	
	$("#partnerTypeUp").on( "selectmenuchange", function(event, type, mapCat2){
		
		var obj=null;
		var prtType = $("#partnerTypeUp").val();
		
		$.get("${contextPath}/admin/getCategoryMappingPartner?prtType="+prtType, function(data){
			var obj = jQuery.parseJSON(data);
			$('#partNameUp').empty();
			$('#partNameUp').append("<option value=''>-Select-</option>");
			
			for(var i = 0; i < obj.length; i++){
				var value = obj[i];
				var newOption = $("<option value="+value.prtNo+">"+value.partnerName+"</option>");
				$('#partNameUp').append(newOption);	       
			}
			$("#partnerTypeUp").selectmenu( "refresh" );
			
			 if(type == "refresh"){
				$("#partnerTypeUp").selectmenu("refresh");
				$('#partNameUp').val(mapCat2);
			}else{
				$("#partNameUp").selectmenu( "refresh" );
			} 
		});
		 
	});
	
</script>
<script>
$("#mapcategoryBean").validate({
	onkeyup: true,
	ignore:[],
   	rules: {
   	 partnerNo:{
   		dropdowncheck:true
   	 }
 },
 messages: {
	 partnerNo:{
		dropdowncheck:"Please Select Partner Name"
	 }
 },
 errorElement: "div",
 submitHandler: function (form) {
	form.submit();
 },
});


$('span.view_mapped_details').on('click', function () {
	var id = $(this).closest('tr').data('id');
	var categoryid_ = $(this).closest('tr').data('categoryid1');
	var categoryname_ = $(this).closest('tr').data('categoryname_');
	var partnername_= $(this).closest('tr').data('category_partnername_');
	var partnercategoryname= $(this).closest('tr').data('category_partnercategoryname_');
	var partnerId_= $(this).closest('tr').data('category_partnerid_');
	var categorystatus= $(this).closest('tr').data('categorystatus_');
	var categorystatuscode= $(this).closest('tr').data('categorystatuscode_');
	var categorystatusclass= $(this).closest('tr').data('categorystatusclass_');
	//var partnercategoryname=$(this).closest('tr).data('category_partnercategoryname_');
	$('#partnername_').text(partnername_);
	$('#categoryname_').text(categoryname_);
	$('#partnerCategoryname_').text(partnercategoryname);
	$('#myModalA').data('id',id).modal('show');
});

$('span.view_mapped_detailsUpdate').on('click', function () {
	var id = $(this).closest('tr').data('id');
	var categoryid_ = $(this).closest('tr').data('categoryid1');
	var categoryname_ = $(this).closest('tr').data('categoryname_');
	var partnername_= $(this).closest('tr').data('category_updatepartnername_');
	var partnercategoryname= $(this).closest('tr').data('category_updatepartnercategoryname_');
	var partnerId_= $(this).closest('tr').data('category_partnerid_');
	var categorystatus= $(this).closest('tr').data('categorystatus_');
	var categorystatuscode= $(this).closest('tr').data('categorystatuscode_');
	var categorystatusclass= $(this).closest('tr').data('categorystatusclass_');
	$('#partnername_update').text(partnername_);
	//$('#categoryname_').text(categoryname_);
	var partnercategoryid=$(this).closest('tr').data('part_category_updateid');
	$('#partnerCategoryname_update').text(partnercategoryname);
	$("#partnerCategoryId").text(partnercategoryid);
	$('#myModalAUpdate').data('id',id).modal('show');
});





$('span.editmappedcategory').on('click', function () {
 	var id = $(this).closest('tr').data('id');
 	var categoryid1 = $(this).closest('tr').data('categoryid_');
	var categoryname_ = $(this).closest('tr').data('categoryname_');
	var categoryno = $(this).closest('tr').data('category_categoryno_');
	var partnername1= $(this).closest('tr').data('category_partnername_');
	var partnerno= $(this).closest('tr').data('category_partnerno_');
	var partnerId1= $(this).closest('tr').data('category_partnerid_');
	var partnercategoryno=$(this).closest('tr').data('category_partnercategoryno_');
	var partnercategoryname=$(this).closest('tr').data('category_partnercategoryname_');
	var categoryimageurl= $(this).closest('tr').data('categoryimageurl_');
	var categorystatus= $(this).closest('tr').data('categorystatus_');
	var categorystatuscode= $(this).closest('tr').data('categorystatuscode_');
	
	var categorystatusclass= $(this).closest('tr').data('categorystatusclass_');
	$('#catgry_no').val(categoryno);
	$('#categoryName_details').val(categoryname_);
	$('#edit_partnername_').val(partnername1);
	$('#edit_partnercategoryname_').val(partnercategoryname);
	$('#edit_partnerno_').val(partnerno);
	$('#edit_partcategorystatus_'+categorystatuscode).prop("checked","checked");	
	$('#Partner_category').data('partno',partnerno);
	$('#Partner_category').data('id',id).modal('show');
	});
	
$('span.editmappedcategoryUpdate').on('click', function () {
 	var id = $(this).closest('tr').data('idup');
 	var categoryid1 = $(this).closest('tr').data('categoryupdateid_');
	var categoryname_ = $(this).closest('tr').data('categoryupdatename_');
	var categoryno = $(this).closest('tr').data('category_categoryno_');
	var partnername1= $(this).closest('tr').data('category_updatepartnername_');
	var partnernouop= $(this).closest('tr').data('category_partnernomap_');
	var partnerId1= $(this).closest('tr').data('category_updatepartnerid_');
	var partnercategoryno=$(this).closest('tr').data('category_partnercategoryno_');
	var partnercategoryname=$(this).closest('tr').data('category_updatepartnercategoryname_');
	var categoryimageurl= $(this).closest('tr').data('categoryimageurl_');
	var categorystatusupdate= $(this).closest('tr').data('categoryupdatestatus_');
	var categorystatuscodeupdate= $(this).closest('tr').data('categoryupdatestatuscode_');
	//alert("categorystatuscodeupdate "+categorystatuscodeupdate);
	var categorystatusclassupdate= $(this).closest('tr').data('categoryupdatestatusclass_');
	var partnercategoryid=$(this).closest('tr').data('part_category_updateid');
	$('#catgry_noupdate').val(categoryno);
	$("#edit_partnercategoryId_update").val(partnercategoryid);
	$('#categoryName_detailsupdate').val(categoryname_);
	$('#edit_partnername_update').val(partnername1);
	$('#edit_partnercategoryname_update').val(partnercategoryname);
	$('#edit_partnerno_update').val(partnernouop);
	$('#edit_partcategorystatus_update_'+categorystatuscodeupdate).prop("checked","checked");	
	$('#Partner_categoryUpdate').data('partnum',partnernouop);
	$('#Partner_categoryUpdate').data('id',id).modal('show');
	}); 


$.validator.addMethod('duplicateCategoryDisplayOrder', function(value, element) {
	return !categoryDisplayOrderExist;
});
$.validator.addMethod('displayOrderNotZero', function(value, element) {
	var isValid = false;
	var categoryDisplayOrder = $("#edit_displayorder").val();
	if(categoryDisplayOrder == 0){
		isValid = true;
	}
	return !isValid; 
});

$("#editmapcategoryBean").validate({
	onkeyup: true,
	ignore:[],
   	rules: {
   		catgry_no:{
   		dropdowncheck:true
   	 	}
 },
 messages: {
	 catgry_no:{
			dropdowncheck:"Please Select Category Name"
	 	}
	 },
 errorElement: "div",
 submitHandler: function (form) {
	form.submit();
 },
});


$("#editmapcategoryBeanUpdate").validate({
	onkeyup: true,
	ignore:[],
   	rules: {
   		edit_partnercategoryname_update:{
   		required : true,
   		duplicateeditupdatecatname : true
   	 	},
   	 edit_partnercategoryId_update:{
   		 required : true,
   		duplicateeditupdatecatid : true
   	    }
 },
 messages: {
	 edit_partnercategoryname_update:{
		 required:"Partner Category name cannot be left blank.",
		 duplicateeditupdatecatname:"Partner Category name already exist."
	 	},
	 edit_partnercategoryId_update:{
		 required:"Partner Category Id cannot be left blank.",
		 duplicateeditupdatecatid:"Partner Category Id already exist."
	 }
	 },
 errorElement: "div",
 submitHandler: function (form) {
	form.submit();
 },
});

$("#editAmazonCategoryBean").validate({ 
	onkeyup: true,
	ignore:[],
   	rules: {
   		amazonorderCategoryName:{
   		required : true,
   	 	},
   	 amazonCategoryId:{
   		 required : true,
   	    }
 },
 messages: {
	 amazonorderCategoryName:{
		 required:" Category order cannot be left blank.",
	 	},
	 	amazonCategoryId:{
		 required:"Partner Category Id cannot be left blank.",
	 }
	 },
 errorElement: "div",
 submitHandler: function (form) {
	form.submit();
 },
});
$("#createAmzonPartnerCatForm").validate({
	onkeyup: true,
	ignore:[],
		rules: {
			prtNo:{
			required : true,
		 	},
		 	prtCatNo:{
			 required : true,
		    },
		 amazonOrderCategoryId:{
			 required :true
		 },
		amazonOrderCategoryName:{
			required : true
		}
	},
	messages: {
		prtNo:{
	   		required : "Please select Partner",
	   	 	},
	   	 prtCatNo:{
	   		 required : "Please select partner category ",
	   	    },
	   	 amazonOrderCategoryId:{
	   		 required :"Category Id is required"
	   	 },
	   	amazonOrderCategoryName:{
	   		required : "Amazon Order Category name is required"
	   	}
	 },
	errorElement: "div",
	submitHandler: function (form) {
	form.submit();
	},
	});


$('#updateMappedCategory').click(function () {
	var partrnercategoryNo = $('#Partner_category').data('id');
 	var prtno = $('#Partner_category').data('partno');
  	var json={};
   	json["partnerCatNo"]=partrnercategoryNo;
   	json["partnerNo"]= prtno;
	json["catNo"]=$("#catgry_no").val();
	json["partCatStatus"]=$('input[name=edit_partcategorystatus]:checked').val();
	
	
	var formData = new FormData();
	formData.append('json',JSON.stringify(json));	
	if($("#editmapcategoryBean").validate().element('#catgry_no')){

			$.ajax({
				  url: "${contextPath}/admin/updateMappedCategory",
				  data: formData,
				  dataType: 'text',
				  processData: false,
				  contentType: false,
				  type: 'POST',
				  success: function (response) {
				  $('#Partner_category').modal('hide');
				  		window.location.href="${contextPath}/admin/category?updateMessagecategory="+response+"&prtNo="+prtno;
				  },
				  error: function (response) {
					  $('#Partner_category').modal('hide');
					  window.location.href="${contextPath}/admin/category?updateMessagecategory=failure";
						
	            	 }
		 });
	}
});




$('#updateMappedCategoryPart').click(function () {
	
	 var partrnercategoryNo = $('#Partner_categoryUpdate').data('id');
	 var partnernumber = $('#Partner_categoryUpdate').data('partnum');
	 var json={};
	 json["partnercatnumber"]=partrnercategoryNo;
	 json["partnernumber"]=partnernumber;
	 json["partnercategoryname"]= $("#edit_partnercategoryname_update").val();
	 json["partnercategoryid"]=	$("#edit_partnercategoryId_update").val();
     json["partnercatstatus"]= $('input[name=edit_partcategorystatusupdate]:checked').val();
		
	var formData = new FormData();
	formData.append('json',JSON.stringify(json));	
	if($("#editmapcategoryBeanUpdate").validate().element('#edit_partnercategoryname_update')
			&& $("#editmapcategoryBeanUpdate").validate().element('#edit_partnercategoryId_update')
			){

			$.ajax({
				  url: "${contextPath}/admin/updatePartnerCategoryMapped",
				  data: formData,
				  dataType: 'text',
				  processData: false,
				  contentType: false,
				  type: 'POST',
				  success: function (response) {
				  $('#Partner_categoryUpdate').modal('hide');
				  		window.location.href="${contextPath}/admin/category?updateMessagecategory1="+response+"&partrnercategoryNo="+partrnercategoryNo;
				  },
				  error: function (response) {
					  $('#Partner_categoryUpdate').modal('hide');
					  window.location.href="${contextPath}/admin/category?updateMessagecategory=failure";
						
	            	 }

		 });
	}
});






$('#btnD1').click(function () {
	var id = $('#myModalC').data('id');
	var prtno = $('#myModalC').data('prtno');
	
	var partnerNo = $("#partnerNo").val();
	
	$.get("${contextPath}/admin/deactivateMappedCategory?prtCatNo="+id, function(status) {		
		$('[data-id=' + id + ']').remove();
		$('#myModalC').modal('hide');
		window.location.href="${contextPath}/admin/updatePartnerCategory?deletePrtCatMsg="+status+"&prtNo="+partnerNo;
	});
});

$('#btnDelUpdate').click(function () {
	var id = $('#myModalCUpdate').data('id');
	var prtno = $('#myModalC').data('prtno');
	
	var partnerNo = $("#partNameUp").val();
	
	$.get("${contextPath}/admin/deactivateMappedCategory?prtCatNo="+id, function(status) {		
		$('[data-id=' + id + ']').remove();
		$('#myModalCUpdate').modal('hide');
		window.location.href="${contextPath}/admin/category?deletePrtCatMsgOther="+status;	
		});
});






$('span#deleteMappedCategory').on('click', function () {
	var id = $(this).closest('tr').data('id');
	var partnercategoryno=$(this).closest('tr').data('category_partnercategoryno_');
	var partnercategoryname=$(this).closest('tr').data('category_partnercategoryname_');
	var categoryname1 = $(this).closest('tr').data('categoryname_');
	var categoryno = $(this).closest('tr').data('category_categoryno_');
	var prtno = $(this).closest('tr').data('category_partnerno_');
	$('#deleteMappedCategoryId').text(partnercategoryname+" "+ "mapped with Standard Category "+categoryname1+" will be Deactivated. Do you want to continue?");
	$('#myModalC').data('id', id).modal('show');
	$('#myModalC').data('prtno', prtno).modal('show');
});


$('span#deleteMappedUpdateCategory').on('click', function () {
	var id = $(this).closest('tr').data('idup');
	var partnercategoryno=$(this).closest('tr').data('category_partnercategoryno_');
	var partnercategoryname=$(this).closest('tr').data('category_updatepartnercategoryname_');
	var categoryname1 = $(this).closest('tr').data('category_nameupdate_');
	var categoryno = $(this).closest('tr').data('categoryupdateno');
	var prtno = $(this).closest('tr').data('category_partnernomap_');
	$('#deleteMappedUpdateCategoryId').text(partnercategoryname+" "+ "mapped with Standard Category "+categoryname1+" will be Deactivated. Do you want to continue?");
	$('#myModalCUpdate').data('id', id).modal('show');
	$('#myModalCUpdate').data('prtno', prtno).modal('show');
});


$('#cancelbtnD1').click(function () {
	$.get("${contextPath}/admin/addCategoryCancel",function(data,status) {		
		if(status=="success" ){
			window.location.replace("${contextPath}/"+data);
		}
	});
});
$('#cancelbtnD1_').click(function () {
	$.get("${contextPath}/admin/addCategoryCancel",function(data,status) {		
		if(status=="success" ){
			window.location.replace("${contextPath}/"+data);
		}
	});
});
$('#cancelbtnD2_').click(function () {
	var prtno = $('#Partner_category').data('partno');
	$.get("${contextPath}/admin/updatePartnerCategoryCancel",function(data,status) {
		if(status=="success" ){
		 //window.location.href="${contextPath}/admin/updatePartnerCategory?prtNo="+prtno;
			window.location.href="${contextPath}/admin/category";
		}
	});
});



$('#cancelCreatePartCat').click(function () {
	$("#partnerType").trigger("selectmenuchange", "refresh");
	$("#partnerType").selectmenu("refresh");
	$("#partnerType").val('');
	$("#partName").val('');

	 $("#partName").trigger("selectmenuchange", "refresh");
	$("#partName").selectmenu("refresh"); 
	
	$("#createCategoryName").val('');
	$("#createCategoryId").val('');
});


$('#cancelbtnD2Up_').click(function () {
	var prtno = $('#Partner_category').data('partno');
	$.get("${contextPath}/admin/updatePartnerCategoryCancel",function(data,status) {
		if(status=="success" ){
		 //window.location.href="${contextPath}/admin/updatePartnerCategory?prtNo="+prtno;
			window.location.href="${contextPath}/admin/category";
		}
	});
});




$.validator.addMethod('filterStartValueMin', function(value, element) {
	var isValid = true;
	var startValue = parseInt($("#edit_filterstartvalue").val(), 10);
	if(startValue <= 0){
		isValid = false;
	}
	return isValid;
});
$.validator.addMethod('filterStartValueMax', function(value, element) {
	var isValid = true;
	if($("#edit_filterendvalue").val() != ""){
		var startValue = parseInt($("#edit_filterstartvalue").val(), 10);
		var endValue = parseInt($("#edit_filterendvalue").val(), 10);
		
		if(startValue >= endValue){
			isValid = false;
		}
	}
	return isValid;
});

$.validator.addMethod('filterEndValueSize', function(value, element) {
	var isValid = true;
	if($("#edit_filterstartvalue").val() == ""){
		isValid = false;
	}else {
		var startValue = parseInt($("#edit_filterstartvalue").val(), 10);
		var endValue = parseInt($("#edit_filterendvalue").val(), 10);
		if(endValue <= startValue){
			isValid = false;
		}
	}	
	return isValid;
});
$.validator.addMethod('filterStepValueMin', function(value, element) {
	var isValid = true;
	var stepValue = parseInt($("#edit_filterstepvalue").val(), 10);
	if(stepValue <= 0){
		isValid = false;
	}
	return isValid;
});
	
$.validator.addMethod('filterStepValueMax', function(value, element) {
	var isValid = true;
	if($("#edit_filterendvalue").val() == ""){
		isValid = false;
	} else {
		var stepValue = parseInt($("#edit_filterstepvalue").val(), 10);
		var endValue = parseInt($("#edit_filterendvalue").val(), 10);
		if(stepValue >= endValue){
			isValid = false;
		}
	}
	return isValid;
});
</script>
<script type="text/javascript">
/* $(function () {
    $("#editCategoryImage").bind("change", function () {    	
    	//Get reference of FileUpload.
        var fileUpload = $("#editCategoryImage")[0];
        checkFileUpload(fileUpload);
        if(!uploadFlag) return false;
    });
}); */
function checkFileUpload(fileUpload){
	//Check whether the file is valid Image.
    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.png|.gif)$");
    if (regex.test(fileUpload.value.toLowerCase())) {
        //Check whether HTML5 is supported.
        if (typeof (fileUpload.files) != "undefined") {
            //Initiate the FileReader object.
            var reader = new FileReader();
            //Read the contents of Image File.
            reader.readAsDataURL(fileUpload.files[0]);
            
            reader.onload = function (e) {
                //Initiate the JavaScript Image object.
                var image = new Image();
                //Set the Base64 string return from FileReader as source.
                image.src = e.target.result;
                image.onload = function () {
                	uploadFlag = false;
                    //Determine the Height and Width.
                    var height = this.height;
                    var width = this.width;
                    if (height == 142 && width == 196) {
                        uploadFlag = true;
                    }
                    else{
                    	uploadFlag = false;
                    	alert("Height and width must be 142 and 196");
                    }
                    return uploadFlag;
                };
            }
        } else {
            alert("This browser does not support HTML5.");
            uploadFlag = false;
        }
    } else {
        alert("Please select a valid Image file.");
        uploadFlag = false;
    }
}
function getFormattedDate(date){
	var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
	return new Date(date.replace(pattern,'$3-$2-$1'));
}

$.validator.addMethod("greaterThanSubFilter", function(value, element) {
	var startValue=parseInt($("#subStartValueID").val());
	var endValue=parseInt($("#subEndValueID").val());
	var flag = true;
	if(startValue > endValue){
		flag = false;
	}
	return flag;
});

$.validator.addMethod("greaterThanSubEndValue", function(value, element) {
	var stepValue = parseInt($("#subStepValueID").val());
	var endValue  = parseInt($("#subEndValueID").val());
	
	var flag = true;
	if(parseInt(endValue) < parseInt(stepValue)){
		flag = false;
	}
	if(stepValue == '0') {
		$.validator.addMethod("subStepValueNotZero", function(value, element) {
			var stepValue = $("#subStepValueID").val();
			if(stepValue == '0'){
				return false;
			} else {
				return true;
			}
		});
	}
	return flag;
});

function setValidationRuleForSubCategoryDisplayOrder(mandatory){
	$('#gqcSubDisplayOrder').rules('add', {
		required: mandatory,
		digits: true,
		dupCategorySortOrder: true
	});
	
    if(!mandatory){
    	dupCategorySortOrder=false;
    	$("#categoryBeanId").validate().element("#gqcSubDisplayOrder");	
    }
}
var d = new Date(); // for now
var h = d.getHours();
h = (h < 10) ? ("0" + h) : h ;
var m = d.getMinutes();
m = (m < 10) ? ("0" + m) : m ;
var s = d.getSeconds();
s = (s < 10) ? ("0" + s) : s ;
var month = d.getMonth()+1;
var day = d.getDate();
var year = d.getFullYear();
datetext = day+"-"+month+"-"+year+"_"+h+":"+m+":"+s;





</script>