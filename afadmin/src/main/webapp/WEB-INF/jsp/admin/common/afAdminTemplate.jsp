<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="contextPath" value="${pageContext.servletContext.contextPath}"  scope="application"/>
<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="${contextPath}/static/img/favicon.ico" rel="shortcut icon" type="image/ico">
    <title>Affiliate</title>
    <!-- Custom CSS -->
	<link href="${contextPath}/static/css/main.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script>
		var ctx = "${pageContext.request.contextPath}";
    </script>
    <script src="${contextPath}/static/js/integrated.js"></script>
 	<script type="text/javascript" src="${contextPath}/static/js/jquery.validate.min.js"></script>
 	<script src="${contextPath}/static/js/additional-methods.min.js"></script>
 	<script src="${contextPath}/static/js/highcharts.js"></script>
    <script src="${contextPath}/static/js/exporting.js"></script>    
    <script src="${contextPath}/static/js/DnD/jquery.tablednd.js"></script>

 	
</head>
<body>
<div class='container'>
	<header>
		<tiles:insertAttribute name="header" />
	</header>
	<tiles:insertAttribute name="body" />
	<footer id="footer">
		<tiles:insertAttribute name="footer" />
	</footer>
</div>
 
 <script src='https://cdn.tinymce.com/4/tinymce.min.js'></script>
 <script>
	
 tinymce.init({
		selector: 'textarea#emailContent',
		 height : '200px',
		 setup : function(ed){
	         ed.on('NodeChange', function(e){
	             jQuery("textarea[id='emailContent']").val(ed.getContent());
	         });
	    }
	});
</script>
</body>
</html>
