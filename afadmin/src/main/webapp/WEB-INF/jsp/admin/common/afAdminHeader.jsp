<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="now" value="<%=new java.util.Date()%>" />
				<div class='row'>
                    <div class='col-lg-6'>
                        <a class='logo'></a>
<!--                         <a href='index.html' class='logo'></a> -->
                    </div>
                    <div class='col-lg-6 text-right'>
                        <ul class='list-inline'>
                            <li>
                                <p>Welcome <c:out value="${activeUser.firstName}"></c:out></p>
                                <span class='time'><fmt:formatDate type="both"   value="${now}" /></span>                                
                            </li>
                            <li>
                                <a class="btn btn-suc" type="button" href="${contextPath}/admin/logout"><img src="${contextPath}/static/img/logout_icon.png"></img>Logout</a>
                            </li>
                        </ul>
                    </div>
                </div><!----logo--->
                <%-- <div id="nav" class='row menu-wrap bg-b'>
                    <ul id="navigation" class="list-inline">
                        <c:forEach items="${menus}" var="menu">		
							<c:if test="${not subMenus.containsKey(menu.menuId)}">
								<li><a href='${contextPath}${menu.url}'>${menu.title}</a></li>
							</c:if>
							
							<c:if test="${subMenus.containsKey(menu.menuId)}">
								<li><a>${menu.title}</a>
								  <ul class="sub-menu">
									<c:forEach items="${subMenus[menu.menuId]}" var="subMenu">
									 	<li><a href="${contextPath}${subMenu.url}">${subMenu.title}</a></li>
									 </c:forEach> 
								 </ul>
							    </li>				
							</c:if>						
						</c:forEach>
						<li><a href="${contextPath}/admin/changePassword">Change Password</a></li>
                    </ul>
                </div> <!--- header ends here---> --%>
                <div id="nav" class='row menu-wrap bg-b'>
					<ul id="navigation" class="list-inline">
					 
						<c:forEach items="${menus}" var="menu">
							<c:if test="${not subMenus.containsKey(menu.menuId)}">
								<c:choose>
									<c:when test="${menuId==menu.menuId or parentMenuId==menu.menuId}">
										<li class='active'><a href='${contextPath}${menu.url}'>${menu.title}</a></li>
									</c:when>
									<c:otherwise>
										<li><a href='${contextPath}${menu.url}'>${menu.title}</a></li>
									</c:otherwise>
										
								</c:choose>
							</c:if>
							<c:if test="${subMenus.containsKey(menu.menuId)}">
							<c:choose>
									<c:when test="${menuId==menu.menuId or parentMenuId==menu.menuId}">
										<li class='active'><a>${menu.title}</a>
									</c:when>
									<c:otherwise>
										<li><a>${menu.title}</a>
										</c:otherwise>
								</c:choose>
								
								  <ul class="sub-menu">
									<c:forEach items="${subMenus[menu.menuId]}" var="subMenu">
										<c:choose>
											<c:when test="${menuId==subMenu.menuId and parentMenuId==subMenu.parentMenuId}">
										 		<li class="active"><a href="${contextPath}${subMenu.url}">${subMenu.title}</a></li>
										 	</c:when>
											 <c:otherwise>
											 <li><a href="${contextPath}${subMenu.url}">${subMenu.title}</a></li>
											 </c:otherwise>
										 </c:choose>
									 </c:forEach> 
								 </ul>
								
							</c:if>
							
							
						</c:forEach>
						<c:choose>
							<c:when test="${empty menuId}">
								<li class="active"><a href="${contextPath}/admin/changePassword">Change Password</a></li>
							</c:when>
							<c:otherwise>
								<li><a href="${contextPath}/admin/changePassword">Change Password</a></li>
							</c:otherwise>
						</c:choose>
					</ul>
				</div>
				<!--- header ends here--->