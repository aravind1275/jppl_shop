<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script>
	function changeURL() {
		history.pushState({}, '', "${contextPath}/admin/partnerOrderStatus");
	}
</script>

<!-- Modal  for edit-Notification-details-->
 <div id="myModal1" class="modal fade" role="dialog">
	<form:form autocomplete="off" id="editpartnerOrderStatusBean" commandName="editpartnerOrderStatusBean">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" id="cancelbtnD1_" class="close"
						data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Edit Partner Order Details</h4>
				</div>
				<!----modal-header--->
				<div class="modal-body1">
					<div class="row pad_top">
						<div class="col-md-6 user_details">
							<label>Partners</label><span class="mandatory_star">*</span>
							<select name="editPartNo" id="editPartNo" class="select_option">
								<option value="">--Select--</option>
								<c:forEach items="${partnerList}" var="partner">
									<li>
										<option value="${partner.prtNo}">${partner.partnerName}</option>
									</li>
								</c:forEach>
							</select>
						</div>

						<div class="col-md-4">
								<label for="country">Partners Status</label><span class="mandatory_star">*</span>
								 <select  name="editpartnerStatus" id="editpartnerStatus" class="select_option">
									<option value="">--Select--</option>
														
									<c:forEach items="${partnerStatusList}" var="status">
										<li>
										<option value="${status.partnerStatus}">${status.partnerStatus}</option>
										
												</li>
									</c:forEach>
								</select> 
							</div>
					</div>
					<!----row--->
					<div class="col-md-4">
								<label for="country">Partners Jppl Status </label>
								<span class="mandatory_star">*</span>
								<select name="editjpplStatus" id="editjpplStatus" class="select_option">
											<option value="">---Select---</option>
											<option value="tentative">Tentative</option>
  											<option value="processed">Processed</option>
  											<option value="disapproved">Disapproved</option>
  											<option value="failed">Failed</option>
 										</select>
							</div>
					

					
					

				  </div>
				<div class="row btn_save align_right">
					<input class="bg_g_button" type="button" value="Update" id="updatePartnerOrderStatus"/>
				</div>
				<!-------------button----------->
			</div>
		</div>
	</form:form>
</div> 

<div class='container'>
	<section class="insurance_management">
		<div id="breadcrumb">
			<ul>
				<li>Home</li>
				<li>Configuration</li>
				<li>Order Status</li>
			</ul>
		</div><!---breadcrumb--->
		
		<c:if test="${fn:length(message) gt 0}">
			<script>
				changeURL();
			</script>
			<div class="alertMessage" style="color: green; font-weight: bold;">
				<c:out value="${message}"></c:out>
			</div>
		</c:if>
		<c:if test="${fn:length(errormessage) gt 0}">
			<script>
				changeURL();
			</script>
			<div class="alertMessage" style="color: red; font-weight: bold;">
				<c:out value="${errormessage}"></c:out>
			</div>
		</c:if>
		
		<div class="section_heading">
			<h5>Partner Order Status</h5>
		</div>
		<div class="management">
			<!---User Management--->
			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#sectionA">Partner Order List</a></li>
				<li><a data-toggle="tab" href="#sectionB">Create Order Status </a></li>
			</ul>
		
		<div class="tab-content">
		<div id="sectionA" class="tab-pane fade in active">
					<!--- existing banner starts here --->
					<nav class="navbar navbar-default" role="navigation" id="submenu">
						
						<c:if test="${not empty partnerStatusList }">
							<div class="Static_table">
								<h5 class="table_heading">Partner Order Status List</h5>
								<!--- table header --->
								<table class=" partner_info_table table-bordered"
									cellspacing="0" cellpadding="0" width="98%" id="table-2">
									<thead>
										<tr>
											<th class="name">Sl No.</th>
											<th>Partner Name</th>
											<th>Partners Status</th>
											<th>Partners Jppl Status </th>
											<th class="width_section">Action</th>
										</tr>
									</thead>
									<!----thead--->
									<tbody>
										<c:forEach items="${partnerStatusListAll}" var="partner" varStatus="i">
											<tr class="admin" id="1.${partner.id}"
												data-id="${partner.id}"
												data-prtno="${partner.prtNo}"
												data-partnername="${partner.partnerName}"
												data-partnerstatus="${partner.partnerStatus}"
												data-jpplstatus="${partner.jpplStatus}">
												
												<td class="slno_width">${i.count}</td>
												<%-- <td >${partner.prtNo}</td> --%>
												<td >${partner.partnerName}</td>
												<td >${partner.partnerStatus}</td>
												<td >${partner.jpplStatus}</td>
												<td class="action_image">
													<span class="editPartnerOrderstatus" data-target="#myModal1"> <img src="${contextPath}/static/img/edit_icon.png"class="edit_wraper"></img>
												</span></td>
												
											</tr>
										</c:forEach>
									</tbody>
									<!---tbody--->
								</table>
								<!---table-->
								
								<!-------------button----------->
							</div>
						</c:if>
					</nav>
				</div>
				<!----section A---->
		<div id="sectionB" class="tab-pane fade submenu">
					<form:form autocomplete="off" id="partnerOrderStatusBean"
						commandName="partnerOrderStatusBean" enctype="multipart/form-data">
						<div class="row pad_top">

							<div class="col-md-4">
								<label>Partners</label><span class="mandatory_star">*</span>
								<form:select path="prtNo" name="prtNo" id="prtNo"
									class="select_option">
									<form:option value="">--Select--</form:option>
									<c:forEach items="${partnerList}" var="partner">
										<li><form:option value="${partner.prtNo}"
												label="${partner.partnerName}" /></li>
									</c:forEach>
								</form:select> 
							</div>
							
                            <div class="col-md-4">
								<label for="country">Partners Status</label><span class="mandatory_star">*</span>
								 <form:select path="partnerStatus" name="partnerStatus" id="partnerStatus"
									class="select_option">
									<form:option value="">--Select--</form:option>
									<c:forEach items="${partnerStatusList}" var="partner">
										<li><form:option value="${partner.partnerStatus}"
												label="${partner.partnerStatus}" /></li>
									</c:forEach>
								</form:select> 
							</div>
							
							<div class="col-md-4">
								<label for="country">Partners Jppl Status </label>
								<span class="mandatory_star">*</span>
								<form:select name="" id="jpplStatus" path="jpplStatus" class="select_option">
											<form:option value="">---Select---</form:option>
  											<form:option value="tentative">Tentative</form:option>
  											<form:option value="processed">Processed</form:option>
  											<form:option value="disapproved">Disapproved</form:option>
  											<form:option value="failed">Failed</form:option>

  											
 										</form:select>
							</div>
							
                        </div>
						
						<div class="row btn_save pull-right">
							<input class="bg_g_button" type="button" id="cancelbtnD"
								value="Cancel" />
							<button type="button" class="bg_g_button " onClick="$(this).closest('form').submit();  checkValidationMessage();" id="save">Save</button>
						</div>
						<!------ button ------>
					</form:form>				
					</div> <!----section A---->
					
					
				</div><!-- tab-content -->
				</div><!-- management -->
		</section>   <!--  section -->
		</div>  <!-- container -->
		
<script type="text/javascript">
var partnerOrderStatusExist = false;
$(document).ready(function() {
	$('.urlMand').hide();
	
	$("#partnerOrderStatusBean").validate({
		onkeyup : true,
	onkeyup : true,
	ignore : [],
	rules : {
		prtNo:{
			required: true,
			duplicate : true,
		},
		partnerStatus : {
			required : true,
			duplicate : true,
			
		},
		jpplStatus : {
			required : true,
			duplicate : true,
		}
		
	},
	messages : {
		prtNo:{
			 required:"Please Select Partner ",
			 duplicate : "Partner already exist",
		 },
		 partnerStatus : {
			required : "Please Select Partner Status ",
			duplicate : "Partner  Status already exist",
		},
		jpplStatus : {
			required : "Please Select Jppl  Status ",
			duplicate : "Partner jppl Status already exist",
			
		}
		
	},
	errorElement : "div",
	submitHandler : function(form) {
		form.submit();
	}
});
	
 	$("#editpartnerOrderStatusBean").validate({
		onkeyup : true,
	onkeyup : true,
	ignore : [],
	rules : {
		editPartNo:{
			required : true,
			duplicate : true,
		},
		editpartnerStatus : {
			required : true,
			duplicate : true,
			
		},
		editjpplStatus : {
			required : true,
			duplicate : true,
		}
		
	},
	messages : {
		editPartNo:{
			 required:"Please Select Partner ", 
			duplicate : " Partner already exist",
			
		 },
		 editpartnerStatus : {
			 required:"Please Select Partner Status ",
			 duplicate : "  Partner  Status already exist",
		},
		editjpplStatus : {
		    required:"Please Select Jppl  Status ",
			duplicate : " Partner jppl Status already exist",
			
		}
		
	},
	errorElement : "div",
	submitHandler : function(form) {
		form.submit();
	}
}); 
});


$('#prtNo').on('selectmenuchange', function() {
	$("#partnerOrderStatusBean").validate().element('#prtNo');
	if($('#prtNo').val() !=''){
		$('.urlMand').show();
		
	}else{
		$('.urlMand').hide();
	}
});

$('#partnerStatus').on('selectmenuchange', function() {
	$("#partnerOrderStatusBean").validate().element('#partnerStatus');
	if($('#partnerStatus').val() !=''){
		$('.urlMand').show();
		
	}else{
		$('.urlMand').hide();
	}
});

/* $('#editpartnerStatus').on('selectmenuchange', function() {
	$("#partnerOrderStatusBean").validate().element('#editpartnerStatus');
	if($('#editpartnerStatus').val() !=''){
		$('.urlMand').show();
		
	}else{
		$('.urlMand').hide();
	}
}); */

/* $('#editPartNo').on('selectmenuchange', function() {
	$("#partnerOrderStatusBean").validate().element('#editPartNo');
	if($('#editPartNo').val() !=''){
		//$('#edit_notificationlandingurl').rules('add', 'required');
	}else{
		//$('#edit_notificationlandingurl').rules('remove', 'required');
	}
}); */


$('#cancelbtnD').click(function() {
	$.get("${contextPath}/admin/addPartnerOrderStatusCancel", function(data, status) {
		if (status == "success") {
			window.location.replace("${contextPath}/" + data);
		}
	});
});

$('#cancelbtnD1_').click(function() {
	$.get("${contextPath}/admin/addPartnerOrderStatusCancel", function(data, status) {
		if (status == "success") {
			window.location.replace("${contextPath}/" + data);
		}
	});
});

$.validator.addMethod('duplicate', function(value, element) {
	if (partnerOrderStatusExist) {
		return false;
	} else {
		return true;
	}
});


$('#prtNo,#partnerStatus,#jpplStatus').on('selectmenuchange', function() {
			var prtNo = $('#prtNo').val();
			var partnerStatus = $('#partnerStatus').val();
			var jpplStatus = $('#jpplStatus').val();
			$.get(
					"${contextPath}/admin/checkPartnerOrderStatusExist?prtNo="+prtNo+"&partnerStatus="+partnerStatus+"&jpplStatus="+jpplStatus, 
							function(data, status) {
						if (status == "success" && data != '') {
							partnerOrderStatusExist = true;
						} else {
							partnerOrderStatusExist = false;
						}
						$("#partnerOrderStatusBean").validate().element('#prtNo');
						$("#partnerOrderStatusBean").validate().element('#partnerStatus');
						$("#partnerOrderStatusBean").validate().element('#jpplStatus');
					});
		});
		
		
$('#editPartNo,#editpartnerStatus,#editjpplStatus').on('selectmenuchange', function() {
		
			var prtNo = $('#editPartNo').val();
			var partnerStatus = $('#editpartnerStatus').val();
			var jpplStatus = $('#editjpplStatus').val();
			partnerOrderStatusExist = false;

		/* 	if (notificationId != notificationIdHidden) { */
				$.get("${contextPath}/admin/checkPartnerOrderStatusExist?prtNo="+prtNo+"&partnerStatus="+partnerStatus+"&jpplStatus="+jpplStatus,
						function(data, status) {
					if (status == "success" && data != '') {
						partnerOrderStatusExist = true;
					} else {
						partnerOrderStatusExist = false;
					}
					$("#editpartnerOrderStatusBean").validate().element('#editPartNo');
					$("#editpartnerOrderStatusBean").validate().element('#editpartnerStatus');
					$("#editpartnerOrderStatusBean").validate().element('#editjpplStatus');
				});
				
			
		});
		
		
$('#updatePartnerOrderStatus').click(function() {
	var id = $('#myModal1').data('id');
	var json = {};
	json["id"] = id;
	json["prtNo"] = $("#editPartNo").val();
	json["partnerStatus"] = $("#editpartnerStatus").val();
	json["jpplStatus"] = $("#editjpplStatus").val();
	
	var formData = new FormData();
	formData.append('json', JSON.stringify(json));
	if ( $("#editpartnerOrderStatusBean").validate().element('#editPartNo')
			&& $("#editpartnerOrderStatusBean").validate().element('#editpartnerStatus')
			&& $("#editpartnerOrderStatusBean").validate().element('#editjpplStatus')){
		
		$.ajax({
			url : "${contextPath}/admin/updatePartnerOrderStatus",
			data : formData,
			dataType : 'text',
			processData : false,
			contentType : false,
			type : 'POST',

			success : function(response) {
				$('#myModal1').modal('hide');
				window.location.href = "${contextPath}/admin/orderStatus?updateMessage=" + response;
			}
		});
	}
});
	


$('span.editPartnerOrderstatus').on('click', function() {
	var id = $(this).closest('tr').data('id');
	var prtno = $(this).closest('tr').data('prtno');
	var partnerstatus = $(this).closest('tr').data('partnerstatus');
	var jpplstatus = $(this).closest('tr').data('jpplstatus');
	
	$("#editPartNo").val(prtno).selectmenu("refresh");
	$('#editpartnerStatus').val(partnerstatus).selectmenu("refresh");
	$('#editjpplStatus').val(jpplstatus).selectmenu("refresh");
	
	$('#myModal1').data('id', id).modal('show');
});
		
		
$('.partner_info_table').DataTable( {
    "paging": true,
    "searching": false,
    "lengthChange": false,
    "info":false,
    "iDisplayLength" : 25,
    "order": [[ 0, "asc" ]],
    columnDefs: [
         {type: 'non-empty-string'} // define 'name' column as non-empty-string type
    ],
});

</script>
		
		
		
		
		
		
		
		