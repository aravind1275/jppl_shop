<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<section class="insurance_management">
	<div id="breadcrumb">
		<ul>
			<li>Home</li>
			<li>User Management</li>
			<li>Add User</li>
		</ul>
	</div>
	<!---breadcrumb--->
	
	   <c:if test="${fn:length(message) gt 0}">
			<script>
				changeURL();
			</script>
			<div class="alertMessage" style="color: green; font-weight: bold;">
				<c:out value="${message}"></c:out>
			</div>
		</c:if>
		<c:if test="${fn:length(errormessage) gt 0}">
			<script>
				changeURL();
			</script>
			<div class="alertMessage" style="color: red; font-weight: bold;">
				<c:out value="${errormessage}"></c:out>
			</div>
		</c:if>
	<div class="section_heading">
		<h5>Add New User</h5>
	</div>
	<form:form autocomplete="off" id="adminUserBean" commandName="adminUserBean">
		<div class="main_section bg-b-w">
			<div class="row pad_top">
				<div class="col-md-4">
					<label>First Name</label><span class="mandatory_star">*</span>
					<form:input minlength="3" maxlength="100" path="firstName" class="form-control" />
				</div>
				<div class="col-md-4">
					<label>Last Name</label><span class="mandatory_star">*</span>
					<form:input  minlength="3" maxlength="100" type='text' path="lastName" class="form-control" />
				</div>
				<div class="col-md-4">
					<label>Mobile Number</label><span class="mandatory_star">*</span>
					<form:input type='text' maxlength="10" path="mobileNumber" class="form-control" />
				</div>
			</div>
			<!-------------row----------->
			<div class="row pad_top">
				<div class="col-md-4">
					<label>Email ID (@jetprivilege only)</label><span
						class="mandatory_star">*</span>
					<form:input type='text' maxlength="100" id="userName" path="userName" class="form-control" />
				</div>
				<div class="col-md-4">
					<label>Password</label><span class="mandatory_star">*</span>
					<form:password id="password" path="password" maxlength="100" class="form-control" maximum="100" />
				</div>
				<div class="col-md-4">
					<label>Assign Role</label><span class="mandatory_star">*</span>
					<fieldset class="infield">
						
									<form:select id="userRole" name="userRole" path="userRole"
										class="select_option">
										<form:option value="-1">Select role</form:option>
										<c:forEach items="${roles}" var="role">
											<form:option value="${role.roleId}">${role.roleName}</form:option>
										</c:forEach>
									</form:select>
													</fieldset>
				</div>
			</div>
			<!-------------row----------->
			<div class="row pad_top">
				<div class="col-md-4">
					<label>Status</label><span class="mandatory_star">*</span>
					<fieldset class="infield list-inline">
					
					<c:forEach items="${statusValues}" var="status">
							<li>
								<label>
									<form:radiobutton path="statusType"	class="css-checkbox" id="${status.key}" value="${status.key}" />
									<label for="${status.key}" class="css-label radGroup2">${status.value}</label>
								</label>
							</li>
					</c:forEach>
						
						
									
					</fieldset>
				</div>
				<!--- status wrap -->
			</div>
			<!-------------row----------->
		</div>
		<div class="btn_save pull-right">
			<button type="button" class="bg_g_button" onClick="window.history.back()">Cancel</button>
			<button type="button" class="bg_g_button" onClick="$(this).closest('form').submit();">Save</button>
		</div>
	</form:form>
	<!-------------button----------->
</section>

<script type="text/javascript">
$(function() {
    $("#userName").on("focus", function() {
        var dest = $(this);
        dest.val(dest.val().split(" ").join("")); 
    });
});

var userNameExist=false;

$(document).ready(function () {
	
	clearFields();

	
	    $.validator.addMethod("AlphabetsOnly", function(value, element) {
			return this.optional(element) || /^[a-z][a-z@\s]*$/i.test(value);
		});
	    
	    $.validator.addMethod('duplicateCheck', function(value, element) {
	        if(userNameExist){
	           return false;
	        }else{
	         return true;
	        }
		
		});
	    $.validator.addMethod("dropdowncheck", function(value, element) {
			if(value==-1)
			return false;
			else
			return true;
		});

		$.validator.addMethod('passwordChecking', function (value,element) { 
			return this.optional(element) || /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#%^&*()_+])[A-Za-z\d][A-Za-z\d!@#%^&*()_+]{7,14}$/.test(value);	
		});
		
		$.validator.addMethod("notEqual", function(value, element) { 
			return $('#firstName').val() != $('#lastName').val() });

		$.validator.addMethod("requireEmail", function(value, element) {
		    var re = /^[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
		    if (re.test(value)) {
		        if (value.indexOf("@jetprivilege.com", value.length - "@jetprivilege.com".length) !== -1) {
		            return true;
		        } else {
		            return false;
		        }
		    } else {
		        return false;
		    }
		});
});
		
		 $("#adminUserBean").validate({

		        ignore:[],

				rules: {
					firstName:{
						required: true,
						AlphabetsOnly:true,
						rangelength:[1,100],
		            },
					lastName:{
						required: true,
						AlphabetsOnly:true,
						notEqual : true,
						rangelength:[1,100]
					},
		 			mobileNumber:{
						required: true,
						digits:true,
						rangelength:[10,10]
					},
					userName:{
						required: true,
						duplicateCheck:true,
						rangelength:[1,100],
						requireEmail:true
					},
					password:{
						required: true,
						passwordChecking:true
					},
					cfmpassword:{
						required: true,
						equalTo: "#password"
					},
					
					userRole:{
						dropdowncheck: true,
					},
					statusType:{
						dropdowncheck: true,
					}
		     },

		     messages: {
				
		        firstName:{
					required:"Please enter First Name",
					minlength:"Please enter atleast 3 characters",
					AlphabetsOnly:"Alphabets  only"
				},
				lastName:{
					required: "Please enter Last Name",
					AlphabetsOnly:"Alphabets  only",
					notEqual:"Last name should not be same as first name"
				},
				userName:{
					required: "Please enter User Name/Email ID",
					duplicateCheck:"Username already exists",
					requireEmail:"Only jetprivilege ID allowed (No blank spaces or special characters allowed)"
				},
		 		password:{
					required: "Please enter password",
					passwordChecking : 'Minimum 8 and Maximum 15 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character(except space, dot, comma, hyphen and dollar)'
			    },
			    cfmpassword:{
			    	required: "Please confirm password",
			    	equalTo: "Passwords do not match"
				},
		 		mobileNumber:{
					required: "Please enter Mobile Number",
					digits:"Please enter numbers only",
					rangelength:"Please enter 10-digit mobile number"
				},
		 		userRole:{
		 			dropdowncheck: "Please select a Role"
				},
				statusType:{
		 			dropdowncheck: "Please select a Status"
				}
		     },
		     errorElement: "div",
		     submitHandler: function (form) {
				form.submit();
			},
		 });
		 
		 $("#userName").change(function(){
				var userName1=$('#userName').val();
				$.get("${contextPath}/admin/checkUserName?userName="+userName1, function(data,status) {
					if(status=="success" && data!=''){
						userNameExist=true;	
					}
					else{
						userNameExist=false;
						}
					$("#adminUserBean").validate().element('#userName');
					
				});

			});
	 
	 $("#userRole").on('selectmenuchange',function() {
		 $("#adminUserBean").validate().element("#userRole");
	 });
		 
		 $(document).on("keydown", function (e) {
			    if (e.which === 8 && !$(e.target).is("input, textarea")) {
			        e.preventDefault();
			    }
			});
	
		 function clearFields(){
			 $("#userName").val(" ");
			 $("#password").val("");
		 }

			

</script>

