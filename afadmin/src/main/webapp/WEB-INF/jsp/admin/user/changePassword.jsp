<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class='container'>
            
            <section class="affliates_management">
                <div id ="breadcrumb">
                    <ul>
                        <li>Home</li>
                        <li>Change Password</li>
                    </ul>
                </div>  <!---breadcrumb--->
				<div class="section_heading">
					<h5>Change Password</h5> 
				</div>
				<form:form  autocomplete="off" id="changePassword" commandName="changePasswordBean" class="form-signin">
			    <input type="hidden" name="userId" value="${adminUserBean.userId}"/>
			    <input type="hidden" id="password" name="password" value="${adminUserBean.password}" />
				<div class="main_section bg-b-w">
					<div class="row pad_top">
                        <div class="col-md-4">
							<label>Type the existing password</label><span class="mandatory_star">*</span>
							<!-- <input type="text" class="form-control" /> -->
							<form:password id="existingPassword" path="existingPassword" class="form-control" maximum="15"/>
                        </div>
                        <div class="col-md-4">
							<label>Enter the new password</label><span class="mandatory_star">*</span>
							<!-- <input type="text" class="form-control" /> -->
							<form:password path="newPassword" maxlength="100" class="form-control" maximum="15" />
                        </div>
						<div class="col-md-4">
							<label>Re-enter the new password</label><span class="mandatory_star">*</span>
							<!-- <input type="text" class="form-control" /> -->
							<form:password path="reenterNewPassword" maxlength="100" class="form-control" maximum="15"/>
                        </div>
                    </div> <!--- row --->
				</div><!--- main section --->
				<div class="row btn_save pull-right">
					<%-- <a class="bg_g_button" href="${contextPath}/admin/changePassword" >Cancel</a> --%>
					<button class="bg_g_button" onClick="location.href='${contextPath}/admin/changePassword';" >Cancel</button>
		            <button class="bg_g_button" onClick="$(this).closest('form').submit();" >Save</button>
				</div><!----- button ----->
				</form:form>
            </section>
            
</div>
<c:if test="${fn:length(successMessage) gt 0}">
		<div style="color: green; font-weight: bold;">
			<c:out value="${successMessage}"></c:out>
		</div>
	</c:if>
<script>
    $(window).resize(function(){
    var height = $(this).height() - $(".container header").height() + $("#footer").height()
    $('.container section').height(height);
     });

     $(window).resize(); 
</script>

<script type="text/javascript">
$(document).ready(function () {
	var notEqual=false;
	$("#existingPassword").keyup(function(){
		if($('#existingPassword').val()!=$('#password').val())notEqual=true;
		else notEqual=false;
	});

	$("#newPassword").keyup(function(){
		if($('#newPassword').val()==$('#existingPassword').val())notEqual=true;
		else notEqual=false;
	});
	
	$.validator.addMethod( 'notEqual', function(value, element) {
        if(notEqual)return false;
        else return true;
	});

	$.validator.addMethod('passwordChecking', function (value,element) { 
		return this.optional(element) || /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+])[A-Za-z\d][A-Za-z\d!@#$%^&*()_+]{7,14}$/.test(value);	
	});
	
	/* document.getElementById("existingPassword").onchange = function () {
		//	document.getElementById("uploadFile").value = str;
			$("#changePassword").validate().element("#existingPassword");
	}; */
	 /* for jquery validation*/
	 $("#changePassword").validate({
		 rules: {
			 existingPassword:{
				 required: true,
				 notEqual:"#existingPassword",
				// passwordChecking:true
				 },
			 newPassword: {
				 required :true,
				 notEqual:"#existingPassword",
				 passwordChecking:true
				 },
			 reenterNewPassword:{
					required: true,
					equalTo: "#newPassword",
					passwordChecking:true
				}
	        },
	        messages: {
	        	existingPassword:{
	        		required:"Please enter Existing Password",
	        		notEqual:"Entered Password is not matching with existing",
	        		passwordChecking : 'Minimum 8 and Maximum 15 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character:'
		        	} ,
	        	newPassword :{
		        	required: "Please enter new password",
		        	notEqual:"New Password cannot be same as existing",
		        	passwordChecking : 'Minimum 8 and Maximum 15 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character:'
		        	},
	        	reenterNewPassword:{
			    	required: "Please Re enter password",
			    	equalTo: "Passwords do not match",
			    	passwordChecking : 'Minimum 8 and Maximum 15 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character:'
		        }
	        },
	        errorElement: "div",
	        submitHandler: function (form) {
	               form.submit();
	        }
		 
	 });

	 /* for submit the form on key press enter*/
	 $(document).keypress(function(event){
		    if(event.keyCode == 13){
			    $("#changePassword").submit();
		    }
		});
	
});
$(document).on("keydown", function (e) {
    if (e.which === 8 && !$(e.target).is("input, textarea")) {
        e.preventDefault();
    }
});

</script>
