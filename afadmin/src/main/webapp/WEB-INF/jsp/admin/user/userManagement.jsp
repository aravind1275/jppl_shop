<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<script>
	function changeURL() {
		history.pushState({}, '', "${contextPath}/admin/userManagement");
	}
</script>
<!-- Modal -->
		<div id="myModala" class="modal fade" role="dialog">	
			<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Are You Sure?</h4>
				</div><!--header-->
				<div class="modal-body">
					<p>
										<img src="${contextPath}/static/img/info_icon1.png"></img>
										<span id="deleteUserId">will be deleted.Are you sure?</span> 
									</p>
				</div><!---body--->
				<div class="modal-footer">
					<button id="btnDelteYes" class="btn btn-grey bgp" data-dismiss="modal">Delete</button>
				</div>
			</div><!---modal content--->
			</div>
		</div>	
		<!-- Modal  for edit-details-->
        <div id="myModal1" class="modal fade" role="dialog">
        <form:form autocomplete="off"  id="editUserBean" 	commandName="editUserBean">
         <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" 
                        aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Edit User Details</h4>
                    </div><!----modal-header--->
                    <div class="modal-body1">
                        <div class="row" >
                          <div class="col-md-6 ">
											<label>First Name</label> <input type="text" maxlength="100"
												class="form-control " name="edit_firstname" id="edit_firstname" >
										</div>
										<div class="col-md-6">
											<label>Last Name</label> <input type="text"  maxlength="100"
												class="form-control " name="edit_lastname" id="edit_lastname">
										</div>
                        </div><!----row--->
                        <div class="row">
                            <div class="col-md-6">
											<label>Email</label> <input type="text" class="form-control " maxlength="100"
												readonly="true" name="edit_email" id="edit_email">
										</div>
										<div class="col-md-6">
											<label>Mobile</label> <input type="text" maxlength="10"
												class="form-control " name="edit_mobile" id="edit_mobile">
										</div>
                        </div><!----row--->
                        <div class="row " id="text1">
                            <div class="col-md-6 user_role">
                                <label>User Role</label>
                                <fieldset>
                                   <select id="edit_role" name="edit_role" class="select_option">
													<c:forEach items="${roles}" var="role">
														<option value="${role.roleId}">${role.roleName}</option>
													</c:forEach>
												</select>
                                </fieldset>
                            </div>
                            <div class='col-md-6 text top' >
                                <label>Status</label>
                                <div class="half-col last">
                                    <ul class="list-inline">
                                       <c:forEach items="${statusValues}" var="status">
														<li><label> 
														<input type="radio"	name="edit_status" class="css-checkbox"
																id="edit_${status.key}" value="${status.key}"  />
														<label for="edit_${status.key}" class="css-label radGroup2">${status.value}</label>
														<%-- 	</c:if>   --%>
																
														</label></li>
													</c:forEach>
                                    </ul>
                                </div>
                            </div>
                        </div><!----row--->
                        <div class="row btn_save">
                            <a href='javascript:void(0);' type="button" id="updateUser" class="bg_g_button" >Update</a>
                        </div>		<!----row--->
                    </div>
                </div>
            </div>
            </form:form>
        </div>
        <!-- Modal content for view details----------------------------------->
        <div id="myModal2" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content for view details-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">User Details</h4>
                    </div><!----modal---header--->
                    <div class="modal-body2">
                        <div class="row">
                            <div class='col-md-6 user_details' >
                                <label>First Name</label>
                              	<p id="firstname"></p>
                            </div>
                            <div class='col-md-6 user_details ' >
                                <label>Last Name</label>
                                <p id="lastname"></p>
                            </div>
                        </div><!----row--->
                        <div class="row">
                            <div class='col-md-6 user_details' >
                                <label>Email</label>
                               <p id="email"></p>
                            </div>
                            <div class=' col-md-6 user_details ' >
                                <label>Mobile</label>
                               <p id="mobile"></p>
                            </div>
                        </div><!----row--->	
                        <div class="row">
                            <div class='user_details' >
                                <label>User Role</label>
                              <p id="role"></p>
                            </div>
                        </div><!----row--->
                    </div><!----modal-body---->
                </div>
            </div>
        </div>					
        
        <section class="insurance_management">
                <div id ="breadcrumb">
                    <ul>
                        <li> Home</li>
                        <li>Management</li>
						<li>User Management</li>
                    </ul>
                </div>  <!---breadcrumb--->
                
				<div class="section_heading">
				    <c:if test="${fn:length(message) gt 0}">
				    <script>
				changeURL();
			     </script><div class="alertMessage" style="color:green;font-weight: bold;"  ><c:out value="${message}"></c:out></div> </c:if>
	 				 <c:if test="${fn:length(errormessage) gt 0}">
	 				 <script>
				changeURL();
			</script><div class="alertMessage" style="color:red;font-weight: bold;"  ><c:out value="${errormessage}"></c:out></div> </c:if>
					<span>User Management</span> 
					<a class="bg_w_button"  href="${contextPath}/admin/createUser" >Add New User</a>
				</div>
                <div class="insurance_policy_table user_management mar_bottom">
                    <table class=" table table-bordered" cellspacing="0" cellpadding="0" width="100%">
						<thead>
							<tr>
								<th class="name">User Name</th>
								<th>Role</th>
								<th>Status</th>
								<th>Last Login</th>
								<th class="width_section">Action</th>
							</tr>
						
						</thead><!----thead--->
						<tbody>
						<c:forEach items="${userList}" var="user">
							<tr class="" data-id="${user.userId}"
							data-firstname="${user.firstName}"
									data-lastname="${user.lastName}" data-email="${user.userName}"
									data-mobile="${user.mobileNumber}" data-role="${user.role}"
									data-roleid="${user.roleId}"
									data-status="${user.userStatusCode}">
								<td class="email">	<a href="javascript:void(0);">${user.userName}</a></td>
								<td>${user.role}</td>
								<td class="${user.userStatusClass}">${user.userStatus}</td>
								<td>${user.lastLogin}</td>
								<td>
									<span class="view_details"  data-target="#myModal2">
									<img src="${contextPath}/static/img/View_details_icon1.png"></img></span>
									
									<span class="edituser" data-toggle="modal" data-target="#myModal1">
										<img src="${contextPath}/static/img/edit_icon.png" class="edit_wraper" ></img>
									</span>
									<c:if test="${user.userId!=1 && user.roleId!=1}">
									<span class="delete_icon" id="deleteUser" data-target="#myModala"> 
										<img src="${contextPath}/static/img/delete_icon.png" class="fa fa-lg"></img>
									</span>
									</c:if>
								</td>
							</tr> 
						</c:forEach>
							
						</tbody><!---tbody--->
					</table><!---table-->	       
                </div>
            </section>
            
            			
<script type="text/javascript">

var userNameExist=false;
var id;

$('span.view_details').on('click', function () {
	
	var id = $(this).closest('tr').data('id');
	var firstname= $(this).closest('tr').data('firstname');
	var lastname= $(this).closest('tr').data('lastname');
	var email= $(this).closest('tr').data('email');
	var mobile= $(this).closest('tr').data('mobile');
	var role= $(this).closest('tr').data('role');
	$('#firstname').text(firstname);
	$('#lastname').text(lastname);
	$('#email').text(email);
	$('#mobile').text(mobile);
	$('#role').text(role);
	$('#myModal2').data('id', id).modal('show');
	
});

$('span.edituser').on('click', function () {
	
	id = $(this).closest('tr').data('id');
	var firstname= $(this).closest('tr').data('firstname');
	var lastname= $(this).closest('tr').data('lastname');
	var email= $(this).closest('tr').data('email');
	var mobile= $(this).closest('tr').data('mobile');
	var role= $(this).closest('tr').data('role');
	var roleId= $(this).closest('tr').data('roleid');
	var userStatus=$(this).closest('tr').data('status');
	$('#edit_firstname').val(firstname);
	$('#edit_lastname').val(lastname);
	$('#edit_email').val(email);
	$('#edit_mobile').val(mobile);
	$('#edit_role').val(roleId);
	$('#edit_role').selectmenu("refresh");
	$('#edit_'+userStatus).attr("checked","checked");
	//$('#edit_role option[value="' + roleId + '"]').prop('selected', true);
	//$('#myModal1').data('id', id).modal('show');
	
});

$('#updateUser').click(function () {
	
	/* vid = $('#myModal1').data('id');
	alert($('#myModal1').data('id')); */
	var json={};
	json["userId"]=id;
	json["firstName"]=$("#edit_firstname").val();
	json["lastName"]=$("#edit_lastname").val();
	json["emailId"]=$("#edit_email").val();
	json["mobileNumber"]=$("#edit_mobile").val();
	json["roleId"]=$("#edit_role").val();
	json["statusType"]=$('input[name=edit_status]:checked').val();
	var formData = new FormData();
	formData.append('json',JSON.stringify(json));
	if($("#editUserBean").validate().element('#edit_firstname') && $("#editUserBean").validate().element('#edit_lastname')
			 && $("#editUserBean").validate().element('#edit_email')&& $("#editUserBean").validate().element('#edit_mobile')){
		$.ajax({
			  url: "${contextPath}/admin/updateUser",
			  data: formData,
			  dataType: 'text',
			  processData: false,
			  contentType: false,
			  type: 'POST',
			  success: function (response) {
				
				  $('#myModal1').modal('hide');
				  window.location.href = "${contextPath}/admin/userManagement?updateMessage=" +response;
					/* window.location.reload(); */
			  }
			 });
	}
	
});

$('span#deleteUser').on('click', function () {
	var id = $(this).closest('tr').data('id');
	var email= $(this).closest('tr').data('email');
	$('#deleteUserId').text(email+" "+ "will be deleted. Do you want to continue?");
	$('#myModala').data('id', id).modal('show');
});
$('#btnDelteYes').click(function () {
	var id = $('#myModala').data('id');
	$.get("${contextPath}/admin/deleteUser?userId="+id , function(data,status) {
		if(status=="success" ){
			$('[data-id=' + id + ']').remove();
			$('#myModala').modal('hide');
		}
	});
});

$(document).ready(function () {
	
	    $.validator.addMethod("AlphabetsOnly", function(value, element) {
			return this.optional(element) || /^[a-z][a-z@\s]*$/i.test(value);
		});
	    
	    $.validator.addMethod("notEqual", function(value, element) { 
			return $('#edit_firstname').val() != $('#edit_lastname').val() });

});    
		    $("#editUserBean").validate({

		        ignore:[],

				rules: {
					edit_firstname:{
						required: true,
						AlphabetsOnly:true,
						rangelength:[1,100]
		            },
					edit_lastname:{
						required: true,
						notEqual : true,
						AlphabetsOnly:true
					},
					
		 			edit_mobile:{
						required: true,
						digits:true,
						rangelength:[10,10]
					}
		     },

		     messages: {
				
		    	 edit_firstname:{
					required:"Please enter First Name",
					minlength:"Please enter atleast 3 characters ",
					AlphabetsOnly:"Alphabets  only"
				},
				edit_lastname:{
					required: "Please enter Last Name",
					notEqual:"Last name should not be same as first name",
					AlphabetsOnly:"Alphabets  only"
				},
		
				edit_mobile:{
					required: "Please enter Mobile Number",
					digits:"Please enter numbers only",
					rangelength:"Please enter 10-digit mobile number"
				}
		     },
		     errorElement: "div",
		     submitHandler: function (form) {
				form.submit();
			},


			});
	

$(document).on("keydown", function (e) {
    if (e.which === 8 && !$(e.target).is("input, textarea")) {
        e.preventDefault();
    }
});


</script>

					