<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class='container'>
	<section class="affliates_management">
		<div id ="breadcrumb">
			<ul>
				<li>Home</li>
				<li>Content Management</li>
				<li>SEO</li>
			</ul>
		</div>  <!---breadcrumb--->
		
		<div class="section_heading">
			<c:if test="${fn:length(message) gt 0}"><div class="alertMessage" style="color:green;font-weight: bold;"  ><c:out value="${message}"></c:out></div> </c:if>
			<c:if test="${fn:length(errorMessage) gt 0}"><div class="alertMessage" style="color:red;font-weight: bold;"><c:out value="${errorMessage}"></c:out></div> </c:if>
			<h5>Search Engine Optimization</h5> 
		</div><!-- TITLE -->
		
		<form:form autocomplete="off" name="seoBean" id="seoBean" commandName="seoBean" >
			<div class="row pad_top">
				<div class="col-md-4">
					<label for="country">Select Page</label><span class="mandatory_star">*</span>
					<form:select path="seoPageId" name="seoPageId" id="seoPageId" class="select_option">
						<form:option value="" data-headertext="" data-title="" data-metadesc="" data-metatag="" >--Select Page--</form:option>
						<c:forEach items="${seoPageId}" var="pageId">
							<c:set var="hdrTxt" value=""/>
							<c:set var="ttl" value=""/>
							<c:set var="mDesc" value=""/>
							<c:set var="mTag" value=""/>
							
							<c:forEach items="${seoData}" var="sd">
								<c:if test="${pageId.key eq sd.seoPageId}">
									<c:set var="hdrTxt" value="${sd.headerText}"/>
									<c:set var="ttl" value="${sd.title}"/>
									<c:set var="mDesc" value="${sd.metaDesc}"/>
									<c:set var="mTag" value="${sd.metaTag}"/>
								</c:if>
							</c:forEach>
							
							<c:choose>								
								<c:when test="${selectedPage eq pageId.key}">
									<li><form:option selected="true" value="${pageId.key}" label="${pageId.value}" data-headertext="${hdrTxt}" data-title="${ttl}" data-metadesc="${mDesc}" data-metatag="${mTag}" /></li>										
								</c:when>
								<c:otherwise>
									<li><form:option value="${pageId.key}" label="${pageId.value}" data-headertext="${hdrTxt}" data-title="${ttl}" data-metadesc="${mDesc}" data-metatag="${mTag}" /></li>
								</c:otherwise>
							</c:choose> 
						</c:forEach>
					</form:select>
				</div>
			</div>	<!-- row -->
			
			<div class="row pad_top">
				<div class="col-md-6">
					<label>Recommended Title</label><span style="padding-left: 10px;"><img class="seo_edit" src="${contextPath}/static/img/edit_icon_new.png"></span>
					<form:input path="title" name="title" id="title" class="form-control" readonly="true" />
				</div>
			</div>	<!-- row -->
			
			<div class="row pad_top">
				<div class="col-md-6">
					<label>Recommended Meta Description</label><span style="padding-left: 10px;"><img class="seo_edit" src="${contextPath}/static/img/edit_icon_new.png"></span>
					<form:textarea path="metaDesc" name="metaDesc" id="metaDesc" class="form-control" readonly="true" />
				</div>
			</div>	<!-- row -->
			
			<div class="row pad_top">
				<div class="col-md-6">
					<label>Recommended Meta Tags</label><span style="padding-left: 10px;"><img class="seo_edit" src="${contextPath}/static/img/edit_icon_new.png"></span>
					<form:textarea path="metaTag" name="metaTag" id="metaTag" class="form-control" readonly="true" />
				</div>
			</div>	<!-- row -->
			
			<div class="row pad_top">
				<div class="col-md-6">
					<label>Recommended H1 Tags</label><span style="padding-left: 10px;"><img class="seo_edit" src="${contextPath}/static/img/edit_icon_new.png"></span>
					<form:textarea maxlength="250" path="headerText" name="headerText" id="headerText" class="form-control" readonly="true" />
				</div>
			</div>	<!-- row -->
			
			<div class="row btn_save pull-right">
				<input class="bg_g_button" type="button" id="cancelbtnD" value="Cancel" />
				<button type="submit" class="bg_g_button">Save</button>
			</div>
		</form:form>
	</section>
</div>





<!-- ********************JS Start******************** -->

<script type="text/javascript">

var pageId;
var headerText;
var title;
var metaDesc;
var metaTags;

$(document).ready(function () {
	setSeoData();
	
	$("#seoBean").validate({
		onkeyup: true,
		ignore:[],
		rules: {	
			seoPageId:{
				required: true
			}
		},
		messages: {
			seoPageId:{
				required: "Please select a page"
			}
		},
		errorElement: "div",
		errorPlacement: function(error, element) {
			error.insertAfter(element);
		},
		submitHandler: function (form) {
			form.submit();
		}
	});
});

$("#cancelbtnD").on('click', function(){
	$('#headerText').val(headerText).prop("readonly",true);
	$("#title").val(title).prop("readonly",true);
	$("#metaDesc").val(metaDesc).prop("readonly",true);
	$("#metaTag").val(metaTags).prop("readonly",true);
	
	$('#seoPageId').val(pageId);
	$('#seoPageId').selectmenu("refresh");
});

$(".seo_edit").on('click', function(){
	$(this).parent().next('.form-control').removeAttr("readonly");
});

$('#seoPageId').on('selectmenuchange', function() {
	setSeoData();
	$("#seoBean").validate().element("#seoPageId");
});

function setSeoData(){
	pageId = $('#seoPageId').val();
	headerText = $('#seoPageId :selected').data('headertext');
	title = $('#seoPageId :selected').data('title');
	metaDesc = $('#seoPageId :selected').data('metadesc');
	metaTags = $('#seoPageId :selected').data('metatag');
	
	$('#headerText').val(headerText);
	$('#title').val(title);
	$('#metaDesc').val(metaDesc);
	$('#metaTag').val(metaTags);
}

</script>