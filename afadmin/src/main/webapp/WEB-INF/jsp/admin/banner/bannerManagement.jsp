<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script>
	function changeURL() {
		history.pushState({}, '', "${contextPath}/admin/bannerManagement");
	}
</script>

<!-- Modal content for delete -->
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title">Are You Sure?</h4>
			</div>
			<!--header-->
			<div class="modal-body">
				<p>
					<img src="${contextPath}/static/img/info_icon1.png"></img> <span
						id="deleteBannerId" class="bannerNameClass"> banner will be
						deleted.Are you sure?</span>
				</p>
			</div>
			<!---body--->
			<div class="modal-footer">
				<button id="btnD" class="bg_g_button" data-dismiss="modal">Delete</button>
			</div>
		</div>
		<!---modal content--->
	</div>
</div>



<!-- Modal content for image details----------------------------------->
<div id="myModal1_" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content for view details-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title">Image Details</h4>
			</div>
			<!----modal---header--->
			<div class="modal-body2">

				<!----row--->
				<div class="row">
					<div class='col-md-6 user_details'>
						<label>Banner Image</label> <img src='' id="bannerimage_"
							
							height="300px" width="448px" />
						<p id="bannerimageurl"></p>
					</div>
				</div>
			</div>
			<!----modal-body---->
		</div>
	</div>
</div>

<!-- Modal  for edit-banner-details-->
<div id="myModal1" class="modal fade" role="dialog">
	<form:form autocomplete="off" id="editBannerBean" commandName="editBannerBean">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" id="cancelbtnD1_" class="close"
						data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Edit Banner Details</h4>
				</div>
				<!----modal-header--->
				<div class="modal-body1">
					<div class="row pad_top">
						<div class="col-md-6 user_details">
							<label>Partners</label>
							<select name="editPartNo" id="editPartNo" class="select_option" disabled="disabled" >
								<option value="">--Select--</option>
								<c:forEach items="${partnerList}" var="partner">
									<li>
										<option value="${partner.prtNo}">${partner.partnerName}</option>
									</li>
								</c:forEach>
							</select>
						</div>

						<div class="col-md-6 user_details">
							<label>Banner ID</label>
							<input type="text" class="form-control " name="edit_bannerid" maxlength="50" id="edit_bannerid">
							<input type="hidden" id="edit_banneridHidden" />
						</div>
					</div>
					<!----row--->
					<div class="row pad_top">
						<div class="col-md-6 user_details">
							<label>Banner Name</label>
							<input type="text" class="form-control " name="edit_bannername" maxlength="100" id="edit_bannername">
						</div>

						<div class="col-md-6 user_details">
							<label>Landing URL</label>
							<input type="text" class="form-control " name="edit_bannerlandingurl" id="edit_bannerlandingurl">
						</div>
					</div>
					<!----row--->

					<div class="row pad_top" style="display: none">
						<div class="col-md-6 user_details">
							<label>Banner Sections</label>
							<div class="half-col last status">
								<ul class="list-inline">
									<c:forEach items="${bannerSections}" var="status">
										<li><label> <input type="radio"
												name="edit_bannersections" class="css-checkbox"
												id="edit_bannersections_${status.key}" value="${status.key}"
												disabled="disabled" /> <label
												for="edit_bannersections_${status.key}"
												class="css-label radGroup2">${status.value}</label>
										</label></li>
									</c:forEach>
								</ul>
							</div>
						</div>
					</div>
					<!----row--->

					<div class="row pad_top">
						<div class="col-md-6 browse_btn">
							<label>Banner Image</label><br> <input
								id="edit_bannerimageurl" class="upload_file upload_file_style"
								placeholder="Choose File" disabled="disabled" /> <input
								type="hidden" id="edit_bannerimageurlHidden" />
							<div class="fileUpload btn padTop_Btm">
								<span class="bg_w_button">Browse</span> <input
									name="edit_bannerimage" id="edit_bannerimage" type="file"
									class="upload upload_btn_modal editBannerImageUpload bwrsBtnAddPar" />
							</div>
						</div>

						<!-- <div class="col-md-6 image_wrap">
								 <img id="edit_bannerimage_" width="283px" height="107px"  src="" style="margin-top: 10px;">
							</div> -->

						<div class="col-md-4 image_wrap image_wrap1"
							style="width: 250px; height: 115px; margin-top: 10px;">
							<img id="edit_bannerimage_" src=""
								style="max-height: 100%; max-width: 100%;">
						</div>
						<div class="col-md-1 mar_top">
							<input class="bg_g_button" type="button" value="Upload"
								onclick="editUploadImage('edit_bannerimage','banner','edit_bannerimageurl')" />
							<input type="hidden" name="editBannerImageUrl"
								id="editBannerImageUrl" />
							<div id="editBannerError" class="customErrorMessageDisp error"></div>
						</div>
					</div>
					<!------------- row ----------->
					<div class="row pad_top">
						<div class="col-md-6 user_details">
							<label>Banner Status</label>
							<div class="half-col last status">
								<ul class="list-inline">
									<c:forEach items="${bannerStatus}" var="status">
										<li><label> <input type="radio"
												name="edit_bannerstatus" class="css-checkbox"
												id="edit_bannerstatus_${status.key}" value="${status.key}" />
												<label for="edit_bannerstatus_${status.key}"
												class="css-label radGroup2">${status.value}</label>
										</label></li>
									</c:forEach>
								</ul>
							</div>
						</div>
					</div>
					<!-- Added By Pawan -->
					<div class="row pad_top">
						<div class="col-md-4 user_details">
							<label>Start Date</label>
							 <div class="pick_date">
							 	<input type="text" class="form-control datepicker" id="editFromDate" name="editFromDate" readonly="readonly"/>
							</div> 
						</div>
						<div class="col-md-4 user_details">
							<label>End Date</label>
						   	<div class="pick_date">
								<input  type="text" class="form-control datepicker" id="editToDate" name="editToDate" readonly="readonly" />
							</div>
						</div>
					</div>
					<div class="row pad_top">
					<div class="col-md-6">
                        <label>Source</label>
                        <input type="text" name="edit_utmSourceName" id="edit_utmSourceName" class="form-control utmParam" placeholder="Name" />
                        <input type="text" name="edit_utmSourceValue" id="edit_utmSourceValue" class="form-control utmParam" class="form-control"  placeholder="Value" /> 
                        <input type="hidden" id="edit_utmSourceNameHidden"/>
                        <input type="hidden" id="edit_utmSourceValueHidden"/>
                     </div>
                     <div class="col-md-6">
                        <label>Medium</label>
                        <input type="text" name="edit_utmMediumName" id="edit_utmMediumName" class="form-control utmParam" placeholder="Name" />
                        <input type="text" name="edit_utmMediumValue" id="edit_utmMediumValue" class="form-control utmParam" class="form-control"  placeholder="Value" /> 
                     	<input type="hidden" id="edit_utmMediumNameHidden"/>
                        <input type="hidden" id="edit_utmMediumValueHidden"/>
                     </div>
                     <div class="col-md-6">
                        <label>Campaign</label>
                        <input type="text" name="edit_utmCampaignName" id="edit_utmCampaignName" class="form-control utmParam" placeholder="Name" />
                        <input type="text" name="edit_utmCampaignValue" id="edit_utmCampaignValue" class="form-control utmParam" class="form-control"  placeholder="Value" /> 
                     	<input type="hidden" id="edit_utmCampaignNameHidden"/>
                        <input type="hidden" id="edit_utmCampaignValueHidden"/>
                     </div>
                    </div>
                    <div class="editUtmerror"></div>
				  </div>
				<div class="row btn_save align_right">
					<input class="bg_g_button" type="button" value="Update" id="updateBanner"/>
				</div>
				<!-------------button----------->
			</div>
		</div>
	</form:form>
</div>

<!-- Modal content for view details carousel Banner management----------------------------------->
<div id="myModal2" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content for view details-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title">Banner Details</h4>
			</div>
			<!----modal---header--->
			<div class="modal-body2">
				<div class="row">
					<div class='col-md-6 user_details'>
						<label>Banner ID</label>
						<p id="bannerid" class="bannerNameClass"></p>
					</div>
					<div class='col-md-6 user_details'>
						<label>Banner Name</label>
						<p id="bannername" class="bannerNameClass"></p>
					</div>
				</div>
				<!----row--->
				<div class="row">
					<div class='col-md-6 user_details '>
						<label>Banner Sections</label>
						<p id="bannersections"></p>
					</div>
					<div class='col-md-6 user_details '>
						<label>Partner Name</label>
						<p id="partnername"></p>
					</div>
				</div>
				<!----row--->
				<div class="row user_details image_wrap_modal">
					<label style="">Banner Image</label> <img src='' id="bannerimage"
						height="107px" width="430px" />
					<p id="bannerimageurl" class="bannerNameClass"></p>

				</div>
				<!----row--->
				<div class="row">
					<div class='col-md-6 user_details'>
						<label>Landing URL</label>
						<p id="bannerlandingurl" class="bannerNameClass"></p>
					</div>
					<div class='col-md-6 user_details'>
						<label>Banner Status</label>
						<p id="bannerstatus"></p>
					</div>
				</div>
				<!----row--->
			</div>
			<!----modal-body---->
		</div>
	</div>
</div>

<div class='container'>
	<section class="insurance_management">
		<div id="breadcrumb">
			<ul>
				<li>Home</li>
				<li>Content Management</li>
				<li>Banner Management</li>
			</ul>
		</div>
		<!---breadcrumb--->

		<c:if test="${fn:length(message) gt 0}">
			<script>
				changeURL();
			</script>
			<div class="alertMessage" style="color: green; font-weight: bold;">
				<c:out value="${message}"></c:out>
			</div>
		</c:if>
		<c:if test="${fn:length(errormessage) gt 0}">
			<script>
				changeURL();
			</script>
			<div class="alertMessage" style="color: red; font-weight: bold;">
				<c:out value="${errormessage}"></c:out>
			</div>
		</c:if>

		<div class="section_heading">
			<h5>Banner Management</h5>
		</div>
		<!--- section heading ---->
		<div class="management">
			<!---User Management--->
			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#sectionA">Existing
						Banner</a></li>
				<li><a data-toggle="tab" href="#sectionB">Create New Banner</a></li>
			</ul>
			<div class="tab-content">
				<div id="sectionA" class="tab-pane fade in active">
					<!--- existing banner starts here --->
					<nav class="navbar navbar-default" role="navigation" id="submenu">
					<%-- 	<c:if test="${not empty cBannerList }"> --%>
							<h5 class="table_heading">Carousel Banners</h5>
							<!---- table header ---->
							<table class=" partner_info_table table-bordered" cellspacing="0"
								cellpadding="0" width="98%" id="table-1">
								<thead>
									<tr>
										 <!--<th class="name">Sl No.</th>--> 
										<th>partner  ID</th>
										<th>Banner ID</th>
										<th>Banner Name</th>
										<th>Banner Section</th>
										<th>Banner Image Name</th>
										<th>Banner Landing URL</th>
										<th>Banner Status</th>
									
										<th>utmSourceName</th>
										<th>utmSourceValue</th>
										<th> utmMediumName</th>
										<th>utmMediumValue</th>
										<th>utmCampaignName</th>
										<th>utmCampaignValue</th>
										<th>fromDate</th>
										<th>toDate</th>
										<th>Banner  No</th>
										<th class="width_section">Action</th>
									</tr>
								</thead>
								<!----thead--->
								
							<%-- 	<tbody>
									<c:forEach items="${cBannerList}" var="banner" varStatus="i">
										<tr class="admin" id="1.${banner.bannerNo}"
											data-id="${banner.bannerNo}"
											data-bannername="${banner.bannerName}"
											data-bannerid="${banner.bannerId}"
											data-bannersections="${banner.bannerSections}"
											data-bannersectionscode="${banner.bannerSectionsCode}"
											data-bannerimageurl="${banner.bannerImageUrl}"
											data-bannerlandingurl="${banner.bannerLandingUrl}"
											data-bannerstatus="${banner.bannerStatus}"
											data-bannerstatuscode="${banner.bannerStatusCode}"
											data-bannerstatusclass="${banner.bannerStatusClass}"
											data-bannerprtno="${banner.partNo}"
											data-bannerprtname="${banner.partnerName}"
											data-editfromdate="${banner.fromDate}"
											data-edittodate="${banner.toDate}"
											data-utmsourcename="${banner.utmSourceName}"
											data-utmsourcevalue="${banner.utmSourceValue}"
											data-utmmediumname="${banner.utmMediumName}"
											data-utmmediumvalue="${banner.utmMediumValue}"
											data-utmcampaignname="${banner.utmCampaignName}"
											data-utmcampaignvalue="${banner.utmCampaignValue}">

											<td class="slno_width">${i.count}</td>
											<td class="td_width">${banner.bannerId}</td>
											<td class="td_width">${banner.bannerName}</td>
											<td>${banner.bannerSections}</td>
											<!-- TODO:AW-321 -->
											<td class="td_width">${banner.bannerImageUrl}</td>
											<td class="td_width">${banner.bannerLandingUrl}</td>
											<td class="${banner.bannerStatusClass}">${banner.bannerStatus}</td>
											<td class="action_image"><span
												href="javascript:void(0);" class="view_details"
												data-target="#myModal2"><img
													src="${contextPath}/static/img/View_details_icon1.png"></img></span>
												<span class="editbanner" data-target="#myModal1"> <img
													src="${contextPath}/static/img/edit_icon.png"
													class="edit_wraper"></img>
											</span> <span class="delete btnDelete" data-target="#myModal"
												id="deleteBanner"> <img
													src="${contextPath}/static/img/delete_icon1.png" class="fa fa-trash fa-lg"></img>
											</span></td>
										</tr>
									</c:forEach>
								</tbody> --%>
								<!---tbody--->
							</table>
							<!---Crousal banner table-->

							<div class="row btn_save pull-right">
								<!-- 									<input class="bg_g_button" type="button" value="Edit Order" /> -->

								<span class="ceditorder"><input class="bg_g_button"
									type="button" id="ceditreorder"
									onClick="$(this).closest('form').submit();" value="Edit Order"></span>
								<span class="csaveorder" style="display: none;"><input
									class="bg_g_button" type="button" id="csbutton"
									onClick="$(this).closest('form').submit();" disabled="disabled"
									value="Save Order"></span> <input type="hidden"
									id="creorder_" name="reorder" />

							</div>
							<!------ button ------>
					<%-- 	</c:if> --%>
						<%-- <c:if test="${not empty sBannerList }">
							<div class="Static_table">
								<h5 class="table_heading">Static Banners</h5>
								<!--- table header --->
								<table class=" partner_info_table table-bordered"
									cellspacing="0" cellpadding="0" width="98%" id="table-2">
									<thead>
										<tr>
											<th class="name">Sl No.</th>
											<th>Banner ID</th>
											<th>Banner Name</th>
											<th>Banner Section</th>
											<!-- TODO:AW-322 -->
											<th>Banner Image Name</th>
											<th>Banner Landing URL</th>
											<th>Banner Status</th>
											<th class="width_section">Action</th>
										</tr>
									</thead>
									<!----thead--->
									<tbody>
										<c:forEach items="${sBannerList}" var="banner" varStatus="i">
											<tr class="admin" id="2.${banner.bannerNo}"
												data-id="${banner.bannerNo}"
												data-bannername="${banner.bannerName}"
												data-bannerId="${banner.bannerId}"
												data-bannersections="${banner.bannerSections}"
												data-bannersectionscode="${banner.bannerSectionsCode}"
												data-bannerimageurl="${banner.bannerImageUrl}"
												data-bannerlandingurl="${banner.bannerLandingUrl}"
												data-bannerstatus="${banner.bannerStatus}"
												data-bannerstatuscode="${banner.bannerStatusCode}"
												data-bannerstatusclass="${banner.bannerStatusClass}"
												data-bannerprtno="${banner.partNo}"
												data-bannerprtname="${banner.partnerName}"
												data-editfromdate="${banner.fromDate}"
												data-edittodate="${banner.toDate}">

												<td class="slno_width">${i.count}</td>
												<td class="td_width">${banner.bannerId}</td>
												<td class="td_width">${banner.bannerName}</td>
												<!-- TODO:AW-321 -->
												<td>${banner.bannerSections}</td>
												<td class="td_width">${banner.bannerImageUrl}</td>
												<td class="td_width">${banner.bannerLandingUrl}</td>
												<td class="${banner.bannerStatusClass}">${banner.bannerStatus}</td>
												<td class="action_image"><span href="javascript:void(0);" class="view_details" data-target="#static_banner_modal">
												<img src="${contextPath}/static/img/View_details_icon1.png"></img></span>
													<span class="editbanner" data-target="#myModal1"> <img src="${contextPath}/static/img/edit_icon.png"class="edit_wraper"></img>
												</span> <span class="delete btnDelete" data-target="#myModal" id="deleteBanner"> <img src="${contextPath}/static/img/delete_icon1.png" class="fa fa-trash fa-lg"></img>
												</span></td>
											</tr>
										</c:forEach>
									</tbody>
									<!---tbody--->
								</table>
								<!---table-->
								<div class="row btn_save pull-right">
									<!--<input class="bg_g_button" type="button" value="Edit Order" /> -->
									<span class="seditorder"><input class="bg_g_button" type="button" id="seditreorder" onClick="$(this).closest('form').submit();" value="Edit Order"></span>
									<span class="ssaveorder" style="display: none;"><input class="bg_g_button" type="button" id="ssbutton" onClick="$(this).closest('form').submit();" disabled="disabled" value="Save Order"></span> <input type="hidden" id="sreorder_" name="reorder" />
								</div>
								<!-------------button----------->
							</div>
						</c:if> --%>
					</nav>
				</div>
				<!----section A---->

				<!-----create new banner here---->
				<div id="sectionB" class="tab-pane fade submenu">
					<form:form autocomplete="off" id="bannerBean"
						commandName="bannerBean" enctype="multipart/form-data">
						<div class="row pad_top">

							<div class="col-md-4">
								<label for="country">Partners</label>
								<form:select path="partNo" name="partNo" id="partNo"
									class="select_option">
									<form:option value="">--Select--</form:option>
									<c:forEach items="${partnerList}" var="partner">
										<li><form:option value="${partner.prtNo}"
												label="${partner.partnerName}" /></li>
									</c:forEach>
								</form:select>
							</div>

							<div class="col-md-4">
								<label>Banner ID</label><span class="mandatory_star">*</span>
								<form:input path="bannerId" id="bannerId" name="bannerId"
									maxlength="50" class="form-control" />
							</div>
							<div class="col-md-4">
								<label>Banner Name</label><span class="mandatory_star">*</span>
								<form:input path="bannerName" maxlength="100"
									class="form-control" />
							</div>
						</div>
						<!--- row --->

						<div class="row pad_top">
							<div class="col-md-4">
								<label>Landing URL</label><span class="mandatory_star urlMand">*</span>
								<form:input path="bannerLandingUrl" class="form-control" />
							</div>

							<div class="col-md-4">
								<div class='text top'>
									<label>Banner Sections</label><span class="mandatory_star">*</span>
									<div class="half-col last">
										<ul class="list-inline">
											<c:forEach items="${bannerSections}" var="status">
												<li><label> <form:radiobutton
															path="bannerSectionsCode" class="css-checkbox"
															id="bannerSections_${status.key}" value="${status.key}" />
														<label for="bannerSections_${status.key}"
														class="css-label radGroup2">${status.value}</label>
												</label></li>
											</c:forEach>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<!--- row --->

						<div class="row pad_top">
							<div class="col-md-4">
								<label>Banner Image</label><span class="mandatory_star">*</span><br>
								<input id="uploadFile" class="upload_file"
									placeholder="Choose File" disabled="disabled" />
								<div class="fileUpload btn padTop_Btm">
									<span class="bg_w_button">Browse</span>
									<!-- TODO: Banner image placement -->
									<input id="bannerImage" name="bannerImage" type="file"
										class="upload newBannerImageUpload bwrsBtnAddPar" />
								</div>
							</div>
							<!---- browse button --->

							<!-- <div class="col-md-4 image_wrap">
                                     <img id="bannerImage_" width="348px" height="107px"  src="">
                                </div> -->

							<div class="col-md-4 image_wrap image_wrap1 noMar-top">
								<img id="bannerImage_" src=""
									style="max-height: 100%; max-width: 100%;" />
							</div>
							<div class="col-md-4">
								<input class="bg_g_button" type="button" value="Upload"
									onclick="uploadImage('bannerImage','banner','bannerImageUrl')" />
								<form:hidden path="bannerImageUrl" id="bannerImageUrl" />
								<div id="newBannerError" class="customErrorMessageDisp error"></div>
							</div>
						</div>
						<!------------- row ----------->
						<div class="row pad_top pad_bottom">
							<div class="col-md-4">
								<div class='text top'>
									<label>Banner Status</label><span class="mandatory_star">*</span>

									<div class="half-col last">
										<ul class="list-inline">
											<c:forEach items="${bannerStatus}" var="status">
												<li><label> <form:radiobutton
															path="bannerStatusCode" class="css-checkbox"
															id="${status.key}" value="${status.key}" /> <label
														for="${status.key}" class="css-label radGroup2">${status.value}</label>
												</label></li>
											</c:forEach>
										</ul>
									</div>
								</div>
								<!--                                        <div class="col-md-4"> -->

								<!--                                     </div> -->
							</div>
							<!--- radio button ---->
							<div class="col-md-4">
								<label>Start Date</label><span class="mandatory_star">*</span>
								<div class="pick_date">
									<form:input  type="text" class="form-control startDate" id="fromDate" path="fromDate" name="fromDate" readonly="readonly"/>
								</div>
							</div>
							<div class="col-md-4">
								<label>End Date</label><span class="mandatory_star">*</span>
								<div class="pick_date">
									<form:input  type="text" class="form-control endDate" id="toDate" path="toDate" name="toDate" readonly="readonly" />
								</div>
							</div>
						</div>
						<div class="row pad_top pad_bottom">
						<div class="col-md-4">
                        <label>Source</label> 
                        <input type="text" name="utmSourceName" id="utmSourceName" path="utmSourceName" class="form-control utmParam"  placeholder="Name" maxlength="45"/> 
                        <input type="text" name="utmSourceValue" id="utmSourceValue" path="utmSourceValue" class="form-control utmParam" placeholder="Value" maxlength="255"/>
                       
                     </div>
                     <div class="col-md-4">
                        <label>Medium</label> 
                        <input type="text" name="utmMediumName" id="utmMediumName" path="utmMediumName" class="form-control utmParam "  placeholder="Name" maxlength="45"/> 
                        <input type="text" name="utmMediumValue" id="utmMediumValue" path="utmMediumValue" class="form-control utmParam" placeholder="Value" maxlength="255"/>
                        
                     </div>
                     <div class="col-md-4">
                        <label>Campaign</label> 
                        <input type="text" name="utmCampaignName" id="utmCampaignName" path="utmCampaignName" class="form-control utmParam"  placeholder="Name" maxlength="45"/> 
                        <input type="text" name="utmCampaignValue" id="utmCampaignValue" path="utmCampaignValue" class="form-control utmParam" placeholder="Value" maxlength="255"/>
                        
                     </div>
                      <div class="utmerror"></div>
                     </div>
						<div class="row btn_save pull-right">
							<input class="bg_g_button" type="button" id="cancelbtnD"
								value="Cancel" /></a>
							<button type="button" class="bg_g_button"
								onClick="$(this).closest('form').submit(); checkValidationMessage();">Save</button>
						</div>
						<!------ button ------>
					</form:form>
				</div>
				<!----section B---->
			</div>
			<!-----tab-content---->
		</div>
		<!---- Banner managemnt----->
	</section>
	<!----- section ---->
</div>
<!---- container ---->

<script type="text/javascript">
	//Variables
	var bannerIDExist = false;
	var userNameExist = false;
	var uploadFlag = false;
	var bannerEditFlag = false;
	var uploadclick = true;
	var utmParamExist = false;

	$(document).ready(function() {
		$('.urlMand').hide();
		$('#fromDate').datepicker({
			  minDate: new Date()
			 });
		
		jQuery.extend( jQuery.fn.dataTableExt.oSort, {
		    "non-empty-string-asc": function (str1, str2) {
		    	str1 = Number($.trim(str1));
		    	str2 = Number($.trim(str2));
		        if(str1 == null || str1 == "")
		            return 1;
		        if(str1 == null || str2 == "")
		            return -1;
		        return ((str1 < str2) ? -1 : ((str1 > str2) ? 1 : 0));
		    },
		    "non-empty-string-desc": function (str1, str2) {
		    	str1 = Number($.trim(str1));
		    	str2 = Number($.trim(str2));
		        if(str1 == null || str1 == "")
		            return 1;
		        if(str1 == null || str2 == "")
		            return -1;
		        return ((str1 < str2) ? 1 : ((str1 > str2) ? -1 : 0));
		    }
		});

		/* $('.partner_info_table').DataTable( {
		     "paging": true,
		     "searching": false,
		     "lengthChange": false,
		     "info":false,
		     "iDisplayLength" : 100,
		     "order": [[ 0, "asc" ]],
		     columnDefs: [
		          {type: 'non-empty-string', targets: 0} // define 'name' column as non-empty-string type
		     ],
		 }); */
		
		
		$("#bannerBean").validate({
			onkeyup : true,
			ignore : [],
			rules : {
				bannerId : {
					required : true,
					rangelength : [ 1, 50 ],
					duplicateBannerId : true,
					AlphaNumeric : true
				},
				bannerName : {
					required : true,
					rangelength : [ 1, 100 ],
					AlphaNumeric : true
				},
				bannerImage : {
					extension : "png|jpg|jpeg|gif",
					filesize : 1100000,
					imageDimention : true
				},
				bannerImageUrl : {
					required : true,
					rangelength : [ 1, 250 ]
				},
				bannerLandingUrl : {
					required :false,
					rangelength : [ 1, 2000 ],
					url : true
				},
				fromDate : {
					required : true,
					dateHigherThanToday : true
				},
				toDate : {
					required : true,
				},
				utmSourceName : {
					duplicateUtmParam :true
				}
			},
			messages : {
				bannerId : {
					required : "Banner Id field should not be empty",
					rangelength : "Maximum 50 characters permitted",
					duplicateBannerId : "BannerID already exist",
					AlphaNumeric : "Special characters are not allowed"
				},
				bannerName : {
					required : "Banner Name field should not be empty",
					rangelength : "Maximum 100 characters permitted",
					AlphaNumeric : "Special characters are not allowed"
				},
				bannerImage : {
					extension : "Invalid file format. Only jpg|jpeg|png|gif files are allowed."
				},
				bannerImageUrl : {
					required : "Please Upload Banner Image",
					rangelength : "Maximum 100 characters permitted"
				},
				bannerLandingUrl : {
					 required : "Banner Landing Url field should not be empty" 
				},
				fromDate : {
					required : "Please Select Start Date",
					dateHigherThanToday : "Start date must be lesser than or equal End date"
				},
				toDate : {
					required : "Please Select End Date",
				},
				utmSourceName :{
					duplicateUtmParam:"utm parameters already exists"
				}
			},
			errorElement : "div",
			errorPlacement : function(error, element) {
				if ($(element).hasClass("newBannerImageUpload")) {
					error.insertAfter($(element).closest("div"));
				} else if ($(element).hasClass("form-control startDate")) {
					error.insertAfter($(element).closest("div"));
				} else if ($(element).hasClass("form-control endDate")) {
					error.insertAfter($(element).closest("div"));
				} else {
					error.insertAfter(element);
				}
			},
			submitHandler : function(form) {
				form.submit();
			}
		});
	});
	
	$(document).ready(function() {
		$("#editBannerBean").validate({
			ignore : [],
			rules : {
				edit_bannerid : {
					required : true,
					rangelength : [ 1, 50 ],
					duplicateBannerId : true,
					AlphaNumeric : true
				},
				edit_bannername : {
					required : true,
					rangelength : [ 1, 100 ],
					AlphaNumeric : true
				},
				edit_bannerimage : {
					extension : "jpg|jpeg|png|gif",
					filesize : 1100000,
					editImageDimention : true
				},
				edit_bannerlandingurl : {
					required :true,
					rangelength : [ 1, 2000 ],
					url : true
				},
				editFromDate : {
					required : true,
					editDateHigherThanToday : true
				},
				editToDate : {
					required : true,
				},
				edit_utmSourceName :{
					duplicateUtmParam :true
				}
			},
			messages : {
				edit_bannerid : {
					required : "Banner Id field should not be empty",
					rangelength : "Maximum 50 characters permitted",
					duplicateBannerId : "BannerID already exist",
					AlphaNumeric : "Special characters are not allowed"
				},
				edit_bannername : {
					required : "Banner Name field should not be empty",
					rangelength : "Maximum 100 characters permitted",
					AlphaNumeric : "Special characters are not allowed"
				},
				edit_bannerimage : {
					extension : "Invalid file format. Only jpg|jpeg|png|gif files are allowed."
				},
				edit_bannerlandingurl : {
					 required : "Banner Landing Url field should not be empty", 
				},
				editFromDate : {
					required : "Please Select Start Date",
					editDateHigherThanToday : "Start date must be lesser than or equal End date"
				},
				editToDate : {
					required : "Please Select End Date",
				},
				edit_utmSourceName :{
					duplicateUtmParam : ""
				}
			},
			errorElement : "div",
			errorPlacement : function(error, element) {
				 if ($(element).hasClass("editBannerImageUpload")) {
					error.insertAfter($(element).closest("div"));
				} else if ($(element).hasClass("datepicker")) {
					error.insertAfter($(element).closest("div"));
				} else {
					error.insertAfter(element);
				}
			},
			submitHandler : function(form) {
				form.submit();
			},
		});
	});
</script>

<script type="text/javascript">
	//Events

	$('a.biurl').on('click', function() {
		var id = $(this).closest('tr').data('id');
		var bannerimageurl = $(this).closest('tr').data('bannerimageurl');
		var imageUrl = '${contextPath}' + '/admin/images/banner/' + bannerimageurl;
              
		$('#bannerimage_').prop('src', imageUrl);
		$('#myModal1_').data('id', id).modal('show');
	});

	/* $('span.view_details').on('click', function() { */
		var partnerName='';
		 $('#table-1').on('click', 'a.view_details', function (e) {
			    var checka = table1.row( $(this).closest('tr')).data();
			  	id = checka.bannerId;
			  	bannerid= table1.row( $(this).closest('tr')).data().bannerId;
			  	bannername= table1.row( $(this).closest('tr')).data().bannerName;
			  	bannersections=table1.row( $(this).closest('tr')).data().bannerSections;
			  	bannerimageurl=table1.row( $(this).closest('tr')).data().bannerImageUrl;
			  	bannerlandingurl=table1.row( $(this).closest('tr')).data().bannerLandingUrl
			  	partnerName=table1.row( $(this).closest('tr')).data().partnerName;
			  	bannerstatus=table1.row( $(this).closest('tr')).data().bannerStatus ;
			  		
						/* var id = $(this).closest('tr').data('id');
						var bannerid = $(this).closest('tr').data('bannerid');
						var bannername = $(this).closest('tr').data(
								'bannername');
						var bannersections = $(this).closest('tr').data(
								'bannersections');
						var bannersectionscode = $(this).closest('tr').data(
								'bannersectionscode');
						var bannerimageurl = $(this).closest('tr').data(
								'bannerimageurl');
						var bannerlandingurl = $(this).closest('tr').data(
								'bannerlandingurl');
						var bannerstatus = $(this).closest('tr').data(
								'bannerstatus');
						var bannerstatuscode = $(this).closest('tr').data(
								'bannerstatuscode');
						var bannerstatusclass = $(this).closest('tr').data(
								'bannerstatusclass');
						var bannerprtname = $(this).closest('tr').data(
								'bannerprtname'); */
								
								$('#bannerid').text(bannerid);
								$('#bannername').text(bannername);
					  			$('#bannersections').text(bannersections);
					  			$('#partnername').text(partnerName);
					  			$('#bannerimageurl').text(bannerimageurl);
					  			$('#bannerlandingurl').text(bannerlandingurl);
					  			$('#bannerstatus').text(bannerstatus);
					  			/* $('#bannerstatus').html(
										"<font class="+bannerstatusclass+">"
												+ bannerstatus); */
					
								var imageUrl = '${contextPath}'
									+ '/admin/images/banner/' + bannerimageurl;

							$('#bannerimage').prop('src', imageUrl);

						/* $('#bannerid').text(bannerid);
						$('#bannername').text(bannername);
						$('#bannersections').text(bannersections);
						$('#partnername').text(bannerprtname);
						$('#bannersectionscode').text(bannersectionscode);

						var imageUrl = '${contextPath}'
								+ '/admin/images/banner/' + bannerimageurl;

						$('#bannerimage').prop('src', imageUrl);
						$('#bannerimageurl').text(bannerimageurl);
						$('#bannerlandingurl').text(bannerlandingurl);
						$('#bannerstatus').html(
								"<font class="+bannerstatusclass+">"
										+ bannerstatus);
						$('#bannerstatuscode').text(bannerstatuscode); */
						$('#myModal2').data('id', id).modal('show');
					});

	var id='',bannerid='',bannername='',bannerimageurl='',bannerlandingurl='',bannerstatus='',
	bannerprtno='',editfromdate='',edittodate='',utmSourceName='',utmSourceValue='',utmMediumName='',utmMediumValue='',
	utmCampaignName='',utmCampaignValue='',partNo='',bannerstatuscode='',bannerNo='',bannersectionscode='';
	/* $('span.editbanner').on('click', function() { */
		 $('#table-1').on('click', 'a.editbanner', function (e) {
                     
                     
		 
		 /* id = $(this).closest('tr').data('id');
		 bannerid = $(this).closest('tr').data('bannerid');
		 bannername = $(this).closest('tr').data('bannername');
		 bannersections = $(this).closest('tr').data('bannersections');
		 bannersectionscode = $(this).closest('tr').data('bannersectionscode');
		 bannerimageurl = $(this).closest('tr').data('bannerimageurl');
		 bannerlandingurl = $(this).closest('tr').data('bannerlandingurl');
		 bannerstatus = $(this).closest('tr').data('bannerstatus');
		 bannerstatuscode = $(this).closest('tr').data('bannerstatuscode');
		 bannerprtno = $(this).closest('tr').data('bannerprtno');
		 editfromdate = $(this).closest('tr').data('editfromdate');
		 edittodate = $(this).closest('tr').data('edittodate');
		 utmSourceName = $(this).closest('tr').data('utmsourcename');
		 utmSourceValue = $(this).closest('tr').data('utmsourcevalue');
		utmMediumName = $(this).closest('tr').data('utmmediumname');
		 utmMediumValue = $(this).closest('tr').data('utmmediumvalue');
		 utmCampaignName = $(this).closest('tr').data('utmcampaignname');
		 utmCampaignValue = $(this).closest('tr').data('utmcampaignvalue'); */
		 var checka = table1.row( $(this).closest('tr')).data();
                 var temp;
		  	id = checka.bannerNo;
//		  	bannerid= table1.row( $(this).closest('tr')).data().bannerId;
		  	bannerid= table1.row( $(this).closest('tr')).data().bannerId;
		  	bannername= table1.row( $(this).closest('tr')).data().bannerName;
		  	bannersections=table1.row( $(this).closest('tr')).data().bannerSections;
		  	bannerimageurl=table1.row( $(this).closest('tr')).data().bannerImageUrl;
		  	bannerlandingurl=table1.row( $(this).closest('tr')).data().bannerLandingUrl;
		  	prtNo=table1.row( $(this).closest('tr')).data().partNo;
		  	temp=table1.row( $(this).closest('tr')).data().partNo;
//                        var bannerprtno = $(this).closest('tr').data('bannerprtno');
		  	utmSourceName=table1.row( $(this).closest('tr')).data().utmSourceName;
		  	utmSourceValue=table1.row( $(this).closest('tr')).data().utmSourceValue;
		  	utmMediumName=table1.row( $(this).closest('tr')).data().utmMediumName ;
		  	utmMediumValue=table1.row( $(this).closest('tr')).data().utmMediumValue;
		  	utmCampaignName=table1.row( $(this).closest('tr')).data().utmCampaignName;
		  	utmCampaignValue=table1.row( $(this).closest('tr')).data().utmCampaignValue ;
		  	editfromdate=table1.row( $(this).closest('tr')).data().fromDate;
		  	edittodate=table1.row( $(this).closest('tr')).data().toDate ;
		  	if(table1.row( $(this).closest('tr')).data().bannerStatus == 'In Active'){
		  		bannerstatuscode = 0;
				}
			else{
				bannerstatuscode =1;
			}
		  	if(table1.row( $(this).closest('tr')).data().bannerSections == 'Static'){
		  		bannersectionscode = 1;
				}
			else{
				bannersectionscode =0;
			}
//		$("#editPartNo").val(temp);
//                        .selectmenu("refresh");
                $("#editPartNo").val(temp).selectmenu("refresh");
		$('#edit_bannerid').val(bannerid);
		$('#edit_bannername').val(bannername);
		$('#edit_bannersections_' + bannersectionscode).prop("checked", "checked");
		$('#edit_bannerimageurl').val(bannerimageurl);
		$('#edit_bannerimageurlHidden').val(bannerimageurl);
		$('#edit_banneridHidden').val(bannerid);
		$('#edit_utmSourceName').val(utmSourceName);
		$('#edit_utmSourceValue').val(utmSourceValue);
		$('#edit_utmMediumName').val(utmMediumName);
		$('#edit_utmMediumValue').val(utmMediumValue);
		$('#edit_utmCampaignName').val(utmCampaignName);
		$('#edit_utmCampaignValue').val(utmCampaignValue);
		
		$('#edit_utmSourceNameHidden').val(utmSourceName);
		$('#edit_utmSourceValueHidden').val(utmSourceValue);
		$('#edit_utmMediumNameHidden').val(utmMediumName);
		$('#edit_utmMediumValueHidden').val(utmMediumValue);
		$('#edit_utmCampaignNameHidden').val(utmCampaignName);
		$('#edit_utmCampaignValueHidden').val(utmCampaignValue);

		var imageUrl = '${contextPath}' + '/admin/images/banner/' + bannerimageurl;

		$('#edit_bannerimage_').prop('src', imageUrl);
		$('#edit_bannerlandingurl').val(bannerlandingurl);
		$('#edit_bannerstatus_' + bannerstatuscode).prop("checked", "checked");
		$('#editFromDate').val(editfromdate);
		$('#editToDate').val(edittodate);

		$('#myModal1').data('id', id).modal('show');
	});

	$('#updateBanner').click(function() {
		var id = $('#myModal1').data('id');
               
		var json = {};
		json["bnrNo"] = id;
		json["bannerId"] = $("#edit_bannerid").val();
		json["bannerName"] = $("#edit_bannername").val();
		json["bannerSections"] = $('input[name=edit_bannersections]:checked').val();
		json["prtNo"] = $("#editPartNo").val();
		if($("#editPartNo").val() ==''){
			$('#edit_bannerlandingurl').rules('remove', 'required');			
		}
		json["editBannerImageUrl"] = $('#edit_bannerimageurlHidden').val();//$("#edit_bannerimageurl").val();//
		json["bannerLandingUrl"] = $("#edit_bannerlandingurl").val();
		json["bannerStatus"] = $('input[name=edit_bannerstatus]:checked').val();
		json["editFromDate"] = $("#editFromDate").val();
		json["editToDate"] = $("#editToDate").val();
		json["utmSourceName"] = $("#edit_utmSourceName").val();
		json["utmSourceValue"] = $("#edit_utmSourceValue").val();
		json["utmMediumName"] = $("#edit_utmMediumName").val();
		json["utmMediumValue"] = $("#edit_utmMediumValue").val();
		json["utmCampaignName"] = $("#edit_utmCampaignName").val();
		json["utmCampaignValue"] = $("#edit_utmCampaignValue").val();
		var bannerstatus = $('input[name=edit_bannerstatus]:checked').val();

		var formData = new FormData();
		formData.append('json', JSON.stringify(json));

		if ( $("#editBannerBean").validate().element('#edit_bannerid')
                               && $("#editBannerBean").validate().element('#edit_bannername')
				&& $("#editBannerBean").validate().element('#edit_bannerimage')
				&& $("#editBannerBean").validate().element('#edit_bannerimageurl')
				&& $("#editBannerBean").validate().element('#edit_bannerlandingurl')
				&& $("#editBannerBean").validate().element('#editPartNo')
				&& $("#editBannerBean").validate().element('#editFromDate')
				&& $("#editBannerBean").validate().element('#editToDate') 
				&& $("#editBannerBean").validate().element('#edit_utmSourceName')) {
                      
			
			$.ajax({
				url : "${contextPath}/admin/updateBanner",
				data : formData,
				dataType : 'text',
				processData : false,
				contentType : false,
				type : 'POST',

				success : function(response) {
					$('#myModal1').modal('hide');
					window.location.href = "${contextPath}/admin/bannerManagement?updateMessage=" + response;
				}
			});
		}
	});

	$('#save').click(function() {
		$(this).closest('form').submit();
	});

	$(document).on("keydown", function(e) {
		if (e.which === 8 && !$(e.target).is("input, textarea")) {
			e.preventDefault();
		}
	});

	$('#cancelbtnD').click(function() {
		$.get("${contextPath}/admin/addBanerCancel", function(data, status) {
			if (status == "success") {
				window.location.replace("${contextPath}/" + data);
			}
		});
	});

	$('#cancelbtnD1_').click(function() {
		$.get("${contextPath}/admin/addBanerCancel", function(data, status) {
			if (status == "success") {
				window.location.replace("${contextPath}/" + data);
			}
		});
	});

	/* $('span.delete')
			.on(
					'click',
					function() { */
						 $('#table-1').on('click', 'a.delete', function (e) {
							 var checka = table1.row( $(this).closest('tr')).data();
							  	id = checka.bannerNo;
						    var id = table1.row( $(this).closest('tr')).data().bannerNo;
						var bannername = table1.row( $(this).closest('tr')).data().bannerName;;
						$('#deleteBannerId')
								.text(
										bannername
												+ " "
												+ " banner will be deleted. Do you want to continue?");
						$('#myModal').data('id', id).modal('show');
					});

	$('#btnD')
			.click(
					function() {
						var id = $('#myModal').data('id');
						$
								.get(
										"${contextPath}/admin/deleteBanner?bnrNo="
												+ id,
										function(status) {
											if (status == "success") {
												$('[data-id=' + id + ']')
														.remove();
												$('#myModal').modal('hide');
												window.location.href = "${contextPath}/admin/bannerManagement?deleteBnrMsg="
														+ status;
											}
											if (status == "failure") {
												$('[data-id=' + id + ']')
														.remove();
												$('#myModal').modal('hide');
												window.location.href = "${contextPath}/admin/bannerManagement?deleteBnrMsg="
														+ status;
											}
										});
					});

	//Drag & Drop table rows functionality starts
	$('span.ceditorder').click(function() {
	
		//alert("edit clicked");
		// Make a nice striped effect on the table
		table_1 = $("#table-1");
		table_1.find("tr:even").addClass("alt");
                 console.log("ffffffffffffffffffd");
		// Initialise the second table specifying a dragClass and an onDrop function that will display an alert
		table_1.tableDnD({
			onDragClass : "myDragClass",
                    
                         onDrop : function(table, row) {
				var rows = table.tBodies[0].rows;
				console.log("rows ----> ",rows)
				var debugStr = "";
                              console.log("fdsfsdfsdfdsfds"+row.id);
				for (var i = 0; i < rows.length; i++) {
					debugStr += rows[i].id + "-" + (i + 1) + ",";
				}
				//alert("debugStr "+debugStr);
				$("#creorder_").val(debugStr);
				console.log("-----> reorder ----> ",$("#creorder_").val());
				//$(table).parent().find('.result').text(debugStr);                
			},

			
			onDragStart : function(table, row) {
                            console.log("-----------------"+row.id);
                            console.log("-----------------"+table.tBodies[0].rows.length);
//				$(table).parent().find('.result').text("Started dragging row "+row.id);
				$("#csbutton").attr("disabled", false);
			}
                        
                       
		});

		$("span.csaveorder").show();
		$("span.ceditorder").hide();
	});

	$('span.seditorder').click(function() {
		// Make a nice striped effect on the table
		table_2 = $("#table-2");
		table_2.find("tr:even").addClass("alt");

		// Initialise the second table specifying a dragClass and an onDrop function that will display an alert
		table_2.tableDnD({
			onDragClass : "myDragClass",
			onDrop : function(table, row) {
				var rows = table.tBodies[0].rows;
				var debugStr = "";

				for (var i = 0; i < rows.length; i++) {
					debugStr += rows[i].id + "-" + (i + 1) + ",";
				}

				$("#sreorder_").val(debugStr);
				//$(table).parent().find('.result').text(debugStr);                
			},
			onDragStart : function(table, row) {
				//$(table).parent().find('.result').text("Started dragging row "+row.id);
				$("#ssbutton").attr("disabled", false);
			}
		});

		$("span.ssaveorder").show();
		$("span.seditorder").hide();
	});

	$('span.csaveorder').click(function() {
		var reorder = $("#creorder_").val();
		console.log("save reorder ---> ",reorder);
		$.ajax({
			url : "${contextPath}/admin/bannerSaveOrder?reorder=" + reorder,
			dataType : 'text',
			processData : false,
			contentType : false,
			type : 'POST',

			success : function(response) {
				window.location.reload();
			}
		});
	});

	$('span.ssaveorder').click(function() {
		var reorder = $("#sreorder_").val();
		$.ajax({
			url : "${contextPath}/admin/bannerSaveOrder?reorder=" + reorder,

			dataType : 'text',
			processData : false,
			contentType : false,
			type : 'POST',
			success : function(response) {
				window.location.reload();
			}
		});
	});
	//Drag & Drop table rows functionality ends

	$(function() {
		$("#editBannerBean").bind(
				"change",
				function() {
					//Get reference of FileUpload.
					var fileUpload = $("#edit_bannerimage")[0];
					var bannerType = $(
							"#editBannerBean input[type='radio']:checked")
							.val();
					checkFileUpload(fileUpload, bannerType, "editBannerError");
					if (!uploadFlag)
						return false;
				});
	});

	$(function() {
		$("#bannerBean").bind(
				"change",
				function() {
					//Get reference of FileUpload.
					var fileUpload = $("#bannerImage")[0];
					var bannerType = $(
							"#bannerBean input[type='radio']:checked").val();
					checkFileUpload(fileUpload, bannerType, "newBannerError");
					if (!uploadFlag)
						return false;
				});
	});

	/*$('#partNo').on('selectmenuchange', function () {
	 $("#bannerBean").validate().element('#partNo');
	 });


	 $('#editPartNo').on('selectmenuchange', function () {
	 $("#editBannerBean").validate().element('#editPartNo');
	 });*/

	$('#bannerImage').on('change', function() {
		$('#uploadFile').val($(this).val());
	});

	$('#editbannerImage').on('change', function() {
		$('#edit_bannerimageurl').val($(this).val());

	});

	$('#edit_bannerimage').on('change', function() {
		$('#edit_bannerimageurl').val($(this).val());
	});

	$(document).on("keydown", function(e) {
		if (e.which === 8 && !$(e.target).is("input, textarea")) {
			e.preventDefault();
		}
	});

	//Methods
	function checkValidationMessage() {
		document.getElementById("newBannerError").style.display = "none";
	}

	function uploadImage(id, type, url) {
		if ($("#bannerBean").validate().element("#bannerImage")) {
			var formData = new FormData();
			formData.append('file', $("#" + id)[0].files[0]);

			if ($("#bannerBean").validate().element("#" + id)) {
				$.ajax({
					url : "${contextPath}/admin/saveImages?type=" + type,
					data : formData,
					dataType : 'text',
					processData : false,
					contentType : false,
					type : 'POST',

					success : function(response) {
						uploadclick = false;
						$('#uploadFile').val(response);
						var newImageUrl = '${contextPath}' + '/admin/images/'
								+ type + "/" + response;
						$('#bannerImage_').attr('src', newImageUrl);
						$("#bannerBean").validate().element("#" + url);
						$('#bannerImageUrl').val(response);
						$("#bannerBean").validate().element("#bannerImageUrl");
					}
				});
			}
		}

	}

	function editUploadImage(id, type, url) {
		if ($("#editBannerBean").validate().element("#edit_bannerimage")) {
			var formData = new FormData();
			formData.append('file', $("#" + id)[0].files[0]);

			if ($("#editBannerBean").validate().element("#" + id)) {
				$.ajax({
					url : "${contextPath}/admin/saveImages?type=" + type,
					data : formData,
					dataType : 'text',
					processData : false,
					contentType : false,
					type : 'POST',

					success : function(response) {
						$('#' + url).val(response);
						var newImageUrl = '${contextPath}' + '/admin/images/'
								+ type + "/" + response;
						$('#edit_bannerimageurlHidden').val(
								$("#edit_bannerimageurl").val());
						$('#edit_bannerimage_').prop('src', newImageUrl);
						$("#editBannerBean").validate().element("#" + url);
					}
				});
			}
		}
	}

	function checkFileUpload(fileUpload, bannerType, bannerStatus) {
		//Check whether the file is valid Image.
		var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:\().+(\)\.def\().+(\])+(.jpg|.jpeg|.png|.gif)$");

		if (regex.test(fileUpload.value.toLowerCase())) {
			bannerEditFlag = true;
			//Check whether HTML5 is supported.
			if (typeof (fileUpload.files) != "undefined") {
				//Initiate the FileReader object.
				var reader = new FileReader();
				//Read the contents of Image File.
				reader.readAsDataURL(fileUpload.files[0]);

				reader.onload = function(e) {
					//Initiate the JavaScript Image object.
					var image = new Image();
					//Set the Base64 string return from FileReader as source.
					image.src = e.target.result;

					image.onload = function() {

						//Determine the Height and Width.
						var height = this.height;
						var width = this.width;

						var errorText = "";
						uploadFlag = false;

						if (bannerType == 0) {
							if (width == 999 && height == 292) {
								uploadFlag = true;
							} else {
								uploadFlag = false;
								//errorText = "Height and Width must be 293 and 735 for Carousel Banners.";
								errorText = "Height and Width must be 292 and 999 for Carousel Banners.";
							}
						} else {
							if (width == 233 && height == 144) {
								uploadFlag = true;
							} else {
								uploadFlag = false;
								errorText = "Height and Width must be 144 and 233 for Static Banners.";
							}
						}

						$("#" + bannerStatus).css({
							"display" : ""
						});
						$("#" + bannerStatus).html("<p>" + errorText + "</p>");

						if (errorText != "") {
							$("#bannerImageUrl-error").css({
								"display" : "none"
							});
						}
						return uploadFlag;
					};
				};
			}
		} else {
			bannerEditFlag = false;
			uploadFlag = false;
		}
	}

	// Validation Method
	$.validator.addMethod("AlphaNumeric", function(value, element) {
		return this.optional(element) || /^[a-zA-Z0-9\s\.]+$/i.test(value);
	});

	$.validator.addMethod('filesize', function(value, element, param) {
		return this.optional(element) || (element.files[0].size <= param)
	}, 'File size should not be more than 1MB');

	$.validator.addMethod('imageDimention', function(value, element) {
		var fileUpload = $("#bannerImage")[0];
		var bannerType = $("#bannerBean input[type='radio']:checked").val();
		checkFileUpload(fileUpload, bannerType, "newBannerError");
		return uploadFlag;
	}, '');

	$.validator.addMethod('editImageDimention',
			function(value, element) {
				var fileUpload = $("#edit_bannerimage")[0];
				var bannerType = $(
						"#editBannerBean input[type='radio']:checked").val();
				checkFileUpload(fileUpload, bannerType, "editBannerError");
				if (bannerEditFlag) {
					return uploadFlag;
				} else {
					return true;
				}

			}, '');

	$.validator.addMethod('duplicateBannerId', function(value, element) {
		if (bannerIDExist) {
			return false;
		} else {
			return true;
		}
	});
	
	$.validator.addMethod('duplicateUtmParam', function(value, element) {
		if (utmParamExist) {
			return false;
		} else {
			return true;
		}
	});

	/* $.validator.addMethod("dropdowncheck", function(value, element) {
		var val = $("#partNo").val();
		if (val == '') {
			return false;
		} else {
			return true;
		}
	});

	$.validator.addMethod("editdropdowncheck", function(value, element) {
		var val_ = $("#editPartNo").val();
		if (val_ == '') {
			return false;
		} else {
			return true;
		}
	}); */

	$('#partNo').on('selectmenuchange', function() {
		$("#bannerBean").validate().element('#partNo');
		if($('#partNo').val() !=''){
			$('.urlMand').show();
			$('#bannerLandingUrl').rules('add', 'required');
		}else{
			$('.urlMand').hide();
			$('#bannerLandingUrl').rules('remove', 'required');
		}
	});

	$('#editPartNo').on('selectmenuchange', function() {
		$("#editBannerBean").validate().element('#editPartNo');
		if($('#editPartNo').val() !=''){
			$('#edit_bannerlandingurl').rules('add', 'required');
		}else{
			$('#edit_bannerlandingurl').rules('remove', 'required');
		}
	});

	$("#bannerId").change(
			function() {
				var bannerId = $('#bannerId').val();
				$.get(
						"${contextPath}/admin/checkBannerID?bannerId="
								+ bannerId, function(data, status) {
							if (status == "success" && data != '') {
								bannerIDExist = true;
							} else {
								bannerIDExist = false;
							}
							$("#bannerBean").validate().element('#bannerId');
						});
			});

	$("#edit_bannerid").change(
			function() {
				var bannerId = $('#edit_bannerid').val();
				var bannerIdHidden = $('#edit_banneridHidden').val();
				bannerIDExist = false;

				if (bannerId != bannerIdHidden) {
					$.get("${contextPath}/admin/checkBannerID?bannerId="
							+ bannerId, function(data, status) {
						if (status == "success" && data != '') {
							bannerIDExist = true;
						} else {
							bannerIDExist = false;
						}
						$("#editBannerBean").validate().element(
								'#edit_bannerid');
					});
				} else {
					$("#bannerBean").validate().element('#utmSourceName');
					$(".utmerror").text("Utm Parameters already exists");
				}
			});
	$("#utmSourceName,#utmSourceValue,#utmMediumName,#utmMediumValue,#utmCampaignName,#utmCampaignValue").on("keyup",	
			function() {
		utmParamExist = false;
		var utmSource = $('#utmSourceName').val()+"="+$('#utmSourceValue').val();
		var utmMedium = $('#utmMediumName').val()+"="+$('#utmMediumValue').val();
		var utmCampaign = $('#utmCampaignName').val()+"="+$('#utmCampaignValue').val();
		var json = {};
		json["utmSource"] = utmSource;
		json["utmMedium"] = utmMedium;
		json["utmCampaign"] = utmCampaign;
		
		var formData = new FormData();
		formData.append('json', JSON.stringify(json));
		
		$.ajax({
			url : "${contextPath}/admin/checkUtmParameter",
			data : formData,
			dataType : 'text',
			processData : false,
			contentType : false,
			type : 'POST',

			success : function(response) {
				if(response=="exist"){
					utmParamExist = true;
					$("#bannerBean").validate().element('#utmSourceName');
				}
				
			}
		});
	
 	});
	
	$("#edit_utmSourceName,#edit_utmSourceValue,#edit_utmMediumName,#edit_utmMediumValue,#edit_utmCampaignName,#edit_utmCampaignValue").on("keyup",	
			function() {
		utmParamExist = false;
		var utmSource = $('#edit_utmSourceName').val()+"="+$('#edit_utmSourceValue').val();
		var utmMedium = $('#edit_utmMediumName').val()+"="+$('#edit_utmMediumValue').val();
		var utmCampaign = $('#edit_utmCampaignName').val()+"="+$('#edit_utmCampaignValue').val();
		var utmSourceHidden = $('#edit_utmSourceNameHidden').val()+"="+$('#edit_utmSourceValueHidden').val();
		var utmMediumHidden = $('#edit_utmMediumNameHidden').val()+"="+$('#edit_utmMediumValueHidden').val();
		var utmCampaignHidden = $('#edit_utmCampaignNameHidden').val()+"="+$('#edit_utmCampaignValueHidden').val();
		
		var json = {};
		json["utmSource"] = utmSource;
		json["utmMedium"] = utmMedium;
		json["utmCampaign"] = utmCampaign;
		
		var formData = new FormData();
		formData.append('json', JSON.stringify(json));
		if(utmSource == utmSourceHidden && utmMedium == utmMediumHidden && utmCampaign == utmCampaignHidden){
		
	}else{
		$.ajax({
			url : "${contextPath}/admin/checkUtmParameter",
			data : formData,
			dataType : 'text',
			processData : false,
			contentType : false,
			type : 'POST',

			success : function(response) {
				if(response=="exist"){
					utmParamExist = true;
					$("#editBannerBean").validate().element('#edit_utmSourceName');
					$(".editUtmerror").text("Utm Parameters already exists");
				}
				
			}
		});
	}
 	});
	//Added by Pawan

	function getFormattedDate(date) {
		var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
		return new Date(date.replace(pattern, '$3-$2-$1'));
	}

	$('#fromDate').on('change', function() {
		$("#bannerBean").validate().element('#fromDate');
		var endDate = getFormattedDate($('#toDate').val());

		if (endDate != 'Invalid Date') {
			$("#bannerBean").validate().element("#toDate");
		}
	});

	$("#toDate").on('change', function() {
		$("#bannerBean").validate().element("#toDate");
		$("#bannerBean").validate().element("#fromDate");
	});


	$("#editFromDate").on('change', function() {
		$("#editBannerBean").validate().element("#editFromDate");
	});
	$("#editToDate").on('change', function() {
		$("#editBannerBean").validate().element("#editToDate");
		$("#editBannerBean").validate().element("#editFromDate");
	});

	$.validator.addMethod("dateHigherThanToday", function(value, element) {
		var startDate = getFormattedDate($('#fromDate').val());
		var endDate = getFormattedDate($('#toDate').val());

		if (endDate == 'Invalid Date')
			return true;
		if (endDate !== 'Invalid Date') {
			if (startDate <= endDate)
				return true;
			if (startDate >= endDate)
				return false;
		}
	});
	
	$.validator.addMethod("editDateHigherThanToday", function(value, element) {
		var startDate = getFormattedDate($('#editFromDate').val());
		var endDate = getFormattedDate($('#editToDate').val());

		if (endDate == 'Invalid Date')
			return true;
		if (endDate !== 'Invalid Date') {
			if (startDate <= endDate)
				return true;
			if (startDate >= endDate)
				return false;
		}
	});

	$("#fromDate").datepicker({
		dateFormat : 'dd/mm/yy',
		showOn : "button",
		buttonImage : "./static/img/2016-06-23.png",
		buttonImageOnly : true,
		minDate: new Date(),
		  /* onClose : function(selectedDate) {
			$("#toDate").datepicker("option", "minDate", selectedDate);
		}, */  
		buttonText : "Select date"
	});

	$("#toDate").datepicker({
		dateFormat : 'dd/mm/yy',
		showOn : "button",
		buttonImage : "./static/img/2016-06-23.png",
		buttonImageOnly : true,
		minDate: new Date(),
		buttonText : "Select date"
	});

	$("#editFromDate").datepicker({
		dateFormat : 'dd/mm/yy',
		showOn : "button",
		buttonImage : "./static/img/2016-06-23.png",
		buttonImageOnly : true,
		minDate: new Date(),
		 /* onClose : function(selectedDate) {
			$("#editToDate").datepicker("option", "minDate", selectedDate);
		},  */
		buttonText : "Select date"
	});

	$("#editToDate").datepicker({
		dateFormat : 'dd/mm/yy',
		showOn : "button",
		buttonImage : "./static/img/2016-06-23.png",
		buttonImageOnly : true,
		minDate: new Date(),
		buttonText : "Select date",
		beforeShow : end_Range
	});
	function end_Range() {
		if ($("#editFromDate").datepicker("getDate") == null) {
			$("#editFromDate").focus();
		} else {
			// set minimum date range
			$("#editToDate").datepicker("option", "minDate",
					new Date($("#editFromDate").datepicker("getDate")));
		}	
	}
	
	
	 /*  Pagination OffersDeal */
    var table1;
     $(document).ready(function() {
    	var i =1;
      table1= $('#table-1').DataTable( {
					    	  "iDisplayLength" : 10,
						      "ordering": false,
						      "bPaginate": true,
						      "bProcessing": true,
						      "searching": false,
						     // "scrollX": true, 
						      "serverSide": true,
						      "rowReorder": true,
												 
							 //"scrollX": true, 
							      
							     
							    
							    
						  "ajax":"${contextPath}/admin/BannerPagination",
							  "type": 'GET',
							  "columns": [
								  
								   {"data":"partNo"},
//								    {"data":"bannerNo"},
								    { "data": "bannerId"},
							        { "data": "bannerName"},
							        { "data": "bannerSections" }, 
							        { "data": "bannerImageUrl" },
							        { "data": "bannerLandingUrl" },
							        { "data": "bannerStatus" },
							        { "data": "utmSourceName" },
							        { "data": "utmSourceValue" },
							        { "data": "utmMediumName" },
							        { "data": "utmMediumValue" },
							        { "data": "utmCampaignName" },
							        { "data": "utmCampaignValue" },
							        { "data": "fromDate" },
							        { "data": "toDate" },
							        {"data":"bannerNo"},
							        {
						                "data": "null",
						                className: "center",
						                defaultContent: '<a class="view_details" ><img class="edit_wraper" src="${contextPath}/static/img/View_details_icon1.png"></a> '
						                +'<a class="editbanner" data-target="#myModal1"><img src="${contextPath}/static/img/edit_icon.png"></img></a>'
						                +'<a class="delete btnDelete" data-target="#myModal" id="deleteBanner"><img src="${contextPath}/static/img/delete_icon1.png" class="fa fa-trash fa-lg"></img></a>'
						            }  
							        
							  
							    ]  ,
							  "columnDefs": [
								  {
						             "targets": [ 0,7,8,9,10,11,12,13,14,15],
						               "visible": false,
						               "searchable": false
						            },
						            
						         
								 { "defaultContent": "-", "targets": "_all" }
						            
								  ],
								  "createdRow": function( row, data, dataIndex){
									  $(row).attr('id', data.bannerNo);
			                                          
                                                           if(data.bannerStatus ==='Active' ){
			                                   console.log("$('td', row) "+$('td', row))
			                                   console.log("$('td', row) "+row);
			                            	$('td', row).eq(5).css('color', 'Green');
			                                  } 
			                            else {
			                            	$('td', row).eq(5).css('color', 'Red');
			                            }     } ,
			                            
			                            
							});
     });
	
	
</script>

