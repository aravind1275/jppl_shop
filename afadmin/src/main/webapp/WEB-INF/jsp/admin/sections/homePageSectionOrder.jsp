<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />

<style>
.ui-autocomplete {
    z-index: 99999999999999 !important;
}
</style>

<script>
	function changeURL(){
                	history.pushState({}, '',"${contextPath}/admin/homePageSectionOrder");
        }
</script>

 
        <div class='container'>
            <section class="affiliate_management">
                <div id ="breadcrumb">
                    <ul>
                        <li>Home</li>
						<li>Configuration</li>
						<li>Home Page Section Order</li>
                    </ul>
                </div>  <!---breadcrumb--->
                
               <c:if test="${fn:length(message) gt 0}">
	              <script>changeURL();</script>
	                <div class="alertMessage" id="divChanged" style="color:green;font-weight: bold;"  ><c:out value="${message}"></c:out></div> </c:if>
	  			<c:if test="${fn:length(errormessage) gt 0}">
	  			   <script>changeURL();</script>
	       		<div class="alertMessage" style="color:red;font-weight: bold;"  ><c:out value="${errormessage}"></c:out></div> </c:if>
                <div class="section_heading">
               
					<h5>Home Page Section Display Order</h5> 
				</div><!--- section heading ---->
				<div class="management"><!---User Management--->
                    <ul class="nav nav-tabs">
                        <li id="viewSectionTab"  class="active"><a data-toggle="tab" href="#sectionA">Home Page Section List</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="sectionA" class="tab-pane fade in active">
                          
                            <table class=" partner_info_table table-bordered" cellspacing="0" cellpadding="0" width="98%" id="table-1">
								
								<thead>
								
									<tr>
										<th class="name">Section Name</th>
										<th>Display Order</th>
										<th>Status</th>
										<th class="width_section">Action</th>
									</tr>
								</thead><!----thead--->
								<tbody>
								 	<c:forEach items="${homeSectionList}" var="homePageSection" varStatus="i">
									<tr class="admin" id="1.${homePageSection.sectionId}"
									 data-id="${homePageSection.sectionId}"
									 data-sectiontitle="${homePageSection.sectionName}"
									 data-displayorder="${homePageSection.sectionPosition}"
									 data-statusvalue="${homePageSection.statusValue}"
									 data-status="${homePageSection.status}"
									 data-statuscodeclass="${homePageSection.statusCodeClass}"
									 data-loggedinstatus="${homePageSection.loggedInStatus}"
									 data-nonLoggedinstatus="${homePageSection.nonLoggedInstatus}"
									 data-sectioncode="${homePageSection.sectionCode}">
									<td class="td_width">${homePageSection.sectionName}</td>
									<td class="td_width">${homePageSection.sectionPosition}</td>
									<td class="${homePageSection.statusCodeClass} td_width">${homePageSection.statusValue}</td>
									<td class="td_width"><span class="editsection"  data-target="#myModal1">
												<img src="${contextPath}/static/img/edit_icon.png" class="edit_wraper" ></img>
											</span></td>
									
									</tr> 
								 	</c:forEach>							
								</tbody><!---tbody--->
								 
							</table><!---table-->	
							
							<div class="row btn_save pull-right">								
								<span class="peditorder"><input class="bg_g_button" type="button" id="peditreorder" onClick="$(this).closest('form').submit();" value="Edit Display Order"></span>						
								<span class="psaveorder" style="display:none;"><input class="bg_g_button" type="button" id="psbutton" onClick="$(this).closest('form').submit();" disabled="disabled" value="Save Order"></span>
								<input type="hidden" id="preorder_" name="reorder"/>
							</div><!------ button ------>
							
                        </div><!----section A---->
                       
                                                               
                    </div><!---- tab-content --->
                </div>	<!---- user managemnt ---->	
            </section><!---- Section ---->	
            	
        </div>
        <!-- Edit section Pop up Start -->
 <div id="myModal1" class="modal fade" role="dialog">
       <form:form autocomplete="off"  id="homepageSectionBean"	commandName="homepageSectionBean">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Edit Home Page Section Details</h4>
                    </div><!----modal-header--->
                    <div class="modal-body1">
                        <div class="row pad_top" >
                            <div class="col-md-4 user_details">
                                <label>Section Title</label>
                                <input type="text" minlength="3" maxlength="50" disabled
												class="form-control " name="section_title" id="section_title">
                            </div>
                           <%--  <div class="col-md-4 statusDiv">
								<div class='text top' >
									<label>Section Status</label>
									<div class="half-col last">
										<ul class="list-inline">
				
											<c:forEach items="${SectionStatus}" var="status">
														<li><label> 														
														<input type="radio"	name="section_status" class="css-checkbox" id="section_status_${status.key}" value="${status.key}"  />
																<label for="section_status_${status.key}" class="css-label radGroup2">${status.value}</label>														 																
														</label></li>
											</c:forEach>
										</ul>
									</div>
								</div>
							</div>  --%>
                        </div><!----row--->
                        <div class="row pad_top">
							<div class="col-md-4 user_details pad_top">
							 <label for="edit_loggedInStatus">Logged In Status</label>
                              <div>  <input type="checkbox" class="allow_checkbox" value="0" name="edit_loggedInStatus" id="edit_loggedInStatus" /></div>
                            </div>
                            <div class="col-md-4 user_details pad_top">
                            <label for="edit_nonLoggedInStatus">Non Logged In Status</label>
                             <div><input type="checkbox" class="allow_checkbox" value="0" name="edit_nonLoggedInStatus" id="edit_nonLoggedInStatus" /></div>
                                
                            </div>
                        </div><!----row--->
                            
						<div class="row btn_save">
							<a href="${contextPath}/admin/homePageSectionOrder"><input class="bg_g_button" type="button" value="Cancel" /></a>
							<input class="bg_g_button" type="button" id="updateSection" value="Update" />
						</div><!-------------button----------->
                    </div>
                </div>
            </div>
            </form:form>
        </div>
<script>

$('span.peditorder').click(function () {
    
    // Make a nice striped effect on the table
    table_1 = $("#table-1");
    table_1.find("tr:even").addClass("alt");
    // Initialise the second table specifying a dragClass and an onDrop function that will display an alert
    table_1.tableDnD({
        onDragClass: "myDragClass",
        onDrop: function(table, row) {
            var rows = table.tBodies[0].rows;
            var debugStr = "";
            for (var i=0; i<rows.length; i++) {
                //debugStr += rows[i].id+"-"+(i+1)+",";
            	debugStr += rows[i].id+",";
            }   
            $("#preorder_").val(debugStr); 
            //$(table).parent().find('.result').text(debugStr);                
        },
        onDragStart: function(table, row) {
            //$(table).parent().find('.result').text("Started dragging row "+row.id);
            $("#psbutton").attr("disabled",false);
        }
    });
    $("span.psaveorder").show();
    $("span.peditorder").hide();
         
    
});


$('span.psaveorder').click(function () {	
	var reorder = $("#preorder_").val();
		$.ajax({
			  url: "${contextPath}/admin/homeSectionDisplaySaveOrder?reorder="+reorder,
			  
			  dataType: 'text',
			  processData: false,
			  contentType: false,
			  type: 'POST',
			  success: function (response) {				 				 
					window.location.reload();
			  }
			 });
});


$('span.editsection').on('click', function () {
	 
	var id = $(this).closest('tr').data('id');
	var sectionTitle = $(this).closest('tr').data('sectiontitle');
	var displayOrder = $(this).closest('tr').data('displayorder');
	var status= $(this).closest('tr').data('status');
	var activationDate= $(this).closest('tr').data('activationdate');
	var deactivationDate= $(this).closest('tr').data('deactivationdate');
	var edit_loggedInStatus=$(this).closest('tr').data('loggedinstatus');
	var edit_nonLoggedInStatus=$(this).closest('tr').data('nonloggedinstatus');
	var sectionCode=$(this).closest('tr').data('sectioncode');
	/* if(sectionCode>5){
		$('.statusDiv').hide();
	}else{
		$('.statusDiv').show();
	} */
	

	 
	$("#section_title").val(sectionTitle);
	$('#display_order').val(displayOrder);
	$('#section_status_'+status).prop("checked","checked");
	$("#hidden_displayorder").val(displayOrder);
	$("#hidden_sectiontitle").val(sectionTitle);
	$("#edit_loggedInStatus").val(edit_loggedInStatus);
	$("#edit_nonLoggedInStatus").val(edit_nonLoggedInStatus);
	if(edit_loggedInStatus == "0"){
		$("#edit_loggedInStatus").prop("checked", false);
	}
	else{
		$("#edit_loggedInStatus").prop("checked", true);
	}
	if(edit_nonLoggedInStatus == "0"){
		$("#edit_nonLoggedInStatus").prop("checked", false);
	}
	else{
		$("#edit_nonLoggedInStatus").prop("checked", true);
	}
	$("#edit_loggedInStatus").on('change', function() {
	    if($(this).is(":checked")) {
	    	$('#edit_loggedInStatus').val(1);
	    }
	    else{
	    	$('#edit_loggedInStatus').val(0);
	    }
	});
	    $("#edit_nonLoggedInStatus").on('change', function() {
		    if($(this).is(":checked")) {
		    	$('#edit_nonLoggedInStatus').val(1);
		    }
		    else{
		    	$('#edit_nonLoggedInStatus').val(0);
		    }});
	$('#myModal1').data('id',id).modal('show');  

	
});
$('#updateSection').click(function () {
	var id = $('#myModal1').data('id');
	var sectionType = $("#section_Type").val();
	var json={};
	json["sectionId"]=$('#myModal1').data('id');
	json["sectionTitle"]=$("#section_title").val();
	json["displayOrder"]=$("#display_order").val();
	json["loggedInStatus"]=$("#edit_loggedInStatus").val();
	json["nonLoggedInStatus"]=$("#edit_nonLoggedInStatus").val();
	//json["status"]=$('input[name=section_status]:checked').val();
	var formData = new FormData();
	formData.append('json',JSON.stringify(json));
	if($("#homepageSectionBean").validate().element('#section_title') 
			
			){		

	$.ajax({
			  url: "${contextPath}/admin/updateHomePageSections",
			  data: formData,
			  dataType: 'text',
			  processData: false,
			  contentType: false,
			  type: 'POST',
			  success: function (response) {
				 
					  $('#myModal1').modal('hide');
				  window.location.href="${contextPath}/admin/homePageSectionOrder?updateMessage="+response;
			  },
			  error: function (response) {
				  $('#myModal1').modal('hide');
				  window.location.href="${contextPath}/admin/homePageSectionOrder?updateMessage=failure";
					
            	 }
			 });
	}
});

</script>
