<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />

<style>
.ui-autocomplete {
    z-index: 99999999999999 !important;
}
</style>

<script>
	function changeURL(){
                	history.pushState({}, '',"${contextPath}/admin/homePageSections");
        }
</script>
<script src='https://cdn.tinymce.com/4/tinymce.min.js'></script> 
		<!-- Select Category Modal Start -->
		<div id="categoriesModal" class="modal fade" role="dialog">	
			<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div id= modalCat class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Select Category</h4>
				</div><!--header-->
				<div class="modal-body">
					<div class="select_category"></div>
				</div><!---body--->
				<div class="modal-footer">
				</div>
			</div><!---modal content--->
			</div>
		</div>
		<!-- Select Category Modal End -->
 
 <!-- Modal for delete -->
		<div id="myModala" class="modal fade" role="dialog">	
			<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Are You Sure?</h4>
				</div><!--header-->
				<div class="modal-body">
					<p>
					<img src="${contextPath}/static/img/info_icon1.png"></img>
					<span id="deleteItemId" class="bannerNameClass"></span> 
					</p>
				</div><!---body--->
				<div class="modal-footer">
					<button id="btnD" class="btn btn-grey bgp" data-dismiss="modal">Delete</button>
				</div>
			</div><!---modal content--->
			</div>
		</div>
 
 		<div id="myModal2" class="modal fade" role="dialog">
        	<form:form autocomplete="off"  id="editSectionItems" commandName="editSectionItems">
				<div class="modal-dialog">
					<!-- Modal content-->
                	<div class="modal-content">
                		<div class="modal-header">
                       		<button type="button" class="close"  data-dismiss="modal" aria-hidden="true">&times;</button>
                        	<h4 class="modal-title">Edit Section Items</h4>
                    	</div><!----modal-header--->
                    	
                    	<div class="modal-body1">
                    		<div class="row pad_top" >
                    		
                    		<div style="margin:0px 0px 5px 15px">
                        		<input type="hidden" value="" id="edit_dupRecord" name="edit_dupRecord" />
                        	</div>
                    			<div class="col-md-6 user_details">
                    			<label>Select A Section</label>
                    			<fieldset class="infield">
									<select id="edit_sectionId" class="select_option" disabled>          
										<option value="">Select Section</option>
										<c:forEach items="${sectionList}" var="section">
											<li>
												<option value="${section.sectionId}">${section.sectionTitle}</option>
											</li>
										</c:forEach> 
									</select>
								</fieldset>
                    			</div>
                    			
                    			<div class="col-md-6 user_details">
                    			<label>Item Type</label>
                    			<fieldset class="infield">
									<select id="edit_itemType" class="select_option" disabled>          
										<option value="">Select Item Type</option>
		          	 					 <c:forEach items="${itemType}" var="item">
			           					  	<li>
			           					      	<option data-itemvalue="${item.value}" value="${item.key}">${item.value}</option>
			           							</li>
		           					</c:forEach> 
									</select>
								</fieldset>
                    			</div>
                    		</div><!----row--->

					<div class="row pad_top">

						<div class="col-md-6 user_details" id="edit_prtDiv">
							<label>Select A Partner</label>
							<fieldset class="infield">
								<select id="edit_prtNo" name="edit_prtNo" class="select_option">
									<option value="">Select Partner</option>
									<c:forEach items="${partnerList}" var="partner">
										<li>
											<option value="${partner.prtNo}">${partner.partnerName}</option>
										</li>
									</c:forEach>
								</select> <input type="hidden" name="edit_prtNo_hidden"
									id="edit_prtNo_hidden" />
							</fieldset>
						</div>
						<div id="edit_brandNameDiv">
							<div class="col-md-6">
								<label>Select Brand</label> <input type='text'
									name="editBrandName" id="editBrandName" class="form-control" />
									<input type="hidden" name="editBrandName_hidden" id="editBrandName_hidden"/>
							</div>
							<div class="row pad_top mar_bottom">
								<div class="col-md-6" id="edit_brandDescDiv">
									<label>Brand Description</label> <input type='text' 
										name="editBrandDescription" id="editBrandDescription"
										class="form-control" />
									
								</div>
							</div>
						</div>


						<div class="col-md-6 user_details">
							<label>Section Item Status</label>
							<div class="half-col last">
								<ul class="list-inline">
									<c:forEach items="${sectionItemStatus}" var="status">
										<li><label> <input type="radio"
												name="edit_sectionItemStatus" class="css-checkbox"
												id="edit_sectionItemStatus_${status.key}"
												value="${status.key}" /> <label
												for="edit_sectionItemStatus_${status.key}"
												class="css-label radGroup2">${status.value}</label>
										</label></li>
									</c:forEach>
								</ul>
							</div>
							<!----row--->
						</div>


					</div>
					<!----row--->
					<div class="row pad_top" id="edit_brandImageDiv">
						<div class="col-md-6 user_details">
							<label>Brand Image</label><br>
							<div class="fileUpload btn">
								<input id="editBrandImageUrl"
									class="upload_file upload_file_style" placeholder="Choose File"
									disabled="disabled" /> <span class="bg_w_button edit_partner_browse">Browse</span>
								<input name="editBrandImage" id="editBrandImage" type="file"
									class="upload upload_btn_modal editBrandImageUpload bwrsBtnPop" />
							</div>
							<div class="image_wrap image_wrap1"
								style="width: 360px; height: 150px;">
								<img id="editBrandImage_" src=""
									style="max-height: 100%; max-width: 100%;">
							</div>

							<div class="mar_top uploadFile_div">
								<input class="bg_g_button"
									onclick="editUploadImage('editBrandImage','brand','editBrandImageUrl')"
									type="button" value="Upload" /> <input type="hidden"
									name="edit_brandImageUrlHidden" id="edit_brandImageUrlHidden">
							</div>
							<div id="icon_error2" class="error"></div>
							
						</div>
					</div>
					
					<!-- Custom Setion Edit -->
					
					<div class="row pad_top" id="edit_CustomSectionDiv">
						<div class="col-md-6 user_details">
							<label>Custom Image</label><br>
							<!-- <div class="fileUpload btn">
								<input id="editBrandImageUrl1"
									class="upload_file upload_file_style" placeholder="Choose File"
									disabled="disabled" /> <span class="bg_w_button edit_partner_browse">Browse</span>
								<input name="editBrandImage1" id="editBrandImage1" type="file"
									class="" />
							</div> -->
							  <div class="fileUpload btn">
								<input id="editBrandImageUrl1" class="upload_file upload_file_style" placeholder="Choose File" disabled="disabled" aria-required="true"> <label class="bg_w_button edit_partner_browse">Browse
                                <input name="editBrandImage1" id="editBrandImage1" type="file" style="display:none"></label>
							</div>
							<div class="image_wrap image_wrap1"
								style="width: 360px; height: 150px;">
								<img id="editBrandImage_1" src=""
									style="max-height: 100%; max-width: 100%;">
							</div>

							<div class="mar_top uploadFile_div" >
								<input class="bg_g_button"
									onclick="editUploadImage1('editBrandImage1','brand1','editBrandImageUrl1')"
									type="button" value="Upload" /> <input type="hidden"
									name="edit_brandImageUrlHidden1" id="edit_brandImageUrlHidden1">
									<div id="icon_error" class="error"></div>
							</div>
							 <div class="row pad_top">
                                <input type="checkbox" class="allow_checkbox" value="0" name="edit_displayPrtPopup" id="edit_displayPrtPopup" />
                                <label for="edit_displayPrtPopup">Display Partner Popup</label>
                            </div>
							<div class="row pad_top" id="edit_CustomSectionDiv">
								<div class="col-md-8">
									<label>Custom Item Text</label> <input type='text' 
										name="editText" id="editText"
										class="form-control" />
									
								</div>
								
								</div>
								<div class="row pad_top " id="edit_CustomSectionDiv">
								<div class="col-md-8" >
									<label>Custom Item Title</label> <span class="mandatory_star">*</span><input type='text' 
										name="editTitle" id="editTitle"
										class="form-control" />
									
								</div>
							</div>
							<div class="row pad_top mar_bottom" id="edit_CustomSectionDiv">
								
								<div class="col-md-12" >
									<label>Custom Item Landing Url</label><span class="mandatory_star">*</span> <input type='text' 
										name="editlandingUrl" id="editlandingUrl"
										class="form-control" />
									
								</div>
							</div>
						</div>
					</div>
					<div class="row pad_top" >
                    			<div class="col-md-6 user_details" id="edit_prtCatDiv">
                    			<label>Partner Category</label>
								<fieldset class="infield">
									<select name="speed" id="edit_prtCatNo" class="select_option">
										<option value="">-Partner Category-</option>
									</select>
									<input type="hidden" name="edit_prtCatNo_hidden" id="edit_prtCatNo_hidden" />
								</fieldset>
                    		</div>
                    		
                    		<div class="col-md-6 user_details" id="edit_prdtDiv">
								<label>Select Product</label>
									<input type='text' name="edit_productName" id="edit_productName" class="form-control"/>
									<input type="hidden" id="edit_prtPrdctNo" name="edit_prtPrdctNo" />
									<input type="hidden" name="edit_prtPrdctNo_hidden" id="edit_prtPrdctNo_hidden" />
                    		</div>
                    		
                    		<div class="col-md-6 user_details" id="edit_offerDiv">
                    			<label>Select Offers</label>
								<input type='text' name="edit_offerTitle" id="edit_offerTitle" class="form-control"/>
								<input type="hidden" id="edit_prtOfferNo" name="edit_prtOfferNo" />
								<input type="hidden" name="edit_prtOfferNo_hidden" id="edit_prtOfferNo_hidden" />
                    		</div>
                    		</div><!----row--->
                    		
                    		
                    		<div class="row pad_top" >
                    		
                    		<div class="col-md-4 user_details">
								<label>Display Order</label><span class="mandatory_star">*</span>
								<input type='text' name="edit_displayOrder"  id="edit_displayOrder" class="form-control" maxlength="3"/>
                           		<input type="hidden" name="edit_displayOrder_hidden" id="edit_displayOrder_hidden" />
							</div>
                    		<div class="col-md-4 user_details">
								<label>Activation Date</label>
								<div class="pick_date">
									<input type="text" class="form-control startDate" id="datepicker13" name="datepicker13"  />
								</div>
							</div>
							<div class="col-md-4 user_details">
								<label>Deactivation Date</label>
								<div class="pick_date">
									<input type="text" class="form-control endDate" id="datepicker14" name="datepicker14" />
									<!--img src="./img/2016-06-23.png" alt="" ></img-->
								</div>
							</div>
                    		</div>
                    		<div class="row pad_top" id="edit_CatList">
	                            <div class="col-md-6">
	                          	  <label>Category Name</label>									
									<input type="text" class="form-control" name="categoryNo_" id="categoryName_details" readonly />	
									<input type="hidden" class="form-control" name="catgry_no" id="catgry_no" />	
									<input type="hidden" name="catgry_no_hidden" id="catgry_no_hidden">
								
									<a href="javascript:void(0);" class="selectCategory selectCategoryLink" id="edit_showCatList">Click here to View Categories</a>
							
	                            </div>
	                            	<div class="col-md-6 category_list_divDetails" id="edit_selectCatListDiv">
											<strong>Category List</strong>
											<div class="category_list_details">
											</div>
										</div>
									
                       		 </div><!----row--->
                       		 
                       		 <div class="row btn_save">
								<input class="bg_g_button" id="cancelButton" type="button" value="Cancel" />
								<input class="bg_g_button" type="button" id="updateSectionItems" value="Update" />
						</div><!-------------button----------->
                    	</div>
                	</div>
				</div>	
            	
            </form:form>
        </div>
 
 
 
 
		<!-- Modal  for edit-details--> 
        <div id="myModal1" class="modal fade" role="dialog">
       <form:form autocomplete="off"  id="homepageSectionBean"	commandName="homepageSectionBean">
					<input type="hidden" id="section_Type" value="1"/>
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Edit Home Page Section Details</h4>
                    </div><!----modal-header--->
                    <div class="modal-body1">
                        <div class="row pad_top" >
                            <div class="col-md-4 user_details">
                                <label>Section Title</label>
                                <input type="text" minlength="3" maxlength="50"
												class="form-control " name="section_title" id="section_title">
								<input type="hidden" id="hidden_sectiontitle"/>				
                            </div>
                            <div class="col-md-4 user_details">
                                <label>Display Order</label>
                                <input type="text" minlength="1" maxlength="3"
												class="form-control " name="display_order" id="display_order">
								<input type="hidden" id="hidden_displayorder"/>
                            </div> 
                            <div class="col-md-4 user_details">
								<input type="checkbox" class="allow_checkbox" name="viewLink" id="viewLink"/>
								<label>Enable View All Link</label>
							</div>
                            <div class="col-md-4 hideStatus">
								<div class='text top' >
									<label>Section Status</label>
									<div class="half-col last">
										<ul class="list-inline">
				
											<c:forEach items="${SectionStatus}" var="status">
														<li><label> 														
														<input type="radio"	name="section_status" class="css-checkbox" id="section_status_${status.key}" value="${status.key}"  />
																<label for="section_status_${status.key}" class="css-label radGroup2">${status.value}</label>														 																
														</label></li>
											</c:forEach>
										</ul>
									</div>
								</div>
							</div> 
                        </div><!----row--->
                        <div class="row pad_top">
							<div class="col-md-4">
								<label>Activation Date</label>
								<div class="pick_date">
									<input type="text" class="form-control startDate" id="datepicker11" name="datepicker11"  />
								</div>
							</div>
							<div class="col-md-4">
								<label>Deactivation Date</label>
								<div class="pick_date">
									<input type="text" class="form-control endDate" id="datepicker12" name="datepicker12" />
									<!--img src="./img/2016-06-23.png" alt="" ></img-->
								</div>
							</div>
							
							<div class="col-md-4">
								<label>Select the Redirection Page</label>
								<select name="redirectionPage" id="redirectionPage" class="form-control"  disabled="disabled">
									<option value="">Select Page</option>
									<option value="offerDeal">Offers & Deal Page</option>
  									<option value="onlinePartner">Online Partner Page</option>
  									<option value="offlinePartner">Offline Partner Page</option>
							</select>
							</div>
                        </div><!----row--->
                        <!--img1  -->
                        <div class="row pad_top">
                        <div class="col-md-6">
									<label>Footer Image 1</label><br>
									<div class="fileUpload btn">
									<input id="footerImageUrl1" class="upload_file upload_file_style" placeholder="Choose File" />
										<span class="bg_w_button edit_partner_browse">Browse</span>
										<input name="footerImage1"  id="footerImage1" type="file" class="upload upload_btn_modal editPartnerImageUpload bwrsBtnPop"/>
									</div>
									<div class="image_wrap image_wrap1" style="width:100px; height:100px;">
								 	<img id="footerImage1_"  src="" style="max-height: 100%; max-width: 100%;">
								</div>
								
							<div class="mar_top uploadFile_div">
								<input class="bg_g_button"  onclick="UploadFooterImage('footerImage1','partner','footerImageUrl1')" type="button" value="Upload" />
								<input type="hidden" name="footerImageUrl1Hidden" id="footerImageUrl1Hidden">	
								<div id="editFooterError1" class="customErrorMessageDisp error"></div>
							
							</div>	
						 	</div>
                        <!--  -->
                        <div class="col-md-6">
									<label>Footer Image 2</label><br>
									<div class="fileUpload btn">
									<input id="footerImageUrl2" class="upload_file upload_file_style" placeholder="Choose File"  />
										<span class="bg_w_button edit_partner_browse">Browse</span>
										<input name="footerImage2"  id="footerImage2" type="file" class="upload upload_btn_modal editPartnerImageUpload bwrsBtnPop"/>
									</div>
									<div class="image_wrap image_wrap1" style="width:100px; height:100px;">
								 	<img id="footerImage2_"  src="" style="max-height: 100%; max-width: 100%;">
								</div>
								
							<div class="mar_top uploadFile_div">
								<input class="bg_g_button"  onclick="UploadFooterImage2('footerImage2','partner','footerImageUrl2')" type="button" value="Upload" />
								<input type="hidden" name="footerImageUrl2Hidden" id="footerImageUrl2Hidden">	
								<div id="editFooterError2" class="customErrorMessageDisp error"></div>
							
							</div>	
						 	</div>
						 </div>
						 	<!--  -->
                            <div class="row pad_top">
                        <div class='text_editor'>
						<label>Footer Description</label>
						<textarea id="footerDesc" name="footerDesc" cols="40" class="text"></textarea>
						<p id="msg2" style="display:none;color:#FF4000;"></p>	
						</div>
						</div>
						<div class="row pad_top">
						<div class="row btn_save">
							<a href="${contextPath}/admin/homePageSections?sectionType=1"><input class="bg_g_button" type="button" value="Cancel" /></a>
							<input class="bg_g_button" type="button" id="updateSection" value="Update" />
						</div><!-------------button----------->
                    </div></div>
                </div>
            </div>
            </form:form>
        </div>
        
        
        
        <div class='container'>
            <section class="affiliate_management">
                <div id ="breadcrumb">
                    <ul>
                        <li>Home</li>
						<li>Content Management</li>
						<li>Home Page Custom Sections</li>
                    </ul>
                </div>  <!---breadcrumb--->
                
               <c:if test="${fn:length(message) gt 0}">
	              <script>changeURL();</script>
	                <div class="alertMessage" id="divChanged" style="color:green;font-weight: bold;"  ><c:out value="${message}"></c:out></div> </c:if>
	  			<c:if test="${fn:length(errormessage) gt 0}">
	  			   <script>changeURL();</script>
	       		<div class="alertMessage" style="color:red;font-weight: bold;"  ><c:out value="${errormessage}"></c:out></div> </c:if>
                <div class="section_heading">
               
					<h5>Home Page Sections</h5> 
				</div><!--- section heading ---->
				<div class="management"><!---User Management--->
                    <ul class="nav nav-tabs">
                        <li id="viewSectionTab"  class="active"><a data-toggle="tab" href="#sectionA">Home Page Sections Listing</a></li>
                        <li id="viewSectionItemTab" ><a data-toggle="tab" href="#sectionB">Section Wise Listing</a></li>
                        <li id="createItemTab" ><a data-toggle="tab" href="#sectionC">Create Section Items</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="sectionA" class="tab-pane fade in active">
                          
                            <table class=" partner_info_table table-bordered" cellspacing="0" cellpadding="0" width="98%" id="table-1">
								
								<thead>
								
									<tr>
										<th class="name">Section Title</th>
										<th>Display Order</th>
										<th>Status</th>
										<th class="width_section">Action</th>
									</tr>
								</thead><!----thead--->
								<tbody>
								 	<c:forEach items="${sectionList}" var="section" varStatus="i">
									<tr class="admin" id="1.${section.sectionId}"
									 data-id="${section.sectionId}"
									 data-sectiontitle="${section.sectionTitle}"
									 data-displayorder="${section.displayOrder}"
									 data-status="${section.status}"
									 data-sectionstatuscode="${section.sectionStatusCode}"
									 data-statuscodeclass="${section.statusCodeClass}"
									 data-activationdate="${section.activationDate}" 
									 data-deactivationdate="${section.deactivationDate}"
									 data-footerimageurl1="${section.footerImageUrl1}"
									 data-footerimageurl2="${section.footerImageUrl2}"
									 data-footerdesc="${section.footerDesc}"
									 data-viewlink="${section.viewLink}"
									 data-redirectionpage="${section.redirectionPage}">
									<td class="td_width">${section.sectionTitle}</td>
										<td class="td_width">${section.displayOrder}</td>
										<td class="${section.statusCodeClass} td_width">${section.status}</td>
										<td class="td_width">
											<span href="javascript:void(0);" class="editsection"  data-target="#myModal1">
												<img src="${contextPath}/static/img/edit_icon.png" class="edit_wraper" ></img>
											</span>
										</td>
									</tr> 
								 	</c:forEach>							
								</tbody><!---tbody--->
								 
							</table><!---table-->	
                        </div><!----section A---->
                        
                        <div id="sectionB" class="tab-pane fade submenu pad_bottom mar_bottom" >
                        
                        <div class="col-md-4 pad_top">
								<form:form autocomplete="off" method="post" id="sectionBean" action="homePageSections" commandName="sectionBean" >			
									<label>Select Section Name</label>
									<fieldset class="infield">
										
										<form:select name="" id="section_id" path="sectionId" class="select_option">
											<form:option value="">---Select---</form:option>
											<c:forEach items="${sectionList}" var="sectionItem">
		           					     	<li>
		           					     	<c:if test="${sectionItem.sectionTitle ne 'Popular Categories'}">
		           					      		<form:option value="${sectionItem.sectionId}" label="${sectionItem.sectionTitle}" />
		           					      	</c:if>
		           							</li>
		           							</c:forEach> 
		           							 
 										</form:select>
 										</fieldset>
									
 								 	</form:form>
 									
 									<br/>
 									<c:if test="${fn:length(sectionItemMessage) gt 0}"><div class="alertMessage" style="color:green;font-weight: bold;"  ><c:out value="${sectionItemMessage}"></c:out></div> </c:if>
								</div>
								<%-- <c:if test="${not empty sectionList}">
                            	<nav class="navbar navbar-default" role="navigation" id="submenu"> --%>
								 <table class=" partner_info_table table-bordered " cellspacing="0" cellpadding="0" width="98%" id="table-1">
								
								<thead>
								
									<tr>
										<th class="name">Sl.No</th>
										<th>Title</th>
										<th>Type</th>
										<th>Display Order</th>
										<th>Status</th>
										<th class="width_section">Action</th>
									</tr>
								</thead><!----thead--->
								<tbody>
								 	<c:forEach items="${sectionItemList}" var="sectionItem" varStatus="i">
									<tr class="admin" id="1.${sectionItem.sectionId}"
									data-itemid="${sectionItem.itemId}"
									 data-id="${sectionItem.sectionId}"
									 data-sectionid="${sectionItem.sectionId}"
									 data-itemtype="${sectionItem.itemType}"
									 data-prtno="${sectionItem.prtNo}"
									 data-prtcatno="${sectionItem.prtCatNo}"
									 data-prtcatname="${sectionItem.prtCatName}"
									 data-prtprdno="${sectionItem.prtPrdctNo}"
									 data-productname="${sectionItem.productName}"
									 data-catno="${sectionItem.catNo}"
									 data-catname="${sectionItem.catName}"
									 data-prtoffrno="${sectionItem.prtOfferNo}"
									 data-prtoffrname="${sectionItem.prtOfferName}"
									 data-dispord="${sectionItem.displayOrder}"
									 data-startdate="${sectionItem.startDate}"
									 data-enddate="${sectionItem.endDate}"
									 data-status="${sectionItem.status}"
									 data-sectionstatuscode="${sectionItem.sectionItemStatus}"
									 data-statuscodeclass="${sectionItem.statusCodeClass}"
									 data-title="${sectionItem.title}"
									 data-brandname="${sectionItem.brandName}"
									 data-brandimageurl="${sectionItem.brandImageUrl}"
									 data-branddescription="${sectionItem.brandDescription}"
									 data-brandnamehidden="${sectionItem.brandNameHidden}"
									 data-title="${sectionItem.title}"
									 data-landingurl="${sectionItem.landingUrl}"
									 data-displayprtpopup="${sectionItem.displayPrtPopup}">
										<td class="td_width">${i.count}</td>
										<td class="td_width">${sectionItem.title}</td>		
										<td class="td_width">${sectionItem.typeName}</td>
										<td class="td_width">${sectionItem.displayOrder}</td>
										<td class="${sectionItem.statusCodeClass} td_width">${sectionItem.status}</td>
										<td class="td_width">
											<span  href="javascript:void(0);" class="editsectionitem" data-target="#myModal2">
												<img src="${contextPath}/static/img/edit_icon.png" class="edit_wraper" ></img>
											</span>
											<span class="delete btnDelete"   data-target="#myModala" id="deleteItems">
												<img src="${contextPath}/static/img/delete_icon1.png"  class="fa fa-trash fa-lg"></img>
											</span>
										</td>
									</tr> 
								 	</c:forEach>							
								</tbody><!---tbody--->
								 
							</table><!---table-->	
                        	<%-- </nav>
                             </c:if> --%>
                        </div><!----section B---->
                        
                        
                        <!----section C starts---->
                          <div id="sectionC" class="tab-pane fade in ">
                     <!---- partner category starts here ---->
                     <nav class="navbar navbar-default" role="navigation" id="submenu">
                        <h5 class="table_heading">Create Section Entities</h5>
                        <!---- table header ---->
                        <form:form autocomplete="off" id="createSectionItems" name="createSectionItems" action="createSectionItems" commandName="sectionItems">
                        <form:input type="hidden" path="getCategoryList" id="getCategoryList" value="${requestScope.categoriesList}" />
                        
                        <div class="row pad_top">
                        <div style="margin:0px 0px 5px 15px">
                        	<form:input type="hidden" path="dupRecord" value="" id="dupRecord" name="dupRecord" />
                        </div>
                        	 
                        	<div class="col-md-4">
	                           	<label>Select A Section</label><span class="mandatory_star">*</span>
								<fieldset class="infield">
									<form:select path="sectionId" id="sectionId" class="select_option">          
										<form:option value="">Select Section</form:option>
		          	 					 <c:forEach items="${sectionList}" var="section">
		           					      <li>
		           					      <c:if test ="${section.sectionTitle ne 'Popular Categories'}">
		           					     <form:option value="${section.sectionId}" label="${section.sectionTitle}" />
		           					      </c:if>
		           						</li>
		           					</c:forEach> 
		           					 
									</form:select>
								</fieldset>	
                           </div>
                           
                           <div class="col-md-4">
	                           	<label>Item Type</label><span class="mandatory_star">*</span>
								<fieldset class="infield">
									<form:select path="itemType" id="itemType" class="select_option">          
										<form:option value="">Select Item Type</form:option>
		          	 					 <c:forEach items="${itemType}" var="item">
		           					      <li>
		           					      <form:option data-itemvalue="${item.value}" value="${item.key}" label="${item.value}" />
		           						</li>
		           					</c:forEach> 
									</form:select>
								</fieldset>	
                           </div>
                           
                           <div class="col-md-4" id="prtNoDiv">
	                           	<label>Select A Partner</label><span class="mandatory_star" id="mandatory_custom">*</span>
								<fieldset class="infield">
									<form:select path="prtNo" id="prtNo" class="select_option">          
										<form:option value="">Select Partner</form:option>
		          	 					 <c:forEach items="${partnerList}" var="partner">
		           					      <li>
		           					      <form:option value="${partner.prtNo}" label="${partner.partnerName}" />
		           						</li>
		           					</c:forEach> 
									</form:select>
								</fieldset>	
                           </div>
                           <!--brand type  -->
                           <div class="col-md-4" id="brandNameDiv">
		                            <label>Select Brand</label> <span class="mandatory_star">*</span> 
									<input type='text' name="brandName" id="brandName" class="form-control"/>
									<form:hidden path="brandNameHidden" id="brandNameHidden"/>	      
	                           </div>
	                           
                        </div>   <!--- row - -->
                        
                        <div class="row pad_top">
                        <div id="brandImageDiv">
                        <div class="row pad_top pad_bottom">
												<div class="col-md-4">
													<label>Brand Image</label><span class="mandatory_star">*</span><br/>
													<input id="uploadFile" class="upload_file" placeholder="Choose File" disabled="disabled" />
													<div class="fileUpload btn padTop_Btm">
														<span class="bg_w_button">Browse</span>
														<!-- TODO: Banner image placement -->
														<input id="brandImage" name="brandImage" type="file" class="upload newBrandImageUpload bwrsBtnAddPar"  />
													</div>
												</div><!---- browse button --->
				                                <div class="col-md-4 image_wrap image_wrap1 noMar-top">
				                                     <img id="brandImage_" src="" style="max-height: 100%; max-width: 100%;"/>
				                                </div>
												<div class="col-md-2">
													<input class="bg_g_button" type="button" value="Upload" onclick="uploadImage('brandImage','brand','brandImageUrl')"/>
													<form:hidden path="brandImageUrl" id="brandImageUrl"/>
												<div id="icon_error3" class="error"></div>
												</div>	
												 
											</div>
											<div class="col-md-4" >
		                            <label>Description</label> 
									<input type='text' name="brandDescription" id="brandDescription"  class="form-control"/>
	                           </div>	
                        </div>
                        
                         <div id="customPageItemDiv">
                        <div class="row pad_top pad_bottom">
												<div class="col-md-4">
													<label>Custom Page Image</label><span class="mandatory_star">*</span><br/>
													<input id="uploadFile1" class="upload_file" placeholder="Choose File" disabled="disabled" style="width:275px" />
													<div class="fileUpload btn padTop_Btm">
														<span class="bg_w_button">Browse</span>
														<!-- TODO: Banner image placement -->
														<input id="brandImage1" name="brandImage1" type="file" class="upload newBrandImageUpload bwrsBtnAddPar"  />
													</div>
												</div><!---- browse button --->
				                                <div class="col-md-4 image_wrap image_wrap1 noMar-top">
				                                     <img id="brandImage_1" src="" style="max-height: 100%; max-width: 100%;"/>
				                                </div>
												<div class="col-md-2">
													<input class="bg_g_button" type="button" value="Upload" onclick="uploadImage1('brandImage1','brand1','brandImageUrl1')"/>
													<form:hidden path="brandImageUrlCustom" id="brandImageUrl1" />
													<div id="icon_error1" class="error"></div>
												</div>	
												<div class="col-md-2">
													<form:checkbox path="displayPrtPopup" id="displayPrtPopup" name="displayPrtPopup" value="0"  />
													<label>Display Partner Popup</label>
												</div>
											</div>
											
											<div class="col-md-4">
		                            <label>Custom Item Title</label><span class="mandatory_star">*</span> 
									<input type='text' name="title" id="title"  class="form-control"/>
									
									<label>Custom Item Landing URL</label><span class="mandatory_star">*</span> 
									<input type='text' name="landingUrl" id="landingUrl"  class="form-control"/>
									</div>
									
									<div class="col-md-4">
									<label>Custom Item Text</label> 
									<input type='text' name="customDescription" id="customDescription"  class="form-control"/>
	                           </div>	
                        </div>
                        	<div class="col-md-4" id="categorySelectedDiv">
									<label>Select Category</label><span class="mandatory_star">*</span>
									<input type="text" class="form-control" id="categorySelected" readonly>	
									<form:hidden path="catNo" id="catNo"/>	
									<a href="javascript:void(0);" class="selectCategory"  data-toggle="modal" data-target="#categoriesModal">Click here to View Categories</a>
							</div>
							
                        	<div id="prtCatNoDiv">
	                        	<div class="col-md-4">
									<label>Partner Category</label> <span class="mandatory_star">*</span> 
									<fieldset class="infield">
										<form:select name="speed" path="prtCatNo" id="prtCatNo"	class="select_option">
											<form:option value="">-Partner Category-</form:option>
										</form:select>
									</fieldset>
								</div>
								
								 
	                           <div class="col-md-4">
		                            <label>Select Product</label> <span class="mandatory_star">*</span> 
									<input type='text' name="productName" id="productName" class="form-control"/>
									<form:hidden path="prtPrdctNo" id="prtPrdctNo"/>	      
	                           </div>
                           </div>
                           
                           <div class="col-md-4" id="offerTitleDiv">
	                            <label>Select Offers</label> <span class="mandatory_star">*</span> 
								<input type='text' name="offerTitle" id="offerTitle" class="form-control"/>
								<form:hidden path="prtOfferNo" id="prtOfferNo"/>	      
                           </div>
                           
                           <div class="col-md-4">
									<div class='text top' >
										<label>Section Item Status</label>
										<div class="half-col last">
											<ul class="list-inline">
												<c:forEach items="${sectionItemStatus}" var="status">
													<li>
														<label>
															<form:radiobutton path="sectionItemStatus" class="css-checkbox sectionItemStatus" id ="${status.key}" value="${status.key}" />
															<label for="${status.key}" class="css-label radGroup2">${status.value}</label>
														</label>
													</li>
												</c:forEach>
											</ul>
										</div>
									</div>
								</div>
                        </div> <!--- row - -->
                        
                        <div class="row pad_top pad_bottom">
                           <div class="col-md-4">
                           <label>Display Order</label><span class="mandatory_star">*</span>
							<form:input type='text' name="displayOrder"  id="displayOrder" path="displayOrder" class="form-control" maxlength="3"/>
                           </div>
                           <div class="col-md-4">
                           		<label>Start Date</label><span class="mandatory_star">*</span>
								<div class="pick_date">
									<form:input class="form-control startDate" id='datepicker1' path="startDate" readonly="true" />
								</div>
                        	</div>
                        <div class="col-md-4">
                        		<label>End Date</label><!-- <span class="mandatory_star">*</span> -->
								<div class="pick_date">
									<form:input class="form-control endDate" id='datepicker2' path="endDate" readonly="true" />
								</div>
                        </div>
                       </div> <!--- row - -->
                       
                       <div class="row btn_save pull-right">
							<input class="bg_g_button" type="button" id="cancelbtnD_sectionItem" value="Cancel" />
							<button type="submit" class="bg_g_button">Save</button>
						</div>
                        </form:form>
                     </nav>
                  </div>
                          
                           
                                                               
                    </div><!---- tab-content --->
                </div>	<!---- user managemnt ---->	
            </section><!---- Section ---->	
            	
        </div>
<script>
var sectionTitleExist=false;
var displayOrder=false;
var sectionItmDispOrd = false;
var duplicateRecord = false;
var edit_duplicateRecord = false;
var uploadFlag=false;
var imageUploadFlag=false;

$.validator.addMethod('imageUpload', function(value, element) {
	return imageUploadFlag; 
});
$('#brandImage').on('change', function() {
	 $("#createSectionItems").validate().element('#brandImage');
		  
	});
$('#brandImage1').on('change', function() {
	 $("#createSectionItems").validate().element('#brandImage1');
		  
	});

$(function () {
    $("#brandImage").bind("change", function () {    	
        var fileUpload = $("#brandImage")[0];    	
        checkFileUpload(fileUpload);
        if(!uploadFlag) return false;
    });
});

$(function () {
    $("#brandImage1").bind("change", function () {    	
        var fileUpload = $("#brandImage1")[0];    	
        checkFileUpload1(fileUpload);
        if(!uploadFlag) return false;
    });
});
$('#brandImage').on('change', function() {
	$('#uploadFile').val($(this).val());
});
$('#brandImage1').on('change', function() {
	$('#uploadFile1').val($(this).val());
});
 $(function () {
    $("#editBrandImage").bind("change", function () {    	
    	//Get reference of FileUpload.
    	$("#editBrandImageUrl").val($(this).val());
		$("#editSectionItems").validate().element("#editBrandImage");
		$('#edit_brandImageUrlHidden').val('');
    	imageUploadFlag=false;
        var fileUpload = $("#editBrandImage")[0];
        checkFileUpload(fileUpload);
        if(!uploadFlag) return false;
    });
}); 
 
 /* custom item type */
 
  $(function () {
    $("#editBrandImage1").bind("change", function () {   
    	//Get reference of FileUpload.
    	$("#editBrandImageUrl1").val($(this).val());
		$("#editSectionItems").validate().element("#editBrandImage1");
		$('#edit_brandImageUrlHidden1').val('');
    	imageUploadFlag=false;
        var fileUpload = $("#editBrandImage1")[0];
        checkFileUpload1(fileUpload);
        if(!uploadFlag) return false;
    });
}); 
/*brand upload image  */
 function uploadImage(id,type,url){
			  var fileUpload = $("#"+id)[0];
		        checkFileUpload(fileUpload,"brand");
		        $("#createSectionItems").validate().element("#"+id)
		        
		        if(!uploadFlag) return false;			        		       
		    	var formData = new FormData();
		    	formData.append('file',$("#"+id)[0].files[0]);	
		    	if($("#createSectionItems").validate().element("#"+id)){		
		    		$.ajax({
		    			 url: "${contextPath}/admin/updatepartnerLogo?type="+type,
		    			  data: formData,
		    			  dataType: 'text',
		    			  processData: false,
		    			  contentType: false,
		    			  type: 'POST',
		    			  success: function (response) {		    				  
		    			 $('#uploadFile').val(response);			 
		    			 var newImageUrl='${contextPath}'+'/admin/images/'+type+"/"+response;
		    			 $('#brandImage_').prop('src',newImageUrl);
		    			 $("#createSectionItems").validate().element("#"+url);
		    			 $("#brandImageUrl").val(response);
		   
		    			 $("#createSectionItems").validate().element('#brandImage');
		    			 $("#createSectionItems").validate().element("#brandImageUrl");
		   			  }
		    			 });		
		    	} 
		    	
		    	}
		    	
		    	/* custom sectionType */
		    	function uploadImage1(id,type,url){
			  var fileUpload = $("#"+id)[0];
		        checkFileUpload(fileUpload,"brand2");
		        $("#createSectionItems").validate().element("#"+id)
		        
		        if(!uploadFlag) return false;			        		       
		    	var formData = new FormData();
		    	formData.append('file',$("#"+id)[0].files[0]);	
		    	if($("#createSectionItems").validate().element("#"+id)){		
		    		$.ajax({
		    			 url: "${contextPath}/admin/updatepartnerLogo?type="+type,
		    			  data: formData,
		    			  dataType: 'text',
		    			  processData: false,
		    			  contentType: false,
		    			  type: 'POST',
		    			  success: function (response) {		    				  
		    			 $('#uploadFile1').val(response);			 
		    			 var newImageUrl='${contextPath}'+'/admin/images/'+type+"/"+response;
		    			 $('#brandImage_1').prop('src',newImageUrl);
		    			 $("#createSectionItems").validate().element("#"+url);
		    			 $("#brandImageUrl1").val(response);
		   
		    			 $("#createSectionItems").validate().element('#brandImage1');
		    			 $("#createSectionItems").validate().element("#brandImageUrl1");
		   			  }
		    			 });		
		    	} 
		    	
		    	}
		    	
 function checkFileUpload(fileUpload,type,error){
		//Check whether the file is valid Image.
	    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:\().+(\)\.def\().+(\])+(.jpg|.jpeg|.png|.gif)$");
	    if (regex.test(fileUpload.value.toLowerCase())) {
	        //Check whether HTML5 is supported.
	        if (typeof (fileUpload.files) != "undefined") {
	            //Initiate the FileReader object.
	            var reader = new FileReader();
	            //Read the contents of Image File.
	            reader.readAsDataURL(fileUpload.files[0]);
	            reader.onload = function (e) {
	                //Initiate the JavaScript Image object.
	                var image = new Image();
	                //Set the Base64 string return from FileReader as source.
	                image.src = e.target.result;
	                image.onload = function () {
	                    //Determine the Height and Width.
	                    var height = this.height;
	                    var width = this.width;  
	                    
	        			var errorText = "";
						uploadFlag = false;
						
							if(height ==140 && width == 127){
								uploadFlag = true;
							}
							else{
								uploadFlag = false;
								errorText = "Image Icon should not be more than 140*127";
							}
							if(type=="brand1"){
								$("#icon_error").css({"display":""});
							if(errorText != ""){
								$("#icon_error").text(errorText);
							}else{
								$("#icon_error").css({"display":"none"});
							}
							}
							if(type=="brand2"){
								$("#icon_error1").css({"display":""});
							if(errorText != ""){
								$("#icon_error1").text(errorText);
							}else{
								$("#icon_error1").css({"display":"none"});
							}
							}
							
							if(type=="Brand"){
								$("#icon_error2").css({"display":""});
							if(errorText != ""){
								$("#icon_error2").text(errorText);
							}else{
								$("#icon_error2").css({"display":"none"});
							}
							}
							
							if(type=="brand"){
								$("#icon_error2").css({"display":""});
							if(errorText != ""){
								$("#icon_error2").text(errorText);
							}else{
								$("#icon_error2").css({"display":"none"});
							}
							}
	                };
	            }
	        } else {
	           
	            uploadFlag = false;
	        }
	    } else {
	      
	        uploadFlag = false;
	    }
	}
		    	
		    	
		    	function checkFileUpload1(fileUpload,type,error){
		    		//Check whether the file is valid Image.
		    	    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:\().+(\)\.def\().+(\])+(.jpg|.jpeg|.png|.gif)$");
		    	    if (regex.test(fileUpload.value.toLowerCase())) {
		    	        //Check whether HTML5 is supported.
		    	        if (typeof (fileUpload.files) != "undefined") {
		    	            //Initiate the FileReader object.
		    	            var reader = new FileReader();
		    	            //Read the contents of Image File.
		    	            reader.readAsDataURL(fileUpload.files[0]);
		    	            reader.onload = function (e) {
		    	                //Initiate the JavaScript Image object.
		    	                var image = new Image();
		    	                //Set the Base64 string return from FileReader as source.
		    	                image.src = e.target.result;
		    	                image.onload = function () {
		    	                    //Determine the Height and Width.
		    	                    var height = this.height;
		    	                    var width = this.width;                     

		    	        			var errorText = "";
		    						uploadFlag = false;
		    						
		    							if(width == height && width <= 32){
		    								uploadFlag = true;
		    							}
		    							else{
		    								uploadFlag = false;
		    								errorText = "Partner Icon should not be more than 32*32";
		    							}
		    							if(errorText != ""){
		    								$("#icon_error").text(errorText);
		    							}else{
		    								$("#icon_error").css({"display":"none"});
		    							}        
		    	                    uploadFlag = true;                                       
		    	                };
		    	            }
		    	        } else {
		    	           
		    	            uploadFlag = false;
		    	        }
		    	    } else {
		    	      
		    	        uploadFlag = false;
		    	    }
		    	}

$('span.editsection').on('click', function () {
	 
	var id = $(this).closest('tr').data('id');
	var sectionTitle = $(this).closest('tr').data('sectiontitle');
	var displayOrder = $(this).closest('tr').data('displayorder');
	var status= $(this).closest('tr').data('status');
	var sectionStatusCode= $(this).closest('tr').data('sectionstatuscode');
	var activationDate= $(this).closest('tr').data('activationdate');
	var deactivationDate= $(this).closest('tr').data('deactivationdate');
	var footerDesc=$(this).closest('tr').data('footerdesc');
	footerDesc = decodeURIComponent(footerDesc).split('+').join(" ");
	var footerImage1=$(this).closest('tr').data('footerimageurl1');
	var footerImage2=$(this).closest('tr').data('footerimageurl2');
	var imageUrl1='${contextPath}'+'/admin/images/partner/'+footerImage1;
	var imageUrl2='${contextPath}'+'/admin/images/partner/'+footerImage2;
	var viewLink=$(this).closest('tr').data('viewlink');
	var redirectionPage=$(this).closest('tr').data('redirectionpage');
	if(footerImage1 ==''){
		imageUrl1='';
	}
	if(footerImage2 ==''){
		imageUrl2='';
	}
	$("#section_title").val(sectionTitle);
	$('#display_order').val(displayOrder);
	$('#section_status_'+sectionStatusCode).prop("checked","checked");
	$('#datepicker11').val(activationDate);
	$('#datepicker12').val(deactivationDate);	
	$("#hidden_displayorder").val(displayOrder);
	$("#hidden_sectiontitle").val(sectionTitle);
	$('#footerDesc').val(footerDesc);
	$('#footerImageUrl1').val(footerImage1);
	$('#footerImageUrl2').val(footerImage2);
	$('#footerImage1_').prop('src',imageUrl1); 
	$('#footerImage2_').prop('src',imageUrl2); 
	$('#redirectionPage').val(redirectionPage);
	
	if(viewLink == 1){
		$("#redirectionPage").removeAttr("disabled");
		$('#viewLink').prop('checked',true); 
	}
	$('#myModal1').data('id',id).modal('show'); 
	$(".hideStatus").hide();
	tinymce.init({
		   selector: 'textarea#footerDesc',
		   height: '100px',
		   plugins: 'link',
		   autoresize_bottom_margin: 100,
		   max_height: '50px',
		   toolbar_items_size : 'small',
		   /* file_browser_callback: function(field_name, url, type, win) {
	            if(type=='image') $('#my_form input').click();
	        }, */
		    force_br_newlines : true,
		    force_p_newlines : false,
		    forced_root_block : '',
		    plugins: [
		    	'advlist autolink lists link image charmap print preview hr anchor pagebreak',
		    	'searchreplace wordcount visualblocks visualchars code fullscreen',
		    	'insertdatetime media nonbreaking save table contextmenu directionality',
		    	'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
		    	],
		    setup : function(ed){
		   	ed.on("NodeChange", function(e){
		           jQuery("textarea[id='footerDesc']").val(ed.getContent());
			          // if($("#editLogoPartnerBean").validate().element('#edit_footerDesc'));
			           $(document).on('focusin', function(e) {
			        	    if ($(e.target).closest(".mce-window").length) {
			        	        e.stopImmediatePropagation();
			        	    }
			        	});
		       });
		  }
		}); 
});

$('#updateSection').click(function () {
	//alert($("#footerDesc").val().length);
	var id = $('#myModal1').data('id');
	var sectionType = $("#section_Type").val();
	var json={};
	json["sectionId"]=$('#myModal1').data('id');
	json["sectionTitle"]=$("#section_title").val();
	json["displayOrder"]=$("#display_order").val();
	json["sectionStatus"]=$('input[name=section_status]:checked').val();
	json["sectionStartDate"]=$("#datepicker11").val();
	json["sectionEndDate"]=$("#datepicker12").val();
	json["sectionType"]=$("#section_Type").val();
	json["footerDesc"]=$("#footerDesc").val();
	json["footerImageUrl1"]=$("#footerImageUrl1").val();
	json["footerImageUrl2"]=$("#footerImageUrl2").val();
	if($('input[name=viewLink]:checked').val() == "on"){
	json["viewLink"]=1;
	}else{
		json["viewLink"]=0;
	}
	json["redirectionPage"]=$("#redirectionPage").val();
	
	var formData = new FormData();
	formData.append('json',JSON.stringify(json));
	if($("#homepageSectionBean").validate().element('#section_title') 
			&& $("#homepageSectionBean").validate().element('#display_order')			
			&& $("#homepageSectionBean").validate().element('#datepicker11')
			&& $("#homepageSectionBean").validate().element('#datepicker12')
			&& $("#homepageSectionBean").validate().element('#footerDesc')
			&& $("#homepageSectionBean").validate().element("#footerImage1")
			&& $("#homepageSectionBean").validate().element("#footerImage2")
			&& $("#homepageSectionBean").validate().element("#viewLink")
			){		

	$.ajax({
			  url: "${contextPath}/admin/updateSection",
			  data: formData,
			  dataType: 'text',
			  processData: false,
			  contentType: false,
			  type: 'POST',
			  success: function (response) {
				 
					  $('#myModal1').modal('hide');
				  window.location.href="${contextPath}/admin/homePageSections?updateMessage="+response+"&sectionType="+sectionType;
			  },
			  error: function (response) {
				  $('#myModal1').modal('hide');
				  window.location.href="${contextPath}/admin/homePageSections?updateMessage=failure";
					
            	 }
			 });
	}
});

$('span.editsectionitem').on('click', function () {	
	
	var itemid = $(this).closest('tr').data('itemid');
	var id = $(this).closest('tr').data('id');
	var itemtype = $(this).closest('tr').data('itemtype');
	var prtno = $(this).closest('tr').data('prtno');
	var prtnohidden = $(this).closest('tr').data('edit_prtNo_hidden');
	var catnohidden =  $(this).closest('tr').data('catgry_no_hidden');
	
	var prtprdno=$(this).closest('tr').data('prtprdno');
	var productname = $(this).closest('tr').data('productname');
	var prtcatname=$(this).closest('tr').data('prtcatname');
	var prtcatno=$(this).closest('tr').data('prtcatno');
	var catno=$(this).closest('tr').data('catno');
	var catname=$(this).closest('tr').data('catname');
	
	var prtoffrno=$(this).closest('tr').data('prtoffrno');
	var prtoffrname=$(this).closest('tr').data('prtoffrname');
	
	var startdate=$(this).closest('tr').data('startdate');
	var enddate=$(this).closest('tr').data('enddate');
	var displayorder = $(this).closest('tr').data('dispord');
	var status= $(this).closest('tr').data('status');
	var sectionstatuscode= $(this).closest('tr').data('sectionstatuscode');
	var statuscodeclass= $(this).closest('tr').data('statuscodeclass');
	
	var brandname=$(this).closest('tr').data('brandname');
	var brandimageurl=$(this).closest('tr').data('brandimageurl');
	var imageUrl='${contextPath}'+'/admin/images/brand/'+brandimageurl;
	var brandDescription=$(this).closest('tr').data('branddescription');
	var brandNameHidden=$(this).closest('tr').data('brandnamehidden');
	var brandImageUrlHidden=$(this).closest('tr').data('brandimageurl');
    var title=$(this).closest('tr').data('title');
	var landingUrl=$(this).closest('tr').data('landingurl');
	var text=$(this).closest('tr').data('branddescription'); 
	var brandimageurl1=$(this).closest('tr').data('brandimageurl');
	var imageUrl1='${contextPath}'+'/admin/images/brand1/'+brandimageurl1;
	var brandImageUrlHidden1=$(this).closest('tr').data('brandimageurl');
	var displayPrtPopup =$(this).closest('tr').data('displayprtpopup');
	
	editHideDynamicFields();
	$("#edit_sectionId").val(id);
	$('#edit_itemType').val(itemtype);
	$('#edit_prtNo').val(prtno);
	$('#edit_prtNo_hidden').val(prtno);
	$('#categoryName_details').val(catname);
	$('#catgry_no').val(catno);
	$('#catgry_no_hidden').val(catno);
	$('#edit_offerTitle').val(prtoffrname);
	$('#edit_prtOfferNo').val(prtoffrno);
	$('#edit_prtOfferNo_hidden').val(prtoffrno);
	$('#edit_displayOrder').val(displayorder);
	$('#edit_displayOrder_hidden').val(displayorder);
	$('#datepicker13').val(startdate);
	$('#datepicker14').val(enddate);	
	$('#edit_sectionItemStatus_'+sectionstatuscode).prop("checked","checked");
	$( "#edit_sectionId" ).selectmenu( "refresh" );
	$( "#edit_itemType" ).selectmenu( "refresh" );
	$('#edit_prtCatNo_hidden').val(prtcatno);	
	$('#edit_prtPrdctNo_hidden').val(prtprdno);
	$('#editBrandName').val(brandname);
	$('#editBrandImageUrl').val(brandimageurl);
	$('#editBrandImage_').prop('src',imageUrl); 
	$('#editBrandDescription').val(brandDescription);
	$('#editBrandName_hidden').val(brandNameHidden);
	$('#edit_brandImageUrlHidden').val(brandImageUrlHidden);	
	
	$('#editText').val(text);
	$('#editTitle').val(title);
	$('#editlandingUrl').val(landingUrl);
	$('#editBrandImageUrl1').val(brandimageurl1);
	$('#editBrandImage_1').prop('src',imageUrl1);
	$('#edit_brandImageUrlHidden1').val(brandImageUrlHidden1);
	$('#edit_displayPrtPopup').val(displayPrtPopup);
	if(displayPrtPopup == "0"){
		$("#edit_displayPrtPopup").prop("checked", false);
	}
	else{
		$("#edit_displayPrtPopup").prop("checked", true);
	}
	
	if(brandimageurl != ""){
		imageUploadFlag=true;
	}
	//*****************
	var currentDate = $.datepicker.parseDate("dd/mm/yy", $.datepicker.formatDate("dd/mm/yy", new Date()));
	var startDate = $.datepicker.parseDate("dd/mm/yy",$.datepicker.formatDate("dd/mm/yy",getFormattedDate(startdate)));
	
	if (startDate <= currentDate) {
		$('#datepicker13').datepicker('disable');
	}
	
	if (getFormattedDate(enddate) != 'Invalid Date') {
		var endDate = $.datepicker.parseDate("dd/mm/yy",$.datepicker.formatDate("dd/mm/yy",getFormattedDate(enddate)));
		if (endDate <= currentDate) {
			$('#datepicker14').attr('readonly', true).datepicker('disable');
		}
	}
	
	
	//******************
	showDynamicFields();
	
	$( "#edit_prtNo" ).selectmenu( "refresh" );
	$("#edit_prtNo").trigger("selectmenuchange", ["refresh", prtcatno, prtprdno, productname]);
	
	$('#myModal2').data('id',itemid).modal('show');
	var itemType = $("#edit_itemType :selected").data('itemvalue');
	edit_changeValidationRule(itemType);
	//$('#editSectionItems').valid();
	$("#editSectionItems").validate().resetForm();
});
//closeCross
/* $("#closeCross").on('click', function(){
	window.location.href="${contextPath}/admin/homePageSections?sectionType=1";
});
 */
	$("#edit_prtNo").on("selectmenuchange", function() {
		if ($("#edit_prtNo_hidden").val() != $("#edit_prtNo").val()) {
		$('#edit_offerTitle').val('');
		$('#edit_prtOfferNo').val('');
		$('#catgry_no').val('');
		$('#categoryName_details').val('');
			}
		});
		$("#catgry_no").on("change",function() {
		if (catno != $("#catgry_no").val() && catnohidden != $("#catgry_no_hidden").val()) {
		$('#edit_offerTitle').val('');
		$('#edit_prtOfferNo').val('');
		}
		});

function showDynamicFields(){
	var itemType = $("#edit_itemType :selected").data('itemvalue');
	switch(itemType){
		case "Product":
			$("#edit_prtDiv").show();
			$("#edit_prtCatDiv").show();
			$("#edit_prdtDiv").show();
			break;
		case "Partner":
			$("#edit_prtDiv").show();
			break;
		case "Category":
			$("#edit_CatList").show();
			break;
		case "Offers & Deals":
			$("#edit_prtDiv").show();
			$("#edit_CatList").show();
			$("#edit_offerDiv").show();
			break;
		case "Brand":
			$('#edit_brandNameDiv').show();
			$('#edit_brandDescDiv').show();
			$('#edit_brandImageDiv').show();
			break;
		 case "Custom Page Item": 
			 $('#edit_CustomSectionDiv').show();
				$("#edit_prtDiv").show();
	}
}

function editHideDynamicFields(){
	$("#edit_prtDiv").hide();
	$("#edit_prtCatDiv").hide();
	$("#edit_prdtDiv").hide();
	$("#edit_offerDiv").hide();
	$("#edit_CatList").hide();
	$("#edit_selectCatListDiv").hide();
	$('#datepicker13').attr('readonly', true).datepicker('enable');
	$('#datepicker14').attr('readonly', false).datepicker('enable');
	$('#edit_brandNameDiv').hide();
	$('#edit_brandDescDiv').hide();
	$('#edit_brandImageDiv').hide();
	$("#edit_CustomSectionDiv").hide(); 
}

$(document).ready(function () {
	  
	 <c:if test="${sectionItemTab==true}">
		$('#viewSectionTab').removeClass('active');
		$('#sectionA').removeClass('active');
		$('#viewSectionItemTab').addClass('active');
		$('#sectionB').addClass('in active');
		$('#createItemTab').removeClass('active');
		$('#sectionC').removeClass('active');
		</c:if>
		<c:if test="${createItemTab==true}">
		$('#viewSectionTab').removeClass('active');
		$('#sectionA').removeClass('active');
		$('#viewSectionItemTab').removeClass('active');
		$('#sectionB').removeClass('active');
		$('#createItemTab').addClass('active');
		$('#sectionC').addClass('in active');
		</c:if>
		hideDynamicField();

	 
	
	$("#homepageSectionBean").validate({

	    ignore:[],	
		rules: {
			section_title:{
				required: true,
				duplicateSectionTitle:true,
			},  
			display_order:{
				required: true,
				digits:true,
				displayOrderNotZero:true,
				dispOrd:true,
				 
			},
			datepicker11:{
				required: true,
				dateHigherThanToday:true
			},
			datepicker12:{
				greaterThanEndDate:true
			},
			footerDesc:{

				rangelength:[0,2000]
			},
			footerImage1:{
				extension : "png",
				//filesize : 1100000,
				//imageDimention1 : true
			},
			footerImage2:{
				extension : "png",
				//filesize : 1100000,
				//imageDimention2 : true
			},
			viewLink :{
				viewLinkCheck: true
			}
	 },
	 
	 messages: {
		 section_title:{
			 required:"Please enter section title",
			 duplicateSectionTitle:"Section title already exists"
		 },
		 display_order:{
				required: "Please enter display order ",
		 		digits:"Please enter numeric data",
		 		displayOrderNotZero:"Section Display Order cannot be 0",
		 		dispOrd:"Section Display Order already exists"
			},
		 datepicker11:{
				required: "Please Select Activation Date",
				dateHigherThanToday:"Activation date must be less than or equal to Deactivation date "
				
			},
		datepicker12:{
			greaterThanEndDate:"Deactivation date must be greater than or equal to Activation date "
		},
		footerDesc:{
			rangelength:"Maximum 2000 characters allowed."
		},
		footerImage1:{
			extension : "Invalid file format. Only png files are allowed."
		},
		footerImage2:{
			extension : "Invalid file format. Only png files are allowed."
		},
		viewLink :{
			viewLinkCheck: "Please select the Redirection page"
		}
	 },
	 errorElement: "div",
	 errorPlacement: function(error, element) {
	     if ($(element).hasClass("form-control startDate")) {
	         error.insertAfter($(element).closest("div"));
	     }
	     else if ($(element).hasClass("form-control endDate")){
	      error.insertAfter($(element).closest("div"));
	     }
	     else if ($(element).hasClass("allow_checkbox")){
		      error.insertAfter($(element).closest("div"));
		 }
	     else {
	         error.insertAfter(element);
	     }
	 },
	 submitHandler: function (form) {
		form.submit();
	},
	});
	
	
	
	//*****************************
	$("#createSectionItems").validate({
	    ignore:[],
		rules: {
			dupRecord:{
				dupRecord: true
			},
			sectionId:{
				required: true
	        },
	        itemType:{
				required: true
			},
			prtNo:{
				required: false
			},
			catNo:{
				required: false
			},
			prtCatNo:{
				required: false
			},
			prtPrdctNo:{
				required: false
			},
			prtOfferNo:{
				required: false
			},
			displayOrder:{
				required: true,
				digits:true,
				dupDispOrd: true,
			},
			startDate:{
				required: true,
				startDate: true
			},
			endDate:{
				endDate:true
			},
			brandName:{
				required:false
			},
			brandImage:{
				required:false,
				extension: "png|jpg|jpeg|gif"
			},
			brandImageUrl:{
				required:false,
				
			},
			brandDescription:{
				rangelength:[0,50]
			},
			
			brandImage1:{
				required:false,
				extension: "png|jpg|jpeg|gif"
			},
			brandImageUrl1:{
				required:false,
				
			},
			 landingUrl:{
				rangelength : [ 1, 2000 ],
				url : true
			},
			title:{
				required: true,
			} 
	 	},
		 messages: {
			dupRecord:{
				dupRecord: "This record is duplicate."
			},
		 	sectionId:{
				required: "Please select Section."
	        },
	        itemType:{
				required: "Please select Item Type."
			},
			prtNo:{
				required: "Please select a Partner."
			},
			catNo:{
				required: "Please select a Category."
			},
			prtCatNo:{
				required: "Please select Partner Category."
			},
			prtPrdctNo:{
				required: "Please enter a Product, select from dropdown."
			},
			prtOfferNo:{
				required: "Please enter a Offers, select from dropdown."
			},
			displayOrder:{
				required: "Please enter Display Order.",
				digits: "Please enter numeric data.",
				dupDispOrd: "Display Order already exists.",
			},
			startDate:{
				required: "Please select Start Date.",
				startDate: "Start Date must be less than or equal to End Date."
			},
			endDate:{
				endDate: "End Date must be greater than or equal to Start Date."
			},
			brandName:{
				required:"Please enter a Brand, select from dropdown"
			},
			brandImage:{
				required:"Please upload brand image",
				extension:"Invalid file format. Only jpg|jpeg|png|gif files are allowed."			
		    },
			brandImageUrl:{
				required:"Please click On brand image upload button",
		    },
		    brandDescription:{
				rangelength:"Maximum 50 Characters Allowed"
			},
			brandImage1:{
				required:"Please upload brand image",
				extension:"Invalid file format. Only jpg|jpeg|png|gif files are allowed."			
		    },
			brandImageUrl1:{
				required:"Please click On brand image upload button",
		    },
		      title:{
		    	required:"Please Enter Title",
			},
			landingUrl:{
				rangelength:"Maximum 50 Characters Allowed",
				required:"Please Landing Url",
			}  
			
		 },
		 errorElement: "div",
		 errorPlacement: function(error, element) {
			if ($(element).hasClass("form-control startDate")) {
				error.insertAfter($(element).closest("div"));
			}
			else if ($(element).hasClass("form-control endDate")){
				error.insertAfter($(element).closest("div"));
			}else if($(element).hasClass("newBrandImageUpload")){
				error.insertAfter($(element).closest("div"));
			}
			else {
				error.insertAfter(element);
			}
		},
		submitHandler: function (form) {
			form.submit();
		}
	});
	//*****************************
	
	$("#editSectionItems").validate({
	    ignore:[],
		rules: {
			edit_dupRecord:{
				dupRecord: true
			},
			edit_prtNo:{
				required: false
			},
			catgry_no:{
				required: false
			},
			edit_prtCatNo:{
				required: false
			},
			edit_prtPrdctNo:{
				required: false
			},
			edit_prtOfferNo:{
				required: false
			},
			edit_displayOrder:{
				required: true,
				digits:true,
				dupDispOrd: true,
			},
			datepicker13:{
				required: true,
				edit_startDate: true
			},
			datepicker14:{
				edit_endDate:true
			},
			editBrandName:{
				required:false
			},
			editBrandImageUrl:{
				required:false,
				rangelength:[1,100]
			},
			editBrandImage:{
				extension: "png|jpg|jpeg|gif"
			},
			editBrandDescription:{
				rangelength:[0,50]
			},
			c:{
				required:false
			},
			editTitle:{
				required:false
			},
			editBrandImageUrl1:{
				required:false,
				rangelength:[1,100]
			},
			editBrandImage1:{
				extension: "png|jpg|jpeg|gif"
			},
			editlandingUrl:{
				required:false,
				url : true
			},
			
	 	},
		 messages: {
			 edit_dupRecord:{
				dupRecord: "This record is duplicate."
			},
			edit_prtNo:{
				required: "Please select a Partner."
			},
			catgry_no:{
				required: "Please select a Category."
			},
			edit_prtCatNo:{
				required: "Please select Partner Category."
			},
			edit_prtPrdctNo:{
				required: "Please enter a Product, select from dropdown."
			},
			edit_prtOfferNo:{
				required: "Please enter a Offers, select from dropdown."
			},
			edit_displayOrder:{
				required: "Please enter Display Order.",
				digits: "Please enter numeric data.",
				dupDispOrd: "Display Order already exists.",
			},
			datepicker13:{
				required: "Please select Start Date.",
				edit_startDate: "Start Date must be less than or equal to End Date."
			},
			datepicker14:{
				edit_endDate: "End Date must be greater than or equal to Start Date."
			},
			editBrandName:{
				required:"Please enter a Brand, select from dropdown"
			},
			editBrandImage:{
				extension:"Invalid file format. Only jpg|jpeg|png|gif files are allowed."
			},	
			editBrandImageUrl:{
				required:"Please Select Brand Image Url",
				rangelength:"Maximum 100 characters permitted"
			},
			edit_brandImageUrlHidden:{
				required:"Please Click on Upload Button"
			},
			editBrandDescription:{
				rangelength:"Maximum 50 Characters Allowed"
			},
			editTitle:{
				required:"Please enter a Title"
			},
			editlandingUrl:{
				required:"Please enter a Title"
			},
			editBrandImage1:{
				extension:"Invalid file format. Only jpg|jpeg|png|gif files are allowed."
			},	
			editBrandImageUrl1:{
				required:"Please Select Custom Image Url",
				rangelength:"Maximum 100 characters permitted"
			},
			edit_brandImageUrlHidden1:{
				required:"Please Click on Upload Button"
			},
		 },
		 errorElement: "div",
		 errorPlacement: function(error, element) {
			if ($(element).hasClass("form-control startDate")) {
				error.insertAfter($(element).closest("div"));
			}
			else if ($(element).hasClass("form-control endDate")){
				error.insertAfter($(element).closest("div"));
			}else if ($(element).hasClass("editBrandImageUpload")){
		         error.insertAfter($(element).closest("div"));
		     }
			else {
				error.insertAfter(element);
			}
		},
		submitHandler: function (form) {
			form.submit();
		}
	});
	
	
});

$.validator.addMethod('dupRecord', function(value, element) {
	return !duplicateRecord;
});

$.validator.addMethod('dupDispOrd', function(value, element) {
	return !sectionItmDispOrd;
});

$.validator.addMethod("endDate", function(value, element) {
	var startDate = getFormattedDate($('#datepicker1').val());
	var endDate = getFormattedDate($('#datepicker2').val());
	if(endDate=='Invalid Date')
		return true;
	if (endDate >= startDate)
		return true;
	if (endDate <= startDate)
		return false;
});

$.validator.addMethod("startDate", function(value, element) {
	var startDate = getFormattedDate($('#datepicker1').val());
	var endDate = getFormattedDate($('#datepicker2').val());
	if(endDate=='Invalid Date')
		return true;
	if (startDate <= endDate)
		return true;
	if (startDate > endDate)
		return false;
});

$.validator.addMethod("edit_endDate", function(value, element) {
	var startDate = getFormattedDate($('#datepicker13').val());
	var endDate = getFormattedDate($('#datepicker14').val());
	if(endDate=='Invalid Date')
		return true;
	if (endDate >= startDate)
		return true;
	if (endDate <= startDate)
		return false;
});

$.validator.addMethod("edit_startDate", function(value, element) {
	var startDate = getFormattedDate($('#datepicker13').val());
	var endDate = getFormattedDate($('#datepicker14').val());
	if(endDate=='Invalid Date')
		return true;
	if (startDate <= endDate)
		return true;
	if (startDate > endDate)
		return false;
});

$.validator.addMethod('duplicateSectionTitle', function(value, element) {
    if(sectionTitleExist){
		return false;
    }
    else{
		return true;
    }
});	

$.validator.addMethod('dispOrd', function(value, element) {
	return !displayOrder;
});
	
$("#section_title").change(function(){
	var section_title=$('#section_title').val();
	var sectionHidden = $('#hidden_sectiontitle').val();
	
	if(section_title != sectionHidden){
		$.get("${contextPath}/admin/checkSectionTitle?sectionTitle="+section_title, function(data,status) {
		if(status=="success" && data!=''){
			sectionTitleExist=true;	
		}
		else{
			sectionTitleExist=false;
		}
		$("#homepageSectionBean").validate().element('#section_title');	
	});
		
	}else{
		$("#homepageSectionBean").validate().element('#section_title');
	}
});	

$('#display_order').on('keyup', function () {
	var disOrder = parseInt($("#display_order").val());
	var hiddenVal = parseInt($("#hidden_displayorder").val());				
	displayOrder = false;
	 
	if(disOrder != hiddenVal && disOrder != 0){
		$.get("${contextPath}/admin/checkDispOrd?displayOrder="+disOrder, function(data,status) {
			if(status=="success" && data!=''){
				displayOrder=true;	
			}
			else{
				displayOrder=false;
			}
			$("#homepageSectionBean").validate().element('#display_order');	
		});
	}
	else{
		$("#homepageSectionBean").validate().element('#display_order');
	}
	
});

$("#display_order").on('blur', function(){
	$("#homepageSectionBean").validate().element('#display_order');	
});

$.validator.addMethod('displayOrderNotZero', function(value, element) {
	var isValid = false;
	var dis_order = parseInt($("#display_order").val());
	
	if(dis_order == 0){
		isValid = true;
	}
	return !isValid; 
});

$('#datepicker11').on('change', function () {
	$("#homepageSectionBean").validate().element('#datepicker11');
	var endDate = getFormattedDate($('#datepicker12').val());
	
	if(endDate != 'Invalid Date'){
		$("#homepageSectionBean").validate().element('#datepicker12');
	}
});

$('#datepicker12').on('change', function () {
	$("#homepageSectionBean").validate().element('#datepicker11'); 
	$("#homepageSectionBean").validate().element('#datepicker12'); 
});

$.validator.addMethod("greaterThanEndDate", function(value, element) {
	var startDate = getFormattedDate($('#datepicker11').val());
		var endDate = getFormattedDate($('#datepicker12').val());
		if(endDate=='Invalid Date')
			return true;
		if (endDate >= startDate)
			return true;
		if (endDate <= startDate)
			return false;
	});
 $.validator.addMethod("viewLinkCheck", function(value,element){
	var viewLink = $('input[name=viewLink]:checked').val();
	var redirect = $('#redirectionPage').val();
	if(viewLink=='on' && redirect==''){
		return false;
	} else 
		return true;
	
	/*   if(viewLink=="undefined"  && redirect==''){
		alert("hii")
		return true;
	} 	 */
});
$.validator.addMethod("dateHigherThanToday", function(value, element) {
	var startDate = getFormattedDate($('#datepicker11').val());
	var endDate = getFormattedDate($('#datepicker12').val());
	
	if(endDate=='Invalid Date')
		return true;
	if (startDate <= endDate)
		return true;
	if (startDate >= endDate)
		return false;
});

function getFormattedDate(date){
	var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
	return new Date(date.replace(pattern,'$3-$2-$1'));
}

$('#cancelbtnD').click(function () {
	var secTyp = $("#section_Type").val();
		window.location.href="${contextPath}/admin/homePageSections?sectionType="+secTyp;
	 
});
	
$(document).on('click','.selectCategory',function(){

	var json = $("#getCategoryList").val();
	var content = JSON.parse(json);	
	$('.select_category').jstree({
			'core' : {
				'data' : content,
				 "check_callback" : true,
			}
	});
	
	$('.category_list_details').jstree({
		'core' : {
			'data' : content
		},
		"plugins" : [ "sort" ]
	});
});

$("#edit_showCatList").on('click', function(){
	$("#edit_selectCatListDiv").show();
});

function checkCategory(){
	var category = $("#categoryNo_").val();
	if(category != ""){
		$('#categoryNo_-error').hide();
	} else {
		$('#categoryNo_-error').show();
	}
}

$(document).on('click','.select_category .jstree-container-ul li a',function(){
	var selected_categoryValue = $('.select_category').jstree().get_selected(true)[0].text;
	$('#categorySelected').val(selected_categoryValue);
	$('#catNo').val($(".jstree-clicked").attr("catno"));
	$("#createSectionItems").validate().element('#catNo');
	$("#offerTitle").val('');
	$("#prtOfferNo").val('');
	 $('.close').click();
	
	checkDuplicateRecord();
});

$("#offerTitle").on('blur', function(){
	if($("#offerTitle").val() == ""){
		$("#prtOfferNo").val('');
	}
	$("#createSectionItems").validate().element('#prtOfferNo');
	checkDuplicateRecord();
});

$("#offerTitle").on('click', function(){
	$("#prtOfferNo").val(''); 
	$("#offerTitle").val('');
});

$("#offerTitle").on( "keyup", function(){
	var prtNo = $("#prtNo").val();
	var catNo = $("#catNo").val();

	$("#offerTitle").autocomplete({
		minLength: 1,
		source: function(request, response) {    	  
			$.ajax({
				dataType: "json",
				type: "GET",
				xhrFields: {
					withCredentials: true
				},
				crossDomain: true,
				cache: true,
				data: {
					keyword: request.term
				}, 
				url: "${contextPath}/admin/getOfferTitle?prtNo="+prtNo+"&catNo="+catNo,
				success: function(data){
					var array= $.map( data, function(key,value) {
						return {                    	  
							label:key,
							value:value
						}
					});
					response($.ui.autocomplete.filter(array, request.term));
				},
				error: function (data) {
				}
			})
		},
		focus: function( event, ui ) {
			event.preventDefault();
		},
		select: function(event, ui) {
			$("#prtOfferNo").val(ui.item.value); 
			$("#offerTitle").val(ui.item.label);
			$("#createSectionItems").validate().element('#prtOfferNo');
			return false;
		}
	});
});

$("#prtNo").on( "selectmenuchange", function(){
	var obj=null;
	var prtNo = $("#prtNo").val();
	$("#productName").val('');
	$("#prtPrdctNo").val('');
	$("#categorySelected").val('');
	$("#catNo").val('');
	$("#offerTitle").val('');
	$("#prtOfferNo").val('');
	
	$.get("${contextPath}/admin/getPartnerCategories?prtNo="+prtNo, function(data){
		var obj = jQuery.parseJSON(data);
		$('#prtCatNo').empty();
		$('#prtCatNo').append("<option value=''>-Partner Category-</option>");
		
		for(var i = 0; i < obj.length; i++){
			var value = obj[i];
			var newOption = $("<option value="+value.prtCatNo+">"+value.partnercategoryName+"</option>");
			$('#prtCatNo').append(newOption);	       
		}
		$( "#prtCatNo" ).selectmenu( "refresh" );
	});

	//$("#createSectionItems").validate().element('#prtNo');
	checkDuplicateRecord();
});

$("#productName").on('blur', function(){
	if($("#productName").val() == ""){
		$("#prtPrdctNo").val('');
	}
	
	$("#createSectionItems").validate().element('#prtPrdctNo');
	checkDuplicateRecord();
});

$("#productName").on('click', function(){
	$("#productName").val(''); 
	$("#prtPrdctNo").val('');
});

$("#productName").on('keyup', function(){
	var prtCatNo = $("#prtCatNo").val();
	var productName = $("#productName").val();
	
	$( "#productName" ).autocomplete({
		minLength: 1,
		source: function(request, response) {    	  
			$.ajax({
				dataType: "json",
				type: "GET",
				xhrFields: {
					withCredentials: true
				},
				crossDomain: true,
				cache: true,
				data: {
					keyword: request.term
				},
				url: "${contextPath}/admin/getPartnerProducts?prtCatNo="+prtCatNo+"&rpName="+productName,
				success: function(data){ 
					var array= $.map( data, function(key,value) {
						return {                    	  
							label:key,
							value:value
						}
					});
					response($.ui.autocomplete.filter(array, request.term));
				},
				error: function (data) {
				}
			})
		},
		focus: function( event, ui ) {
			event.preventDefault();
		},
		select: function(event, ui) {
			$("#prtPrdctNo").val(ui.item.value); 
			$("#productName").val(ui.item.label);
			$("#createSectionItems").validate().element('#prtPrdctNo');
			return false;
		}
	});
	
});
function editUploadImage(id,type,url){
	
	var fileUpload = $("#"+id)[0];			 
	checkFileUpload(fileUpload,"brand");
	if(!uploadFlag){
		return false;
	}
	
   	var formData = new FormData();
   	formData.append('file',$("#"+id)[0].files[0]);	
   	
   	if($("#editSectionItems").validate().element("#"+id)){		
  			$.ajax({
			url: "${contextPath}/admin/updatepartnerLogo?type="+type,
		  	data: formData,
			dataType: 'text',
			processData: false,
			contentType: false,
			type: 'POST',
			  	success: function (response) {
   			 	$('#'+url).val(response);			 
   			 	var newImageUrl='${contextPath}'+'/admin/images/'+type+"/"+response;
   			 	imageUploadFlag=true;
   				$('#editBrandImage_').prop('src',newImageUrl);
   				$("#edit_brandImageUrlHidden").val(response);
   				$("#editSectionItems").validate().element("#edit_brandImageUrlHidden");
   			 	$("#editSectionItems").validate().element("#"+url);

  			  	}
		});		
   	}
}
/* custom item type */
 function editUploadImage1(id,type,url){
	var fileUpload = $("#"+id)[0];			 
	checkFileUpload(fileUpload,"brand1");
	if(!uploadFlag){
		return false;
	}
	
   	var formData = new FormData();
   	formData.append('file',$("#"+id)[0].files[0]);	
   	
   	if($("#editSectionItems").validate().element("#"+id)){		
  			$.ajax({
			url: "${contextPath}/admin/updatepartnerLogo?type="+type,
		  	data: formData,
			dataType: 'text',
			processData: false,
			contentType: false,
			type: 'POST',
			  	success: function (response) {
   			 	$('#'+url).val(response);			 
   			 	var newImageUrl='${contextPath}'+'/admin/images/'+type+"/"+response;
   			 	imageUploadFlag=true;
   				$('#editBrandImage_1').prop('src',newImageUrl);
   				$("#edit_brandImageUrlHidden1").val(response);
   				$("#editSectionItems").validate().element("#edit_brandImageUrlHidden1");
   			 	$("#editSectionItems").validate().element("#"+url);

  			  	}
		});		
   	}
}
$("#brandName").on('blur', function(){
	if($("#brandName").val() == ""){
		$("#brandName").val('');
	}
	
	$("#createSectionItems").validate().element('#brandName');
	checkDuplicateRecord();
});
$("#editBrandName").on('blur', function(){
	if($("#editBrandName").val() == ""){
		$("#editBrandName").val('');
	}
	
	$("#editSectionItems").validate().element('#editBrandName');
	edit_checkDuplicateRecord();
});

$("#brandName").on('click', function(){
	$("#brandName").val(''); 
});
/*brand  autosuggestion */
   $("#brandName").on('keyup', function(e){
		$("#createSectionItems").validate().element('#brandName');
		var brandName = $("#brandName").val();
		$( "#brandName" ).autocomplete({
			minLength: 1,
			source: function(request, response) {    	  
				$.ajax({
					dataType: "json",
					type: "GET",
					xhrFields: {
						withCredentials: true
					},
					crossDomain: true,
					cache: true,
					data: {
						keyword: request.term
					},
					url: "${contextPath}/admin/getBrandName?brName="+brandName,
					success: function(data){ 
						var array= $.map( data, function(key,value) {
							return {                    	  
								label:key,
								value:value
							}
						});
						response($.ui.autocomplete.filter(array, request.term));
					},
					error: function (data) {
					}
				})
			},
			focus: function( event, ui ) {
				event.preventDefault();
			},
			change: function(event,ui)
	        {
				if(ui.item==null){
                    $("#brandName").val("");
                }
	        },
			select: function(event, ui) {
				
				$("#brandName").val(ui.item.label); 
				/* $("#brandNo").val(ui.item.value); */
				$("#createSectionItems").validate().element('#brandName');
				return false;
			}
		});
		
	}); 
   $("#editBrandName").on('keyup', function(e){
		$("#editSectionItems").validate().element('#editBrandName');
		var brandName = $("#editBrandName").val();
		$( "#editBrandName" ).autocomplete({
			minLength: 1,
			source: function(request, response) {    	  
				$.ajax({
					dataType: "json",
					type: "GET",
					xhrFields: {
						withCredentials: true
					},
					crossDomain: true,
					cache: true,
					data: {
						keyword: request.term
					},
					url: "${contextPath}/admin/getBrandName?brName="+brandName,
					success: function(data){ 
						var array= $.map( data, function(key,value) {
							return {                    	  
								label:key,
								value:value
							}
						});
						response($.ui.autocomplete.filter(array, request.term));
					},
					error: function (data) {
					}
				})
			},
			focus: function( event, ui ) {
				event.preventDefault();
			},
			change: function(event,ui)
	        {
				if(ui.item==null){
                    $("#editBrandName").val("");
                }
	        },
			select: function(event, ui) {
				//$("#editBrandNo").val(ui.item.value); 
				$("#editBrandName").val(ui.item.label);
				$("#editSectionItems").validate().element('#editBrandName');
				return false;
			}
		});
	}); 
 
$("#prtCatNo").on( "selectmenuchange", function(){
	var prtCatNo = $("#prtCatNo").val();
	$("#productName").val('');
	$("#prtPrdctNo").val('');
	$("#createSectionItems").validate().element('#prtCatNo');
});

$("#itemType").on('selectmenuchange', function(){
	hideDynamicField();
	var itemType = $("#itemType :selected").data('itemvalue');
	changeValidationRule(itemType);
	switch(itemType){
		case "Product":
			$("#prtNoDiv").show();
			$("#prtCatNoDiv").show();
			$("#createSectionItems").validate().element('#catNo');
			$("#createSectionItems").validate().element('#prtOfferNo');
			$("#createSectionItems").validate().element('#brandName');
			$("#createSectionItems").validate().element('#brandImageUrl');
			break;
		case "Partner":
			$("#prtNoDiv").show();
			/* $("#createSectionItems").validate().element('#catNo');
			$("#createSectionItems").validate().element('#prtCatNo');
			$("#createSectionItems").validate().element('#prtPrdctNo');
			$("#createSectionItems").validate().element('#prtOfferNo');
			$("#createSectionItems").validate().element('#brandNo');
			$("#createSectionItems").validate().element('#brandImageUrl'); */
			break;
		case "Category":
			$("#categorySelectedDiv").show();
			$("#createSectionItems").validate().element('#prtNo');
			$("#createSectionItems").validate().element('#prtCatNo');
			$("#createSectionItems").validate().element('#prtPrdctNo');
			$("#createSectionItems").validate().element('#prtOfferNo');
			$("#createSectionItems").validate().element('#brandNo');
			$("#createSectionItems").validate().element('#brandImageUrl');
			break;
		case "Offers & Deals":
			$("#prtNoDiv").show();
			$("#categorySelectedDiv").show();
			$("#offerTitleDiv").show();
			//$("#createSectionItems").validate().element('#prtNo');
			/* $("#createSectionItems").validate().element('#prtCatNo');
			$("#createSectionItems").validate().element('#prtPrdctNo');
			$("#createSectionItems").validate().element('#brandNo');
			$("#createSectionItems").validate().element('#brandImageUrl'); */
			break;
		case "Brand":
			$("#brandNameDiv").show();
			$("#brandImageDiv").show();
			$("#createSectionItems").validate().element('#prtNo');
			$("#createSectionItems").validate().element('#catNo');
			$("#createSectionItems").validate().element('#prtCatNo');
			$("#createSectionItems").validate().element('#prtPrdctNo');
			$("#createSectionItems").validate().element('#prtOfferNo');
			break;
			
		case "Custom Page Item":
			$("#customPageItemDiv").show();
			$("#prtNoDiv").show();
			/* $('#prtNo').rules('add', {
				required: false
			}); */
			
			$('#mandatory_custom').hide();
//                         /code by reddy
//		         $("#createSectionItems").validate().element('#prtNo');
//			$("#createSectionItems").validate().element('#landingUrl');
//			$("#createSectionItems").validate().element('#title');
//			$("#createSectionItems").validate().element('#brandImageUrl1');
			break;
			
	}
	$("#createSectionItems").validate().element('#itemType');
});

$("#sectionId").on('selectmenuchange', function(){
	hideDynamicField();
	$("#itemType").val('').selectmenu("refresh");
	$('#createSectionItems').find('input, select, textarea').not("#sectionId, #cancelbtnD_sectionItem, #getCategoryList, .sectionItemStatus,.bg_g_button").val('');
	$('#createSectionItems').validate().resetForm();
	
	$("#createSectionItems").validate().element('#sectionId');
});

$('#datepicker1').on('change', function () {
	$("#createSectionItems").validate().element('#datepicker1');
	var endDate = getFormattedDate($('#datepicker2').val());
	
	if(endDate != 'Invalid Date'){
		$("#createSectionItems").validate().element('#datepicker2');
	}
});

$('#datepicker2').on('change', function () {
	$("#createSectionItems").validate().element('#datepicker1'); 
	$("#createSectionItems").validate().element('#datepicker2'); 
});

$("#cancelbtnD_sectionItem").on('click', function(){
	window.location.reload();
});

$("#displayOrder").on('keyup',function(){
	var dispOrd = $('#displayOrder').val();
	var sectionId = $("#sectionId :selected").val();
	
	$.get("${contextPath}/admin/checkSectionItemDisplayOrder?displayOrder="+dispOrd+"&sectionId="+sectionId, function(data,status) {
		if(status=="success" && data!=''){
			sectionItmDispOrd=true;	
		}
		else{
			sectionItmDispOrd=false;
		}
		$("#createSectionItems").validate().element('#displayOrder');
	});
	$("#createSectionItems").validate().element('#sectionId');
});	

function hideDynamicField (){
	$("#prtNoDiv").hide();
	$("#categorySelectedDiv").hide();
	$("#prtCatNoDiv").hide();
	$("#offerTitleDiv").hide();
	$("#brandNameDiv").hide();
	$("#brandImageDiv").hide();
	$("#customPageItemDiv").hide();
	
	$("#prtNo").val('');
	$("#categorySelected").val('');
	$("#catNo").val('');
	$("#prtCatNo").val('');
	$("#productName").val('');
	$("#prtPrdctNo").val('');
	$("#offerTitle").val('');
	$("#prtOfferNo").val('');
	$('#prtNo').selectmenu("refresh");
	$('#prtCatNo').selectmenu("refresh");
}


$("#section_id").on( "selectmenuchange", function(){	
	 
		 var section_id= $('#section_id').val();
		 $(this).closest('form').trigger('submit');
		 
}); 

function changeValidationRule(itemType){
	
	$('#prtNo').rules('add', {
		required: false
	});
	$('#catNo').rules('add', {
		required: false
	});
	$('#prtCatNo').rules('add', {
		required: false
	});
	$('#prtPrdctNo').rules('add', {
		required: false
	});
	$('#prtOfferNo').rules('add', {
		required: false
	});
	$('#brandName').rules('add', {
		required: false
	});
	$('#brandImageUrl').rules('add', {
		required: false
	});
	$('#brandImage').rules('add', {
		required: false
	});
	$('#brandImageUrl1').rules('add', {
		required: false
	});
	$('#brandImage1').rules('add', {
		required: false
	});
	$('#landingUrl').rules('add', {
		required: false
	});
	$('#title').rules('add', {
		required: false
	});
	switch(itemType){
		case "Product":
			$('#prtNo').rules('add', {
				required: true
			});
			$('#prtCatNo').rules('add', {
				required: true
			});
			$('#prtPrdctNo').rules('add', {
				required: true
			});
			break;
		case "Partner":
			$('#prtNo').rules('add', {
				required: true
			});
			break;
		case "Category":
			$('#catNo').rules('add', {
				required: true
			});
			break;
		case "Offers & Deals":
			$('#prtNo').rules('add', {
				required: true
			});
			$('#catNo').rules('add', {
				required: true
			});
			$('#prtOfferNo').rules('add', {
				required: true
			});
			break;
		case "Brand":
			$('#brandName').rules('add', {
				required: true
			});
			$('#brandImageUrl').rules('add', {
				required: true
			});
			$('#brandImage').rules('add', {
				required: true
			});
			
			break;
			
		 case "Custom Page Item":
			 $('#landingUrl').rules('add', {
				required: true
			});
			$('#title').rules('add', {
				required: true
			});
			
			$('#brandImageUrl1').rules('add', {
				required: true
			});
			$('#brandImage1').rules('add', {
				required: true
			});
			/* $('#prtNo').rules('add', {
				required: false
			}); */
			break; 
	}
}

function edit_changeValidationRule(itemType){
	
	$('#edit_prtNo').rules('add', {
		required: false
	});
	$('#catgry_no').rules('add', {
		required: false
	});
	$('#edit_prtCatNo').rules('add', {
		required: false
	});
	$('#edit_prtPrdctNo').rules('add', {
		required: false
	});
	$('#edit_prtOfferNo').rules('add', {
		required: false
	});
	$('#editBrandName').rules('add', {
		required: false
	});
	$('#editBrandImageUrl').rules('add', {
		required: false
	});
	$('#editBrandImage').rules('add', {
		required: false
	});
	$('#edit_brandImageUrlHidden').rules('add', {
		required: false
	});
	
	$('#editTitle').rules('add', {
		required: false
	});
	$('#editBrandImageUrl1').rules('add', {
		required: false
	});
	$('#editBrandImage1').rules('add', {
		required: false
	});
	$('#edit_brandImageUrlHidden1').rules('add', {
		required: false
	});
	$('#editlandingUrl').rules('add', {
		required: false
	});
	
	switch(itemType){
		case "Product":
			$('#edit_prtNo').rules('add', {
				required: true
			});
			$('#edit_prtCatNo').rules('add', {
				required: true
			});
			$('#edit_prtPrdctNo').rules('add', {
				required: true
			});
			break;
		case "Partner":
			$('#edit_prtNo').rules('add', {
				required: true
			});
			break;
		case "Category":
			$('#catgry_no').rules('add', {
				required: true
			});
			break;
		case "Offers & Deals":
			$('#edit_prtNo').rules('add', {
				required: true
			});
			$('#catgry_no').rules('add', {
				required: true
			});
			$('#edit_prtOfferNo').rules('add', {
				required: true
			});
			break;
		case "Brand":
			$('#editBrandName').rules('add', {
				required: true
			});
			 $('#edit_brandImageUrlHidden').rules('add', {
				required: true
			}); 
			$('#editBrandImageUrl').rules('add', {
				required: true
			});
			
			break;
			
		case "Custom Page Item":
			$('#editTitle').rules('add', {
				required: true
			});
			 $('#edit_brandImageUrlHidden1').rules('add', {
				required: true
			}); 
			$('#editBrandImageUrl1').rules('add', {
				required: true
			});
			$('#editlandingUrl').rules('add', {
				required: true
			});
			/* $('#edit_prtNo').rules('add', {
				required: false
			}); */
			break;
	}
}


function checkDuplicateRecord(){
	duplicateRecord = false;
	var sectionId = $("#sectionId").val();
	var itemType = $("#itemType").val();
	var prtNo = $("#prtNo").val();
	var catNo = $("#catNo").val();
	var prtCatNo = $("#prtCatNo").val();
	var prtPrdctNo = $("#prtPrdctNo").val();
	var prtOfferNo = $("#prtOfferNo").val();
	var startDate = $("#datepicker1").val();
	var endDate = $("#datepicker2").val();
	var brandName=$("#brandName").val();
	var queryString = "sectionId="+sectionId+"&itemType="+itemType+"&prtNo="+prtNo+"&catNo="+catNo+"&prtPrdctNo="+prtPrdctNo+"&prtOfferNo="+prtOfferNo+"&brandName="+brandName;
	var checkForDuplicateRecord = false;
	var itemTypeVal = $("#itemType :selected").data('itemvalue');
	
	switch(itemTypeVal){
		case "Product":
			if(prtPrdctNo != ""){
				checkForDuplicateRecord = true;
			}
			break;
		case "Partner":
			if(prtNo != ""){
				checkForDuplicateRecord = true;
			}
			break;
		case "Category":
			if(catNo != ""){
				checkForDuplicateRecord = true;
			}
			break;
		case "Offers & Deals":
			if(prtOfferNo != ""){
				checkForDuplicateRecord = true;
			}
			break;
		case "Brand":
			if(brandName !=""){
				checkForDuplicateRecord = true;
			}
			break;
		case "Custom Page Item":
			if(prtNo!=""){
				checkForDuplicateRecord = true;
			}
			break;
	}
	
	if(checkForDuplicateRecord){
		$.get("${contextPath}/admin/checkDuplicateRecord?"+queryString, function(data,status) {
			if(status=="success" && data!=''){
				duplicateRecord = true;
			}
			else{
				duplicateRecord = false;
			}
			$("#createSectionItems").validate().element('#dupRecord');
		});	
	}
	$("#createSectionItems").validate().element('#dupRecord');
}



//******************EDIT Section Items

$(document).on('click','.category_list_details .jstree-container-ul li a',function(){
	var selected_categoryValue = $('.category_list_details').jstree().get_selected(true)[0].text;
	$('#categoryName_details').val(selected_categoryValue);
	$('#edit_offerTitle').val('');
	$('#edit_prtOfferNo').val('');
	$('#catgry_no').val($(this).attr("catno"));
	edit_checkDuplicateRecord();
});

$("#edit_prtNo").on( "selectmenuchange", function(event, type, prtcatno, prtprdno, productname){
	var prtNo = $("#edit_prtNo").val();
	$.get("${contextPath}/admin/getPartnerCategories?prtNo="+prtNo, function(data){
		var obj = jQuery.parseJSON(data);
		$('#edit_prtCatNo').empty();
		$('#edit_prtCatNo').append("<option value=''>-Partner Category-</option>");
		
		for(var i = 0; i < obj.length; i++){
			var value = obj[i];
			var newOption = $("<option value="+value.prtCatNo+">"+value.partnercategoryName+"</option>");
			$('#edit_prtCatNo').append(newOption);	       
		}
		
		if(type == "refresh"){
			$('#edit_prtCatNo').val(prtcatno);
			$("#edit_prtCatNo").trigger("selectmenuchange", ["refresh", prtprdno, productname]);
		}
		else{
			$("#edit_productName").val('');
			$("#edit_prtPrdctNo").val('');
		}
		
		$( "#edit_prtCatNo" ).selectmenu( "refresh" );
		
	});
	$("#editSectionItems").validate().element('#edit_prtNo');
	edit_checkDuplicateRecord();
});

$("#edit_productName").on('blur', function(){
	if($("#edit_productName").val() == ""){
		$("#edit_prtPrdctNo").val('');
	}
	
	$("#editSectionItems").validate().element('#edit_prtPrdctNo');
	edit_checkDuplicateRecord();
});

$("#edit_productName").on('click', function(){
	$("#edit_productName").val(''); 
	$("#edit_prtPrdctNo").val('');
});

$("#editBrandName").on('click', function(){
	$("#editBrandName").val(''); 
});

$('#editBrandImageUrl').on('change', function() {
	$('#editBrandImage_').val($(this).val());
});

$("#edit_prtCatNo").on( "selectmenuchange", function(event, type, prtprdno, productname){
	var prtCatNo = $("#edit_prtCatNo").val();
	$("#edit_productName").val('');
	$("#edit_prtPrdctNo").val('');
	$( "#edit_productName" ).autocomplete({
		minLength: 1,
		source: function(request, response) {    	  
			$.ajax({
				dataType: "json",
				type: "GET",
				xhrFields: {
					withCredentials: true
				},
				crossDomain: true,
				cache: true,
				data: {
					keyword: request.term
				}, 
				url: "${contextPath}/admin/getPartnerProducts?prtCatNo="+prtCatNo+"&rpName="+request.term,
				success: function(data){
					var array= $.map( data, function(key,value) {
						return {                    	  
							label:key,
							value:value
						}
					});
					response($.ui.autocomplete.filter(array, request.term));
				},
				error: function (data) {
				}
			})
		},
		focus: function( event, ui ) {
			event.preventDefault();
		},
		select: function(event, ui) {
			$("#edit_prtPrdctNo").val(ui.item.value); 
			$("#edit_productName").val(ui.item.label);
			$("#editSectionItems").validate().element('#edit_prtPrdctNo');
			return false;
		}
	});
	
	if(type == "refresh"){
		$('#edit_prtPrdctNo').val(prtprdno);
		$('#edit_productName').val(productname);
	}
	
	$("#editSectionItems").validate().element('#edit_prtCatNo');
});

$("#edit_offerTitle").on('blur', function(){
	if($("#edit_offerTitle").val() == ""){
		$("#edit_prtOfferNo").val('');
	}
	$("#editSectionItems").validate().element('#edit_prtOfferNo');
	edit_checkDuplicateRecord();
});

$("#edit_offerTitle").on('click', function(){
	$("#edit_offerTitle").val(''); 
	$("#edit_prtOfferNo").val('');
});

$("#edit_offerTitle").on( "keyup", function(){
	var prtNo = $("#edit_prtNo").val();
	var catNo = $("#catgry_no").val();
	$("#edit_offerTitle").autocomplete({
		minLength: 1,
		source: function(request, response) {    	  
			$.ajax({
				dataType: "json",
				type: "GET",
				xhrFields: {
					withCredentials: true
				},
				crossDomain: true,
				cache: true,
				data: {
					keyword: request.term
				}, 
				url: "${contextPath}/admin/getOfferTitle?prtNo="+prtNo+"&catNo="+catNo,
				success: function(data){
					var array= $.map( data, function(key,value) {
						return {                    	  
							label:key,
							value:value
						}
					});
					response($.ui.autocomplete.filter(array, request.term));
				},
				error: function (data) {
				}
			})
		},
		focus: function( event, ui ) {
			event.preventDefault();
		},
		select: function(event, ui) {
			$("#edit_prtOfferNo").val(ui.item.value); 
			$("#edit_offerTitle").val(ui.item.label);
			return false;
		}
	});
});

$("#edit_displayOrder").on('keyup',function(){
	var dispOrd = $('#edit_displayOrder').val();
	var dispOrdHidden = $('#edit_displayOrder_hidden').val();
	var sectionId = $("#edit_sectionId :selected").val();
	
	sectionItmDispOrd = false;
	
	if(dispOrd != dispOrdHidden){
		$.get("${contextPath}/admin/checkSectionItemDisplayOrder?displayOrder="+dispOrd+"&sectionId="+sectionId, function(data,status) {
			if(status=="success" && data!=''){
				sectionItmDispOrd=true;	
			}
			else{
				sectionItmDispOrd=false;
			}
			$("#editSectionItems").validate().element('#edit_displayOrder');
		});
	}
	else{
		$("#editSectionItems").validate().element('#edit_displayOrder');
	}
	
});	

$('#datepicker13').on('change', function () {
	$("#editSectionItems").validate().element('#datepicker13');
	var endDate = getFormattedDate($('#datepicker14').val());
	
	if(endDate != 'Invalid Date'){
		$("#editSectionItems").validate().element('#datepicker14');
	}
});

$('#datepicker14').on('change', function () {
	$("#editSectionItems").validate().element('#datepicker13'); 
	$("#editSectionItems").validate().element('#datepicker14'); 
});

$('#updateSectionItems').click(function () {
	var id = $('#myModal2').data('id');
	var sectionId = $("#edit_sectionId").val();
	var json={};
	var startdate,endDate;
	startdate=$("#datepicker13").val();
	endDate=$("#datepicker14").val();
	json["edit_itemId"]=id;
	json["edit_sectionId"]=sectionId;
	json["edit_itemType"]=$("#edit_itemType").val();
	json["edit_prtNo"]=$("#edit_prtNo").val();
	json["edit_prtCatNo"]=$("#edit_prtCatNo").val();
	json["edit_prtPrdctNo"]=$("#edit_prtPrdctNo").val();
	json["edit_prtOfferNo"]=$("#edit_prtOfferNo").val();
	json["edit_sectionItemStatus"]=$('input[name=edit_sectionItemStatus]:checked').val();
	json["edit_displayOrder"]=$("#edit_displayOrder").val();
	json["catgry_no"]=$("#catgry_no").val();
	json["startDate"]=startdate;
	json["endDate"]=endDate;
	json["edit_brandName"]=$("#editBrandName").val();
	json["edit_brandImageUrl"]=$("#editBrandImageUrl").val();
	json["edit_brandDescription"]=$("#editBrandDescription").val();
	json["edit_brandImageUrl1"]=$("#editBrandImageUrl1").val();
	
	json["edit_landingUrl"]=$("#editlandingUrl").val();
	json["edit_title"]=$("#editTitle").val();
	json["edit_text"]=$("#editText").val();
	json["displayPrtPopup"]=$('#edit_displayPrtPopup').val();
	var formData = new FormData();
	formData.append('json',JSON.stringify(json));
	
// 	$('#edit_prtNo').rules('add', {
// 		required: false
// 	});
	
	
	if($('#editSectionItems').valid()){
		$.ajax({
			  url: "${contextPath}/admin/updateSectionItems",
			  data: formData,
			  dataType: 'text',
			  processData: false,
			  contentType: false,
			  type: 'POST',
			  success: function (response) {
					  $('#myModal2').modal('hide');
					  window.location.href="${contextPath}/admin/homePageSections?updateMessage="+response+"&sectionId="+sectionId+"&itemTab=1";
			  },
			  error: function (response) {
				  $('#myModal2').modal('hide');
				  window.location.href="${contextPath}/admin/homePageSections?updateMessage=failure";
            	 }
			 });
	}
});

function edit_checkDuplicateRecord(){
	duplicateRecord = false;
	var sectionId = $("#edit_sectionId").val();
	var itemType = $("#edit_itemType").val();
	var prtNo = $("#edit_prtNo").val();
	var prtNoHidden = $("#edit_prtNo_hidden").val();
	var catNo = $("#catgry_no").val();
	var catNoHidden = $("#catgry_no_hidden").val();
	var prtCatNo = $("#edit_prtCatNo").val();
	var prtPrdctNo = $("#edit_prtPrdctNo").val();
	var prtPrdctNoHidden = $("#edit_prtPrdctNo_hidden").val();
	var prtOfferNo = $("#edit_prtOfferNo").val();
	var prtOfferNoHidden = $("#edit_prtOfferNo_hidden").val();
	var brandName =$("#editBrandName").val();
	var brandNameHidden =$("#editBrandName_hidden").val();
	var queryString = "sectionId="+sectionId+"&itemType="+itemType+"&prtNo="+prtNo+"&catNo="+catNo+"&prtPrdctNo="+prtPrdctNo+"&prtOfferNo="+prtOfferNo+"&brandName="+brandName;
	var checkForDuplicateRecord = false;
	var itemTypeVal = $("#edit_itemType :selected").data('itemvalue');
	
	switch(itemTypeVal){
		case "Product":
			if(prtPrdctNo != "" && (prtPrdctNo != prtPrdctNoHidden)){
				checkForDuplicateRecord = true;
			}
			break;
		case "Partner":
			if(prtNo != "" && (prtNo != prtNoHidden)){
				checkForDuplicateRecord = true;
			}
			break;
		case "Category":
			if(catNo != "" && (catNo != catNoHidden)){
				checkForDuplicateRecord = true;
			}
			break;
		case "Offers & Deals":
			if(prtOfferNo != "" && (prtOfferNo != prtOfferNoHidden)){
				checkForDuplicateRecord = true;
			}
			break;
		case "Brand":
			if(brandName != "" && (brandName != brandNameHidden) ){
				checkForDuplicateRecord = true;
			}
			break; 
	}
	
	if(checkForDuplicateRecord){
		$.get("${contextPath}/admin/checkDuplicateRecord?"+queryString, function(data,status) {
			if(status=="success" && data!=''){
				duplicateRecord = true;
			}
			else{
				duplicateRecord = false;
			}
			$("#editSectionItems").validate().element('#edit_dupRecord');
		});
	}
	else{
		$("#editSectionItems").validate().element('#edit_dupRecord');
	}
}
$("#cancelButton").on("click", function(){
	$('#myModal2').modal('hide');
});

$('#btnD').click(function () {
	var id = $('#myModala').data('itemid');
	var sectionId = $('#myModala').data('sectionid');
	$.get("${contextPath}/admin/deleteItem?itemId="+id , function(status) {
		if(status=="success" ){
			$('[data-id=' + id + ']').remove();
			$('#myModala').modal('hide');
			$('#viewSectionTab').removeClass('active');
		  	window.location.href="${contextPath}/admin/homePageSections?deleteMsg="+status+"&sectionId="+sectionId+"&itemTab=1";
		}
		if(status=="failure" ){
			$('#myModala').data('id', id).modal('show');
			$('#viewSectionTab').removeClass('active');
			 window.location.href="${contextPath}/admin/homePageSections?deleteMsg="+status;
		}
	});
});

$('span#deleteItems').on('click', function () {
	var id = $(this).closest('tr').data('itemid');
	var sectionid = $(this).closest('tr').data('sectionid');
	var title= $(this).closest('tr').data('title');
	$('#deleteItemId').text(title+" "+ " Item will be deleted. Do you want to continue?");
	$('#myModala').data('sectionid',sectionid);
	$('#myModala').data('itemid', id);
	$('#myModala').data('id', id).modal('show');
});
function UploadFooterImage(id,type,url){
	
	var fileUpload = $("#"+id)[0];			 
	checkFileUpload(fileUpload);
	/* if(!uploadFlag){
		return false;
	} */
	
   	var formData = new FormData();
   	formData.append('file',$("#"+id)[0].files[0]);	
   	if($("#homepageSectionBean").validate().element("#"+id)){		
  			$.ajax({
			url: "${contextPath}/admin/updatepartnerLogo?type="+type,
		  	data: formData,
			dataType: 'text',
			processData: false,
			contentType: false,
			type: 'POST',
			  	success: function (response) {
   			 	$('#'+url).val(response);			 
   			 	var newImageUrl='${contextPath}'+'/admin/images/'+type+"/"+response;
   			 	
   			 imageUploadFlag=true;
   				$('#footerImage1_').prop('src',newImageUrl);
   			    $('#footerImageUrl1').val(response);
   			 
   			 $("#homepageSectionBean").validate().element("#footerImageUrl1Hidden");
   			 $("#homepageSectionBean").validate().element("#"+url);
  			  	}
  			  	
		});		
   	}
}

function UploadFooterImage2(id,type,url){
	
	var fileUpload = $("#"+id)[0];			 
	//checkFileUpload(fileUpload);
	/* if(!uploadFlag){
		return false;
	} */
	
   	var formData = new FormData();
   	formData.append('file',$("#"+id)[0].files[0]);	
   	if($("#homepageSectionBean").validate().element("#"+id)){		
  			$.ajax({
			url: "${contextPath}/admin/updatepartnerLogo?type="+type,
		  	data: formData,
			dataType: 'text',
			processData: false,
			contentType: false,
			type: 'POST',
			  	success: function (response) {
   			 	$('#'+url).val(response);			 
   			 	var newImageUrl='${contextPath}'+'/admin/images/'+type+"/"+response;
   			 	
   			 imageUploadFlag=true;
   				$('#footerImage2_').prop('src',newImageUrl);
   			    $('#footerImageUrl2').val(response);
   			 
   			 $("#homepageSectionBean").validate().element("#footerImageUrl2Hidden");
   			 $("#homepageSectionBean").validate().element("#"+url);
  			  	}
  			  	
		});		
   	}
}

$.validator.addMethod('imageDimention1',
		function(value, element) {
			var fileUpload = $("#footerImage1")[0];
			checkFileUpload(fileUpload, "footerImage1", "editFooterError1");
			if (imageUploadFlag) {
				return uploadFlag;
			} else {
				return true;
			}

		}, '');
$.validator.addMethod('imageDimention2',
		function(value, element) {
			var fileUpload = $("#footerImage2")[0];
			checkFileUpload(fileUpload, "footerImage2", "editFooterError2");
			if (imageUploadFlag) {
				return uploadFlag;
			} else {
				return true;
			}

		}, '');
function checkFileUpload1(fileUpload, type, footerStatus) {
	//Check whether the file is valid Image.
	//var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:\().+(\)\.def\().+(\])+(.jpg|.jpeg|.png|.gif)$");
	var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:\().+(\)\.def\().+(\])+(.png)$");

	if (regex.test(fileUpload.value.toLowerCase())) {
		imageUploadFlag = true;
		//Check whether HTML5 is supported.
		if (typeof (fileUpload.files) != "undefined") {
			//Initiate the FileReader object.
			var reader = new FileReader();
			//Read the contents of Image File.
			reader.readAsDataURL(fileUpload.files[0]);

			reader.onload = function(e) {
				//Initiate the JavaScript Image object.
				var image = new Image();
				//Set the Base64 string return from FileReader as source.
				image.src = e.target.result;

				image.onload = function() {

					//Determine the Height and Width.
					var height = this.height;
					var width = this.width;

					var errorText = "";
					uploadFlag = false;

					/* if (type == "footerImage1") { */
						/* if (width == 20 && height == 20) {
							uploadFlag = true;
						} else {
							uploadFlag = false;
							errorText = "Height and Width must be 20 and 20";
						} */
						/*}  else {
						if (width == 233 && height == 144) {
							uploadFlag = true;
						} else {
							uploadFlag = false;
							errorText = "Height and Width must be 144 and 233 for Static Banners.";
						}
					} */

					$("#" + footerStatus).css({
						"display" : ""
					});
					$("#" + footerStatus).html("<p>" + errorText + "</p>");

					if (errorText != "") {
						$("#footerImageUrl-error").css({
							"display" : "none"
						});
					}
					return uploadFlag;
				};
			};
		}
	} else {
		imageUploadFlag = false;
		uploadFlag = false;
	}
}
$('#footerImage1').on('change', function() {
	$('#footerImageUrl1').val($(this).val());

});
$('#footerImage2').on('change', function() {
	$('#footerImageUrl2').val($(this).val());

});

$('#viewLink').click(function() {
	if (($("#viewLink:checked").length) > 0) {
		$("#redirectionPage").removeAttr("disabled");
	} else {
		$("#redirectionPage").attr("disabled", "disabled");
		$("#redirectionPage").val("");

	}
});
$('#redirectionPage').on('change',function(){
	var redirect = 	$("#redirectionPage").val();
	var view = $('#viewLink').val();
	if(view=="on" && redirect!=''){
	$("#homepageSectionBean").validate().element('#viewLink');
	}
});
$("#displayPrtPopup").on('change', function() {
    if($(this).is(":checked")) {
    	$('#displayPrtPopup').val(1);
    }
    else{
    	$('#displayPrtPopup').val(0);
    	
    }
});
$("#edit_displayPrtPopup").on('change', function() {
    if($(this).is(":checked")) {
    	$('#edit_displayPrtPopup').val(1);
    }
    else{
    	$('#edit_displayPrtPopup').val(0);
    }
});
/* $("#footerImageUrl1").on('click',function(){
	$("#footerImage1").val("");
	$("#footerImageUrl1").val("");
});
$("#footerImageUrl2").on('click', function(){
	$('#footerImage2').val('');
	$("#footerImageUrl2").val('');
}); */


	</script>