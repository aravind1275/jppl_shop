<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<section>
	<div id="breadcrumb">
		<ul>
			<!--- breadcrumb--->
			<li>Home</li>
			<li><strong>Dashboard</strong></li>
		</ul>
	</div>
	<h5>Admin Dashboard</h5>
	<c:if test="${fn:length(successMessage) gt 0}">
		<div id="msg" align="center" style="color: blue; font-weight: normal;">
			<c:out value="${successMessage}"></c:out>
		</div>
	</c:if>
</section>

