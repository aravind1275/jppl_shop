<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!------ Commission Management ------>
<div class='container'>
	<section class="insurance_management">
		<div id="breadcrumb">
			<ul>
				<li>Home</li>
				<li>Configuration</li>
				<li>Commission</li>
			</ul>
		</div>
		<!---breadcrumb--->

		<div class="section_heading">
			<c:if test="${fn:length(message) gt 0}">
				<div class="alertMessage" style="color: green; font-weight: bold;">
					<c:out value="${message}"></c:out>
				</div>
			</c:if>
			<c:if test="${fn:length(errormessage) gt 0}">
				<div class="alertMessage" style="color: red; font-weight: bold;">
					<c:out value="${errormessage}"></c:out>
				</div>
			</c:if>
			<h5>Commission Management</h5>
		</div>
		<!--- section heading ---->

		<div class="management">
			<!---User Management--->
			<ul class="nav nav-tabs">
				<li id="Commission_" class="active"><a data-toggle="tab"
					href="#sectionA">Commission</a></li>
				<li id="addCommission_"><a data-toggle="tab" href="#sectionB">Add
						Commission</a></li>
				<li id="addJPMiles_"><a data-toggle="tab" href="#sectionC">Configuration
						for JPMiles</a></li>
				<li id="uploadCommission"><a data-toggle="tab" href="#sectionD">File
						Uploading of Commission</a></li>
			</ul>
			<!--- sub tabs ------>

			<div class="tab-content">
				<div id="sectionA" class="tab-pane fade in active">
					<nav class="navbar navbar-default" role="navigation" id="submenu">
						<!-----existing user starts here---->
						<form:form autocomplete="off" method="GET" action="commission"
							id="viewCommissionBean" commandName="viewCommissionBean">
							<!-----Commission Products starts here---->
							<div class="row pad_top">
								<div class="col-md-3 partner_no">
									<label>Select a Partner</label>
									<fieldset class="infield">
										<form:select name="prtNo_" path="partNo" id="prtNo_"
											class="select_option">
											<form:option value="">-Select Partner-</form:option>
											<c:forEach items="${partnerList}" var="partner">
												<form:option value="${partner.prtNo}"
													label="${partner.partnerName}" />
											</c:forEach>
										</form:select>
									</fieldset>
									<div class="error hidden">Partner should be selected</div>
								</div>

								<!--Added start and end date by pawan-->
								<div class="col-md-3">
									<label>Start Date</label>
									<div class="pick_date start_date">
										<form:input type="text" class="form-control startDate"
											id="fromDate" path="activationDate" name="fromDate"
											readonly="true" />
										<p id="log" style="display: none; color: red;"></p>
									</div>
									<div class="error hidden">Start date should be selected</div>
								</div>
								<div class="col-md-3">
									<label>End Date</label>
									<div class="pick_date end_date">
										<form:input type="text" class="form-control endDate"
											id="toDate" name="toDate" path="deactivationDate"
											readonly="true" />
										<p id="log" style="display: none; color: red;"></p>
									</div>
									<div class="error hidden">End date should be selected</div>
								</div>
								<div class="col-md-3 comm_report_button">
									<input class="bg_w_button" type="button" id="commissionClear"
										value="Clear" /> <input class="bg_w_button" id="search"
										type="button" value="Search" />
								</div>
							</div>
							<!--- row --->
						</form:form>
						<%-- <c:if test="${not empty commissionList}"> --%>
						<a  href="/afadmin/admin/commissionExportCsv" class="bg_w_button odexp" id="export">export to csv</a>
							<table id="table" class=" partner_info_table table-bordered commission "
								cellspacing="0" cellpadding="0" width="98%">
								<thead>
									<tr>
									     <th>Partner No</th>
										<th>Partner Category No</th>
										<th>Product Name NO</th>
										<th>Partner</th>
										<th>Partner Category</th>
										<th>Product Name</th>
										<!-- <th>Brand Name</th>
										<th>Category Path</th> -->
										<th>Comm. Type</th>
										<th>Transaction Type</th>
										<th>Partner Comm.</th>
										<th>JPMiles Comm.</th>
										<th>Lower Value</th>
										<th>Upper Value</th>
										<th>Start Date</th>
										<th>End Date</th>
										<th>Comm. Status</th>
										<th class="width_section">Action</th>
									</tr>
								</thead>
								<!----thead--->
							<%-- 	<tbody>
									<c:forEach items="${commissionList}" var="commission"
										varStatus="i">
										<tr class="admin btnDelete1" data-id="${commission.prtCommNo}"
											data-partno="${commission.partNo}"
											data-partname="${commission.partnerName}"
											data-partcategoryno="${commission.prtCatNo}"
											data-partcategoryname="${commission.prtCategoryName}"
											data-brandname="${commission.productBrand}"
											data-categorypath="${commission.categoryPath}"
											data-partproductno="${commission.prtProductNo}"
											data-partproductname="${commission.prtProductName}"
											data-commissionrate="${commission.commissionRate}"
											data-membershare="${commission.memberShare}"
											data-activationdate="${commission.activationDate}"
											data-deactivationdate="${commission.deactivationDate}"
											data-commissionstatus="${commission.commissionSatus}"
											data-commissionstatuscode="${commission.commissionStatusCode}"
											data-commissionstatusclass="${commission.commissionStatusClass}"
											data-maxjpmiles="${commission.maxJpmiles}"
											data-commissiontype="${commission.commisionType}"
											data-lowervalue="${commission.lowerValue}"
											data-uppervalue="${commission.upperValue}"
											data-bonuscommissiontype="${commission.bonusCommissionType}"
											data-bonuscommissionrate="${commission.bonusCommissionRate}"
											data-bonuspartnercode="${commission.bonusPartnerCode}"
											data-bonusactivitycode="${commission.bonusActivityCode}">
											<td class="slno_width">${i.count}</td>
											<td class="td_width">${commission.partnerName}</td>
											<td class="td_width">${commission.prtCategoryName}</td>
											<td class="td_width">${commission.prtProductName}</td>
											<td class="td_width">${commission.productBrand}</td>
											<td class="td_width"><c:out
													value="${commission.categoryPath}" /></td>
											<c:choose>
												<c:when test="${commission.commisionType eq 2}">
													<td class="td_width">Flat Amount</td>
												</c:when>
												<c:otherwise>
													<td class="td_width">Percentage</td>
												</c:otherwise>
											</c:choose>
											<td class="td_width">${commission.commissionRate}</td>
											<td class="td_width">${commission.memberShare}</td>
											<td class="td_width">${commission.lowerValue}</td>
											<td class="td_width">${commission.upperValue}</td>
											<td class="td_width">${commission.activationDate}</td>
											<td class="td_width">${commission.deactivationDate}</td>
											<td class="${commission.commissionStatusClass} td_width">${commission.commissionSatus}</td>
											<td class="td_width"><span class="view_details"
												data-target="#myModal2"> <img
													src="${contextPath}/static/img/View_details_icon1.png"></img>
											</span> <span class="editcommission" data-target="#myModal1">
													<img src="${contextPath}/static/img/edit_icon.png"
													class="edit_wraper"></img>
											</span> <span class="addcommission_" data-target="#myModalab">
													<img src="${contextPath}/static/img/001446.png"
													class="fa fa-trash fa-lg"></img>
											</span></td>
										</tr>
									</c:forEach>
								</tbody> --%>
								<!---tbody--->
							</table>
							<!---table-->
					<%-- 	</c:if> --%>
					</nav>
				</div>
				<!----section A---->
				<!-----Add commission starts here---->
				<div id="sectionB" class="tab-pane fade submenu">
					<form:form autocomplete="off" action="addCommission" method="POST"
						name="commissionBean" id="commissionBean"
						commandName="commissionbean">
						<div class="row pad_top">
							<div class="col-md-4">
								<label for="country">Partners</label><span
									class="mandatory_star">*</span>
								<form:select path="partNo" name="partnerNo" id="partnerNo"
									class="select_option">
									<form:option value="">--Select--</form:option>
									<c:forEach items="${partnerList}" var="partner">
										<li><form:option value="${partner.prtNo}"
												label="${partner.partnerName}" /></li>
									</c:forEach>
								</form:select>
							</div>
							<div class="col-md-4">
								<label for="state">Partner Categories</label>
								<!-- <span class="mandatory_star">*</span> -->
								<form:select path="prtCatNo" name="partnerCategory"
									id="partnerCategory" class="select_option">
									<form:option value="">--Select--</form:option>
								</form:select>
							</div>
							<div class="col-md-4">
								<label>Partner Products</label> <input type='text' id="rpName"
									name="rpName" placeholder=" What's your Wish?"
									class="form-control" />
								<form:input type="hidden" path="prtProductNo" id="prtProductNo" />
							</div>
						</div>
						<!--- row --->

						<div class="row pad_top">
							<div class="col-md-4">
								<label>Category Path</label>
								<form:input type='text' maxlength="100" path="categoryPath"
									name="categoryPath" id="categoryPath" class="form-control" />
							</div>
							<div class="col-md-4">
								<label>Maximum JPMiles can be Earned</label>
								<form:input type='text' name="maxJpmiles" id="maxJpmiles"
									path="maxJpmiles" class="form-control" maxlength="6" />
							</div>
						</div>
						<!------------- row ----------->

						<div class="row pad_top">
							<div class="col-md-10">
								<label>Partner Commission</label><span class="mandatory_star">*</span>
							</div>
							<div class="col-md-4">
								<!-- <label>Partner Commission</label><span class="mandatory_star">*</span> -->
								<div>
									<form:radiobutton class="css-checkbox" path="commisionType"
										name="commisionTypeRadio" id="flatAmount" value="2" />
									<label class="css-label radGroup2" for="flatAmount">Flat
										Amount</label>
									<form:radiobutton class="css-checkbox" path="commisionType"
										name="partnerTypeRadio" id="percentage" value="1" />
									<label class="css-label radGroup2" for="percentage">Percentage</label>
								</div>
								<form:input type='text' maxlength="6" name="commissionRate"
									id="commissionRate" path="commissionRate" class="form-control" />
							</div>
							<div class="col-md-4">
								<label>JPMiles Commission</label><span class="mandatory_star">*</span>
								<form:input maxlength="6" type='text' name="memberShare"
									id="memberShare" path="memberShare" class="form-control" />
							</div>
							<div class="col-md-4">
								<label>Commission Status</label><span class="mandatory_star">*</span>
								<div class="half-col last">
									<ul class="list-inline">
										<c:forEach items="${commissionStatus}" var="status">
											<li><label> <form:radiobutton
														path="commissionStatusCode" class="css-checkbox"
														id="${status.key}" value="${status.key}" /> <label
													for="${status.key}" class="css-label radGroup2">${status.value}</label>
											</label></li>
										</c:forEach>
									</ul>
								</div>
							</div>
						</div>
						<!------------- row ----------->

						<div class="row pad_top">
							<div class="col-md-10">
								<label>Bonus JPMiles Commission</label>
							</div>

							<div class="col-md-4">
								<div class="half-col last">
									<ul class="list-inline">
										<li><form:radiobutton class="css-checkbox commTypeRadio"
												path="bonusCommissionType" name="bonusCommissionType"
												id="bonusCommissionType_flatAmount" value="2" /> <label
											class="css-label radGroup2"
											for="bonusCommissionType_flatAmount">Flat Amount</label></li>

										<li><form:radiobutton class="css-checkbox commTypeRadio"
												path="bonusCommissionType" name="bonusCommissionType"
												id="bonusCommissionType_percentage" value="1" /> <label
											class="css-label radGroup2"
											for="bonusCommissionType_percentage">Percentage</label></li>
									</ul>
								</div>

								<form:input type='text' maxlength="6" name="bonusCommissionRate"
									id="bonusCommissionRate" path="bonusCommissionRate"
									class="form-control" readonly="true" />
							</div>

							<div class="col-md-4">
								<label>Bonus Partner Code</label>
								<form:input type='text' name="bonusPartnerCode"
									id="bonusPartnerCode" path="bonusPartnerCode"
									class="form-control" />
							</div>

							<div class="col-md-4">
								<label>Bonus Activity Code</label>
								<form:input type='text' name="bonusActivityCode"
									id="bonusActivityCode" path="bonusActivityCode"
									class="form-control" />
							</div>
						</div>
						<!-- row -->

						<div class="row pad_top">
							<div class="col-md-10">
								<label class="headLabel">Transaction Value Range</label>
							</div>
							<div class="col-md-4">
								<label>Lower Value</label>
								<form:input type='text' name="lowerValue" id="lowerValue"
									path="lowerValue" class="form-control" />
							</div>
							<div class="col-md-4">
								<label>Upper Value</label>
								<form:input type='text' name="upperValue" id="upperValue"
									path="upperValue" class="form-control" />
							</div>
								<div class="col-md-4">
								<label>Transaction Status</label><span class="mandatory_star">*</span>
								<div>
									<form:radiobutton class="css-checkbox" path="transactionStatus"
										name="processed" id="processed" value="2" />
									<label class="css-label radGroup2" for="processed">
										Processed</label>
									<form:radiobutton class="css-checkbox" path="transactionStatus"
										name="tentative" id="tentative" value="1" />
									<label class="css-label radGroup2" for="tentative">Tentative/Processed</label>
								</div>
								
							</div>
							
						</div>
						<!-- row -->

						<div class="row pad_top pad_bottom">
							<div class="col-md-4">
								<label>Start Date</label><span class="mandatory_star">*</span>
								<div class="pick_date">
									<form:input class="form-control startDate" name="datepicker1"
										id='datepicker1' path="activationDate" />
								</div>
							</div>

							<div class="col-md-4">
								<label>End Date</label>
								<div class="pick_date">
									<form:input class="form-control endDate" name="datepicker2"
										id='datepicker2' path="deactivationDate" />
								</div>
								<input type="hidden" id="selectEnddate" name="selectEnddate" />
							</div>
						</div>
						<!------------- row ----------->
						<div class="row btn_save pull-right">
							<input class="bg_g_button" type="button" id="cancelbtn_"
								value="Cancel" /> <input class="bg_g_button" type="button"
								id="save" value="Save" />
						</div>
						<!-------------button----------->
					</form:form>
				</div>
				<!----section B---->

				<!-- Section C Starts -->
				<div id="sectionC" class="tab-pane fade submenu mapKeyword">
					<form id="jpMileConfigForm" name="jpMileConfigForm" method="POST"
						action="ConfingureJPMile">
						<div class="row pad_top">
							<div class="col-md-4">
								<label>JPMiles for Products</label><span class="mandatory_star">*</span>
								<input id="jpMileConfig" maxlength="3" name="jpMileConfig"
									type="text" class="form-control"
									value="${requestScope.configJPMiles}" />
							</div>
						</div>
						<div class="row btn_save pull-right">
							<a href="${contextPath}/admin/commission"><input
								class="bg_g_button" type="button" value="Cancel" id="

" /></a>
							<button type="button" class="bg_g_button"
								onClick="$(this).closest('form').submit();">Save</button>
						</div>
					</form>
				</div>
				<div id="sectionD" class="tab-pane fade submenu">
					<form id="uploadCSVForm" name="uploadCSVForm" method="POST"
						action="uploadCommissionFile" enctype="multipart/form-data">
						<div class="row pad top">
							<div class="col-md-3 upload_keyword">
								<br> <label>Upload Commission File</label><span
									class="mandatory_star">*</span>
								<div class="fileUpload btn ">
									<input name="uploadFile2" id="uploadFile2" class="upload_file"
										placeholder=".csv file only" disabled="disabled" /> <span
										class="bg_w_button" style="position: relative; top: 5px;">Browse</span>
									<input type="file" name="uploadCSVFile" id="uploadCSVFile"
										class="upload uploadfile_button" /> <input type="hidden"
										id="filename" name="filename" /> <br>
								</div>
							</div>
							<div class="col-md-1">
								<p id="log" style="display: none; color: red;"></p>
								<div class="fileUpload btn"
									style="position: relative; top: 16px; right: 12px">
									<button type="submit" class="bg_g_button" id="UploadFile">Upload</button>

								</div>
							</div>

						</div>
					</form>
				</div>
				<!-- Section D Ends -->

			</div>
			<!-----tab-content---->
		</div>
		<!---- user managemnt----->
	</section>
</div>


<!------ Modal content for view details-------->
<div id="myModal2" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content for view details-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title">Partner Commission Details</h4>
			</div>
			<!----modal---header--->

			<div class="modal-body2">
				<div class="row">
					<div class='col-md-6 user_details '>
						<label>Partner Name</label>
						<p id="partnername"></p>
					</div>
					<div class='col-md-6 user_details'>
						<label>Partner Category</label>
						<p id="partnercategoryname"></p>
					</div>
				</div>
				<!----row--->

				<div class="row">
					<div class='col-md-6 user_details'>
						<label>Product Name</label>
						<p id="productname"></p>
					</div>

				</div>
				<!----row--->

				<div class="row">
					<!-- <div class='col-md-6 user_details'>
						<label>Brand Name</label>
						<p id="brandname"></p>
					</div> -->
					<div class=' col-md-6 user_details '>
						<label>Category Path</label>
						<p id="categorypath"></p>
					</div>
				</div>
				<!----row--->

				<div class="row">
					<div class='col-md-6 user_details'>
						<label>JPmiles Commission</label>
						<p id="jpmilecommission"></p>
					</div>
					<div class='col-md-6 user_details'>
						<label>Start Date</label>
						<p id="startdate"></p>
					</div>
				</div>
				<!----row--->

				<div class="row">
					<div class=' col-md-6 user_details '>
						<label>End Date</label>
						<p id="enddate"></p>
					</div>
					<div class='col-md-6 user_details'>
						<label>Commission Status</label>
						<p id="commissionstatus"></p>
					</div>
				</div>
				<!----row--->
				<!----row--->

				<div class="row">
					<div class=' col-md-6 user_details '>
						<label>Lower Value</label>
						<p id="view_lowerValue"></p>
					</div>
					<div class='col-md-6 user_details'>
						<label>Upper Value</label>
						<p id="view_upperValue"></p>
					</div>
				</div>
				<div class="row">
					<div class=' col-md-6 user_details '>
						<label>Commission Type</label>
						<p id="commType"></p>
					</div>
					<div class=' col-md-6 user_details '>
						<label>Partner Commission</label>
						<p id="partnercommission"></p>
					</div>
				</div>
				<div class="row">
				<div class=' col-md-6 user_details '>
						<label>Transaction Status</label>
						<p id="transactionStatus"></p>
					</div>
				</div>
			</div>
			<!----modal-body---->
		</div>
	</div>
</div>


<!------ Modal  for edit-details ------>
<div id="myModal1" class="modal fade" role="dialog">
	<form:form autocomplete="off" id="editCommissionBean">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" id="cancelbtnD" class="close"
						data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Edit Commission Details</h4>
				</div>
				<!----modal-header--->

				<div class="modal-body1">
					<div class="row pad_top">
						<div class="col-md-4 user_details">
							<label>Partner Name</label> <input type="text" maxlength="25"
								class="form-control " name="edit_partnername"
								id="edit_partnername" readonly> <input type="hidden"
								id="edit_prtcommno">
						</div>
						<div class="col-md-4 user_details">
							<label>Partner Category</label> <input type="text"
								maxlength="150" class="form-control "
								name="edit_partnercategoryname" id="edit_partnercategoryname"
								readonly>
						</div>
						<div class="col-md-4 user_details">
							<label>Partner Product</label> <input type="text" maxlength="250"
								class="form-control " name="edit_partnerproductname"
								id="edit_partnerproductname" readonly>
						</div>
					</div>
					<!----row--->

					<div class="row pad_top">
						<!-- <div class="col-md-4 user_details">
							<label>Brand Name</label> <input type="text" maxlength="100"
								class="form-control " name="edit_brandName" id="edit_brandName"
								readonly>
						</div> -->
						<div class="col-md-4 user_details">
							<label>Category Path</label> <input type="text" maxlength="100"
								class="form-control " name="edit_categoryPath"
								id="edit_categoryPath" readonly>
						</div>

						<!-- <div class="col-md-4 user_details">
							<label>Partner Commission</label> <input type="text"
								maxlength="6" class="form-control "
								name="edit_partnercommission" id="edit_partnercommission">
						</div> -->
					</div>
					<!----row--->

					<div class="row pad_top">
						<div class="col-md-4 user_details">
							<label>JPmiles Commission</label> <input type="text"
								maxlength="6" class="form-control " name="edit_jpmilecommission"
								id="edit_jpmilecommission">
						</div>
						<div class="col-md-4 user_details">
							<label>Maximum JPMiles can be Earned</label> <input type="text"
								maxlength="6" class="form-control" name="edit_maxJpmiles"
								id="edit_maxJpmiles" />
						</div>
						<div class="col-md-4">
							<div class='text top'>
								<label>Commission Status</label>
								<div class="half-col last">
									<ul class="list-inline" style="display: inline-flex;">
										<c:forEach items="${commissionStatus}" var="status">
											<li><label> <input type="radio"
													name="edit_commissionstatus" class="css-checkbox"
													id="edit_commissionstatus_${status.key}"
													value="${status.key}" /> <label
													for="edit_commissionstatus_${status.key}"
													class="css-label radGroup2">${status.value}</label>
											</label></li>
										</c:forEach>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!----row--->
					<div class="row pad_top">
						<div class="col-md-5 user_details">
							<label>Partner Commission</label>
							<div class="half-col last">
								<ul class="list-inline">
									<li><input type="radio" name="edit_commType"
										class="css-checkbox edit_commType" id="edit_flatAmount"
										value="2" disabled /> <label for="edit_flatAmount"
										class="css-label radGroup2">Flat Amount</label></li>
									<li><input type="radio" name="edit_commType"
										class="css-checkbox edit_commType" id="edit_Percentage"
										value="1" disabled /> <label for="edit_Percentage"
										class="css-label radGroup2">Percentage</label></li>
								</ul>
							</div>
							<input type="text" maxlength="6" class="form-control "
								name="edit_partnercommission" id="edit_partnercommission">
						</div>
						<div class="col-md-5 user_details">
								<label>Transaction Status</label>
								<div class="half-col last">
								<ul class="list-inline">
									<li><input type="radio" name="edit_transactionStatus"
										class="css-checkbox edit_commType" id="edit_processed"
										value="2"  /> <label for="edit_processed"
										class="css-label radGroup2">Processed</label></li>
									<li><input type="radio" name="edit_transactionStatus"
										class="css-checkbox edit_commType" id="edit_tentative"
										value="1"  /> <label for="edit_tentative"
										class="css-label radGroup2">Tentative</label></li>
								</ul>
							</div>
								
							</div>
					</div>
					<div class="row pad_top">
						<div class="col-md-10">
							<label class="headLabel">Transaction Value Range</label>
						</div>
						<div class="col-md-4 user_details">
							<label>Lower Value</label> <input type="text"
								class="form-control " name="edit_lowerValue"
								id="edit_lowerValue" readonly />
						</div>
						<div class="col-md-4 user_details">
							<label>Upper Value</label> <input type="text"
								class="form-control" name="edit_upperValue" id="edit_upperValue"
								readonly />
						</div>
					</div>
					<!-- row -->

					<div class="row pad_top">
						<div class="col-md-10">
							<label>Bonus JPMiles Commission</label>
						</div>

						<div class="col-md-5 user_details">
							<div class="half-col last">
								<ul class="list-inline">
									<li><input type="radio" class="css-checkbox"
										name="edit_bonusCommissionType"
										id="edit_bonusCommissionType_2" value="2" disabled /> <label
										class="css-label radGroup2" for="edit_bonusCommissionType_2">Flat
											Amount</label></li>

									<li><input type="radio" class="css-checkbox"
										name="edit_bonusCommissionType"
										id="edit_bonusCommissionType_1" value="1" disabled /> <label
										class="css-label radGroup2" for="edit_bonusCommissionType_1">Percentage</label>
									</li>
								</ul>
							</div>

							<input type='text' maxlength="6" name="edit_bonusCommissionRate"
								id="edit_bonusCommissionRate" class="form-control" readonly />
						</div>
					</div>
					<!-- row -->

					<div class="row pad_top">
						<div class="col-md-4 user_details">
							<label>Bonus Partner Code</label> <input type='text'
								name="edit_bonusPartnerCode" id="edit_bonusPartnerCode"
								class="form-control" readonly />
						</div>

						<div class="col-md-4 user_details">
							<label>Bonus Activity Code</label> <input type='text'
								name="edit_bonusActivityCode" id="edit_bonusActivityCode"
								class="form-control" readonly />
						</div>
					</div>
					<!-- row -->


					<div class="row pad_top">
						<div class="col-md-4">
							<label>Start Date</label>
							<div class="pick_date">
								<input type="text" class="form-control startDate"
									name="datepicker11" id="datepicker11" readonly> <input
									type="hidden" name="datepicker11Hidden" id="datepicker11Hidden">
							</div>
						</div>
						<div class="col-md-4">
							<label>End Date</label>
							<div class="pick_date">
								<input type="text" class="form-control endDate"
									name="datepicker12" id="datepicker12"> <input
									type="hidden" name="datepicker12Hidden" id="datepicker12Hidden">
							</div>
						</div>
					</div>
					<!------------- row ----------->

					<div class="row btn_save">
						<input class="bg_g_button" type="button" id="updateCommission"
							value="Update" />
					</div>
					<!-------------button----------->
				</div>
			</div>
		</div>
	</form:form>
</div>


<!------ Modal content for Add commissionion ------>
<div id="myModalab" class="modal fade" role="d	ialog">
	<form:form autocomplete="off" id="addCommissionBean">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" id="cancelbtnD_" class="close"
						data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Add Commission Details</h4>
				</div>
				<!----modal-header--->

				<div class="modal-body1">
					<div class="row pad_top">
						<div class="col-md-4 user_details">
							<label>Partner Name</label> <input type="text" minlength="3"
								maxlength="25" class="form-control " name="add_partnername"
								id="add_partnername" readonly>
						</div>
						<div class="col-md-4 user_details">
							<label>Partner Category</label> <input type="text"
								maxlength="150" class="form-control "
								name="add_partnercategoryname" id="add_partnercategoryname"
								readonly>
						</div>
						<div class="col-md-4 user_details">
							<label>Partner Product</label> <input type="text" maxlength="250"
								class="form-control " name="add_partnerproductname"
								id="add_partnerproductname" readonly>
						</div>
					</div>
					<!----row--->

					<div class="row pad_top">
						<div class="col-md-4 user_details">
							<label>Brand Name</label> <input type="text" maxlength="100"
								class="form-control " name="add_brandName" id="add_brandName"
								readonly>
						</div>
						<div class="col-md-4 user_details">
							<label>Category Path</label> <input type="text" maxlength="100"
								class="form-control " name="add_categoryPath"
								id="add_categoryPath" readonly>
						</div>
					</div>
					<!----row--->

					<!----row--->
					<div class="row pad_top">
						<!-- <div class="col-md-4 user_details">
							<label>Partner Commission</label> <input type="text"
								maxlength="6" class="form-control " name="add_partnercommission"
								id="add_partnercommission">
						</div> -->
						<div class="col-md-4 user_details">
							<label>JPmiles Commission</label> <input type="text"
								maxlength="6" class="form-control " name="add_jpmilecommission"
								id="add_jpmilecommission">
						</div>
						<div class="col-md-4 user_details">
							<label>Max JPMiles can be Earned</label> <input type="text"
								maxlength="6" class="form-control " name="add_maxJpmiles"
								id="add_maxJpmiles">
						</div>
					</div>
					<!--  -->
					<div class="row pad_top">
						<div class="col-md-6 user_details">
							<label>Partner Commission</label>
							<div class="half-col last">
								<ul class="list-inline">
									<li><input type="radio" name="add_commType"
										class="css-checkbox edit_commType" id="add_flatAmount"
										value="2" /> <label for="add_flatAmount"
										class="css-label radGroup2">Flat Amount</label></li>
									<li><input type="radio" name="add_commType"
										class="css-checkbox add_commType" id="add_Percentage"
										value="1" /> <label for="add_Percentage"
										class="css-label radGroup2">Percentage</label></li>
								</ul>
							</div>
							<input type="text" maxlength="6" class="form-control "
								name="add_partnercommission" id="add_partnercommission">
						</div>
					</div>
					<div class="row pad_top">
						<div class="col-md-10">
							<label class="headLabel">Transaction Value Range</label>
						</div>
						<div class="col-md-4 user_details">
							<label>Lower Value</label> <input type="text"
								class="form-control " name="add_lowerValue" id="add_lowerValue" />
						</div>
						<div class="col-md-4 user_details">
							<label>Upper Value</label> <input type="text"
								class="form-control" name="add_upperValue" id="add_upperValue" />
						</div>
					</div>
					<!-- row -->

					<div class="row pad_top">
						<div class="col-md-10">
							<label>Bonus JPMiles Commission</label>
						</div>

						<div class="col-md-6 user_details">
							<div class="half-col last">
								<ul class="list-inline">
									<li><input type="radio"
										class="css-checkbox add_commTypeRadio"
										name="add_bonusCommissionType" id="add_bonusCommissionType_2"
										value="2" /> <label class="css-label radGroup2"
										for="add_bonusCommissionType_2">Flat Amount</label></li>

									<li><input type="radio"
										class="css-checkbox add_commTypeRadio"
										name="add_bonusCommissionType" id="add_bonusCommissionType_1"
										value="1" /> <label class="css-label radGroup2"
										for="add_bonusCommissionType_1">Percentage</label></li>
								</ul>
							</div>

							<input type='text' maxlength="6" name="add_bonusCommissionRate"
								id="add_bonusCommissionRate" class="form-control" readonly />
						</div>
						
						<div class="col-md-6 user_details">
							<label>Transaction Status</label>
							<div class="half-col last">
								<ul class="list-inline">
									<li><input type="radio" name="add_transactionStatus"
										class="css-checkbox edit_commType" id="add_processed"
										value="2"  /> <label for="add_processed"
										class="css-label radGroup2">Processed</label></li>
									<li><input type="radio" name="add_transactionStatus"
										class="css-checkbox edit_commType" id="add_tentative"
										value="1"  /> <label for="add_tentative"
										class="css-label radGroup2">Tentative</label></li>
								</ul>
							</div>
						</div>
						
					</div>
					<!-- row -->

					<div class="row pad_top">
						<div class="col-md-6 user_details">
							<label>Bonus Partner Code</label> <input type='text'
								name="add_bonusPartnerCode" id="add_bonusPartnerCode"
								class="form-control" />
						</div>

						<div class="col-md-6 user_details">
							<label>Bonus Activity Code</label> <input type='text'
								name="add_bonusActivityCode" id="add_bonusActivityCode"
								class="form-control" />
						</div>
					</div>
					<!-- row -->

					<div class="row pad_top">
						<div class="col-md-4">
							<label>Start Date</label>
							<div class="pick_date">
								<input class="form-control startDate" name="datepicker13"
									id="datepicker13" />
							</div>
						</div>
						<div class="col-md-4">
							<label>End Date</label>
							<div class="pick_date">
								<input class="form-control endDate" name="datepicker14"
									id="datepicker14" />
							</div>
						</div>
						<div class="col-md-4">
							<div class='text top'>
								<label>Commission Status</label>
								<div class="half-col last">
									<ul class="list-inline" style="display: inline-flex;">
										<c:forEach items="${commissionStatus}" var="status">
											<li><label> <input type="radio"
													name="add_commissionstatus" class="css-checkbox"
													id="add_commissionstatus_${status.key}"
													value="${status.key}" /> <label
													for="add_commissionstatus_${status.key}"
													class="css-label radGroup2">${status.value}</label>
											</label></li>
										</c:forEach>
									</ul>
								</div>
							</div>
						</div>

					</div>
					<!------------- row ----------->


					<div class="row pad_top">
						<div class="row btn_save">
							<input class="bg_g_button" type="button" id="addNewCommission"
								value="Add" />
						</div>
						<!-------------button----------->
					</div>
				</div>
			</div>
		</div>
	</form:form>
</div>

<!-- model upload commission-->


<script>
	$(document).ready(
			function() {
				var d = new Date(); // for now
				var h = d.getHours();
				h = (h < 10) ? ("0" + h) : h;
				var m = d.getMinutes();
				m = (m < 10) ? ("0" + m) : m;
				var s = d.getSeconds();
				s = (s < 10) ? ("0" + s) : s;
				var month = d.getMonth() + 1;
				var day = d.getDate();
				var year = d.getFullYear();
				datetext = day + "-" + month + "-" + year + "_" + h + ":" + m
						+ ":" + s;

				/* $('.partner_info_table').DataTable({
					"paging" : true,
					"searching" : false,
					"lengthChange" : false,
					"info" : false,
					"iDisplayLength" : 50,
					// 					"order" : [ [ 4, "asc" ] ],
					"dom" : 'Blfrtip',
					//"scrollX":        true,
					"searching" : false,
					//      	scrollCollapse: true,
					//      	columnDefs: [
					//      			{ width: '243', targets: '_all' }
					//      	], */
					//"buttons" : [ {
					//	extend : 'excel',
					//	text : 'export to excel',
					/* 	title : 'view_commission_' + datetext */
					//}, ],
				//}); */

				<c:if test="${addcommissionTab==true}">
				$('#commision').removeClass('active');
				$('#sectionA').removeClass('in active');
				$('#addcommision').addClass('active');
				$('#sectionB').addClass('in active');
				$('#sectionD').removeClass('active');
				</c:if>

				$.validator.addMethod("dropdowncheck",
						function(value, element) {
							if (value == "")
								return false;
							else
								return true;
						});

				$("#editCommissionBean").validate({
					onkeyup : true

				});
				$("#commissionBean").validate({
					onkeyup : true

				});
				$("#addCommissionBean").validate({
					onkeyup : true

				});

			});

	$('#partnerNo').on(
			'selectmenuchange',
			function() {
				var obj = null;
				var partnerNo = $("#partnerNo option:selected").val();
				$("#rpName").val('');
				$("#datepicker1").trigger("change");

				$.get("${contextPath}/admin/selectPartnerCategoires?partNo="
						+ partnerNo, function(data) {
					var obj = jQuery.parseJSON(data);
					$('#partnerCategory').empty();
					$('#partnerCategory').append(
							"<option value=''>Select</option>");

					for (var i = 0; i < obj.length; i++) {
						var value = obj[i];
						var newOption = $("<option value="+value.prtCatNo+">"
								+ value.partnercategoryName + "</option>");
						$('#partnerCategory').append(newOption);
					}

					$("#partnerCategory").selectmenu("refresh");
					$("#commissionBean").validate().element('#partnerNo');
				});
			});

	/* $('span.view_details').on(
			'click',
			function() { */
				var id='', partnerName='', partcategoryname ='',partproductname='',commisionType='',commissionRate='',memberShare='',lowerValue='',upperValue='',
				deactivationDate='',activationDate='',commissionSatus='',transactionStatus='';
				 $('#table').on('click', 'a.view_details', function (e) {
					  var checka = table1.row( $(this).closest('tr')).data();
					  	id = checka.id;
						partnerName = table1.row( $(this).closest('tr')).data().partnerName;
						partcategoryname = table1.row( $(this).closest('tr')).data().prtCategoryName;
						partproductname = table1.row( $(this).closest('tr')).data().prtProductName;
						commisionType = table1.row( $(this).closest('tr')).data().commisionType;
						commissionRate = table1.row( $(this).closest('tr')).data().commissionRate;
						memberShare = table1.row( $(this).closest('tr')).data().memberShare;
						lowerValue = table1.row( $(this).closest('tr')).data().lowerValue;
						upperValue = table1.row( $(this).closest('tr')).data().upperValue;
						activationDate = table1.row( $(this).closest('tr')).data().activationDate;
						deactivationDate = table1.row( $(this).closest('tr')).data().deactivationDate;
						commissionSatus = table1.row( $(this).closest('tr')).data().commissionSatus;
						transactionStatus = table1.row( $(this).closest('tr')).data().transactionStatus;
						
				 
			/* 	var id = $(this).closest('tr').data('id');
				var partname = $(this).closest('tr').data('partname');
				var partcategoryname = $(this).closest('tr').data(
						'partcategoryname');
				var partproductname = $(this).closest('tr').data(
						'partproductname');
				var commissionrate = $(this).closest('tr').data(
						'commissionrate');
				var membershare = $(this).closest('tr').data('membershare');
				var activationdate = $(this).closest('tr').data(
						'activationdate');
				var deactivationdate = $(this).closest('tr').data(
						'deactivationdate');
				var commissionstatus = $(this).closest('tr').data(
						'commissionstatus');
				var commissionstatuscode = $(this).closest('tr').data(
						'commissionstatuscode');
				var commissionstatusclass = $(this).closest('tr').data(
						'commissionstatusclass');

				//var brandname = $(this).closest('tr').data('brandname');
				var categorypath = $(this).closest('tr').data('categorypath');
				var lowerValue = $(this).closest('tr').data('lowervalue');
				var upperValue = $(this).closest('tr').data('uppervalue');
				var commType = $(this).closest('tr').data('commissiontype');
				var transactionStatus = $(this).closest('tr').data('transactionstatus'); */
				$('#partnername').text(partnerName);
				$('#partnercategoryname').text(partcategoryname);
				$('#productname').text(partproductname);
				$('#partnercommission').text(commissionRate);
				$('#jpmilecommission').text(memberShare);
				$('#startdate').text(activationDate);
				$('#enddate').text(deactivationDate);
				//$('#brandname').text(brandname);
				//$('#categorypath').text(categorypath);
				$('#view_lowerValue').text(lowerValue);
				$('#view_upperValue').text(upperValue);
				if (commisionType == 1) {
					$('#commType').text("Commission");
				} else {
					$('#commType').text("Flat Commission");
				}
				if(transactionStatus == 2) {
					$('#transactionStatus').text("Processed");
				} else {
					$('#transactionStatus').text("Tentative/Processed");
				}
				$('#commissionstatus').text(commissionSatus);
				e.preventDefault();
				$('#myModal2').data('id', id).modal('show');

			});
</script>

<!-- edit commission details -->
<script>
	$("#editCommissionBean")
			.validate(
					{

						ignore : [],
						rules : {

							edit_partnercommission : {
								required : true,
								number : true,
							},
							edit_jpmilecommission : {
								required : true,
								number : true,
							},
							datepicker11 : {
								greaterThanEditEndDate : true,
								duplicateDatesStart : true
							},
							datepicker12 : {
								lessThanEditStartDate : true,
								duplicateDatesEnd : true
							},
							edit_maxJpmiles : {
								digits : true,
								rangelength : [ 1, 6 ]
							},
							edit_lowerValue : {
								number : true,
								editLowerValueSmallerThanHigher : true
							},
							edit_upperValue : {
								number : true,
								editUpperValueGreaterThanLower : true
							},
							edit_bonusCommissionRate : {
								number : true
							}
						},
						messages : {
							edit_partnercommission : {
								required : "Please enter Partner Commission",
								number : "Please enter digits only"
							},
							edit_jpmilecommission : {
								required : "Please enter JP Mile Commission",
								number : "Please enter digits only"
							},
							datepicker11 : {
								greaterThanEditEndDate : "Start date must be less than or equal to end date",
								duplicateDatesStart : "Details Exist, Please Select another date"
							},
							datepicker12 : {
								lessThanEditStartDate : "End date should not be less than start date or current date",
								duplicateDatesEnd : "Details Exist, Please Select another date"
							},
							edit_maxJpmiles : {
								digits : "Please enter numeric data",
								rangelength : "Maximum 6 digits permitted"
							},
							edit_lowerValue : {
								number : "Please enter numeric data",
								editLowerValueSmallerThanHigher : "Lower Value Should Be smaller than Upper Value"

							},
							edit_upperValue : {
								number : "Please enter numeric data",
								editUpperValueGreaterThanLower : "Upper Value Should Be Greater than Lower Value"
							},
							edit_bonusCommissionRate : {
								number : "Please enter digits only"
							}
						},
						errorElement : "div",
						errorPlacement : function(error, element) {
							if ($(element).hasClass("startDate")) {
								error.insertAfter($(element).closest("div"));
							} else if ($(element).hasClass("endDate")) {
								error.insertAfter($(element).closest("div"));
							} else {
								error.insertAfter(element);
							}
						},
						submitHandler : function(form) {
							form.submit();
						},
					});
	$('#edit_lowerValue').on('change', function() {
		$("#editCommissionBean").validate().element('#edit_lowerValue');
		$("#editCommissionBean").validate().element('#edit_lowerValue');
	});

	$('#edit_upperValue').on('change', function() {
		$("#editCommissionBean").validate().element('#edit_upperValue');
		$("#editCommissionBean").validate().element('#edit_upperValue');
	});

	$.validator.addMethod("editUpperValueGreaterThanLower", function(value,
			element) {
		var lowerValue = $('#edit_lowerValue').val();
		var upperValue = $('#edit_upperValue').val();
		if (lowerValue == '' || upperValue == '') {
			return true;
		} else if (lowerValue - upperValue > 0) {
			return false;
		} else {
			return true;
		}

	});

	$.validator.addMethod("editLowerValueSmallerThanHigher", function(value,
			element) {
		var lowerValue = $('#edit_lowerValue').val();
		var upperValue = $('#edit_upperValue').val();

		if (lowerValue == '' || upperValue == '') {
			return true;
		} else if (lowerValue - upperValue > 0) {
			return false;
		} else {
			return true;
		}

	});
	$.validator.addMethod("greaterThanEditEndDate", function(value, element) {
		var startDate = getFormattedDate($('#datepicker11').val());
		var endDate = getFormattedDate($('#datepicker12').val());
		if (endDate == 'Invalid Date')
			return true;
		if (endDate >= startDate)
			return true;
		if (endDate <= startDate)
			return false;
	});

	$.validator.addMethod("lessThanEditStartDate", function(value, element) {
		if (getFormattedDate($('#datepicker12').val()) == 'Invalid Date')
			return true;

		var currentDate = $.datepicker.formatDate("dd/mm/yy", new Date());
		var startDate = $.datepicker.formatDate("dd/mm/yy", getFormattedDate($(
				'#datepicker11').val()));
		var endDate = $.datepicker.formatDate("dd/mm/yy", getFormattedDate($(
				'#datepicker12').val()));

		if (($.datepicker.parseDate("dd/mm/yy", endDate) < $.datepicker
				.parseDate("dd/mm/yy", currentDate))
				|| ($.datepicker.parseDate("dd/mm/yy", endDate) < $.datepicker
						.parseDate("dd/mm/yy", startDate))) {
			return false;
		} else {
			return true;
		}
	});
	var id='', partnerName='', partcategoryname ='',partproductname='',commisionType='',commissionRate='',memberShare='',lowerValue='',upperValue='',
		deactivationDate='',activationDate='',commissionSatus='',transactionStatus='',maxjpmiles='',commissionstatuscode='';
	/* $('span.editcommission')
			.on(
					'click',
					function() { */
						 $('#table').on('click', 'a.editcommission', function (e) {
					    var checka = table1.row( $(this).closest('tr')).data();
					  	id = checka.prtCommNo;
						partnerName = table1.row( $(this).closest('tr')).data().partnerName;
						partcategoryname = table1.row( $(this).closest('tr')).data().prtCategoryName;
						partproductname = table1.row( $(this).closest('tr')).data().prtProductName;
						commisionType = table1.row( $(this).closest('tr')).data().commisionType;
						commissionRate = table1.row( $(this).closest('tr')).data().commissionRate;
						memberShare = table1.row( $(this).closest('tr')).data().memberShare;
						lowerValue = table1.row( $(this).closest('tr')).data().lowerValue;
						upperValue = table1.row( $(this).closest('tr')).data().upperValue;
						activationDate = table1.row( $(this).closest('tr')).data().activationDate;
						deactivationDate = table1.row( $(this).closest('tr')).data().deactivationDate;
						if(table1.row( $(this).closest('tr')).data().commissionSatus == 'Active'){
							commissionstatuscode = 1;
							}
						else{
							commissionstatuscode =0;
						}
						transactionStatus = table1.row( $(this).closest('tr')).data().transactionStatus;
						 $('#edit_partnername').val(partnerName);
				  		 $('#edit_partnercategoryname').val(partcategoryname);
				  			$('#edit_partnerproductname').val(partproductname);
				  			 if (commisionType == "2") {
								$('#edit_flatAmount').prop("checked", "checked");
							} else {
								$('#edit_Percentage').prop("checked", "checked");
							} 
				  			$('#edit_flatAmount').val(commisionType);
							$('#edit_Percentage').val(commisionType);
				  			$('#edit_partnercommission').val(commissionRate);
				  			$('#edit_jpmilecommission').val(memberShare);
				  			$('#edit_lowerValue').val(lowerValue);
				  			$('#edit_upperValue').val(upperValue);
				  			$('#datepicker11').val(activationDate);
				  			$('#datepicker12').val(deactivationDate);
				  			$('#edit_commissionstatus_' + commissionstatuscode)
							.prop("checked", "checked");
				  			if(transactionStatus == "2") {
								$('#edit_processed').prop("checked", "checked");
							} else {
								$('#edit_tentative').prop("checked", "checked");
							}
				  			$('#edit_processed').val(transactionStatus);
							$('#edit_tentative').val(transactionStatus);
							/* if(commissionSatus == "1") {
								$('#edit_commissionstatus_').prop("checked", "checked");
							} else {
								$('#edit_commissionstatus_').prop("checked", "checked");
							} */
				  	        table1 = $('#dtable1').DataTable();
						/* var partname = $(this).closest('tr').data('partname');
						var partcategoryname = $(this).closest('tr').data(
								'partcategoryname');
						var partno = $(this).closest('tr').data('partno');
						var partcategoryno = $(this).closest('tr').data(
								'partcategoryno');
						var partproductno = $(this).closest('tr').data(
								'partproductno');
						var categorypath = $(this).closest('tr').data(
								'categorypath');
						var partproductname = $(this).closest('tr').data(
								'partproductname');
						var commissionrate = $(this).closest('tr').data(
								'commissionrate');
						var membershare = $(this).closest('tr').data(
								'membershare');
						var activationdate = $(this).closest('tr').data(
								'activationdate');
						var deactivationdate = $(this).closest('tr').data(
								'deactivationdate');
						var commissionstatus = $(this).closest('tr').data(
								'commissionstatus');
						var commissionstatuscode = $(this).closest('tr').data(
								'commissionstatuscode');
						var commissionstatusclass = $(this).closest('tr').data(
								'commissionstatusclass');
						var maxjpmiles = $(this).closest('tr').data(
								'maxjpmiles');
						var currentDate = $.datepicker
								.parseDate("dd/mm/yy", $.datepicker.formatDate(
										"dd/mm/yy", new Date()));
						var startDate = $.datepicker.parseDate("dd/mm/yy",
								$.datepicker.formatDate("dd/mm/yy",
										getFormattedDate(activationdate)));
						var commissionType = $(this).closest('tr').data(
								'commissiontype');
						var transactionStatus = $(this).closest('tr').data(
						'transactionstatus');
						var lowerValue = $(this).closest('tr').data(
								'lowervalue');
						var upperValue = $(this).closest('tr').data(
								'uppervalue');
						var bonusCommissionType = $(this).closest('tr').data(
								'bonuscommissiontype');
						var bonusCommissionRate = $(this).closest('tr').data(
								'bonuscommissionrate');
						var bonusPartnerCode = $(this).closest('tr').data(
								'bonuspartnercode');
						var bonusActivityCode = $(this).closest('tr').data(
								'bonusactivitycode');

						$('#edit_partnercommission').attr('readonly', false);
						$('#edit_jpmilecommission').attr('readonly', false);
						$('#datepicker11').datepicker('enable');
						$('#datepicker12').attr('readonly', false).datepicker(
								'enable');
						$("#edit_maxJpmiles").attr('readonly', false);

						if (startDate <= currentDate) {
							$('#edit_partnercommission').attr('readonly', true);
							$('#edit_jpmilecommission').attr('readonly', true);
							$("#edit_maxJpmiles").attr('readonly', true);
							$('#datepicker11').datepicker('disable');
						}

						if (getFormattedDate(deactivationdate) != 'Invalid Date') {
							var endDate = $.datepicker
									.parseDate(
											"dd/mm/yy",
											$.datepicker
													.formatDate(
															"dd/mm/yy",
															getFormattedDate(deactivationdate)));
							if (endDate <= currentDate) {
								$('#datepicker12').attr('readonly', true)
										.datepicker('disable');
							}
						}

						$('#edit_prtcommno').val(id);
						$('#edit_partnername').val(partname);
						$('#edit_partnercategoryname').val(partcategoryname);
						$('#edit_partnerproductname').val(partproductname);

						//$('#edit_brandName').val(brandname);
						$('#edit_categoryPath').val(categorypath);

						$('#edit_partnercommission').val(commissionrate);
						$('#edit_jpmilecommission').val(membershare);
						$('#datepicker11').val(activationdate);
						$('#datepicker11Hidden').val(activationdate);
						$('#datepicker12').val(deactivationdate);
						$('#datepicker12Hidden').val(deactivationdate);
						$('#edit_commissionstatus_' + commissionstatuscode)
								.prop("checked", "checked");
						$("#edit_maxJpmiles").val(maxjpmiles);
						$("#edit_lowerValue").val(lowerValue);
						$("#edit_upperValue").val(upperValue);
						if (commissionType == "2") {
							$('#edit_flatAmount').prop("checked", "checked");
						} else {
							$('#edit_Percentage').prop("checked", "checked");
						}

						if(transactionStatus == "2") {
							$('#edit_processed').prop("checked", "checked");
						} else {
							$('#edit_tentative').prop("checked", "checked");
						}
						
						if (bonusCommissionType != 3) {
							$(
									"#edit_bonusCommissionType_"
											+ bonusCommissionType).prop(
									"checked", "checked");
						}

						$("#edit_bonusCommissionRate").val(bonusCommissionRate);
						$("#edit_bonusPartnerCode").val(bonusPartnerCode);
						$("#edit_bonusActivityCode").val(bonusActivityCode); 

						$('#myModal1').data('partno', partno);
						$('#myModal1').data('partcatno', partcategoryno);
						$('#myModal1').data('partprdctno', partproductno);*/
						  e.preventDefault();
						$('#myModal1').data('id', id).modal('show');
					});

					var partno='',partcatno='',partprdctno='';
	/* $('span.addcommission_').on(
			'click',
			function() { */
				 $('#table').on('click', 'a.addcommission_', function (e) {
					  var checka = table1.row( $(this).closest('tr')).data();
					  	id = checka.prtCommNo;
					  	partno=table1.row( $(this).closest('tr')).data().partNo;
					  	partcatno=table1.row( $(this).closest('tr')).data().prtCatNo;
					  	partprdctno=table1.row( $(this).closest('tr')).data().prtProductNo;
						partnerName = table1.row( $(this).closest('tr')).data().partnerName;
						partcategoryname = table1.row( $(this).closest('tr')).data().prtCategoryName;
						partproductname = table1.row( $(this).closest('tr')).data().prtProductName;
						commisionType = table1.row( $(this).closest('tr')).data().commisionType;
						commissionRate = table1.row( $(this).closest('tr')).data().commissionRate;
						memberShare = table1.row( $(this).closest('tr')).data().memberShare;
						lowerValue = table1.row( $(this).closest('tr')).data().lowerValue;
						upperValue = table1.row( $(this).closest('tr')).data().upperValue;
						activationDate = table1.row( $(this).closest('tr')).data().activationDate;
						deactivationDate = table1.row( $(this).closest('tr')).data().deactivationDate;
						if(table1.row( $(this).closest('tr')).data().commissionSatus == 'Active'){
							commissionstatuscode = 1;
							}
						else{
							commissionstatuscode =0;
						}
						transactionStatus = table1.row( $(this).closest('tr')).data().transactionStatus;
				 
			/* 	var id = $(this).closest('tr').data('id');
				var partno = $(this).closest('tr').data('partno');
				var partname = $(this).closest('tr').data('partname');
				var partcatno = $(this).closest('tr').data('partcategoryno');
				var partcategoryname = $(this).closest('tr').data(
						'partcategoryname');
				var partprdctno = $(this).closest('tr').data('partproductno');
				var partproductname = $(this).closest('tr').data(
						'partproductname');
				var commissionrate = $(this).closest('tr').data(
						'commissionrate');
				var membershare = $(this).closest('tr').data('membershare');
				var activationdate = $(this).closest('tr').data(
						'activationdate');
				var deactivationdate = $(this).closest('tr').data(
						'deactivationdate');
				var commissionstatus = $(this).closest('tr').data(
						'commissionstatus');
				var commissionstatuscode = $(this).closest('tr').data(
						'commissionstatuscode');
				var commissionstatusclass = $(this).closest('tr').data(
						'commissionstatusclass');
				var categorypath = $(this).closest('tr').data('categorypath'); */

				/* $('#add_prtcommno').val(id);
				$('#add_partnername').val(partname);
				$('#add_partnercategoryname').val(partcategoryname);
				$('#add_partnerproductname').val(partproductname);
				$('#add_commissionstatus_' + commissionstatuscode).prop(
						"checked", "checked");
				$('#add_Percentage').prop("checked", "checked");

				$('#add_tentative').prop("checked", "checked");
				$('#add_categoryPath').val(categorypath); */
				
				    $('#add_partnername').val(partnerName);
		  		    $('#add_partnercategoryname').val(partcategoryname);
		  			$('#add_partnerproductname').val(partproductname);
		  			$('#add_Percentage').prop("checked", "checked");
					$('#add_tentative').prop("checked", "checked");
					$('#add_commissionstatus_' + commissionstatuscode).prop(
							"checked", "checked");
		  		

			 	$('#myModalab').data('partno', partno);
				$('#myModalab').data('partcatno', partcatno);
				$('#myModalab').data('partprdctno', partprdctno); 

				e.preventDefault();
				$('#myModalab').data('id', id).modal('show');

			});

	$('#updateCommission')
			.click(
					function() {
						var id = $('#myModal1').data('id');
						var partno = $(this).closest('tr').data('partno');
						var json = {};
						var startdate, enddate;

						var prtNo_ = $("#prtNo_").val();
						startdate = $("#datepicker11").val();
						enddate = $("#datepicker12").val();
						json["prtCommNo"] = id;
						json["partnerActivationDate"] = startdate;
						json["partnerDeactivationDate"] = enddate;
						json["partnerName"] = $("#edit_partnername").val();
						json["partnerCategoryName"] = $(
								"#edit_partnercategoryname").val();
						json["partnerProductNameName"] = $(
								"#edit_partnerproductname").val();
						json["categoryPath"] = $("#edit_categoryPath").val();
						json["partnerCommission"] = $("#edit_partnercommission")
								.val();
						json["jpMileCommission"] = $("#edit_jpmilecommission")
								.val();
						json["commissionStatus"] = $(
								'input[name=edit_commissionstatus]:checked')
								.val(); 
						json["maxJpmiles"] = $("#edit_maxJpmiles").val();
						json["lowerValue"] = $("#edit_lowerValue").val();
						json["upperValue"] = $("#edit_upperValue").val();
						json["commType"] = $(
								'input[name=edit_commType]:checked').val();
						json["transactionStatus"] = $(
						'input[name=edit_transactionStatus]:checked').val();

						var bonusCommissionType = $(
								'input[name=edit_bonusCommissionType]:checked')
								.val();

						if (bonusCommissionType == undefined) {
							bonusCommissionType = "";
						}

						json["bonusCommissionType"] = bonusCommissionType;
						json["bonusCommissionRate"] = $(
								"#edit_bonusCommissionRate").val();
						json["bonusPartnerCode"] = $("#edit_bonusPartnerCode")
								.val();
						json["bonusActivityCode"] = $("#edit_bonusActivityCode")
								.val();

						var formData = new FormData();
						formData.append('json', JSON.stringify(json));
						if ($("#editCommissionBean").validate().element(
								'#edit_partnercommission')
								&& $("#editCommissionBean").validate().element(
										'#edit_jpmilecommission')
								&& $("#editCommissionBean").validate().element(
										'#datepicker11')
								&& $("#editCommissionBean").validate().element(
										'#datepicker12')
								&& $("#editCommissionBean").validate().element(
										'#edit_maxJpmiles')) {
							$
									.ajax({
										url : "${contextPath}/admin/updateCommission",
										data : formData,
										dataType : 'text',
										processData : false,
										contentType : false,
										type : 'POST',
										success : function(response) {
											$('#myModal1').modal('hide');
											window.location.href = "${contextPath}/admin/commission?updateMessage="
													+ response
													+ "&partNo="
													+ prtNo_;
										}
									});
						}
					});

	$('#datepicker11').on('change', function() {
		successStartDateFlag = false;

		var obj = null;
		var json = {};
		var prtcommno = $('#myModal1').data('id');
		var partno = $('#myModal1').data('partno');
		var partcategoryno = $('#myModal1').data('partcatno');
		var partproductno = $('#myModal1').data('partprdctno');
		var activationdate = $("#datepicker11").val();
		var activationDateHidden = $('#datepicker11Hidden').val();
		var deactivationdate = $("#datepicker12").val();
		var commissionrate = $("#edit_partnercommission").val();
		var membershare = $("#edit_jpmilecommission").val();
		//var brandName = $("#edit_brandName").val();
		var catPath = $("#edit_categoryPath").val();

		json["partComNo"] = prtcommno;
		json["partNo"] = partno;
		json["activationDate"] = activationdate;
		json["activationDateHidden"] = activationDateHidden;
		json["deactivationDate"] = deactivationdate;
		json["partCategoryNo"] = partcategoryno;
		json["partProductNo"] = partproductno;
		json["commissionrate"] = commissionrate;
		json["membershare"] = membershare;
		json["brandName"] = "";
		json["catPath"] = catPath;

		var formData = new FormData();
		formData.append('json', JSON.stringify(json));

		$.ajax({
			url : "${contextPath}/admin/checkExistingDetailsEditStartDate",
			data : formData,
			dataType : 'text',
			processData : false,
			contentType : false,
			type : 'POST',
			success : function(response) {
				if (response == "failure") {
					successStartDateFlag = true;
				}
				if (response == "success") {
					successStartDateFlag = false;
				}
				$("#editCommissionBean").validate().element('#datepicker11');
			}
		});

		$("#editCommissionBean").validate().element('#datepicker11');
	});

	$('#datepicker12')
			.on(
					'change',
					function() {
						successEndDateFlag = false;
						editEndDateValidation = false;

						var obj = null;
						var json = {};
						var partno = $('#myModal1').data('partno');
						var prtcommno = $('#myModal1').data('id');

						var partcategoryno = $('#myModal1').data('partcatno');
						var partproductno = $('#myModal1').data('partprdctno');
						var activationdate = $("#datepicker11").val();
						var deactivationdate = $("#datepicker12").val();
						var deactivationDateHidden = $('#datepicker12Hidden')
								.val();
						var commissionrate = $("#edit_partnercommission").val();
						var membershare = $("#edit_jpmilecommission").val();
						//var brandName = $("#edit_brandName").val();
						var catPath = $("#edit_categoryPath").val();
						var lowerValue = $("#edit_lowerValue").val();
						var upperValue = $("#edit_upperValue").val();

						if (getFormattedDate(deactivationdate) != 'Invalid Date') {
							var currentDate = $.datepicker.formatDate(
									"dd/mm/yy", new Date());
							var startDate = $.datepicker.formatDate("dd/mm/yy",
									getFormattedDate(activationdate));
							var endDate = $.datepicker.formatDate("dd/mm/yy",
									getFormattedDate(deactivationdate));

							if (($.datepicker.parseDate("dd/mm/yy", endDate) < $.datepicker
									.parseDate("dd/mm/yy", currentDate))
									|| ($.datepicker.parseDate("dd/mm/yy",
											endDate) < $.datepicker.parseDate(
											"dd/mm/yy", startDate))) {
								editEndDateValidation = true;
							} else {
								json["partComNo"] = prtcommno;
								json["partNo"] = partno;
								json["activationDate"] = activationdate;
								json["deactivationDate"] = deactivationdate;
								json["deactivationDateHidden"] = deactivationDateHidden;
								json["partCategoryNo"] = partcategoryno;
								json["partProductNo"] = partproductno;
								json["commissionrate"] = commissionrate;
								json["membershare"] = membershare;
								json["brandName"] = "";
								json["catPath"] = catPath;
								json["lowerValue"] = lowerValue;
								json["upperValue"] = upperValue;

								var formData = new FormData();
								formData.append('json', JSON.stringify(json));

								$
										.ajax({
											url : "${contextPath}/admin/checkExistingDetailsForEditEndDate",
											data : formData,
											dataType : 'text',
											processData : false,
											contentType : false,
											type : 'POST',
											success : function(response) {
												if (response == "failure") {
													successEndDateFlag = true;
												}
												if (response == "success") {
													successEndDateFlag = false;
												}
												$("#editCommissionBean")
														.validate()
														.element(
																'#datepicker12');
											}
										});
							}
						}
						$("#editCommissionBean").validate().element(
								'#datepicker12');
					});
</script>

<script type="text/javascript">
	var rpNameExist = false;
	var rpNameInvalid = false;
	var rpBrandInvalid = true;

	var successStartDateFlag = false;
	var successEndDateFlag = false;
	var editEndDateValidation = false;
	var successflag = false;

	$.validator.addMethod('duplicateDatesStart', function(value, element) {
		return !successStartDateFlag;
	});

	$.validator.addMethod('duplicateDatesEnd', function(value, element) {
		return !successEndDateFlag;
	});

	$.validator.addMethod('editEndDateValidation', function(value, element) {
		return !editEndDateValidation;
	});

	$.validator.addMethod('duplicateDates', function(value, element) {
		if (successflag) {
			return false;
		} else {
			return true;
		}

	});

	function getFormattedDate(date) {
		var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
		return new Date(date.replace(pattern, '$3-$2-$1'));
	}

	var currentDate = $.datepicker.formatDate('dd/mm/yy', new Date());
	var startDate = $('#datepicker1').val();
	if (getFormattedDate(startDate) < getFormattedDate(currentDate)) {
		$('#datepicker1').attr("disabled", "true");

	}
	$('#lowerValue').on('change', function() {
		$("#commissionBean").validate().element('#lowerValue');
		$("#commissionBean").validate().element('#upperValue');
	});

	$('#upperValue').on('change', function() {
		$("#commissionBean").validate().element('#lowerValue');
		$("#commissionBean").validate().element('#upperValue');
	});

	$.validator.addMethod("upperValueGreaterThanLower",
			function(value, element) {
				var lowerValue = $('#lowerValue').val();
				var upperValue = $('#upperValue').val();
				if (lowerValue == '' || upperValue == '') {
					return true;
				} else if (lowerValue - upperValue > 0) {
					return false;
				} else {
					return true;
				}

			});

	$.validator.addMethod("lowerValueSmallerThanHigher", function(value,
			element) {
		var lowerValue = $('#lowerValue').val();
		var upperValue = $('#upperValue').val();

		if (lowerValue == '' || upperValue == '') {
			return true;
		} else if (lowerValue - upperValue > 0) {
			return false;
		} else {
			return true;
		}

	});
	var fullDate = new Date();
	var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate
			.getMonth() + 1) : '0' + (fullDate.getMonth() + 1);
	var today = fullDate.getDate() + "/" + twoDigitMonth + "/"
			+ fullDate.getFullYear();

	$.validator.addMethod("dateHigherThanToday", function(value, element) {
		var startDate = getFormattedDate($('#datepicker1').val());
		var endDate = getFormattedDate($('#datepicker2').val());
		if (endDate == 'Invalid Date')
			return true;
		else if (startDate <= endDate)
			return true;
		else if (startDate >= endDate)
			return false;
	});

	$.validator.addMethod("greaterThanEndDate", function(value, element) {
		var startDate = getFormattedDate($('#datepicker1').val());
		var endDate = getFormattedDate($('#datepicker2').val());
		if (endDate == 'Invalid Date')
			return true;
		if (endDate >= startDate)
			return true;
		if (endDate <= startDate)
			return false;
	});

	$("#commissionBean")
			.validate(
					{

						ignore : [],
						rules : {
							activationDate : {
								required : true,
								duplicateDatesStart : true,
								dateHigherThanToday : true
							},
							rpName : {
								rpNameInvalid : true
							},

							partNo : {
								required : true
							},
							deactivationDate : {
								greaterThanEndDate : true,
								duplicateDatesEnd : true
							},
							commissionRate : {
								required : true,
								number : true,
							},
							memberShare : {
								required : true,
								number : true,
							},
							productBrand : {
								rpBrandInvalidName : true,
							},
							maxJpmiles : {
								digits : true,
								rangelength : [ 1, 6 ]
							},
							lowerValue : {
								number : true,
								lowerValueSmallerThanHigher : true
							},
							upperValue : {
								number : true,
								upperValueGreaterThanLower : true
							},
							bonusCommissionRate : {
								number : true
							}
						},
						messages : {
							partNo : {
								required : "Please Select Partner"
							},
							rpName : {
								rpNameInvalid : "Invalid Product, Please select from dropdown"
							},
							activationDate : {
								required : "Please enter Commission Start Date",
								duplicateDatesStart : "Details Exists, Please select another date",
								dateHigherThanToday : "Start date must be less than or equal to end date"
							},
							deactivationDate : {
								duplicateDatesEnd : "Details Exist, Please Select another date",
								greaterThanEndDate : "End Date must be greater than or equal to start date"
							},
							commissionRate : {
								required : "Please enter Partner Commission",
								number : "Please enter digits only",
							},
							memberShare : {
								required : "Please enter JP Mile Commission",
								number : "Please enter digits only",
							},
							productBrand : {
								rpBrandInvalidName : "Invalid Brand, Please select from dropdown",
							},
							maxJpmiles : {
								digits : "Please enter numeric data",
								rangelength : "Maximum 6 digits permitted"
							},
							lowerValue : {
								digits : "Please enter numeric data",
								lowerValueSmallerThanHigher : "Lower Value Should Be smaller than Upper Value"
							},
							upperValue : {
								digits : "Please enter numeric data",
								upperValueGreaterThanLower : "Upper Value Should Be greater than lower value"
							},
							bonusCommissionRate : {
								number : "Please enter digits only"
							}
						},
						errorElement : "div",
						errorPlacement : function(error, element) {
							if ($(element).hasClass("form-control startDate")) {
								error.insertAfter($(element).closest("div"));
							} else if ($(element).hasClass(
									"form-control endDate")) {
								error.insertAfter($(element).closest("div"));
							} else {
								error.insertAfter(element);
							}
						},
						submitHandler : function(form) {
							form.submit();
						},
					});

	$.validator.addMethod("AlphabetsOnly", function(value, element) {
		return this.optional(element) || /^[a-z][a-z@\s]*$/i.test(value);
	});

	$.validator.addMethod('duplicaterpName', function(value, element) {
		if (rpNameExist) {
			return false;
		} else {
			return true;
		}

	});

	$.validator.addMethod('rpNameInvalid', function(value, element) {
		if (rpNameInvalid) {
			return false;
		} else {
			return true;
		}

	});

	$.validator.addMethod("rpBrandInvalidName", function(value, element) {
		return rpBrandInvalid;
	});

	$(".commTypeRadio").on('change', function() {
		$("#bonusCommissionRate").prop("readonly", false);
	});

	$(".add_commTypeRadio").on('change', function() {
		$("#add_bonusCommissionRate").prop("readonly", false);
	});

	$('#partnerCategory')
			.on(
					'selectmenuchange',
					function() {
						var prtCatNo = $("#partnerCategory option:selected")
								.val();
						$("#commissionBean").data('validator').resetForm();
						$("#rpName").val('');
						$("#prtProductNo").val('');
						/* $("#rpName")
								.autocomplete(
										{
											minLength : 1,
											source : function(request, response) {
												$
														.ajax({
															dataType : "json",
															type : "GET",
															xhrFields : {
																withCredentials : true
															},
															crossDomain : true,
															cache : true,
															data : {
																keyword : request.term
															},
															url : "${contextPath}/admin/getPartnerProducts?prtCatNo="
																	+ prtCatNo,
															success : function(
																	data) {
																var array = $
																		.map(
																				data,
																				function(
																						key,
																						value) {
																					return {
																						label : key,
																						value : value
																					}
																				});
																response($.ui.autocomplete
																		.filter(
																				array,
																				request.term));
															},
															error : function(
																	data) {
																alert("Error")
															}
														})
											},
											focus : function(event, ui) {
												event.preventDefault();
											},
											select : function(event, ui) {
												$("#prtProductNo").val(
														ui.item.value);
												$("#rpName").val(ui.item.label);
												rpNameInvalid = false;
												$("#commissionBean").validate()
														.element('#rpName');
												return false;
											}
										}); */

						$("#productBrand")
								.autocomplete(
										{
											minLength : 1,
											source : function(request, response) {
												$
														.ajax({
															dataType : "json",
															type : "GET",
															xhrFields : {
																withCredentials : true
															},
															crossDomain : true,
															cache : true,
															data : {
																keyword : request.term
															},
															url : "${contextPath}/admin/getBrandForParnerCategory?prtCatNo="
																	+ prtCatNo,
															success : function(
																	data) {
																var array = $
																		.each(
																				data,
																				function(
																						i,
																						v) {
																					return v;
																				});
																response($.ui.autocomplete
																		.filter(
																				array,
																				request.term));
															},
															error : function(
																	data) {
																//alert("Error")
															}
														})
											},
											focus : function(event, ui) {
												event.preventDefault();
											},
											select : function(event, ui) {
												$("#productBrand").val(
														ui.item.value);
												rpBrandInvalid = true;
												$("#commissionBean")
														.validate()
														.element(
																'#productBrand');
												return false;
											}
										});
					});

	$('#rpName')
			.on(
					'keyup',
					function() {
						var prtCatNo = $("#partnerCategory option:selected")
								.val();
						
						var rpName = $("#rpName").val();
						//alert("rpName>>"+rpName);
						$("#commissionBean").data('validator').resetForm();
						//$("#rpName").val('');
						$("#prtProductNo").val('');
						$("#rpName")
								.autocomplete(
										{
											minLength : 1,
											source : function(request, response) {
												$
														.ajax({
															dataType : "json",
															type : "GET",
															xhrFields : {
																withCredentials : true
															},
															crossDomain : true,
															cache : true,
															data : {
																keyword : request.term
															},
															url : "${contextPath}/admin/getPartnerProducts?prtCatNo="
																	+ prtCatNo
																	+ "&rpName="
																	+ rpName,
															success : function(
																	data) {
																var array = $
																		.map(
																				data,
																				function(
																						key,
																						value) {
																					return {
																						label : key,
																						value : value
																					}
																				});
																response($.ui.autocomplete
																		.filter(
																				array,
																				request.term));
															},
															error : function(
																	data) {
																//alert("Error")
															}
														})
											},
											focus : function(event, ui) {
												event.preventDefault();
											},
											select : function(event, ui) {
												$("#prtProductNo").val(
														ui.item.value);
												$("#rpName").val(ui.item.label);
												rpNameInvalid = false;
												$("#commissionBean").validate()
														.element('#rpName');
												return false;
											}
										});
					});

	$('#productBrand').click(function() {
		var prtCatNo = $("#partnerCategory option:selected").val();
		$("#productBrand").val('');
		rpBrandInvalid = false;

		if (prtCatNo == '') {
			alert("Please select the partner category");
		}
	});

	$('#rpName').on('blur', function() {
		var rpName = $("#rpName").val();
		if (rpName == '') {
			rpNameInvalid = false;
			$("#commissionBean").validate().element('#rpName');
			$('#productBrand').attr('readonly', false);
			$('#categoryPath').attr('readonly', false);

		} else {
			$('#productBrand').val('');
			$('#categoryPath').val('');
			$('#productBrand').attr('readonly', true);
			$('#categoryPath').attr('readonly', true);
		}
	});

	$('#rpName').on(
			'change',
			function() {
				var prtNo = $("#partnerNo").val();
				var prtPrdNo = $("#prtProductNo").val();
				var prtCatNo = $("#partnerCategory").val();
				if (prtPrdNo == '') {
					rpNameInvalid = true;
					$("#commissionBean").validate().element('#rpName');
				} else {
					$.get("${contextPath}/admin/checkPartnerProducts?prtNo="
							+ prtNo + "&prtPrdNo=" + prtPrdNo + "&prtCatNo="
							+ prtCatNo, function(data, status) {
						if (status == "success" && data != '') {
							rpNameExist = true;
						} else {
							rpNameExist = false;
						}
						rpNameInvalid = false;
						$("#commissionBean").validate().element('#rpName');
					});
				}
			});

	$("#rpName").click(function() {
		var prtCatNo = $("#partnerCategory option:selected").val();
		$("#rpName").val('');
		$("#prtProductNo").val('');
		if (prtCatNo == '') {
			alert("Please select the partner category");
			$("#rpName").val('');
			$("#partnerCategory").focus();
			return false;
		}
	});
	$('#save').click(function() {

		var prtNo = $("#prtNo option:selected").val();
		var prtCatNo = $("#prtCatNo option:selected").val();
		if (prtNo == '') {
			$("#commissionBean").validate().element("prtNo");
		}
		if (prtCatNo = '') {
			$("#commissionBean").validate().element("prtCatNo");
		}
		$(this).closest('form').submit();
	});

	$('#cancelbtnD').click(
			function() {
				$.get("${contextPath}/admin/addCommissionCancel", function(
						data, status) {
					if (status == "success") {
						window.location.replace("${contextPath}/" + data);
					}
				});

			});
	$('#cancelbtnD_').click(
			function() {
				$.get("${contextPath}/admin/addCommissionCancel", function(
						data, status) {
					if (status == "success") {
						window.location.replace("${contextPath}/" + data);
					}
				});

			});
	$('#cancelbtn_').click(
			function() {
				$.get("${contextPath}/admin/addCommissionCancel", function(
						data, status) {
					if (status == "success") {
						window.location.replace("${contextPath}/" + data);
					}
				});

			});

	$('#datepicker1')
			.on(
					'change',
					function() {
						if ($("#commissionBean").validate().element(
								'#partnerNo')) {
							var obj = null;
							var json = {};
							var partNo = $("#partnerNo").val();
							var partCategoryNo = $('#partnerCategory').val();
							var partProductNo = $('#prtProductNo').val();
							var activationdate = $("#datepicker1").val();
							var commissionrate = $("#commissionRate").val();
							var membershare = $("#memberShare").val();
							//var brandName = $("#productBrand").val();
							var catPath = $("#categoryPath").val();
							var lowerValue = $("#lowerValue").val();
							var upperValue = $("#upperValue").val();

							json["partNo"] = partNo;
							json["activationDate"] = activationdate;
							json["partCategoryNo"] = partCategoryNo;
							json["partProductNo"] = partProductNo;
							json["commissionrate"] = commissionrate;
							json["membershare"] = membershare;
							json["brandName"] = "";
							json["catPath"] = catPath;
							json["lowerValue"] = lowerValue;
							json["upperValue"] = upperValue;
							var formData = new FormData();
							formData.append('json', JSON.stringify(json));
							if (activationdate != "") {
								$
										.ajax({
											url : "${contextPath}/admin/checkExistingDetailsStartDate",
											data : formData,
											dataType : 'text',
											processData : false,
											contentType : false,
											type : 'POST',
											success : function(response) {

												if (response == "failure") {
													successStartDateFlag = true;
												}
												if (response == "success") {
													successStartDateFlag = false;
												}
												$("#commissionBean")
														.validate()
														.element('#datepicker1');
											}
										});

								$("#commissionBean").validate().element(
										'#datepicker1');
								var endDate = getFormattedDate($('#datepicker2')
										.val());

								if (endDate != 'Invalid Date') {
									$("#commissionBean").validate().element(
											'#datepicker2');
								}

							}
						}
					});

	$('#datepicker2').on('change', function() {
		//$("#commissionBean").validate().element('#selectEnddate');
		if ($("#commissionBean").validate().element('#datepicker1')) {
			var obj = null;
			var json = {};
			var partNo = $("#partnerNo").val();
			var partCategoryNo = $('#partnerCategory').val();
			var partProductNo = $('#prtProductNo').val();
			var activationdate = $("#datepicker1").val();
			var deactivationdate = $("#datepicker2").val();
			var commissionrate = $("#commissionRate").val();
			var membershare = $("#memberShare").val();
			//var brandName = $("#productBrand").val();
			var catPath = $("#categoryPath").val();
			var lowerValue = $("#lowerValue").val();
			var upperValue = $("#upperValue").val();

			json["partNo"] = partNo;
			json["activationDate"] = activationdate;
			json["deactivationDate"] = deactivationdate;
			json["partCategoryNo"] = partCategoryNo;
			json["partProductNo"] = partProductNo;
			json["commissionrate"] = commissionrate;
			json["membershare"] = membershare;
			json["brandName"] = "";
			json["catPath"] = catPath;
			json["lowerValue"] = lowerValue;
			json["upperValue"] = upperValue;

			var formData = new FormData();
			formData.append('json', JSON.stringify(json));

			$.ajax({
				url : "${contextPath}/admin/checkExistingDetailsEndDate",
				data : formData,
				dataType : 'text',
				processData : false,
				contentType : false,
				type : 'POST',
				success : function(response) {
					if (response == "failure") {
						successEndDateFlag = true;
					}
					if (response == "success") {
						successEndDateFlag = false;
					}
					$("#commissionBean").validate().element('#datepicker2');
				}
			});
			$("#commissionBean").validate().element('#datepicker1');
		}
	});

	$('#datepicker14').on('click', function() {
		if (datepicker14 == '')
			alert("Please Select Start Date");
	});
	$('#datepicker13').on('change', function() {
		$.datepicker._clearDate('#datepicker14');
		$("#addCommissionBean").validate().element("#datepicker13");
		$("#addCommissionBean").validate().element("#datepicker14");
		var id = $('#myModalab').data('id');
		var partno = $('#myModalab').data('partno');
		var partcatno = $('#myModalab').data('partcatno');
		var partproductno = $('#myModalab').data('partprdctno');
		//var brandName = $("#add_brandName").val();
		var catPath = $("#add_categoryPath").val();

		var startdate, enddate;
		startdate = $("#datepicker13").val();
		enddate = $("#datepicker14").val();
		var obj = null;
		var json = {};
		var commissionrate = $("#add_partnercommission").val();
		var membershare = $("#add_jpmilecommission").val();
		json["partNo"] = partno;
		json["partCategoryNo"] = partcatno;
		json["partProductNo"] = partproductno;
		json["activationDate"] = startdate;
		json["deactivationDate"] = enddate;
		json["commissionrate"] = commissionrate;
		json["membershare"] = membershare;
		json["brandName"] = "";
		json["catpath"] = catpath;

		var formData = new FormData();
		formData.append('json', JSON.stringify(json));
		$.ajax({
			url : "${contextPath}/admin/checkExistingDetailsStartDate",
			data : formData,
			dataType : 'text',
			processData : false,
			contentType : false,
			type : 'POST',
			success : function(response) {
				if (response == "failure") {
					successStartDateFlag = true;
				}
				if (response == "success") {
					successStartDateFlag = false;
				}
				$("#addCommissionBean").validate().element('#datepicker13');
			}
		});

	});
	$('#datepicker14').on('change', function() {
		$("#addCommissionBean").validate().element("#datepicker13");
		$("#addCommissionBean").validate().element("#datepicker14");
		var id = $('#myModalab').data('id');
		var partno = $('#myModalab').data('partno');
		var partcatno = $('#myModalab').data('partcatno');
		var partproductno = $('#myModalab').data('partprdctno');
		//var brandName = $("#add_brandName").val();
		var catPath = $("#add_categoryPath").val();

		var startdate, enddate;
		startdate = $("#addStartDate").val();
		enddate = $("#addEndDate").val();
		var obj = null;
		var json = {};
		var commissionrate = $("#add_partnercommission").val();
		var membershare = $("#add_jpmilecommission").val();
		json["partNo"] = partno;
		json["partCategoryNo"] = partcatno;
		json["partProductNo"] = partproductno;
		json["activationDate"] = startdate;
		json["deactivationDate"] = enddate;
		json["commissionrate"] = commissionrate;
		json["membershare"] = membershare;
		json["brandName"] = "";
		json["catPath"] = catPath;
		var formData = new FormData();
		formData.append('json', JSON.stringify(json));
		$.ajax({
			url : "${contextPath}/admin/checkExistingDetailsEndDate",
			data : formData,
			dataType : 'text',
			processData : false,
			contentType : false,
			type : 'POST',
			success : function(response) {
				if (response == "failure") {
					successEndDateFlag = true;
				}
				if (response == "success") {
					successEndDateFlag = false;
				}
				$("#addCommissionBean").validate().element('#datepicker14');
			}
		});

	});
</script>

<script>
	$.validator.addMethod('duplicateDates_', function(value, element) {
		if (successflag) {
			return false;
		} else {
			return true;
		}

	});

	var currentDate = $.datepicker.formatDate('dd/mm/yy', new Date());
	var startDate = $('#datepicker13').val();
	if (getFormattedDate(startDate) < getFormattedDate(currentDate)) {
		$('#datepicker13').attr("disabled", "true");
	}

	var fullDate = new Date();
	var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate
			.getMonth() + 1) : '0' + (fullDate.getMonth() + 1);
	var today = fullDate.getDate() + "/" + twoDigitMonth + "/"
			+ fullDate.getFullYear();

	$.validator.addMethod("dateHigherThanToday_", function(value, element) {
		var startDate = getFormattedDate($('#datepicker13').val());
		var endDate = getFormattedDate($('#datepicker14').val());

		if (endDate == 'Invalid Date')
			return true;
		else if (startDate <= endDate)
			return true;
		else if (startDate >= endDate)
			return false;

	});

	$.validator.addMethod("greaterThanEndDate_", function(value, element) {
		var startDate = getFormattedDate($('#datepicker13').val());
		var endDate = getFormattedDate($('#datepicker14').val());
		if (endDate == 'Invalid Date')
			return true;
		if (endDate >= startDate)
			return true;
		if (endDate <= startDate)
			return false;
	});

	$('#add_lowerValue').on('change', function() {
		$("#addCommissionBean").validate().element('#add_lowerValue');
		$("#addCommissionBean").validate().element('#add_lowerValue');
	});

	$('#edit_upperValue').on('change', function() {
		$("#addCommissionBean").validate().element('#add_upperValue');
		$("#addCommissionBean").validate().element('#add_upperValue');
	});

	$.validator.addMethod("addUpperValueGreaterThanLower", function(value,
			element) {
		var lowerValue = $('#add_lowerValue').val();
		var upperValue = $('#add_upperValue').val();
		if (lowerValue == '' || upperValue == '') {
			return true;
		} else if (lowerValue - upperValue > 0) {
			return false;
		} else {
			return true;
		}

	});

	$.validator.addMethod("addLowerValueSmallerThanHigher", function(value,
			element) {
		var lowerValue = $('#add_lowerValue').val();
		var upperValue = $('#add_upperValue').val();

		if (lowerValue == '' || upperValue == '') {
			return true;
		} else if (lowerValue - upperValue > 0) {
			return false;
		} else {
			return true;
		}

	});

	$("#addCommissionBean")
			.validate(
					{
						ignore : [],
						rules : {
							datepicker13 : {
								required : true,
								dateHigherThanToday_ : true,
								duplicateDatesStart : true
							},

							datepicker14 : {
								duplicateDatesEnd : true,
								greaterThanEndDate_ : true
							},
							add_partnercommission : {
								required : true,
								number : true
							},
							add_jpmilecommission : {
								required : true,
								number : true
							},
							add_maxJpmiles : {
								digits : true,
								rangelength : [ 1, 6 ]
							},
							add_lowerValue : {
								number : true,
								addLowerValueGreaterThanUpper : true
							},
							add_upperValue : {
								number : true,
								addUpperValueGreaterThanLower : true
							},
							add_bonusCommissionRate : {
								number : true
							}
						},
						messages : {
							datepicker13 : {
								required : "Please enter Commission Start Date",
								duplicateDatesStart : "Details Exists, Please select another date",
								dateHigherThanToday_ : "Start date must be less than or equal End date ",

							},
							datepicker14 : {
								duplicateDatesEnd : "Details Exists,Please enter another date",
								greaterThanEndDate_ : "End Date must be greater than or equal to start date"
							},
							add_partnercommission : {
								required : "Please enter Partner Commission",
								number : "Please enter digits only"
							},
							add_jpmilecommission : {
								required : "Please enter JP Mile Commission",
								number : "Please enter digits only"
							},
							add_maxJpmiles : {
								digits : "Please enter numeric data",
								rangelength : "Maximum 6 digits permitted"
							},
							add_lowerValue : {
								number : "Please enter numeric data",
								addLowerValueGreaterThanUpper : "Lower Value Should Be smaller than Upper Value"
							},
							add_upperValue : {
								number : "Please enter numeric data",
								addUpperValueGreaterThanLower : "Upper Value Should Be Greater than Lower Value"
							},
							add_bonusCommissionRate : {
								number : "Please enter digits only"
							}
						},
						errorElement : "div",
						errorPlacement : function(error, element) {
							if ($(element).hasClass("startDate")) {
								error.insertAfter($(element).closest("div"));
							} else if ($(element).hasClass("endDate")) {
								error.insertAfter($(element).closest("div"));
							} else {
								error.insertAfter(element);
							}
						},
						submitHandler : function(form) {
						},
					});

	$('#addNewCommission')
			.click(
					function() {
						var partcommno = $('#myModalab').data('id');
						var partno = $('#myModalab').data('partno');
						var partcatno = $('#myModalab').data('partcatno');
						var partproductno = $('#myModalab').data('partprdctno');
						//var brandName = $("#add_brandName").val();
						var categoryPath = $("#add_categoryPath").val();
						var json = {};
						var prtNo_ = $("#prtNo_").val();
						var startdate, enddate;
						startdate = $("#datepicker13").val();
						enddate = $("#datepicker14").val();
						json["partnerActivationDate"] = startdate;
						json["partnerDeactivationDate"] = enddate;
						json["partcommno"] = partcommno;
						json["partnerNo"] = partno;
						json["partnerCatNo"] = partcatno;
						json["partnerPrdctNo"] = partproductno;
						json["partnerCommission"] = $("#add_partnercommission")
								.val();
						json["jpMileCommission"] = $("#add_jpmilecommission")
								.val();
						json["commissionStatus"] = $(
								'input[name=add_commissionstatus]:checked')
								.val();
						json["brandName"] = "";
						json["categoryPath"] = categoryPath;
						json["maxJpmiles"] = $("#add_maxJpmiles").val();
						json["lowerValue"] = $("#add_lowerValue").val();
						json["upperValue"] = $("#add_upperValue").val();
						json["commType"] = $('input[name=add_commType]:checked')
								.val();
						json["transactionStatus"] = $('input[name=add_transactionStatus]:checked').val();
						json["upperValue"] = $("#add_upperValue").val();

						var bonusCommissionType = $(
								'input[name=add_bonusCommissionType]:checked')
								.val();

						if (bonusCommissionType == undefined) {
							bonusCommissionType = "";
						}

						json["bonusCommissionType"] = bonusCommissionType
						json["bonusCommissionRate"] = $(
								"#add_bonusCommissionRate").val();
						json["bonusPartnerCode"] = $("#add_bonusPartnerCode")
								.val();
						json["bonusActivityCode"] = $("#add_bonusActivityCode")
								.val();

						var formData = new FormData();
						formData.append('json', JSON.stringify(json));
						if ($("#addCommissionBean").validate().element(
								'#add_partnercommission')
								&& $("#addCommissionBean").validate().element(
										'#add_jpmilecommission')
								&& $("#addCommissionBean").validate().element(
										'#datepicker13')
								&& $("#addCommissionBean").validate().element(
										'#datepicker14')
								&& $("#addCommissionBean").validate().element(
										'#add_maxJpmiles')
								&& $("#addCommissionBean").validate().element(
										'#add_bonusCommissionRate')) {
							$
									.ajax({
										url : "${contextPath}/admin/addNewCommission",
										data : formData,
										dataType : 'text',
										processData : false,
										contentType : false,
										type : 'POST',
										success : function(response) {
											$('#myModal3').modal('hide');
											window.location.href = "${contextPath}/admin/commission?addComMsg="
													+ response
													+ "&partNo="
													+ prtNo_;
										}
									});

						}
					});

	$("#partnerCategory").on("selectmenuselect", function() {
		var prtNo = $("#partnerNo option:selected").val();
		$("#rpName").val('');
		$("#prtProductNo").val('');
		$("#datepicker1").trigger("change");
		if (prtNo == '') {
			alert("Please select the Partner");
			$("#rpName").val('');
			$("#partnerNo-button").focus();
			return false;
		}
	});
</script>
<!-- Added Validation by pawan -->
<script>
	$('#search')
			.on(
					'click',
					function() {
						var input_from = $('#fromDate').val();
						var input_to = $('#toDate').val();
						var input_dropdown = $('#prtNo_').val();
						var valid = false;
						

						if (input_dropdown != ''
								&& ((input_to == '' && input_from == '') || (input_to != '' && input_from != ''))) {
							
							$('.error').addClass('hidden');
							valid = true;
						} else if (input_to != '' && input_from == '') {
							$('.pick_date.start_date').next('.error')
									.removeClass('hidden');
							$("#fromDate").focus();
							valid = false;
						} else if (input_to == '' && input_from != '') {
							$('.pick_date.end_date').next('.error')
									.removeClass('hidden');
							$("#toDate").focus();
							valid = false;
						} else if (input_dropdown == ''
								&& (input_to != '' && input_from != '')) {
							$('.error').addClass('hidden');
							valid = true;
						} else if(input_from==''&& input_to == ''&& input_dropdown=='' ){
							valid =true;
							$('.error').addClass('hidden');
							
						}else {
							$('.error').removeClass('hidden');
							$("#fromDate").focus();
							valid = false;
						}
					
					
						if (valid) {
							if(table1 != undefined){
								
								table1.destroy();
								}
							var partNo = $('#prtNo_').val();
							var fromDate = $('#fromDate').val();
							var toDate = $('#toDate').val();
							  table1= $('#table').DataTable( {
							      "iDisplayLength" : 10,
							      "ordering": false,
							      "bPaginate": true,
							      "bProcessing": true,
							      "searching": false,
							     // "scrollX": true, 
							      "serverSide": true,
							  "ajax":"${contextPath}/admin/commissionServer?fromDate="+ fromDate+ "&partNo="+ partNo+ "&toDate=" + toDate,
							  "type": 'GET',
							  "columns": [
								     { "data": "partNo" }, 
							       { "data": "prtCatNo"},
							        { "data": "prtProductNo"},
							        { "data": "partnerName" }, 
							        { "data": "prtCategoryName" },
							        { "data": "prtProductName" },
							         {"data": "commisionType"} ,
							           
							        {"data": "transactionStatus"},
							         
							        { "data": "commissionRate" },
							        { "data": "memberShare" },
							        { "data": "lowerValue" },
							        { "data": "upperValue" },
							        { "data": "activationDate" },
									{ "data": "deactivationDate" },
									{"data": "maxJpmiles"},
							         { "data":"commissionSatus" }, 
							        
							       /*  { "data": "commissionStatusClass" },
							        { "data": "bonusCommissionType" },
							        { "data": "bonusCommissionRate" },
							        { "data": "transactionStatus" }, */
							     
							        {
						                "data": "null",
						                className: "center",
						                defaultContent: '<a class="editcommission" data-target="#myModal1"><img class="edit_wraper" src="${contextPath}/static/img/edit_icon.png"</a> <a class="view_details" data-target="#myModal2"><img class="edit_wraper" src="${contextPath}/static/img/View_details_icon1.png"></a>'
						                +'<a class="addcommission_" data-target="#myModalab"><img src="${contextPath}/static/img/001446.png " style="width:15px" class="fa fa-trash fa-lg" ></img></a>'
											
							
						            }  
							    ]  ,
							  "columnDefs": [
								  { targets : [6],
							          render : function (data, type, row) {
							             return data == '1' ? 'Pecentage' : 'Flat Amount'
							          }
							        },
							        { targets : [7],
								          render : function (data, type, row) {
								             return data == '1' ? 'Tentative/Processed' : 'Processed'
								          }
								        },
								  {
						                "targets": [ 0 ],
						                "visible": false,
						                "searchable": false
						            },
						            {
						                "targets": [ 1],
						                "visible": false
						            },
						            {
						                "targets": [ 2],
						                "visible": false
									},
									{
						                "targets": [ 14],
						                "visible": false
						            },
								  { "defaultContent": "-", "targets": "_all" }
								  ],
								  
							  "buttons" : [ {
									extend : 'excel',
									text : 'export to excel',
									title : 'view_commission_' + datetext
								}, ],
								"createdRow": function( row, data, dataIndex){
									  
		                            if(data.commissionSatus ==='Active' ){
		                            
		                            	$('td', row).eq(11).css('color', 'Green');
		                            }
		                            else {
		                            	$('td', row).eq(11).css('color', 'Red');
		                            }     }
							}); 
						}
					});
</script>

<script>
	$('#prtNo_').on('selectmenuchange', function() {
		var prtNo_ = $('#prtNo_').val();
		if (prtNo_) {
			$('.partner_no').find('.error').addClass('hidden');
			$('.pick_date.start_date').next('.error').addClass('hidden');
			$('.pick_date.end_date').next('.error').addClass('hidden');

		}
	});

	$('#toDate,#fromDate').on('change', function() {
		var toDate = $('#toDate').val();
		var fromDate = $('#fromDate').val();
		$('.pick_date.start_date').next('.error').addClass('hidden');
		$('.pick_date.end_date').next('.error').addClass('hidden');

		if (toDate == '' && fromDate == '') {
			$('.pick_date.start_date').next('.error').removeClass('hidden');
			$('.pick_date.end_date').next('.error').removeClass('hidden');
		} else if (toDate == '' && fromDate != '') {
			//$('.pick_date.end_date').next('.error').removeClass('hidden');
			$('.pick_date.start_date').next('.error').addClass('hidden');
		} else if (toDate != '' && fromDate == '') {
			$('.pick_date.start_date').next('.error').removeClass('hidden');
			$('.pick_date.end_date').next('.error').addClass('hidden');
		}
	});

	$("#commissionClear").on(
			'click',
			function() {
				$('.error').addClass('hidden');
				$("#prtNo_").val("");
				$("#prtNo_-button").children(".ui-selectmenu-text").html(
						"-Select Partner-")
				$("#toDate").val("");
				$("#fromDate").val("");
			});
</script>

<script type="text/javascript">
	$.validator.addMethod("minChar", function(value, element) {
		var jpMileConfig = $('#jpMileConfig').val();
		if (jpMileConfig.length <= 3) {
			return true;
		} else {
			return false;
		}
	});

	$('#jpMileConfig').on('keyup', function() {
		$("#jpMileConfigForm").validate().element('#jpMileConfig');
	});

	$(document).ready(function() {
		$('#percentage').prop("checked", true);
		$('#tentative').prop("checked", true);
		<c:if test="${addJPMiles_== true}">
		$('#Commission_').removeClass('active');
		$('#sectionA').removeClass('in active');
		$('#addCommission_').removeClass('active');
		$('#addJPMiles_').addClass('active');
		$('#sectionC').addClass('in active');
		$('#sectionD').removeClass('active');
		</c:if>

		$("#jpMileConfigForm").validate({
			ignore : [],
			rules : {
				jpMileConfig : {
					required : true,
					digits : true,
					minChar : true
				}
			},
			messages : {
				jpMileConfig : {
					required : "Please enter JPMiles",
					digits : "Please enter numeric data",
					minChar : "Only 3 characters allowed"
				}
			},
			errorElement : "div",
			errorPlacement : function(error, element) {
				error.insertAfter(element);
			},
			submitHandler : function(form) {
				form.submit();
			},
		});

	});
</script>
<script type="text/javascript">
	$('#lowerValue,#upperValue').on('change', function() {
		$("#datepicker1").trigger("change");
	});
	$(document).ready(
			function() {
				<c:if test="${uploadCommission==true}">
				$('#Commission_').removeClass('active');
				$('#sectionA').removeClass('in active');
				$('#addCommission_').removeClass('active');
				$('#addJPMiles_').removeClass('active');
				$('#uploadCommission').addClass('active');
				$('#sectionC').removeClass('active');
				$('#sectionD').addClass('in active');
				</c:if>

				$('#uploadCommission').on('click', function() {
					$('#Commission_').removeClass('active');
					$('#sectionA').removeClass('in active');
				});

				$("#uploadCSVForm").validate({
					ignore : [],
					rules : {
						uploadCSVFile : {
							required : true,
							extension : "csv" 
						}
					},
					messages : {

						uploadCSVFile : {
							required : "Please upload csv file",
							extension : "File format shoud be .csv"
						}
					},
					errorElement : "div",
					errorPlacement : function(error, element) {
						if ($(element).hasClass("uploadBtn2")) {
							error.insertAfter($(element).closest("div"));
						} else {
							error.insertAfter(element);
						}
					},
					submitHandler : function(form) {
						form.submit();
					},
				});
				$('#UploadFile').on('click', function() {
					$('#uploadCommission').addClass('active');
					$('#sectionD').addClass('active');
				});
				$('#UploadFile').on('change', function() {
					$('#UploadFile').val('');

				});

				$('#uploadCSVFile').on(
						'change',
						function() {
							var filename = $(this).val();
							var datetext;
							var d = new Date(); // for now
							var h = d.getHours();
							h = (h < 10) ? ("0" + h) : h;
							var m = d.getMinutes();
							m = (m < 10) ? ("0" + m) : m;
							var s = d.getSeconds();
							s = (s < 10) ? ("0" + s) : s;
							var month = d.getMonth() + 1;
							var day = d.getDate();
							var year = d.getFullYear();
							datetext = day + "-" + month + "-" + year + "_" + h
									+ ":" + m + ":" + s;

							$('#uploadFile2').val($(this).val());
							$('#filename').val("commission file-" + datetext);
							/* $('#filename').val($(this).val()); */
							$("#uploadCSVForm").validate().element(
									'#uploadCSVFile');

						});
			});
	

		 var table1;
		$(document).ready(function() {
			$('#search').trigger("click");
			 
		  }); 
	
	
</script>
