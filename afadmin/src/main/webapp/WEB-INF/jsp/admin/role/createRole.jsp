<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<form:form autocomplete="off"   id="adminRolesBean" commandName="adminRolesBean">
<section class="insurance_management">
	<div id="breadcrumb">
		<ul>
			<li>Home</li>
			<li>Management</li>
			<li>Role Management</li>
			<li>Add Role</li>
		</ul>
	</div>
	<!--- breadcrumb --->
	<div class="section_heading">
		<h5>Add Role</h5>
	</div>
	
		<div class="main_section bg-b-w">
			<div class="row pad_top">
				<div class="col-md-4">
					<label>Role Name</label><span class="mandatory_star">*</span> 
					<form:input path="roleName"  maxlength="50" class="form-control" />
				</div>
				<div class="col-md-4">
					<label>Description</label><span class="mandatory_star">*</span> 
					<form:input path="description" maxlength="50" class="form-control" />
				</div>
			</div>
			<!----- row ------->
		</div>
		<div class="edit_role_table user_management checkbox">
			<table class=" table table-bordered" cellspacing="0" cellpadding="0" width="100%">
				<thead>
					<tr>
					<tr>
						<th>Permission</th>
						<th>Action</th>
					</tr>
				</thead>
				<!---- thead --->
				<tbody class="label_font">
					<c:forEach items="${permissionValues}" var="permissionValue">
						<tr>
							<td>${permissionValue.permissionName}</td>
							<td>
								<form:checkbox path="rolePermisionId"  class="one_required" value="${permissionValue.permissionId}" label="Allow"/>
							</td>
						</tr>
					</c:forEach>	
				</tbody>
				<!--- tbody --->
			</table>
			<!--- table -->
		</div>
		<div class="btn_save pull-right">
		
				 <a href="${contextPath}/admin/roleManagement" ><input class="bg_g_button" type="button" value="Cancel"/></a>
					<input class="bg_g_button" type="button" value="Save" onClick="$(this).closest('form').submit();"/>
			
		</div>
	
	<!----------- button ----------->
</section>
</form:form>
<script type="text/javascript">
var roleNameExist=false;
$(document).ready(function () {
	$.validator.addMethod("AlphaNumericOnly", function(value, element) {
		return this.optional(element) || /^[a-z][a-z0-9@\s]*$/i.test(value);
	});
	$.validator.addMethod("AlphabetsOnly", function(value, element) {
			return this.optional(element) || /^[a-z][a-z@\s]*$/i.test(value);
		});
	 $.validator.addMethod('duplicateCheckRole', function(value, element) {
	        if(roleNameExist){
	           return false;
	        }else{
	         return true;
	        }
		
		});
	 
	 $.validator.addMethod("one_required", function() {
		    return $("#adminRolesBean").find(".one_required:checked").length > 0;
		}, 'Please select at least one checkbox.');

$("#adminRolesBean").validate({
	
	 errorPlacement: function(error, element) {
	        if ($(element).hasClass("one_required")) {
	            error.insertBefore($(element).closest("table"));
	        } else {
	            error.insertAfter(element);
	        }
	    },

    ignore:[],
	rules: {
		roleName:{
			required: true,
			duplicateCheckRole:true,
			AlphabetsOnly:true
        },
        description:{
        	required: true,
        	AlphaNumericOnly:true
        	
        },
        
        
 },

 messages: {
	roleName:{
		required: "Please enter a role name",
		duplicateCheckRole:"Role name already exists",
		AlphabetsOnly:"Alphabets only"
	},
	description:{
		required:"Please enter role description",
		AlphaNumericOnly:"Alphanumeric characters only"
		
	},
	
 },
 errorElement: "div",
 submitHandler: function (form) {
	form.submit();
},
});
$("#roleName").change(function(){
	var roleName1=$('#roleName').val();
	$.get("${contextPath}/admin/checkRoleName?roleName="+roleName1, function(data,status) {
		if(status=="success" && data!=''){
			roleNameExist=true;	
		}
		else{
			roleNameExist=false;
			}
		$("#adminRolesBean").validate().element('#roleName');
	});
});
});
</script>

