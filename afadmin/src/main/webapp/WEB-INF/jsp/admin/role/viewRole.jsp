<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
<body>
<h3>Create Role</h3>
<form:form method="post" commandName="adminRolesBean">
 
    <table>
    <tr>
        <td><label>Role Name</label></td>
        <td><form:input path="roleName" id="roleName" maxlength="50" placeholder="Enter role name" /></td> 
    
        <td><label>Role Description</label></td>
        <td> <form:input path="description"  maxlength="50" placeholder="Role description"/></td>
    </tr>
    <tr>
    	<td>Permission</td>
    	<td>Actions</td>
    </tr>

      
       <c:forEach items="${permissionActionsForvalues}" var="permissionActions"> 
            <tr>
                <td>${permissionActions.key.permissionName}</td>
                <c:forEach items="${permissionActions.value}" var="permissionAction">
                <td>
	    		  <form:checkbox path="permissionActions" value="${permissionActions.key.permissionId}#${permissionAction.key}" label="${permissionAction.value}"/>
	    	    </td>
                </c:forEach>
        	</tr>
       </c:forEach>
     
   
</table>  
     
</form:form>

</body>
</html>


