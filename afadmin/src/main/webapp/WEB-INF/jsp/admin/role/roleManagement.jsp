<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script>
	function changeURL() {
		history.pushState({}, '', "${contextPath}/admin/roleManagement");
	}
</script>
<section class="insurance_management">
	<div id="breadcrumb">
		<ul>
			<li>Home</li>
			<li>Management</li>
			<li>Role Management</li>
		</ul>
	</div>
	<div id="myModala" class="modal fade" role="dialog">	
			<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Are You Sure?</h4>
				</div><!--header-->
				<div class="modal-body">
					<p>
										<img src="${contextPath}/static/img/info_icon1.png"></img>
										<span id="deleteRoleId">will be deleted.Are you sure?</span> 
									</p>
				</div><!---body--->
				<div class="modal-footer">
					<button id="btnDelteYes" class="btn btn-grey bgp" data-dismiss="modal">Delete</button>
				</div>
			</div><!---modal content--->
			</div>
		</div>	
	<c:if test="${fn:length(message) gt 0}">
	 <script>
				changeURL();
			     </script>
		<div class="alertMessage" style="color: green; font-weight: bold;">
			<c:out value="${message}"></c:out>
		</div>
	</c:if>
	 <c:if test="${fn:length(errormessage) gt 0}">
	  <script>
				changeURL();
			     </script>
	 <div class="alertMessage" style="color:red;font-weight: bold;"  >
	 	<c:out value="${errormessage}">
	 	</c:out>
	 </div> 
	 </c:if>
	<!---breadcrumb--->
	<div class="section_heading">
		<span>Role Management</span> <a class="bg_w_button"
			href="${contextPath}/admin/createRole">Add New Role</a>
	</div>
	
	<div class="insurance_policy_table user_management role_management mar_bottom">
		<table class=" table table-bordered" cellspacing="0" cellpadding="0" width="100%">
			<thead>
				<tr>
				<tr>
					<th>Role Name</th>
					<th>Description</th>
					<th class="width_section">Action</th>
				</tr>
			</thead>
			<!----thead--->
			<tbody>
				<c:forEach items="${roles}" var="role">
					<tr data-id="${role.roleId}" data-roleName="${role.roleName}">
						<td>${role.roleName}</td>
						<td>${role.description}</td>
						<td>
							<span class="edituser">
								<a href="${contextPath}/admin/updateRole?roleId=${role.roleId}"> <img class="edit_wraper" src="${contextPath}/static/img/edit_icon.png"></a>
							</span>
							<c:if test="${role.roleId!=1}">
								<span class="delete_icon" id="deleteUser" data-target="#myModala"> 
									<img src="${contextPath}/static/img/delete_icon.png" class="fa fa-lg"></img>
								</span>
							</c:if>
						</td>
					</tr>
				</c:forEach>
			</tbody>
			<!---tbody--->
		</table>
		<!---table-->
	</div>
</section>
<script type="text/javascript">

	$('span#deleteUser').on('click', function () {
		var id = $(this).closest('tr').data('id');
		//var roleName= $(this).closest('tr').data('roleName');
		$('#deleteRoleId').text(" Selected role will be deleted. Do you want to continue?");
		$('#myModala').data('id', id).modal('show');
	});

	$('#btnDelteYes').click(function () {
		var id = $('#myModala').data('id');
		$.get("${contextPath}/admin/deleteRole?roleId="+id , function(data,status) {
			if(status=="success" ){
				$('[data-id=' + id + ']').remove();
				$('#myModala').modal('hide');
			}
		});
	});
	
</script>
