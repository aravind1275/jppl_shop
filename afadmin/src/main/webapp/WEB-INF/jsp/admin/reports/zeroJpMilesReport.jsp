<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script>
	function changeURL(){
                	history.pushState({}, '',"${contextPath}/admin/zeroJpMilesReport");
                	
        }
</script>

<div class='container'>
<section class="affliates_management">
            <div id="breadcrumb">
				<ul>
					<li>Home</li>
					<li>Reports</li>
					<li>Zero JpMiles Report</li>
				</ul>
			</div><!---breadcrumb--->
			
			    <c:if test="${fn:length(message) gt 0}">
	              <script>changeURL();</script>
	                <div class="alertMessage" id="divChanged" style="color:green;font-weight: bold;"  ><c:out value="${message}"></c:out></div> </c:if>
	  			<c:if test="${fn:length(errormessage) gt 0}">
	  			   <script>changeURL();</script>
	       		<div class="alertMessage" style="color:red;font-weight: bold;"  ><c:out value="${errormessage}"></c:out></div> 
	       		<script>
	       		setTimeout(function(){ 
	       			document.location.reload(true);
	       		}, 2000);</script>
	       		</c:if>
				<div class="section_heading">
				
				<h5>Zero Jp Miles Report</h5>
				</div>
	
	<div class="management">
	<ul class="nav nav-tabs">
    <li id="zeroJpmilesList"  class="active"><a data-toggle="tab" href="#sectionA">Report Of Zero Jp Miles.</a></li>
    <li id="zeroJpmilesFileUpload" ><a data-toggle="tab" href="#sectionB">File uploading Of Zero Jp Miles</a></li>
    </ul>
	<div class="tab-content">
	<div id="sectionA" class="tab-pane fade in active">		
	<form:form autocomplete="off" id="zeroJpMilesReport" action="zeroJpMilesReport" class="reports">
			<!-- <div class="main_section bg-b-w "> -->
			
					<div class="row pad_top">
					<div class="col-md-2">
						<label>Start Date</label><span class="mandatory_star">*</span>
						<div class="pick_date">
							<input type="text" class="form-control startDate" id="fromDate" name="fromDate" value="${fromDate}" readonly />
<!-- 							<p id="log" style="display:none;color:red;"></p> -->
						</div>
					</div>
					
					<div class="col-md-2">
						<label>End Date</label><span class="mandatory_star">*</span>
						<div class="pick_date">
							<input type="text" class="form-control endDate" id="toDate" name="toDate" value="${toDate}" readonly />
<!-- 							<p id="log" style="display:none;color:red;"></p> -->
						</div>
					</div>
					
					
					<!-- <div class="col-md-2">
						<label>Partner Type</label><span class="mandatory_star">*</span>
						<fieldset class="infield">
							<select name="partnerTypeRepo" id="partnerTypeRepo" class="select_option">
								<option value="">-Select Partner-</option>
							
							                <option value="host">Host Partner</option>
  											<option value="default">Search Partner</option>
  											<option value="logo">Logo Partner</option>
  											<option value="offline">Offline Partner</option>
  											<option value="talk">Talk Partner</ption>
  											<option value="read">Read Partner</option>
							 -->
								<%-- <c:forEach items="${partnerList}" var="partner">
									<c:choose>
										<c:when test="${partner.prtNo eq prtNo}">
											<option value="${partner.prtNo}" selected >${partner.partnerName}</option>
										</c:when>
										<c:otherwise>
											<option value="${partner.prtNo}" >${partner.partnerName}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach> --%>
							<%-- </select>
							<c:if test="${checkValPostReport==true}">
							<input type="hidden" id="hiddenPartnerTypeRepo" value="${partType}">
							</c:if>
						</fieldset>
					</div> --%>
					
					
					
					
					<div class="col-md-2">
						<label>Filter By</label><span class="mandatory_star">*</span>
						<fieldset class="infield">
							<select name="partnerName" id="partnerName" class="select_option">
								<option value="">-Select Partner-</option>
								<option value="All">All</option>								
								<c:forEach items="${partnerList}" var="partner">
									<c:choose>
										<c:when test="${partner.prtNo eq prtNo}">
											<option value="${partner.prtNo}" selected >${partner.partnerName}</option>
										</c:when>
										<c:otherwise>
											<option value="${partner.prtNo}" >${partner.partnerName}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
 							</select>
    						<%--  <c:if test="${checkValPostReport==true}">
                             <input type="hidden" id="hiddenPrtNo" value="${prtNo}">
                             </c:if> --%>
						</fieldset>
					</div>
					
					<div class="col-md-2">
						<label>JP Number</label><span class="mandatory_star"></span>
						<input type="text" class="form-control" name="jpNumber" id="jpNumber" placeholder="Enter JP No" value="${jpNumber}" />
					</div>
					
					<!--new field status starts  -->
					<div class="col-md-2">
						<label>Status</label><span class="mandatory_star">*</span>
						<fieldset class="infield">
							<select name="status" id="status" class="select_option">
								 <option value="">-Select Status-</option>
								<c:forEach items="${statusList}" var="stat">
									<c:choose>
										<c:when test="${stat eq status}">
											<option value="${stat}" selected >${stat}</option>
										</c:when>
										<c:otherwise>
											<option value="${stat}" >${stat}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</fieldset>
					</div>
					
					
					
					<!--new field status ends  -->
					<div class="row pad_top export_btn">
					<!-- radio for valid invalid -->
					<div id="radioValid">
					<input type="radio" class="css-checkbox" name="validTypeRadio" id ="validTypeRadio1" value="valid" checked="checked">
                       <label class="css-label radGroup2" for="validTypeRadio1">Valid</label>
                    <input type="radio" class="css-checkbox" name="validTypeRadio" id ="validTypeRadio2" value="invalid" >
                       <label class="css-label radGroup2" for="validTypeRadio2">Invalid</label>
                       </div>
                       <input type="hidden" id="hiddenVal"  value="${hiddenVal}" />
                       <br>
					</div>
					</div>
					<!--  <div class="col-md-3 upload_keyword">
									<label>Upload KeyWord File</label><span class="mandatory_star">*</span>
									<div class="fileUpload btn ">						
										<input name="uploadFile2" id="uploadFile2"  class="upload_file" placeholder=".csv file only" disabled="disabled" />
										<span class="bg_w_button">Browse</span>									
										<input type="file" name="uploadCSVFile" id="uploadCSVFile" class="upload uploadfile_button"    />
										<input type="hidden" id="filename" name="filename"  />
									</div>
	                              
								</div>
								<div class="col-md-1">
									  <div class="fileUpload btn ">
	                                	<input class="bg_g_button" type="submit"  id="UploadFile" value="Upload" />
										<button type="submit" class="bg_g_button" id="UploadFile" onClick="$(this).closest('form').submit();">Upload</button>
									</div>
								</div> -->
					<div class="row pad_top">
					<div class="col-md-2 report_button">
							<div class="all-default-button">
								<input class="bg_w_button" id="Search" type="button" onClick="$(this).closest('form').submit();" value="Search" />
								 <input class="bg_w_button" type="button" id="clear" value="Clear" />
							</div>
							<div class="all-csv-export-button">
								<input type="button" class="bg_w_button_new" id="export_new" value="Export to csv"> 
								<input class="bg_w_button" type="button" id="clear_new" value="Clear" />
							</div>
							
						</div>
						
				
						
						
				</div>
		
			
			<!-- </div> main section -->
			
			
				<c:if test="${! empty zeroJpMilesReport }">
				<div class="report_table">

					<table class="partner_info_table table-bordered" id="business_report " cellspacing="0" cellpadding="0" width="100%"><thead>
							<tr>
								<th>JP Number</th>
								<th>Reference Number</th>
								<th>Affiliate Order Number</th>
								<th>Base Jp miles</th>
								<th>Email Status</th>
								<th>Sms Status</th>
								<th>Bonus Jp miles</th>
							</tr>
						</thead>
						<!--- thead ---->
						<tbody>
												
						
							<c:forEach items="${zeroJpMilesReport}" var="zeroReport">
							<tr>
								<td>${zeroReport.jpNumber}</td>
								<td>${zeroReport.transactionId}</td>
								<td>${zeroReport.partnerItemId}</td>
								<td>${zeroReport.jpmilesPurchase}</td>
								<td>${zeroReport.emailStatus}</td>
								<td>${zeroReport.smsStatus}</td>
								<td>${zeroReport.bonusJpmiles}</td>
							</tr>
							</c:forEach>
						</tbody>
						<!--- tbody ---->
					</table>
					<!--- table ---->
				</div>
			</c:if> <!-- report list -->
			
			
			</form:form> <!-- zeroJpMilesReport form -->
			
			</div><!-- section A -->
			
			
			<div id="sectionB" class="tab-pane fade submenu" >
			 <form id="uploadCSVForm" name="uploadCSVForm" method="POST" action="uploadZeroJpmilesFile" enctype="multipart/form-data">         
			<div class="row pad top">
                                <div class="col-md-3 upload_keyword">
                                <br>
                     
									<label>Upload KeyWord File</label><span class="mandatory_star">*</span>
									<div class="fileUpload btn ">						
										<input name="uploadFile2" id="uploadFile2"  class="upload_file" placeholder=".csv file only" disabled="disabled" />
										<span class="bg_w_button" style="position:relative;top:5px;">Browse</span>									
										<input type="file" name="uploadCSVFile" id="uploadCSVFile" class="upload uploadfile_button"    />
										<input type="hidden" id="filename" name="filename"  />
									<br>	
									</div>
									</div> 
									<div class="col-md-1">
									<p id="log" style="display:none;color:red;"></p>
	                                 <div class="fileUpload btn "  style="position:relative;top:16px;right:12px">
	                                	<!-- <input class="bg_g_button" type="submit"  id="UploadFile" value="Upload" /> -->
 										<button type="submit" class="bg_g_button" id="UploadFile" >Upload</button>
 										
									</div>
									
								</div>
								</div>
								<br><br>
								<label><span class="mandatory_star">*</span>NOTE:
Setting the Email & SMS status to "1" in the file and uploading it will trigger the transaction email & SMS to that JPMember.</label>
				
						</form>	
                         </div><!--- row --->
				
			
			
			
			
			
		</div><!-- tab content -->	
	</div>	<!-- management -->
</section>  <!-- section management ends -->
</div> <!-- div class container ends -->



<script type="text/javascript">
</script>

<!-- Form Validation -->
<script type="text/javascript">
$(document).ready(function () {
	
	 $("#radioValid").hide();
		if($("#status").val() == "All"){
			 $("#radioValid").show();
	 if($("#hiddenVal").val() == "valid"){
		 $("#validTypeRadio1").prop("checked", true);
	 }else if($("#hiddenVal").val() == "invalid"){
		 $("#validTypeRadio2").prop("checked", true);
	 }
		}
		/* <c:if test="${checkValPostReport==true}">
		
		var partnerTypeRepo=$('#hiddenPartnerTypeRepo').val();
		var partnerName = $('#hiddenPrtNo').val();
		 
		$("#prtType").val(prtType);
		$("#prtType").trigger("selectmenuchange", ["refresh", logoPrtNo]);
		
		</c:if> */
		
		
	$("#zeroJpMilesReport").validate({
		onkeyup: true,
		ignore:[],
		rules: {	
			fromDate:{
				required: true,
				dateHigherThanToday:true
			},
			toDate:{
				required: true,
				greaterThanEndDate:true,
				dateHigherThanToday:true
			},
			partnerName:{
				required: true,
			},
			status:{
				required: true,
			}
			
		},
		messages: {
			fromDate:{
				required: "Start date cannot be empty",
				dateHigherThanToday:"Start date must be less than or equal End date "
			},
			toDate:{
				required: "End date cannot be empty",
				greaterThanEndDate:"End date must be greater than or equal to Start date ",
				dateHigherThanToday:"Start date must be greater than or equal End date "
			},
			partnerName:{
				dropdowncheck: "Please select a Partner"
			},
			status:{
				dropdowncheck: "Please select a status"
			}
		},
		errorElement: "div",
		errorPlacement: function(error, element) {
			if ($(element).hasClass("form-control startDate")) {
				error.insertAfter($(element).closest("div"));
			}
			else if ($(element).hasClass("form-control endDate")){
				error.insertAfter($(element).closest("div"));
			}
			else {
				error.insertAfter(element);
			}
		},
		submitHandler: function (form) {
			form.submit();
		}
	});









$("#uploadCSVForm").validate({
	ignore:[],	
	rules: {
		uploadCSVFile:{
			required: true,
			extension: "csv"
		}
		},
	messages: {
		
		uploadCSVFile:{
			required: "Please upload csv file",
			extension: "File format shoud be .csv"
		}
	},
		errorElement: "div",
		errorPlacement: function(error, element) {
		if ($(element).hasClass("uploadBtn2")){
	    	error.insertAfter($(element).closest("div"));
		}
		else {
	    	error.insertAfter(element);
		}
		},
	submitHandler: function (form) {
		form.submit();
	},
});



});




$("#partnerTypeRepo").on( "selectmenuchange", function(event, type, mapCat){
	var obj=null;
	var prtType = $("#partnerTypeRepo").val();
	$.get("${contextPath}/admin/getPartnerWithType?prtType="+prtType, function(data){
		var obj = jQuery.parseJSON(data);
		$('#partnerName').empty();
		$('#partnerName').append("<option value=''>-Select-</option>");
		
		for(var i = 0; i < obj.length; i++){
			var value = obj[i];
			var newOption = $("<option value="+value.prtNo+">"+value.partnerName+"</option>");
			$('#partnerName').append(newOption);	       
		}
		$( "#partnerTypeRepo" ).selectmenu( "refresh" );
		
		 if(type == "refresh"){
			$("#partnerTypeRepo").selectmenu("refresh");
			$('#partnerName').val(mapCat);
		}else{
			$("#partnerName").selectmenu( "refresh" );
			//$("#textCatMapping").val('');
		} 
	});
	 
});

//UploadFile
/* $('#UploadFile').on('click', function(){
	alert($("#uploadFile2").val());
	
	
	if(($("#uploadFile2").val()=="")&& ($("#uploadFile2").val()==null)){
		alert("hold");
	 $("#log").html("End Date should be Greater than Start Date");
	 $("#log").show();
	 $('#log').fadeIn().delay(4000).fadeOut();
	}
	
	
	
	alert($("#uploadFile2").val());
	
	if(($("#uploadFile2").val()!="") && ($("#uploadFile2").val()!=null)){
	$(this).closest('form').submit();
	alert("gone");
	}
	
	alert($("#uploadFile2").val());
}); */
	
//UploadFile
$('#UploadFile').on('change', function(){
	$('#UploadFile').val('');
	
});

$('#uploadCSVFile').on('change', function() {
	var filename = $(this).val();
	var	datetext;
	var d = new Date(); // for now
	var h = d.getHours();
	h = (h < 10) ? ("0" + h) : h ;
	var m = d.getMinutes();
	m = (m < 10) ? ("0" + m) : m ;
	var s = d.getSeconds();
	s = (s < 10) ? ("0" + s) : s ;
	var month = d.getMonth()+1;
	var day = d.getDate();
	var year = d.getFullYear();
	datetext = day+"-"+month+"-"+year+"_"+h+":"+m+":"+s;
	
	
	
	$('#uploadFile2').val($(this).val());
	$('#filename').val("Zero Jp miles-"+datetext);
	/* $('#filename').val($(this).val()); */
	$("#uploadCSVForm").validate().element('#uploadCSVFile');

});

</script>

<!-- Events -->
<script type="text/javascript">
/* $('#search').click(function() {
    var sts=$('#fromDate').val();
    var end=$('#toDate').val();
    var selectId = $('#select_option_wrap').val();
    var b = end.split("-") ;
    var a = sts.split("-") ;
    var startDate1 = new Date( a[2], (a[1] - 1), a[0] ) ;
    var endDate1 = new Date( b[2], (b[1] - 1), b[0] ) ;
    startDate1.setHours(0,0,0,0);
    endDate1.setHours(0,0,0,0);
    if(startDate1===""){
        $("#log").html("Start Date cannot be empty");
        $("#log").show();
        $('#log').fadeIn().delay(2000).fadeOut();
     e.preventDefault();
    }
    if(endDate1===""){
        $("#log").html("End Date cannot be empty");
        $("#log").show();
        $('#log').fadeIn().delay(2000).fadeOut();
     e.preventDefault();
    }
   if(selectId===0){
        $("#log").html("Please select a partner");
        $("#log").show();
        $('#log').fadeIn().delay(2000).fadeOut();
     e.preventDefault();
   }
     if(endDate1<startDate1){
          $("#log").html("End Date should be Greater than Start Date");
            $("#log").show();
            $('#log').fadeIn().delay(2000).fadeOut();
         e.preventDefault();
     }
     else if('${servicerptList.size()}'>0){
   window.location.href=""
       
   }
   	
	//$(this).closest('form').submit();
     
}); */

$('#status').on('selectmenuchange', function () {

	if($("#status").val() == "All"){
		$("#radioValid").show();
		$("input[name='validTypeRadio']").on('change',function(){
			
				if( $("#validTypeRadio1").is(':checked')) {
				$("#validTypeRadio1").prop("checked", true);
				$("#validTypeRadio1").val("valid");
			}
				if($("#validTypeRadio2").is(':checked')) {
				$("#validTypeRadio2").prop("checked", true);
				$("#validTypeRadio2").val("invalid");
			}  
		});
	}
	else{
		$("#validTypeRadio1").val('');
		$("#validTypeRadio2").val('');
		$("#radioValid").hide();
		}
});

$("#clear").on('click', function () {
	$("#fromDate").val('');
	$("#toDate").val('');
	$("#jpNumber").val('');
	$('#partnerName').val('');
	$('#partnerName').selectmenu("refresh");
	//new report starts  
	$('#status').val('');
	$("#validTypeRadio1").val('');
	$("#validTypeRadio2").val('');
	$("#validTypeRadio2").prop("unchecked", true);
	$("#validTypeRadio1").prop("unchecked", true);
    $("#radioValid").hide();
	$('#status').selectmenu("refresh");
	//new report ends
	
	$("#zeroJpMilesReport").data('validator').resetForm();
	//$("#businessReport")[0].reset();
});

$( "#fromDate" ).on('change', function() {
	$("#zeroJpMilesReport").validate().element("#fromDate");
	var endDate = getFormattedDate($('#toDate').val());
	
	if(endDate!='Invalid Date'){
		$("#zeroJpMilesReport").validate().element("#toDate");
	}
});

$("#clear_new").on('click', function () {
	$("#fromDate").val('');
	$("#toDate").val('');
	$("#jpNumber").val('');
	$('#partnerName').val('');
	$('#partnerName').selectmenu("refresh");
	//new report starts  
	$('#status').val('');
	$("#validTypeRadio1").val('');
	$("#validTypeRadio2").val('');
	$("#validTypeRadio2").prop("unchecked", true);
	$("#validTypeRadio1").prop("unchecked", true);
    $("#radioValid").hide();
	$('#status').selectmenu("refresh");
	//new report ends
	
	$("#businessReport").data('validator').resetForm();
	//$("#businessReport")[0].reset();
});

$( "#toDate" ).on('change', function() {
	$("#zeroJpMilesReport").validate().element("#fromDate");
	$("#zeroJpMilesReport").validate().element("#toDate");
});

$('#partnerName').on('selectmenuchange', function () {
	
	var PartnerName  = $('#partnerName').find('option:selected').text();
	
	console.log("partnerN---->",PartnerName);
	
	if(PartnerName == "All")
		{
			console.log("inside if");
			 /* $(".all-csv-export-button").css({display: "block"});
			 $(".all-default-button").css({display: "none"}); */
			 /* $("#Search").prop('disabled', true); */
			 $(".all-csv-export-button").css("display","block");
			 $(".new_export").css("display","block");
			 $(".all-default-button").css("display","none");
			 $("#export").css("display","none");
			 
			$("#businessReport").validate().element('#partnerName');			
		}
	else
		{	
		console.log("inside else");
		/* $(".all-csv-export-button").css({display: "none"});
		 $(".all-default-button").css({display: "block"}); */
		 $(".all-csv-export-button").css("display","none");
		 $(".all-default-button").css("display","block");
		 $(".new_export").css("display","none");
			$("#businessReport").validate().element('#partnerName');
			 $("#export").css("display","block");
			
		}
});

$('#status').on('selectmenuchange', function () {
	$("#zeroJpMilesReport").validate().element('#status');
});
$( "#jpNumber" ).on('change', function() {
	var jpNumber_ = $("#jpNumber").val();
	if(jpNumber_ != "") {
		if($("#fromDate").hasClass('error')) {
			$("#fromDate").removeClass('error');
			$("#toDate").removeClass('error');
			$("#fromDate-error").html("");
			$("#toDate-error").html("");
			
			$('#fromDate').rules('add', {
				required: false,
				dateHigherThanToday : true
			});
			$('#toDate').rules('add', {
				required: false,
				greaterThanEndDate:true,
				dateHigherThanToday:true
			});
			$('#partnerName').rules('add', {
				dropdowncheck: false
			});
			$('#status').rules('add', {
				dropdowncheck: false
			});
			$("#zeroJpMilesReport").validate().element('#fromDate');
			$("#zeroJpMilesReport").validate().element('#toDate');
			$("#zeroJpMilesReport").validate().element('#partnerName');
			$("#zeroJpMilesReport").validate().element('#status');
		}
		if($("#partnerName").hasClass('error')) {
			$("#partnerName").removeClass('error');
			$("#partnerName-error").html("");
		}
		if($("#status").hasClass('error')) {
			$("#status").removeClass('error');
			$("#status-error").html("");
		}
	}
});
</script>

<!-- Validation Methods -->
<script type="text/javascript">
$.validator.addMethod("dateHigherThanToday", function(value, element) {
	var startDate = getFormattedDate($('#fromDate').val());
	var endDate = getFormattedDate($('#toDate').val());
	
	if(endDate!='Invalid Date'){
		if (startDate <= endDate)
			return true;
		
		if (startDate >= endDate)
			return false;
	}
	else{
		return true;
	}
});

$.validator.addMethod("greaterThanEndDate", function(value, element) {
	var startDate = getFormattedDate($('#fromDate').val());
	var endDate = getFormattedDate($('#toDate').val());
	
	if(endDate!='Invalid Date'){
		if (endDate >= startDate)
			return true;
		
		if (endDate <= startDate)
			return false;
	}
	else{
		return true;
	}
});

$.validator.addMethod("dropdowncheck", function(value, element) {
	var jpNumber_ = $("#jpNumber").val();
	if(jpNumber_=="") {
		changeValidationRule();
	}
	if(value=="" && jpNumber_=="") {
		return false;
	} else
		return true;
});	

function changeValidationRule(){
	var flag = false;
	
	var fromDate_ = $("#fromDate").val();
	var toDate_   = $("#toDate").val();
	
	var partnerName_ = $("#partnerName").val();
	var status_ = $("#status").val();
	if(partnerName_ != "") {
		$("#partnerName-error").html("");
	}
	if(fromDate_ == "" || toDate_ == ""){
		flag = true;
	}
	if(status_!=""){
		$("#status-error").html("");
	}
	$('#fromDate').rules('add', {
		required: flag,
		dateHigherThanToday : true
	});
	$('#toDate').rules('add', {
		required: flag,
		greaterThanEndDate:true,
		dateHigherThanToday:true
	});
	if(flag == true){
		$("#zeroJpMilesReport").validate().element('#fromDate');
		$("#zeroJpMilesReport").validate().element('#toDate');
	}
}
</script>

<!-- Methods -->
<script type="text/javascript">
var	datetext;
function getFormattedDate(date){
	var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
	return new Date(date.replace(pattern,'$3-$2-$1'));
}

var d = new Date(); // for now
var h = d.getHours();
h = (h < 10) ? ("0" + h) : h ;
var m = d.getMinutes();
m = (m < 10) ? ("0" + m) : m ;
var s = d.getSeconds();
s = (s < 10) ? ("0" + s) : s ;
var month = d.getMonth()+1;
var day = d.getDate();
var year = d.getFullYear();
datetext = day+"-"+month+"-"+year+"_"+h+":"+m+":"+s;

		
	$('.partner_info_table').DataTable( {
        "paging": true,
        "searching": false,
        "lengthChange": false,
        "info":false,
        "iDisplayLength" : 100,
        "order": [[ 0, "asc" ]],
        "dom": 'Blfrtip',
        columnDefs: [
             {type: 'non-empty-string', targets: 4} // define 'name' column as non-empty-string type
        ],
    	 "searching": false,
		 "buttons": [
			 { extend: 'csv',   text:'export to csv',  title:'ZeroJpmilesReport_'+datetext},
        ],
    });
	
$("#export_new").on("click",function(){
		
		console.log("-------------> entered <---------------");
		$("#zeroJpMilesReport").validate();
		console.log("entered");
		var startDate;
		var endDate;
		var jpNumber;
		var status;
		var validTypeRadio = "not defined";
		var allowExport = true;
		 
		startDate = $("#fromDate").val();
		endDate = $("#toDate").val();
		jpNumber = $("#jpNumber").val();
		startDate = $("#fromDate").val();
		status = $("#status").find(":selected").text();
		
		
		console.log("---------->startDate ",startDate);
		console.log("---------->endDate ",endDate);
		console.log("---------->jpNumber ",jpNumber);
		console.log("---------->startDate ",startDate);
		console.log("---------->validTypeRadio ",validTypeRadio);
		console.log("---------->status ",status);
		
		
		if(startDate.length == 0){
			console.log("invalid date");
			$("#zeroJpMilesReport").validate().element("#fromDate");
			allowExport = false;
		}
		if(endDate.length == 0){
			console.log("invalid todate");
			$("#zeroJpMilesReport").validate().element("#toDate");
			allowExport = false;
		}
		if(status == "-Select Status-"){
			console.log("invalid status");
			$("#zeroJpMilesReport").validate().element("#status");
			allowExport = false;
		}
		
		
		if(allowExport){
			
			/* $("#fromDate-error").html("");
			$("#toDate-error").html("");
			$("#status-error").html(""); */
			
			if(status == "All"){
				if( $("#validTypeRadio1").is(':checked')) {
					validTypeRadio = "valid";
					} 
					if($("#validTypeRadio2").is(':checked')) {
						validTypeRadio = "invalid";
					}  
			}
			
			
			
			
			/* $.ajax({
					 url: "${contextPath}/admin/exportAllBusinessCsv?fromDate="+startDate+"&toDate="+endDate+"&jpNumber="+jpNumber+
							 "&status="+status+"&validTypeRadio="+validTypeRadio+"&reportType=business",
					 type: "GET",
					 contentType : 'application/json; charset=utf-8',
					 dataType: "json",
					 success: function(response)
					 {
						 console.log("export date---:",response);
					 }
				
			
		}); */

			window.location.href = "${contextPath}/admin/exportAllzeroJpMilesReport?fromDate="+startDate+"&toDate="+endDate+"&jpNumber="+jpNumber+
			 						"&status="+status+"&validTypeRadio="+validTypeRadio+"&reportType=ZeroJpMilesReport_";
			
			
			
		}
		
	});
	
</script>


