<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<section id="transaction_report">
<form:form  action="transactionList" method="post" id="transaction-form">
<div id="breadcrumb">
<ul><li><strong>Transaction details</strong></li></ul>
JP Number :
<input type="text" name="inputMemberId" id="inputMemberId" maxlength="100">
<input type="submit" value="Search">
</div>
<c:if test="${! empty transactionList }">
<table border="1">
<tr>
<th>Transaction Date</th>
<th>partner</th>
<th>Category</th>
<th>Product</th>
<th>Transaction Amount</th> 
<th>No of JP Miles</th>
<th>Expected Date Of Credit</th>
<th>Date Credited</th>
<th>Status</th>
</tr>

<c:forEach var="transaction" items="${transactionList}">
<tr>
<td>${transaction.transactionDate }</td>
<td>${transaction.partnerName }</td>
<td>${transaction.partnerCategoryName }</td>
<td>${transaction.partnerProductName }</td>
<td>${transaction.amount }</td>
<td>${transaction.miles }</td>
<td>${transaction.expectedCreditDate }</td>
<td>${transaction.milesPostingDate }</td>
<td>${transaction.transactionStatus }</td>
</tr>
</c:forEach>
</table>
</c:if>
</form:form>
</section>


<script>
$(document).ready(function(){
	$("#transaction-form").validate({
		rules :{
	        "inputMemberId" : {
	            required : true
	        }
	    },
	    messages :{
	        "inputMemberId" : {
	            required : 'Enter JP Number'
	        }
	    }
	    });
	});

</script>
