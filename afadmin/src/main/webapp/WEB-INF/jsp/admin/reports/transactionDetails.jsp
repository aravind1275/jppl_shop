<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class='container'>
            
            <section class="insurance_management">
            <form:form  autocomplete="off" action="transactionDetailsList" method="post" id="transaction-form">
                <div id ="breadcrumb">
                    <ul>
                        <li>Home</li>
						<li>Transaction</li>
                    </ul>
                </div>  <!---breadcrumb--->
				<div class="section_heading">
					<h5>Transaction Details</h5> 
				</div><!--- section heading ---->
				<div class="row pad_top ">
                                    
                                    
                                    <div class="col-md-2">
						<label>Start Date</label>
<!--                                                <span class="mandatory_star">*</span>-->
						<div class="pick_date">
							<input type="text" class="form-control startDate" id="fromDate" name="fromDate" value="${fromDate}" readonly />
 							
						</div>
					</div>
                                                        
                                                        
                                               <div class="col-md-2">
						<label>End Date</label>
<!--                                                <span class="mandatory_star">*</span>-->
						<div class="pick_date">
							<input type="text" class="form-control endDate" id="toDate" name="toDate" value="${toDate}" readonly />
							
						</div>
					</div>         
                                                        
                                           
                                    
					<div class="col-md-4">
						<label>JP Number</label><span class="mandatory_star">*</span>
						<input type="text" name="inputMemberId" class="form-control" name="jpNumber" id="inputMemberId" maxlength="25" value="${inputMemberId}" >	
					</div> 
					
				</div> 
                                        
                                        <div class="row pad_top ">
                                            
<!--                                           <div class="col-md-4 search_button">
						<input id="search_btn" class="bg_g_button" type="submit" value="Search" />
					</div>-->
                                        
                                        
                                        <div class="col-md-2 report_button">
                                                        <input class="bg_w_button" type="button" id="search" onClick="$(this).closest('form').submit();" value="Search" /> 
                                                        <input class="bg_w_button" type="button" id="clear" value="Clear" />
                                                         
						</div>
                                        
                                         
                                            
                                        </div>
                                        
                                        
                                      
				<c:if test="${not empty message }">
			<div style="color:green;font-weight: bold;margin-left:15px;"  ><c:out value="${message}"></c:out></div> 
			</c:if>
				
				<c:if test="${! empty transactionDetailsList }">
<!-- 				<div id="transaction_id" class="transaction_table"> -->

		

<!--<a  href="/afadmin/admin/exportBusinessCsv?jpNumber=${inputMemberId}" class="bg_w_button" id="export">export to csv</a>-->
				<input type="button" class="bg_w_button_new_trac" id="export" value="Export to csv"> 
				
					<table class=" partner_info_table table-bordered " cellspacing="0" cellpadding="0" width="98%" id="transactionDate">
						<thead>
							<tr>
								<th>Transaction Date</th>
								<th class="no-sort">Partner</th>
								<th class="no-sort">Category</th>
								<th class="no-sort">Product</th>
								<th class="no-sort">Transaction Amount</th>
								<th class="no-sort">No. of JP Miles</th>
								<th class="no-sort">Expected Date of Credit</th>
								<th class="no-sort">Date Credited</th>
								<th class="no-sort">Status</th>
							</tr>
						</thead><!----thead--->
						<tbody>
						<c:forEach var="transaction" items="${transactionDetailsList}">
							<tr class="super_admin" data-id="1">
<!--								<td class="td_width">${transaction.transactionDate }</td>-->
                                                                <td class="td_width">${transaction.transactionDate }</td>
								<td class="td_width">${transaction.partnerName }</td>
								<td class="td_width">${transaction.partnerCategoryName }</td>
								<td class="td_width">${transaction.partnerProductName }</td>
								<td class="td_width">${transaction.amount }</td>
								<td class="td_width">${transaction.miles }</td>
								<td class="td_width">${transaction.expectedCreditDate }</td>
								<td class="td_width">${transaction.milesPostingDate }</td>
								<td class="td_width">${transaction.productStatus }</td>
							</tr> 
							</c:forEach>
						</tbody><!---tbody--->
					</table><!---table-->
<!-- 				</div> -->
				</c:if>
				</form:form>
            </section><!-----section---->
            
        </div><!-----container---->
<script>
$(document).ready(function(){	
        
        $('#transactionDate').DataTable({

    "ordering": true,
    columnDefs: [{
      orderable: false,
      
      targets: "no-sort"
    }],
    "bFilter": false,
    "bInfo":false,
    "bPaginate":false
            });        
  
        
	$("#transaction-form").validate({
		ignore:[],
	   	rules:{
	   		fromDate:{				
				dateHigherThanToday:true
			},
			toDate:{				
				greaterThanEndDate:true,
				dateHigherThanToday:true
			},
	   		inputMemberId:{
	            required : true,
	            rangelength:[1,25]
	        }
	    },
	    messages :{
	    	fromDate:{
				required: "Start date cannot be empty",
				dateHigherThanToday:"Start date must be less than or equal End date "
			},
			toDate:{
				required: "End date cannot be empty",
				greaterThanEndDate:"End date must be greater than or equal to Start date ",
				dateHigherThanToday:"Start date must be greater than or equal End date "
			},
	    	inputMemberId: {
	            required : "Enter JP Number",
	            rangelength:"Maximum 25 characters permitted"
	        }
	    },
	    errorElement: "div",
	    errorPlacement: function(error, element) {
			if ($(element).hasClass("form-control startDate")) {
				error.insertAfter($(element).closest("div"));
			}
			else if ($(element).hasClass("form-control endDate")){
				error.insertAfter($(element).closest("div"));
			}
			else {
				error.insertAfter(element);
			}
		},
	     submitHandler: function (form) {
				form.submit();
			},	    
	});
	
	function getFormattedDate(date){
		var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
		return new Date(date.replace(pattern,'$3-$2-$1'));
	}
	
	$.validator.addMethod("dateHigherThanToday", function(value, element) {
		var startDate = getFormattedDate($('#fromDate').val());
		var endDate = getFormattedDate($('#toDate').val());
		
		if(endDate!='Invalid Date'){
			if (startDate <= endDate)
				return true;
			
			if (startDate >= endDate)
				return false;
		}
		else{
			return true;
		}
	});
	
	$.validator.addMethod("greaterThanEndDate", function(value, element) {
		var startDate = getFormattedDate($('#fromDate').val());
		var endDate = getFormattedDate($('#toDate').val());
		
		if(endDate!='Invalid Date'){
			if (endDate >= startDate)
				return true;
			
			if (endDate <= startDate)
				return false;
		}
		else{
			return true;
		}
	});
	
        $("#clear").on('click',function(){
            $("#fromDate").val('');
            $("#toDate").val('');
            $("#inputMemberId").val('');
            $("#export").hide();
            $("#fromDate-error").hide();
            $("#toDate-error").hide();            
        });              
});

$("#export").on("click",function(){	
	console.log("-------------> entered <---------------");
	$("#analyticsReport").validate();
	console.log("entered");
	var startDate;
	var endDate;
	var jpNumber;
	var status;
	var validTypeRadio = "not defined";
	var allowExport = true;
	 
	startDate = $("#fromDate").val();
	endDate = $("#toDate").val();
	jpNumber = $("#inputMemberId").val();
	
	console.log("---------->startDate ",startDate);
	console.log("---------->endDate ",endDate);
	console.log("---------->jpNumber ",jpNumber);
	
	if(allowExport){		
		window.location.href = "${contextPath}/admin/exportTransactionCsv?fromDate="+startDate+"&toDate="+endDate+"&inputMemberId="+jpNumber;		
	}	
});
</script>