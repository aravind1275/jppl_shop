<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script>
	function changeURL(){
                	history.pushState({}, '',"${contextPath}/admin/invalidReport");
                	
        }
</script>

	<div class='container'>
		<section class="affliates_management">
			<div id="breadcrumb">
				<ul>
					<li>Home</li>
					<li>Reports</li>
					<li>Invalid Report</li>
				</ul>
			</div>
			<!---breadcrumb--->
			<div class="section_heading">
				<c:if test="${fn:length(message) gt 0}"><script>changeURL();</script><div class="alertMessage" style="color:green;font-weight: bold;"  ><c:out value="${message}"></c:out></div> </c:if>
				<c:if test="${fn:length(errormessage) gt 0}"><script>changeURL();</script><div class="alertMessage" style="color:red;font-weight: bold;"  ><c:out value="${errormessage}"></c:out></div> </c:if>
				<c:if test="${fn:length(orderAndTransError) gt 0}"><script>changeURL();</script><div class="alertMessage" style="color:red;font-weight: bold;"  ><c:out value="${orderAndTransError}"></c:out></div>
				<script>
	       		setTimeout(function(){ 
	       			document.location.reload(true);
	       		}, 2000);</script>
				 </c:if>
				<h5>Invalid Report</h5>
			</div>
			<div class="management">
			<ul class="nav nav-tabs">
              <li id="invalidList"  class="active"><a data-toggle="tab" href="#sectionA">Report Of Invalid.</a></li>
              <li id="invalidFileUpload" ><a data-toggle="tab" href="#sectionB">File uploading Of Invalid</a></li>
           </ul>
    <div class="tab-content">
    <div id="sectionA" class="tab-pane fade in active">		
			 <form:form autocomplete="off" id="invalidReport" class="reports">
			<div class="main_section bg-b-w ">
				<div class="row pad_top">
					<div class="col-md-2">
						<label>Start Date</label><span class="mandatory_star">*</span>
						<div class="pick_date">
							<input type="text" class="form-control startDate" id="fromDate" name="fromDate" value="${fromDate}" readonly />
<!-- 							<p id="log" style="display:none;color:red;"></p> -->
						</div>
					</div>
					
					<div class="col-md-2">
						<label>End Date</label><span class="mandatory_star">*</span>
						<div class="pick_date">
							<input type="text" class="form-control endDate" id="toDate" name="toDate" value="${toDate}" readonly />
<!-- 							<p id="log" style="display:none;color:red;"></p> -->
						</div>
					</div>
					
					<div class="col-md-2">
						<label>Filter By</label><span class="mandatory_star">*</span>
						<fieldset class="infield">
							<select name="partnerName" id="partnerName" class="select_option">
								<option value="">-Select Partner-</option>
								<c:forEach items="${partnerList}" var="partner">
									<c:choose>
										<c:when test="${partner.prtNo eq prtNo}">
											<option value="${partner.prtNo}" selected >${partner.partnerName}</option>
										</c:when>
										<c:otherwise>
											<option value="${partner.prtNo}" >${partner.partnerName}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</fieldset>
					</div>
					
					<div class="col-md-2">
						<label>JP Number</label><span class="mandatory_star"></span>
						<input type="text" class="form-control" name="jpNumber" id="jpNumber" placeholder="Enter JP No" value="${jpNumber}" />
					</div>
					
					<br>
					<div class="col-md-2 report_button">
							<input class="bg_w_button" type="button" onClick="$(this).closest('form').submit();" value="Search" /> <input class="bg_w_button" type="button" id="clear" value="Clear" />
						</div>
				</div>
				<!--- row ------>
			</div>
			<!---- date picker section ---->
			<c:if test="${! empty invalidReportList }">
				<div class="report_table">
				<!-- <input class="bg_w_button" type="button" id="export" value="EXPORT" />  -->
				<a  href="/afadmin/admin/exportInvalidCsv?fromDate=${fromDate}&toDate=${toDate}&jpNumber=${jpNumber}&partnerName=${prtNo}" class="bg_w_button" id="export">export to csv</a>
					<!-- <div class="expot_btn">
						<label>Export</label> <input class="bg_w_button" type="button" value="XLS" />
					</div> -->
					<table id="business_report" class="display" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>JP Number</th>
								<th>Reference Number</th>
								<th>Affiliate Order Number</th>
								<th>Order Date</th>
								<th>Merchant</th>
								<th>Product name</th>
								<th>Partner Category</th>
								<th>Partner ProductId</th>
								<th>message</th>
								<th>Traking</th>
							</tr>
						</thead>
						<!--- thead ---->
						<tbody>
						</tbody>
						<!--- tbody ---->
					</table>
					<!--- table ---->
				</div>
			</c:if>
			<!--- report table ---->
			</form:form>
			</div><!-- section A -->
			
			<div id="sectionB" class="tab-pane fade submenu" >
			 <form id="uploadCSVForm" name="uploadCSVForm" method="POST" action="uploadInvalidFile" enctype="multipart/form-data">         
			<div class="row pad top">
                                <div class="col-md-3 upload_keyword">
                                <br>
                     
									<label>Upload KeyWord File</label><span class="mandatory_star">*</span>
									<div class="fileUpload btn ">						
										<input name="uploadFile2" id="uploadFile2"  class="upload_file" placeholder=".csv file only" disabled="disabled" />
										<span class="bg_w_button" style="position:relative;top:5px;">Browse</span>									
										<input type="file" name="uploadCSVFile" id="uploadCSVFile" class="upload uploadfile_button"    />
										<input type="hidden" id="filename" name="filename"  />
									<br>	
									</div>
									</div> 
									<div class="col-md-1">
									<p id="log" style="display:none;color:red;"></p>
	                                 <div class="fileUpload btn "  style="position:relative;top:16px;right:12px">
	                                	<!-- <input class="bg_g_button" type="submit"  id="UploadFile" value="Upload" /> -->
 										<button type="submit" class="bg_g_button" id="UploadFile" >Upload</button>
 										
									</div>
									
								</div>
								</div>
								<!-- <br><br>
								<label><span class="mandatory_star">*</span>NOTE:
                                Setting the Email & SMS status to "1" in the file and uploading it will trigger the transaction email & SMS to that JPMember.</label>
				 -->
						</form>	
                         </div><!--- row --->
			
			</div>
			</div>	<!-- management -->
		</section>  <!-- section management ends -->
	</div>
	

<!-- variables          -->
<script type="text/javascript">
</script>

<!-- Form Validation -->
<script type="text/javascript">
$(document).ready(function () {
	
	$("#invalidReport").validate({
		onkeyup: true,
		ignore:[],
		rules: {	
			fromDate:{
				required: true,
				dateHigherThanToday:true
			},
			toDate:{
				required: true,
				greaterThanEndDate:true,
				dateHigherThanToday:true
			},
			partnerName:{
				required: true,
			},
			
			
		},
		messages: {
			fromDate:{
				required: "Start date cannot be empty",
				dateHigherThanToday:"Start date must be less than or equal End date "
			},
			toDate:{
				required: "End date cannot be empty",
				greaterThanEndDate:"End date must be greater than or equal to Start date ",
				dateHigherThanToday:"Start date must be greater than or equal End date "
			},
			partnerName:{
				dropdowncheck: "Please select a Partner"
			},
			
		},
		errorElement: "div",
		errorPlacement: function(error, element) {
			if ($(element).hasClass("form-control startDate")) {
				error.insertAfter($(element).closest("div"));
			}
			else if ($(element).hasClass("form-control endDate")){
				error.insertAfter($(element).closest("div"));
			}
			else {
				error.insertAfter(element);
			}
		},
		submitHandler: function (form) {
			form.submit();
		}
	});
		
		
	$("#uploadCSVForm").validate({
		ignore:[],	
		rules: {
			uploadCSVFile:{
				required: true,
				extension: "csv"
			}
			},
		messages: {
			
			uploadCSVFile:{
				required: "Please upload csv file",
				extension: "File format shoud be .csv"
			}
		},
			errorElement: "div",
			errorPlacement: function(error, element) {
			if ($(element).hasClass("uploadBtn2")){
		    	error.insertAfter($(element).closest("div"));
			}
			else {
		    	error.insertAfter(element);
			}
			},
		submitHandler: function (form) {
			form.submit();
		},
	});
	
	
	//UploadFile
	$('#UploadFile').on('change', function(){
		$('#UploadFile').val('');
		
	});

	$('#uploadCSVFile').on('change', function() {
		var filename = $(this).val();
		var	datetext;
		var d = new Date(); // for now
		var h = d.getHours();
		h = (h < 10) ? ("0" + h) : h ;
		var m = d.getMinutes();
		m = (m < 10) ? ("0" + m) : m ;
		var s = d.getSeconds();
		s = (s < 10) ? ("0" + s) : s ;
		var month = d.getMonth()+1;
		var day = d.getDate();
		var year = d.getFullYear();
		datetext = day+"-"+month+"-"+year+"_"+h+":"+m+":"+s;
		
		
		
		$('#uploadFile2').val($(this).val());
		$('#filename').val("Zero Jp miles-"+datetext);
		/* $('#filename').val($(this).val()); */
		$("#uploadCSVForm").validate().element('#uploadCSVFile');

	});
		
});
</script>

<!-- Events -->
<script type="text/javascript">




$("#clear").on('click', function () {
	$("#fromDate").val('');
	$("#toDate").val('');
	$("#jpNumber").val('');
	$('#partnerName').val('');
	$('#partnerName').selectmenu("refresh");
	
	$("#invalidReport").data('validator').resetForm();
	//$("#invalidReport")[0].reset();
});

$( "#fromDate" ).on('change', function() {
	$("#invalidReport").validate().element("#fromDate");
	var endDate = getFormattedDate($('#toDate').val());
	
	if(endDate!='Invalid Date'){
		$("#invalidReport").validate().element("#toDate");
	}
});

$( "#toDate" ).on('change', function() {
	$("#invalidReport").validate().element("#fromDate");
	$("#invalidReport").validate().element("#toDate");
});

$('#partnerName').on('selectmenuchange', function () {
	$("#invalidReport").validate().element('#partnerName');
});

$( "#jpNumber" ).on('change', function() {
	var jpNumber_ = $("#jpNumber").val();
	if(jpNumber_ != "") {
		if($("#fromDate").hasClass('error')) {
			$("#fromDate").removeClass('error');
			$("#toDate").removeClass('error');
			$("#fromDate-error").html("");
			$("#toDate-error").html("");
			
			$('#fromDate').rules('add', {
				required: false,
				dateHigherThanToday : true
			});
			$('#toDate').rules('add', {
				required: false,
				greaterThanEndDate:true,
				dateHigherThanToday:true
			});
			$('#partnerName').rules('add', {
				dropdowncheck: false
			});
			
			$("#invalidReport").validate().element('#fromDate');
			$("#invalidReport").validate().element('#toDate');
			$("#invalidReport").validate().element('#partnerName');
		}
		if($("#partnerName").hasClass('error')) {
			$("#partnerName").removeClass('error');
			$("#partnerName-error").html("");
		}
		
	}
});
</script>

<!-- Validation Methods -->
<script type="text/javascript">
$.validator.addMethod("dateHigherThanToday", function(value, element) {
	var startDate = getFormattedDate($('#fromDate').val());
	var endDate = getFormattedDate($('#toDate').val());
	
	if(endDate!='Invalid Date'){
		if (startDate <= endDate)
			return true;
		
		if (startDate >= endDate)
			return false;
	}
	else{
		return true;
	}
});

$.validator.addMethod("greaterThanEndDate", function(value, element) {
	var startDate = getFormattedDate($('#fromDate').val());
	var endDate = getFormattedDate($('#toDate').val());
	
	if(endDate!='Invalid Date'){
		if (endDate >= startDate)
			return true;
		
		if (endDate <= startDate)
			return false;
	}
	else{
		return true;
	}
});

$.validator.addMethod("dropdowncheck", function(value, element) {
	var jpNumber_ = $("#jpNumber").val();
	if(jpNumber_=="") {
		changeValidationRule();
	}
	if(value=="" && jpNumber_=="") {
		return false;
	} else
		return true;
});	

function changeValidationRule(){
	var flag = false;
	
	var fromDate_ = $("#fromDate").val();
	var toDate_   = $("#toDate").val();
	
	var partnerName_ = $("#partnerName").val();
	var status_ = $("#status").val();
	if(partnerName_ != "") {
		$("#partnerName-error").html("");
	}
	if(fromDate_ == "" || toDate_ == ""){
		flag = true;
	}
	/* if(status_!=""){
		$("#status-error").html("");
	} */
	$('#fromDate').rules('add', {
		required: flag,
		dateHigherThanToday : true
	});
	$('#toDate').rules('add', {
		required: flag,
		greaterThanEndDate:true,
		dateHigherThanToday:true
	});
	if(flag == true){
		$("#invalidReport").validate().element('#fromDate');
		$("#invalidReport").validate().element('#toDate');
	}
}
</script>

<!-- Methods -->
<script type="text/javascript">
var	datetext;
function getFormattedDate(date){
	var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
	return new Date(date.replace(pattern,'$3-$2-$1'));
}

var d = new Date(); // for now
var h = d.getHours();
h = (h < 10) ? ("0" + h) : h ;
var m = d.getMinutes();
m = (m < 10) ? ("0" + m) : m ;
var s = d.getSeconds();
s = (s < 10) ? ("0" + s) : s ;
var month = d.getMonth()+1;
var day = d.getDate();
var year = d.getFullYear();
datetext = day+"-"+month+"-"+year+"_"+h+":"+m+":"+s;
var fromDate=$('#fromDate').val();
var toDate=$('#toDate').val();
var jpNumber=$('#jpNumber').val();
var partnerName=$('#partnerName').val();
/* var status= $('#status').val();
var validTypeRadio= $("#hiddenVal").val(); */
	$(document).ready(function() {
 	var table = $('.display').removeAttr('width').DataTable({
	dom: 'Blfrtip',
	scrollX:        true,
	"bPaginate": true,
	 "processing": true,
     "serverSide": true,
	//scrollCollapse: true,
	"ordering": false,
	columnDefs: [
	{ width: '243', targets: '_all' }
	],
	buttons: [
                 { extend: 'csv',   text:'export to csv',  title:'Invalid_Report_'+datetext},
                 { extend: 'excel', text:'export to excel',title:'Invalid_Report_'+datetext},
              ],
	fixedColumns: false,
	/* "infoCallback": function( settings, start, end, max, total, pre ) {
        return "showing " +start +" to "+(start+Number($('#length').val()-1)) +" of " +$('#count').val();
    }, */
    "ajax":  "${contextPath}/admin/serverPaginationInvalid?fromDate="+fromDate+"&toDate="+toDate+"&jpNumber="+jpNumber+"&partnerName="+
    partnerName+"&reportType=invalid",
    "columns": [
        { "data": "jpNumber" },
        { "data": "transactionId" },
        { "data": "partnerItemId" },
        { "data": "formatedTransactionDate" },
        { "data": "partnerName" },
        { "data": "productName" },
        { "data": "partnerCategoryName" },
        { "data": "partnerProductId" },
        { "data": "message" },
        { "data": "trackingId" },
      
      
        
    ]  
	});

	
		
	}); 	
	
</script>

	
