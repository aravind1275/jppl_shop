<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class='container'>
  <section class="affliates_management">
  <div id ="breadcrumb">
                    <ul>
                        <li>Home</li>
                        <li>Reports</li>
						<li>User Activity Log</li>
                    </ul>
                </div>  <!---breadcrumb--->
                
                <div class="section_heading">
					<c:if test="${fn:length(message) gt 0}"><div style="color:green;font-weight: bold;"  ><c:out value="${message}"></c:out></div> </c:if>
					<h5>User Activity Log</h5> 
				</div>
                 <form:form autocomplete="off" id="userActivityLog" class="reports">
                 <div class="main_section bg-b-w ">
					<div class="row pad_top">
					
					<div class="col-md-3">
						<label>Start Date</label>
						<div class="pick_date">
							<input type="text" class="form-control startDate" id="fromDate" name="userActFromDate" value="${userActFromDate}" readonly />
						</div>
					</div>
					
					<div class="col-md-3">
						<label>End Date</label>
						<div class="pick_date">
						<input type="text" class="form-control endDate" id="toDate" name="userActTodate" value="${userActTodate}" readonly />
						<p id="log" style="display:none;color:red;"></p>
						</div>
					</div>
					<br>
					<!-- <div class="col-md-4 report_button">
		             <input class="bg_w_button" type="button" id="export" value="Export" />
					</div> -->
				    </div>
				    <div class="row pad_top">
				    <div class="col-md-2 report_button">
							<input class="bg_w_button" id="checkin" type="button"  value="Search" />&nbsp;&nbsp;&nbsp;&nbsp; <input class="bg_w_button" type="button" id="clear" value="Clear" />
					</div><!-- onClick="$(this).closest('form').submit();" -->
					</div>
					</div>    
					<!---- date picker section ---->
					
					
					<c:if test="${! empty userActivityLogList }">
 						
 						<div class="report_table">
 						<!-- <div class="user_activity_policy_table action_border "> -->
 						<table  id="usActLog"   class="display" cellspacing="0" width="100%">
 						 <thead>
							<tr>
								<th class="name">Sl No.</th>
								<th>Activity Module</th>
								<th>Activity Details</th>
								<th>Old Field Values</th>
								<th>New Field Values</th>
								<th>User Id</th>
								<th>Date Of Change</th>
								<th>IP Value</th>
							</tr>
						 </thead>
						 
						 <tbody>
						 	<c:forEach items="${userActivityLogList}" var="userActLogList" varStatus="i">
						 		<tr>
						 		<td class="slno_width">${i.count}</td>
						 		<td>${userActLogList.userActModule}</td>
						 		<td>${userActLogList.userActivity}</td>
						 		<td>${userActLogList.userOldFieldValue}</td>
						 		<td>${userActLogList.userNewFieldValue}</td>
						 		<td>${userActLogList.userActCreatedBy}</td>
						 		<td>${userActLogList.userActCreatedDateTime}</td>
						 		<td>${userActLogList.userActIpVal}</td>
						 		</tr>
						 	</c:forEach>
						 </tbody>
 						</table>
 						</div>
					</c:if>
					
                 </form:form>
  </section>
</div>    


<script type="text/javascript">
$(document).ready(function () {
	var	datetext;
	function getFormattedDate(date){
		var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
		return new Date(date.replace(pattern,'$3-$2-$1'));
	}

	var d = new Date(); // for now
	var h = d.getHours();
	h = (h < 10) ? ("0" + h) : h ;
	var m = d.getMinutes();
	m = (m < 10) ? ("0" + m) : m ;
	var s = d.getSeconds();
	s = (s < 10) ? ("0" + s) : s ;
	var month = d.getMonth()+1;
	var day = d.getDate();
	var year = d.getFullYear();
	datetext = day+"-"+month+"-"+year+"_"+h+":"+m+":"+s;
	 
	 $('.display').DataTable({
		 dom: 'Blfrtip',
		 "scrollX": true,
		    "scrollCollapse": true,
		 'bSort':false,
		 columnDefs: [
		 {  'targets': 3 , 'width': '300px'},
		 ],
		 fixedColumns: true,
		 buttons: [
		               { extend: 'csv',   text:'export to csv',  title:'UserActivity_Log_'+datetext},
		               { extend: 'excel', text:'export to excel',title:'UserActivityLog_'+datetext},
		            ]

		 });
	 
	
});
</script>




<script type="text/javascript">

$("#clear").on('click', function () {
	$("#fromDate").val('');
	$("#toDate").val('');
});

$('#checkin').click(function(e) {
	
	var sts=$('#fromDate').val();
	   var end=$('#toDate').val();
	   var b = end.split("/") ;
	   var a = sts.split("/") ;
	   var i=0;
	  
    /* var startDate1 = new Date( a[2], (a[1] - 1), a[0] ) ;
    var endDate1 = new Date( b[2], (b[1] - 1), b[0] ) ;
    startDate1.setHours(0,0,0,0);
    endDate1.setHours(0,0,0,0);
   / * */
   end =b[2]+b[1]+b[0];
   sts = a[2]+a[1]+a[0];
   end = Number(end);
   sts = Number(sts);
   i=1;
   if(i==1){
      if(end<sts){
    	  if(end!=='' && sts!==''){
    	 
     	 $("#log").html("End Date should be Greater than Start Date");
			 $("#log").show();
			 $('#log').fadeIn().delay(4000).fadeOut();
     	e.preventDefault();
     }
      } 
      
      
     if(end==''&& sts==''){
    	 $("#log").html("Both dates cannot be left blank");
		 $("#log").show();
		 $('#log').fadeIn().delay(4000).fadeOut();
 	e.preventDefault();
     }
    if(end!==''&& sts!==''){
    	if(end>sts){
    		 $(this).closest('form').submit();
    	}
    	if(end==sts){
    		 $(this).closest('form').submit();
    	} 
    }
   if(end!=='' && sts==''){
	   $(this).closest('form').submit();
   }
    
   if(end=='' && sts!==''){
	   $(this).closest('form').submit();
   }
   }
});

</script>

<script type="text/javascript">

/* var	datetext;
function getFormattedDate(date){
	var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
	return new Date(date.replace(pattern,'$3-$2-$1'));
}

var d = new Date(); // for now
var h = d.getHours();
h = (h < 10) ? ("0" + h) : h ;
var m = d.getMinutes();
m = (m < 10) ? ("0" + m) : m ;
var s = d.getSeconds();
s = (s < 10) ? ("0" + s) : s ;
var month = d.getMonth()+1;
var day = d.getDate();
var year = d.getFullYear();
datetext = day+"-"+month+"-"+year+"_"+h+":"+m+":"+s;
 
 $('.display').DataTable({
	 dom: 'Blfrtip',
	 "scrollX": true,
	    "scrollCollapse": true,
	 'bSort':false,
	 columnDefs: [
	 {  'targets': 3 , 'width': '300px'},
	 ],
	 fixedColumns: true,
	 buttons: [
	               { extend: 'csv',   text:'export to csv',  title:'UserActivity_Log_'+datetext},
	               { extend: 'excel', text:'export to excel',title:'UserActivityLog_'+datetext},
	            ]

	 });
  */
	
</script>













