<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script>
	function changeURL(){
			history.pushState({}, '',"${contextPath}/admin/jpMilesStatusUpdate");
    }
</script>

<div id="myModal1" class="modal fade" role="dialog">
	<form:form autocomplete="off" id="updatejpMilesStatus" commandName="">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" id="cancelbtnD" class="closeOff"
						data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Edit JpMiles Status Details</h4>
				</div>
				<!----modal-header--->
				<div class="modal-body1">
					<div class="row pad_top">
						<div class="col-md-6 user_details">
							<label>Transaction Id</label>
						<input type="text" class="form-control " name="edit_transactionId" maxlength="100" id="edit_transactionId" readonly="readonly">
						</div>
						
						<div class="col-md-6 user_details">
							<label>Order Id</label>
						<input type="text" class="form-control " name="edit_partnerItemId" maxlength="100" id="edit_partnerItemId" readonly="readonly">
						</div>

					</div>
					<div class="row pad_top">
						<div class="col-md-6 user_details">
							<label>Jp miles Credit</label>
						<input type="text" class="form-control " name="edit_jpmilesToCredit" maxlength="250" id="edit_jpmilesToCredit">
						</div>
						
						<div class="col-md-6 user_details">
							<label>JPmiles Cancel</label>
						<input type="text" class="form-control " name="edit_jpmilesCancel" maxlength="250" id="edit_jpmilesCancel">
						</div>
						<div class="col-md-6 user_details">
						<label>Sms Status</label>
							<select name="edit_smsStatus" id="edit_smsStatus" class="select_option">
											<option value="">---Select---</option>
											<option value="1">send</option>
  											<option value="0">Not Sent </option>
 										</select>
 										</div>
					</div>
					<div class="row pad_top">
						<div class="col-md-6 user_details">
							<label>JPMiles Status</label>
							<select name="edit_jpmiles_Status" id="edit_jpmiles_Status" class="select_option">
											<option value="1">Requested</option>
  											<option value="2">Sent For Credit</option>
  											<option value="3">Credited</option>
  											<option value="4">Declined</option>
 										</select>
						</div>
					</div>
				
				  </div>
				<div class="row btn_save align_right">
					<input class="bg_g_button" type="button" value="jpMilesCredit Update" id="jpMilesCredit"/>
				</div>
				<!-------------button----------->
			</div>
		</div>
	</form:form>
</div> 




<div class='container'>
		<section class="affliates_management">
			<div id="breadcrumb">
				<ul>
					<li>Home</li>
					<li>Reports</li>
					<li>JPMiles Status Update</li>
				</ul>
			</div>
			<!---breadcrumb--->
			<c:if test="${fn:length(message) gt 0}"><script>changeURL();</script><div class="alertMessage" style="color:green;font-weight: bold;"  ><c:out value="${message}"></c:out></div> </c:if>
	  			<c:if test="${fn:length(errormessage) gt 0}"><script>changeURL();</script><div class="alertMessage" style="color:red;font-weight: bold;"  ><c:out value="${errormessage}"></c:out></div> </c:if>
			<div class="section_heading">
				<h5>JPMiles Status Update</h5>
			</div>
			 <form:form autocomplete="off" id="jpMilesStatusUpdate" class="reports">
			<div class="main_section bg-b-w ">
				<div class="row pad_top">
					<div class="col-md-2">
						<label>Start Date</label><span class="mandatory_star">*</span>
						<div class="pick_date">
							<input type="text" class="form-control startDate" id="fromDate" name="fromDate" value="${fromDate}" readonly />
						</div>
					</div>
					
					<div class="col-md-2">
						<label>End Date</label><span class="mandatory_star">*</span>
						<div class="pick_date">
							<input type="text" class="form-control endDate" id="toDate" name="toDate" value="${toDate}" readonly />
						</div>
					</div>
					
					<div class="col-md-2">
						<label>Filter By</label><span class="mandatory_star">*</span>
						<fieldset class="infield">
							<select name="partnerName" id="partnerName" class="select_option">
								<option value="">-Select Partner-</option>
								<c:forEach items="${partnerList}" var="partner">
									<c:choose>
										<c:when test="${partner.prtNo eq prtNo}">
											<option value="${partner.prtNo}" selected >${partner.partnerName}</option>
										</c:when>
										<c:otherwise>
											<option value="${partner.prtNo}" >${partner.partnerName}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</fieldset>
					</div>
					
					<div class="col-md-2">
						<label>JP Number</label><span class="mandatory_star"></span>
						<input type="text" class="form-control" name="jpNumber" id="jpNumber" placeholder="Enter JP No" value="${jpNumber}" />
					</div>
					<%-- <div class="col-md-2">
						<label>Reference Number</label><span class="mandatory_star"></span>
						<input type="text" class="form-control" name="referenceNumber" id="referenceNumber" placeholder="Enter Reference No" value="${referenceNumber}" />
					</div> --%>
					
					<!--new field status starts  -->
					<div class="col-md-2">
						<label>Status</label><span class="mandatory_star">*</span>
						<fieldset class="infield">
							<select name="status" id="status" class="select_option">
								 <option value="">-Select Status-</option>
								<c:forEach items="${statusList}" var="stat">
									<c:choose>
										<c:when test="${stat eq status}">
											<option value="${stat}" selected >${stat}</option>
										</c:when>
										<c:otherwise>
											<option value="${stat}" >${stat}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</fieldset>
					</div>
					<!--new field status ends  -->
					<div class="row pad_top export_btn">
					<!-- radio for valid invalid -->
					<div id="radioValid">
					<input type="radio" class="css-checkbox" name="validTypeRadio" id ="validTypeRadio1" value="valid" checked="checked">
                       <label class="css-label radGroup2" for="validTypeRadio1">Valid</label>
                    <input type="radio" class="css-checkbox" name="validTypeRadio" id ="validTypeRadio2" value="invalid" >
                       <label class="css-label radGroup2" for="validTypeRadio2">Invalid</label>
                       </div>
                       <input type="hidden" id="hiddenVal"  value="${hiddenVal}" />
                       <br>
					</div>
					<br>
					<div class="col-md-2 report_button">
							<input class="bg_w_button" type="button" onClick="$(this).closest('form').submit();" value="Search" /> 
							<input class="bg_w_button" type="button" id="clear" value="Clear" />
						</div>
						<!-- <div class="col-md-2 report_button1">
							<input class="bg_w_button" type="button" id="jpMilesCredit" value="Jppl Credit" /> 
						</div> -->
				</div>
				<!--- row ------>
			</div>
			<!---- date picker section ---->
			<c:if test="${! empty jpMilesStatusUpdateList }">
				<div class="report_table">
					<table id="jpMilesStatusUpdate" class="display" cellspacing="0" width="100%">
						<thead>
							<tr>
							    <th>Check</th>
								<th>JP Number</th>
								<th>Reference Number</th>
								<th>Affiliate Order Number</th>
								<th>Order Date</th>
								<th>Merchant</th>
								<th>Partner Category</th>
								<th>JPPL Category</th>
								<th>Product</th>
								<th>Transaction Amount / price</th>
								<th>Partner Commission Rate</th>
								<th>Partner Commission Amount</th>
								<th>JPPL commission amount</th>
								<th>% of JPPL Commission</th>
								<th>% of Commission given to member</th>
								<th>Tax%</th>
								<th>No Of JPMiles</th>
								<th>Expected Date Of Credit</th>
								<th>JPMiles Credited</th>
								<th>JPMiles Decline</th>
								<th>Order Status</th>
								<th>Source</th>
								<th>Medium</th>
								<th>Campaign</th>
								<th>Member/Guest</th>
								<th>Transaction via</th>
								<th>JPMiles Status</th>
								<th>SMS Status</th>
								
							</tr>
						</thead>
						<!--- thead ---->
						<tbody>

						</tbody>
						<!--- tbody ---->
					</table>
					<!--- table ---->
				</div>
			</c:if>
			<!--- report table ---->
			</form:form>
		</section>
	</div>
	
<!-- variables          -->
<script type="text/javascript">
</script>

<!-- Form Validation -->
<script type="text/javascript">
$(document).ready(function () {
	
	 $("#radioValid").hide();
		if($("#status").val() == "All"){
			 $("#radioValid").show();
	 if($("#hiddenVal").val() == "valid"){
		 $("#validTypeRadio1").prop("checked", true);
	 }else if($("#hiddenVal").val() == "invalid"){
		 $("#validTypeRadio2").prop("checked", true);
	 }
		}
	$("#jpMilesStatusUpdate").validate({
		onkeyup: true,
		ignore:[],
		rules: {	
			fromDate:{
				required: true,
				dateHigherThanToday:true
			},
			toDate:{
				required: true,
				greaterThanEndDate:true,
				dateHigherThanToday:true
			},
			partnerName:{
				required: true,
			},
			status:{
				required: true,
			}
			
		},
		messages: {
			fromDate:{
				required: "Start date cannot be empty",
				dateHigherThanToday:"Start date must be less than or equal End date "
			},
			toDate:{
				required: "End date cannot be empty",
				greaterThanEndDate:"End date must be greater than or equal to Start date ",
				dateHigherThanToday:"Start date must be greater than or equal End date "
			},
			partnerName:{
				dropdowncheck: "Please select a Partner"
			},
			status:{
				dropdowncheck: "Please select a status"
			}
		},
		errorElement: "div",
		errorPlacement: function(error, element) {
			if ($(element).hasClass("form-control startDate")) {
				error.insertAfter($(element).closest("div"));
			}
			else if ($(element).hasClass("form-control endDate")){
				error.insertAfter($(element).closest("div"));
			}
			else {
				error.insertAfter(element);
			}
		},
		submitHandler: function (form) {
			form.submit();
		}
	});
});
</script>	
<script type="text/javascript">
$("#clear").on('click', function () {
	$("#fromDate").val('');
	$("#toDate").val('');
	$("#jpNumber").val('');
	$("#referenceNumber").val('')
	$('#partnerName').val('');
	$('#partnerName').selectmenu("refresh");
	$('#status').val('');
	$("#validTypeRadio1").val('');
	$("#validTypeRadio2").val('');
	$("#validTypeRadio2").prop("unchecked", true);
	$("#validTypeRadio1").prop("unchecked", true);
    $("#radioValid").hide();
	$('#status').selectmenu("refresh");
	
	$("#jpMilesStatusUpdate").data('validator').resetForm();
});

$( "#fromDate" ).on('change', function() {
	$("#jpMilesStatusUpdate").validate().element("#fromDate");
	var endDate = getFormattedDate($('#toDate').val());
	
	if(endDate!='Invalid Date'){
		$("#jpMilesStatusUpdate").validate().element("#toDate");
	}
});

$( "#toDate" ).on('change', function() {
	$("#jpMilesStatusUpdate").validate().element("#fromDate");
	$("#").validate().element("#toDate");
});

$('#partnerName').on('selectmenuchange', function () {
	$("#jpMilesStatusUpdate").validate().element('#partnerName');
});
$('#status').on('selectmenuchange', function () {
	$("#jpMilesStatusUpdate").validate().element('#status');
});

$( "#jpNumber" ).on('change', function() {
	var jpNumber_ = $("#jpNumber").val();
	if(jpNumber_ != "") {
		if($("#fromDate").hasClass('error')) {
			$("#fromDate").removeClass('error');
			$("#toDate").removeClass('error');
			$("#fromDate-error").html("");
			$("#toDate-error").html("");
			
			$('#fromDate').rules('add', {
				required: false,
				dateHigherThanToday : true
			});
			$('#toDate').rules('add', {
				required: false,
				greaterThanEndDate:true,
				dateHigherThanToday:true
			});
			$('#partnerName').rules('add', {
				dropdowncheck: false
			});
			$('#status').rules('add', {
				dropdowncheck: false
			});
			$("#jpMilesStatusUpdate").validate().element('#fromDate');
			$("#jpMilesStatusUpdate").validate().element('#toDate');
			$("#jpMilesStatusUpdate").validate().element('#partnerName');
			$("#jpMilesStatusUpdate").validate().element('#status');
		}
		if($("#partnerName").hasClass('error')) {
			$("#partnerName").removeClass('error');
			$("#partnerName-error").html("");
		}
		if($("#status").hasClass('error')) {
			$("#status").removeClass('error');
			$("#status-error").html("");
		}
	}
});
</script>

<!-- Validation Methods -->
<script type="text/javascript">
$.validator.addMethod("dateHigherThanToday", function(value, element) {
	var startDate = getFormattedDate($('#fromDate').val());
	var endDate = getFormattedDate($('#toDate').val());
	
	if(endDate!='Invalid Date'){
		if (startDate <= endDate)
			return true;
		
		if (startDate >= endDate)
			return false;
	}
	else{
		return true;
	}
});

$.validator.addMethod("greaterThanEndDate", function(value, element) {
	var startDate = getFormattedDate($('#fromDate').val());
	var endDate = getFormattedDate($('#toDate').val());
	
	if(endDate!='Invalid Date'){
		if (endDate >= startDate)
			return true;
		
		if (endDate <= startDate)
			return false;
	}
	else{
		return true;
	}
});

$.validator.addMethod("dropdowncheck", function(value, element) {
	var jpNumber_ = $("#jpNumber").val();
	if(jpNumber_=="") {
		changeValidationRule();
	}
	if(value=="" && jpNumber_=="") {
		return false;
	} else
		return true;
});	

function changeValidationRule(){
	var flag = false;
	
	var fromDate_ = $("#fromDate").val();
	var toDate_   = $("#toDate").val();
	
	var partnerName_ = $("#partnerName").val();
	var status_ = $("#status").val();
	if(partnerName_ != "") {
		$("#partnerName-error").html("");
	}
	if(fromDate_ == "" || toDate_ == ""){
		flag = true;
	}
	if(status_!=""){
		$("#status-error").html("");
	}
	$('#fromDate').rules('add', {
		required: flag,
		dateHigherThanToday : true
	});
	$('#toDate').rules('add', {
		required: flag,
		greaterThanEndDate:true,
		dateHigherThanToday:true
	});
	if(flag == true){
		$("#jpMilesStatusUpdate").validate().element('#fromDate');
		$("#jpMilesStatusUpdate").validate().element('#toDate');
	}
}
</script>


<!-- Methods -->
<script type="text/javascript">
var	datetext;
function getFormattedDate(date){
	var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
	return new Date(date.replace(pattern,'$3-$2-$1'));
}

var d = new Date(); // for now
var h = d.getHours();
h = (h < 10) ? ("0" + h) : h ;
var m = d.getMinutes();
m = (m < 10) ? ("0" + m) : m ;
var s = d.getSeconds();
s = (s < 10) ? ("0" + s) : s ;
var month = d.getMonth()+1;
var day = d.getDate();
var year = d.getFullYear();
datetext = day+"-"+month+"-"+year+"_"+h+":"+m+":"+s;
var fromDate=$('#fromDate').val();
var toDate=$('#toDate').val();
var jpNumber=$('#jpNumber').val();
var partnerName=$('#partnerName').val();
var status= $('#status').val();
var validTypeRadio= $("#hiddenVal").val();
	$(document).ready(function() {
 	var table = $('.display').removeAttr('width').DataTable({
	dom: 'Blfrtip',
	scrollX:        true,
	"bPaginate": true,
	 "processing": true,
     "serverSide": true,
     "searching": false,
	//scrollCollapse: true,
	"ordering": false,
	columnDefs: [
	{ width: '243', targets: '_all' }
	],
	buttons: [
                /*  { extend: 'csv',   text:'export to csv',  title:'Business_Report_'+datetext},
                 { extend: 'excel', text:'export to excel',title:'Business_Report_'+datetext}, */
              ],
	fixedColumns: false,
    "ajax":  "${contextPath}/admin/serverPagination?fromDate="+fromDate+"&toDate="+toDate+"&jpNumber="+jpNumber+"&partnerName="+
    partnerName+"&status="+status+"&validTypeRadio="+validTypeRadio+"&reportType=jpplStatus",
    "columns": [
    	{
            "data": "null",
            className: "center",
            defaultContent: '<a href="" class="editStatus"><img class="edit_wraper" src="${contextPath}/static/img/edit_icon.png"></a>'
        },
        { "data": "jpNumber" },
        { "data": "transactionId" },
        { "data": "partnerItemId" },
        { "data": "formatedTransactionDate" },
        { "data": "partnerName" },
        { "data": "partnerCategoryName" },
        { "data": "jpplCategoryName" },
        { "data": "productName" },
        { "data": "price" },
        { "data": "ratePerMile" },
        { "data": "commAmount" },
        { "data": "jpplCommAmount" },
        { "data": "commissionRate" },
        { "data": "memberShare" },
        { "data": "tax" },
        { "data": "jpmilesPurchase" },
        { "data": "formatedExpectedDateOfCredit" },
        { "data": "jpmilesToCredit" },
        { "data": "jpmilesCancel" },
        { "data": "purchase_status" },
        { "data": "source" },
        { "data": "medium" },
        { "data": "campaign" },
        { "data": "member_or_guest" },
        { "data": "transactionVia" },
        { "data": "jpmiles_Status" },
        { "data": "smsStatus" }
    ]  
	});
 	  $('#jpMilesStatusUpdate').on('click', 'a.editStatus', function (e) {
 		 var checka = table.row( $(this).closest('tr')).data();
 	  	   id = checka.id; 		  
 		  var transactionId = table.row( $(this).closest('tr')).data().transactionId;
 		  var partnerItemId = table.row( $(this).closest('tr')).data().partnerItemId;
 		  var jpmilesToCredit = table.row( $(this).closest('tr')).data().jpmilesToCredit;
 		  var jpmilesCancel = table.row( $(this).closest('tr')).data().jpmilesCancel;
 		  var smsStatus = table.row( $(this).closest('tr')).data().smsStatus;
 		  var jpmiles_Status = table.row( $(this).closest('tr')).data().jpmiles_Status;
 		  
 		  $('#edit_transactionId').val(transactionId);
 		  $('#edit_partnerItemId').val(partnerItemId);
 		  $('#edit_jpmilesToCredit').val(jpmilesToCredit);
 		  $('#edit_jpmilesCancel').val(jpmilesCancel);
 		  $('#edit_smsStatus').val(smsStatus).selectmenu("refresh");
 		  if(jpmiles_Status=="Requested"){
 			 $('#edit_jpmiles_Status').val(1).selectmenu("refresh");
 		  }else if(jpmiles_Status=="Sent For Credit"){
 			 $('#edit_jpmiles_Status').val(2).selectmenu("refresh");
 		  }else if(jpmiles_Status=="Credited"){
 			 $('#edit_jpmiles_Status').val(3).selectmenu("refresh");
 		  }else{
 			 $('#edit_jpmiles_Status').val(4).selectmenu("refresh");
 		  }
 		 
 	  
 	         e.preventDefault();
 	        $('#myModal1').data('id', id).modal('show');
 	    } );
 	  
 	 $('#jpMilesCredit').on('click',function(){
 		 var json = {};
 		json["transactionId"] = $("#edit_transactionId").val();
 		json["partnerItemId"] = $("#edit_partnerItemId").val();
 		json["jpmilesToCredit"] = $("#edit_jpmilesToCredit").val();
 		json["jpmilesCancel"] = $("#edit_jpmilesCancel").val();
 		json["smsStatus"] = $("#edit_smsStatus").val();
 		json["jpmiles_Status"] = $("#edit_jpmiles_Status").val(); 
			var formData = new FormData()
			formData.append('json', JSON.stringify(json));
	  $.ajax({
			url : "${contextPath}/admin/jpplCreditUpdate",
			data : formData,
			dataType : 'text',
			processData : false,
			contentType : false,
			type : 'POST',

			success : function(response) {
				window.location.href = "${contextPath}/admin/jpMilesStatusUpdate?updateMsg="+response;
			},
			error: function (response) {
				 window.location.href="${contextPath}/admin/jpMilesStatusUpdate?updateMsg=failure";

				           	}
		});
	 
	  });
		
	}); 
	
	
	
	
	
</script>

	

