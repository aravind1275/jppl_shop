<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class='container'>
            <section class="affliates_management">
                <div id ="breadcrumb">
                    <ul>
                        <li>Home</li>
                        <li>Reports</li>
						<li>Analytics Report</li>
                    </ul>
                </div>  <!---breadcrumb--->
				<div class="section_heading">
					<c:if test="${fn:length(message) gt 0}"><div style="color:green;font-weight: bold;"  ><c:out value="${message}"></c:out></div> </c:if>
					<h5>Analytics Report</h5> 
				</div>
				<form:form autocomplete="off" id="analyticsReport" class="reports">
				<div class="main_section bg-b-w ">
				<div class="row pad_top">
					<div class="col-md-2">
						<label>Start Date</label><span class="mandatory_star">*</span>
						<div class="pick_date">
							<input type="text" class="form-control startDate" id="fromDate" name="fromDate" value="${fromDate}" readonly />
						</div>
					</div>
					<div class="col-md-2">
						<label>End Date</label><span class="mandatory_star">*</span>
						<div class="pick_date">
							<input type="text" class="form-control endDate" id="toDate" name="toDate" value="${toDate}" readonly />
						</div>
					</div>
					<div class="col-md-2">
						<label>Filter By</label><span class="mandatory_star">*</span>
						<fieldset class="infield">
							<select name="partnerName" id="partnerName" class="select_option">
								<option value="">-Select Partner-</option>
								<option value="all">All</option>
								<c:forEach items="${partnerList}" var="partner">								
									<c:choose>
										<c:when test="${partner.prtNo eq prtNo}">
											<option value="${partner.prtNo}" selected >${partner.partnerName}</option>
										</c:when>
										<c:otherwise>
											<option value="${partner.prtNo}" >${partner.partnerName}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</fieldset>
					</div>
					<div class="col-md-2">
						<label>JP Number</label><span class="mandatory_star"></span>
						<input type="text" class="form-control" name="jpNumber" id="jpNumber" placeholder="Enter JP No" value="${jpNumber}" />
					</div>
					<!--new field status starts  -->
					<div class="col-md-2">
						<label>Status</label><span class="mandatory_star">*</span>
						<fieldset class="infield">
							<select name="status" id="status" class="select_option">
								 <option value="">-Select Status-</option>
								<c:forEach items="${statusList}" var="stat">
									<c:choose>
										<c:when test="${stat eq status}">
											<option value="${stat}" selected >${stat}</option>
										</c:when>
										<c:otherwise>
											<option value="${stat}" >${stat}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</fieldset>
					</div>
					<div class="row pad_top export_btn">
					<!-- radio for valid invalid -->
					<div id="radioValid">
					<input type="radio" class="css-checkbox" name="validTypeRadio" id ="validTypeRadio1" value="valid" checked="checked">
                       <label class="css-label radGroup2" for="validTypeRadio1">Valid</label>
                    <input type="radio" class="css-checkbox" name="validTypeRadio" id ="validTypeRadio2" value="invalid" >
                       <label class="css-label radGroup2" for="validTypeRadio2">Invalid</label>
                       </div>
                       <input type="hidden" id="hiddenVal"  value="${hiddenVal}" />
                       <br>
				</div>
				<br>
					<div class="col-md-2 report_button">
							<div class="all-default-button">
								<input class="bg_w_button" id="Search" type="button" onClick="$(this).closest('form').submit();" value="Search" />
								 <input class="bg_w_button" type="button" id="clear" value="Clear" />
							</div>
							<div class="all-csv-export-button">
								<input type="button" class="bg_w_button_new" id="export_new" value="Export to csv"> 
								<input class="bg_w_button" type="button" id="clear_new" value="Clear" />
							</div>
							
						</div>
				</div>
						</div>
			<c:if test="${! empty analyticrptlist }">
				<div class="report_table">
				<!-- <div class="expot_btn">
					<label>Export</label> <input class="bg_w_button" type="button" value="XLS" />
				</div> -->
					<a  href="/afadmin/admin/exportAnalyticsCsv?fromDate=${fromDate}&toDate=${toDate}&jpNumber=${jpNumber}&partnerName=${prtNo}&status=${status}&validTypeRadio=${hiddenVal}" class="bg_w_button" id="export">export to csv</a>
					
					<table id="business_report" class="display" cellspacing="0" width="100%">
					    <thead>
					        <tr>
					            <th>JP Number</th>
					            <th>Reference Number</th>
					            <th>Affiliate Order Number</th>
					            <th>Order Date</th>
					            <th>Merchant</th>
					            <th>Partner Category</th>
					            <th>JPPL Category</th>
					            <th>Product</th>
					            <th>Transaction Amount / price</th>
					            <th>No Of JPMiles</th>
					            <th>Partner Commission Rate</th>
								<th>Partner Commission Amount</th>
								<th>JPPL commission amount</th>
					            <th>Expected Date Of Credit</th>
					            <th>JPMiles Credited</th>
					            <th>JPMiles Decline</th>
					            <th>Status</th>
					            <th>Source</th>
								<th>Medium</th>
								<th>Campaign</th>
								<th>Member/Guest</th>
								<th>Transaction Via</th>
					        </tr>
					    </thead><!--- thead ---->
					    <tbody>    
					    	<c:forEach items="${analyticrptlist}" var="analyticReport">
								<tr>
									<td>${analyticReport.jpNumber}</td>
									<td>${analyticReport.transactionId}</td>
									<td>${analyticReport.partnerItemId}</td>
									<td data-order="${analyticReport.orderDateTime}">${analyticReport.formatedTransactionDate}</td>
									<td>${analyticReport.partnerName}</td>
									<td>${analyticReport.partnerCategoryName}</td>
									<td>${analyticReport.jpplCategoryName}</td>
									<td>${analyticReport.productName}</td>
									<td>${analyticReport.price}</td>
									<td>${analyticReport.jpmilesPurchase}</td>
									<td>${analyticReport.ratePerMile}</td>
									<td>${analyticReport.commAmount}</td>
									<td>${analyticReport.jpplCommAmount}</td>
									<td data-order="${analyticReport.expectedDateOfCreditTime}">${analyticReport.formatedExpectedDateOfCredit}</td>

									<c:choose>
										<c:when test="${analyticReport.purchase_status eq 'failed'}">
											<td>NA</td>
											<td>NA</td>
										</c:when>
										<c:otherwise>
											<td>${analyticReport.jpmilesToCredit}</td>
											<td>${analyticReport.jpmilesCancel}</td>
										</c:otherwise>
									</c:choose>


									<td>${analyticReport.purchase_status}</td>
									<td>${analyticReport.source}</td>
									<td>${analyticReport.medium}</td>
									<td>${analyticReport.campaign}</td>
									<td>${analyticReport.member_or_guest}</td>
									<td>${analyticReport.transactionVia}</td>
								</tr>
							</c:forEach>
						</tbody><!--- tbody ---->
					</table><!--- table ---->
				</div><!--- report table ---->
				</c:if>
		</form:form>
            </section>
         </div>
         
         
<!-- variables          -->
<script type="text/javascript">
</script>

<!-- Form Validation -->
<script type="text/javascript">
$(document).ready(function () {
	
	 $("#radioValid").hide();
		if($("#status").val() == "All"){
			 $("#radioValid").show();
	 if($("#hiddenVal").val() == "valid"){
		 $("#validTypeRadio1").prop("checked", true);
	 }else if($("#hiddenVal").val() == "invalid"){
		 $("#validTypeRadio2").prop("checked", true);
	 }
		}
	
	$("#analyticsReport").validate({
		onkeyup: true,
		ignore:[],
		rules: {	
			fromDate:{
				required: true,
				dateHigherThanToday:true
			},
			toDate:{
				required: true,
				greaterThanEndDate:true,
				dateHigherThanToday:true
			},
			partnerName:{
				required: true,
			},
			status:{
				required: true,
			}
		},
		messages: {
			fromDate:{
				required: "Start date cannot be empty",
				dateHigherThanToday:"Start date must be less than or equal End date "
			},
			toDate:{
				required: "End date cannot be empty",
				greaterThanEndDate:"End date must be greater than or equal to Start date ",
				dateHigherThanToday:"Start date must be greater than or equal End date "
			},
			partnerName:{
				dropdowncheck: "Please select a Partner"
			}
		},
		errorElement: "div",
		errorPlacement: function(error, element) {
			if ($(element).hasClass("form-control startDate")) {
				error.insertAfter($(element).closest("div"));
			}
			else if ($(element).hasClass("form-control endDate")){
				error.insertAfter($(element).closest("div"));
			}
			else {
				error.insertAfter(element);
			}
		},
		submitHandler: function (form) {
			form.submit();
		}
	});
});
</script>

<!-- Events -->
<script type="text/javascript">

$('#status').on('selectmenuchange', function () {

	if($("#status").val() == "All"){
		$("#radioValid").show();
		$("input[name='validTypeRadio']").on('change',function(){
			
				if( $("#validTypeRadio1").is(':checked')) {
				$("#validTypeRadio1").prop("checked", true);
				$("#validTypeRadio1").val("valid");
			}
				if($("#validTypeRadio2").is(':checked')) {
				$("#validTypeRadio2").prop("checked", true);
				$("#validTypeRadio2").val("invalid");
			}  
		});
	}
	else{
		$("#validTypeRadio1").val('');
		$("#validTypeRadio2").val('');
		$("#radioValid").hide();
		}
});

$("#clear").on('click', function () {
	$("#fromDate").val('');
	$("#toDate").val('');
	$("#jpNumber").val('');
	$('#partnerName').val('');
	$('#partnerName').selectmenu("refresh");
	$('#status').val('');
	$('#status').selectmenu("refresh");
	$("#validTypeRadio1").val('');
	$("#validTypeRadio2").val('');
	$("#validTypeRadio2").prop("unchecked", true);
	$("#validTypeRadio1").prop("unchecked", true);
    $("#radioValid").hide();
	$("#analyticsReport").data('validator').resetForm();
	//$("#analyticsReport")[0].reset();
});

$( "#fromDate" ).on('change', function() {
	$("#analyticsReport").validate().element("#fromDate");
	var endDate = getFormattedDate($('#toDate').val());
	
	if(endDate!='Invalid Date'){
		$("#analyticsReport").validate().element("#toDate");
	}
});

$( "#toDate" ).on('change', function() {
	$("#analyticsReport").validate().element("#fromDate");
	$("#analyticsReport").validate().element("#toDate");
});

$('#partnerName').on('selectmenuchange', function () {
	
var PartnerName  = $('#partnerName').find('option:selected').text();
	
	console.log("partnerN---->",PartnerName);
	
	if(PartnerName == "All")
		{
			console.log("inside if");
			 /* $(".all-csv-export-button").css({display: "block"});
			 $(".all-default-button").css({display: "none"}); */
			 / $("#Search").prop('disabled', true); /
			 $(".all-csv-export-button").css("display","block");
			 $("#export").css("display","none");
			 $(".new_export").css("display","block");
			 $(".all-default-button").css("display","none");			 
			 $("#analyticsReport").validate().element('#partnerName');		
		}
	else
		{	
		console.log("inside else");
		/* $(".all-csv-export-button").css({display: "none"});
		 $(".all-default-button").css({display: "block"}); */
		 $(".all-csv-export-button").css("display","none");
		 $(".all-default-button").css("display","block");
		 $(".new_export").css("display","none");
		 $("#export").css("display","block");		 
		 $("#analyticsReport").validate().element('#partnerName');
			
		}
	
	
});

$('#status').on('selectmenuchange', function () {
	$("#analyticsReport").validate().element('#status');
});

$( "#jpNumber" ).on('change', function() {
	var jpNumber_ = $("#jpNumber").val();
	if(jpNumber_ != "") {
		if($("#fromDate").hasClass('error')) {
			$("#fromDate").removeClass('error');
			$("#toDate").removeClass('error');
			$("#fromDate-error").html("");
			$("#toDate-error").html("");
			
			$('#fromDate').rules('add', {
				required: false,
				dateHigherThanToday : true
			});
			$('#toDate').rules('add', {
				required: false,
				greaterThanEndDate:true,
				dateHigherThanToday:true
			});
			$('#partnerName').rules('add', {
				dropdowncheck: false
			});
			
			$('#status').rules('add', {
				dropdowncheck: false
			});
			$("#analyticsReport").validate().element('#fromDate');
			$("#analyticsReport").validate().element('#toDate');
			$("#analyticsReport").validate().element('#partnerName');
			$("#analyticsReport").validate().element('#status');
		}
		if($("#partnerName").hasClass('error')) {
			$("#partnerName").removeClass('error');
			$("#partnerName-error").html("");
			
			if($("#status").hasClass('error')) {
				$("#status").removeClass('error');
				$("#status-error").html("");
			}
		}
	}
});
</script>

<!-- Validation Methods -->
<script type="text/javascript">
$.validator.addMethod("dateHigherThanToday", function(value, element) {
	var startDate = getFormattedDate($('#fromDate').val());
	var endDate = getFormattedDate($('#toDate').val());
	
	if(endDate!='Invalid Date'){
		if (startDate <= endDate)
			return true;
		
		if (startDate >= endDate)
			return false;
	}
	else{
		return true;
	}
});

$.validator.addMethod("greaterThanEndDate", function(value, element) {
	var startDate = getFormattedDate($('#fromDate').val());
	var endDate = getFormattedDate($('#toDate').val());
	
	if(endDate!='Invalid Date'){
		if (endDate >= startDate)
			return true;
		
		if (endDate <= startDate)
			return false;
	}
	else{
		return true;
	}
});

$("#clear_new").on('click', function () {
	$("#fromDate").val('');
	$("#toDate").val('');
	$("#jpNumber").val('');
	$('#partnerName').val('');
	$('#partnerName').selectmenu("refresh");
	//new report starts  
	$('#status').val('');
	$("#validTypeRadio1").val('');
	$("#validTypeRadio2").val('');
	$("#validTypeRadio2").prop("unchecked", true);
	$("#validTypeRadio1").prop("unchecked", true);
    $("#radioValid").hide();
	$('#status').selectmenu("refresh");
	//new report ends
	
	$("#businessReport").data('validator').resetForm();
	//$("#businessReport")[0].reset();
});

$.validator.addMethod("dropdowncheck", function(value, element) {
	var jpNumber_ = $("#jpNumber").val();
	if(jpNumber_=="") {
		changeValidationRule();
	}
	if(value=="" && jpNumber_=="") {
		return false;
	} else
		return true;
});

function changeValidationRule(){
	var flag = false;
	
	var fromDate_ = $("#fromDate").val();
	var toDate_   = $("#toDate").val();
	var partnerName_ = $("#partnerName").val();
	if(partnerName_ != "") {
		$("#partnerName-error").html("");
	}
	var status_ = $("#status").val();
	if(status_ != "") {
		$("#status-error").html("");
	}
	if(fromDate_ == "" || toDate_ == ""){
		flag = true;
	}
	$('#fromDate').rules('add', {
		required: flag,
		dateHigherThanToday : true
	});
	$('#toDate').rules('add', {
		required: flag,
		greaterThanEndDate:true,
		dateHigherThanToday:true
	});
	if(flag == true){
		$("#analyticsReport").validate().element('#fromDate');
		$("#analyticsReport").validate().element('#toDate');
	}
}
</script>

<!-- Methods -->
<script type="text/javascript">
function getFormattedDate(date){
	var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
	return new Date(date.replace(pattern,'$3-$2-$1'));
}

var d = new Date(); // for now
var h = d.getHours();
h = (h < 10) ? ("0" + h) : h ;
var m = d.getMinutes();
m = (m < 10) ? ("0" + m) : m ;
var s = d.getSeconds();
s = (s < 10) ? ("0" + s) : s ;
var month = d.getMonth()+1;
var day = d.getDate();
var year = d.getFullYear();
datetext = day+"-"+month+"-"+year+"_"+h+":"+m+":"+s;
var fromDate=$('#fromDate').val();
var toDate=$('#toDate').val();
var jpNumber=$('#jpNumber').val();
var partnerName=$('#partnerName').val();
var status= $('#status').val();
var validTypeRadio= $("#hiddenVal").val();
$(document).ready(function() {

$('.display').removeAttr('width').DataTable({
	dom: 'Blfrtip',
	scrollX:        true,
	scrollCollapse: true,
	"bPaginate": true,
	"processing": true,
    "serverSide": true,
    "ordering": false,
	columnDefs: [
	{ width: '243', targets: '_all' }
	],
	buttons: [
                 { extend: 'csv', text:'export to csv',title:'Analytics_Report_'+datetext},
                 { extend: 'excel', text:'export to excel',title:'Analytics_Report_'+datetext},
              ],
	fixedColumns: false,
	"ajax":  "${contextPath}/admin/serverPagination?fromDate="+fromDate+"&toDate="+toDate+"&jpNumber="+jpNumber+"&partnerName="+
    partnerName+"&status="+status+"&validTypeRadio="+validTypeRadio+"&reportType=analytics",
    "columns": [
        { "data": "jpNumber" },
        { "data": "transactionId" },
        { "data": "partnerItemId" },
        { "data": "formatedTransactionDate" },
        { "data": "partnerName" },
        { "data": "partnerCategoryName" },
        { "data": "jpplCategoryName" },
        { "data": "productName" },
        { "data": "price" },
        { "data": "jpmilesPurchase" },
        { "data": "ratePerMile" },
        { "data": "commAmount" },
        { "data": "jpplCommAmount" },
        { "data": "formatedExpectedDateOfCredit" },
        { "data": "jpmilesToCredit" },
        { "data": "jpmilesCancel" },
        { "data": "purchase_status" },
        { "data": "source" },
        { "data": "medium" },
        { "data": "campaign" },
        { "data": "memberOrGuest" },
        { "data": "transactionVia" }            
    ]  
	});

	
		
	

	});
	
	
$("#export_new").on("click",function(){
	
	console.log("-------------> entered <---------------");
	$("#analyticsReport").validate();
	console.log("entered");
	var startDate;
	var endDate;
	var jpNumber;
	var status;
	var validTypeRadio = "not defined";
	var allowExport = true;
	 
	startDate = $("#fromDate").val();
	endDate = $("#toDate").val();
	jpNumber = $("#jpNumber").val();
	startDate = $("#fromDate").val();
	status = $("#status").find(":selected").text();
	
	
	console.log("---------->startDate ",startDate);
	console.log("---------->endDate ",endDate);
	console.log("---------->jpNumber ",jpNumber);
	console.log("---------->startDate ",startDate);
	console.log("---------->validTypeRadio ",validTypeRadio);
	console.log("---------->status ",status);
	
	
	if(startDate.length == 0){
		console.log("invalid date");
		$("#analyticsReport").validate().element("#fromDate");
		allowExport = false;
	}
	if(endDate.length == 0){
		console.log("invalid todate");
		$("#analyticsReport").validate().element("#toDate");
		allowExport = false;
	}
	if(status == "-Select Status-"){
		console.log("invalid status");
		$("#analyticsReport").validate().element("#status");
		allowExport = false;
	}
	
	
	if(allowExport){
		
		/* $("#fromDate-error").html("");
		$("#toDate-error").html("");
		$("#status-error").html(""); */
		
		if(status == "All"){
			if( $("#validTypeRadio1").is(':checked')) {
				validTypeRadio = "valid";
				} 
				if($("#validTypeRadio2").is(':checked')) {
					validTypeRadio = "invalid";
				}  
		}
		
		
		
		
		/* $.ajax({
				 url: "${contextPath}/admin/exportAllBusinessCsv?fromDate="+startDate+"&toDate="+endDate+"&jpNumber="+jpNumber+
						 "&status="+status+"&validTypeRadio="+validTypeRadio+"&reportType=business",
				 type: "GET",
				 contentType : 'application/json; charset=utf-8',
				 dataType: "json",
				 success: function(response)
				 {
					 console.log("export date---:",response);
				 }
			
		
	}); */

		window.location.href = "${contextPath}/admin/exportAllAnalyticsCsv?fromDate="+startDate+"&toDate="+endDate+"&jpNumber="+jpNumber+
		 						"&status="+status+"&validTypeRadio="+validTypeRadio+"&reportType=business";
		
		
		
	}
	
	
	
	
	
	
	
});
</script>
         
         