<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

	<div class='container'>
		<section class="affliates_management">
			<div id="breadcrumb">
				<ul>
					<li>Home</li>
					<li>Reports</li>
					<li>Queing Logic Report</li>
				</ul>
			</div>
			<!---breadcrumb--->
			<div class="section_heading">
				<c:if test="${fn:length(message) gt 0}"><div style="color:green;font-weight: bold;"  ><c:out value="${message}"></c:out></div> </c:if>
				<h5>Queing Logic Report</h5>
			</div>
			 <form:form autocomplete="off" id="queingLogicReport" >
			<div class="main_section bg-b-w ">
				<div class="row pad_top">
					<div class="col-md-3">
						<label>Start Date</label><span class="mandatory_star">*</span>
						<div class="pick_date">
							<input type="text" class="form-control startDate" id="fromDate" name="fromDate" value="${fromDate}" readonly />
<!-- 							<p id="log" style="display:none;color:red;"></p> -->
						</div>
					</div>
					
					<div class="col-md-3">
						<label>End Date</label><span class="mandatory_star">*</span>
						<div class="pick_date">
							<input type="text" class="form-control endDate" id="toDate" name="toDate" value="${toDate}" readonly />
<!-- 							<p id="log" style="display:none;color:red;"></p> -->
						</div>
					</div>
					
					<div class="col-md-3 margT5">
						<label>Filter By</label>
						<fieldset class="infield">
							<select name="partnerName" id="partnerName" class="select_option">
								<option value="">-Select Partner-</option>
								<c:forEach items="${partnerList}" var="partner">
									<c:choose>
										<c:when test="${partner.prtNo eq prtNo}">
											<option value="${partner.prtNo}" selected >${partner.partnerName}</option>
										</c:when>
										<c:otherwise>
											<option value="${partner.prtNo}" >${partner.partnerName}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</fieldset>
					</div>
					
					<div class="col-md-3">
						<label>JP Number</label><span class="mandatory_star"></span>
						<input type="text" class="form-control" name="jpNumber" id="jpNumber" placeholder="Enter JP No" value="${jpNumber}" />
					</div>
					
					
				</div>
				<div class="row pad_top export_btn">
					<div class="col-md-2 report_button">
						<input class="bg_w_button" id="search" type="button" onClick="$(this).closest('form').submit();" value="Search" /> <input class="bg_w_button" type="button" id="clear" value="Clear" />
					</div>
				</div>
				<!--- row ------>
			</div>
			<!---- date picker section ---->
			<c:if test="${! empty queingreportBeanList}">
				<div class="report_table">
					<!-- <div class="expot_btn">
						<label>Export</label> <input class="bg_w_button" type="button" value="XLS" />
					</div> -->
					<table id="business_report" class="display" cellspacing="0" width="100%">
						<thead>
							<tr>
								<!-- <th>Sr.No</th> -->
								<th>JP Number</th>
								<th>Partner Name</th>
								<th>Transaction Date</th>
								<th>Product Name</th>
								<th>Expected Date of Credit</th>
								<th>Affiliate Order Number</th>
								<th>Affiliate Reference Number</th>
								<th>JP Miles</th>
								<th>Status</th>
								<th>OrderCount</th>
								<th>Request</th>
								<th>Response</th>
							</tr>
						</thead>
						<!--- thead ---->
						<tbody>


							<c:forEach items="${queingreportBeanList}" var="queingReport">
								<tr>
									<td>${queingReport.jpNumber}</td>
									<td>${queingReport.partnerName}</td>
									<td data-order="${queingReport.orderDateTime}">${queingReport.formatedTransactionDate}</td>
									<c:choose>
									<c:when test="${empty queingReport.productName}">
									<td>NA</td>
									</c:when>
									<c:otherwise>
									<td>${queingReport.productName}</td>
									</c:otherwise>
									</c:choose>
									
									<td data-order="${queingReport.expectedDateOfCreditTime}">${queingReport.formatedExpectedDateOfCredit}</td>
									<td>${queingReport.partnerItemId}</td>
									<td>${queingReport.transactionId}
									<td>${queingReport.jpmilesPurchase}</td>
									<td>${queingReport.jpmiles_Status}</td>
									<td>${queingReport.orderCount}</td>
									<c:choose>
									<c:when test="${queingReport.jpmiles_Status eq 'Declined'}">
									<td  style="color: blue;"><a href="/afadmin/admin/downloadFile?orderId=${queingReport.partnerItemId}&type=3&isXML=${queingReport.isXml}">Request</a></td>
									<td style="color: blue;"><a href="/afadmin/admin/downloadFile?orderId=${queingReport.partnerItemId}&type=4&isXML=${queingReport.isXml}">Response</a></td>
									</c:when>
									<c:otherwise>
									<td>Request</td>
									<td>Response</td>
									</c:otherwise>
									</c:choose>
								</tr>
							</c:forEach>
						</tbody>
						<!--- tbody ---->
					</table>
					<!--- table ---->
				</div>
			</c:if>
			<!--- report table ---->
			</form:form>
		</section>
	</div>
	
	<!--  form validation -->
	<script>
	$(document).ready(function(){
		
	$("#queingLogicReport").validate({
			onkeyup: true,
			ignore : [],
			rules:{
		fromDate : {
					required : true ,
					dateHigherThanToday:true
				},
				toDate : {
					required :true,
					dateHigherThanToday:true
				}
			},
		messages: {
			fromDate : {
				required : "Start date cannot be empty",
				dateHigherThanToday:"Start date must be less than or equal End date "
			},
			toDate : {
				required : "End date cannot be empty",
				greaterThanEndDate:"End date must be greater than or equal to Start date ",
				dateHigherThanToday:"End date must be greater than or equal to start date "
			}
		},
		errorElement: "div",
		errorPlacement: function(error, element) {
			if ($(element).hasClass("form-control startDate")) {
				error.insertAfter($(element).closest("div"));
			}
			else if ($(element).hasClass("form-control endDate")){
				error.insertAfter($(element).closest("div"));
			}
			else {
				error.insertAfter(element);
			}
		},
		submitHandler: function (form) {
			form.submit();
		}
		});
	});
	
	$("#clear").on('click', function () {
		$("#fromDate").val('');
		$("#toDate").val('');
		$("#jpNumber").val('');
		$('#partnerName').val('');
		$('#partnerName').selectmenu("refresh");
		
		$("#queingLogicReport").data('validator').resetForm();
		//$("#businessReport")[0].reset();
	});

	$( "#fromDate" ).on('change', function() {
		$("#queingLogicReport").validate().element("#fromDate");
		var endDate = getFormattedDate($('#toDate').val());
		
		if(endDate!='Invalid Date'){
			$("#queingLogicReport").validate().element("#toDate");
		}
	});

	$( "#toDate" ).on('change', function() {
		$("#queingLogicReport").validate().element("#fromDate");
		$("#queingLogicReport").validate().element("#toDate");
	});

	$('#partnerName').on('selectmenuchange', function () {
		$("#queingLogicReport").validate().element('#partnerName');
	});
	$('#status').on('selectmenuchange', function () {
		$("#queingLogicReport").validate().element('#status');
	});
	$( "#jpNumber" ).on('change', function() {
		var jpNumber_ = $("#jpNumber").val();
		if(jpNumber_ != "") {
			if($("#fromDate").hasClass('error')) {
				$("#fromDate").removeClass('error');
				$("#toDate").removeClass('error');
				$("#fromDate-error").html("");
				$("#toDate-error").html("");
				
				$('#fromDate').rules('add', {
					required: false,
					dateHigherThanToday : true
				});
				$('#toDate').rules('add', {
					required: false,
					greaterThanEndDate:true,
					dateHigherThanToday:true
				});
				$('#partnerName').rules('add', {
					dropdowncheck: false
				});
				$('#status').rules('add', {
					dropdowncheck: false
				});
				$("#queingLogicReport").validate().element('#fromDate');
				$("#queingLogicReport").validate().element('#toDate');
				$("#queingLogicReport").validate().element('#partnerName');
			}
			if($("#partnerName").hasClass('error')) {
				$("#partnerName").removeClass('error');
				$("#partnerName-error").html("");
			}
			if($("#status").hasClass('error')) {
				$("#status").removeClass('error');
				$("#status-error").html("");
			}
		}
	});
	</script>

	<!-- Validation Methods -->
	<script type="text/javascript">
	$.validator.addMethod("dateHigherThanToday", function(value, element) {
		var startDate = getFormattedDate($('#fromDate').val());
		var endDate = getFormattedDate($('#toDate').val());
		
		if(endDate!='Invalid Date'){
			if (startDate <= endDate)
				return true;
			
			if (startDate >= endDate)
				return false;
		}
		else{
			return true;
		}
	});

	$.validator.addMethod("greaterThanEndDate", function(value, element) {
		var startDate = getFormattedDate($('#fromDate').val());
		var endDate = getFormattedDate($('#toDate').val());
		
		if(endDate!='Invalid Date'){
			if (endDate >= startDate)
				return true;
			
			if (endDate <= startDate)
				return false;
		}
		else{
			return true;
		}
	});

	$.validator.addMethod("dropdowncheck", function(value, element) {
		var jpNumber_ = $("#jpNumber").val();
		if(jpNumber_=="") {
			changeValidationRule();
		}
		if(value=="" && jpNumber_=="") {
			return false;
		} else
			return true;
	});	

	function changeValidationRule(){
		var flag = false;
		
		var fromDate_ = $("#fromDate").val();
		var toDate_   = $("#toDate").val();
		
		var partnerName_ = $("#partnerName").val();
		var status_ = $("#status").val();
		if(partnerName_ != "") {
			$("#partnerName-error").html("");
		}
		if(fromDate_ == "" || toDate_ == ""){
			flag = true;
		}
		
		$('#fromDate').rules('add', {
			required: flag,
			dateHigherThanToday : true
		});
		$('#toDate').rules('add', {
			required: flag,
			greaterThanEndDate:true,
			dateHigherThanToday:true
		});
		if(flag == true){
			$("#queingLogicReport").validate().element('#fromDate');
			$("#queingLogicReport").validate().element('#toDate');
		}
	}
	</script>

	<!-- Methods -->
	<script type="text/javascript">
	var	datetext;
	function getFormattedDate(date){
		var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
		return new Date(date.replace(pattern,'$3-$2-$1'));
	}

	var d = new Date(); // for now
	var h = d.getHours();
	h = (h < 10) ? ("0" + h) : h ;
	var m = d.getMinutes();
	m = (m < 10) ? ("0" + m) : m ;
	var s = d.getSeconds();
	s = (s < 10) ? ("0" + s) : s ;
	var month = d.getMonth()+1;
	var day = d.getDate();
	var year = d.getFullYear();
	datetext = day+"-"+month+"-"+year+"_"+h+":"+m+":"+s;

	$('.display').removeAttr('width').DataTable({
		dom: 'Blfrtip',
		scrollX:        true,
		//scrollCollapse: true,
		"order": [[ 2, "desc" ]],
		columnDefs: [
		{ width: '243', targets: '_all' }
		],
		buttons: [
	                 { extend: 'csv',   text:'export to csv',  title:'queingLogicReport_'+datetext},
	                 { extend: 'excel', text:'export to excel',title:'queingLogicReport_'+datetext},
	              ],
		fixedColumns: false
		});
		
	</script>
	</script>