<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class='container'>
            <section class="affliates_management">
                <div id ="breadcrumb">
                    <ul>
                        <li>Home</li>
                        <li>Reports</li>
						<li>Mapped Products Report</li>
                    </ul>
                </div>  <!---breadcrumb--->
				<div class="section_heading">
					<c:if test="${fn:length(message) gt 0}"><div class="alertMessage" style="color:green;font-weight: bold;">
					<c:out value="${message}"></c:out></div></c:if>
					<h5>Mapped Products Report</h5> 
				</div>
				<form:form autocomplete="off" id="mappedProductsReport" commandName="mappedProductsReport">
				<div class="main_section bg-b-w ">
				<div class="row pad_top">
					<div class="col-md-3">
						<label>Select By</label>
						<fieldset class="infield">
							<form:select path="selectBy" id="selectBy" class="select_option">
								<form:option value="">-Select By-</form:option>
								<form:option value="PRTNAME">Partner Name</form:option>
								<form:option value="BRANDNAME">Brand Name</form:option>
								<form:option value="PID">Product ID</form:option>
								<form:option value="MPID">Master Product ID</form:option>
								<form:option value="PRDNAME">Product Name</form:option>
								<form:option value="JPPLCAT">JPPL Category</form:option>
							</form:select>
						</fieldset>
					</div>
					<div class="col-md-3">
						<label>Select By Value</label>
						<form:input type="text" class="form-control" path="selectByValue" id="selectByValue" />
					</div>
				</div>
				
				<div class="row pad_top export_btn">
					<div class="col-md-2 report_button">
						<input class="bg_w_button" type="button" onClick="$(this).closest('form').submit();" value="Search" />
						<input class="bg_w_button" type="button" id="clear" value="Clear" />
					</div>
				</div>
				<!--- row ------>
			</div>
			<c:if test="${! empty mappedProductsReportList }">
				<div class="report_table pad_bottom mar_bottom">
					<table id="Mapped_Products_Report" class="display" cellspacing="0" width="100%">
					    <thead>
					        <tr>
					            <th>Partner Name</th>
					            <th>Brand Name</th>
					            <th>Product ID</th>
					            <th>Master Product ID</th>
					            <th>Product Name</th>
					            <th>JPPL Category</th>
					        </tr>
					    </thead><!--- thead ---->
					    <tbody>    
					    	<c:forEach items="${mappedProductsReportList}" var="mappedProductsReport">
								<tr>
									<td>${mappedProductsReport.partnerName}</td>
									<td>${mappedProductsReport.brandName}</td>
									<td>${mappedProductsReport.productID}</td>
									<td>${mappedProductsReport.masterPID}</td>
									<td>${mappedProductsReport.productName}</td>
									<td>${mappedProductsReport.categoryName}</td>
									
								</tr>
							</c:forEach>
						</tbody><!--- tbody ---->
					</table><!--- table ---->
				</div><!--- report table ---->
				</c:if>
		</form:form>
            </section>
         </div>
         
<!-- Form Validation -->
<script type="text/javascript">
$(document).ready(function () {
	$("#mappedProductsReport").validate({
		onkeyup: true,
		ignore:[],
		rules: {	
			selectByValue:{
				notEmpty : true,
			}
		},
		messages: {
			selectByValue:{
				notEmpty: "Select By is selected, Select By Value cannot be empty",
			}
		},
		errorElement: "div",
		errorPlacement: function(error, element) {
				error.insertAfter(element);
		},
		submitHandler: function (form) {
			form.submit();
		}
	});
});
$.validator.addMethod("notEmpty", function(value, element) {
	if(($("#selectBy").val() != "") && (value == "")) {
		return false;
	} else
		return true;
});
</script>

<!-- Events -->
<script type="text/javascript">
$("#clear").on('click', function () {
	var validator = $( "#mappedProductsReport" ).validate();
	validator.resetForm();
	validator.reset();
	
	$("#selectBy").val("");
	$("#selectByValue").val("");
	$('#selectBy').selectmenu("refresh");
});
$('#selectBy').on('selectmenuchange', function() {
	$("#selectByValue").val("");
});
</script>

<!-- Validation Methods -->
<script type="text/javascript">


</script>

<!-- Methods -->
<script type="text/javascript">
var d = new Date(); // for now
var h = d.getHours();
h = (h < 10) ? ("0" + h) : h ;
var m = d.getMinutes();
m = (m < 10) ? ("0" + m) : m ;
var s = d.getSeconds();
s = (s < 10) ? ("0" + s) : s ;
var month = d.getMonth()+1;
var day = d.getDate();
var year = d.getFullYear();
datetext = day+"-"+month+"-"+year+"_"+h+":"+m+":"+s;

$('.display').removeAttr('width').DataTable({
	dom: 'Blfrtip',
	scrollX:        true,
	"searching": false,
	scrollCollapse: true,
	columnDefs: [
			{ width: '243', targets: '_all' }
	],
	buttons: [
                 { extend: 'excel', text:'export to excel',title:'Mapped_Products_Report_'+datetext},
              ],
	fixedColumns: false
});
</script>
         
         