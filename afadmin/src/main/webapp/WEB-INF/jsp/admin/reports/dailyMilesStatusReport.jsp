<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

	<div class='container'>
		<section class="affliates_management">
			<div id="breadcrumb">
				<ul>
					<li>Home</li>
					<li>Reports</li>
					<li>Daily Miles Status Report</li>
				</ul>
			</div>
			<!---breadcrumb--->
			<div class="section_heading">
				<c:if test="${fn:length(message) gt 0}"><div style="color:green;font-weight: bold;"  ><c:out value="${message}"></c:out></div> </c:if>
				<h5>Daily Miles Status Report</h5>
			</div>
			 <form:form autocomplete="off" id="dailyMilesStatusReport" >
			<div class="main_section bg-b-w ">
				<div class="row pad_top">
					<div class="col-md-3">
						<label>Start Date</label><span class="mandatory_star">*</span>
						<div class="pick_date">
							<input type="text" class="form-control startDate" id="fromDate" name="fromDate" value="${fromDate}" readonly />
<!-- 							<p id="log" style="display:none;color:red;"></p> -->
						</div>
					</div>
					
					<div class="col-md-3">
						<label>End Date</label><span class="mandatory_star">*</span>
						<div class="pick_date">
							<input type="text" class="form-control endDate" id="toDate" name="toDate" value="${toDate}" readonly />
<!-- 							<p id="log" style="display:none;color:red;"></p> -->
						</div>
					</div>
					
					<div class="col-md-3 margT5">
						<label>Filter By</label>
						<fieldset class="infield">
							<select name="partnerName" id="partnerName" class="select_option">
								<option value="">-Select Partner-</option>
								<c:forEach items="${partnerList}" var="partner">
									<c:choose>
										<c:when test="${partner.prtNo eq prtNo}">
											<option value="${partner.prtNo}" selected >${partner.partnerName}</option>
										</c:when>
										<c:otherwise>
											<option value="${partner.prtNo}" >${partner.partnerName}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</fieldset>
					</div>
					
					<div class="col-md-3">
						<label>JP Number</label><span class="mandatory_star"></span>
						<input type="text" class="form-control" name="jpNumber" id="jpNumber" placeholder="Enter JP No" value="${jpNumber}" />
					</div>
					
					
				</div>
				<div class="row pad_top export_btn">
					<div class="col-md-2 report_button">
						<input class="bg_w_button" id="search" type="button" onClick="$(this).closest('form').submit();" value="Search" /> <input class="bg_w_button" type="button" id="clear" value="Clear" />
					</div>
				</div>
				<!--- row ------>
			</div>
			<!---- date picker section ---->
			<c:if test="${! empty dailyMilesReportList }">
				<div class="report_table">
					<!-- <div class="expot_btn">
						<label>Export</label> <input class="bg_w_button" type="button" value="XLS" />
					</div> -->
					<table id="business_report" class="display" cellspacing="0" width="100%">
						<thead>
							<tr>
								<!-- <th>Sr.No</th> -->
								<th>JP Number</th>
								<th>Partner Name</th>
								<th>Transaction Date</th>
								<th>Product Name</th>
								<th>Expected Date of Credit</th>
								<th>Affiliate Order Number</th>
								<th>Affiliate Reference Number</th>
								<th>JP Miles</th>
								<th>Status</th>
								<th>Transaction via</th>
								<th>Request</th>
								<th>Response</th>
							</tr>
						</thead>
						<!--- thead ---->
						<tbody>


							<c:forEach items="${dailyMilesReportList}" var="dailyReport">
								<tr>
									<td>${dailyReport.jpNumber}</td>
									<td>${dailyReport.partnerName}</td>
									<td data-order="${dailyReport.orderDateTime}">${dailyReport.formatedTransactionDate}</td>
									<c:choose>
									<c:when test="${empty dailyReport.productName}">
									<td>NA</td>
									</c:when>
									<c:otherwise>
									<td>${dailyReport.productName}</td>
									</c:otherwise>
									</c:choose>
									
									<td data-order="${dailyReport.expectedDateOfCreditTime}">${dailyReport.formatedExpectedDateOfCredit}</td>
									<td>${dailyReport.partnerItemId}</td>
									<td>${dailyReport.transactionId}
									<td>${dailyReport.jpmilesPurchase}</td>
									<td>${dailyReport.jpmiles_Status}</td>
									<td>${dailyReport.transactionVia}</td>
									<c:choose>
									<c:when test="${dailyReport.request}">
									<td  style="color: blue;"><a href="/afadmin/admin/downloadFile?orderId=${dailyReport.partnerItemId}&type=1&isXML=${dailyReport.isXml}">Request</a></td>
									<td style="color: blue;"><a href="/afadmin/admin/downloadFile?orderId=${dailyReport.partnerItemId}&type=2&isXML=${dailyReport.isXml}">Response</a></td>
									</c:when>
									<c:otherwise>
									<td>Request</td>
									<td>Response</td>
									</c:otherwise>
									</c:choose>
								</tr>
							</c:forEach>
						</tbody>
						<!--- tbody ---->
					</table>
					<!--- table ---->
				</div>
			</c:if>
			<!--- report table ---->
			</form:form>
		</section>
	</div>
	

<!-- variables          -->
<script type="text/javascript">
</script>

<!-- Form Validation -->
<script type="text/javascript">

$(document).ready(function () {
	$("#dailyMilesStatusReport").validate({
		onkeyup: true,
		ignore:[],
		rules: {	
			fromDate:{
				required: true,
				dateHigherThanToday:true
			},
			toDate:{
				required: true,
				greaterThanEndDate:true,
				dateHigherThanToday:true
			},
		},
		messages: {
			fromDate:{
				required: "Start date cannot be empty",
				dateHigherThanToday:"Start date must be less than or equal End date "
			},
			toDate:{
				required: "End date cannot be empty",
				greaterThanEndDate:"End date must be greater than or equal to Start date ",
				dateHigherThanToday:"Start date must be greater than or equal End date "
			},
		},
		errorElement: "div",
		errorPlacement: function(error, element) {
			if ($(element).hasClass("form-control startDate")) {
				error.insertAfter($(element).closest("div"));
			}
			else if ($(element).hasClass("form-control endDate")){
				error.insertAfter($(element).closest("div"));
			}
			else {
				error.insertAfter(element);
			}
		},
		submitHandler: function (form) {
			form.submit();
		}
	});
});
</script>

<!-- Events -->
<script type="text/javascript">
/* $('#search').click(function() {
    var sts=$('#fromDate').val();
    var end=$('#toDate').val();
    var selectId = $('#select_option_wrap').val();
    var b = end.split("-") ;
    var a = sts.split("-") ;
    var startDate1 = new Date( a[2], (a[1] - 1), a[0] ) ;
    var endDate1 = new Date( b[2], (b[1] - 1), b[0] ) ;
    startDate1.setHours(0,0,0,0);
    endDate1.setHours(0,0,0,0);
    if(startDate1===""){
        $("#log").html("Start Date cannot be empty");
        $("#log").show();
        $('#log').fadeIn().delay(2000).fadeOut();
     e.preventDefault();
    }
    if(endDate1===""){
        $("#log").html("End Date cannot be empty");
        $("#log").show();
        $('#log').fadeIn().delay(2000).fadeOut();
     e.preventDefault();
    }
   if(selectId===0){
        $("#log").html("Please select a partner");
        $("#log").show();
        $('#log').fadeIn().delay(2000).fadeOut();
     e.preventDefault();
   }
     if(endDate1<startDate1){
          $("#log").html("End Date should be Greater than Start Date");
            $("#log").show();
            $('#log').fadeIn().delay(2000).fadeOut();
         e.preventDefault();
     }
     else if('${servicerptList.size()}'>0){
   window.location.href=""
       
   }
   	
	//$(this).closest('form').submit();
     
}); */

$("#clear").on('click', function () {
	$("#fromDate").val('');
	$("#toDate").val('');
	$("#jpNumber").val('');
	$('#partnerName').val('');
	$('#partnerName').selectmenu("refresh");
	
	$("#dailyMilesStatusReport").data('validator').resetForm();
	//$("#businessReport")[0].reset();
});

$( "#fromDate" ).on('change', function() {
	$("#dailyMilesStatusReport").validate().element("#fromDate");
	var endDate = getFormattedDate($('#toDate').val());
	
	if(endDate!='Invalid Date'){
		$("#dailyMilesStatusReport").validate().element("#toDate");
	}
});

$( "#toDate" ).on('change', function() {
	$("#dailyMilesStatusReport").validate().element("#fromDate");
	$("#dailyMilesStatusReport").validate().element("#toDate");
});

$('#partnerName').on('selectmenuchange', function () {
	$("#dailyMilesStatusReport").validate().element('#partnerName');
});
$('#status').on('selectmenuchange', function () {
	$("#dailyMilesStatusReport").validate().element('#status');
});
$( "#jpNumber" ).on('change', function() {
	var jpNumber_ = $("#jpNumber").val();
	if(jpNumber_ != "") {
		if($("#fromDate").hasClass('error')) {
			$("#fromDate").removeClass('error');
			$("#toDate").removeClass('error');
			$("#fromDate-error").html("");
			$("#toDate-error").html("");
			
			$('#fromDate').rules('add', {
				required: false,
				dateHigherThanToday : true
			});
			$('#toDate').rules('add', {
				required: false,
				greaterThanEndDate:true,
				dateHigherThanToday:true
			});
			$('#partnerName').rules('add', {
				dropdowncheck: false
			});
			$('#status').rules('add', {
				dropdowncheck: false
			});
			$("#dailyMilesStatusReport").validate().element('#fromDate');
			$("#dailyMilesStatusReport").validate().element('#toDate');
			$("#dailyMilesStatusReport").validate().element('#partnerName');
		}
		if($("#partnerName").hasClass('error')) {
			$("#partnerName").removeClass('error');
			$("#partnerName-error").html("");
		}
		if($("#status").hasClass('error')) {
			$("#status").removeClass('error');
			$("#status-error").html("");
		}
	}
});
</script>

<!-- Validation Methods -->
<script type="text/javascript">
$.validator.addMethod("dateHigherThanToday", function(value, element) {
	var startDate = getFormattedDate($('#fromDate').val());
	var endDate = getFormattedDate($('#toDate').val());
	
	if(endDate!='Invalid Date'){
		if (startDate <= endDate)
			return true;
		
		if (startDate >= endDate)
			return false;
	}
	else{
		return true;
	}
});

$.validator.addMethod("greaterThanEndDate", function(value, element) {
	var startDate = getFormattedDate($('#fromDate').val());
	var endDate = getFormattedDate($('#toDate').val());
	
	if(endDate!='Invalid Date'){
		if (endDate >= startDate)
			return true;
		
		if (endDate <= startDate)
			return false;
	}
	else{
		return true;
	}
});

$.validator.addMethod("dropdowncheck", function(value, element) {
	var jpNumber_ = $("#jpNumber").val();
	if(jpNumber_=="") {
		changeValidationRule();
	}
	if(value=="" && jpNumber_=="") {
		return false;
	} else
		return true;
});	

function changeValidationRule(){
	var flag = false;
	
	var fromDate_ = $("#fromDate").val();
	var toDate_   = $("#toDate").val();
	
	var partnerName_ = $("#partnerName").val();
	var status_ = $("#status").val();
	if(partnerName_ != "") {
		$("#partnerName-error").html("");
	}
	if(fromDate_ == "" || toDate_ == ""){
		flag = true;
	}
	
	$('#fromDate').rules('add', {
		required: flag,
		dateHigherThanToday : true
	});
	$('#toDate').rules('add', {
		required: flag,
		greaterThanEndDate:true,
		dateHigherThanToday:true
	});
	if(flag == true){
		$("#dailyMilesStatusReport").validate().element('#fromDate');
		$("#dailyMilesStatusReport").validate().element('#toDate');
	}
}
</script>

<!-- Methods -->
<script type="text/javascript">
var	datetext;
function getFormattedDate(date){
	var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
	return new Date(date.replace(pattern,'$3-$2-$1'));
}

var d = new Date(); // for now
var h = d.getHours();
h = (h < 10) ? ("0" + h) : h ;
var m = d.getMinutes();
m = (m < 10) ? ("0" + m) : m ;
var s = d.getSeconds();
s = (s < 10) ? ("0" + s) : s ;
var month = d.getMonth()+1;
var day = d.getDate();
var year = d.getFullYear();
datetext = day+"-"+month+"-"+year+"_"+h+":"+m+":"+s;

$('.display').removeAttr('width').DataTable({
	dom: 'Blfrtip',
	scrollX:        true,
	//scrollCollapse: true,
	"order": [[ 2, "desc" ]],
	columnDefs: [
	{ width: '243', targets: '_all' }
	],
	buttons: [
                 { extend: 'csv',   text:'export to csv',  title:'DilyMiles_Report_'+datetext},
                 { extend: 'excel', text:'export to excel',title:'DilyMiles_Report_'+datetext},
              ],
	fixedColumns: false
	});
	
</script>

	
