<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<div class='container'>
<section class="affliates_management">

<div id="breadcrumb">
  <ul>
	  <li>Home</li>
	  <li>Reports</li>
	  <li>Missing Transaction Report</li>
  </ul>
</div><!---breadcrumb--->

     <c:if test="${fn:length(message) gt 0}">
	       <script>changeURL();</script>
	         <div class="alertMessage" id="divChanged" style="color:green;font-weight: bold;">
	    <c:out value="${message}"></c:out>
	        </div> 
	 </c:if>


     <c:if test="${fn:length(errormessage) gt 0}">
	  	  <script>changeURL();</script>
	       	 <div class="alertMessage" style="color:red;font-weight: bold;">
	    <c:out value="${errormessage}"></c:out>
	        </div> 
	 </c:if>
    
    
    <div class="section_heading">
				<h5>Missing Transaction Report</h5>
	</div>

<div class="management">

<ul class="nav nav-tabs">
    <li id="missingTransreport"  class="active"><a data-toggle="tab" href="#sectionA">Report Of Missing Transactions.</a></li>
    <!-- <li id="" ><a data-toggle="tab" href="#sectionB"></a></li> -->
</ul>


<div class="tab-content">


<div id="sectionA" class="tab-pane fade in active">	
<form id="uploadMissingTrans" name="uploadMissingTrans" method="POST" action="MissingTransactionReport" enctype="multipart/form-data">         
<div class="row pad top">
<div class="col-md-3 upload_keyword">
            <br>
<label>Upload Missing Transaction File</label><span class="mandatory_star">*</span>
<div class="fileUpload btn ">						
<input name="uploadFile2" id="uploadFile2"  class="upload_file" placeholder=".csv file only" disabled="disabled" />
<span class="bg_w_button" style="position:relative;top:5px;">Browse</span>									
<input type="file" name="uploadMissing" id="uploadMissing" class="upload uploadfile_button"    />
<input type="hidden" id="filename" name="filename"  />
			<br>	
</div>
</div> 
<div class="col-md-1">
<p id="log" style="display:none;color:red;"></p>
<div class="fileUpload btn "  style="position:relative;top:16px;right:12px">
	                                	<!-- <input class="bg_g_button" type="submit"  id="UploadFile" value="Upload" /> -->
<button type="submit" class="bg_g_button" id="UploadFile" >Upload</button>
</div>
</div>
</div>
<br><br>
			
</form>	
</div><!--- row --->
</div><!-- tab content -->	
</div><!-- management -->
</section>  <!-- section management ends -->
</div><!-- div class container ends -->




<script type="text/javascript">


$(document).ready(function () {

	
	$("#uploadMissingTrans").validate({
		ignore:[],	
		rules: {
			uploadMissing:{
				required: true,
				extension: "csv"
			}
			},
		messages: {
			
			uploadMissing:{
				required: "Please upload csv file",
				extension: "File format shoud be .csv"
			}
		},
			errorElement: "div",
			errorPlacement: function(error, element) {
			if ($(element).hasClass("uploadBtn2")){
		    	error.insertAfter($(element).closest("div"));
			}
			else {
		    	error.insertAfter(element);
			}
			},
		submitHandler: function (form) {
			form.submit();
		},
	});
	
	
});




$('#UploadFile').on('change', function(){
	$('#UploadFile').val('');
	
});

$('#uploadMissing').on('change', function() {
	var filename = $(this).val();
	var	datetext;
	var d = new Date(); // for now
	var h = d.getHours();
	h = (h < 10) ? ("0" + h) : h ;
	var m = d.getMinutes();
	m = (m < 10) ? ("0" + m) : m ;
	var s = d.getSeconds();
	s = (s < 10) ? ("0" + s) : s ;
	var month = d.getMonth()+1;
	var day = d.getDate();
	var year = d.getFullYear();
	datetext = day+"-"+month+"-"+year+"_"+h+":"+m+":"+s;
	
	$('#uploadFile2').val($(this).val());
	$('#filename').val("Missing Transaction-"+datetext);
	/* $('#filename').val($(this).val()); */
	$("#uploadMissingTrans").validate().element('#uploadMissing');

});


</script>

