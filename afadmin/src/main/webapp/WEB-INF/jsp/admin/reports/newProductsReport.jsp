<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class='container'>
            <section class="affliates_management">
                <div id ="breadcrumb">
                    <ul>
                        <li>Home</li>
                        <li>Reports</li>
						<li>New Products Report</li>
                    </ul>
                </div>  <!---breadcrumb--->
				<div class="section_heading">
					<c:if test="${fn:length(message) gt 0}"><div class="alertMessage" style="color:green;font-weight: bold;">
					<c:out value="${message}"></c:out></div></c:if>
					<h5>New Products Report</h5> 
				</div>
				<form:form autocomplete="off" id="newProductsReport" commandName="newProductsReport">
				<div class="main_section bg-b-w ">
				<div class="row pad_top">
					<div class="col-md-3">
						<label>Start Date</label><span class="mandatory_star">*</span>
						<div class="pick_date">
							<form:input type="text" class="form-control startDate" id="fromDate" path="fromDate" readonly="true" />
						</div>
					</div>
					<div class="col-md-3">
						<label>End Date</label><span class="mandatory_star">*</span>
						<div class="pick_date">
							<form:input type="text" class="form-control endDate" id="toDate" path="toDate" readonly="true"/>
						</div>
					</div>
					<div class="col-md-3">
						<label>Brand Filter </label><span class="mandatory_star">*</span>
						<fieldset class="infield">
							<form:select path="brandName" id="brandName" class="select_option">
								<form:option value="">-Select Brand-</form:option>
								<c:forEach items="${brandList}" var="brand">
									<form:option value="${brand.brandName}">${brand.brandName}</form:option>
								</c:forEach>
							</form:select>
						</fieldset>
					</div>
					<div class="col-md-3">
						<label>Partner Filter </label><span class="mandatory_star">*</span>
						<fieldset class="infield">
							<form:select id="partnerName" path="partnerName" class="select_option">
								<form:option value="">-Select Partner-</form:option>
								<<c:forEach items="${partnerList}" var="partner">
									<form:option value="${partner.prtNo}">${partner.partnerName}</form:option>
								</c:forEach>
							</form:select>
						</fieldset>
					</div>
				</div>
				<div class="row pad_top export_btn">
					<div class="col-md-2 report_button">
						<input class="bg_w_button" type="button" onClick="$(this).closest('form').submit();" value="Search" /> <input class="bg_w_button" type="button" id="clear" value="Clear" />
					</div>
				</div>
				<!--- row ------>
			</div>
			<c:if test="${! empty newProductsReportlist }">
				<div class="report_table">
				<!-- <div class="expot_btn">
					<label>Export</label> <input class="bg_w_button" type="button" value="XLS" />
				</div> -->
					<table id="new_Products_Report" class="display" cellspacing="0" width="100%">
					    <thead>
					        <tr>
					            <th>Partner Name</th>
					            <th>Brand</th>
					            <th>Product ID</th>
					            <th>Product Name</th>
					            <th>MRP(INR)</th>
					            <th>Product Landing URL</th>
					            <th>Product Image URL</th>
					            <th>JPPL Category</th>
					        </tr>
					    </thead><!--- thead ---->
					    <tbody>    
					    	<c:forEach items="${newProductsReportlist}" var="newProductsReport">
								<tr>
									<td>${newProductsReport.partnerName}</td>
									<td>${newProductsReport.brandName}</td>
									<td>${newProductsReport.productID}</td>
									<td>${newProductsReport.productName}</td>
									<td>${newProductsReport.price}</td>
									<td>${newProductsReport.productLandingURL}</td>
									<td>${newProductsReport.productImageURL}</td>
									<td>${newProductsReport.categoryName}</td>
								</tr>
							</c:forEach>
						</tbody><!--- tbody ---->
					</table><!--- table ---->
				</div><!--- report table ---->
				</c:if>
		</form:form>
            </section>
         </div>
         
         
<!-- variables          -->
<script type="text/javascript">
</script>

<!-- Form Validation -->
<script type="text/javascript">
$(document).ready(function () {
	$("#newProductsReport").validate({
		onkeyup: true,
		ignore:[],
		rules: {	
			fromDate:{
				required : true,
				dateHigherThanToday : true
			},
			toDate:{
				required : true,
// 				greaterThanEndDate:true,
				dateHigherThanToday:true
			},
			partnerName:{
				dropdowncheck: true
			},
			brandName:{
				dropdowncheck: true
			}
		},
		messages: {
			fromDate:{
				required: "Start date cannot be empty",
				dateHigherThanToday:"Start date must be greater than or equal End date "
			},
			toDate:{
				required: "End date cannot be empty",
// 				greaterThanEndDate:"End date must be greater than or equal to Start date ",
				dateHigherThanToday:"End date must be lesser than or equal Start date "
			},
			partnerName:{
				dropdowncheck: "Please select a Partner"
			},
			brandName:{
				dropdowncheck: "Please select a Brand"
			}
		},
		errorElement: "div",
		errorPlacement: function(error, element) {
			if ($(element).hasClass("form-control startDate")) {
				error.insertAfter($(element).closest("div"));
			}
			else if ($(element).hasClass("form-control endDate")){
				error.insertAfter($(element).closest("div"));
			}
			else {
				error.insertAfter(element);
			}
		},
		submitHandler: function (form) {
			form.submit();
		}
	});
});
</script>

<!-- Events -->
<script type="text/javascript">
$("#clear").on('click', function () {
	var validator = $( "#newProductsReport" ).validate();
	validator.resetForm();
	validator.reset();
	
	$("#fromDate").val("");
	$("#toDate").val("");
	$("#brandName").val("");
	$("#partnerName").val("");
	$('#brandName').selectmenu("refresh");
	$('#partnerName').selectmenu("refresh");
});

$( "#fromDate" ).on('change', function() {
	$("#newProductsReport").validate().element("#fromDate");
	var endDate = getFormattedDate($('#toDate').val());
	
	if(endDate!='Invalid Date'){
		$("#newProductsReport").validate().element("#toDate");
	}
});

$( "#toDate" ).on('change', function() {
	$("#newProductsReport").validate().element("#fromDate");
	$("#newProductsReport").validate().element("#toDate");
});

$('#partnerName').on('selectmenuchange', function () {
	$("#newProductsReport").validate().element('#partnerName');
});
$('#brandName').on('selectmenuchange', function () {
	$("#newProductsReport").validate().element('#brandName');
});

</script>

<!-- Validation Methods -->
<script type="text/javascript">
$.validator.addMethod("dateHigherThanToday", function(value, element) {
	var startDate = getFormattedDate($('#fromDate').val());
	var endDate = getFormattedDate($('#toDate').val());
	
	if(endDate!='Invalid Date'){
		if (startDate < endDate) {
			return false;
		}
		
		if (startDate >= endDate) {
			return true;
		}
	}
	else{
		return true;
	}
});

$.validator.addMethod("greaterThanEndDate", function(value, element) {
	var startDate = getFormattedDate($('#fromDate').val());
	var endDate = getFormattedDate($('#toDate').val());
	
	if(endDate!='Invalid Date'){
		if (endDate >= startDate)
			return true;
		
		if (endDate <= startDate)
			return false;
	}
	else{
		return true;
	}
});

$.validator.addMethod("dropdowncheck", function(value, element) {
	if(value=="") {
		return false;
	} else
		return true;
});

</script>

<!-- Methods -->
<script type="text/javascript">
$( "#fromDate" ).datepicker({
    dateFormat: 'dd/mm/yy',
    minDate: "today",
    maxDate: 'today',
	showOn: "button",
	buttonImage: "./static/img/2016-06-23.png",
	buttonImageOnly: true,
	buttonText: "Select date"
});

$( "#toDate" ).datepicker({
    dateFormat: 'dd/mm/yy',
    minDate :"today-30",
	showOn: "button",
	maxDate: '0',
	buttonImage: "./static/img/2016-06-23.png",
	buttonImageOnly: true,
	buttonText: "Select date"

});
function getFormattedDate(date){
	var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
	return new Date(date.replace(pattern,'$3-$2-$1'));
}

var d = new Date(); // for now
var h = d.getHours();
h = (h < 10) ? ("0" + h) : h ;
var m = d.getMinutes();
m = (m < 10) ? ("0" + m) : m ;
var s = d.getSeconds();
s = (s < 10) ? ("0" + s) : s ;
var month = d.getMonth()+1;
var day = d.getDate();
var year = d.getFullYear();
datetext = day+"-"+month+"-"+year+"_"+h+":"+m+":"+s;

$('.display').removeAttr('width').DataTable({
	dom: 'Blfrtip',
	scrollX:        true,
	"searching": false,
	scrollCollapse: true,
	columnDefs: [
			{ width: '243', targets: '_all' }
	],
	buttons: [
                 { extend: 'excel', text:'export to excel',title:'new_Products_Report_'+datetext},
              ],
	fixedColumns: false
	});

</script>
         
         