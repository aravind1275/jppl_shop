<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<c:set var="contextPath" value="${pageContext.servletContext.contextPath}"  scope="application"/>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="${contextPath}/static/img/favicon.ico" rel="shortcut icon" type="image/ico"> 
    <title>AFFILIATE ADMIN</title>
    <!-- Custom CSS -->
	<link href="${contextPath}/static/css/main.css" rel="stylesheet">
	<script>
		var ctx = "${pageContext.request.contextPath}";
    </script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn"t work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<div class="login_wrap">
		<div class="login-holder">
			<div class="login">
				<div class="logo"><img src="${contextPath}/static/img/jetprivilege_logo.png" alt=""></div>				
				<form:form  autocomplete="off" id="adminUserBean" class="form-signin" commandName="adminUserBean">
					<h2>Admin Login</h2>
					<div class="alertMessage" style="color:red;font-weight: bold;"  ><c:out value="${errorMsg}"></c:out></div> 
					<div class="alertMessage" style="color:green;font-weight: bold;"><c:out value="${successMessage}"></c:out></div> 
					<label>Username</label>
					<form:input path="userName"  class="form-control "/>
					<label>Password</label>
					<form:password path="password"  class="form-control "/>
					<p>
						<span class="text-left">
						<a class="text-left" href="${contextPath}/admin/adminForgotPassword">Forgot Password?</a>
						</span>
						<span class="text-right">
							<a class="btn bg-b" onClick="$(this).closest('form').submit();">Login</a>
						</span>
					</p>
				</form:form>
			</div><!-- login -->
        </div>
	</div> <!-- login_wrap -->	
    <!-- jQuery -->
	<script src="${contextPath}/static/js/integrated.js"></script>	
	<script type="text/javascript" src="${contextPath}/static/js/jquery.validate.min.js"></script>
 	<script src="${contextPath}/static/js/additional-methods.min.js"></script>
</body>



<script type="text/javascript">
$(document).ready(function () {

	$.validator.addMethod('passwordChecking', function (value,element) { 
		return this.optional(element) || /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+])[A-Za-z\d][A-Za-z\d!@#$%^&*()_+]{8,15}$/.test(value);	
	});

	 /* for jquery validation*/
	 $("#adminUserBean").validate({
		 rules: {
			 userName:"required",
			 password:{
				 required:true,
				// passwordChecking:true
				 },
	        },
	        messages: {
	        	userName: "Please enter User Name/Email ID",
	        	password :{
		        	required :"Please enter password",
		        	//passwordChecking : 'Minimum 8 and Maximum 15 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character:'
		        }
	        },
	        errorElement: "div",
	        submitHandler: function (form) {
	               form.submit();
	        }
		 
	 });

	 /* for submit the form on key press enter*/
	 $(document).keypress(function(event){
		    if(event.keyCode == 13){
			    $("#adminUserBean").submit();
		    }
		});
	
});

</script>
</html>









