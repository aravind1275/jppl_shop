<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script>
	function changeURL() {
		history.pushState({}, '', "${contextPath}/admin/notification");
	}
</script>

<!-- Modal content for delete -->
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title">Are You Sure?</h4>
			</div>
			<!--header-->
			<div class="modal-body">
				<p>
					<img src="${contextPath}/static/img/info_icon1.png"></img> <span
						id="deletenotificationId" class="bannerNameClass"> Notification will be
						deleted.Are you sure?</span>
				</p>
			</div>
			<!---body--->
			<div class="modal-footer">
				<button id="btnD" class="bg_g_button" data-dismiss="modal">Delete</button>
			</div>
		</div>
		<!---modal content--->
	</div>
</div>



<!-- <!-- Modal content for image details----------------------------------->
<div id="myModal1_" class="modal fade" role="dialog">
	<div class="modal-dialog">
		Modal content for view details
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title">Image Details</h4>
			</div>
			
			<div class="modal-body2">

			
				<div class="row">
					<div class='col-md-6 user_details'>
						<label>Notification Image</label> <img src='' id="notificationimage_"
							
							height="300px" width="448px" />
						<p id="notificationimageurl"></p>
					</div>
				</div>
			</div>
				</div>
	</div>
</div>

<!-- Modal  for edit-Notification-details-->
 <div id="myModal1" class="modal fade" role="dialog">
	<form:form autocomplete="off" id="editnotificationBean" commandName="editnotificationBean">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" id="cancelbtnD1_" class="close"
						data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Edit Notification Details</h4>
				</div>
				<!----modal-header--->
				<div class="modal-body1">
					<div class="row pad_top">
						<div class="col-md-6 user_details">
							<label>Partners</label>
							<select name="editPartNo" id="editPartNo" class="select_option">
								<option value="">--Select--</option>
								<c:forEach items="${partnerList}" var="partner">
									<li>
										<option value="${partner.prtNo}">${partner.partnerName}</option>
									</li>
								</c:forEach>
							</select>
						</div>

						<div class="col-md-6 user_details">
							<label>Notification ID</label>
							<input type="text" class="form-control " name="edit_notificationid" maxlength="50" id="edit_notificationid">
							<input type="hidden" id="edit_notificationidHidden" />
						</div>
					</div>
					<!----row--->
					<div class="row pad_top">
						<div class="col-md-6 user_details">
							<label>Notification Name</label>
							<input type="text" class="form-control " name="edit_notificationname" maxlength="100" id="edit_notificationname">
						</div>
						

						<div class="col-md-6 user_details">
							<label>Landing URL</label>
							<input type="text" class="form-control " name="edit_notificationlandingurl" id="edit_notificationlandingurl">
						</div>
					</div>
					<div class="row pad_top">
					<div class="col-md-6 user_details">
							<label>Notification Text</label>
							<!-- <input type="text" class="form-control " name="edit_notificationtext" maxlength="100" id="edit_notificationtext"> -->
							<%--  <form:textarea id="edit_notificationtext" name="edit_notificationtext" path="notificationText" cols="40" class="form-control text" readonly="true" maxlength="251"/> --%>
							 <textarea id="edit_notificationtext" cols="40" name="edit_notificationtext" class="form-control text" readonly="true" maxlength="251"></textarea>
						</div>
						<div class="col-md-6 user_details">
							<label>Notification Title</label>
							<input type="text" class="form-control " name="edit_notificationtitle" maxlength="100" id="edit_notificationtitle">
						</div>
					</div>
					

					<div class="row pad_top">
						<div class="col-md-6 browse_btn">
							<label>Notification Image</label><br> <input
								id="edit_notificationimageurl" class="upload_file upload_file_style"
								placeholder="Choose File" disabled="disabled" /> <input
								type="hidden" id="edit_notificationimageurlHidden" />
							<div class="fileUpload btn padTop_Btm">
								<span class="bg_w_button">Browse</span> <input
									name="edit_notificationimage" id="edit_notificationimage" type="file"
									class="upload upload_btn_modal editBannerImageUpload bwrsBtnAddPar" />
							</div>
						</div>

						<!-- <div class="col-md-6 image_wrap">
								 <img id="edit_notificationimage_" width="283px" height="107px"  src="" style="margin-top: 10px;">
							</div> -->

						<div class="col-md-4 image_wrap image_wrap1"
							style="width: 250px; height: 115px; margin-top: 10px;">
							<img id="edit_notificationimage_"  src=""
								style="max-height: 100%; max-width: 100%;">
						</div>
						<div class="col-md-1 mar_top">
							<input class="bg_g_button" type="button" value="Upload"
								onclick="editUploadImage('edit_notificationimage','notification','edit_notificationimageurl')" />
							<input type="hidden" name="editNotificationImageUrl"
								id="editNotificationImageUrl" />
							<div id="editBannerError" class="customErrorMessageDisp error"></div>
						</div>
					</div>
					<!------------- row ----------->
					<div class="row pad_top">
						<div class="col-md-6 user_details">
							<label>notification Status</label>
							<div class="half-col last status">
								<ul class="list-inline">
									<c:forEach items="${notificationStatus}" var="status">
										<li><label> <input type="radio"
												name="edit_notificationstatus" class="css-checkbox"
												id="edit_notificationstatus_${status.key}" value="${status.key}" />
												<label for="edit_notificationstatus_${status.key}"
												class="css-label radGroup2">${status.value}</label>
										</label></li>
									</c:forEach>
								</ul>
							</div>
						</div>
					</div>
					<div class="row pad_top">
							<div class="col-md-4 user_details pad_top">
							 <label for="edit_loggedInNotification">Logged In Status</label>
                              <div>  <input type="checkbox" class="allow_checkbox" value="0" name="edit_loggedInNotification" id="edit_loggedInNotification" /></div>
                            </div>
                            <div class="col-md-4 user_details pad_top">
                            <label for="edit_nonLoggedInNotification">Non Logged In Status</label>
                             <div><input type="checkbox" class="allow_checkbox" value="0" name="edit_nonLoggedInNotification" id="edit_nonLoggedInNotification" /></div>
                                
                            </div>
                        </div>
					<!-- Added By Pawan -->
					<div class="row pad_top">
						<div class="col-md-4 user_details">
							<label>Start Date</label>
							 <div class="pick_date">
							 	<input type="text" class="form-control datepicker" id="editFromDate" name="editFromDate" readonly="readonly"/>
							</div> 
						</div>
						<div class="col-md-4 user_details">
							<label>End Date</label>
						   	<div class="pick_date">
								<input  type="text" class="form-control datepicker" id="editToDate" name="editToDate" readonly="readonly" />
							</div>
						</div>
						<div class="col-md-4 user_details">
								<label>Timestamp of notification</label>
								<input type="text" class="form-control" name="edit_timeStamp" id="edit_timeStamp" placeholder="hh:mm:ss" />
							</div>
					</div>
				  </div>
				<div class="row btn_save align_right">
					<input class="bg_g_button" type="button" value="Update" id="updateNotification"/>
				</div>
				<!-------------button----------->
			</div>
		</div>
	</form:form>
</div> 

<!-- Modal content for view details Notification management----------------------------------->
 <div id="myModal2" class="modal fade" role="dialog">
	<div class="modal-dialog">
		Modal content for view details
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title">Notification Details</h4>
			</div>
			
			<div class="modal-body2">
				<div class="row">
					<div class='col-md-6 user_details'>
						<label>Notification ID</label>
						<p id="notificationid" class="notificationNameClass"></p>
					</div>
					<div class='col-md-6 user_details'>
						<label>Notification Name</label>
						<p id="notificationname" class="notificationNameClass"></p>
					</div>
				</div>
				<div class="row">
					<div class='col-md-6 user_details '>
						<label>Notification Text</label>
						<p id="notificationtext"></p>
					</div>
					<div class='col-md-6 user_details '>
						<label>Notification Title</label>
						<p id="notificationtitle"></p>
					</div>
					
				</div>
				<!----row--->
				<div class="row user_details image_wrap_modal">
					<label style="">Notification Image</label> <img src='' id="notificationimage"
						height="107px" width="430px" />
					<p id="notificationimageurl" class="notificationNameClass"></p>

				</div>
				<!----row--->
				<div class="row">
					<div class='col-md-6 user_details'>
						<label>Landing URL</label>
						<p id="notificationlandingurl" class="notificationNameClass"></p>
					</div>
					<div class='col-md-6 user_details'>
						<label>Notification Status</label>
						<p id="notificationstatus"></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> 

<div class='container'>
	<section class="insurance_management">
		<div id="breadcrumb">
			<ul>
				<li>Home</li>
				<li>Configuration</li>
				<li>Notification</li>
			</ul>
		</div>
		<!---breadcrumb--->

		<c:if test="${fn:length(message) gt 0}">
			<script>
				changeURL();
			</script>
			<div class="alertMessage" style="color: green; font-weight: bold;">
				<c:out value="${message}"></c:out>
			</div>
		</c:if>
		<c:if test="${fn:length(errormessage) gt 0}">
			<script>
				changeURL();
			</script>
			<div class="alertMessage" style="color: red; font-weight: bold;">
				<c:out value="${errormessage}"></c:out>
			</div>
		</c:if>

		<div class="section_heading">
			<h5>Notification</h5>
		</div>
		<!--- section heading ---->
		<div class="management">
			<!---User Management--->
			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#sectionA">Existing
						Notification</a></li>
				<li><a data-toggle="tab" href="#sectionB">Create Notification</a></li>
			</ul>
			<div class="tab-content">
				<div id="sectionA" class="tab-pane fade in active">
					<!--- existing banner starts here --->
					<nav class="navbar navbar-default" role="navigation" id="submenu">
						
						<c:if test="${not empty notificationList }">
							<div class="Static_table">
								<h5 class="table_heading">Notification List</h5>
								<!--- table header --->
								<table class=" partner_info_table table-bordered"
									cellspacing="0" cellpadding="0" width="98%" id="table-2">
									<thead>
										<tr>
											<th class="name">Sl No.</th>
											<th>Notifcation ID</th>
											<th>Notifcation Name</th>
											<!-- <th>Notifcation Text</th> -->
											<th>Notifcation Title</th>
											<th>Notifcation Image </th>
											<th>Notifcation Landing URL</th>
											<th>Notifcation Status</th>
											<th class="width_section">Action</th>
										</tr>
									</thead>
									<!----thead--->
									<tbody>
										<c:forEach items="${notificationList}" var="notification" varStatus="i">
											<tr class="admin" id="2.${notification.notificationNo}"
												data-id="${notification.notificationNo}"
												data-notificationid="${notification.notificationId}"
												data-notificationname="${notification.notificationName}"
												data-notificationtext="${notification.notificationText}"
												data-notificationtitle="${notification.notificationTitle}"
												data-notificationimageurl="${notification.notificationImageUrl}"
												data-notificationlandingurl="${notification.notificationLandingUrl}"
												data-notificationstatus="${notification.notificationStatus}"
												data-notificationstatuscode="${notification.notificationStatusCode}"
												data-notificationstatusclass="${notification.notificationStatusClass}"
												data-notificationprtno="${notification.partNo}"
												data-notificationprtname="${notification.partnerName}"
												data-loggedinnotification="${notification.loggedInStatus}"
									            data-nonloggedinnotification="${notification.nonLoggedInStatus}"
												data-timestamp="${notification.timeStamp}"
												data-editfromdate="${notification.fromDate}"
												data-edittodate="${notification.toDate}">
                                                
												<td class="slno_width">${i.count}</td>
												<td class="td_width">${notification.notificationId}</td>
												<td class="td_width">${notification.notificationName}</td>
												<!-- TODO:AW-321 -->
												<%-- <td>${notification.notificationText}</td> --%>
												<td>${notification.notificationTitle}</td>
												<td class="td_width">${notification.notificationImageUrl}</td>
												<td class="td_width">${notification.notificationLandingUrl}</td>
												<td class="${notification.notificationStatusClass}">${notification.notificationStatus}</td>
												<td class="action_image"><span href="javascript:void(0);" class="view_details" data-target="#static_banner_modal">
												<img src="${contextPath}/static/img/View_details_icon1.png"></img></span>
													<span class="editNotification" data-target="#myModal1"> <img src="${contextPath}/static/img/edit_icon.png"class="edit_wraper"></img>
												</span> <span class="delete btnDelete" data-target="#myModal" id="deleteNotification"> <img src="${contextPath}/static/img/delete_icon1.png" class="fa fa-trash fa-lg"></img>
												</span></td>
											</tr>
										</c:forEach>
									</tbody>
									<!---tbody--->
								</table>
								<!---table-->
								
								<!-------------button----------->
							</div>
						</c:if>
					</nav>
				</div>
				<!----section A---->

				<!-----create new notification here---->
				<div id="sectionB" class="tab-pane fade submenu">
					<form:form autocomplete="off" id="notificationBean"
						commandName="notificationBean" enctype="multipart/form-data">
						<div class="row pad_top">

							<div class="col-md-4">
								<label for="country">Partners</label>
								<form:select path="partNo" name="partNo" id="partNo"
									class="select_option">
									<form:option value="">--Select--</form:option>
									<c:forEach items="${partnerList}" var="partner">
										<li><form:option value="${partner.prtNo}"
												label="${partner.partnerName}" /></li>
									</c:forEach>
								</form:select>
							</div>

							<div class="col-md-4">
								<label>Notification ID</label><span class="mandatory_star">*</span>
								<form:input path="notificationId" id="notificationId" name="notificationId"
									maxlength="50" class="form-control" />
							</div>
							<div class="col-md-4">
								<label>Notification Name</label><span class="mandatory_star">*</span>
								<form:input path="notificationName" maxlength="100"
									class="form-control" />
							</div>
							<div class="col-md-4">
								<label>Notification Text</label><span class="mandatory_star">*</span>
								<%-- <form:input path="notificationText" maxlength="250"
									class="form-control" /> --%>
									  <form:textarea id="notificationText" name="notificationText" path="notificationText" cols="40" class="form-control text" readonly="true" maxlength="251"/>
							</div>
							<div class="col-md-4 user_details">
							<label>Notification Title</label><span class="mandatory_star">*</span>
							<form:input path="notificationTitle" maxlength="100"
									class="form-control" />
						  </div>
							<div class="row pad_top">
							<div class="col-md-4 user_details pad_top">
							 <label for="loggedInStatus">Logged In Status</label>
                              <div>  <input type="checkbox" class="allow_checkbox" value="0" name="loggedInStatus" id="loggedInStatus" /></div>
                            </div>
                            <div class="col-md-4 user_details pad_top">
                            <label for="nonLoggedInStatus">Non Logged In Status</label>
                             <div><input type="checkbox" class="allow_checkbox" value="0" name="nonLoggedInStatus" id="nonLoggedInStatus" /></div>
                                
                            </div>
                        </div>
						</div>
						<!--- row --->

						<div class="row pad_top">
							<div class="col-md-4">
								<label>Landing URL</label><!-- <span class="mandatory_star">*</span> -->
								<form:input path="notificationLandingUrl" class="form-control" />
							</div>
								<div class="col-md-4">
								<div class='text top'>
									<label>Notification Status</label><span class="mandatory_star">*</span>

									<div class="half-col last">
										<ul class="list-inline">
											<c:forEach items="${notificationStatus}" var="status">
												<li><label>
												 <form:radiobutton path="notificationStatusCode" class="css-checkbox" id="${status.key}" value="${status.key}" />
												  <label for="${status.key}" class="css-label radGroup2">${status.value}</label>
												</label></li>
											</c:forEach>
										</ul>
									</div>
								</div>
							</div>
							
						</div>
						<!--- row --->

						<div class="row pad_top">
							<div class="col-md-4">
								<label>Notification Image</label><span class="mandatory_star">*</span><br>
								<input id="uploadFile" class="upload_file"
									placeholder="Choose File" disabled="disabled" />
								<div class="fileUpload btn padTop_Btm">
									<span class="bg_w_button">Browse</span>
									<!-- TODO: notification image placement -->
									<input id="notificationImage" name="notificationImage" type="file"
										class="upload newBannerImageUpload bwrsBtnAddPar" />
								</div>
							</div>
							<!---- browse button --->

							

							<div class="col-md-4 image_wrap image_wrap1 noMar-top">
								<img id="notificationImage_" src=""
									style="max-height: 100%; max-width: 100%;" />
							</div>
							 <div class="col-md-4">
								<input class="bg_g_button" type="button" value="Upload"
									onclick="uploadImage('notificationImage','notification','notificationImageUrl')" />
								<form:hidden path="notificationImageUrl" id="notificationImageUrl" />
								<div id="newBannerError" class="customErrorMessageDisp error"></div>
							</div> 
						</div>
						<!------------- row ----------->
						<div class="row pad_top pad_bottom">
							
							
							
							<!--- radio button ---->
							<div class="col-md-4">
								<label>Start Date</label><span class="mandatory_star">*</span>
								<div class="pick_date">
									<form:input  type="text" class="form-control startDate" id="fromDate" path="fromDate" name="fromDate" readonly="readonly"/>
								</div>
							</div>
							<div class="col-md-4">
								<label>End Date</label><span class="mandatory_star">*</span>
								<div class="pick_date">
									<form:input  type="text" class="form-control endDate" id="toDate" path="toDate" name="toDate" readonly="readonly" />
								</div>
							</div>
							<div class="col-md-4">
							<label>Timestamp of notification Trigger</label> <span class="mandatory_star">*</span>
							<form:input path="timeStamp" name="timeStamp" id="timeStamp" placeholder="hh:mm:ss" class="form-control" autocomplete="off"/>
						</div>
						</div>
						<div class="row btn_save pull-right">
							<input class="bg_g_button" type="button" id="cancelbtnD"
								value="Cancel" /></a>
							<button type="button" class="bg_g_button " onClick="$(this).closest('form').submit(); checkValidationMessage();">Save</button>
						</div>
						<!------ button ------>
					</form:form>
				</div> 
				<!----section B---->
			</div>
			<!-----tab-content---->
		</div>
		<!---- Notification managemnt----->
	</section>
	<!----- section ---->
</div>
<!---- container ---->

<script type="text/javascript">
	//Variables
	var notificationIDExist = false;
	var userNameExist = false;
	var uploadFlag = false;
	var notificationEditFlag = false;
	var uploadclick = true;
	var notificationText;
	
	
	$(document).ready(function() {
		$('.urlMand').hide();
		$('#fromDate').datepicker({
			  minDate: new Date()
			 });
		
		tinymce.init({
			selector: 'textarea#notificationText',
			height: '100px',
			theme: 'modern',
		
			plugins: [
				'advlist autolink lists link image charmap print preview hr anchor pagebreak',
				'searchreplace wordcount visualblocks visualchars code fullscreen',
				'insertdatetime media nonbreaking save table contextmenu directionality',
				'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
			],
			toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image forecolor backcolor | fontsizeselect',
			image_advtab: true,
			templates: [
				{ title: 'Test template 1', content: 'Test 1' },
				{ title: 'Test template 2', content: 'Test 2' }
			],
			content_css: [
				'//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
				'//www.tinymce.com/css/codepen.min.css'
			],
			force_br_newlines : true,
			force_p_newlines : false,
			forced_root_block : '',
			fontsize_formats: "8px 10px 12px 14px 18px 24px 36px",
			setup : function(ed){
				ed.on("NodeChange", function(e){
					jQuery("textarea[id='notificationText']").val(ed.getContent());
					
				});
				
			}
		}); 
		
		jQuery.extend( jQuery.fn.dataTableExt.oSort, {
		    "non-empty-string-asc": function (str1, str2) {
		    	str1 = Number($.trim(str1));
		    	str2 = Number($.trim(str2));
		        if(str1 == null || str1 == "")
		            return 1;
		        if(str1 == null || str2 == "")
		            return -1;
		        return ((str1 < str2) ? -1 : ((str1 > str2) ? 1 : 0));
		    },
		    "non-empty-string-desc": function (str1, str2) {
		    	str1 = Number($.trim(str1));
		    	str2 = Number($.trim(str2));
		        if(str1 == null || str1 == "")
		            return 1;
		        if(str1 == null || str2 == "")
		            return -1;
		        return ((str1 < str2) ? 1 : ((str1 > str2) ? -1 : 0));
		    }
		});

		$('.partner_info_table').DataTable( {
		     "paging": true,
		     "searching": false,
		     "lengthChange": false,
		     "info":false,
		     "iDisplayLength" : 100,
		     "order": [[ 0, "asc" ]],
		     columnDefs: [
		          {type: 'non-empty-string', targets: 0} // define 'name' column as non-empty-string type
		     ],
		 });
		
		
		$("#notificationBean").validate({
			onkeyup : true,
			ignore : [],
			rules : {
				notificationId : {
					required : true,
					duplicateNotificationId : true,
					rangelength : [ 1, 50 ],
					AlphaNumeric : true,
					noSpace:true,
					
				},
				notificationName : {
					required : true,
					rangelength : [ 1, 100 ],
					AlphaNumeric : true,
					noSpace:true
				},
				notificationText : {
					required : true,
					rangelength : [ 1, 250 ],
					noSpace:true
				},
				notificationTitle : {
					required : true,
					rangelength : [ 1, 255 ],
					AlphaNumeric : true,
					noSpace:true
				},
				notificationImage : {
					extension : "png|jpg|jpeg|gif",
					imageDimention : true,
				},
				notificationImageUrl : {
					required : true,
					rangelength : [ 1, 250 ]
				},
				notificationLandingUrl : {
					rangelength : [ 1, 2000 ],
					url : true
				},
				fromDate : {
					required : true,
					dateHigherThanToday : true
				},
				
				toDate : {
					required : true,
				},
				timeStamp : {
					required : true,
				}
			},
			messages : {
				notificationId : {
					required : "Notification Id field should not be empty",
					rangelength : "Maximum 50 characters permitted",
					AlphaNumeric : "Special characters are not allowed",
					noSpace : "Please enter a valid Notification Id",
					duplicateNotificationId : "NotificationID already exist",
					
				},
				notificationName : {
					required : "Notification Name field should not be empty",
					rangelength : "Maximum 100 characters permitted",
					AlphaNumeric : "Special characters are not allowed",
					noSpace : "Please enter a valid Notification Name"
				},
				notificationText : {
					required : "Notification Text field should not be empty",
					rangelength : "Maximum 100 characters permitted",
					noSpace : "Please enter a valid Notification Text"
				},
				notificationTitle : {
					required : "Notification Title field should not be empty",
					rangelength : "Maximum 100 characters permitted",
					AlphaNumeric : "Special characters are not allowed",
					noSpace : "Please enter a valid Notification Title"
				},
				notificationImage : {
					extension : "Invalid file format. Only jpg|jpeg|png|gif files are allowed."
				},
				notificationImageUrl : {
					required : "Please Upload Notification Image",
					rangelength : "Maximum 100 characters permitted"
				},
				
				fromDate : {
					required : "Please Select Start Date",
					dateHigherThanToday : "Start date must be lesser than or equal End date"
				},
				toDate : {
					required : "Please Select End Date",
				},
				timeStamp : {
					required : "Please enter timestamp."
				}
			},
			errorElement : "div",
			errorPlacement : function(error, element) {
				if ($(element).hasClass("newNotificationImageUpload")) {
					error.insertAfter($(element).closest("div"));
				} else if ($(element).hasClass("form-control startDate")) {
					error.insertAfter($(element).closest("div"));
				} else if ($(element).hasClass("form-control endDate")) {
					error.insertAfter($(element).closest("div"));
				} else {
					error.insertAfter(element);
				}
			},
			submitHandler : function(form) {
				form.submit();
			}
		});
	});
	
	$(document).ready(function() {
		$("#editnotificationBean").validate({
			ignore : [],
			rules : {
				edit_notificationid : {
					required : true,
					rangelength : [ 1, 50 ],
					duplicateNotificationId : true,
					AlphaNumeric : true,
					
					noSpace:true
				},
				edit_notificationname : {
					required : true,
					rangelength : [ 1, 100 ],
					AlphaNumeric : true,
					noSpace:true
				},
				edit_notificationtext : {
					required : true,
					rangelength : [ 1, 250 ],
					noSpace:true
				},
				edit_notificationtitle : {
					required : true,
					rangelength : [ 1, 255 ],
					AlphaNumeric : true,
					noSpace:true
				},
				edit_notificationimage : {
					extension : "jpg|jpeg|png|gif",
					editImageDimention : true,
					
				},
				 edit_notificationlandingurl : {
					rangelength : [ 1, 2000 ],
					url : true
				}, 
				editFromDate : {
					required : true,
					editDateHigherThanToday : true
				},
				editToDate : {
					required : true,
				},
				edit_timeStamp : {
					required : true,
				}
			},
			messages : {
				edit_notificationid : {
					required : "Notification Id field should not be empty",
					rangelength : "Maximum 50 characters permitted",
					AlphaNumeric : "Special characters are not allowed",
					duplicateNotificationId : "NotificationID already exist",
					noSpace : "Please enter a valid Notification id",
				},
				edit_notificationname : {
					required : "Notification Name field should not be empty",
					rangelength : "Maximum 100 characters permitted",
					AlphaNumeric : "Special characters are not allowed",
					noSpace : "Please enter a valid Notification Name"
				},
				edit_notificationtext : {
					required : "Notification Name field should not be empty",
					rangelength : "Maximum 100 characters permitted",
					noSpace : "Please enter a valid Notification Text"
				},
				edit_notificationtitle : {
					required : "Notification Title field should not be empty",
					rangelength : "Maximum 100 characters permitted",
					AlphaNumeric : "Special characters are not allowed",
					noSpace : "Please enter a valid Notification Title"
				},
				edit_notificationimage : {
					extension : "Invalid file format. Only jpg|jpeg|png|gif files are allowed."
				},
				
				editFromDate : {
					required : "Please Select Start Date",
					editDateHigherThanToday : "Start date must be lesser than or equal End date"
				},
				editToDate : {
					required : "Please Select End Date",
				},
				edit_timeStamp : {
					required : "Please enter timestamp."
				}
			},
			errorElement : "div",
			errorPlacement : function(error, element) {
				 if ($(element).hasClass("editBannerImageUpload")) {
					error.insertAfter($(element).closest("div"));
				} else if ($(element).hasClass("datepicker")) {
					error.insertAfter($(element).closest("div"));
				} else {
					error.insertAfter(element);
				}
			},
			submitHandler : function(form) {
				form.submit();
			},
		});
	});
</script>

<script type="text/javascript">
	//Events

	$('a.biurl').on('click', function() {
		var id = $(this).closest('tr').data('id');
		var notificationimageurl = $(this).closest('tr').data('notificationimageurl');
		var imageUrl = '${contextPath}' + '/admin/images/notification/' + notificationimageurl;

		$('#notificationimage_').prop('src', imageUrl);
		$('#myModal1_').data('id', id).modal('show');
	});

	$('span.view_details').on('click', function() {
						var id = $(this).closest('tr').data('id');
						var notificationid = $(this).closest('tr').data('notificationid');
						var notificationname = $(this).closest('tr').data(
								'notificationname');
						var notificationtext = $(this).closest('tr').data(
								'notificationtext');
						var notificationtitle = $(this).closest('tr').data(
						'notificationtitle');
						
						var notificationimageurl = $(this).closest('tr').data(
								'notificationimageurl');
						var notificationlandingurl = $(this).closest('tr').data(
								'notificationlandingurl');
						var notificationstatus = $(this).closest('tr').data(
								'notificationstatus');
						var notificationstatuscode = $(this).closest('tr').data(
								'notificationstatuscode');
						var notificationstatusclass = $(this).closest('tr').data(
								'notificationstatusclass');
						var notificationprtname = $(this).closest('tr').data(
								'notificationprtname');

						$('#notificationid').text(notificationid);
						$('#notificationname').text(notificationname);
						$('#notificationtext').text(notificationtext);
						$('#notificationtitle').text(notificationtitle);
						$('#partnername').text(notificationprtname);

						var imageUrl = '${contextPath}'
								+ '/admin/images/notification/' + notificationimageurl;

						$('#notificationimage').prop('src', imageUrl);
						$('#notificationimageurl').text(notificationimageurl);
						$('#notificationlandingurl').text(notificationlandingurl);
						$('#notificationstatus').html(
								"<font class="+notificationstatusclass+">"
										+ notificationstatus);
						$('#notificationstatuscode').text(notificationstatuscode);
						$('#myModal2').data('id', id).modal('show');
					});

	$('span.editNotification').on('click', function() {
		
		var id = $(this).closest('tr').data('id');
		var notificationid = $(this).closest('tr').data('notificationid');
		var notificationname = $(this).closest('tr').data('notificationname');
		var notificationtext = $(this).closest('tr').data('notificationtext');
		var notificationtitle = $(this).closest('tr').data('notificationtitle');
		var notificationimageurl = $(this).closest('tr').data('notificationimageurl');
		var notificationlandingurl = $(this).closest('tr').data('notificationlandingurl');
		var notificationstatus = $(this).closest('tr').data('notificationstatus');
		var notificationstatuscode = $(this).closest('tr').data('notificationstatuscode');
		var notificationprtno = $(this).closest('tr').data('notificationprtno');
		var editfromdate = $(this).closest('tr').data('editfromdate');
		var edittodate = $(this).closest('tr').data('edittodate');
		var timeStamp = $(this).closest('tr').data('timestamp');
		var edit_loggedInNotification = $(this).closest('tr').data('loggedinnotification');
		var edit_nonLoggedInNotification = $(this).closest('tr').data('nonloggedinnotification');
		notificationtext = decodeURIComponent(notificationtext).split('+').join(" ");
		$("#editPartNo").val(notificationprtno).selectmenu("refresh");
		$('#edit_notificationid').val(notificationid);
		$('#edit_notificationname').val(notificationname);
		$('#edit_notificationtext').val(notificationtext);
		$('#edit_notificationtitle').val(notificationtitle);
		$('#edit_notificationimageurl').val(notificationimageurl);
		$('#edit_notificationimageurlHidden').val(notificationimageurl);
		$('#edit_notificationidHidden').val(notificationid);

		var imageUrl = '${contextPath}' + '/admin/images/notification/' + notificationimageurl;

		$('#edit_notificationimage_').prop('src', imageUrl);
		$('#edit_notificationlandingurl').val(notificationlandingurl);
		$('#edit_notificationstatus_' + notificationstatuscode).prop("checked", "checked");
		$('#editFromDate').val(editfromdate);
		$('#editToDate').val(edittodate);
		$("#edit_timeStamp").val(timeStamp);
		
		if(edit_loggedInNotification == "0"){
			$("#edit_loggedInNotification").prop("checked", false);
		}
		else{
			$("#edit_loggedInNotification").prop("checked", true);
		}
		$("#edit_loggedInNotification").val(edit_loggedInNotification);
		$("#edit_nonLoggedInNotification").val(edit_nonLoggedInNotification);
		if(edit_nonLoggedInNotification == "0"){
			$("#edit_nonLoggedInNotification").prop("checked", false);
		}
		else{
			$("#edit_nonLoggedInNotification").prop("checked", true);
		}
		tinymce.init({
			   selector: 'textarea#edit_notificationtext',
			   height: '200px',
			   plugins: 'link',
			    force_br_newlines : true,
			    force_p_newlines : false,
			    forced_root_block : '',
			    setup : function(ed){
			   	ed.on("NodeChange", function(e){
			           jQuery("textarea[id='edit_notificationtext']").val(ed.getContent());
				           if($("#editnotificationBean").validate().element('#edit_notificationtext'));
				           $(document).on('focusin', function(e) {
				        	    if ($(e.target).closest(".mce-window").length) {
				        	        e.stopImmediatePropagation();
				        	    }
				        	});
			       });
			  }
			});
		
		$('#myModal1').data('id', id).modal('show');
	});

	$('#updateNotification').click(function() {
		var id = $('#myModal1').data('id');
		
		var json = {};
		json["notificationNo"] = id;
		
		json["notificationId"] = $("#edit_notificationid").val();
		json["notificationName"] = $("#edit_notificationname").val();
		json["notificationText"] = $("#edit_notificationtext").val();
		json["notificationTitle"] = $("#edit_notificationtitle").val();
		json["prtNo"] = $("#editPartNo").val();
		if($("#editPartNo").val() ==''){
			$('#edit_notificationlandingurl').rules('remove', 'required');			
		}
		json["editNotificationImageUrl"] = $('#edit_notificationimageurlHidden').val();//$("#edit_bannerimageurl").val();//
		json["notificationLandingUrl"] = $("#edit_notificationlandingurl").val();
		json["notificationStatus"] = $('input[name=edit_notificationstatus]:checked').val();
		json["editFromDate"] = $("#editFromDate").val();
		json["editToDate"] = $("#editToDate").val();
		json["timeStamp"] = $("#edit_timeStamp").val();
		
		json["loggedInNotification"]=$("#edit_loggedInNotification").val();
		json["nonLoggedInNotification"]=$("#edit_nonLoggedInNotification").val();
		var notificationstatus = $('input[name=edit_notificationstatus]:checked').val();
		
		var formData = new FormData();
		formData.append('json', JSON.stringify(json));
		if ( $("#editnotificationBean").validate().element('#edit_notificationid')
				&& $("#editnotificationBean").validate().element('#edit_notificationname')
				&& $("#editnotificationBean").validate().element('#edit_notificationtext')
				&& $("#editnotificationBean").validate().element('#edit_notificationtitle')
				&& $("#editnotificationBean").validate().element('#edit_notificationimage')
				&& $("#editnotificationBean").validate().element('#edit_notificationimageurl')
				&& $("#editnotificationBean").validate().element('#edit_notificationlandingurl')
				&& $("#editnotificationBean").validate().element('#editPartNo')
				&& $("#editnotificationBean").validate().element('#editFromDate')
				&& $("#editnotificationBean").validate().element('#editToDate') 
				&& $("#editnotificationBean").validate().element('#edit_timeStamp')) {
			
			$.ajax({
				url : "${contextPath}/admin/updateNotification",
				data : formData,
				dataType : 'text',
				processData : false,
				contentType : false,
				type : 'POST',

				success : function(response) {
					$('#myModal1').modal('hide');
					window.location.href = "${contextPath}/admin/notification?updateMessage=" + response;
				}
			});
		}
	});

	$('#save').click(function() {
		$(this).closest('form').submit();
	});

	$(document).on("keydown", function(e) {
		if (e.which === 8 && !$(e.target).is("input, textarea")) {
			e.preventDefault();
		}
	});

	$('#cancelbtnD').click(function() {
		$.get("${contextPath}/admin/addNotificationCancel", function(data, status) {
			if (status == "success") {
				window.location.replace("${contextPath}/" + data);
			}
		});
	});

	$('#cancelbtnD1_').click(function() {
		$.get("${contextPath}/admin/addNotificationCancel", function(data, status) {
			if (status == "success") {
				window.location.replace("${contextPath}/" + data);
			}
		});
	});

	$('span.delete')
			.on(
					'click',
					function() {
						var id = $(this).closest('tr').data('id');
						var notificationname = $(this).closest('tr').data(
								'notificationname');
						$('#deleteNotificationId')
								.text(
										notificationname
												+ " "
												+ " notification will be deleted. Do you want to continue?");
						$('#myModal').data('id', id).modal('show');
					});

	$('#btnD')
			.click(
					function() {
						var id = $('#myModal').data('id');
						$
								.get(
										"${contextPath}/admin/deleteNotification?notificationNo="
												+ id,
										function(status) {
											if (status == "success") {
												$('[data-id=' + id + ']')
														.remove();
												$('#myModal').modal('hide');
												window.location.href = "${contextPath}/admin/notification?deleteBnrMsg="
														+ status;
											}
											if (status == "failure") {
												$('[data-id=' + id + ']')
														.remove();
												$('#myModal').modal('hide');
												window.location.href = "${contextPath}/admin/notification?deleteBnrMsg="
														+ status;
											}
										});
					});

	

	

	$(function() {
		$("#editnotificationBean").bind(
				"change",
				function() {
					//Get reference of FileUpload.
					var fileUpload = $("#edit_notificationimage")[0];
					var notificationType = $(
							"#editnotificationBean input[type='radio']:checked")
							.val();
					checkFileUpload(fileUpload, notificationType, "editBannerError");
					if (!uploadFlag)
						return false;
				});
	});

	$(function() {
		$("#notificationBean").bind(
				"change",
				function() {
					//Get reference of FileUpload.
					var fileUpload = $("#notificationImage")[0];
					var notificationType = $(
							"#notificationBean input[type='radio']:checked").val();
					checkFileUpload(fileUpload, notificationType, "newBannerError");
					if (!uploadFlag)
						return false;
				});
	});

	

	$('#notificationImage').on('change', function() {
		$('#uploadFile').val($(this).val());
	});

	$('#editnotificationImage').on('change', function() {
		$('#edit_notificationimageurl').val($(this).val());

	});

	$('#edit_notificationimage').on('change', function() {
		$('#edit_notificationimageurl').val($(this).val());
	});

	$(document).on("keydown", function(e) {
		if (e.which === 8 && !$(e.target).is("input, textarea")) {
			e.preventDefault();
		}
	});
	$.validator.addMethod("noSpace", function(value, element) { 
	     return value == '' || value.trim().length != 0;  
	   });
	

	function uploadImage(id, type, url) {
		if ($("#notificationBean").validate().element("#notificationImage")) {
			var formData = new FormData();
			formData.append('file', $("#" + id)[0].files[0]);

			if ($("#notificationBean").validate().element("#" + id)) {
				$.ajax({
					url : "${contextPath}/admin/saveImages?type=" + type,
					data : formData,
					dataType : 'text',
					processData : false,
					contentType : false,
					type : 'POST',

					success : function(response) {
						uploadclick = false;
						$('#uploadFile').val(response);
						var newImageUrl = '${contextPath}' + '/admin/images/'
								+ type + "/" + response;
						$('#notificationImage_').attr('src', newImageUrl);
						$("#notificationBean").validate().element("#" + url);
						$('#notificationImageUrl').val(response);
						$("#notificationBean").validate().element("#notificationImageUrl");
					}
				});
			}
		}

	}

	function editUploadImage(id, type, url) {
		if ($("#editnotificationBean").validate().element("#edit_notificationimage")) {
			var formData = new FormData();
			formData.append('file', $("#" + id)[0].files[0]);

			if ($("#editnotificationBean").validate().element("#" + id)) {
				$.ajax({
					url : "${contextPath}/admin/saveImages?type=" + type,
					data : formData,
					dataType : 'text',
					processData : false,
					contentType : false,
					type : 'POST',

					success : function(response) {
						$('#' + url).val(response);
						var newImageUrl = '${contextPath}' + '/admin/images/'
								+ type + "/" + response;
						$('#edit_notificationimageurlHidden').val(
								$("#edit_notificationimageurl").val());
						$('#edit_notificationimage_').prop('src', newImageUrl);
						$("#editnotificationBean").validate().element("#" + url);
					}
				});
			}
		}
	}

	function checkFileUpload(fileUpload, notificationType, notificationStatu) {
		//Check whether the file is valid Image.
		var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:\().+(\)\.def\().+(\])+(.jpg|.jpeg|.png|.gif)$");

		if (regex.test(fileUpload.value.toLowerCase())) {
			notificationEditFlag = true;
			//Check whether HTML5 is supported.
			if (typeof (fileUpload.files) != "undefined") {
				//Initiate the FileReader object.
				var reader = new FileReader();
				//Read the contents of Image File.
				reader.readAsDataURL(fileUpload.files[0]);

				reader.onload = function(e) {
					//Initiate the JavaScript Image object.
					var image = new Image();
					//Set the Base64 string return from FileReader as source.
					image.src = e.target.result;

					image.onload = function() {

						//Determine the Height and Width.
						var height = this.height;
						var width = this.width;

						var errorText = "";
						uploadFlag = false;

						if (notificationType == 0) {
							if (width == 1366 && height == 438) {
								uploadFlag = true;
							} else {
								uploadFlag = false;
								errorText = "Height and Width must be 438 and 1366 for Notification Image.";
							}
						} 
						else {
							if (width == 1366 && height == 438) {
								uploadFlag = true;
							} else {
								uploadFlag = false;
								errorText = "Height and Width must be 438 and 1366 for Notification Image.";
							}
						} 

						$("#" + notificationStatu).css({
							"display" : ""
						});
						$("#" + notificationStatu).html("<p>" + errorText + "</p>");

						if (errorText != "") {
							$("#notificationImageUrl1-error").css({
								"display" : "none"
							});
						}
						return uploadFlag;
					};
				};
			}
		} else {
			notificationEditFlag = false;
			uploadFlag = false;
		}
	}

	// Validation Method
	$.validator.addMethod("AlphaNumeric", function(value, element) {
		return this.optional(element) || /^[a-zA-Z0-9\s\.]+$/i.test(value);
	});
	$.validator.addMethod("time24", function(value, element) {
		if (!/^\d{2}:\d{2}:\d{2}$/.test(value)) return false;
		var parts = value.split(':');
		if (parts[0] > 23 || parts[1] > 59 || parts[2] > 59) return false;
		return true;
	}, "Please enter a valid time, between 00:00:00 and 23:59:59.");

	$.validator.addMethod('filesize', function(value, element, param) {
		return this.optional(element) || (element.files[0].size <= param)
	}, 'File size should not be more than 1MB');

	$.validator.addMethod('imageDimention', function(value, element) {
		var fileUpload = $("#notificationImage")[0];
		var notificationType = $("#notificationBean input[type='radio']:checked").val();
		checkFileUpload(fileUpload, notificationType, "newBannerError");
		return uploadFlag;
	}, '');

	$.validator.addMethod('editImageDimention',
			function(value, element) {
				var fileUpload = $("#edit_notificationimage")[0];
				var notificationType = $(
						"#editnotificationBean input[type='radio']:checked").val();
				checkFileUpload(fileUpload, notificationType, "editBannerError");
				if (notificationEditFlag) {
					return uploadFlag;
				} else {
					return true;
				}

			}, '');

	$.validator.addMethod('duplicateNotificationId', function(value, element) {
		if (notificationIDExist) {
			return false;
		} else {
			return true;
		}
	});

	

	$('#partNo').on('selectmenuchange', function() {
		$("#notificationBean").validate().element('#partNo');
		if($('#partNo').val() !=''){
			$('.urlMand').show();
			
		}else{
			$('.urlMand').hide();
		}
	});

	$('#editPartNo').on('selectmenuchange', function() {
		$("#notificationBean").validate().element('#editPartNo');
		if($('#editPartNo').val() !=''){
			//$('#edit_notificationlandingurl').rules('add', 'required');
		}else{
			//$('#edit_notificationlandingurl').rules('remove', 'required');
		}
	});

	
	$("#notificationId").change(
			function() {
				var notificationId = $('#notificationId').val();
				$.get(
						"${contextPath}/admin/checkNotificationID?notificationId="
								+ notificationId, function(data, status) {
							if (status == "success" && data != '') {
								notificationIDExist = true;
							} else {
								notificationIDExist = false;
							}
							$("#notificationBean").validate().element('#notificationId');
						});
			});

	$("#edit_notificationid").change(
			function() {
				var notificationId = $('#edit_notificationid').val();
				var notificationIdHidden = $('#edit_notificationidHidden').val();
				notificationIDExist = false;

				if (notificationId != notificationIdHidden) {
					$.get("${contextPath}/admin/checkNotificationID?notificationId="
							+ notificationId, function(data, status) {
						if (status == "success" && data != '') {
							notificationIDExist = true;
						} else {
							notificationIDExist = false;
						}
						$("#editnotificationBean").validate().element(
								'#edit_notificationid');
					});
				} else {
					$("#editnotificationBean").validate().element('#edit_notificationid');
				}
			});


	function getFormattedDate(date) {
		var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
		return new Date(date.replace(pattern, '$3-$2-$1'));
	}

	$('#fromDate').on('change', function() {
		$("#notificationBean").validate().element('#fromDate');
		var endDate = getFormattedDate($('#toDate').val());

		if (endDate != 'Invalid Date') {
			$("#notificationBean").validate().element("#toDate");
		}
	});

	$("#toDate").on('change', function() {
		$("#notificationBean").validate().element("#toDate");
		$("#notificationBean").validate().element("#fromDate");
	});


	$("#editFromDate").on('change', function() {
		$("#editnotificationBean").validate().element("#editFromDate");
	});
	$("#editToDate").on('change', function() {
		$("#editnotificationBean").validate().element("#editToDate");
		$("#editnotificationBean").validate().element("#editFromDate");
	});

	$.validator.addMethod("dateHigherThanToday", function(value, element) {
		var startDate = getFormattedDate($('#fromDate').val());
		var endDate = getFormattedDate($('#toDate').val());

		if (endDate == 'Invalid Date')
			return true;
		if (endDate !== 'Invalid Date') {
			if (startDate <= endDate)
				return true;
			if (startDate >= endDate)
				return false;
		}
	});
	
	$.validator.addMethod("editDateHigherThanToday", function(value, element) {
		var startDate = getFormattedDate($('#editFromDate').val());
		var endDate = getFormattedDate($('#editToDate').val());

		if (endDate == 'Invalid Date')
			return true;
		if (endDate !== 'Invalid Date') {
			if (startDate <= endDate)
				return true;
			if (startDate >= endDate)
				return false;
		}
	});
	
	$("#edit_nonLoggedInNotification").on('change', function() {
	    if($(this).is(":checked")) {
	    	$('#edit_nonLoggedInNotification').val(1);
	    }
	    else{
	    	$('#edit_nonLoggedInNotification').val(0);
	    }});
	
	$("#edit_loggedInNotification").on('change', function() {
	    if($(this).is(":checked")) {
	    	$('#edit_loggedInNotification').val(1);
	    }
	    else{
	    	$('#edit_loggedInNotification').val(0);
	    }});
	$("#loggedInStatus").on('change', function() {
	    if($(this).is(":checked")) {
	    	$('#loggedInStatus').val(1);
	    }
	    else{
	    	$('#loggedInStatus').val(0);
	    }});
	
	$("#nonLoggedInStatus").on('change', function() {
		
	    if($(this).is(":checked")) {
	    	$('#nonLoggedInStatus').val(1);
	    }
	    else{
	    	$('#nonLoggedInStatus').val(0);
	    }});

	$("#fromDate").datepicker({
		dateFormat : 'dd/mm/yy',
		showOn : "button",
		buttonImage : "./static/img/2016-06-23.png",
		buttonImageOnly : true,
		minDate: new Date(),
		  /* onClose : function(selectedDate) {
			$("#toDate").datepicker("option", "minDate", selectedDate);
		}, */  
		buttonText : "Select date"
	});

	$("#toDate").datepicker({
		dateFormat : 'dd/mm/yy',
		showOn : "button",
		buttonImage : "./static/img/2016-06-23.png",
		buttonImageOnly : true,
		minDate: new Date(),
		buttonText : "Select date"
	});

	$("#editFromDate").datepicker({
		dateFormat : 'dd/mm/yy',
		showOn : "button",
		buttonImage : "./static/img/2016-06-23.png",
		buttonImageOnly : true,
		minDate: new Date(),
		 /* onClose : function(selectedDate) {
			$("#editToDate").datepicker("option", "minDate", selectedDate);
		},  */
		buttonText : "Select date"
	});

	$("#editToDate").datepicker({
		dateFormat : 'dd/mm/yy',
		showOn : "button",
		buttonImage : "./static/img/2016-06-23.png",
		buttonImageOnly : true,
		minDate: new Date(),
		buttonText : "Select date",
		beforeShow : end_Range
	});
  $('#edit_timeStamp').timepicker({
	minuteStep: 1,
	secondStep: 1,
	maxHours: 24,
	defaultTime: false,
	showMeridian: false,
	showSeconds: true,
	showInputs: false,
	disableFocus: true,
	template: false
}); 
  $('#timeStamp').timepicker({
		minuteStep: 1,
		secondStep: 1,
		maxHours: 24,
		defaultTime: false,
		showMeridian: false,
		showSeconds: true,
		showInputs: false,
		disableFocus: true,
		template: false
	});
	function end_Range() {
		if ($("#editFromDate").datepicker("getDate") == null) {
			$("#editFromDate").focus();
		} else {
			// set minimum date range
			$("#editToDate").datepicker("option", "minDate",
					new Date($("#editFromDate").datepicker("getDate")));
		}
		;
	};
	
</script>

