<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<style>
.ui-dialog {
    z-index: 1 !important;
}

.margin_top{
	margin-top: 50px;
}
</style>

<section>
	<div id="breadcrumb">
		<ul>
			<!--- breadcrumb--->
			<li>Home</li>
			<li><strong>Clear Cache</strong></li>
		</ul>
	</div>
	<h5>Clear Cache</h5>
	<%-- <c:if test="${fn:length(successMessage) gt 0}">
		<div id="msg" align="center" style="color: blue; font-weight: normal;">
			<c:out value="${successMessage}"></c:out>
		</div>
	</c:if> --%>

	<div class="margin_top">
		<form id="cacheForm" name="cacheForm">
			<label>Clear Portal Cache</label><br/>
			<input type="button" class="bg_g_button" id="clearBtn" name="clearBtn" value="Clear Cache" />
		</form>
	</div>
	
	<div class="margin_top">
		<h5 id="response"></h5>
	</div>
	
</section>


<script>
	$("#clearBtn").on('click', function(e){
		confirmDialog("Are you sure, you want to clear cache");
	});
	
	function confirmDialog(message) {
	    $('<div></div>').appendTo('body')
			.html('<div><h6>'+message+'?</h6></div>')
			.dialog({
				modal: true, title: 'Clear Cache', zIndex: 1, autoOpen: true,
				width: 'auto', resizable: false,
				buttons: {
					Yes: function () {
						$.get("${contextPath}/admin/clearCache" , function(status) {
							$("#response").html(status);
							//alert(status);
						});
						$(this).dialog("close");
					},
					No: function () {                                                                 
						$(this).dialog("close");
					}
				},
				close: function (event, ui) {
					$(this).remove();
				}
			});
	}
</script>
