<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<script>
	function changeURL(){
                	history.pushState({}, '',"${contextPath}/admin/partner");
        }
</script>
<script src='https://cdn.tinymce.com/4/tinymce.min.js'></script>
<!-- Modal for delete -->
		<div id="myModala" class="modal fade" role="dialog">	
			<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Are You Sure?</h4>
				</div><!--header-->
				<div class="modal-body">
					<p>
					<img src="${contextPath}/static/img/info_icon1.png"></img>
					<span id="deletePartnerId" class="bannerNameClass"></span> 
					</p>
				</div><!---body--->
				<div class="modal-footer">
					<button id="btnD" class="btn btn-grey bgp" data-dismiss="modal">Delete</button>
				</div>
			</div><!---modal content--->
			</div>
		</div>
		 <!-- Modal for deletING fILE -->
		<div id="deleteFileModal" class="modal fade" role="dialog">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title">Are You Sure?</h4>
               </div>
               <!--header-->
               <div class="modal-body">
                  <p><img src="${contextPath}/static/img/info_icon1.png"></img>
                    <span id="deleteFileId" class="bannerNameClass"></span>
                  </p>
               </div>
               <!---body--->
               <div class="modal-footer">
                  <button id="btnFile" class="btn btn-grey bgp" data-dismiss="modal">Delete</button>
               </div>
            </div>
            <!---modal content--->
         </div>
      </div>
		
		<!-- Modal  for edit-details--> 
		
		 
        <div id="myModal1" class="modal fade" role="dialog">
        <form:form autocomplete="off"  id="editPartnerBean"	commandName="editPartnerBean">
					
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                       <button type="button" class="close" id="cancelbtnD" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Edit Partner Details This</h4>
                    </div><!----modal-header--->
                    <div class="modal-body1">
                        <div class="row pad_top" >
                            <div class="col-md-4 user_details">
                                <label>Partner ID</label>
                                <input type="text" minlength="3" maxlength="50"
												class="form-control " name="edit_partnerid" id="edit_partnerid">
								<input type="hidden" id="edit_partnerhiddenid"/>
                            </div>
                            <div class="col-md-4 user_details">
                                <label>Activity ID</label>
                                <input type="text" maxlength="20"
												class="form-control " name="edit_activityid" id="edit_activityid">
								<input type="hidden" id="edit_activityhiddenid"/>
                            </div>
                            <div class="col-md-4 user_details">
                                <label>Partner Name</label>
                               <input type="text" maxlength="100"
												class="form-control " name="edit_partnername" id="edit_partnername">
								<input type="hidden" id="edit_partnerhiddenname"/>
                            </div>
                        </div><!----row--->
                        <div class="row pad_top" >
							<div class="col-md-6 user_details">
                                <label>Rate Per Mile</label>
                                <input type="text" maxlength="6"
								class="form-control number" name="edit_ratepermile" id="edit_ratepermile">
                            </div>
                            <div class="col-md-6">
								<div class='text top' >
									<label>Partner Status</label>
									<div class="half-col last">
										<ul class="list-inline">
				
											<c:forEach items="${partnerStatus}" var="status">
														<li><label> 														
														<input type="radio"	name="edit_partnerstatus" class="css-checkbox"
																id="edit_partnerstatus_${status.key}" value="${status.key}"  />
																<label for="edit_partnerstatus_${status.key}" class="css-label radGroup2">${status.value}</label>														 																
														</label></li>
											</c:forEach>
										</ul>
									</div>
								</div>
							</div>
                        </div><!----row--->
                        <div class="row pad_top">
							<div class="col-md-6">
								<label>Activation Date</label>
								<div class="pick_date">
									<input type="text" class="form-control startDate" id="datepicker11" name="datepicker11"  />
								</div>
							</div>
							<div class="col-md-6">
								<label>Deactivation Date</label>
								<div class="pick_date">
									<input type="text" class="form-control endDate" id="datepicker12" name="datepicker12" />
									<!--img src="./img/2016-06-23.png" alt="" ></img-->
								</div>
							</div>
                        </div><!----row--->
                            <div class="row pad_top mar_bottom urlParam">
                 <label class="headLabel">URL Parameter</label>
                 	<div class="col-md-6">
                        <label>Affiliate ID</label>
                        <input type="text" name="edit_affiliateIdName" id="edit_affiliateIdName" class="form-control" placeholder="Name" /> 
                        <input type="text" name="edit_affiliateId" id="edit_affiliateId" class="form-control" placeholder="Value" /> 
                     </div>
                     <div class="col-md-6">
                        <label>Source of Info</label>
                        <input type="text" name="edit_sourceOfInfoName" id="edit_sourceOfInfoName" class="form-control" placeholder="Name" />
                        <input type="text" name="edit_sourceOfInfo" id="edit_sourceOfInfo" class="form-control" class="form-control"  placeholder="Value" /> 
                     </div>
                  </div>
                   <div class="row pad_top mar_bottom urlParam">
                      <div class="col-md-6">
                        <label>Prefix of Transaction</label>
                        <input type="text" name="edit_referenceTokenName" id="edit_referenceTokenName" class="form-control" placeholder="Name" />
                        <input type="text" name="edit_referenceToken" id="edit_referenceToken" class="form-control" placeholder="Value" /> 
                     </div>
                      <div class="col-md-6">
                        <label>Param Four</label>
                        <input type="text" name="edit_paramFourName" id="edit_paramFourName" class="form-control" placeholder="Name" />
                        <input type="text" name="edit_paramFour" id="edit_paramFour" class="form-control" class="form-control"  placeholder="Value" /> 
                     </div>
                  </div>
                        <div class="row pad_top">
							 <div class="col-md-4 user_details">
                                <label>Partner Landing Url</label>
                                <input type="text" maxlength="200" class="form-control " name="edit_PrtLandingUrl" id="edit_PrtLandingUrl">
                            </div>
                            <div class="col-md-4 user_details">
                               <label>Display Order</label>
                               <input type="text" maxlength="3" class="form-control number" name="edit_prtDispOrd" id="edit_prtDispOrd">
							   <input type="hidden" id="edit_prtDispOrdHidden"/>
                            </div>
                            <div class="col-md-4 user_details">
                               <label>Number of days for reconciliation</label>
                               <input type="text" maxlength="4" class="form-control number" name="edit_jpmileReconPeriod" id="edit_jpmileReconPeriod">
                            </div>
                            
                             <div class="col-md-4 user_details">
                               <label>Aggregate Id</label>
                               <input type="text"  class="form-control" name ="edit_aggregateId" id="edit_aggregateId">
                               <input type="hidden" id="edit_aggregatehiddenid"/>
                            </div>
                        </div><!----row--->
                        <div class="row pad_top">
                            
                             <div class="col-md-4 user_details">
                                <label>Partner Description</label>
                                <input type="text" maxlength="20" class="form-control " name="edit_partnerDesc" id="edit_partnerDesc">
                                <input type="hidden" id="edit_partnerDescHidden"/>
                            </div>
                            <!-- <div class="col-md-4 user_details">
                               <label>Terms and Conditions</label>
                               <input type="text" maxlength="251" class="form-control" name="edit_termsAndCondition" id="edit_termsAndCondition">
                            </div> -->

							</div>
						<br>
						<div class='text_editor'>
                               <label>JPMile Description</label>
                               <!-- <input type="text" maxlength="100" class="form-control" name="edit_jpmileDesc" id="edit_jpmileDesc"> -->
							   <textarea id="edit_jpmileDesc" cols="40" name="edit_jpmileDesc" class="text"></textarea> 
							   <input type="hidden" id="edit_jpmileDescHidden"/>
                        </div>
						
						<br>	
						<!---text editor--->                            
						<div class='text_editor'>
						<label>Terms & Conditions</label>
                         <textarea id="edit_termsAndCondition" cols="40" name="edit_termsAndCondition" class="text"></textarea>						
                         <p id="msg2" style="display:none;color:#FF4000;"></p>	
                        </div><!----row--->
                        
                        <div class="row pad_top">
                             <div class="col-md-6 user_details pad_top">
                                <input type="checkbox" class="allow_checkbox" value="0" name="edit_displayPrtPopup" id="edit_displayPrtPopup" />
                                <label for="edit_displayPrtPopup">Display Partner Popup</label>
                            </div>
                            <div class="col-md-6 user_details pad_top">
                                <input type="checkbox" class="allow_checkbox" value="0" name="edit_allowJpnumber" id="edit_allowJpnumber" />
                                <label for="edit_allowJpnumber">Allow User to enter JPNumber</label>
                            </div>
                        </div><!----row--->
                        <div id="talkreadmap">
                        <div class="row pad_top" >
                        	<div class="col-md-4 user_details">
                                <label >Partner Search Url</label>
                                <input type="text" maxlength="200" class="form-control " name="edit_talkreadPartUrl" id="edit_talkreadPartUrl">
                            </div>     
							<div class="col-md-4 user_details">
                                <label>Partner Tile Position</label>
                                <input type="text" maxlength="3"
								class="form-control number" name="edit_talkReadTilPos" id="edit_talkReadTilPos">
								<input type="hidden" id="edit_talkReadTilPosHidden"/>
                            </div>
                        </div><!----row--->
                   </div>
                        <div class="row pad_top">
							<!-- 	
							<div class="col-md-4 browse_btn">
									<label>Partner Image</label><br>
									<input id="edit_partnerimageurl" class="upload_file" placeholder="Choose File" disabled="disabled" />
									<div class="fileUpload btn browse_btn">
										<span class="bg_w_button">Browse</span>
										<input name="partnerImage"  id="editPartnerImage" type="file" class="upload"/>
									</div>	
								</div> -->
								
							<div class="row pad_top mar_bottom">
                     		<div class="col-md-8">
									<label>Partner Image</label><br>
									<div class="fileUpload btn">
									<input id="edit_partnerimageurl" class="upload_file upload_file_style" placeholder="Choose File" disabled="disabled" />
										<span class="bg_w_button edit_partner_browse">Browse</span>
										<input name="editPartnerImage"  id="editPartnerImage" type="file" class="upload upload_btn_modal editPartnerImageUpload bwrsBtnPop"/>
									</div>
									<div class="image_wrap image_wrap1" style="width:360px; height:150px;">
								 	<img id="editPartnerImage_"  src="" style="max-height: 100%; max-width: 100%;">
								</div>
								
							<div class="mar_top uploadFile_div">
								<input class="bg_g_button"  onclick="editUploadImage('editPartnerImage','partner','edit_partnerimageurl')" type="button" value="Upload" />
								<input type="hidden" name="edit_partnerimageurlHidden" id="edit_partnerimageurlHidden">	
							</div>	
						 	</div>			
								</div>
								
								<div class="row pad_top mar_bottom">
                     		<div class="col-md-8">
									<label>Partner Icon</label><br>
									<div class="fileUpload btn">
									<input id="edit_partnericonurl" name="edit_partnericonurl" class="upload_file upload_file_style urlBtn" placeholder="Choose File" disabled="disabled" />
										<span class="bg_w_button edit_partner_browse">Browse</span>
										<input name="editPartnerIcon"  id="editPartnerIcon" type="file" class="upload upload_btn_modal editPartnerIconUpload bwrsBtnPop"/>
										<div class="iconBtn error"></div>
									</div>
									<div class="image_wrap image_wrap1" style="width:35px; height:35px;">
								 	<img id="editPartnerIcon_"  src="" style="max-height: 100%; max-width: 100%;">
								</div>
								
							<div class="mar_top uploadFile_div">
								<input class="bg_g_button"  onclick="editUploadIcon('editPartnerIcon','partner','edit_partnericonurl')" type="button" value="Upload" />
								<input type="hidden" name="edit_partnericonurlhidden" id="edit_partnericonurlhidden">	
							</div>	
						 	</div>			
								</div>
								
								
								<!-- -----------------------------Partner Popup Image------------------------------------------------- -->
								
								<div class="row pad_top mar_bottom">
									<div class="col-md-8">
										<label>Partner Popup Image</label><br>
											<div class="fileUpload btn">
											<input id="edit_partnerpopupimageurl" class="upload_file upload_file_style" placeholder="Choose File" disabled="disabled" />
												<span class="bg_w_button edit_partner_browse">Browse</span>
												<input name="editPartnerPopupImage"	id="editPartnerPopupImage" type="file" class="upload upload_btn_modal editPartnerPopupImageUpload bwrsBtnPop" />									
											</div>
										<div class="image_wrap image_wrap1" style="width:360px; height:150px;">										
										<img id="editPartnerPopupImage_" src="" style="max-height: 100%; max-width: 100%;">
									</div>	
								<div class="mar_top uploadFile_div">
									<input class="bg_g_button" onclick="editUploadPopupImage('editPartnerPopupImage','partner','edit_partnerpopupimageurl')" type="button" value="Upload" />
									<input type="hidden" name="edit_partnerpopupimageurlHidden" id="edit_partnerpopupimageurlHidden">
								</div>
							</div>
						</div>
								
								
								
						</div> <!------------- row ----------->
						<div class="row pad_top">
							<div class="col-md-6 checkbox" style="display:none">
								<div class="">
									<input id="edit_hostpartner" type="checkbox" >
									<label for="edit_hostpartner">Host Partner</label>
								</div>
								<div class="">
									<input id="edit_defaultpartner" type="checkbox">
									<label for="edit_defaultpartner">Search Partner</label>
								</div>
							</div>
							<div class="col-md-6 checkbox" style="display:none">
								<div class="">
									<input id="edit_talkpartner" type="checkbox" >
									<label for="edit_talkpartner">Talk Partner</label>
								</div>
								<div class="">
									<input id="edit_readpartner" type="checkbox">
									<label for="edit_readtpartner">Read Partner</label>
								</div>
							</div>
							
							
							
							
							
						</div>
						<div class="row btn_save">
							<a href="${contextPath}/admin/partner"><input class="bg_g_button" type="button" value="Cancel" /></a>
							<input class="bg_g_button" type="button" id="updatePartner" value="Update" />
						</div><!-------------button----------->
                    </div>
                </div>
            </div>
            </form:form>
        </div>
        <!-- Modal  for edit-logoPartner-details-->  
        <div id="myModal_Logo_" class="modal fade" role="dialog">
        <form:form autocomplete="off"  id="editLogoPartnerBean"	commandName="editLogoPartnerBean">
					
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                       <button type="button" class="close" id="cancelbtnD" onClick="onClickCloseBtn();" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Edit Partner Details Logo</h4>
                    </div><!----modal-header--->
                    <div class="modal-body1">
                        <div class="row pad_top" >
                            <div class="col-md-4 user_details">
                                <label>Partner ID</label>
                                <input type="text" minlength="3" maxlength="50"
												class="form-control " name="edit_partnerid_" id="edit_partnerid_">
								<input type="hidden" id="edit_partnerhiddenid_"/>
                            </div>
                            <div class="col-md-4 user_details">
                                <label>Activity ID</label>
                                <input type="text" maxlength="20"
												class="form-control " name="edit_activityid_" id="edit_activityid_">
								<input type="hidden" id="edit_activityhiddenid_"/>
                            </div>
                            <div class="col-md-4 user_details">
                                <label>Partner Name</label>
                               <input type="text" maxlength="100"
												class="form-control " name="edit_partnername_" id="edit_partnername_">
								<input type="hidden" id="edit_partnerhiddenname_"/>
                            </div>
                        </div><!----row--->
                        <div class="row pad_top" >
							<div class="col-md-6 user_details" id="edit_ratepermile_Div">
                                <label>Rate Per Mile</label>
                                <input type="text" maxlength="6"
								class="form-control number" name="edit_ratepermile_" id="edit_ratepermile_">
                            </div>
                            <div class="col-md-6">
								<div class='text top' >
									<label>Partner Status</label>
									<div class="half-col last">
										<ul class="list-inline">
				
											<c:forEach items="${partnerStatus}" var="status">
														<li><label> 														
														<input type="radio"	name="edit_partnerstatus__" class="css-checkbox"
																id="edit_partnerstatus__${status.key}" value="${status.key}"  />
																<label for="edit_partnerstatus__${status.key}" class="css-label radGroup2">${status.value}</label>														 																
														</label></li>
											</c:forEach>
										</ul>
									</div>
								</div>
							</div>
                        </div><!----row--->
                        <div class="row pad_top">
							<div class="col-md-6">
								<label>Activation Date</label>
								<div class="pick_date">
									<input type="text" class="form-control startDate" id="datepicker11_" name="datepicker11_"  />
								</div>
							</div>
							<div class="col-md-6">
								<label>Deactivation Date</label>
								<div class="pick_date">
									<input type="text" class="form-control endDate" id="datepicker12_" name="datepicker12_" />
									<!--img src="./img/2016-06-23.png" alt="" ></img-->
								</div>
							</div>
							
							
                        </div><!----row--->
                            <div class="row pad_top mar_bottom urlParam">
                 <label class="headLabel">URL Parameter</label>
                 	<div class="col-md-6">
                        <label>Affiliate ID</label>
                        <input type="text" name="edit_affiliateIdName_" id="edit_affiliateIdName_" class="form-control" placeholder="Name" /> 
                        <input type="text" name="edit_affiliateId_" id="edit_affiliateId_" class="form-control" placeholder="Value" /> 
                     </div>
                     <div class="col-md-6">
                        <label>Source of Info</label>
                        <input type="text" name="edit_sourceOfInfoName_" id="edit_sourceOfInfoName_" class="form-control" placeholder="Name" />
                        <input type="text" name="edit_sourceOfInfo_" id="edit_sourceOfInfo_" class="form-control" class="form-control"  placeholder="Value" /> 
                     </div>
                  </div>
                   <div class="row pad_top mar_bottom urlParam">
                      <div class="col-md-6">
                        <label>Prefix of Transaction</label>
                        <input type="text" name="edit_referenceTokenName_" id="edit_referenceTokenName_" class="form-control" placeholder="Name" />
                        <input type="text" name="edit_referenceToken_" id="edit_referenceToken_" class="form-control" placeholder="Value" /> 
                     </div>
                     <div class="col-md-6">
                        <label>Param Four</label>
                        <input type="text" name="edit_paramFourName_" id="edit_paramFourName_" class="form-control" placeholder="Name" />
                        <input type="text" name="edit_paramFour_" id="edit_paramFour_" class="form-control" class="form-control"  placeholder="Value" /> 
                     </div>
                  </div>
                        <div class="row pad_top" >
                            <div class="col-md-4 user_details">
                                <label>Partner Landing Url</label>
                                <input type="text" maxlength="200" class="form-control " name="edit_logoPrtLandingUrl" id="edit_logoPrtLandingUrl">
								<!-- TODO: Comment hidden variables -->
								<!-- <input type="hidden" id="edit_logoPrtLandingUrl"/> -->
                            </div>
                           <div class="col-md-4 user_details">
                                <label>Display Order</label>
                               <input type="text" maxlength="3" class="form-control number" name="edit_logoPrtDispOrd" id="edit_logoPrtDispOrd">
								<input type="hidden" id="edit_logoPrtDispOrdHidden"/>
                            </div>
                            <div class="col-md-4 user_details" id="edit_jpmileReconDiv">
                               <label>Number of days for reconciliation</label>
                               <input type="text" maxlength="4" class="form-control number" name="edit_logo_jpmileReconPeriod" id="edit_logo_jpmileReconPeriod">
                            </div>
                            <div class="col-md-4 user_details">
                               <label>Aggregate Id</label>
                               <input type="text" class="form-control " name="edit_logo_aggregateId" id="edit_logo_aggregateId">
                               <input type="hidden" id="edit_aggregatehiddenid_"/>
                            </div>
                        </div><!----row--->
                         <div class="row pad_top" >
                             
                            <div class="col-md-4 user_details">
                                <label>Partner Description</label>
                                <input type="text" maxlength="20" class="form-control" name="edit_logoPartnerDesc" id="edit_logoPartnerDesc">
                                <input type="hidden" id="edit_logoPartnerDescHidden"/>
                            </div>
                            <!-- <div class="col-md-4 user_details">
                               <label>Terms and Conditions</label>
                               <input type="text" maxlength="251" class="form-control" name="edit_logoTermsAndCondition" id="edit_logoTermsAndCondition">
                            </div> -->
                        </div><!----row--->
                        <br>
                        <div class='text_editor'>
                               <label>JPMile Description</label>
                               <!-- <input type="text" maxlength="100" class="form-control" name="edit_logoJpmileDesc" id="edit_logoJpmileDesc"> -->
                               <textarea id="edit_logoJpmileDesc" name="edit_logoJpmileDesc" cols="40" class="text"></textarea>
<!-- 							   <input type="hidden" id="edit_logoJpmileDescHidden"/>
 -->                    </div>
                        
                        <br>
                        <!---text editor--->
						<div class='text_editor'>
						<label>Terms & Conditions</label>
						<textarea id="edit_logoTermsAndCondition" name="edit_logoTermsAndCondition" cols="40" class="text"></textarea>
						<p id="msg2" style="display:none;color:#FF4000;"></p>	
						</div>
						<!---text editor--->
                        <div class="row pad_top">
                             <div class="col-md-6 user_details pad_top">
                                <input type="checkbox" class="allow_checkbox" value="0" name="edit_logoDisplayPrtPopup" id="edit_logoDisplayPrtPopup" />
                                <label for="edit_displayPrtPopup">Display Partner Popup</label>
                            </div>
                            <div class="col-md-6 user_details pad_top">
                                <input type="checkbox" class="allow_checkbox" value="0" name="edit_logoAllowJpnumber" id="edit_logoAllowJpnumber" />
                                <label for="edit_allowJpnumber">Allow User to enter JPNumber</label>
                            </div>
                        </div><!----row--->
                        
                        <div class="row pad_top" >
                        	<div class="col-md-4 user_details">
                                <label id="edit_logoSrchUrl">Logo Partner Search Url</label>
                                <label id="edit_offLineSrchUrl">OffLine Partner Search Url</label>
                                <input type="text" maxlength="200"
												class="form-control " name="edit_logoPrtSearchUrl" id="edit_logoPrtSearchUrl">
								<!-- TODO: Comment hidden variables -->
								<!-- <input type="hidden" id="edit_logoPrtLandingUrl"/> -->
                            </div>
							<div class="col-md-4 user_details">
                                <label id="edit_logoTilPos">Logo Partner Tile Position</label>
                                <label id="edit_offLineTilPos">OffLine Partner Tile Position</label>
                                <input type="text" maxlength="3"
								class="form-control number" name="edit_logoPrtTilePos" id="edit_logoPrtTilePos">
								<input type="hidden" id="edit_logoPrtTilePosHidden"/>
                            </div>
                        </div><!----row--->
                        <div class="row pad_top" >
							<!-- 	
							<div class="col-md-4 browse_btn">
									<label>Partner Image</label><br>
									<input id="edit_partnerimageurl" class="upload_file" placeholder="Choose File" disabled="disabled" />
									<div class="fileUpload btn browse_btn">
										<span class="bg_w_button">Browse</span>
										<input name="partnerImage"  id="editPartnerImage" type="file" class="upload"/>
									</div>	
								</div> -->
							<div class="row pad_top mar_bottom">	
							<div class="col-md-6">
									<label>Partner Image</label><br>
									
									<div class="fileUpload btn">
										<input id="edit_partnerimageurl_" class="upload_file upload_file_style" placeholder="Choose File" disabled="disabled" />
										<span class="bg_w_button edit_partner_browse">Browse</span>
										<input name="editPartnerImageBtn"  id="editPartnerImageBtn" type="file" class="upload upload_btn_modal editPartnerImageUpload bwrsBtn"/>
									</div>	
									<div class="image_wrap image_wrap1" style="width:360px; height:150px;">
								 	<img id="editPartnerImage__"  src="" style="max-height: 100%; max-width: 100%;">
								</div>
								
							<div class="col-md-2 mar_top uploadFile_div">
								<input class="bg_g_button"  onclick="editUploadImageLogo('editPartnerImageBtn','partner','edit_partnerimageurl_')" type="button" value="Upload" />
								<input type="hidden" name="editLogo_partnerimageurlHidden" id="editLogo_partnerimageurlHidden">	
							</div>
							</div>
							
                            </div>	
					<!-- 	</div> 
						<div class="row pad_top" >-->
							<div class="row pad_top mar_bottom">	
							<div class="col-md-8">
									<label>Partner Icon</label><br>
									
									<div class="fileUpload btn">
										<input id="edit_partnericonurl_" name="edit_partnericonurl_" class="upload_file upload_file_style logoUrlBtn" placeholder="Choose File" disabled="disabled" />
										<span class="bg_w_button edit_partner_browse">Browse</span>
										<input name="editPartnerIconBtn"  id="editPartnerIconBtn" type="file" class="upload upload_btn_modal editPartnerIconUpload bwrsBtn"/>
										<div class="logoIconBtn error"></div>
									</div>	
									<div class="image_wrap image_wrap1" style="width:35px; height:35px;">
								 	<img id="editPartnerIcon__"  src="" style="max-height: 100%; max-width: 100%;">
								</div>
								
							<div class="mar_top uploadFile_div">
								<input class="bg_g_button"  onclick="editUploadIconLogo('editPartnerIconBtn','partner','edit_partnericonurl_')" type="button" value="Upload" />
								<input type="hidden" name="editLogo_partnericonurlhidden" id="editLogo_partnericonurlhidden">	
							</div>
							</div>
							
                            </div>
                            
                            <!-- -----------------------------Partner Popup Image in Logo------------------------------------------- -->
	
							<div class="row pad_top mar_bottom">
								<div class="col-md-10">
									<label>Partner Popup Image</label><br>
											<div class="fileUpload btn">
												<input id="edit_partnerpopupimageurl_" class="upload_file upload_file_style" placeholder="Choose File" disabled="disabled" />
												<span class="bg_w_button edit_partner_browse">Browser</span>
												<input name="editPartnerPopupImageBtn" id="editPartnerPopupImageBtn" type="file" class="upload upload_btn_modal editPartnerPopupImageUpload bwrsBtn" />
											</div>
											<div class="image_wrap image_wrap1" style="width:360px; height:150px;">
											<img id="editPartnerPopupImage__"  src="" style="max-height: 100%; max-width: 100%;">
										</div>
										
										<div class="col-md-2 mar_top uploadFile_div">
											<input class="bg_g_button" onclick="editUploadPopupImageLogo('editPartnerPopupImageBtn','partner','edit_partnerpopupimageurl_')" type="button" value="Upload" />
											<input type="hidden" name="editLogo_partnerpopupimageurlHidden" id="editLogo_partnerpopupimageurlHidden">
										</div>
								</div>
							</div>	                            
                            
                            	
						</div> <!------------- row ----------->
						<div class="row pad_top">
							<div class="col-md-6" style="display:none">
								<div class="">
									<input id="edit_hostpartner_" type="radio" name="partnerNames">
									<label for="edit_hostpartner_">Host Partner</label>
								</div>
								<div class="">
									<input id="edit_defaultpartner_" type="radio" name="partnerNames">
									<label for="edit_defaultpartner_">Search Partner</label>
								</div>
								<div class="">
									<input id="edit_logopartner" type="radio" name="partnerNames">
									<label for="edit_logopartner">Logo Partner</label>
								</div>
								<div class="">
									<input id="edit_offlinepartner" type="radio" name="partnerNames">
									<label for="edit_offlinepartner">OffLine Partner</label>
								</div>
							</div>
							<%-- <div class="col-md-6">
								<div class='text top' >
									<label>Partner Status</label>
									<div class="half-col last">
										<ul class="list-inline">
				edit_logopartner
											<c:forEach items="${partnerStatus}" var="status">
														<li><label> 														
														<input type="radio"	name="edit_partnerstatus__" class="css-checkbox"
																id="edit_partnerstatus__${status.key}" value="${status.key}"  />
																<label for="edit_partnerstatus__${status.key}" class="css-label radGroup2">${status.value}</label>														 																
														</label></li>
											</c:forEach>
										</ul>
									</div>
								</div>
							</div> --%>
						</div>
						
						<div class="row pad_top">
    					<div class="col-md-6" style="display:none">
    					
    					        <div class="">
									<input id="edit_talkpartner_" type="radio" name="partnerNames">
									<label for="edit_talkpartner_">Talk Partner</label>
								</div>
								<div class="">
									<input id="edit_readpartner_" type="radio" name="partnerNames">
									<label for="edit_readpartner_">Read Partner</label>
								</div>
    					
    					</div>
    					</div>						
						
						
						<div class="row btn_save">
							<a href="${contextPath}/admin/partner"><input class="bg_g_button" type="button" value="Cancel" /></a>
							<input class="bg_g_button" type="button" id="updateLogoPartner" value="Update" />
						</div><!-------------button----------->
                    </div>
                </div>
            </div>
            </form:form>
        </div>
        <!-- Modal content for view details----------------------------------->
        <div id="myModal2" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content for view details-->
                <div class="modal-content">	
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Partner Details This</h4>
                    </div><!----modal---header--->
                    <div class="modal-body2">
                        <div class="row">
                            <div class='col-md-4 user_details' >
                                <label>Partner ID</label>
                               <p id="partnerid" class="bannerNameClass"></p>
                            </div>
                            <div class='col-md-4 user_details' >
                                <label>Activity ID</label>
                               <p id="activityid" class="bannerNameClass"></p>
                            </div>
                            <div class='col-md-4 user_details ' >
                                <label>Partner Name</label>
                                <p id="partnername" class="bannerNameClass"></p>
                            </div>
                        </div><!----row--->
                        
                        
                         <div class="row">
                         <div class='col-md-8 user_details' >
                                <label>Partner Search Url</label>
                               <p id="talkReadpartnersearchUrl" class="bannerNameClass"></p>
                            </div>
                             <div class='col-md-4 user_details ' >
                                <label>Partner Tile Position</label>
                                <p id="talkreadParttileposition" class="bannerNameClass number"></p>
                            </div>
                        </div><!----row--->
                        
                        
                        
                        <div class="row">
						<div class='col-md-8 user_details'>
							<label  style="">Partner Image</label>
							<img src='' id = "partnerimage" onerror="this.src='${contextPath}/static/img/invites.png'" height="150px" width="300px"/>
							<p id="partnerimageurl" class="bannerNameClass"></p>
                        </div>
                        <div class='col-md-4 user_details'>
							<label  style="">Partner Icon</label>
							<img src='' id = "partnericon" onerror="this.src='${contextPath}/static/img/invites.png'" height="20px" width="20px"/>
							<p id="partnericonurl" class="bannerNameClass"></p>
                        </div>
                        
                        <!-- ---------------------Partner Popup Image --------------------------------------------- -->
                        
                        <div class='col-md-4 user_details'>
                        	<label  style="">Partner Popup Icon</label>
                        	<img src='' id = "partnerpopupimage" onerror="this.src='${contextPath}/static/img/invites.png'" height="150px" width="175px" />
							<p id="partnerpopupimageurl" class="bannerNameClass"></p>                        
                        </div>
                        
                        </div><!----row---><!----row--->
                        <div class="row">
                            <div class='col-md-4 user_details' >
                                <label>Partner ActivationDate</label>
                               <p id="partneractivationdate"></p>
                            </div>
                            <div class=' col-md-4 user_details ' >
                                <label>Partner DeactivationDate</label>
                                <p id="partnerdeactivationdate"></p>
                            </div>
                            <div class=' col-md-4 user_details ' >
                                <label>Partner Landing Url</label>
                                <p id="partnerlandingUrl" class="bannerNameClass"></p>
                            </div>
                        </div><!----row--->	
                         <div class="row">
                         	 <div class='col-md-4 user_details' >
                                <label>Affiliate ID Name</label>
                               <p id="affiliateIDName"></p>
                            </div>
                            <div class=' col-md-4 user_details ' >
                                <label>Source of Info Name</label>
                                <p id="sourceofInfoName"></p>
                            </div>
                            <div class=' col-md-4 user_details ' >
                                <label>Prefix of Transaction Name</label>
                                <p id="referencetokenName"></p>
                            </div>
                        </div>
                         <div class="row">
                        	<div class='col-md-4 user_details' >
                                <label>Affiliate ID Value</label>
                               <p id="affiliateID"></p>
                            </div>
                            <div class=' col-md-4 user_details ' >
                                <label>Source of Info Value</label>
                                <p id="sourceofInfo"></p>
                            </div>
                            <div class=' col-md-4 user_details ' >
                                <label>Prefix of Transaction Value</label>
                                <p id="referencetoken"></p>
                            </div>
                        </div>
                         <div class="row">
                        	<div class='col-md-4 user_details' >
                                <label>Param Four Name</label>
                               <p id="paramFourName"></p>
                            </div>
                            <div class=' col-md-4 user_details ' >
                                <label>Param Four Value</label>
                                <p id="paramFour"></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class='col-md-4 user_details' >
                                <label>Host Partner</label>
                               <p id="hostpartner"></p>
                            </div>
							<div class='col-md-4 user_details' >
                                <label>Search Partner</label>
                                <p id="defaultpartner"></p>
                            </div>
                            <div class='col-md-4 user_details' >
                                <label>Logo Partner</label>
                               <p id="logopartner"></p>
                            </div>
                        </div><!----row--->
                        <div class="row">
                            <div class='col-md-4 user_details' >
                                <label>Read Partner</label>
                               <p id="readpartner"></p>
                            </div>
							<div class='col-md-4 user_details' >
                                <label>Talk Partner</label>
                                <p id="talkpartner"></p>
                            </div>
                        </div>
                        
						<div class="row">
                            <div class='col-md-4 user_details' >
                                <label>Rate Per Mile</label>
                               <p id="ratePerMiles" class="number"></p>
                            </div>
                             <div class='col-md-4 user_details ' >
                                <label>Display Order</label>
                                <p id="logoprtdisplayorder" class="bannerNameClass"></p>
                            </div>
							<div class='col-md-4 user_details' >
                                <label>Partner Status</label>
                                <p id="partnerstatus"></p>
                            </div>
                        </div><!----row--->
                    </div><!----modal-body---->
                </div>
            </div>
        </div>
        
        <div id="myModal3" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content for view details-->
                <div class="modal-content">	
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Partner Details Logo</h4>
                    </div><!----modal---header--->
                    <div class="modal-body2">
                        <div class="row">
                            <div class='col-md-4 user_details' >
                                <label>Partner ID</label>
                               <p id="partnerid_" class="bannerNameClass"></p>
                            </div>
                            <div class='col-md-4 user_details' >
                                <label>Activity ID</label>
                               <p id="activityid_" class="bannerNameClass"></p>
                            </div>
                            <div class='col-md-4 user_details ' >
                                <label>Partner Name</label>
                                <p id="partnername_" class="bannerNameClass"></p>
                            </div>
                        </div><!----row--->
                        <div class="row">
                         <div class='col-md-8 user_details' >
                                <label id="view_logoUrl">Logo Partner Search Url</label>
                                <label id="view_offlineUrl">Offline Partner Search Url</label>
                               <p id="logopartnersearchUrl" class="bannerNameClass"></p>
                            </div>
                             <div class='col-md-4 user_details ' >
                                <label id="view_logoPos">Logo Partner Tile Position</label>
                                <label id="view_offlinePos">Offline Partner Tile Position</label>
                                <p id="logopartnertileposition" class="bannerNameClass number"></p>
                            </div>
                        </div><!----row--->
                        <div class="row">
						<div class="col-md-8 user_details">
							<label  style="">Partner Image</label>
							<img src='' id = "partnerimage_" onerror="this.src='${contextPath}/static/img/invites.png'" height="150px" width="300px"/>
							<p id="partnerimageurl_" class="bannerNameClass"></p>
                        </div>
                        <div class="col-md-4 user_details">
							<label  style="">Partner Icon</label>
							<img src='' id = "partnericon_" onerror="this.src='${contextPath}/static/img/invites.png'" height="20px" width="20px"/>
							<p id="partnericonurl_" class="bannerNameClass"></p>
                        </div>
                        
                        <!-- -------------------------Partner Popup Logo------------------------------- -->
                        
                        <div class="col-md-4 user_details">
                        	<label  style="">Partner Popup Image</label>
                        	<img src='' id = "partnerpopupimage_" onerror="this.src='${contextPath}/static/img/invites.png'" height="150px" width="175px" />
                        	<p id="partnerpopupimageurl_" class="bannerNameClass"></p>
                        </div>
                        
                        </div><!----row--->
                        <div class="row">
                            <div class='col-md-4 user_details' >
                                <label>Partner ActivationDate</label>
                               <p id="partneractivationdate_"></p>
                            </div>
                            <div class=' col-md-4 user_details ' >
                                <label>Partner DeactivationDate</label>
                                <p id="partnerdeactivationdate_"></p>
                            </div>
                            <div class='col-md-4 user_details' >
                                <label>Partner Landing Url</label>
                               <p id="partnerLandingUrl_" class="bannerNameClass"></p>
                            </div>
                        </div><!----row--->	
                        <div class="row">
                        	<div class='col-md-4 user_details' >
                                <label>Affiliate ID Name</label>
                               <p id="affiliateIDName_"></p>
                            </div>
                            <div class=' col-md-4 user_details ' >
                                <label>Source of Info Name</label>
                                <p id="sourceOfInfoName_"></p>
                            </div>
                            <div class=' col-md-4 user_details ' >
                                <label>Prefix of Transaction Name</label>
                                <p id="referencetokenName_"></p>
                            </div>
                        </div>
                        <div class="row">
                        	<div class='col-md-4 user_details' >
                                <label>Affiliate ID Value</label>
                               <p id="affiliateID_"></p>
                            </div>
                            <div class=' col-md-4 user_details ' >
                                <label>Source of Info Value</label>
                                <p id="sourceofInfo_"></p>
                            </div>
                            <div class=' col-md-4 user_details ' >
                                <label>Prefix of Transaction Value</label>
                                <p id="referencetoken_"></p>
                            </div>
                        </div>
                        <div class="row">
                        	<div class='col-md-4 user_details' >
                                <label>Param Four Name</label>
                               <p id="paramFourName_"></p>
                            </div>
                            <div class=' col-md-4 user_details ' >
                                <label>Param Four Value</label>
                                <p id="paramFour_"></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class='col-md-4 user_details' >
                                <label>Host Partner</label>
                               <p id="hostpartner_"></p>
                            </div>
							<div class='col-md-4 user_details' >
                                <label>Search Partner</label>
                                <p id="defaultpartner_"></p>
                            </div>
                            <div class='col-md-4 user_details' >
                                <label>Logo Partner</label>
                                <p id="logopartner_"></p>
                            </div>
                        </div><!----row--->
						<div class="row">
                            <div class='col-md-4 user_details' id="view_rateperMileDiv">
                                <label>Rate Per Mile</label>
                               <p id=ratePerMile_></p>
                            </div>
                             <div class='col-md-4 user_details' id="view_offlineDiv">
                                <label>Offline Partner</label>
                               <p id=offlinepartner></p>
                            </div>
							<div class='col-md-4 user_details' >
                                <label>Partner Status</label>
                                <p id="partnerstatus_"></p>
                            </div>
                            <div class='col-md-4 user_details ' >
                                <label>Display Order</label>
                                <p id="logopartnerdisplayorder" class="bannerNameClass number"></p>
                            </div>
                        </div><!----row--->
                    </div><!----modal-body---->
                </div>
            </div>
        </div>
        
        <!-- Modal content for image details----------------------------------->
					<div id="myModal1_" class="modal fade" role="dialog">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal"
										aria-hidden="true">&times;</button>
									<h4 class="modal-title">Partner Image</h4>
								</div>
								<!----modal---header--->
								<div class="modal-body2">
									
									<!----row--->
									<div class="row">
										<div class='col-md-6 user_details'>
											<label>Partner Logo</label>
											<img src='' id = "partnerimage_" onerror="this.src='${contextPath}/static/img/invites.png'" height="300px" width="448px"/>
											<p id="partnerimageurl"></p>
										</div>									
									</div>																	
								</div>
								
								
								<!----modal-body---->
							</div>
						</div>
					</div>
					
					
					<!--Edit partner mapping  -->
	<div id="editPartnerMapping" class="modal fade" role="dialog">
        <form:form autocomplete="off"  id="editPartnerMappingBean" >
		<div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content"> 
                    <div class="modal-header">
                        <button type="button" id="cancelbtnDAmzon" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Edit Partner Mapping Details</h4>
                    </div><!----modal-header--->
                    <div class="modal-body1">
                    
                     <div class="row pad_top">
								<div class="col-md-4 shilpi">
								<label>Select Partner Type</label>
								<select name="edit_PartnerTypeMapping" id="edit_PartnerTypeMapping" class="select_option">
											<option value="">---Select---</option>
  											<option value="host">Host Partner</option>
  											<option value="default">Search Partner</option>
  											<option value="logo">Logo Partner</option>
  											<option value="offline">Offline Partner</option>
  											<option value="talk">Talk Partner</ption>
  											<option value="read">Read Partner</option>
 								</select>
								
								</div>
						 </div>
                    
                    
                        <div class="row pad_top" >
                            <div class="col-md-4 user_details">
                                <label>Partner Name</label><span class="mandatory_star">*</span>
                                 <!-- <input type="text" maxlength="25" class="form-control " name="edit_partnername1" id="edit_partnername1" > -->
                                 <select name="edit_partnername1" id="edit_partnername1" class="select_option">
											<option value="">--Select--</option>
								<c:forEach items="${partnerList}" var="partner">
									<li>
										<option value="${partner.prtNo}">${partner.partnerName}</option>
									</li>
								</c:forEach>
 								</select>
								<input type="hidden" name="edit_extPartNoHidden" id="edit_extPartNoHidden"/>
                            </div>
                      
                            <!-- <div class="col-md-4 user_details">
                                <label>Partner Mapping Name</label>
                                <input type="text" maxlength="150" class="form-control " name="edit_partnermappingname" id="edit_partnermappingname">
                            </div> -->
                            
                             <div class="col-md-4">
								<label for="country">Mapping Partner Name</label><span class="mandatory_star">*</span>
							  <select name="edit_partnermappingname" id="edit_partnermappingname"
									class="select_option">
									<option value="">--Select--</option>
									<c:forEach items="${offersLinkList}" var="partner">
										<li><option value="${partner}"/>${partner}</li>
									</c:forEach>
								</select>  
								<input type="hidden" id="edit_partnerhiddenmappingname" name="edit_partnerhiddenmappingname"/>
							</div>
							<div class="col-md-6 user_details pad_top">
                                <input type="checkbox" class="allow_checkbox" value="0" name="edit_allowLandingUrl" id="edit_allowLandingUrl" />
                                <label for="edit_allowLandingUrl">Allow Partner landing Url</label>
                            </div>
                            </div><!--- row --->
                       		 </div><!----row--->
                      	 
						
				
					<div class="row btn_save align_right" >
						<input class="bg_g_button" type="button"  id="updatePartnerMapping" value="Update" />
					</div><!-------- button ------>
						</div> <!----  row  --->
                </div>
            </form:form>
        </div>
     <!--  Modal content for delete  -->
  <div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title">Are You Sure?</h4>
			</div>
			<!--header-->
			<div class="modal-body">
				<p>
					<img src="${contextPath}/static/img/info_icon1.png"></img> <span
						id="deleteMappingPartner1" class="bannerNameClass"> Partner Mapping will be
						deleted.Are you sure?</span>
				</p>
			</div>
			<!---body--->
			<div class="modal-footer">
				<button id="btnDelete1" class="bg_g_button" data-dismiss="modal">Delete</button>
			</div>
		</div>
		<!---modal content--->
	</div>
</div>
        
   
        
        <div class='container'>
            <section class="affiliate_management">
                <div id ="breadcrumb">
                    <ul>
                        <li>Home</li>
						<li>Configuration</li>
						<li>Partner Info</li>
                    </ul>
                </div>  <!---breadcrumb--->
                
               
                
                <c:if test="${fn:length(message) gt 0}">
	              <script>changeURL();</script>
	                <div class="alertMessage" id="divChanged" style="color:green;font-weight: bold;"  ><c:out value="${message}"></c:out></div> </c:if>
	  			<c:if test="${fn:length(errormessage) gt 0}">
	  			   <script>changeURL();</script>
	       		<div class="alertMessage" style="color:red;font-weight: bold;"  ><c:out value="${errormessage}"></c:out></div> </c:if>
				<div class="section_heading">
					
					<h5>Partner Management</h5> 
				</div><!--- section heading ---->
				<div class="management"><!---User Management--->
                    <ul class="nav nav-tabs">
                        <li id="viewPartnerTab"  class="active"><a data-toggle="tab" href="#sectionA">Existing Partner</a></li>
                        <li id="createPartnerTab" ><a data-toggle="tab" href="#sectionB">Add New Partner</a></li>
                        <li id="mapKeywordTab" ><a data-toggle="tab" href="#sectionC">Mapping to Keyword</a></li>
                        <li id="logoPartnerConfigurationTab"><a data-toggle="tab" href="#sectionD">Configuration of Logo Partners</a></li>                        
                        <li id="categoryMappingTab"><a data-toggle="tab" href="#sectionE">Category Mapping</a></li> 
                        <li id="partnerMappingTab"><a data-toggle="tab" href="#sectionF">Create Partner Mapping</a></li> 
                        <li  id="partnerMappingList" ><a data-toggle="tab" href="#sectionG">Partner Mapping List</a></li>
                    
                    </ul>
                    <div class="tab-content">
                        <div id="sectionA" class="tab-pane fade in active">
                         <div class="row pad_top">
								<div class="col-md-4">
								
								<form:form autocomplete="off" method="GET" id="partnerBean" action="partner"
								commandName="partnerBean" >								
									<label>Select Partner Type</label>
									<fieldset class="infield">

									<form:select name="" id="partnerType" path="partnerType"
										class="select_option">
										<form:option value="">---Select---</form:option>
										<form:option value="host">Host Partner</form:option>
										<form:option value="default">Search Partner</form:option>
										<form:option value="logo">Logo Partner</form:option>
										<form:option value="offline">Offline Partner</form:option>
										<form:option value="talk">Talk Partner</form:option>
										<form:option value="read">Read Partner</form:option>

									</form:select>
								</fieldset>
									
 									</form:form>
 									
 									<br/>
 									<c:if test="${fn:length(partnerMessage) gt 0}"><div class="alertMessage" style="color:green;font-weight: bold;"  ><c:out value="${partnerMessage}"></c:out></div> </c:if>
								</div>
					
					</div><!--- row --->
                           <c:if test="${activateLogoPartners eq false}">
                        	<%-- <c:if test="${not empty partnerList}"> --%>
                            <nav class="navbar navbar-default" role="navigation" id="submenu">
                                <!-----existing new partner starts here---->
                                
                            <table class=" partner_info_table table-bordered" cellspacing="0" cellpadding="0" width="98%" id="table"  style="margin:0 !important;">
								
								<thead>
								
									<tr>
										<th >Partner No.</th>
										<th >jpmileDesc</th>
										<th >partnerDesc</th>
										<th >termsAndCondition</th>
										<th >displayPrtPopup</th>
										<th >jpmileReconPeriod</th>
										<th >paramFour</th>
										<th >paramFourName</th>
										<th >sourceOfInfo</th>
										<th >allowJpnumber</th>
										
										<th >hostPartnerCode</th>
										<th >defaultPartnerCode</th>
										<th >readPartnerCode</th>
										<th >talkPartnerCode</th>
										<th >talkPartnerCode</th>
										
										<th>Partner Id</th>
										<th>Activity Id</th>
										<th>Partner Name</th>
										<th>Partner Image Name</th>
										<th>Partner Icon Name</th>
										<th>Partner Landing Url</th>
										<th>Display Order</th>
										<th>Activation Date</th>
										<th>Deactivation Date</th>
										<th>Partner Type</th>
										<th>Rate Per Mile</th>
										<th>Status</th>
										<th class="width_section">Action</th>
										<th>aggregateId</th>
										<th>token id</th>
									</tr>
								</thead><!----thead--->
								<%-- <tbody>
								<c:forEach items="${partnerList}" var="partner" varStatus="i">
									<tr class="admin" id="1.${partner.prtNo}"
									 data-id="${partner.prtNo}"
									 data-partnerid="${partner.partnerId}"
									 data-partnername="${partner.partnerName}" 
									 data-partnerstatus="${partner.partnerStatus}"
								     data-partnerstatuscode="${partner.partnerStatusCode}"
									 data-ratepermile="${partner.ratePerMile}"
									 data-partnerdisplayorder="${partner.logoPrtDisplayOrder}"
									 data-partnerlandingurl="${partner.partnerLandingUrl}"
									 data-hostpartner="${partner.hostPartnerCode}"
								     data-defaultpartner="${partner.defaultPartnerCode}"
								     data-logopartner="${partner.logoPartnerCode}"
								     data-offlinepartner="${partner.offLinePartnerCode}" 
									 data-partneractivationdate="${partner.activationDate}"
									 data-logopartnersearchurl="${partner.logoPartnerSearchUrl}"
									 data-partnerdeactivationdate="${partner.deactivationDate}"
									 data-partnerimageurl="${partner.partnerImageUrl}"
									 data-partnericonurl="${partner.partnerIconUrl}"
									 data-logoprtdisplayorder="${partner.logoPrtDisplayOrder}"
									 data-logoprttilepos="${partner.logoPrtTilePos}"
									 data-partnerstatusclass="${partner.partnerStatusClass}"
									 data-affiliateid="${partner.affiliateId}"
									 data-sourceofinfo="${partner.sourceOfInfo}"
									 data-referencetoken="${partner.referenceToken}"
									 data-affiliateidname="${partner.affiliateIdName}"
									 data-sourceofinfoname="${partner.sourceOfInfoName}"
									 data-referencetokenname="${partner.referenceTokenName}"
									 data-jpmiledesc="${partner.jpmileDesc}"
									 data-partnerdesc="${partner.partnerDesc}"
									 data-partnerjpmilereconperiod="${partner.jpmileReconPeriod}"
									 data-activityid="${partner.activityId}"
									 data-paramfour="${partner.paramFour}"
									 data-paramfourname="${partner.paramFourName}"
									 data-termsandcondition="${partner.termsAndCondition}"
									 data-allowjpnumber="${partner.allowJpnumber}"
									 data-talkpartner="${partner.talkPartnerCode}"
									 data-readpartner="${partner.readPartnerCode}"
									 data-displayprtpopup="${partner.displayPrtPopup}">
									<td class="slno_width">${i.count}</td>
										<td class="td_width">${partner.partnerId}</td>
										<td class="td_width">${partner.activityId}</td>
										<td class="td_width">${partner.partnerName}</td>
										<td class="td_width"><a href="javascript:void(0);" class="biurl" data-target="#myModal1_">${partner.partnerImageUrl}</a></td>
										<td class="td_width">${partner.partnerImageUrl}</td>
										<td class="td_width">${partner.partnerIconUrl}</td>
										<td class="td_width">${partner.partnerLandingUrl}</td>
										<td class="td_width">${partner.logoPrtDisplayOrder}</td>
										<td class="td_width">${partner.activationDate}</td>
										<td class="td_width">${partner.deactivationDate}</td>
										<c:if test="${partner.defaultPartnerCode eq 1}">
										<td class="td_width">Search Partner</td>
										</c:if>
										<c:if test="${partner.hostPartnerCode eq 1}">
										<td class="td_width">Host Partner</td>
										</c:if>
										<c:if test="${partner.readPartnerCode eq 1}">
										<td class="td_width">Read Partner</td>
										</c:if>
										<c:if test="${partner.talkPartnerCode eq 1}">
										<td class="td_width">Talk Partner</td>
										</c:if>
										<td class="td_width">${partner.ratePerMile}</td>
										<td class="${partner.partnerStatusClass} td_width">${partner.partnerStatus}</td>
										<td class="td_width">
											<span href="javascript:void(0);" class="view_details" data-target="#myModal2"><img src="${contextPath}/static/img/View_details_icon1.png"></img></span>
											<span class="editpartner"  data-target="#myModal1">
												<img src="${contextPath}/static/img/edit_icon.png" class="edit_wraper" ></img>
											</span>
											<span class="delete btnDelete"   data-target="#myModala" id="deletePartner">
												<img src="${contextPath}/static/img/delete_icon1.png"  class="fa fa-trash fa-lg"></img>
											</span>
										</td>
									</tr> 
									</c:forEach>									
								</tbody><!---tbody---> --%>
								
								
							</table><!---table-->	
							<div class="row btn_save pull-right">								
								<span class="peditorder"><input class="bg_g_button" type="button" id="peditreorder" onClick="$(this).closest('form').submit();" value="Edit Order"></span>						
								<span class="psaveorder" style="display:none;"><input class="bg_g_button" type="button" id="psbutton" onClick="$(this).closest('form').submit();" disabled="disabled" value="Save Order"></span>
								<input type="hidden" id="preorder_" name="reorder"/>
							</div><!------ button ------>
                            </nav>
                             </c:if>
                            <%--  </c:if> --%>
                             <c:if test="${activateLogoPartners eq true || activateOffLinePartners eq true}">
                           <%--   <c:if test="${not empty partnerList}"> --%>
                            <nav class="navbar navbar-default" role="navigation" id="submenu">
                                <!-----existing new partner starts here---->
                              
                            <table class=" partner_info_table table-bordered" cellspacing="0" cellpadding="0" width="98%" id="table-1" style="margin:0 !important;">
								
								
								<thead>
								
									<tr>
										
										<th >Partner No.</th> 
										<th>Partner Id</th>
										<th>Activity Id</th>
										<th>Partner Name</th>
										<th>Partner Image Name</th>
										<th>Partner Icon Name</th>
										<th>Partner Landing Url</th>
										<th>Logo Partner Search Url</th>
										<th>Display Order</th>
										<th>Logo Partner Tile Position</th>
										<th>Activation Date</th>
										<th>Deactivation Date</th>
										<th>Partner Type</th>
										<c:if test="${activateLogoPartners eq true}">
										<th>Rate Per Mile</th>
										</c:if>
										<th>Status</th>
										<th class="width_section">Action</th>
										<th>aggregate id</th>
										<th>token Id</th>
									</tr>
								</thead><!----thead--->
								<%-- <tbody>
								<c:forEach items="${partnerList}" var="partner" varStatus="i">
									<tr class="admin" id="1.${partner.prtNo}"
									 data-id_="${partner.prtNo}"
									 data-partnerid="${partner.partnerId}"
									 data-partnername="${partner.partnerName}" 
									 data-partnerstatus="${partner.partnerStatus}"
								     data-partnerstatuscode="${partner.partnerStatusCode}"
									 data-ratepermile="${partner.ratePerMile}"
									 data-hostpartner="${partner.hostPartnerCode}"
								     data-defaultpartner="${partner.defaultPartnerCode}"
								     data-logopartner="${partner.logoPartnerCode}"
								     data-offlinepartner="${partner.offLinePartnerCode}" 
									 data-partneractivationdate="${partner.activationDate}"
									 data-partnerdeactivationdate="${partner.deactivationDate}"
									 data-partnerimageurl="${partner.partnerImageUrl}"
									 data-partnericonurl="${partner.partnerIconUrl}"
									 data-partnerlandingurl="${partner.partnerLandingUrl}"
									 data-logopartnersearchurl="${partner.logoPartnerSearchUrl}"
									 data-logoprtdisplayorder="${partner.logoPrtDisplayOrder}"
									 data-logoprttilepos="${partner.logoPrtTilePos}"
									 data-partnerstatusclass="${partner.partnerStatusClass}"
									 data-affiliateid="${partner.affiliateId}"
									 data-sourceofinfo="${partner.sourceOfInfo}"
									 data-referencetoken="${partner.referenceToken}"
									 data-affiliateidname="${partner.affiliateIdName}"
									 data-sourceofinfoname="${partner.sourceOfInfoName}"
									 data-referencetokenname="${partner.referenceTokenName}"
									 data-jpmiledesc="${partner.jpmileDesc}"
									 data-partnerdesc="${partner.partnerDesc}"
									 data-partnerjpmilereconperiod="${partner.jpmileReconPeriod}"
									 data-activityid="${partner.activityId}"
									 data-paramfour="${partner.paramFour}"
									 data-paramfourname="${partner.paramFourName}"
									 data-termsandcondition="${partner.termsAndCondition}"
									 data-allowjpnumber="${partner.allowJpnumber}"
									 data-displayprtpopup="${partner.displayPrtPopup}">									
										<td class="slno_width">${i.count}</td>
										<td class="td_width">${partner.partnerId}</td>
										<td class="td_width">${partner.activityId}</td>
										<td class="td_width">${partner.partnerName}</td>
										<td class="td_width"><a href="javascript:void(0);" class="biurl" data-target="#myModal1_">${partner.partnerImageUrl}</a></td>
										<td class="td_width">${partner.partnerImageUrl}</td>
										<td class="td_width">${partner.partnerIconUrl}</td>
										<td class="td_width">${partner.partnerLandingUrl}</td>
										<td class="td_width">${partner.logoPartnerSearchUrl}</td>
										<td class="td_width">${partner.logoPrtDisplayOrder}</td>
										<td class="td_width">${partner.logoPrtTilePos}</td>
										<td class="td_width">${partner.activationDate}</td>
										<td class="td_width">${partner.deactivationDate}</td>
										<c:if test="${partner.logoPartnerCode eq 1}">
										<td class="td_width">Logo Partner</td>
										<td class="td_width">${partner.ratePerMile}</td>
										</c:if>
										<c:if test="${partner.defaultPartnerCode eq 1}">
										<td class="td_width">Search Partner</td>
										</c:if>
										<c:if test="${partner.hostPartnerCode eq 1}">
										<td class="td_width">Host Partner</td>
										</c:if>
										<c:if test="${partner.offLinePartnerCode eq 1}">
										<td class="td_width">Offline Partner</td>
										</c:if>
										<td class="${partner.partnerStatusClass} td_width">${partner.partnerStatus}</td>
										<td class="td_width">
											<span href="javascript:void(0);" class="view_details_" data-target="#myModal3"><img src="${contextPath}/static/img/View_details_icon1.png"></img></span>
											<span class="editpartner_"  data-target="#myModal_Logo_">
												<img src="${contextPath}/static/img/edit_icon.png" class="edit_wraper" ></img>
											</span>
											<span class="delete btnDelete"   data-target="#myModala" id="deletePartner">
												<img src="${contextPath}/static/img/delete_icon1.png"  class="fa fa-trash fa-lg"></img>
											</span>
										</td>
									</tr> 
									</c:forEach>									
								</tbody><!---tbody---> --%>
							
							</table><!---table-->	
							<div class="row btn_save pull-right">								
								<span class="peditorder"><input class="bg_g_button" type="button" id="peditreorder" onClick="$(this).closest('form').submit();" value="Edit Order"></span>						
								<span class="psaveorder" style="display:none;"><input class="bg_g_button" type="button" id="psbutton" onClick="$(this).closest('form').submit();" disabled="disabled" value="Save Order"></span>
								<input type="hidden" id="preorder_" name="reorder"/>
							</div><!------ button ------>
                            </nav>
                             </c:if>
                          <%--    </c:if> --%>
                         
                        </div><!----section A---->
                        <div id="sectionB" class="tab-pane fade submenu" ><!-----create new partner starts here---->
                        <form:form autocomplete="off" name="partnerBeanNew"  id="partnerBeanNew" commandName="partnerBean" enctype="multipart/form-data">
                            <div class="row pad_top">
								<%-- <div class="checkbox">
									<div class="col-md-4">
 											<form:checkbox path="hostPartner" name="hostPartner" id="hostPartner" value="1" label="Host Partner"/>
									</div>
									<div class="col-md-4">
											<form:checkbox path="defaultPartner" name="defaultPartner" id="defaultPartner" value="1" label="Search Partner" />
									</div>
									<div class="col-md-4">
											<form:checkbox path="logoPartner" name="logoPartner" id="logoPartner" value="1" label="Logo Partner"/>
									</div>
                                </div> --%>
                                
                               	<div class="row pad_top">
                           			<form:radiobutton class="css-checkbox" path="partnerType" name="partnerTypeRadio" id ="hostPartner" value="hostPartner" />
                           			<label class="css-label radGroup2" for="hostPartner">Host Partner</label>
                           			<form:radiobutton class="css-checkbox" path="partnerType" name="partnerTypeRadio" id ="defaultPartner" value="searchPartner" />
                           			<label class="css-label radGroup2" for="defaultPartner">Search Partner</label>
                           			<form:radiobutton class="css-checkbox" path="partnerType" name="partnerTypeRadio" id ="logoPartner" value="logoPartner" />
                           			<label class="css-label radGroup2" for="logoPartner">Logo Partner</label>
                           			<form:radiobutton class="css-checkbox" path="partnerType" name="partnerTypeRadio" id ="offLinePartner" value="offLinePartner" />
                           			<label class="css-label radGroup2" for="offLinePartner">Offline Partner</label>
                           			<form:radiobutton class="css-checkbox" path="partnerType" name="partnerTypeRadio" id ="talkPartner" value="talkPartner" />
                           			<label class="css-label radGroup2" for="talkPartner">Talk Partner</label>
                           			<form:radiobutton class="css-checkbox" path="partnerType" name="partnerTypeRadio" id ="readPartner" value="readPartner" />
                           			<label class="css-label radGroup2" for="readPartner">Read Partner</label>
		                  		</div>
                                
                            </div>
                            <div class="row pad_top">
                                <div class="col-md-4">
									<label>Partner ID</label><span class="mandatory_star">*</span>									
									<form:input type='text' minlength="3" maxlength="50" name="partnerId" id="partnerId" path="partnerId"
								class="form-control" />	
								</div>
								<div class="col-md-4">
									<label>Activity ID</label><span class="mandatory_star">*</span>
									<form:input maxlength="20" type='text' name="activityId" id="activityId" path="activityId" class="form-control" />
								</div>
                                <div class="col-md-4">
									<label>Partner Name</label><span class="mandatory_star">*</span>
									<form:input minlength="3" maxlength="100" type='text' name="partnerName" id="partnerName" path="partnerName" class="form-control" />
								</div>
                            </div><!--- row --->  
                            <div class="row pad_top">
								<div class="col-md-4">
									<label>Activation Date</label><span class="mandatory_star">*</span>
									<div class="pick_date">
										<form:input  class="form-control startDate" name="datepicker1" id='datepicker1' path="activationDate" />
										<!--img src="./img/2016-06-23.png" alt="" ></img-->
									</div>
								</div>
								<div class="col-md-4">
									<label>Deactivation Date</label>
									<div class="pick_date">
										<form:input  class="form-control endDate" name="datepicker2" id='datepicker2' path="deactivationDate"/>
										<!--img src="./img/2016-06-23.png" alt="" ></img-->
									</div>
								</div>
								 <div class="col-md-4" id="ratePerMileDiv">
									<label>Rate Per Mile</label><span id="manStar" class="mandatory_star">*</span>
									<form:input type='text' maxlength="6" name ="ratePerMile" id="ratePerMile" path="ratePerMile" class="form-control number" />	
								</div>
							</div>
							<div class="row pad_top">
                                <div class="col-md-4">
									<label>Partner Landing Url</label><span class="mandatory_star">*</span>
									<form:input  maxlength="200" type='text' name="partnerLandingUrl" id="partnerLandingUrl" path="partnerLandingUrl" class="form-control" />	
								</div>
                                <div class="col-md-4">
									<label>Display Order</label><span class="mandatory_star">*</span>						
									<form:input type='text' maxlength="3" name="logoPrtDisplayOrder" id="logoPrtDisplayOrder" path="logoPrtDisplayOrder" class="form-control number" />	
								</div>
								<div class="col-md-4">
									<div class='text top' >
										<label>Partner Status</label>
										<div class="half-col last">
											<ul class="list-inline">
												<c:forEach items="${partnerStatus}" var="status">
													<li>
														<label>
															<form:radiobutton path="partnerStatusCode" class="css-checkbox" id ="${status.key}" value="${status.key}" />
															<label for="${status.key}" class="css-label radGroup2">${status.value}</label>
														</label>
													</li>
												</c:forEach>
											</ul>
										</div>
									</div>
								</div>
                            </div><!------------- row ----------->
                            
                            <!-- ********************************* -->
                             
                             <div class="row pad_top">
                                <div class="col-md-4">
                                	<label>Partner Description</label><span class="mandatory_star" id="partnerDescDiv">*</span>
	                        		<form:input type="text" name="partnerDesc" id="partnerDesc" path="partnerDesc" maxlength="20" class="form-control"/> 
	                        		<div class="error_partnerDesc"></div>
                     			</div>
                                <div class="col-md-4" id="jpMileReconDiv">
									<label>Number of days for reconciliation</label><span class="mandatory_star">*</span>						
									<form:input type='text' maxlength="4" name="jpmileReconPeriod" id="jpmileReconPeriod" path="jpmileReconPeriod" class="form-control number" />	
								</div>
								
								<div class="col-md-4">
									<label>Aggregate Id</label><!-- <span class="mandatory_star">*</span> -->	
									<form:input maxlength="100" type='text' name="aggregateId" id="aggregateId" path="aggregateId" class="form-control" />
								</div>
								
                            </div><!------------- row ----------->
                            <div class="row pad_top">
                            	<div class="col-md-6">
									<label>JPMile Description (Ex:Earn --- {jpm} --- {rs} ---)</label><span class="mandatory_star">*</span>						
									<%-- <form:input type="text" maxlength="100" name="jpmileDesc" id="jpmileDesc" path="jpmileDesc" class="form-control" /> --%>								
 				                     <form:textarea id="jpmileDesc" cols="40" path="jpmileDesc" class="text"/>
								 </div>
                            </div>
                             <!-- ********************************* -->
                     
							 <div id="logo_partner" class="row pad_top display_none">
                                <div class="col-md-4">
									<label id="logoPrtPosLbl">Logo Partner Position On Search Page<span class="mandatory_star">*</span></label>
									<label id="offlinePrtPosLbl">Offline Partner Position On Search Page<span class="mandatory_star">*</span></label>
									<label id="partnerPosOnSearchPg">Partner Tile Position <span class="mandatory_star">*</span></label>
									<form:input maxlength="3" type='text' name="logoPrtTilePos" id="logoPrtTilePos" path="logoPrtTilePos" class="form-control number" />	
								</div>
								<div class="col-md-4">
									<label id="logoPrtUrlLbl">Logo Partner Search URL</label>
									<label id="offlinePrtUrlLbl">Offline Partner Search URL<span class="mandatory_star">*</span></label>
									<label id="partnerSearchUrl">Partner Search URL<span class="mandatory_star">*</span></label>
									<form:input maxlength="200" type='text' name="logoPartnerSearchUrl" id="logoPartnerSearchUrl" path="logoPartnerSearchUrl" class="form-control" />	
								</div>
                             </div><!--- row --->
                             
                             <!-- ********************************* -->
                            
                             <div class="row pad_top mar_bottom urlParam">
                 <label class="headLabel ">URL Parameter</label>
                 	 <div class="col-md-4">
                        <label>Affiliate ID</label>
                        <input type="text" name="affiliateIdName" id="affiliateIdName" path="affiliateIdName" class="form-control param1" placeholder="Name" /> 
                        <input type="text" name="affiliateId" id="affiliateId" path="affiliateId" class="form-control" placeholder="Value" /> 
                        <div class="error_param1"></div>
                     </div>
                     <div class="col-md-4">
                        <label>Source of Info</label> 
                        <input type="text" name="sourceOfInfoName" id="sourceOfInfoName" path="sourceOfInfoName" class="form-control param2"  placeholder="Name" /> 
                        <input type="text" name="sourceOfInfo" id="sourceOfInfo" path="sourceOfInfo" class="form-control" placeholder="Value" />
                        <div class="error_param2"></div>
                     </div>
                  </div>
                  
                  <div class="row pad_top mar_bottom urlParam">
                     <div class="col-md-4">
                       <label >Prefix of Transaction (Name)<span class="mandatory_star1" id="transactionDiv"></span></label>
                        <input type="text" name="referenceTokenName" id="referenceTokenName" path="referenceTokenName" class="form-control param3" placeholder="Name" /> 
                        <input type="text" name="referenceToken" id="referenceToken" path="referenceToken" class="form-control" placeholder="Value" />
                        <div class="error_param3"></div>
                     </div>
                     <div class="col-md-4">
                        <label>Param Four</label> 
                        <input type="text" name="paramFourName" id="paramFourName" path="paramFourName" class="form-control param4"  placeholder="Name" /> 
                        <input type="text" name="paramFour" id="paramFour" path="paramFour" class="form-control" placeholder="Value" />
                        <div class="error_param4"></div>
                     </div>
                  </div>
                  
                  <div class="row pad_top mar_bottom">
                     <%-- <div class="col-md-4">
                        <label>Terms and Conditions</label><span class="mandatory_star">*</span>
                        <form:input type='text' class="form-control" maxlength="251" path="termsAndCondition" name="termsAndCondition" id="termsAndCondition" />
                     </div> --%>
                     <!---text editor--->
  					<div class='col-md-6 text_editor'>
					<label>Terms & Conditions</label><span class="mandatory_star">*</span>
					<form:textarea id="termsAndCondition" cols="40" path="termsAndCondition" class="text"/>
					<p id="msg2" style="display:none;color:#FF4000;"></p>	
					</div>
					<!---text editor--->
                     <div class="col-md-4">
						<form:checkbox path="displayPrtPopup" id="displayPrtPopup" name="displayPrtPopup" value="0" class="css-checkbox checkBoxMargin" />
						<label>Display Partner Popup</label>
					</div>
					<div class="col-md-4">
						<form:checkbox path="allowJpnumber" id="allowJpnumber" name="allowJpnumber" value="0" class="css-checkbox checkBoxMargin" />
						<label>Allow User to enter JPNumber</label>
					</div>
                  </div>
                  
                  
							<div class="row pad_top mar_bottom">
								<div class="col-md-4">
									<label>Partner Image Create</label><span class="mandatory_star">*</span>
									<div>
										<input id="uploadFile" class="upload_file" placeholder="Choose File" disabled="disabled" />
										<div class="fileUpload btn padTop_Btm">
											<span class="bg_w_button">Browse</span>
											<input name="partnerImage"  id="partnerImage" type="file" class="upload newPartnerImageUpload bwrsBtnAddPar"/>
										</div>
									</div>	
									<div class="image_wrap image_wrap1" style="width:360px; height:150px;">
                                     <!-- img  width="100%" height="107px"  src=""-->
                                     <img id="partnerImage_" src="" style="max-height: 100%; max-width: 100%;" />
                                </div>
								<div class="pad_top">
									<input class="bg_g_button" type="button" value="Upload" onclick="uploadImage('partnerImage','partner','partnerImageUrl')" />
								
									<form:hidden path="partnerImageUrl" id="partnerImageUrl" />	
								</div>
								</div>
								<div class="col-md-4">
									<label>Partner Icon</label><span class="mandatory_star">*</span>
									<div>
										<input id="uploadFile_" class="upload_file_" placeholder="Choose File" disabled="disabled" />
										<div class="fileUpload btn padTop_Btm">
											<span class="bg_w_button">Browse</span>
											<input name="partnerIcon"  id="partnerIcon" type="file" class="upload newPartnerIconUpload bwrsBtnAddPar"/>
										</div>
										<div id="icon_error" class="error"></div>
									</div>	
									<div class="image_wrap image_wrap1" style="width:35px; height:35px;">
                                     <!-- img  width="100%" height="107px"  src=""-->
                                     <img id="partnerIcon_" src="" style="max-height: 100%; max-width: 100%;" />
                                </div>
								<div class="pad_top">
									<input class="bg_g_button" type="button" value="Upload" onclick="uploadIcon('partnerIcon','partner','partnerIconUrl')" />
								
									<form:hidden path="partnerIconUrl" id="partnerIconUrl" />	
								</div>
								</div>
		<!-- --------------------------------------------------------- Partner Image Popup ---------------------------------------------------- -->	
								<div class="col-md-4">
									<label> Partner Image Popup </label><span class="mandatory_star">*</span>
									<div>
										<input id="upload_File_" class="upload_file_" placeholder="Choose File" disabled="disabled" />
										<div class="fileUpload btn padTop_Btm">
											<span class="bg_w_button">Browse</span>
											<input name="partner_Image_"  id="partner_Image_" type="file" class="upload newPartnerImagePopupUpload bwrsBtnAddPar"/>
										</div>
										<div class="image_wrap image_wrap1" style="width:360px;  height:150px;">
                                     		<!-- img  width="100%" height="107px"  src=""-->
                                     		<img id="partner_Image_Display" src="" style="max-height: 100%; max-width: 100%;" />
                                		</div>
                                	</div>
                                	<div class="pad_top">
                                		<input class="bg_g_button" type="button" value="Upload" onclick="uploadPopupImage('partner_Image_','partner','partnerPopupImageUrl')" />
                                		
                                		   <form:hidden path="partnerPopupImageUrl" id="partnerPopupImageUrl" />   
                                	</div>
                                </div>
								
								
							</div><!--- row --->
				    <%--       <div class="row pad_top">
								
                                <div class="col-md-4">
                                    <div class='text top' >
                                        <label>Partner Status</label>
                                        <div class="half-col last">
                                            <ul class="list-inline">
                                                <c:forEach items="${partnerStatus}" var="status">
												<li>
													<label>
														<form:radiobutton path="partnerStatusCode" class="css-checkbox" id ="${status.key}" value="${status.key}" />
														<label for="${status.key}" class="css-label radGroup2">${status.value}</label>
													</label>
												</li>
												</c:forEach>
											</ul>
                                        </div>
                                    </div>
                                </div>
                            </div> --%>
                            
                            <div class="row btn_save pull-right">
								<a  href="${contextPath}/admin/partner"><input class="bg_g_button" type="button" value="Cancel" id="cancelbtnD" /></a>
								<button type="button" class="bg_g_button" id="savePartner" onClick="checkValidation(); $(this).closest('form').submit();">Save</button>
							</div><!---- button ----->
							</form:form>
                        </div><!----section B---->
                        
                        
                        <!----section C starts---->
                          <div id="sectionC" class="tab-pane fade submenu mapKeyword">
                         <div class="row pad_top">
							 <form id="uploadCSVForm" name="uploadCSVForm" method="POST" action="uploadFile" enctype="multipart/form-data">
								<div class="row">
								<div class="col-md-4">
									<label>Select a Partner</label>
									<fieldset class="infield">
										<select name="prtType" id="prtType" class="select_option">
											    <option value="">--Select--</option>
											 	<option value="logo">Logo Partner</option>
												<option value="offline">Offline Partner</option>
												<option value="read">Read Partner</option>
												<option value="talk">Talk Partner</option>
		 								</select>
		 								<c:if test="${mappingKeyWordTab==true}">
		 								<input type="hidden" id="hiddenPrtType" value="${prttype}">
		 								</c:if>
	 								</fieldset>
								</div>
								<div class="col-md-4">
									<label>Select Partner Name</label>
									<fieldset class="infield">
										<select name="logoPrtNo" id="logoPrtNo"  class="select_option">
											<option value="">--Select--</option>
		 								</select>
		 								<c:if test="${mappingKeyWordTab==true}">
		 									<input type="hidden" id="hiddenLogoPrtNo" value="${logoprtNo}">
		 								</c:if>
	 								</fieldset>
								</div>
                                <div class="col-md-3 upload_keyword">
									<label>Upload KeyWord File</label><span class="mandatory_star">*</span>
									<div class="fileUpload btn ">						
										<input name="uploadFile2" id="uploadFile2"  class="upload_file" placeholder=".csv file only" disabled="disabled" />
										<span class="bg_w_button">Browse</span>									
										<input type="file" name="uploadCSVFile" id="uploadCSVFile" class="upload uploadfile_button"    />
										<input type="hidden" id="filename" name="filename"  />
									</div>
	                               <div class="fileUpload btn ">
	                                	<input class="bg_g_button" type="submit"  id="UploadFile" value="Upload" />
<!-- 										<button type="submit" class="bg_g_button" id="UploadFile" onClick="$(this).closest('form').submit();">Upload</button> -->
									</div>
								</div>
								<div class="col-md-1">
									 
								</div>
								</div>
								</form>
                         </div><!--- row --->
                         <table class=" partner_info_table table-bordered" cellspacing="0" cellpadding="0" width="98%" id="tt" >
								
								
								<thead>
								
									<tr>
										<th class="name">Sl No.</th>
										<th>Uploaded Files</th>
										<th>Date</th>
										<th class="width_section">Action</th>
									</tr>
								</thead><!----thead--->
								<tbody>
								<c:forEach items="${keywordsFile}" var="keyWord" varStatus="i">
									<tr class="admin" id="1.${keyWord.keyWordNo}"
									 data-id_="${keyWord.keyWordNo}"
									 data-filepath="${keyWord.filePath}">									
										<td class="slno_width">${i.count}</td>
										<td class="td_width"><a class="dwldFile" href="/afadmin/admin/download?filePath=${keyWord.filePath}">${keyWord.fileName}</a></td>
											<td class="td_width">${keyWord.activationDate}</td>
										<td class="td_width">
											<span class="delete btnDelete" data-target="#deleteFileModal" id="deleteFile">
												<img src="${contextPath}/static/img/delete_icon1.png"  class="fa fa-trash fa-lg"></img>
											</span>
										</td>
									</tr> 
									</c:forEach>									
								</tbody><!---tbody--->
							
							</table><!---table-->	
                          </div> <!----section C Ends---->
                          
                          <!-- Section D Starts -->
							<div id="sectionD" class="tab-pane fade submenu mapKeyword" >
								<form id="logoPartnerConfigForm" name="logoPartnerConfigForm" method="POST" action="configureLogoPartner">
									<div class="row pad_top">
										<div class="col-md-4">
											<label>Tile Number for Search Result</label><span class="mandatory_star">*</span>
											<input id="configTileNo" maxlength="2" name="configTileNo" type="text" class="form-control" value="${requestScope.configTileNo}"   />	
										</div>
										<!-- <div class="col-md-4">
											<label>Number of Logo Partners in Home page</label><span class="mandatory_star">*</span>
											<input id="configTotalLogoPartner" name="configTotalLogoPartner" type="text" class="form-control"   />	
										</div> -->
									</div>
									<div class="row btn_save pull-right">
										<a  href="${contextPath}/admin/partner"><input class="bg_g_button" type="button" value="Cancel" id="

" /></a>
										<button type="button" class="bg_g_button" onClick="$(this).closest('form').submit();">Save</button>
									</div>
								</form>
							</div>
                          <!-- Section D Ends -->
                           
                           <!-- category Mapping Starts -->
                          <div id="sectionE" class="tab-pane fade ">
                          <form:form autocomplete="off" method="GET" id="partnerCatMapping"
								commandName="partnerCatMapping" >	
                           <div class="row pad_top">
                           
								<div class="col-md-2">
									<label>Select Partner Type</label>
									</div>
									<!-- <fieldset class="infield"> -->
									<div class="col-md-3">
										<select name="prtTypeCatMapping" id="prtTypeCatMapping" class="select_option">
											<option value="">---Select---</option>
  											<option value="host">Host Partner</option>
  											<option value="default">Search Partner</option>
  											<option value="logo">Logo Partner</option>
  											<option value="offline">Offline Partner</option>
  											<option value="talk">Talk Partner</ption>
  											<option value="read">Read Partner</option>

 										</select>
								    </div>
						   </div>
						   
						   <div class="row pad_top">
						    <div class="col-md-2">
									<label>Select Partner Name</label>
									</div>
									<!-- <fieldset class="infield"> -->
									<div class="col-md-3">
										<select name="prtNameCatMapping" id="prtNameCatMapping" class="select_option">
										<option value="">-Select Partner-</option>
										</select>
								    </div>
						   </div>
						   
						   <div class="row pad_top">
						   <div class="col-md-2">
						   <label>Category Mapping</label>
						   </div>
						   <div class="col-md-4">
						   <textarea id="textCatMapping" name="textCatMapping" class="text" rows="4" cols="50"></textarea>
						   <i ><u><a href="javascript:void(0)" id="fetchLink"> Fetch Mapped JPPL Categories</a></u></i>
						   </div>
						   </div>
						   <div class="row btn_save pull-right">
						   <a  href="${contextPath}/admin/partner"><input class="bg_g_button" type="button" value="Cancel" id="" /></a>
						   <button type="button" class="bg_g_button" id="updatePartnerCategoryMap">Save</button>
						   </div>
						   </form:form>
                          </div>
                          <!-- category mapping ends --> <!-- jagjeet --> 
                         <!--  section F -->
                          
                          <div id="sectionF" class="tab-pane fade in ">
                         
                          <form:form autocomplete="off" method="GET" id="createPartnerMappingForm"
								commandName="createPartnerMappingForm" >
                         
                          <div class="row pad_top">
								<div class="col-md-4">
								<label>Select Partner Type</label><span class="mandatory_star">*</span>
								<select name="partnerTypeMapping" id="partnerTypeMapping" class="select_option">
											<option value="">---Select---</option>
  											<option value="host">Host Partner</option>
  											<option value="default">Search Partner</option>
  											<option value="logo">Logo Partner</option>
  											<option value="offline">Offline Partner</option>
  											<option value="talk">Talk Partner</ption>
  											<option value="read">Read Partner</option>
 								</select>
								
								</div>
						 </div>
					 
						  <div class="row pad_top">
								<div class="col-md-4">
								<label>Select Partner</label><span class="mandatory_star">*</span>
								<select name="partName" id="partName" class="select_option">
											<option value="">---Select---</option>
 								</select>
								</div>
						 </div>
						 
						 <!-- <div class="row pad_top">
								<div class="col-md-4">
								<label>Mapping Partner Name</label><span class="mandatory_star">*</span>
								<input type="text" class="form-control" id="createMappingPartnerName" name="createMappingPartnerName"/>
								</div>
						
						
						 </div> -->
						 
						 <div class="col-md-4">
								<label for="country">Mapping Partner Name</label><span class="mandatory_star">*</span>
							 <select name="createMappingPartnerName" id="createMappingPartnerName"
									class="select_option">
									<option value="">--Select--</option>
									<c:forEach items="${offersLinkList}" var="partner">
										<li><option value="${partner}"/>${partner}</li>
									</c:forEach>
								</select>  
							</div>
						<div class="col-md-4">
						<%-- <form:checkbox path="allowLandingUrl" id="allowLandingUrl" name="allowLandingUrl" value="0" class="css-checkbox checkBoxMargin" /> --%>
						<input type="checkbox" class="allow_checkbox" name="allowLandingUrl" id="allowLandingUrl" value="0"/>
						<label>Allow Partner landing Url</label>
					   </div>
 				   
						 <div class="row btn_save pull-right">
						   <input class="bg_g_button" type="button" value="Cancel" id="cancelCreatePartCat" />
						   <button type="button" class="bg_g_button" id="createPartnerSave">Save</button>
						   </div>
						 </form:form>
                         </div>
                          
                                 <!-- End section F --> 
                                 
                                 
                                   <!--Section G starts  -->
                        <div id="sectionG" class="tab-pane fade in "><!---- partner category starts here ---->
                            <nav class="navbar navbar-default" role="navigation" id="submenu">
							<h5 class="table_heading">Partner Mapping List</h5><!---- table header ---->
							<form:form autocomplete="off" id="afPartnerMappingList" action="afPartnerMapping"
							commandName="afPartnerMappingList" >
							
                            <table class=" partner_info_table table-bordered" cellspacing="0" cellpadding="0" width="98%">
								<thead>
									<tr>
										<th class="name">Sl No.</th>
										<th>Partner Name </th>
										<th>Mapping Partner Name </th>
										<th class="width_section">Action</th>
									</tr>
								</thead><!----thead--->
								<tbody>
								<c:forEach items="${afPartnerMappingList}" var="mappingdetails" varStatus="i">
									<tr class="admin" 
									data-id="${mappingdetails.prtMapNo}" 
									data-prtname="${mappingdetails.partnerName}"
									data-prtno="${mappingdetails.partNO}"
									data-partnermappingname="${mappingdetails.partnerMappingName}"
									data-partnertype="${mappingdetails.partnerType}"
									data-allowlandingurl="${mappingdetails.allowLandingUrl}">
										<td>${i.count}</td>
										<td>${mappingdetails.partnerName}</td>
										<td>${mappingdetails.partnerMappingName}</td>
										
										<td>											
											<span class="editPartnerMappingBean"  data-target="#editPartnerMapping">
												<img src="${contextPath}/static/img/edit_icon.png" class="edit_wraper" ></img>
											</span>
											<span class="deleteMapping b" data-target="#myModal" id="deletePartnerMapping">
											 <img src="${contextPath}/static/img/delete_icon1.png" class="fa fa-trash fa-lg"></img></span>
										</td>
									
									</tr> 
									</c:forEach>
								</tbody><!---tbody--->
							</table><!---table-->	
                           </form:form>
                          
                            </nav>
                         </div>
                         
                         <!--section G ends  -->
                                                                 
                    </div><!---- tab-content --->
                </div>	<!---- user managemnt ---->	
            </section><!---- Section ---->	
            	
        </div>
<!-- <script src='https://cdn.tinymce.com/4/tinymce.min.js'></script>
 --><script type="text/javascript">
var partnerIDExist=false;
var activityIDExist=false;
var partnerNameExist=false;
var uploadFlag=false;
var imageUploadFlag=false;
var iconUploadFlag=false;
var aggregateIDExist=false;
var partnerMappingNameExist=false;

$('a.biurl').on('click', function () {
	var id = $(this).closest('tr').data('id');
	var partnerimageurl= $(this).closest('tr').data('partnerimageurl');
	var partnerpopupimageurl = $(this).closest('tr').data('partnerpopupimageurl');
	var imageUrl='${contextPath}'+'/admin/images/partner/'+partnerimageurl;
	var imagePopupUrl='${contextPath}'+'/admin/images/partner/'+partnerpopupimageurl;
	$('#partnerimage_').prop('src',imageUrl);
	$('#partnerpopupimage_').prop('src',imagePopupUrl);
	$('#myModal1_').data('id', id).modal('show');
});
var submitButtonClick=false;
$("#savePartner").click(function() {
submitButtonClick=true;
});
$(document).ready(function () {
	tinymce.init({
		   selector: 'textarea#termsAndCondition',
		   plugins: 'link',
		   height: '200px',
		    force_br_newlines : true,
		    force_p_newlines : false,
		    forced_root_block : '',
		    plugins: [
		    	'advlist autolink lists link image charmap print preview hr anchor pagebreak',
		    	'searchreplace wordcount visualblocks visualchars code fullscreen',
		    	'insertdatetime media nonbreaking save table contextmenu directionality',
		    	'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
		    	],
		   setup : function(ed){
		   	ed.on("NodeChange", function(e){
		           jQuery("textarea[id='termsAndCondition']").val(ed.getContent());
		          if(submitButtonClick)$("#partnerBeanNew").validate().element('#termsAndCondition');
		       });
		  }
		});
});

$(document).ready(function () {
	tinymce.init({
		   selector: 'textarea#jpmileDesc',
		   height: '200px',
		    force_br_newlines : true,
		    force_p_newlines : false,
		    forced_root_block : '',
		    plugins: [
		    	'advlist autolink lists link image charmap print preview hr anchor pagebreak',
		    	'searchreplace wordcount visualblocks visualchars code fullscreen',
		    	'insertdatetime media nonbreaking save table contextmenu directionality',
		    	'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
		    	],
		   setup : function(ed){
		   	ed.on("NodeChange", function(e){
		           jQuery("textarea[id='jpmileDesc']").val(ed.getContent());
		          if(submitButtonClick)$("#partnerBeanNew").validate().element('#jpmileDesc');
		       });
		  }
		});
});


$("#termsAndCondition").on('blur', function(){
	$("#partnerBeanNew").validate().element('#termsAndCondition');
});

$("#jpmileDesc").on('blur', function(){
	$("#partnerBeanNew").validate().element('#jpmileDesc');
});

	  
	


$("#ratePerMile").on('blur', function(){
if($("#ratePerMile").val()=="")
$("#ratePerMile").val("0.0");
});
var id='', partnerid='', partnername='', logoprtdisplayorder='', partnerlandingurl='', jpmile='', ratePerMile='',
hostpartner='', defaultpartner='', logopartner='', talkpartner='', readpartner='', partneractivationdate='',partnerdeactivationdate='',
partnerstatus='',partnerstatuscode='',partnerimageurl='',partnerpopupimageurl='',partnericonurl='',imageUrl='',imagePopupUrl='',iconUrl='',affiliateidname='',
referencetokenname='',sourceofinfoname='',activityid='',affiliateid='',referencetoken='',paramfour='',paramfourname='',
talkreadPartTileposition='',talkReadpartsearchurl='';
/* $('span.view_details').on('click', function () { */
	 $('#table').on('click', 'a.view_details', function (e) {
	var checka = table1.row( $(this).closest('tr')).data();
            id=table1.row( $(this).closest('tr')).data().prtNo;
            partnerid=table1.row( $(this).closest('tr')).data().partnerId;
            activityid=table1.row( $(this).closest('tr')).data().activityId;
            partnername=table1.row( $(this).closest('tr')).data().partnerName;
            talkReadpartsearchurl=table1.row( $(this).closest('tr')).data().logoPartnerSearchUrl;
            logoprttilepos=table1.row( $(this).closest('tr')).data().logoPrtTilePos;
            partnerimageurl=table1.row( $(this).closest('tr')).data().partnerImageUrl;            
            partnerpopupimageurl=table1.row( $(this).closest('tr')).data().partnerPopupImageUrl;            
            partnericonurl=table1.row( $(this).closest('tr')).data().partnerIconUrl;
            partneractivationdate=table1.row( $(this).closest('tr')).data().activationDate;
            partnerdeactivationdate=table1.row( $(this).closest('tr')).data().deactivationDate;
            partnerlandingurl=table1.row( $(this).closest('tr')).data().partnerLandingUrl;
            affiliateidname=table1.row( $(this).closest('tr')).data().affiliateIdName;
            sourceofinfoname=table1.row( $(this).closest('tr')).data().sourceOfInfoName;
            ratePerMile=table1.row( $(this).closest('tr')).data().ratePerMile;
            defaultpartner=table1.row( $(this).closest('tr')).data().defaultPartnerCode;
            hostpartner=table1.row( $(this).closest('tr')).data().hostPartnerCode;
            logopartner=table1.row( $(this).closest('tr')).data().logoPartnerCode;
            talkpartner=table1.row( $(this).closest('tr')).data().talkPartnerCode;
            readpartner=table1.row( $(this).closest('tr')).data().readPartnerCode;
            partnerstatus=table1.row( $(this).closest('tr')).data().partnerStatus;
            logoprtdisplayorder=table1.row( $(this).closest('tr')).data().logoPrtDisplayOrder;
            affiliateid=table1.row( $(this).closest('tr')).data().affiliateId;
             imageUrl='${contextPath}'+'/admin/images/partner/'+partnerimageurl;
             iconUrl='${contextPath}'+'/admin/images/partner/'+partnericonurl;
             imagePopupUrl='${contextPath}'+'/admin/images/partner/'+partnerpopupimageurl;
	/* var id = $(this).closest('tr').data('id');
	var partnerid = $(this).closest('tr').data('partnerid');
	var partnername= $(this).closest('tr').data('partnername');
	var logoprtdisplayorder= $(this).closest('tr').data('logoprtdisplayorder');
	var partnerlandingurl= $(this).closest('tr').data('partnerlandingurl');
	var jpmile= $(this).closest('tr').data('jpmile');
	var ratePerMile= $(this).closest('tr').data('ratepermile');
	var hostpartner= $(this).closest('tr').data('hostpartner');
	var defaultpartner= $(this).closest('tr').data('defaultpartner');
	var logopartner= $(this).closest('tr').data('logopartner');
	
	var talkpartner= $(this).closest('tr').data('talkpartner');
	var readpartner= $(this).closest('tr').data('readpartner');
	
	var partneractivationdate= $(this).closest('tr').data('partneractivationdate');
	var partnerdeactivationdate= $(this).closest('tr').data('partnerdeactivationdate');
	var partnerstatus= $(this).closest('tr').data('partnerstatus');
	var partnerstatuscode= $(this).closest('tr').data('partnerstatuscode');
	var partnerstatusclass= $(this).closest('tr').data('partnerstatusclass');
	var partnerimageurl= $(this).closest('tr').data('partnerimageurl');
	var partnericonurl= $(this).closest('tr').data('partnericonurl');
	var imageUrl='${contextPath}'+'/admin/images/partner/'+partnerimageurl;
	var iconUrl='${contextPath}'+'/admin/images/partner/'+partnericonurl;
	var affiliateidname= $(this).closest('tr').data('affiliateidname');
	var referencetokenname= $(this).closest('tr').data('referencetokenname');
	var sourceofinfoname= $(this).closest('tr').data('sourceofinfoname');
	var affiliateid= $(this).closest('tr').data('affiliateid');
	var referencetoken= $(this).closest('tr').data('referencetoken');
	var sourceofinfo= $(this).closest('tr').data('sourceofinfo');
	var activityid= $(this).closest('tr').data('activityid');
	var paramfour= $(this).closest('tr').data('paramfour');
	var paramfourname= $(this).closest('tr').data('paramfourname');
	
	var talkreadPartTileposition= $(this).closest('tr').data('logoprttilepos');
	var talkReadpartsearchurl = $(this).closest('tr').data('logopartnersearchurl'); */
	
	
		$('#partnerid').text(partnerid);
		$('#partnername').text(partnername);
		$('#logoprtdisplayorder').text(logoprtdisplayorder);
		$('#partnerlandingUrl').text(partnerlandingurl);
		$('#ratePerMiles').text(ratePerMile);
		$('#hostpartner').text(hostpartner==1?"Yes":"No");
		$('#defaultpartner').text(defaultpartner==1?"Yes":"No");
		$('#logopartner').text(logopartner==1?"Yes":"No");
		 
		$('#talkpartner').text(talkpartner==1?"Yes":"No");
		$('#readpartner').text(readpartner==1?"Yes":"No");
		$('#partnerstatus').text(partnerstatus);
		
		$('#partnerimage').prop('src',imageUrl);
		$('#partnerimageurl').text(partnerimageurl);
		
		$('#partnerpopupimage').prop('src',imagePopupUrl);
		$('#partnerpopupimageurl').text(partnerpopupimageurl);
		
		
		$('#partnericon').prop('src',iconUrl);
		$('#partnericonurl').text(partnericonurl);
		$('#partneractivationdate').text(partneractivationdate);
		$('#partnerdeactivationdate').text(partnerdeactivationdate);
		/* $('#partnerstatus').html("<font class="+partnerstatusclass+">"+partnerstatus);
		$('#partnerstatuscode').text(partnerstatuscode); */
		$('#affiliateID').text(affiliateid);
		$('#referencetoken').text(referencetoken);
		/* $('#sourceofInfo').text(sourceofinfo); */
		$('#affiliateIDName').text(affiliateidname);
		$('#referencetokenName').text(referencetokenname);
		$('#sourceofInfoName').text(sourceofinfoname);
		$('#activityid').text(activityid);
		$('#paramFour').text(paramfour);
		$('#paramFourName').text(paramfourname);
		$("#talkReadpartnersearchUrl").text(talkReadpartsearchurl);
		$("#talkreadParttileposition").text(talkreadPartTileposition);
	
	$('#myModal2').data('id',id).modal('show');
        
//        alert("data entered");
		
});
/* $('span.view_details_').on('click', function () { */
	 $('#table-1').on('click', 'a.view_details_', function (e) {
		 var checka = table2.row( $(this).closest('tr')).data();
			id=table2.row( $(this).closest('tr')).data().prtNo;
			partnerid=table2.row( $(this).closest('tr')).data().partnerId;
			activityid=table2.row( $(this).closest('tr')).data().activityId;
			partnername=table2.row( $(this).closest('tr')).data().partnerName;
			talkReadpartsearchurl=table2.row( $(this).closest('tr')).data().logoPartnerSearchUrl;
			logoprttilepos=table2.row( $(this).closest('tr')).data().logoPrtTilePos;
			partnerimageurl=table2.row( $(this).closest('tr')).data().partnerImageUrl;			
			partnerpopupimageurl=table2.row( $(this).closest('tr')).data().partnerPopupImageUrl;			
			partnericonurl=table2.row( $(this).closest('tr')).data().partnerIconUrl;
			partneractivationdate=table2.row( $(this).closest('tr')).data().activationDate;
			partnerdeactivationdate=table2.row( $(this).closest('tr')).data().deactivationDate;
			partnerlandingurl=table2.row( $(this).closest('tr')).data().partnerLandingUrl;
			affiliateidname=table2.row( $(this).closest('tr')).data().affiliateIdName;
			sourceofinfoname=table2.row( $(this).closest('tr')).data().sourceOfInfoName;
			ratePerMile=table2.row( $(this).closest('tr')).data().ratePerMile;
			defaultpartner=table2.row( $(this).closest('tr')).data().defaultPartnerCode;
			hostpartner=table2.row( $(this).closest('tr')).data().hostPartnerCode;
			logopartner=table2.row( $(this).closest('tr')).data().logoPartnerCode;
			talkpartner=table2.row( $(this).closest('tr')).data().talkPartnerCode;
			readpartner=table2.row( $(this).closest('tr')).data().readPartnerCode;
			partnerstatus=table2.row( $(this).closest('tr')).data().partnerStatus;
			logoprtdisplayorder=table2.row( $(this).closest('tr')).data().logoPrtDisplayOrder;
			affiliateid=table2.row( $(this).closest('tr')).data().affiliateId;
			logopartnersearchurl=table2.row( $(this).closest('tr')).data().logoPartnerSearchUrl;
			imageUrl='${contextPath}'+'/admin/images/partner/'+partnerimageurl;
			 iconUrl='${contextPath}'+'/admin/images/partner/'+partnericonurl;
			 imagePopupUrl='${contextPath}'+'/admin/images/partner/'+partnerpopupimageurl;
	 
/* 	var id = $(this).closest('tr').data('id_');
	var partnerid = $(this).closest('tr').data('partnerid');
	var partnername= $(this).closest('tr').data('partnername');
	var jpmile= $(this).closest('tr').data('jpmile');
	
	
	//var logopartnerlandingurl = $(this).closest('tr').data('logopartnerlandingurl');
	var partnerlandingURL = $(this).closest('tr').data('partnerlandingurl');
	var logopartnersearchurl = $(this).closest('tr').data('logopartnersearchurl');
	var logopartnerdisplayorder= $(this).closest('tr').data('logoprtdisplayorder');
	var logopartnertileposition= $(this).closest('tr').data('logoprttilepos');
	
	var ratePerMile= $(this).closest('tr').data('ratepermile');
	var hostpartner= $(this).closest('tr').data('hostpartner');
	var defaultpartner= $(this).closest('tr').data('defaultpartner');
	var logopartner= $(this).closest('tr').data('logopartner');
	var offlinepartner= $(this).closest('tr').data('offlinepartner');

	var partneractivationdate= $(this).closest('tr').data('partneractivationdate');
	var partnerdeactivationdate= $(this).closest('tr').data('partnerdeactivationdate');
	var partnerstatus= $(this).closest('tr').data('partnerstatus');
	var partnerstatuscode= $(this).closest('tr').data('partnerstatuscode');
	var partnerstatusclass= $(this).closest('tr').data('partnerstatusclass');
	var partnerimageurl= $(this).closest('tr').data('partnerimageurl');
	var partnericonurl= $(this).closest('tr').data('partnericonurl');
	var imageUrl='${contextPath}'+'/admin/images/partner/'+partnerimageurl;
	var iconUrl='${contextPath}'+'/admin/images/partner/'+partnericonurl;
	var affiliateid= $(this).closest('tr').data('affiliateid');
	var sourceofinfo= $(this).closest('tr').data('sourceofinfo');
	var referencetoken= $(this).closest('tr').data('referencetoken');
	var affiliateidname= $(this).closest('tr').data('affiliateidname');
	var sourceofinfoname= $(this).closest('tr').data('sourceofinfoname');
	var referencetokenname= $(this).closest('tr').data('referencetokenname');
	var activityid= $(this).closest('tr').data('activityid');
	var paramfour= $(this).closest('tr').data('paramfour');
	var paramfourname= $(this).closest('tr').data('paramfourname'); */
	hideViewoffline(offlinepartner);
	$('#partnerid_').text(partnerid);
	$('#partnername_').text(partnername);
	
	$('#ratePerMile_').text(ratePerMile);
	$('#hostpartner_').text(hostpartner==1?"Yes":"No");
	$('#defaultpartner_').text(defaultpartner==1?"Yes":"No");
	$('#logopartner_').text(logopartner==1?"Yes":"No");
	$('#offlinepartner').text(offlinepartner==1?"Yes":"No");
	$('#partneractivationdate_').text(partneractivationdate);
	$('#partnerdeactivationdate_').text(partnerdeactivationdate);
	/* $('#partnerstatus_').html("<font class="+partnerstatusclass+">"+partnerstatus);
	$('#partnerstatuscode_').text(partnerstatuscode); */
	$('#partnerstatus_').text(partnerstatus);
	$('#partnerimage_').prop('src',imageUrl);
	$('#partnerpopupimage_').prop('src',imagePopupUrl);
	$('#partnerimageurl_').text(partnerimageurl);
	$('#partnerpopupimageurl_').text(partnerpopupimageurl);
	
	$('#partnericon_').prop('src',iconUrl);
	$('#partnericonurl_').text(partnericonurl);
	$('#partnerLandingUrl_').text(partnerlandingurl);
	//(logopartnerLandingUrl!=='' || logopartnerLandingUrl!='undefined') && 
	if((logopartnersearchurl!=='' || logopartnersearchurl!='undefined') && (logopartnerdisplayorder!=='' || logopartnerdisplayorder!=='undefined') && (logopartnertileposition!=='' || logopartnertileposition!=='undefined'))
	{
	//$('#logopartnerlandingurl').text(logopartnerlandingurl);
	$('#logopartnersearchUrl').text(logopartnersearchurl);
	$('#logopartnerdisplayorder').text(logoprtdisplayorder);
	$('#logopartnertileposition').text(logoprttilepos);
	
	}
	$('#affiliateID_').text(affiliateid);
	//$('#sourceofInfo_').text(sourceofinfo);
	$('#referencetoken_').text(referencetoken);
	$('#affiliateIDName_').text(affiliateidname);
	$('#sourceOfInfoName_').text(sourceofinfoname);
	$('#referencetokenName_').text(referencetokenname);
	$('#activityid_').text(activityid);
	$('#paramFour_').text(paramfour);
	$('#paramFourName_').text(paramfourname);
	$('#myModal3').data('id_',id).modal('show');  
        
        
//        alert("data entered ==================>");
});

function hideViewoffline(offlinepartner){
	if(offlinepartner==1){
		$("#view_logoUrl").hide();
    	$("#view_logoPos").hide();
    	$("#view_rateperMileDiv").hide();
    	$("#view_offlineDiv").show();
    	$("#view_offlineUrl").show();
    	$("#view_offlinePos").show();
	}else{
		$("#view_logoUrl").show();
    	$("#view_logoPos").show();
    	$("#view_rateperMileDiv").show();
    	$("#view_offlineDiv").hide();
    	$("#view_offlineUrl").hide();
    	$("#view_offlinePos").hide();
    	} 	
}

/* $('span.editpartner').on('click', function () { */
	var partnerJpmileReconPeriod='',partnerdisplayorder='',editpartnertilpos='',
	sourceofinfo='',jpmiledesc='',editpartnersearchurl='',partnerstatuscode='',termsAndCondition='',jpmiledesc='',
	partnerDesc='',displayPrtPopup='',partnerJpmileReconPeriod='',paramfour='',paramfourname='',allowJpnumber='',aggregateId=''
		,referencetoken='',referencetokenName='';
 $('#table').on('click', 'a.editpartner', function (e) {
	imageUploadFlag=false;
	iconUploadFlag=false;
	var checka = table1.row( $(this).closest('tr')).data();
	id=table1.row( $(this).closest('tr')).data().prtNo;
	partnerid=table1.row( $(this).closest('tr')).data().partnerId;
	activityid=table1.row( $(this).closest('tr')).data().activityId;
	partnername=table1.row( $(this).closest('tr')).data().partnerName;
	talkReadpartsearchurl=table1.row( $(this).closest('tr')).data().logoPartnerSearchUrl;
	logoprttilepos=table1.row( $(this).closest('tr')).data().logoPrtTilePos;
	partnerimageurl=table1.row( $(this).closest('tr')).data().partnerImageUrl;
	partnerpopupimageurl=table1.row( $(this).closest('tr')).data().partnerPopupImageUrl;	
	partnericonurl=table1.row( $(this).closest('tr')).data().partnerIconUrl;
	partneractivationdate=table1.row( $(this).closest('tr')).data().activationDate;
	partnerdeactivationdate=table1.row( $(this).closest('tr')).data().deactivationDate;
	partnerlandingurl=table1.row( $(this).closest('tr')).data().partnerLandingUrl;
	affiliateidname=table1.row( $(this).closest('tr')).data().affiliateIdName;
	sourceofinfoname=table1.row( $(this).closest('tr')).data().sourceOfInfoName;
	ratePerMile=table1.row( $(this).closest('tr')).data().ratePerMile;
	defaultpartner=table1.row( $(this).closest('tr')).data().defaultPartnerCode;
	hostpartner=table1.row( $(this).closest('tr')).data().hostPartnerCode;
	logopartner=table1.row( $(this).closest('tr')).data().logoPartnerCode;
	talkpartner=table1.row( $(this).closest('tr')).data().talkPartnerCode;
	readpartner=table1.row( $(this).closest('tr')).data().readPartnerCode;
	partnerstatus=table1.row( $(this).closest('tr')).data().partnerStatus;
	if(partnerstatus == 'Active'){
		partnerstatuscode = 1;
		}
	else{
		partnerstatuscode =0;
	}
	logoprtdisplayorder=table1.row( $(this).closest('tr')).data().logoPrtDisplayOrder;
	affiliateid=table1.row( $(this).closest('tr')).data().affiliateId;
	 imageUrl='${contextPath}'+'/admin/images/partner/'+partnerimageurl;
	 iconUrl='${contextPath}'+'/admin/images/partner/'+partnericonurl;
	 imagePopupUrl='${contextPath}'+'/admin/images/partner/'+partnerpopupimageurl;	 
	  termsAndCondition =table1.row( $(this).closest('tr')).data().termsAndCondition;
		 jpmiledesc= table1.row( $(this).closest('tr')).data().jpmileDesc;
		 partnerDesc= table1.row( $(this).closest('tr')).data().partnerDesc;
		 displayPrtPopup =table1.row( $(this).closest('tr')).data().displayPrtPopup;
		 partnerJpmileReconPeriod=table1.row( $(this).closest('tr')).data().jpmileReconPeriod;
		 paramfour= table1.row( $(this).closest('tr')).data().paramFour;
		  paramfourname= table1.row( $(this).closest('tr')).data().paramFourName;
		  sourceofinfo= table1.row( $(this).closest('tr')).data().sourceOfInfo;
		  allowJpnumber =table1.row( $(this).closest('tr')).data().allowJpnumber;
		  aggregateId=table1.row( $(this).closest('tr')).data().aggregateId;
		   referencetokenName= table1.row( $(this).closest('tr')).data().referenceTokenName;
		   referencetoken= table1.row( $(this).closest('tr')).data().referenceToken;
	/* var id = $(this).closest('tr').data('id');
	var partnerid = $(this).closest('tr').data('partnerid');
	var partnername= $(this).closest('tr').data('partnername');
	var ratePerMile= $(this).closest('tr').data('ratepermile');
	var partnerdisplayorder= $(this).closest('tr').data('partnerdisplayorder');
	
	var hostpartner= $(this).closest('tr').data('hostpartner');
	var defaultpartner= $(this).closest('tr').data('defaultpartner');	
	var talkpartner= $(this).closest('tr').data('talkpartner');
	var readpartner= $(this).closest('tr').data('readpartner');
	var partneractivationdate= $(this).closest('tr').data('partneractivationdate');
	var partnerdeactivationdate= $(this).closest('tr').data('partnerdeactivationdate');
	var partnerstatus= $(this).closest('tr').data('partnerstatus');
	var partnerstatuscode= $(this).closest('tr').data('partnerstatuscode');
	var partnerstatusclass= $(this).closest('tr').data('partnerstatusclass');
	var partnerimageurl= $(this).closest('tr').data('partnerimageurl');
	var partnericonurl= $(this).closest('tr').data('partnericonurl');
	var imageUrl='${contextPath}'+'/admin/images/partner/'+partnerimageurl;
	var iconUrl='${contextPath}'+'/admin/images/partner/'+partnericonurl;
	var partnerLandingUrl= $(this).closest('tr').data('partnerlandingurl');
	var affiliateid= $(this).closest('tr').data('affiliateid');
	var sourceofinfo= $(this).closest('tr').data('sourceofinfo');
	var referencetoken= $(this).closest('tr').data('referencetoken');
	var affiliateidname= $(this).closest('tr').data('affiliateidname');
	var sourceofinfoname= $(this).closest('tr').data('sourceofinfoname');
	var referencetokenname= $(this).closest('tr').data('referencetokenname');
	var partnerDesc= $(this).closest('tr').data('partnerdesc');
	var activityid= $(this).closest('tr').data('activityid');
	var paramfour= $(this).closest('tr').data('paramfour');
	var paramfourname= $(this).closest('tr').data('paramfourname');
	var partnerJpmileReconPeriod = $(this).closest('tr').data('partnerjpmilereconperiod');
	var termsAndCondition = $(this).closest('tr').data('termsandcondition');
	var jpmiledesc= $(this).closest('tr').data('jpmiledesc');
	var allowJpnumber = $(this).closest('tr').data('allowjpnumber');
	var displayPrtPopup = $(this).closest('tr').data('displayprtpopup');
	var editpartnersearchurl = $(this).closest('tr').data('logopartnersearchurl');
	var editpartnertilpos = $(this).closest('tr').data('logoprttilepos'); */
	  //edit_talkReadTilPos  edit_talkreadPartUrl logoprttilepos   logopartnersearchurl
	$("#edit_jpmileReconPeriod").val(partnerJpmileReconPeriod);
	$('#edit_partnerid').val(partnerid);
	$('#edit_partnerhiddenid').val(partnerid);
	$('#edit_partnername').val(partnername);
	$('#edit_partnerhiddenname').val(partnername);	
	$('#edit_ratepermile').val(ratePerMile);
	$('#edit_aggregateId').val(aggregateId);
	$('#edit_prtDispOrd').val(logoprtdisplayorder);
	$('#edit_prtDispOrdHidden').val(logoprtdisplayorder);
	//edit_talkreadPartUrl edit_talkReadTilPos
	if(hostpartner==1){
		$('#edit_hostpartner').prop("checked", "checked");
		$("#edit_talkreadPartUrl").rules('remove', 'required');
		$("#edit_talkReadTilPos").rules('remove', 'required');
		$("#talkreadmap").hide();
	}
	if(defaultpartner==1){
	$('#edit_defaultpartner').prop("checked", "checked");
	$("#edit_talkreadPartUrl").rules('add', 'required');
	$("#edit_talkReadTilPos").rules('add', 'required');
	//$("#talkreadmap").hide();
	}
	if(talkpartner==1)
		$('#edit_talkpartner').prop("checked", "checked");
	
	if(readpartner==1)
		$('#edit_readpartner').prop("checked", "checked"); 
	
	if(readpartner==1 || talkpartner==1){
		$("#talkreadmap").show();
		$('#edit_ratepermile').rules('remove', 'required');
		$('#edit_referenceTokenName').rules('remove', 'required');
		$('#edit_partnerDesc').rules('add', 'required');
		$("#edit_talkreadPartUrl").rules('add', 'required');
		$("#edit_talkReadTilPos").rules('add', 'required');
	}
	
	$('#edit_logoPrtTilePosHidden').val(editpartnertilpos);
	$('#datepicker11').val(partneractivationdate);
	$('#datepicker12').val(partnerdeactivationdate);
	$('#edit_partnerimageurl').val(partnerimageurl);
	$('#edit_partnericonurl').val(partnericonurl);
	$('#edit_partnerpopupimageurl').val(partnerpopupimageurl);
	$('#edit_partnericonurlhidden').val(partnericonurl);
	$('#editPartnerImage_').prop('src',imageUrl);
	$('#editPartnerPopupImage_').prop('src',imagePopupUrl);
	$('#editPartnerIcon_').prop('src',iconUrl);
	$('#edit_partnerstatus_'+partnerstatuscode).prop("checked","checked");
	$('#edit_PrtLandingUrl').val(partnerlandingurl);
	$('#edit_affiliateId').val(affiliateid);
	$('#edit_sourceOfInfo').val(sourceofinfo);
	$('#edit_referenceToken').val(referencetoken);
	$('#edit_affiliateIdName').val(affiliateidname);
	$('#edit_sourceOfInfoName').val(sourceofinfoname);
	$('#edit_referenceTokenName').val(referencetokenName);
	$('#edit_jpmileDesc').val(jpmiledesc);
	$('#edit_partnerDesc').val(partnerDesc);
	$('#edit_activityid').val(activityid);
	$('#edit_paramFour').val(paramfour);
	$('#edit_paramFourName').val(paramfourname);	
	termsAndCondition = decodeURIComponent(termsAndCondition).split('+').join(" ");
	jpmiledesc = decodeURIComponent(jpmiledesc).split('+').join(" ");
	$("#edit_termsAndCondition").val(termsAndCondition);
	$("#edit_jpmileDesc").val(jpmiledesc);
	$("#edit_allowJpnumber").val(allowJpnumber);
	
	$("#edit_talkreadPartUrl").val(talkReadpartsearchurl); 
	$("#edit_talkReadTilPos").val(logoprttilepos);
	if(allowJpnumber == "0"){
		$("#edit_allowJpnumber").prop("checked", false);
	}
	else{
		$("#edit_allowJpnumber").prop("checked", true);
	}
	
	$("#edit_displayPrtPopup").val(displayPrtPopup);
	if(displayPrtPopup == "0"){
		$("#edit_displayPrtPopup").prop("checked", false);
	}
	else{
		$("#edit_displayPrtPopup").prop("checked", true);
	}
	
	if(partnerimageurl != ""){
		imageUploadFlag=true;
	}
	if(partnerpopupimageurl != ""){
		imageUploadFlag=true;
	}
	if(partnericonurl != ""){
		iconUploadFlag=true;
	}
	var submitButtonClick=false;
	tinymce.init({
		   selector: 'textarea#edit_termsAndCondition',
		   height: '200px',
		   plugins: 'link',
		    force_br_newlines : true,
		    force_p_newlines : false,
		    forced_root_block : '',
		    plugins: [
		    	'advlist autolink lists link image charmap print preview hr anchor pagebreak',
		    	'searchreplace wordcount visualblocks visualchars code fullscreen',
		    	'insertdatetime media nonbreaking save table contextmenu directionality',
		    	'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
		    	],
		    setup : function(ed){
		   	ed.on("NodeChange", function(e){
		           jQuery("textarea[id='edit_termsAndCondition']").val(ed.getContent());
			           if($("#editPartnerBean").validate().element('#edit_termsAndCondition'));
			           $(document).on('focusin', function(e) {
			        	    if ($(e.target).closest(".mce-window").length) {
			        	        e.stopImmediatePropagation();
			        	    }
			        	});
		       });
		  }
		});
	
	tinymce.init({
		   selector: 'textarea#edit_jpmileDesc',
		    height: '200px',
		    force_br_newlines : true,
		    force_p_newlines : false,
		    forced_root_block : '',
		    plugins: [
		    	'advlist autolink lists link image charmap print preview hr anchor pagebreak',
		    	'searchreplace wordcount visualblocks visualchars code fullscreen',
		    	'insertdatetime media nonbreaking save table contextmenu directionality',
		    	'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
		    	],
		    setup : function(ed){
		   	ed.on("NodeChange", function(e){
		           jQuery("textarea[id='edit_jpmileDesc']").val(ed.getContent());
			           if($("#editPartnerBean").validate().element('#edit_jpmileDesc'));
			           $(document).on('focusin', function(e) {
			        	    if ($(e.target).closest(".mce-window").length) {
			        	        e.stopImmediatePropagation();
			        	    }
			        	});
		       });
		  }
		});
	
	
	
	
	$('#myModal1').data('id',id).modal('show');  
});
 var partnerJpmileReconPeriod='',partnerdisplayorder='',editpartnertilpos='',
	sourceofinfo='',jpmiledesc='',editpartnersearchurl='',partnerstatuscode='',termsAndCondition='',jpmiledesc='',
	partnerDesc='',displayPrtPopup='',partnerJpmileReconPeriod='',paramfour='',paramfourname='',allowJpnumber='';
    var referenceTokenName='',referencetoken='';
/* $('span.editpartner_').on('click', function () { */
	var offlinepartner='';
	 $('#table-1').on('click', 'a.editpartner_', function (e) {
	imageUploadFlag=false;
	iconUploadFlag=false;
	imagePopupUploadFlag=false;
	/* var id = $(this).closest('tr').data('id_');
	var partnerid = $(this).closest('tr').data('partnerid');
	var partnername= $(this).closest('tr').data('partnername');
	var ratePerMile= $(this).closest('tr').data('ratepermile');
	var hostpartner= $(this).closest('tr').data('hostpartner');
	var defaultpartner= $(this).closest('tr').data('defaultpartner');
	var partneractivationdate= $(this).closest('tr').data('partneractivationdate');
	var partnerdeactivationdate= $(this).closest('tr').data('partnerdeactivationdate');
	var partnerstatus= $(this).closest('tr').data('partnerstatus');
	var partnerstatuscode= $(this).closest('tr').data('partnerstatuscode');
	var partnerstatusclass= $(this).closest('tr').data('partnerstatusclass');
	
	var talkpartner= $(this).closest('tr').data('talkpartner');
	var readpartner= $(this).closest('tr').data('readpartner');

	//var logopartnerlandingurl = $(this).closest('tr').data('logopartnerlandingurl');
	var partnerLandingUrl = $(this).closest('tr').data('partnerlandingurl');
	var logopartnersearchurl = $(this).closest('tr').data('logopartnersearchurl');
	var logopartnerdisplayorder= $(this).closest('tr').data('logoprtdisplayorder');
	var logopartnertileposition= $(this).closest('tr').data('logoprttilepos');
	var logopartner= $(this).closest('tr').data('logopartner');
	var offlinepartner= $(this).closest('tr').data('offlinepartner');
	var partnerimageurl= $(this).closest('tr').data('partnerimageurl');
	var partnericonurl= $(this).closest('tr').data('partnericonurl');
	var imageUrl='${contextPath}'+'/admin/images/partner/'+partnerimageurl;
	var iconUrl='${contextPath}'+'/admin/images/partner/'+partnericonurl;
	var affiliateid= $(this).closest('tr').data('affiliateid');
	var sourceofinfo= $(this).closest('tr').data('sourceofinfo');
	var referencetoken= $(this).closest('tr').data('referencetoken');
	var affiliateidname= $(this).closest('tr').data('affiliateidname');
	var sourceofinfoname= $(this).closest('tr').data('sourceofinfoname');
	var referencetokenname= $(this).closest('tr').data('referencetokenname');
	var jpmiledesc= $(this).closest('tr').data('jpmiledesc');
	var partnerDesc= $(this).closest('tr').data('partnerdesc');
	var activityid= $(this).closest('tr').data('activityid');
	var paramfour= $(this).closest('tr').data('paramfour');
	var paramfourname= $(this).closest('tr').data('paramfourname');
	var partnerJpmileReconPeriod = $(this).closest('tr').data('partnerjpmilereconperiod');
	var termsAndCondition = $(this).closest('tr').data('termsandcondition');
	var allowJpnumber = $(this).closest('tr').data('allowjpnumber');
	var displayPrtPopup = $(this).closest('tr').data('displayprtpopup'); */
	 var checka = table2.row( $(this).closest('tr')).data();
	id=table2.row( $(this).closest('tr')).data().prtNo;
	partnerid=table2.row( $(this).closest('tr')).data().partnerId;
	activityid=table2.row( $(this).closest('tr')).data().activityId;
	partnername=table2.row( $(this).closest('tr')).data().partnerName;
	logoprttilepos=table2.row( $(this).closest('tr')).data().logoPrtTilePos;
	partnerimageurl=table2.row( $(this).closest('tr')).data().partnerImageUrl;
	partnerimagepopupurl=table2.row( $(this).closest('tr')).data().partnerPopupImageUrl;	
	partnericonurl=table2.row( $(this).closest('tr')).data().partnerIconUrl;
	partneractivationdate=table2.row( $(this).closest('tr')).data().activationDate;
	partnerdeactivationdate=table2.row( $(this).closest('tr')).data().deactivationDate;
	partnerlandingurl=table2.row( $(this).closest('tr')).data().partnerLandingUrl;
	affiliateidname=table2.row( $(this).closest('tr')).data().affiliateIdName;
	sourceofinfoname=table2.row( $(this).closest('tr')).data().sourceOfInfoName;
	ratePerMile=table2.row( $(this).closest('tr')).data().ratePerMile;
	defaultpartner=table2.row( $(this).closest('tr')).data().defaultPartnerCode;
	hostpartner=table2.row( $(this).closest('tr')).data().hostPartnerCode;
	logopartner=table2.row( $(this).closest('tr')).data().logoPartnerCode;
	talkpartner=table2.row( $(this).closest('tr')).data().talkPartnerCode;
	readpartner=table2.row( $(this).closest('tr')).data().readPartnerCode;
	partnerstatus=table2.row( $(this).closest('tr')).data().partnerStatus;
	if(partnerstatus == 'Active'){
		partnerstatuscode = 1;
		}
	else{
		partnerstatuscode =0;
	}
	logoprtdisplayorder=table2.row( $(this).closest('tr')).data().logoPrtDisplayOrder;
	affiliateid=table2.row( $(this).closest('tr')).data().affiliateId;
	logopartnersearchurl=table2.row( $(this).closest('tr')).data().logoPartnerSearchUrl;
	imageUrl='${contextPath}'+'/admin/images/partner/'+partnerimageurl;
	 iconUrl='${contextPath}'+'/admin/images/partner/'+partnericonurl;
	 imagePopupUrl='${contextPath}'+'/admin/images/partner/'+partnerpopupimageurl;
	 termsAndCondition =table2.row( $(this).closest('tr')).data().termsAndCondition;
	 jpmiledesc= table2.row( $(this).closest('tr')).data().jpmileDesc;
	 partnerDesc= table2.row( $(this).closest('tr')).data().partnerDesc;
	 displayPrtPopup =table2.row( $(this).closest('tr')).data().displayPrtPopup;
	 partnerJpmileReconPeriod=table2.row( $(this).closest('tr')).data().jpmileReconPeriod;
	 paramfour= table2.row( $(this).closest('tr')).data().paramFour;
	  paramfourname= table2.row( $(this).closest('tr')).data().paramFourName;
	  sourceofinfo= table2.row( $(this).closest('tr')).data().sourceOfInfo;
	  allowJpnumber =table2.row( $(this).closest('tr')).data().allowJpnumber;
	  offlinepartner=table2.row( $(this).closest('tr')).data().offLinePartnerCode
	  aggregateId=table2.row( $(this).closest('tr')).data().aggregateId;
	  referenceTokenName=table2.row( $(this).closest('tr')).data().referenceTokenName
	  referencetoken= table2.row( $(this).closest('tr')).data().referenceToken;
	 hideEditoffline(offlinepartner);
	$("#edit_logo_jpmileReconPeriod").val(partnerJpmileReconPeriod);
	$('#edit_partnerid_').val(partnerid);
	$('#edit_partnerhiddenid_').val(partnerid);
	$('#edit_partnername_').val(partnername);
	$('#edit_partnerhiddenname_').val(partnername);
	$('#edit_logo_aggregateId').val(aggregateId);
	//$('#edit_logoPrtLandingUrl').val(logopartnerlandingurl); 
	$('#edit_logoPrtLandingUrl').val(partnerlandingurl);
	$('#edit_logoPrtSearchUrl').val(logopartnersearchurl);
	$('#edit_logoPrtDispOrd').val(logoprtdisplayorder);
	$('#edit_logoPrtDispOrdHidden').val(logopartnerdisplayorder);
	$('#edit_logoPrtTilePos').val(logoprttilepos);
	$('#edit_logoPrtTilePosHidden').val(logoprttilepos);	
	$('#edit_ratepermile_').val(ratePerMile);
	
	if(hostpartner==1)
		$('#edit_hostpartner_').prop("checked", "checked");
	if(defaultpartner==1)
		$('#edit_defaultpartner_').prop("checked", "checked");
	if(logopartner==1)
		$('#edit_logopartner').prop("checked", "checked");
	if(offlinepartner==1)
		$('#edit_offlinepartner').prop("checked", "checked");
	
	if(talkpartner==1)
		$('#edit_talkpartner').prop("checked", "checked");
	if(readpartner==1)
		$('#edit_readpartner').prop("checked", "checked");
	
	
	$('#datepicker11_').val(partneractivationdate);
	$('#datepicker12_').val(partnerdeactivationdate);
	$('#edit_partnerimageurl_').val(partnerimageurl);
	$('#edit_partnerpopupimageurl_').val(partnerpopupimageurl);
	$('#edit_partnericonurl_').val(partnericonurl);
	
	
	
	$('#editPartnerImage__').prop('src',imageUrl);
	$('#editPartnerPopupImage__').prop('src',imagePopupUrl);
	
	$('#editPartnerIcon__').prop('src',iconUrl);
	$('#edit_partnerstatus__'+partnerstatuscode).prop("checked","checked");	
	$('#edit_affiliateId_').val(affiliateid);
	$('#edit_sourceOfInfo_').val(sourceofinfo);
	$('#edit_referenceToken_').val(referencetoken);
	$('#edit_affiliateIdName_').val(affiliateidname);
	$('#edit_sourceOfInfoName_').val(sourceofinfoname);
	$('#edit_referenceTokenName_').val(referenceTokenName);
	
	$('#edit_logoPartnerDesc').val(partnerDesc);
	$('#edit_activityid_').val(activityid);
	$('#edit_paramFour_').val(paramfour);
	$('#edit_paramFourName_').val(paramfourname);	
	termsAndCondition = decodeURIComponent(termsAndCondition).split('+').join(" ");
	$("#edit_logoTermsAndCondition").val(termsAndCondition);
	jpmiledesc = decodeURIComponent(jpmiledesc).split('+').join(" ");
	$('#edit_logoJpmileDesc').val(jpmiledesc);
	$("#edit_logoAllowJpnumber").val(allowJpnumber);
	if(allowJpnumber == "0"){
		$("#edit_logoAllowJpnumber").prop("checked", false);
	}
	else{
		$("#edit_logoAllowJpnumber").prop("checked", true);
	}
	
	$("#edit_logoDisplayPrtPopup").val(displayPrtPopup);
	if(displayPrtPopup == "0"){
		$("#edit_logoDisplayPrtPopup").prop("checked", false);
	}
	else{
		$("#edit_logoDisplayPrtPopup").prop("checked", true);
	}
	
	if(partnerimageurl != ""){
		imageUploadFlag=true;
	}
	if(partnerpopupimageurl != ""){
		imageUploadFlag=true;
	}
	if(partnericonurl != ""){
		iconUploadFlag=true;
	}
	
	tinymce.init({
		   selector: 'textarea#edit_logoTermsAndCondition',
		   height: '200px',
		   plugins: 'link',
		    force_br_newlines : true,
		    force_p_newlines : false,
		    forced_root_block : '',
		    plugins: [
		    	'advlist autolink lists link image charmap print preview hr anchor pagebreak',
		    	'searchreplace wordcount visualblocks visualchars code fullscreen',
		    	'insertdatetime media nonbreaking save table contextmenu directionality',
		    	'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
		    	],
		    setup : function(ed){
		   	ed.on("NodeChange", function(e){
		           jQuery("textarea[id='edit_logoTermsAndCondition']").val(ed.getContent());
			           if($("#editLogoPartnerBean").validate().element('#edit_logoTermsAndCondition'));
			           $(document).on('focusin', function(e) {
			        	    if ($(e.target).closest(".mce-window").length) {
			        	        e.stopImmediatePropagation();
			        	    }
			        	});
		       });
		  }
		});
	

tinymce.init({
	   selector: 'textarea#edit_logoJpmileDesc',
	    height: '200px',
	    force_br_newlines : true,
	    force_p_newlines : false,
	    forced_root_block : '',
	    plugins: [
	    	'advlist autolink lists link image charmap print preview hr anchor pagebreak',
	    	'searchreplace wordcount visualblocks visualchars code fullscreen',
	    	'insertdatetime media nonbreaking save table contextmenu directionality',
	    	'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
	    	],
	    setup : function(ed){
	   	ed.on("NodeChange", function(e){
	           jQuery("textarea[id='edit_logoJpmileDesc']").val(ed.getContent());
		           if($("#editLogoPartnerBean").validate().element('#edit_logoJpmileDesc'));
		           $(document).on('focusin', function(e) {
		        	    if ($(e.target).closest(".mce-window").length) {
		        	        e.stopImmediatePropagation();
		        	    }
		        	});
	       });
	  }
	});
$('#myModal_Logo_').data('id_',id).modal('show');  
});


function hideEditoffline(offlinepartner){
	
	if(offlinepartner==1){
		$("#edit_ratepermile_Div").hide();
    	$("#edit_logoSrchUrl").hide();
    	$("#edit_logoTilPos").hide();
    	$("#edit_jpmileReconDiv").hide();
    	$("#edit_offLineSrchUrl").show();
    	$("#edit_offLineTilPos").show();
    	$('#edit_referenceTokenName_').rules('remove', 'required');
    	$('#edit_logoPartnerDesc').rules('add', 'required');
	}else{
		$("#edit_ratepermile_Div").show();
    	$("#edit_logoSrchUrl").show();
    	$("#edit_logoTilPos").show();
    	$("#edit_jpmileReconDiv").show();
    	$("#edit_offLineSrchUrl").hide();
    	$("#edit_offLineTilPos").hide();
    	$('#edit_logoPartnerDesc').rules('remove', 'required');
	} 	
}


$('#updatePartner').click(function () {
	var id = $('#myModal1').data('id');
	var json={};
	var startdate,enddate;
	startdate=$("#datepicker11").val();
	enddate=$("#datepicker12").val();
	json["partnerActivationDate"]=startdate;
	json["partnerDeactivationDate"]=enddate;
	json["partnerNo"]=id;
	json["partnerId"]=$("#edit_partnerid").val();
	json["partnerName"]=$("#edit_partnername").val();
	json["ratePermile"]=$("#edit_ratepermile").val();
	json["logoPartnerDispOrd"]=$("#edit_prtDispOrd").val();
	json["talkreadsearchurl"]=$("#edit_talkreadPartUrl").val();
	json["talktilepos"]=$("#edit_talkReadTilPos").val(); 
	var hp = $('#edit_hostpartner:checked').val();
	var dp = $('#edit_defaultpartner:checked').val();
	json["hostPartner"]=(hp=="on")?"1":"0";	
	json["defaultPartner"]=(dp=="on")?"1":"0";	
	json["partnerImageUrl"]=$("#edit_partnerimageurl").val();	
	json["partnerPopupImageUrl"]=$("#edit_partnerpopupimageurl").val();	
	json["partnerIconUrl"]=$("#edit_partnericonurl").val();
	json["partnerStatus"]=$('input[name=edit_partnerstatus]:checked').val();
	json["partnerLandingUrl"]=$("#edit_PrtLandingUrl").val();
	json["affiliateId"]=$("#edit_affiliateId").val();
	json["sourceOfInfo"]=$("#edit_sourceOfInfo").val();
	json["referenceToken"]=$("#edit_referenceToken").val();
	json["affiliateIdName"]=$("#edit_affiliateIdName").val();
	json["sourceOfInfoName"]=$("#edit_sourceOfInfoName").val();
	json["referenceTokenName"]=$("#edit_referenceTokenName").val();
	//json["jpmileDesc"]=$("#edit_jpmileDesc").val();
	json["partnerDesc"]=$("#edit_partnerDesc").val();
	json["activityId"]=$("#edit_activityid").val();
	json["paramFour"]=$("#edit_paramFour").val();
	json["paramFourName"]=$("#edit_paramFourName").val();
	json["jpmileReconPeriod"]=$("#edit_jpmileReconPeriod").val();
	json["termsAndCondition"]=$("#edit_termsAndCondition").val();
	json["jpmileDesc"]=$("#edit_jpmileDesc").val();
	json["displayPrtPopup"]=$("#edit_displayPrtPopup").val();
	json["allowJpnumber"]=$("#edit_allowJpnumber").val();
	json["aggregateId"]=$("#edit_aggregateId").val();
	var partnerType = $('#partnerType').val();
	/*$("#editPartnerBean").valid()*/
	var formData = new FormData();
	formData.append('json',JSON.stringify(json));
	
	//if($("#editPartnerBean").valid()){
	
	if($("#editPartnerBean").validate().element('#edit_partnerid')
			&& $("#editPartnerBean").validate().element('#edit_partnername') 
			&& $("#editPartnerBean").validate().element('#edit_ratepermile')
			&& $("#editPartnerBean").validate().element('#edit_prtDispOrd')
			&& $("#editPartnerBean").validate().element('#edit_partnerimageurl')
			&& $("#editPartnerBean").validate().element('#edit_partnerpopupimageurl')			
			&& $("#editPartnerBean").validate().element('#edit_PrtLandingUrl')
			&& $("#editPartnerBean").validate().element('#editPartnerImage')
			&& $("#editPartnerBean").validate().element('#editPartnerPopupImage')
			&& $("#editPartnerBean").validate().element('#edit_partnericonurl')
			&& $("#editPartnerBean").validate().element("#edit_partnerimageurlHidden")
			&& $("#editPartnerBean").validate().element("#edit_partnerpopupimageurlHidden")
			&& $("#editPartnerBean").validate().element("#edit_partnericonurlhidden")
			&& $("#editPartnerBean").validate().element('#editPartnerIcon')
			&& $("#editPartnerBean").validate().element('#datepicker11')
			&& $("#editPartnerBean").validate().element('#datepicker12')
			&& $("#editPartnerBean").validate().element('#edit_affiliateId')
			&& $("#editPartnerBean").validate().element('#edit_referenceToken')
			&& $("#editPartnerBean").validate().element('#edit_sourceOfInfo')
			&& $("#editPartnerBean").validate().element('#edit_affiliateIdName')
			&& $("#editPartnerBean").validate().element('#edit_referenceTokenName')
			&& $("#editPartnerBean").validate().element('#edit_sourceOfInfoName')
			&& $("#editPartnerBean").validate().element('#edit_partnericonurlhidden')
			&& $("#editPartnerBean").validate().element('#edit_jpmileDesc')
			&& $("#editPartnerBean").validate().element('#edit_partnerDesc')
			&& $("#editPartnerBean").validate().element('#edit_jpmileReconPeriod')
			&& $("#editPartnerBean").validate().element('#edit_activityid')
			&& $("#editPartnerBean").validate().element('#edit_paramFour')
			&& $("#editPartnerBean").validate().element('#edit_paramFourName')
			&& $("#editPartnerBean").validate().element('#edit_termsAndCondition')
			&& $("#editPartnerBean").validate().element('#edit_talkReadTilPos')
			&& $("#editPartnerBean").validate().element('#edit_talkreadPartUrl')
			//&& $("#editPartnerBean").validate().element('#edit_aggregateId')
			
			){	
		$.ajax({
			  url: "${contextPath}/admin/updatePartner",
			  data: formData,
			  dataType: 'text',
			  processData: false,
			  contentType: false,
			  type: 'POST',
			  success: function (response) {
				  
					  $('#myModal1').modal('hide');
					  window.location.href="${contextPath}/admin/partner?updateMessage="+response+"&partnerType="+partnerType;
				  
			  },
			  error: function (response) {
				  $('#myModal1').modal('hide');
				  window.location.href="${contextPath}/admin/partner?updateMessage=failure";
            	 }
			 });
	}	
});
$('#updateLogoPartner').click(function () {
	var id = $('#myModal_Logo_').data('id_');

	var json={};
	var startdate,enddate;
	startdate=$("#datepicker11_").val();
	enddate=$("#datepicker12_").val();

	json["partnerActivationDate"]=startdate;
	json["partnerDeactivationDate"]=enddate;
	json["partnerNo"]=id;
	json["partnerId"]=$("#edit_partnerid_").val();
	json["partnerName"]=$("#edit_partnername_").val();
	json["ratePermile"]=$("#edit_ratepermile_").val();
	var hp = $('#edit_hostpartner_:checked').val();
	var dp = $('#edit_defaultpartner_:checked').val();
	var lp = $('#edit_logopartner:checked').val();
	var op = $('#edit_offlinepartner:checked').val();
	var rp = $('#edit_readpartner:checked').val();
	var tp = $('#edit_talkpartner:checked').val();
	
	json["hostPartner"]=(hp=="on")?"1":"0";	
	json["defaultPartner"]=(dp=="on")?"1":"0";
	json["logoPartner"]=(lp=="on")?"1":"0";	
	json["offLinePartner"]=(op=="on")?"1":"0";
	json["talkPartner"]=(tp=="on")?"1":0;
	json["readPartner"]=(rp=="on")?"1":0;
	if(op=="on"){
		/* var jpMileDesc = $("#edit_logoJpmileDesc").val();
	 if(jpMileDesc==""){
		 $("#edit_logoJpmileDesc").val(" ");
	 } */
	/*  var transactionId =$("#edit_referenceTokenName_").val();
	 if(transactionId==""){
		 $("#edit_referenceTokenName_").val(" ");
	 } */
	}
	json["partnerImageUrl"]=$("#edit_partnerimageurl_").val();
	json["partnerPopupImageUrl"]=$("#edit_partnerpopupimageurl_").val();
	json["partnerIconUrl"]=$("#edit_partnericonurl_").val();
	//json["logoPartnerlandingUrl"]=$("#edit_logoPrtLandingUrl").val();
	json["partnerLandingUrl"]=$("#edit_logoPrtLandingUrl").val();
	json["logoPartnersearchUrl"]=$("#edit_logoPrtSearchUrl").val();
	json["logoPartnerDispOrd"]=$("#edit_logoPrtDispOrd").val();
	json["logoPartnerTilePos"]=$("#edit_logoPrtTilePos").val();
	json["affiliateId"]=$("#edit_affiliateId_").val();
	json["sourceOfInfo"]=$("#edit_sourceOfInfo_").val();
	json["referenceToken"]=$("#edit_referenceToken_").val();
	json["affiliateIdName"]=$("#edit_affiliateIdName_").val();
	json["sourceOfInfoName"]=$("#edit_sourceOfInfoName_").val();
	json["referenceTokenName"]=$("#edit_referenceTokenName_").val();
	json["jpmileDesc"]=$("#edit_logoJpmileDesc").val();
	json["partnerDesc"]=$("#edit_logoPartnerDesc").val();
	json["activityId"]=$("#edit_activityid_").val();
	json["paramFour"]=$("#edit_paramFour_").val();
	json["paramFourName"]=$("#edit_paramFourName_").val();
	json["jpmileReconPeriod"]=$("#edit_logo_jpmileReconPeriod").val();
	json["partnerStatus"]=$('input[name=edit_partnerstatus__]:checked').val();
	json["termsAndCondition"]=$("#edit_logoTermsAndCondition").val();
	json["displayPrtPopup"]=$("#edit_logoDisplayPrtPopup").val();
	json["allowJpnumber"]=$("#edit_logoAllowJpnumber").val();
	json["aggregateId"]=$("#edit_logo_aggregateId").val();
	var partnerType = $('#partnerType').val();
	
	var formData = new FormData();
	formData.append('json',JSON.stringify(json));
	if($("#editLogoPartnerBean").validate().element('#edit_partnerid_') 
			&& $("#editLogoPartnerBean").validate().element('#edit_partnername_')			
			&& $("#editLogoPartnerBean").validate().element('#edit_ratepermile_')
			&& $("#editLogoPartnerBean").validate().element('#edit_logoPrtLandingUrl')
			&& $("#editLogoPartnerBean").validate().element('#edit_logoPrtSearchUrl')
			&& $("#editLogoPartnerBean").validate().element('#edit_logoPrtDispOrd')
			&& $("#editLogoPartnerBean").validate().element('#edit_logoPrtTilePos')
			&& $("#editLogoPartnerBean").validate().element('#datepicker11_')
			&& $("#editLogoPartnerBean").validate().element('#datepicker12_')
			&& $("#editLogoPartnerBean").validate().element('#editPartnerImageBtn')
			&& $("#editLogoPartnerBean").validate().element('#editPartnerPopupImageBtn')			
			&& $("#editLogoPartnerBean").validate().element('#edit_partnerimageurl_')
			&& $("#editLogoPartnerBean").validate().element('#edit_partnerpopupimageurl_')			
			&& $("#editLogoPartnerBean").validate().element('#editPartnerIconBtn')
			&& $("#editLogoPartnerBean").validate().element('#edit_partnericonurl_')
			&& $("#editLogoPartnerBean").validate().element('#edit_affiliateId_')
			&& $("#editLogoPartnerBean").validate().element('#edit_referenceToken_')
			&& $("#editLogoPartnerBean").validate().element('#edit_sourceOfInfo_')
			&& $("#editLogoPartnerBean").validate().element('#edit_affiliateIdName_')
			&& $("#editLogoPartnerBean").validate().element('#edit_referenceTokenName_')
			&& $("#editLogoPartnerBean").validate().element('#edit_sourceOfInfoName_')
			&& $("#editLogoPartnerBean").validate().element('#editLogo_partnericonurlhidden')
			&& $("#editLogoPartnerBean").validate().element('#edit_logoJpmileDesc')
			&& $("#editLogoPartnerBean").validate().element('#edit_logoPartnerDesc')
			&& $("#editLogoPartnerBean").validate().element('#edit_logo_jpmileReconPeriod')
			&& $("#editLogoPartnerBean").validate().element('#edit_activityid_')
			&& $("#editLogoPartnerBean").validate().element('#edit_paramFour_')
			&& $("#editLogoPartnerBean").validate().element('#edit_paramFourName_')
			&& $("#editLogoPartnerBean").validate().element('#edit_logoTermsAndCondition')
			&& $("#editLogoPartnerBean").validate().element('#edit_logo_aggregateId')){		

	$.ajax({
			  url: "${contextPath}/admin/updateLogoPartner",
			  data: formData,
			  dataType: 'text',
			  processData: false,
			  contentType: false,
			  type: 'POST',
			  success: function (response) {
				 
					  $('#myModal_Logo_').modal('hide');
				  window.location.href="${contextPath}/admin/partner?updateMessage="+response+"&partnerType="+partnerType;
			  },
			  error: function (response) {
				  $('#myModal_Logo_').modal('hide');
				  window.location.href="${contextPath}/admin/partner?updateMessage=failure";
					
            	 }
			 });
	}
});

/* Jagjeet partnerMappingUpdate() */

$('#updatePartnerCategoryMap').click(function () {
	
	var mapPrtNo = $("#prtNameCatMapping option:selected"). val();
	var prtCatMapText = $('#textCatMapping').val();
	var json={};
	json["mapPrtNo"]=mapPrtNo;
	json["prtCatMapText"]=prtCatMapText;
	var formData = new FormData();
	formData.append('json',JSON.stringify(json));
	
	if($("#partnerCatMapping").validate().element('#prtTypeCatMapping')
			&& $("#partnerCatMapping").validate().element('#prtNameCatMapping') 
			&& $("#partnerCatMapping").validate().element('#textCatMapping')){
	$.ajax({
		  url: "${contextPath}/admin/updatePartnerCategoryMap",
		  data: formData,
		  dataType: 'text',
		  processData: false,
		  contentType: false,
		  type: 'POST',
		  success: function (response) {
		  window.location.href="${contextPath}/admin/partner/?catUpdateMessage="+response+"&mapPrtNo="+mapPrtNo;
		  },
		  error: function (response) {
			  window.location.href="${contextPath}/admin/partner/?catUpdateMessage=failure";
      	 }
		 });
	}
});
//till here





/* $('span#deletePartner').on('click', function () { */
	 $('#table').on('click', 'a.delete', function (e) {
		 var checka = table1.row( $(this).closest('tr')).data();
		 var id = table1.row( $(this).closest('tr')).data().prtNo;
	var partnerType = $('#partnerType').val();
	if(partnerType=='logo' || partnerType=='offline')
		id=$(this).closest('tr').data('id_');
	var partnerid= table1.row( $(this).closest('tr')).data().partnerId;;
	var partnername= table1.row( $(this).closest('tr')).data().partnerName;;
	
	$('#deletePartnerId').text(partnername+" "+ " Partner will be deleted. Do you want to continue?");
	$('#myModala').data('id', id).modal('show');
});

	 $('#table-1').on('click', 'a.delete', function (e) {
		 var checka = table2.row( $(this).closest('tr')).data();
		 var id = table2.row( $(this).closest('tr')).data().prtNo;
	var partnerType = $('#partnerType').val();
	if(partnerType=='logo' || partnerType=='offline')
		//id=$(this).closest('tr').data('id_');
		 id=table2.row( $(this).closest('tr')).data().prtNo;
	var partnerid= table2.row( $(this).closest('tr')).data().partnerId;;
	var partnername= table2.row( $(this).closest('tr')).data().partnerName;;
	
	$('#deletePartnerId').text(partnername+" "+ " Partner will be deleted. Do you want to continue?");
	$('#myModala').data('id', id).modal('show');
});





$('#btnD').click(function () {
	var id = $('#myModala').data('id');
	 
	var partnerType = $('#partnerType').val();
	$.get("${contextPath}/admin/deletePartner?prtNo="+id , function(status) {		
		if(status=="success" ){
			$('[data-id=' + id + ']').remove();
			$('#myModala').modal('hide');
		  window.location.href="${contextPath}/admin/partner?deletePrtMsg="+status+"&partnerType="+partnerType;

		}
		if(status=="failure" ){
			$('#myModala').data('id', id).modal('show');
			 window.location.href="${contextPath}/admin/partner?deletePrtMsg="+status+"&partnerType="+partnerType;

		}
	});
});


$(document).ready(function () {

$("#editPartnerBean").validate({

    ignore:[],	
	rules: {
		edit_partnerid:{
			required: true,
			duplicatePartnerId:true,
			AlphaNumericOnly: true
		},  
		edit_activityid:{
			required: true,
			duplicateActivityId:true
		},
		edit_partnername:{
			required: true,
			//AlphaNumericOnly: true,
			alpha: true,
			rangelength:[3,100],
			duplicatePartnerName:true
        },	               
		edit_ratepermile:{
			required: true,
			NumericOnly: true
		},
		edit_prtDispOrd:{
			required: true,
			digits:true,
			//displayOrderNotZero_:true,
			dupLogoPartnerDispOrd:true,
			edit_prtDispOrdLessThan100 : true
		},
		datepicker11:{
			required: true,
			dateHigherThanToday:true
		},
		datepicker12:{
			greaterThanEndDate:true
		},
		editPartnerImage:{
			extension: "png|jpg|jpeg|gif",
			filesize: 1100000
		},
		editPartnerPopupImage:{
			extension: "png|jpg|jpeg|gif",
			filesize: 1100000
		},
		edit_partnerimageurl:{
			required: true,
			rangelength:[1,100]
		},
		edit_partnerpopupimageurl:{
			required: true,
			rangelength:[1,100]
		},
		editPartnerIcon:{
			//required: true,
			extension: "png|jpg|jpeg|gif"
		},
		edit_partnericonurl:{
			required: true,
			rangelength:[1,100]
		},
		edit_PrtLandingUrl:{
			required:true,
			rangelength:[1,200],
			url:true
		},
		edit_affiliateIdName:{
			//required: true,
			AlphaNumericandsomeSpecial: true
		},
		edit_sourceOfInfoName:{
			//required: true,
			AlphaNumericandsomeSpecial: true
		},
		edit_referenceTokenName:{
			required: true,
			AlphaNumericandsomeSpecial: true
		},
		edit_affiliateId:{
			//required: true,
			AlphaNumericandsomeSpecial: true
		},
		edit_sourceOfInfo:{
			//required: true,
			AlphaNumericandsomeSpecial: true
		},
		edit_referenceToken:{
			//required: true,
			AlphaNumericandsomeSpecial_: true
		},
		edit_partnerimageurlHidden:{
			imageUpload: true,
		},
		edit_partnerpopupimageurlHidden:{
			imageUpload: true,
		},
		edit_partnericonurlhidden:{
			iconUpload: true,
		},
		edit_jpmileDesc:{
			required: true,
			rangelength: [1,600]
		},
		edit_jpmileReconPeriod:{
			required: true,
			digits:true
		},
		edit_paramFourName:{
			//required: true,
			AlphaNumericandsomeSpecial: true
		},
		edit_paramFour:{
			rangelength: [0,300]
			//required: true,
			//AlphaNumericandsomeSpecial: true
		},
		edit_termsAndCondition:{
			required: true,
			rangelength: [1,600]
		},
		
		edit_talkreadPartUrl :{
			required:true,
			rangelength:[1,200],
			url:true
		},
		
		edit_talkReadTilPos :{
			required: true,
 			digits:true,
 			dupLogoPartnerTilePos:true,
 			edit_logoPrtTilePosLessThan100 : true
		},
		/* edit_aggregateId:{
			required: true,
			duplicateAggregateId:true
		}, */
		
 },  
 
 messages: {
	 edit_partnername:{
		 required:"Please enter partner name",
		 rangelength:"Partner name should be between 3-100 Characters",
		 alpha:"Please Enter Valid Special Charaters (Only -.#&')",
		 //AlphaNumericOnly:"AlphaNumeric only(No special characters allowed)",
		 duplicatePartnerName:"Partner Name already exists"
	 },	 
	 edit_partnerid:{
		 required:"Please enter Partner ID ",
		 duplicatePartnerId:"Partner ID already exists",
		 AlphaNumericOnly:"AlphaNumeric only(No special characters allowed)"
		
	 },
	 edit_activityid:{
		 required:"Please enter Activity ID ",
		 duplicateActivityId:"Activity ID already exists"
		
	 },
	 edit_ratepermile:{
		 required:"Please enter Rate Per Mile ",
		 NumericOnly:"Only numeric values are allowed"
	 },
	 edit_prtDispOrd:{
			required: "Please enter display order ",
	 		digits:"Please enter numeric data",
	 		//displayOrderNotZero:"Logo Partner Display Order cannot be 0",
	 		dupLogoPartnerDispOrd:"Partner Display Order already exists",
	 		edit_prtDispOrdLessThan100: "Partner Display Order must be less than or equal to 100"
		},
	 datepicker11:{
			required: "Please Select Activation Date",
			dateHigherThanToday:"Activation date must be less than or equal to Deactivation date "
			
		},
	datepicker12:{
		greaterThanEndDate:"Deactivation date must be greater than or equal to Activation date "
	},		
	 editPartnerImage:{
		extension:"Invalid file format. Only jpg|jpeg|png|gif files are allowed."
	},
	editPartnerPopupImage:{
		extension:"Invalid file format. Only jpg|jpeg|png|gif files are allowed."
	},
	edit_partnerimageurl:{
		required:"Partner Image Url field should not be empty",
		rangelength:"Maximum 100 characters permitted"
	},
	edit_partnerpopupimageurl:{
		required:"Partner Popup Image Url field should not be empty",
		rangelength:"Maximum 100 characters permitted"
	},
	 editPartnerIcon:{
		 //required: "Please upload partner icon",
		extension:"Invalid file format. Only jpg|jpeg|png|gif files are allowed."
	},		
	edit_partnericonurl:{
		required:"Partner Icon Url field should not be empty",
		rangelength:"Maximum 100 characters permitted"
	},
	edit_PrtLandingUrl:{
		required:"Partner Landing Url field should not be empty"
	},
	edit_affiliateIdName:{
		//required: "Please enter affiliate ID name",
		AlphaNumericandsomeSpecial: "AlphaNumeric,Space,Hyphen and Underscore only allowed"
	},
	edit_sourceOfInfoName:{
		//required: "Please enter source of info name",
		AlphaNumericandsomeSpecial: "AlphaNumeric,Space,Hyphen and Underscore only allowed"
	},
	edit_referenceTokenName:{
		required: "Please enter prefix of transaction name",
		AlphaNumericandsomeSpecial: "AlphaNumeric,Space,Hyphen and Underscore only allowed"
	},
	edit_affiliateId:{
		//required: "Please enter param 1 value",
		AlphaNumericandsomeSpecial: "AlphaNumeric,Space,Hyphen and Underscore only allowed"
	},
	edit_sourceOfInfo:{
		//required: "Please enter param 3 value",
		AlphaNumericandsomeSpecial: "AlphaNumeric,Space,Hyphen and Underscore only allowed"
	},
	edit_referenceToken:{
		//required: "Please enter param 2 value",
		AlphaNumericandsomeSpecial_: "AlphaNumeric,Plus,Space,Hyphen,Curly Braces and Underscore only allowed"
	},
	edit_partnerimageurlHidden:{
		imageUpload: "Please click On partner image upload button",
	},
	edit_partnerpopupimageurlHidden:{
		imageUpload: "Please click On partner image upload button",
	},
	edit_partnericonurlhidden:{
		iconUpload: "Please click On partner icon upload button"
	},
	edit_jpmileDesc:{
		required: "Please enter JPMile Description",
		rangelength: "Maximum 600 characters permitted."
	},
	edit_jpmileReconPeriod:{
		required: "Please enter no of days",
		digits:"Please enter numeric data"
	},
	edit_paramFourName:{
		AlphaNumericandsomeSpecial: "AlphaNumeric,Space,Hyphen and Underscore only allowed"
	},
	edit_paramFour:{
		rangelength: "Maximum 300 characters permitted."
		//AlphaNumericandsomeSpecial: "AlphaNumeric,Space,Hyphen and Underscore only allowed"
	},
	edit_termsAndCondition:{
		required: "Please enter Terms and Conditions.",
		rangelength: "Maximum 600 characters permitted."
	},
	edit_talkReadTilPos:{
	    required: "Please enter Partner Tile Position.",
		digits:"Please enter numeric data.",
		dupLogoPartnerTilePos:"Partner Tile Position already exists.",
		edit_logoPrtTilePosLessThan100: "Partner Tile Position must be less than or equal to 100."
	},
	edit_talkreadPartUrl:{
		required:"Partner Search Url field should not be empty."
	},
	/* edit_aggregateId:{
		 required:"Please enter Aggregate ID ",
		 duplicateAggregateId:"Aggregate ID already exists"
		
	 }, */
	},
 errorElement: "div",
 errorPlacement: function(error, element) {
     if ($(element).hasClass("form-control startDate")) {
         error.insertAfter($(element).closest("div"));
     }
     else if ($(element).hasClass("form-control endDate")){
      error.insertAfter($(element).closest("div"));
     }
     else if ($(element).hasClass("editPartnerImageUpload")){
         error.insertAfter($(element).closest("div"));
     }
     else if ($(element).hasClass("editPartnerPopupImageUpload")){
         error.insertAfter($(element).closest("div"));
     }
     else if ($(element).hasClass("editPartnerIconUpload")){
         error.insertAfter($(element).closest("div"));
     }
     else if($(element).hasClass("urlBtn")){
    	 $('.iconBtn').append(error);
     }
     else {
         error.insertAfter(element);
     }
 },
 submitHandler: function (form) {
	form.submit();
},
});
});


$(document).ready(function(){
	$('#partnerCatMapping').validate({
	
		ignore:[],	
		rules: {
			prtTypeCatMapping:{
				required: true
			}, 
			prtNameCatMapping:{
				required: true
			},
			textCatMapping:{
				required: true,
				rangelength: [1,2000]
			}
			
		},
		messages: {
			prtTypeCatMapping:{
				 required:"Please Select Partner Type."
			 },
			 prtNameCatMapping:{
				 required:"Please Select Partner Name."
			 },
			 textCatMapping:{
				 required:"Category mapping column cannot be left Blank.",
				 rangelength: "Maximum 1000 characters permitted."
			 }
		},
		errorElement: "div",
		errorPlacement: function(error, element) {
		if ($(element).hasClass("uploadBtn2")){
	    	error.insertAfter($(element).closest("div"));
		}
		else {
	    	error.insertAfter(element);
		}

		},
	});
});



/* bg_g_button */

function editUploadImage(id,type,url){
	
	var fileUpload = $("#"+id)[0];			 
	checkFileUpload(fileUpload);
	
	if(!uploadFlag){
		return false;
	}
	
   	var formData = new FormData();
   	formData.append('file',$("#"+id)[0].files[0]);	
   	
   	if($("#editPartnerBean").validate().element("#"+id)){		
  			$.ajax({
			url: "${contextPath}/admin/updatepartnerLogo?type="+type,
		  	data: formData,
			dataType: 'text',
			processData: false,
			contentType: false,
			type: 'POST',
			  	success: function (response) {
   			 	$('#'+url).val(response);			 
   			 	var newImageUrl='${contextPath}'+'/admin/images/'+type+"/"+response;
   			 	
   			 imageUploadFlag=true;
   			/* if($('#partnerType').val() == "logo"){
   				$('#editPartnerImage__').prop('src',newImageUrl);
   			 }
   			 else{*/
   				$('#editPartnerImage_').prop('src',newImageUrl);
   			// }
   			$("#editPartnerBean").validate().element("#edit_partnerimageurlHidden");
   			 $("#editPartnerBean").validate().element("#"+url);
  			  	}
		});		
   	}
}

function editUploadPopupImage(id,type,url){
	
	var fileUpload = $("#"+id)[0];			 
	checkFileUpload(fileUpload);
	
	if(!uploadFlag){
		return false;
	}
	
   	var formData = new FormData();
   	formData.append('file',$("#"+id)[0].files[0]);	
   	
   	if($("#editPartnerBean").validate().element("#"+id)){		
  			$.ajax({
			url: "${contextPath}/admin/updatepartnerLogo?type="+type,
		  	data: formData,
			dataType: 'text',
			processData: false,
			contentType: false,
			type: 'POST',
			  	success: function (response) {
   			 	$('#'+url).val(response);			 
   			 	var newImagePopupUrl='${contextPath}'+'/admin/images/'+type+"/"+response;
   			 	
   			 imageUploadFlag=true;
   			/* if($('#partnerType').val() == "logo"){
   				$('#editPartnerImage__').prop('src',newImageUrl);
   			 }
   			 else{*/
   				$('#editPartnerPopupImage_').prop('src',newImagePopupUrl);
   			// }
   			$("#editPartnerBean").validate().element("#edit_partnerpopupimageurlHidden");
   			 $("#editPartnerBean").validate().element("#"+url);
  			  	}
		});		
   	}
}


function editUploadIcon(id,type,url){
	
	var fileUpload = $("#"+id)[0];			 
	checkIconUpload(fileUpload, "edit");
	
	if(!uploadFlag){
		return false;
	}
	
   	var formData = new FormData();
   	formData.append('file',$("#"+id)[0].files[0]);	
   	
   	if($("#editPartnerBean").validate().element("#"+id)){		
  			$.ajax({
			url: "${contextPath}/admin/updatepartnerLogo?type="+type,
		  	data: formData,
			dataType: 'text',
			processData: false,
			contentType: false,
			type: 'POST',
			  	success: function (response) {
			  		
   			 	$('#'+url).val(response);			 
   			 	var newImageUrl='${contextPath}'+'/admin/images/'+type+"/"+response;
   			 	iconUploadFlag=true;
   				$('#editPartnerIcon_').prop('src',newImageUrl);
   				
   				$("#editPartnerBean").validate().element("#edit_partnericonurlhidden");
   			 $("#editPartnerBean").validate().element("#"+url);
  			  	}
		});		
   	}
}

function editUploadImageLogo(id,type,url){
	
	var fileUpload = $("#"+id)[0];			 
	checkFileUpload(fileUpload);
	
	if(!uploadFlag){
		return false;
	}
	
   	var formData = new FormData();
   	formData.append('file',$("#"+id)[0].files[0]);	
   	
   	if($("#editLogoPartnerBean").validate().element("#"+id)){		
  			$.ajax({
			url: "${contextPath}/admin/updatepartnerLogo?type="+type,
		  	data: formData,
			dataType: 'text',
			processData: false,
			contentType: false,
			type: 'POST',
			  	success: function (response) {
   			 	$('#'+url).val(response);			 
   			 	var newImageUrl='${contextPath}'+'/admin/images/'+type+"/"+response;
   			 
   			 	imageUploadFlag=true;
   				$('#editPartnerImage__').prop('src',newImageUrl);
   				
   				$("#editLogoPartnerBean").validate().element("#editLogo_partnerimageurlHidden");
   			 $("#editLogoPartnerBean").validate().element("#"+url);
  			  	}
		});		
   	}
}

function editUploadPopupImageLogo(id,type,url){
	
	var fileUpload = $("#"+id)[0];			 
	checkFileUpload(fileUpload);
	
	if(!uploadFlag){
		return false;
	}
	
   	var formData = new FormData();
   	formData.append('file',$("#"+id)[0].files[0]);	
   	
   	if($("#editLogoPartnerBean").validate().element("#"+id)){		
  			$.ajax({
			url: "${contextPath}/admin/updatepartnerLogo?type="+type,
		  	data: formData,
			dataType: 'text',
			processData: false,
			contentType: false,
			type: 'POST',
			  	success: function (response) {
   			 	$('#'+url).val(response);			 
   			 	var newImagePopupUrl='${contextPath}'+'/admin/images/'+type+"/"+response;
   			 
   			 	imageUploadFlag=true;
   				$('#editPartnerPopupImage__').prop('src',newImagePopupUrl);
   				
   				$("#editLogoPartnerBean").validate().element("#editLogo_partnerpopupimageurlHidden");
   			 $("#editLogoPartnerBean").validate().element("#"+url);
  			  	}
		});		
   	}
}

function editUploadIconLogo(id,type,url){
	
	var fileUpload = $("#"+id)[0];			 
	checkIconUpload(fileUpload, "logoedit");
	
	if(!uploadFlag){
		return false;
	}
	
   	var formData = new FormData();
   	formData.append('file',$("#"+id)[0].files[0]);	
   	
   	if($("#editLogoPartnerBean").validate().element("#"+id)){		
  			$.ajax({
			url: "${contextPath}/admin/updatepartnerLogo?type="+type,
		  	data: formData,
			dataType: 'text',
			processData: false,
			contentType: false,
			type: 'POST',
			  	success: function (response) {
			  		
   			 	$('#'+url).val(response);			 
   			 	var newImageUrl='${contextPath}'+'/admin/images/'+type+"/"+response;
   			 	iconUploadFlag=true;
   				$('#editPartnerIcon__').prop('src',newImageUrl);
   			$("#editLogoPartnerBean").validate().element("#editLogo_partnericonurlhidden");
   			 $("#editLogoPartnerBean").validate().element("#"+url);
  			  	}
		});		
   	}
}

$(document).on("keydown", function (e) {
    if (e.which === 8 && !$(e.target).is("input, textarea")) {
        e.preventDefault();
    }
});

$('span.psaveorder').click(function () {	
	var reorder = $("#preorder_").val();
	var partnerType = $("#partnerType").val();
	var pageInfo = table2.page.info();
	var currentPage = pageInfo.page+1;
	var pageLen = pageInfo.length;
	console.log("------ current page ---- "+(pageInfo.page+1)+" length "+pageLen);
		$.ajax({
			  url: "${contextPath}/admin/partnerSaveOrder?reorder="+reorder+"&prttype="+partnerType+"&curPage="+currentPage+"&pageLen="+pageLen,
			  
			  dataType: 'text',
			  processData: false,
			  contentType: false,
			  type: 'POST',
			  success: function (response) {				 				 
					window.location.reload();
			  }
			 });
});


$("#edit_prtDispOrd").on('blur', function(){
	$("#editPartnerBean").validate().element('#edit_prtDispOrd');
});

$("#edit_ratepermile").on('blur', function(){
	$("#editPartnerBean").validate().element('#edit_ratepermile');
});

$("#edit_displayPrtPopup").on('change', function() {
    if($(this).is(":checked")) {
    	$('#edit_displayPrtPopup').val(1);
    }
    else{
    	$('#edit_displayPrtPopup').val(0);
    }
});

$("#edit_allowJpnumber").on('change', function() {
    if($(this).is(":checked")) {
    	$('#edit_allowJpnumber').val(1);
    }
    else{
    	$('#edit_allowJpnumber').val(0);
    }
});
</script>

<script type="text/javascript">
$('span.peditorder').click(function () {
        
        // Make a nice striped effect on the table
        table_1 = $("#table-1");
        table_1.find("tr:even").addClass("alt");
        // Initialise the second table specifying a dragClass and an onDrop function that will display an alert
        table_1.tableDnD({
            onDragClass: "myDragClass",
            onDrop: function(table, row) {
                var rows = table.tBodies[0].rows;
                var debugStr = "";
                for (var i=0; i<rows.length; i++) {
                    //debugStr += rows[i].id+"-"+(i+1)+",";
                	debugStr += rows[i].id+",";
                }   
                $("#preorder_").val(debugStr);
                console.log("----#preorder-----> "+debugStr);
                //$(table).parent().find('.result').text(debugStr);                
            },
            onDragStart: function(table, row) {
                //$(table).parent().find('.result').text("Started dragging row "+row.id);
                $("#psbutton").attr("disabled",false);
            }
        });
        $("span.psaveorder").show();
        $("span.peditorder").hide();
             
        
    });
</script>

<script type="text/javascript">
var logoPrtDispOrder=false;
var logoPrtTilePos=false;

$.validator.addMethod('imageUpload', function(value, element) {
	return imageUploadFlag; 
});

$.validator.addMethod('iconUpload', function(value, element) {
	return iconUploadFlag; 
});

$.validator.addMethod('displayOrderNotZero', function(value, element) {
	var isValid = false;
	var logoPrtDisplayOrder = parseInt($("#logoPrtDisplayOrder").val());
	
	if(logoPrtDisplayOrder == 0){
		isValid = true;
	}
	return !isValid; 
});
$.validator.addMethod('logoPrtDisplayOrderLessThan100', function(value, element) {
	var isValid = false;
	var logoPrtDisplayOrder = $("#logoPrtDisplayOrder").val();
	
	if(logoPrtDisplayOrder > 100){
		isValid = true;
	}
	return !isValid;
});
$.validator.addMethod('tilePosNotZero', function(value, element) {
	var isValid = false;

	var logoPrtTilePos = parseInt($("#logoPrtTilePos").val());
	
	if(logoPrtTilePos == 0){
		isValid = true;
	}
	
	return !isValid; 
});
$.validator.addMethod('edit_prtDispOrdLessThan100', function(value, element) {
	var isValid = false;
	var edit_prtDispOrd = $("#edit_prtDispOrd").val();
	
	if(edit_prtDispOrd > 100){
		isValid = true;
	}
	return !isValid;
});
$.validator.addMethod('dupLogoPartnerDispOrd', function(value, element) {
	return !logoPrtDispOrder;
});

$.validator.addMethod('dupLogoPartnerTilePos', function(value, element) {
	return !logoPrtTilePos;
});

		 function getFormattedDate(date){
				var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
				return new Date(date.replace(pattern,'$3-$2-$1'));
			}

		 function uploadImage(id,type,url){
			 var fileUpload = $("#"+id)[0];			 
		        checkFileUpload(fileUpload);
		        console.log("inside uploadimage image");
		        $("#partnerBeanNew").validate().element("#"+id)
		        
		        if(!uploadFlag) return false;			        		       
		    	var formData = new FormData();
		    	formData.append('file',$("#"+id)[0].files[0]);	
		    	if($("#partnerBeanNew").validate().element("#"+id)){		
		    		$.ajax({
		    			 url: "${contextPath}/admin/updatepartnerLogo?type="+type,
		    			  data: formData,
		    			  dataType: 'text',
		    			  processData: false,
		    			  contentType: false,
		    			  type: 'POST',
		    			  success: function (response) {		    				  
		    			 $('#uploadFile').val(response);			 
		    			 var newImageUrl='${contextPath}'+'/admin/images/'+type+"/"+response;			
		    			 $('#partnerImage_').prop('src',newImageUrl);
		    			 $("#partnerBeanNew").validate().element("#"+url);
		    			 $("#partnerImageUrl").val(response);
		   
		    			 $("#partnerBeanNew").validate().element('#partnerImage');
		    			 $("#partnerBeanNew").validate().element("#partnerImageUrl");
		   			  }
		    			 });		
		    	}
		    	
		    	}
		 
		 function uploadIcon(id,type,url){
			 var fileUpload = $("#"+id)[0];
			 console.log("inside uploadIcon image");
			 checkIconUpload(fileUpload, "add");
		        $("#partnerBeanNew").validate().element("#"+id)
		       if(!uploadFlag) return false;			        		       
		    	var formData = new FormData();
		    	formData.append('file',$("#"+id)[0].files[0]);	
		    	if($("#partnerBeanNew").validate().element("#"+id)){		
		    		$.ajax({
		    			 url: "${contextPath}/admin/updatepartnerLogo?type="+type,
		    			  data: formData,
		    			  dataType: 'text',
		    			  processData: false,
		    			  contentType: false,
		    			  type: 'POST',
		    			  success: function (response) {		    				  
		    			 $('#uploadFile_').val(response);			 
		    			 var newImageUrl='${contextPath}'+'/admin/images/'+type+"/"+response;			 
		    			 $('#partnerIcon_').prop('src',newImageUrl);
		    			 $("#partnerBeanNew").validate().element("#"+url);
		    			 $("#partnerIconUrl").val(response);
		   
		    			 $("#partnerBeanNew").validate().element('#partnerIcon');
		    			 $("#partnerBeanNew").validate().element("#partnerIconUrl");
		   			  }
		    			 });		
		    	}		    	
		    	}
		 
		 function uploadPopupImage(id,type,url){
			 var fileUpload = $("#"+id)[0];			 
		        checkFileUpload(fileUpload);
		        console.log("inside uploadPopup image");
		        $("#partnerBeanNew").validate().element("#"+id)
		        
		        if(!uploadFlag) return false;			        		       
		    	var formData = new FormData();
		    	formData.append('file',$("#"+id)[0].files[0]);	
		    	if($("#partnerBeanNew").validate().element("#"+id)){		
		    		$.ajax({
		    			 url: "${contextPath}/admin/updatepartnerLogo?type="+type,
		    			  data: formData,
		    			  dataType: 'text',
		    			  processData: false,
		    			  contentType: false,
		    			  type: 'POST',
		    			  success: function (response) {		    				  
		    			 $('#upload_File_').val(response);			 
		    			 var newImagePopupUrl='${contextPath}'+'/admin/images/'+type+"/"+response;			
		    			 $('#partner_Image_Display').prop('src',newImagePopupUrl);
		    			 $("#partnerBeanNew").validate().element("#"+url);
		    			 $("#partnerPopupImageUrl").val(response);
		   
		    			 $("#partnerBeanNew").validate().element('#partner_Image_');
		    			 $("#partnerBeanNew").validate().element("#partnerPopupImageUrl");
		   			  }
		    			 });		
		    	}
		    	
		    	}
		 
		 $('#partnerImage').on('change', function() {
			 $("#partnerBeanNew").validate().element('#partnerImage');
				  
			});
		 
		 $('#partner_Image_').on('change', function() {
			 $("#partnerBeanNew").validate().element('#partner_Image_');
				  
			});
		 
		 
		 $('#cancelbtnD').click(function () {
			 onClickCloseBtn();
			});
		 
		 $("#partnerType").on( "selectmenuchange", function(){	
		
			 console.log("entered in select")
		
		   		 var partnerType= $('#partnerType').val();
		   		 $(this).closest('form').trigger('submit');
			    
			}); 
		 
		 function onClickCloseBtn (){
			var prtTyp = $("#partnerType").val();
			$.get("${contextPath}/admin/partner",function(data,status) {		
				window.location.href="${contextPath}/admin/partner?f="+prtTyp;
			});
		 }
		 
$("#displayPrtPopup").on('change', function() {
    if($(this).is(":checked")) {
    	$('#displayPrtPopup').val(1);
    }
    else{
    	$('#displayPrtPopup').val(0);
    }
});

$("#allowJpnumber").on('change', function() {
    if($(this).is(":checked")) {
    	$('#allowJpnumber').val(1);
    }
    else{
    	$('#allowJpnumber').val(0);
    }
});
		 
</script>

 <script type="text/javascript">
$(function () {
    $("#partnerImage").bind("change", function () {    	
    	//Get reference of FileUpload.
        var fileUpload = $("#partnerImage")[0];    	
        console.log("-----partnerImage--------",$("#partnerImage")[0])
        checkFileUpload(fileUpload);
        if(!uploadFlag) return false;
    });
});

$(function () {
    $("#partner_Image_").bind("change", function () {    	
    	//Get reference of FileUpload.
    	console.log("-------------",$("#partner_Image_")[0])
        var fileUpload = $("#partner_Image_")[0];    	
        checkFileUpload(fileUpload);
        if(!uploadFlag) return false;
    });
});

$(function () {
    $("#editPartnerImage").bind("change", function () {    	
    	//Get reference of FileUpload.
    	imageUploadFlag=false;
        var fileUpload = $("#editPartnerImage")[0];
        checkFileUpload(fileUpload);
        if(!uploadFlag) return false;
    });
});

$(function () {
    $("#editPartnerPopupImage").bind("change", function () {    	
    	//Get reference of FileUpload.
    	imageUploadFlag=false;
        var fileUpload = $("#editPartnerPopupImage")[0];
        checkFileUpload(fileUpload);
        if(!uploadFlag) return false;
    });
});

$(function () {
    $("#editPartnerImageBtn").bind("change", function () {    	
    	//Get reference of FileUpload.
    	imageUploadFlag=false;
        var fileUpload = $("#editPartnerImageBtn")[0];
        checkFileUpload(fileUpload);
        if(!uploadFlag) return false;
    });
});

$(function () {
    $("#editPartnerPopupImageBtn").bind("change", function () {    	
    	//Get reference of FileUpload.
    	imageUploadFlag=false;
        var fileUpload = $("#editPartnerPopupImageBtn")[0];
        checkFileUpload(fileUpload);
        if(!uploadFlag) return false;
    });
});

 $(function () {
    $("#partnerIcon").bind("change", function () { 
    	$('#uploadFile_').val($(this).val());
    	//iconFlag=false;
   	    var fileUpload = $("#partnerIcon")[0]; 
    	//$("#partnerBeanNew").validate().element('#partnerIcon');
        checkIconUpload(fileUpload, "add");
        if(!uploadFlag) return false;
        	
    });
}); 

$(function () {
    $("#editPartnerIcon").bind("change", function () {    	
    	//Get reference of FileUpload.
    	iconUploadFlag=false;
    	$("#editPartnerBean").validate().element('#edit_partnericonurl');
        var fileUpload = $("#editPartnerIcon")[0];
        checkIconUpload(fileUpload, "edit");
        if(!uploadFlag) return false;
    });
});

$(function () {
    $("#editPartnerIconBtn").bind("change", function () {    	
    	//Get reference of FileUpload.
    	iconUploadFlag=false;
    	$("#editLogoPartnerBean").validate().element('#edit_partnericonurl_');
        var fileUpload = $("#editPartnerIconBtn")[0];
        checkIconUpload(fileUpload, "logoedit");
        if(!uploadFlag) return false;
    });
});



function checkFileUpload(fileUpload){
	//Check whether the file is valid Image.
	console.log("inside checkFileUpload");
    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:\().+(\)\.def\().+(\])+(.jpg|.jpeg|.png|.gif)$");
    console.log("inside checkFileUpload --------------> "+fileUpload.value.toLowerCase());
    if (regex.test(fileUpload.value.toLowerCase())) {
    
        //Check whether HTML5 is supported.
        console.log("inside regex");
        if (typeof (fileUpload.files) != "undefined") {
            //Initiate the FileReader object.
            console.log("inside regex if ----> found files");
            var reader = new FileReader();
            //Read the contents of Image File.
            console.log("fileUpload.files[0] ----> "+fileUpload.files[0]);
            reader.readAsDataURL(fileUpload.files[0]);
            console.log("reader");
           
            reader.onload = function (e) {
                //Initiate the JavaScript Image object.
                console.log("reader",e);
                var image = new Image();
                //Set the Base64 string return from FileReader as source.
                 /* console.log("e.target.result-------> ",e.target.result); */
                image.src = e.target.result;
                image.onload = function () {
                    //Determine the Height and Width.
                    /* var height = this.height;
                    var width = this.width;                     
                    if (height > 1500 || width > 1500) {
                        alert("Height and Width must not exceed 1000px.");
                        uploadFlag = false;
                        return uploadFlag;
                    }         */             
                    uploadFlag = true;                                       
                };
            }
        } else {
           
            uploadFlag = false;
        }
    } else {
      
        uploadFlag = false;
    }
}

function checkIconUpload(fileUpload, type){
var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:\().+(\)\.def\().+(\])+(.jpg|.jpeg|.png|.gif)$");
	
	if (regex.test(fileUpload.value.toLowerCase())) {
		//Check whether HTML5 is supported.
		if (typeof (fileUpload.files) != "undefined") {
			//Initiate the FileReader object.
			var reader = new FileReader();
			//Read the contents of Image File.
			reader.readAsDataURL(fileUpload.files[0]);

			reader.onload = function (e) {
				//Initiate the JavaScript Image object.
				var image = new Image();
				//Set the Base64 string return from FileReader as source.
				image.src = e.target.result;
				
				image.onload = function () {
					
					//Determine the Height and Width.
					var height = this.height;
					var width = this.width;
					        
					var errorText = "";
					uploadFlag = false;
					
						if(width == height && width <= 32){
							uploadFlag = true;
						}
						else{
							uploadFlag = false;
							errorText = "Partner Icon should not be more than 32*32";
						}
				 		 
					if(type=="add"){	
						$("#icon_error").css({"display":""});
						if(errorText != ""){
							$("#icon_error").text(errorText);
						}else{
							$("#icon_error").css({"display":"none"});
						}
					}else if(type=="edit"){
						$(".iconBtn").css({"display":""});
						if(errorText != ""){
							$(".iconBtn").text(errorText);
						}else{
							$(".iconBtn").css({"display":"none"});
						}
					}else if(type=="logoedit"){
						$(".logoIconBtn").css({"display":""});
						if(errorText != ""){
							$(".logoIconBtn").text(errorText);
						}else{
							$(".logoIconBtn").css({"display":"none"});
						}
					}
					
					
					return uploadFlag;
				};
			};
		}
	}
	else{
		uploadFlag = false;
	}
}

//On Change Methods For Edit Partner
$('#editPartnerImage').on('change', function() {
	$('#edit_partnerimageurl').val($(this).val());
});
$('#editPartnerPopupImage').on('change', function() {
	$('#edit_partnerpopupimageurl').val($(this).val());
});
$('#editPartnerIcon').on('change', function() {
	$('#edit_partnericonurl').val($(this).val());
});

$('#datepicker11').on('change', function () {
	$("#editPartnerBean").validate().element('#datepicker11');
	//TODO:AW-284
	var endDate = getFormattedDate($('#datepicker12').val());
	
	if(endDate != 'Invalid Date'){
		$("#editPartnerBean").validate().element('#datepicker12');
	}
});

$('#datepicker12').on('change', function () {
	$("#editPartnerBean").validate().element('#datepicker11'); 
	$("#editPartnerBean").validate().element('#datepicker12'); 
});

$("#edit_partnername").change(function(){
	var partnerName=$('#edit_partnerhiddenname').val();
	 
	var cpartnerName=$('#edit_partnername').val();
	cpartnerName=encodeURIComponent(cpartnerName);
	if(partnerName!=cpartnerName){
		$.get("${contextPath}/admin/checkPartnerName?partnerName="+cpartnerName, function(data,status) {
			if(status=="success" && data!=''){
				partnerNameExist=true;	
			}
			else{
				partnerNameExist=false;
			}
			$("#editPartnerBean").validate().element('#edit_partnername');	
		});
	}
	else
		partnerNameExist=false;
});


$("#edit_partnerid").change(function(){
	var partnerId=$('#edit_partnerhiddenid').val();
	var cpartnerId=$('#edit_partnerid').val();
	partnerIDExist = false;
	
	if(partnerId!=cpartnerId){
	$.get("${contextPath}/admin/checkPartnerID?partnerId="+cpartnerId, function(data,status) {
		if(status=="success" && data!=''){
			partnerIDExist=true;	
		}
		else{
			partnerIDExist=false;
			}
		$("#editPartnerBean").validate().element('#edit_partnerid');
		
	});
	}else
		partnerIDExist=false;

});

$("#edit_activityid").change(function(){
	var activityId=$('#edit_activityhiddenid').val();
	var cactivityId=$('#edit_activityid').val();
	activityIDExist = false;
	
	if(activityId!=cactivityId){
	$.get("${contextPath}/admin/checkActivityID?activityId="+cactivityId, function(data,status) {
		if(status=="success" && data!=''){
			activityIDExist=true;	
		}
		else{
			activityIDExist=false;
			}
		$("#editPartnerBean").validate().element('#edit_activityid');
		
	});
	}else
		activityIDExist=false;

});

/* $("#edit_aggregateId").change(function(){
	var aggregateHiddenId=$('#edit_aggregatehiddenid').val();
	var aggregateId=$('#edit_aggregateId').val();
	aggregateIDExist = false;
	
	if(aggregateHiddenId!=aggregateId){
	$.get("${contextPath}/admin/checkAggregateID?aggregateId="+aggregateId, function(data,status) {
		if(status=="success" && data!=''){
			aggregateIDExist=true;	
		}
		else{
			aggregateIDExist=false;
			}
		$("#editPartnerBean").validate().element('#edit_aggregateId');
		
	});
	}else
		aggregateIDExist=false;

}); */


//Validation Methods
$.validator.addMethod("AlphabetsOnly", function(value, element) {
	return this.optional(element) || /^[a-z][a-z@\s]*$/i.test(value);
});

//TODO:Validation methods for AlphaNumericOnly
$.validator.addMethod("AlphaNumericOnly", function(value, element) {
	return this.optional(element) || /^[a-zA-Z0-9\s]*$/i.test(value);
});

$.validator.addMethod("NumericOnly", function(value, element) {
	return this.optional(element) || /^\d*\.?\d*$/i.test(value);
});

$.validator.addMethod('filesize', function (value, element, param) {
	return this.optional(element) || (element.files[0].size <= param)
}, 'File size should not be more than 1MB');

$.validator.addMethod("AlphaNumericandsomeSpecial", function(value, element) {
	return this.optional(element) || /^[a-zA-Z0-9-_\s]*$/i.test(value);
});

$.validator.addMethod("AlphaNumericandsomeSpecial_", function(value, element) {
	return this.optional(element) || /^[a-zA-Z0-9-_\s{}+=]*$/i.test(value);
});

$.validator.addMethod('duplicatePartnerId', function(value, element) {
    if(partnerIDExist){
		return false;
    }
    else{
		return true;
    }
});

$.validator.addMethod('duplicatePartnerName', function(value, element) {
    if(partnerNameExist){
       return false;
    }
    else{
     return true;
    }
});

$.validator.addMethod('duplicateActivityId', function(value, element) {
    if(activityIDExist){
       return false;
    }
    else{
     return true;
    }
});

/* $.validator.addMethod('duplicateAggregateId', function(value, element) {
    if(aggregateIDExist){
       return false;
    }
    else{
     return true;
    }
}); */

$.validator.addMethod("greaterThanEndDate", function(value, element) {
	var startDate = getFormattedDate($('#datepicker11').val());
		var endDate = getFormattedDate($('#datepicker12').val());
		if(endDate=='Invalid Date')
			return true;
		if (endDate >= startDate)
			return true;
		if (endDate <= startDate)
			return false;
	});
	
$.validator.addMethod("dateHigherThanToday", function(value, element) {
	var startDate = getFormattedDate($('#datepicker11').val());
	var endDate = getFormattedDate($('#datepicker12').val());
	
	if(endDate=='Invalid Date')
		return true;
	if (startDate <= endDate)
		return true;
	if (startDate >= endDate)
		return false;
});

$.validator.addMethod("endDate", function(value, element) {
	var startDate = getFormattedDate($('#datepicker1').val());
	var endDate = getFormattedDate($('#datepicker2').val());
	if(endDate=='Invalid Date')
		return true;
	if (endDate >= startDate)
		return true;
	if (endDate <= startDate)
		return false;
});

$.validator.addMethod("startDate", function(value, element) {
	var startDate = getFormattedDate($('#datepicker1').val());
	var endDate = getFormattedDate($('#datepicker2').val());
	if(endDate=='Invalid Date')
		return true;
	if (startDate <= endDate)if($('#logoPrtDisplayOrder').val() == ""){
		//$('#logoPrtDisplayOrder').val(0);
	}
	
	if($('#logoPrtTilePos').val() == ""){
		$('#logoPrtTilePos').val(0);
	}
		return true;
	if (startDate >= endDate)
		return false;
});

$.validator.addMethod("dropdowncheck", function(value, element) {
	if(value==-1)
	return false;
	else
	return true;
});

$.validator.addMethod("conFigTile", function(value, element) {
	var configTile = $('#configTileNo').val();
	if(configTile >=0 && configTile<=12){
		return true;
	}else{
		return false;
	}
});



</script>

<!-- Create New Partner -->
<script type="text/javascript">

function checkValidation(){
	 
	console.log("inside check validation");
	if(!$("#logoPartner").is(':checked') && !$("#offLinePartner").is(':checked') && !$("#readPartner").is(':checked') && !$("#talkPartner").is(':checked') && !$("#defaultPartner").is(':checked') ){
		//$('#logoPrtDisplayOrder').val(0);
		$('#logoPrtTilePos').val(0);
		//$('#logoPartnerLandingUrl').val('');
		$('#logoPartnerSearchUrl').val('');
		
		changeValidationRule(false);
		changeValidationRuleforOffLinePrt(true);
	}
	 
	  if($("#offLinePartner").is(':checked')){
		  $('#jpmileReconPeriod').val('0');
		//changeValidationRule(true);
		//changeValidationRuleforOffLinePrt(false);
	}  
	 // readPartner talkPartner
	  if($("#defaultPartner").is(':checked')){
		 
		changeValidationRule(true);
		changeValidationRuleforOffLinePrt(false);
	}  
	  
}

function changeValidationRule(flag){
	
	console.log("inside change validation rule");
	$('#logoPartnerSearchUrl').rules('add', {
		required: flag,
		rangelength: [1,200],
		url: flag
	});
	
	/* $('#logoPrtDisplayOrder').rules('add', {
		required: flag,
		digits: flag,
		//displayOrderNotZero: flag,
		dupLogoPartnerDispOrd: flag
	}); */
	
	$('#logoPrtTilePos').rules('add', {
		required: flag,
		digits: flag,
		//tilePosNotZero: flag,
		dupLogoPartnerTilePos: flag,
		logoPrtTilePosLessThan100: flag
	});
	
}
function changeValidationRuleforOffLinePrt(flag){
	/* $('#jpmileDesc').rules('add', {
		required: flag,
	}); */
	$('#jpmileReconPeriod').rules('add', {
		required: flag,
	});
	/* $('#partnerDesc').rules('add', {
		required: !flag,
	}); */
}

$.validator.addMethod('logoPrtTilePosLessThan100', function(value, element) {
	var isValid = false;
	var logoPrtTilePos = $("#logoPrtTilePos").val();
	
	if(logoPrtTilePos > 100){
		isValid = true;
	}
	return !isValid;
});
$.validator.addMethod("alpha", function(value, element) {
	 var re = /^[-./#&+\w\s+\w\']*$/;
	    if (re.test(value)) {
	            return true;
	        } else {
	            return false;
	        }
	   
	});


function urlChange(){
	console.log("inside urlChange")
	history.pushState({}, '',"${contextPath}/admin/partner");
}


$(document).ready(function () {
	console.log("inside function");
	//TODO:checkbox
	//Setting defaultPartner checked byDefault
	$('#defaultPartner').prop('checked', true);
	$('#partnerDescDiv').hide();
    $('#logoPrtDisplayOrder').val('');
	$('#logoPrtTilePos').val('');
	
	 if($('#defaultPartner').is(':checked'))
	 {
		 $('#logo_partner').show(); 
		 $("#logoPrtPosLbl").hide();
		 $("#offlinePrtPosLbl").hide();
		 $("#logoPrtUrlLbl").hide();
		 $("#offlinePrtUrlLbl").hide();
		 $("#partnerPosOnSearchPg").show();
	    
	 }
	
	
	
	
	
	$("#partnerBeanNew").validate({
	    ignore:[],
		rules: {
			partnerName:{
				required: true,
				alpha: true,
				duplicatePartnerName:true
	        },
			partnerId:{
				required: true,
				duplicatePartnerId:true,
				AlphaNumericOnly: true
			},
			activityId:{
				required: true,
				duplicateActivityId:true
			},
			ratePerMile:{
				required: true,
				number : true
			},
			activationDate:{
				required: true,
				startDate: true
			},
			deactivationDate:{
				endDate:true
			},
			partnerImage:{
				required:true,
				extension: "png|jpg|jpeg|gif",
				filesize: 1100000
			},
			partnerImageUrl:{
				required:true,
			  	rangelength:[1,100]
			},
			partnerIcon:{
				required:true,
				extension: "png|jpg|jpeg|gif"
			},
			partnerIconUrl:{
				required:true,
			  	rangelength:[1,100]
			},			
			partnerPopupImage:{
				required:true,
				extension: "png|jpg|jpeg|gif",
				filesize: 1100000
			},
			partnerPopupImageUrl:{
				required:true,
			  	rangelength:[1,100]
			},			
			partnerLandingUrl:{
				required:true,
				rangelength:[1,200],
				url:true
			},
			termsAndCondition:{
				required: true,
				rangelength:[1,600]
			},
			logoPartnerSearchUrl:{
				required:true,
				rangelength:[1,200],
				url:true
			},
			logoPrtDisplayOrder:{
				required: true,
				digits:true,
				//displayOrderNotZero:true,
				dupLogoPartnerDispOrd: true,
				logoPrtDisplayOrderLessThan100 : true
			},	
			logoPrtTilePos:{
	 			required: true,
	 			digits:true,
	 			//tilePosNotZero:true,
	 			dupLogoPartnerTilePos:true,
	 			logoPrtTilePosLessThan100 : true
	 		},
	 		hostPartner:{
				isPartnerTypeSelected: true
			},
			affiliateIdName:{
				//required: true,
				AlphaNumericandsomeSpecial: true
			},
			sourceOfInfoName:{
				//required: true,
				AlphaNumericandsomeSpecial: true
			},
			referenceTokenName:{
				required: true,
				AlphaNumericandsomeSpecial: true
			},
			affiliateId:{
				//required: true,
				AlphaNumericandsomeSpecial: true
			},
			sourceOfInfo:{
				//required: true,
				AlphaNumericandsomeSpecial: true
			},
			referenceToken:{
				//required: true,
				AlphaNumericandsomeSpecial_: true
			},
			partnerDesc:{
				required: false
			},
			jpmileDesc:{
				required: true
			},
			jpmileReconPeriod:{
	 			required: true,
	 			digits:true
	 		},
			paramFourName:{
				AlphaNumericandsomeSpecial: true
			},
			/* aggregateId:{
				required: true,
				duplicateAggregateId:true
			}, */
			paramFour:{
				rangelength:[0,300]			
				/* combinedParam: true */
				//AlphaNumericandsomeSpecial: true
			}/* ,
			offLinePartnerSearchUrl:{
				required:true,
				rangelength:[1,200],
				url:true
			},
			offLinePrtTilePos:{
	 			required: true,
	 			digits:true
	 			//dupLogoPartnerTilePos:true,
	 			//logoPrtTilePosLessThan100 : true
	 		} */
	 	},
		 messages: {
			partnerName:{
				required:"Please enter Partner Name",
				minlength:"Please enter atleast 3 characters",
				alpha:"Please Enter Valid Special Charaters (Only -.#&')",
				duplicatePartnerName:"Partner Name already exists"
			},
			partnerId:{
				required: "Please enter Partner ID",
				duplicatePartnerId:"Partner ID already exists",
				AlphaNumericOnly:"AlphaNumeric only(No special characters allowed)",
			},
			activityId:{
				required: "Please enter Activity ID",
				duplicateActivityId:"Activity ID already exists",
			},
			ratePerMile:{
				required: "Please enter rate Per Mile",
				number: "Only numeric values are allowed "
			},
			activationDate:{
				required: "Please Select Activation Date",
				startDate: "* Activation Date must be less than or equal to Deactivation date"
			},
			deactivationDate:{
				endDate: "* Deactivation Date must be greater than or equal to Activation date"
			},	
			partnerImage:{
				required:"Please upload partner image",
				extension:"Invalid file format. Only jpg|jpeg|png|gif files are allowed."			
		    },
			partnerImageUrl:{
				required:"Please click On partner image upload button",
				rangelength:"Maximum 100 characters permitted"
		    },		    
		    partnerPopupImage:{
				required:"Please upload partner popup image",
				extension:"Invalid file format. Only jpg|jpeg|png|gif files are allowed."			
		    },
			partnerPopupImageUrl:{
				required:"Please click On partner popup image upload button",
				rangelength:"Maximum 100 characters permitted"
		    },		    
		    partnerIcon:{
				required:"Please upload partner icon",
				extension:"Invalid file format. Only jpg|jpeg|png|gif files are allowed."
		    },
			partnerIconUrl:{
				required:"Please click On partner icon upload button",
				rangelength:"Maximum 100 characters permitted"
		    },
		    partnerLandingUrl:{
		    	required:"Partner Landing Url field should not be empty"
			},
			partnerDesc:{
				required: "Please enter Partner Description"
			},			
			termsAndCondition:{
				required: "Please enter Terms and Conditions.",
				rangelength: "Maximum 600 characters permitted."
			},
			logoPartnerSearchUrl:{
		    	required:"Partner Search Url field should not be empty"
			},
			logoPrtDisplayOrder:{
				required: "Please enter display order ",
		 		digits:"Please enter numeric data",
		 		//displayOrderNotZero:"Logo Partner Display Order cannot be 0",
		 		dupLogoPartnerDispOrd:"Partner Display Order already exists",
				logoPrtDisplayOrderLessThan100 : "Partner Display Order must be less than or equal to 100"
			},	
			logoPrtTilePos:{
				required: "Please enter Partner Tile Position",
		 		digits:"Please enter numeric data",
		 		//tilePosNotZero:"Logo Partner Tile Position cannot be 0",
		 		dupLogoPartnerTilePos:"Partner Tile Position already exists",
		 		logoPrtTilePosLessThan100: "Partner Tile Position must be less than or equal to 100"
	 		},
			hostPartner:{
				isPartnerTypeSelected: "Please check atleast one partner type"
			},
			affiliateIdName:{
				//required: "Please enter affiliate ID name",
				AlphaNumericandsomeSpecial: "AlphaNumeric ,Space,Hyphen and Underscore only allowed"
			},
			sourceOfInfoName:{
				//required: "Please enter source of info name",
				AlphaNumericandsomeSpecial: "AlphaNumeric,Space,Hyphen and Underscore only allowed"
			},
			referenceTokenName:{
				required: "Please enter prefix of transaction name",
				AlphaNumericandsomeSpecial: "AlphaNumeric,Space,Hyphen and Underscore only allowed"
			},
			affiliateId:{
				//required: "Please enter param 1 value",
				AlphaNumericandsomeSpecial: "AlphaNumeric,Space,Hyphen and Underscore only allowed"
			},
			sourceOfInfo:{
				//required: "Please enter param 3 value",
				AlphaNumericandsomeSpecial: "AlphaNumeric,Space,Hyphen and Underscore only allowed"
			},
			referenceToken:{
				//required: "Please enter param 2 value",
				AlphaNumericandsomeSpecial_: "AlphaNumeric,Plus,Space,Hyphen,Curly Braces and Underscore only allowed"
			},
			jpmileDesc:{
				required: "Please enter JPMile Description"
			},
			jpmileReconPeriod:{
				required: "Please enter no of days",
		 		digits:"Please enter numeric data"
	 		},
	 		paramFourName:{
	 			AlphaNumericandsomeSpecial: "AlphaNumeric,Space,Hyphen and Underscore only allowed"
	 		},
	 		/* aggregateId:{
				required: "Please enter Aggregate ID",
				duplicateAggregateId:"Aggregate ID already exists",
			}, */
	 		paramFour:{
	 			rangelength: " Maximum 300 characters permitted."
	 			//AlphaNumericandsomeSpecial: "AlphaNumeric,Space,Hyphen and Underscore only allowed"
	 		}/* ,
			offLinePartnerSearchUrl:{
		    	required:"Offline Partner Search Url field should not be empty"
			},
			offLinePrtTilePos:{
				required: "Please enter Offline Partner Tile Position",
		 		digits:"Please enter numeric data"
		 		//dupLogoPartnerTilePos:"Offline Partner Tile Position already exists",
		 		//logoPrtTilePosLessThan100: "Offline Partner Tile Position must be less than or equal to 100"
	 		} */
		 },
		 errorElement: "div",
		 errorPlacement: function(error, element) {
			 
			if ($(element).hasClass("form-control startDate")) {
				error.insertAfter($(element).closest("div"));
			}
			else if ($(element).hasClass("form-control endDate")){
				error.insertAfter($(element).closest("div"));
			}
			else if($(element).hasClass("newPartnerImageUpload")){
				error.insertAfter($(element).closest("div"));
			}
			else if($(element).hasClass("newPartnerIconUpload")){
				error.insertAfter($(element).closest("div"));
			}			
			else if($(element).hasClass("newPartnerImagePopupUpload")){
				error.insertAfter($(element).closest("div"));
			} 			
			else if($(element).hasClass('param1')){
				$(".error_param1").append(error);
			}
			else if($(element).hasClass('param2')){
				$(".error_param2").append(error);
			}
			else if($(element).hasClass('param3')){
				$(".error_param3").append(error);
			}
			else if($(element).hasClass('param4')){
				$(".error_param4").append(error);
			}
			else if($(element).hasClass('param5')){
				$(".error_param5").append(error);
			} 
			else {
				error.insertAfter(element);
			}
		},
		submitHandler: function (form) {
			form.submit();
		}
	});	 
	
	
		$('#createPartnerMappingForm').validate({
			ignore:[],	
			rules: {
				createMappingPartnerName:{
					required: true,
					duplicateMappingPartnerName : true
				},
				partName:{
					required: true,
					duplicateMappingPartnerName : true
				},
				partnerTypeMapping:{
					required: true,
					},
				
			}, 
			messages: {
				createMappingPartnerName:{
					required: "Partner Mapping Name",
					 duplicateMappingPartnerName : "Partner Mapping Name already exists"
				 },
				 partName:{
					 required: "Please Select Partner Name",
					 duplicateMappingPartnerName : "Partner Name already exists"
				 },
				 partnerTypeMapping:{
						required:  "Please Select Partner",
						},
			},
			errorElement: "div",
			errorPlacement: function(error, element) {
			if ($(element).hasClass("uploadBtn3")){
		    	error.insertAfter($(element).closest("div"));
			}
			else {
		    	error.insertAfter(element);
			}

			},
		});
		
		
		
		$("#editPartnerMappingBean").validate({ 
			onkeyup: true,
			ignore:[],
		   	rules: {
		   		edit_partnermappingname:{
					required: true,
					duplicateMappingPartnerName : true
				},
				edit_partnername1:{
					required: true,
					duplicateMappingPartnerName :true
				},
				edit_PartnerTypeMapping:{
					required: true,
					},
			}, 
			messages: {
				edit_partnermappingname:{
					required: "Partner Mapping Name",
					 duplicateMappingPartnerName : "Partner Mapping Name already exists"
				 },
				 edit_partnername1:{
						required: "Please Select Partner Name",
						duplicateMappingPartnerName : "Partner Name already exists"
					},
					edit_PartnerTypeMapping:{
						required: "Please Select Partner",
						},
			},
		 errorElement: "div",
		 submitHandler: function (form) {
			form.submit();
		 },
		});
	
	
	changeValidationRule(false);
	changeValidationRuleforOffLinePrt(false);
});


$("#partnerName").change(function(){
	var partnerName=$('#partnerName').val();
	partnerName=encodeURIComponent(partnerName);
	$.get("${contextPath}/admin/checkPartnerName?partnerName="+partnerName, function(data,status) {
		if(status=="success" && data!=''){
			partnerNameExist=true;	
		}
		else{
			partnerNameExist=false;
		}
		$("#partnerBeanNew").validate().element('#partnerName');
	});
});

$("#partnerId").change(function(){
	var partnerId=$('#partnerId').val();
	$.get("${contextPath}/admin/checkPartnerID?partnerId="+partnerId, function(data,status) {
		if(status=="success" && data!=''){
			partnerIDExist=true;	
		}
		else{
			partnerIDExist=false;
		}
		$("#partnerBeanNew").validate().element('#partnerId');	
	});
});

$("#activityId").change(function(){
	var activityId=$('#activityId').val();
	$.get("${contextPath}/admin/checkActivityID?activityId="+activityId, function(data,status) {
		if(status=="success" && data!=''){
			activityIDExist=true;	
		}
		else{
			activityIDExist=false;
		}
		$("#partnerBeanNew").validate().element('#activityId');	
	});
});

/* $("#aggregateId").change(function(){
	var aggregateId=$('#aggregateId').val();
	$.get("${contextPath}/admin/checkAggregateID?aggregateId="+aggregateId, function(data,status) {
		if(status=="success" && data!=''){
			aggregateIDExist=true;	
		}
		else{
			aggregateIDExist=false;
		}
		$("#partnerBeanNew").validate().element('#aggregateId');	
	});
}); */


$('#datepicker1').on('change', function () {
	$("#partnerBeanNew").validate().element('#datepicker1'); 
	var endDate = getFormattedDate($('#datepicker2').val());
	
	if(endDate != 'Invalid Date'){
		$("#partnerBeanNew").validate().element('#datepicker2');
	}
});

$('#datepicker2').on('change', function () {
	$("#partnerBeanNew").validate().element('#datepicker2');
	$("#partnerBeanNew").validate().element('#datepicker1'); 
});

$('#logoPrtDisplayOrder').on('keyup', function () {
	var prtPos = parseInt($("#logoPrtDisplayOrder").val());
	logoPrtDispOrder = false;
	var prtType="";
	if($("#offLinePartner").is(':checked')){
		prtType = "offline";
	}
	if($("#readPartner").is(':checked')){
		prtType = "read";
	}
	if($("#talkPartner").is(':checked')){
		prtType = "talk";
	}
	if(prtPos == 0 || isNaN(prtPos)){
	
		//$("#partnerBeanNew").validate().element('#logoPrtDisplayOrder');
	}
	else{
		$.get("${contextPath}/admin/checkLogoPrtDispOrd?prtPos="+prtPos+"&prtType="+prtType, function(data,status) {
			if(status=="success" && data!=''){
				logoPrtDispOrder=true;	
			}
			else{
				logoPrtDispOrder=false;
			}
			$("#partnerBeanNew").validate().element('#logoPrtDisplayOrder');				
		});
	}
});

$('#logoPrtTilePos').on('keyup', function () {
	var prtPos = parseInt($("#logoPrtTilePos").val());
	
	logoPrtTilePos = false;
	var prtType="";
	if($("#offLinePartner").is(':checked')){
		prtType = "offline";
	}
	if($("#readPartner").is(':checked')){
		prtType = "read";
	}
	if($("#talkPartner").is(':checked')){
		prtType = "talk";
	}
	if($("#logoPartner").is(':checked')){
		prtType = "logo";
	}
	
	if($("#defaultPartner").is(':checked')){
		prtType = "default";
	}
	if(prtPos == 0 || isNaN(prtPos)){
		//$("#partnerBeanNew").validate().element('#logoPrtTilePos');
	}
	else{
		$.get("${contextPath}/admin/checkLogoPrtTilePos?prtPos="+prtPos+"&prtType="+prtType, function(data,status) {
			if(status=="success" && data!=''){
				logoPrtTilePos=true;
				
			}
			else{
				logoPrtTilePos=false;
			}
			$("#partnerBeanNew").validate().element('#logoPrtTilePos');
		});
	}
});

//On Change Methods For New Partner
$('#partnerImage').on('change', function() {
	$('#uploadFile').val($(this).val());
	console.log("inside on change image ");
});

$('#partner_Image_').on('change', function() {
	$('#upload_File_').val($(this).val());
	console.log("inside on change image popup");
});

/* $('#partnerIcon').on('change', function() {
	$('#uploadFile_').val($(this).val());
	 $("#partnerBeanNew").validate().element('#partnerIcon');
	
}); */

$('#hostPartner').on('change', function() {
	var ischecked= $(this).is(':checked');

	if(ischecked){
		$('#logoPrtDisplayOrder').trigger('keyup');
		$('#referenceTokenName').rules('add', 'required');
		changeValidationRule(false);
		changeValidationRuleforOffLinePrt(true);
		$('#logo_partner').hide();
		/* $('#jpMileDescDiv').show(); */
		$('#ratePerMileDiv').show();
		$('#jpMileReconDiv').show();
		$('#transactionDiv').show();
		$('#partnerDescDiv').hide();
		$('#ratePerMile').rules('add', 'required');
		$('#manStar').show();
		$("#partnerSearchUrl").hide();
	}
});

$('#defaultPartner').on('change', function() {
	var ischecked= $(this).is(':checked');

	if(parseInt($('#logoPrtTilePos').val()) == 0){
		$('#logoPrtTilePos').val('');
		
		$("#partnerBeanNew").validate().element('#logoPrtTilePos');
	}
	
	
	if(ischecked){
		$('#logoPrtDisplayOrder').trigger('keyup');
		$('#referenceTokenName').rules('add', 'required');
		$('#partnerDesc').rules('remove', 'required');
		changeValidationRule(true);
		changeValidationRuleforOffLinePrt(true);
        $('#ratePerMileDiv').show();
		$('#jpMileReconDiv').show();
		$('#transactionDiv').show();
		$('#partnerDescDiv').hide();
		$('#manStar').show();
		$('#transactionDiv').show();
		$('#ratePerMile').rules('add', 'required');
		$("#partnerSearchUrl").show();
		
		 $('#logo_partner').show(); 
		 $("#logoPrtPosLbl").hide();
		 $("#offlinePrtPosLbl").hide();
		 $("#logoPrtUrlLbl").hide();
		 $("#offlinePrtUrlLbl").hide();
		 $("#partnerPosOnSearchPg").show();
		 /* $('#logoPartnerSearchUrl').rules('add', 'required');
		 logoPrtTilePos
		 $('#logoPrtTilePos').rules('add', 'required'); */
		
	}
});

$('#logoPartner').on('change', function() {
	
	if(parseInt($('#logoPrtDisplayOrder').val()) == 0){
		$('#logoPrtDisplayOrder').val('');
		$("#partnerBeanNew").validate().element('#logoPrtDisplayOrder');
	}
	
	if(parseInt($('#logoPrtTilePos').val()) == 0){
		$('#logoPrtTilePos').val('');
		$("#partnerBeanNew").validate().element('#logoPrtTilePos');
	}

	var ischecked= $(this).is(':checked');
	
	if(ischecked){
		$('#logoPrtDisplayOrder').trigger('keyup');
		$('#referenceTokenName').rules('add', 'required');
		$('#partnerDesc').rules('remove', 'required');
		$('#ratePerMile').rules('add', 'required');
		changeValidationRule(true);
		changeValidationRuleforOffLinePrt(true);
		$('#logo_partner').show();
		$('#logoPrtPosLbl').show();
		$('#logoPrtUrlLbl').show();
		$('#ratePerMileDiv').show();
/* 		$('#jpMileDescDiv').show();
 */		$('#jpMileReconDiv').show();
		$('#offlinePrtPosLbl').hide();
		$('#offlinePrtUrlLbl').hide();
		$('#transactionDiv').show();
		$('#partnerDescDiv').hide();
		$('#manStar').show();
		$('#transactionDiv').show();
		$("#partnerPosOnSearchPg").hide();
		$("#partnerSearchUrl").hide();
	}
});

$('#talkPartner').on('change', function() {
	
	if(parseInt($('#logoPrtDisplayOrder').val()) == 0){
		$('#logoPrtDisplayOrder').val('');
		$("#partnerBeanNew").validate().element('#logoPrtDisplayOrder');
	}
	
	if(parseInt($('#logoPrtTilePos').val()) == 0){
		$('#logoPrtTilePos').val('');
		$("#partnerBeanNew").validate().element('#logoPrtTilePos');
	}

	var ischecked= $(this).is(':checked');
	
	if(ischecked){
		$('#logoPrtDisplayOrder').trigger('keyup');
		$('#referenceTokenName').rules('remove', 'required');
		$('#ratePerMile').rules('remove', 'required');
		//$('#logoPrtTilePos').rules('add', 'required');
		$('#logoPartnerSearchUrl').rules('add', 'required');
		changeValidationRule(true);
		changeValidationRuleforOffLinePrt(false);
		$('#partnerDesc').rules('add', 'required');
		$('#logo_partner').hide();
		$('#logoPrtPosLbl').hide();
		$('#logoPrtUrlLbl').hide();
		$('#ratePerMileDiv').show();
     	$('#logo_partner').show();
 	    $('#jpMileReconDiv').show();
		$('#transactionDiv').show();
		$('#partnerDescDiv').show();
		$('#manStar').hide();
		$('#transactionDiv').hide();
		$("#partnerPosOnSearchPg").show();
		$("#offlinePrtPosLbl").hide();
		$('#offlinePrtUrlLbl').hide();
		$("#partnerSearchUrl").show();
        
	}
});


$('#readPartner').on('change', function() {
	
	if(parseInt($('#logoPrtDisplayOrder').val()) == 0){
		$('#logoPrtDisplayOrder').val('');
		$("#partnerBeanNew").validate().element('#logoPrtDisplayOrder');
	}
	
	if(parseInt($('#logoPrtTilePos').val()) == 0){
		$('#logoPrtTilePos').val('');
		
		$("#partnerBeanNew").validate().element('#logoPrtTilePos');
	}

	var ischecked= $(this).is(':checked');
	
	if(ischecked){
		
		$('#logoPrtDisplayOrder').trigger('keyup');
		$('#referenceTokenName').rules('remove', 'required');
		$('#ratePerMile').rules('remove', 'required');
		//$('#logoPrtTilePos').rules('add', 'required');
		changeValidationRule(true);
		changeValidationRuleforOffLinePrt(false);
		$('#logoPartnerSearchUrl').rules('add', 'required');
		$('#partnerDesc').rules('add', 'required');
		$('#logo_partner').hide();
		$('#logoPrtPosLbl').hide();
		$('#logoPrtUrlLbl').hide();
		$('#ratePerMileDiv').show();
/* 		$('#jpMileDescDiv').show();
 */		$('#jpMileReconDiv').show();
		$('#transactionDiv').show();
		$('#partnerDescDiv').show();
		$('#logo_partner').show();
		$('#manStar').hide();
		$('#transactionDiv').hide();
		//offlinePrtPosLbl partnerPosOnSearchPg
	    $("#partnerPosOnSearchPg").show();
		$("#offlinePrtPosLbl").hide();
		$("#partnerSearchUrl").show();
		$('#offlinePrtUrlLbl').hide();
		
		}
});

$('#offLinePartner').on('change', function() {
	if(parseInt($('#logoPrtDisplayOrder').val()) == 0){
		$('#logoPrtDisplayOrder').val('');
		$("#partnerBeanNew").validate().element('#logoPrtDisplayOrder');
	}
	
	if(parseInt($('#logoPrtTilePos').val()) == 0){
		$('#logoPrtTilePos').val('');
		$("#partnerBeanNew").validate().element('#logoPrtTilePos');
	}
	
	var ischecked= $(this).is(':checked');
	
	if(ischecked){
		$('#logoPrtDisplayOrder').trigger('keyup');
		$('#referenceTokenName').rules('remove', 'required');
		$('#partnerDesc').rules('add', 'required');
		changeValidationRule(true);
		changeValidationRuleforOffLinePrt(false);
		$('#logo_partner').show();
		$('#offlinePrtPosLbl').show();
		$('#offlinePrtUrlLbl').show();
		$('#logoPrtPosLbl').hide();
		$('#logoPrtUrlLbl').hide();
		$('#ratePerMileDiv').hide();
/* 		$('#jpMileDescDiv').show();
 */		$('#jpMileReconDiv').hide();
		$('#transactionDiv').hide();
		$('#partnerDescDiv').show();
		$("#partnerPosOnSearchPg").hide();
		$("#partnerSearchUrl").hide();

	}
});

$.validator.addMethod('isPartnerTypeSelected', function(value, element) {
	if($("#hostPartner").is(':checked') || $("#defaultPartner").is(':checked') || $("#logoPartner").is(':checked') || $("#offLinePartner").is(':checked') ){
		return true;
	}
	else{
		return false;
	}
});
</script>

<!-- Edit Logo Partner -->
<script type="text/javascript">
$(document).ready(function () {
	$.validator.addMethod('edit_logoPrtDispOrdLessThan100', function(value, element) {
		var isValid = false;
		var sortOrder = $("#edit_logoPrtDispOrd").val();
		
		if(sortOrder > 100){
			isValid = true;
		}
		return !isValid;
	});
	$.validator.addMethod('edit_logoPrtTilePosLessThan100', function(value, element) {
		var isValid = false;
		var sortOrder = $("#edit_logoPrtTilePos").val();
		
		if(sortOrder > 100){
			isValid = true;
		}
		return !isValid;
	});
	$("#editLogoPartnerBean").validate({
		ignore:[],	
		rules: {
			edit_partnerid_:{
				required: true,
				duplicatePartnerId:true,
				AlphaNumericOnly: true
			},
			edit_activityid_:{
				required: true,
				duplicateActivityId:true
			},
			edit_partnername_:{
				required: true,
				alpha: true,
				rangelength:[3,100],
				duplicatePartnerName:true
	        },	               
			edit_ratepermile_:{
				required: true,
				number: true
			},
			edit_logoPrtLandingUrl:{
				required:true,
				rangelength:[1,200],
				url:true
			},
			edit_logoPrtDispOrd:{
				required: true,
				digits:true,
				//displayOrderNotZero_:true,
				dupLogoPartnerDispOrd:true,
				edit_logoPrtDispOrdLessThan100 : true
			},
			edit_logoPrtTilePos:{
	 			required: true,
	 			digits:true,
	 			//tilePosNotZero_:true,
	 			dupLogoPartnerTilePos:true,
	 			edit_logoPrtTilePosLessThan100 : true
	 		},
			datepicker11_:{
				required: true,
				startDate_:true
			},
			datepicker12_:{
				endDate_:true
			},
			editPartnerImageBtn:{
				extension: "png|jpg|jpeg|gif",
				filesize: 1100000
			},
			editPartnerPopupImageBtn:{
				extension: "png|jpg|jpeg|gif",
				filesize: 1100000
			},
			edit_partnerimageurl_:{
				required: true,
				rangelength:[1,100]
			},
			edit_partnerpopupimageurl_:{
				required: true,
				rangelength:[1,100]
			},
			edit_partnericonurl_:{
				required: true,
				rangelength:[1,100]
			},
			editPartnerIconBtn:{
				//required: true,
				extension:"png|jpg|jpeg|gif"
			},
			edit_affiliateIdName_:{
				//required: true,
				AlphaNumericandsomeSpecial: true
			},
			edit_sourceOfInfoName_:{
				//required: true,
				AlphaNumericandsomeSpecial: true
			},
			edit_referenceTokenName_:{
				required: true,
				AlphaNumericandsomeSpecial: true
			},
			edit_affiliateId_:{
				//required: true,
				AlphaNumericandsomeSpecial: true
			},
			edit_sourceOfInfo_:{
				//required: true,
				AlphaNumericandsomeSpecial: true
			},
			edit_referenceToken_:{
				//required: true,
				AlphaNumericandsomeSpecial_: true
			},
			editLogo_partnerimageurlHidden:{
				imageUpload: true
			},
			editLogo_partnericonurlhidden:{
				iconUpload: true
			},
			edit_logoJpmileDesc:{
				required: true
			},
			edit_logo_jpmileReconPeriod:{
				required: true,
				digits:true
			},
			edit_paramFourName_:{
				AlphaNumericandsomeSpecial: true
			},
			edit_paramFour_:{
				rangelength:[0,300]
				//AlphaNumericandsomeSpecial: true
			},
			edit_logoTermsAndCondition:{
				required: true,
				rangelength: [1,600]
			},
			edit_logoPartnerDesc:{
				required: true,
			},
			edit_logoPrtSearchUrl:{
				required:true,
				rangelength:[1,200],
				url:true
			},
			/* edit_logo_aggregateId:{
				required: true,
				duplicateAggregateId:true
			}, */
 		},
		messages: {
			edit_partnername_:{
				required:"Please enter partner name",
				rangelength:"Partner name should be between 3-100 Characters",
				alpha:"Please Enter Valid Special Charaters (Only -.#&')",
				duplicatePartnerName:"Partner Name already exists"
			},	 
			edit_partnerid_:{
				required:"Please enter Partner ID ",
				duplicatePartnerId:"Partner ID already exists",
				AlphaNumericOnly:"AlphaNumeric only(No special characters allowed)"
			},
			edit_activityid_:{
				required:"Please enter Activity ID ",
				duplicateActivityId:"Activity ID already exists"
			},
			edit_ratepermile_:{
				required:"Please enter Rate Per Mile ",
				number:"Only numeric values are allowed"
			},
			edit_logoPrtLandingUrl:{
				required:"Partner Landing Url field should not be empty"
			},
			edit_logoPrtDispOrd:{
				required: "Please enter display order",
		 		digits:"Please enter numeric data",
		 		//displayOrderNotZero_:"Logo Partner Display Order cannot be 0",
		 		dupLogoPartnerDispOrd:"Partner Display Order already exists",
		 		edit_logoPrtDispOrdLessThan100: "Partner Display Order must be less than or equal to 100"
			},	
			edit_logoPrtTilePos:{
				required: "Please enter Partner Tile Position",
		 		digits:"Please enter numeric data",
		 		//tilePosNotZero_:"Logo Partner Tile Position cannot be 0",
		 		dupLogoPartnerTilePos:"Partner Tile Position already exists",
		 		edit_logoPrtTilePosLessThan100: "Partner Tile Position must be less than or equal to 100"
		 	},
			datepicker11_:{
				required: "Please Select Activation Date",
				startDate_:"Activation date must be less than or equal to Deactivation date "
			},
			datepicker12_:{
				endDate_:"Deactivation date must be greater than or equal to Activation date "
			},
			editPartnerImageBtn:{
				extension:"Invalid file format. Only jpg|jpeg|png|gif files are allowed."
			},	
			editPartnerPopupImageBtn:{
				extension:"Invalid file format. Only jpg|jpeg|png|gif files are allowed."
			},
			edit_partnerimageurl_:{
				required:"Please Upload Partner Image",
				rangelength:"Maximum 100 characters permitted"
			},
			edit_partnerpopupimageurl_:{
				required:"Please Upload Partner Popup Image",
				rangelength:"Maximum 100 characters permitted"
			},
			edit_partnericonurl_:{
				required:"Partner Icon Url field should not be empty",
				rangelength:"Maximum 100 characters permitted"
			},
			editPartnerIconBtn:{
				//required:"Please Upload Partner icon",
				extension: "Invalid file format. Only jpg|jpeg|png|gif files are allowed."
			},
			edit_affiliateIdName_:{
				//required: "Please enter affiliate ID name",
				AlphaNumericandsomeSpecial: "AlphaNumeric,Space,Hyphen and Underscore only allowed"
			},
			edit_sourceOfInfoName_:{
				//required: "Please enter source of into name",
				AlphaNumericandsomeSpecial: "AlphaNumeric,Space,Hyphen and Underscore only allowed"
			},
			edit_referenceTokenName_:{
				required: "Please enter prefix of transaction name",
				AlphaNumericandsomeSpecial: "AlphaNumeric,Space,Hyphen and Underscore only allowed"
			},
			edit_affiliateId_:{
				//required: "Please enter param 1 value",
				AlphaNumericandsomeSpecial: "AlphaNumeric,Space,Hyphen and Underscore only allowed"
			},
			edit_sourceOfInfo_:{
				//required: "Please enter param 3 value",
				AlphaNumericandsomeSpecial: "AlphaNumeric,Space,Hyphen and Underscore only allowed"
			},
			edit_referenceToken_:{
				//required: "Please enter param 2 value",
				AlphaNumericandsomeSpecial_: "AlphaNumeric,Plus,Space,Hyphen,Curly Braces and Underscore only allowed"
			},
			editLogo_partnerimageurlHidden:{
				imageUpload: "Please click On partner image upload button",
			},
			editLogo_partnericonurlhidden:{
				iconUpload: "Please click On partner icon upload button"
			},
			edit_logoJpmileDesc:{
				required: "Please enter JPMile Description"
			},
			edit_logo_jpmileReconPeriod:{
				required: "Please enter no of days",
				digits:"Please enter numeric data"
			},
			edit_paramFourName_:{
				AlphaNumericandsomeSpecial: "AlphaNumeric,Space,Hyphen and Underscore only allowed"
			},
			edit_paramFour_:{
				rangelength:" Maximum 300 characters permitted."
				//AlphaNumericandsomeSpecial: "AlphaNumeric,Space,Hyphen and Underscore only allowed"
			},
			edit_logoTermsAndCondition:{
				required: "Please enter Terms and Conditions.",
				rangelength: "Maximum 600 characters permitted."
			},
			edit_logoPartnerDesc:{
				required: "Please enter Partner Description"
			},
			edit_logoPrtSearchUrl:{
				required:"Partner Search Url field should not be empty",
			},
			/* edit_logo_aggregateId:{
				required:"Please enter Aggregate ID ",
				duplicateAggregateId:"Aggregate ID already exists"
			}, */
		},
 		errorElement: "div",
 		errorPlacement: function(error, element) {
			if ($(element).hasClass("form-control startDate")) {
		    	error.insertAfter($(element).closest("div"));
			}
			else if ($(element).hasClass("form-control endDate")){
		 		error.insertAfter($(element).closest("div"));
			}
			else if ($(element).hasClass("editPartnerImageUpload")){
		    	error.insertAfter($(element).closest("div"));
			}
			else if ($(element).hasClass("editPartnerPopupImageUpload")){
		    	error.insertAfter($(element).closest("div"));
			}
			else if($(element).hasClass("logoUrlBtn")){
				$('.logoIconBtn').append(error);
			}
			else {
		    	error.insertAfter(element);
			}
 		},
		submitHandler: function (form) {
			form.submit();
		},
	});
});

$("#edit_partnerid_").change(function(){
	var partnerId = $('#edit_partnerid_').val();
	var partnerIdHidden = $('#edit_partnerhiddenid_').val();
	partnerIDExist=false;
	
	if(partnerId != partnerIdHidden){		
		$.get("${contextPath}/admin/checkPartnerID?partnerId="+partnerId, function(data,status) {
			if(status=="success" && data!=''){
				partnerIDExist=true;	
			}
			else{
				partnerIDExist=false;
			}
			$("#editLogoPartnerBean").validate().element('#edit_partnerid_');
		});
	}
	else{
		$("#editLogoPartnerBean").validate().element('#edit_partnerid_');
	}
});

$("#edit_logoDisplayPrtPopup").on('change', function() {
    if($(this).is(":checked")) {
    	$('#edit_logoDisplayPrtPopup').val(1);
    }
    else{
    	$('#edit_logoDisplayPrtPopup').val(0);
    }
});

$("#edit_logoAllowJpnumber").on('change', function() {
    if($(this).is(":checked")) {
    	$('#edit_logoAllowJpnumber').val(1);
    }
    else{
    	$('#edit_logoAllowJpnumber').val(0);
    }
});

$("#edit_activityid_").change(function(){
	var activityId = $('#edit_activityid_').val();
	var activityIdHidden = $('#edit_activityhiddenid_').val();
	activityIDExist=false;
	
	if(activityId != activityIdHidden){		
		$.get("${contextPath}/admin/checkActivityID?activityId="+activityId, function(data,status) {
			if(status=="success" && data!=''){
				activityIDExist=true;	
			}
			else{
				activityIDExist=false;
			}
			$("#editLogoPartnerBean").validate().element('#edit_activityid_');
		});
	}
	else{
		$("#editLogoPartnerBean").validate().element('#edit_activityid_');
	}
});

/* $("#edit_logo_aggregateId").change(function(){

	var aggregateId = $('#edit_logo_aggregateId').val();
	var aggregateIdHidden = $('#edit_aggregatehiddenid_').val();
	activityIDExist=false;
	
	if(aggregateId != aggregateIdHidden){		
		$.get("${contextPath}/admin/checkAggregateID?aggregateId="+aggregateId, function(data,status) {
			if(status=="success" && data!=''){
				aggregateIDExist=true;	
			}
			else{
				aggregateIDExist=false;
			}
			$("#editLogoPartnerBean").validate().element('#edit_logo_aggregateId');
		});
	}
	else{
		$("#editLogoPartnerBean").validate().element('#edit_logo_aggregateId');
	}
}); */


$("#edit_partnername_").change(function(){
	var partnerName = $('#edit_partnername_').val();
	partnerName=encodeURIComponent(partnerName);
	var partnerNameHidden = $('#edit_partnerhiddenname_').val();
	partnerNameExist=false;
	
	if(partnerName != partnerNameHidden){
		$.get("${contextPath}/admin/checkPartnerName?partnerName="+partnerName, function(data,status) {
			if(status=="success" && data!=''){
				partnerNameExist=true;	
			}
			else{
				partnerNameExist=false;
			}
			$("#editLogoPartnerBean").validate().element('#edit_partnername_');
		});
	}
	else{
		$("#editLogoPartnerBean").validate().element('#edit_partnername_');
	}
});

$("#edit_ratepermile_").on('blur', function(){
	$("#editLogoPartnerBean").validate().element('#edit_ratepermile_');
});

$("#edit_logoPrtLandingUrl").on('blur', function(){
	$("#editLogoPartnerBean").validate().element('#edit_logoPrtLandingUrl');
});

$("#edit_logoPrtDispOrd").on('blur', function(){
	$("#editLogoPartnerBean").validate().element('#edit_logoPrtDispOrd');
});

$("#edit_logoPrtTilePos").on('blur', function(){
	$("#editLogoPartnerBean").validate().element('#edit_logoPrtTilePos');
});
$("#edit_logoPrtTilePos").on('blur', function(){
	$("#editLogoPartnerBean").validate().element('#edit_logoPrtTilePos');
});

$("#edit_logoPartnerDesc").on('blur', function(){
	$("#editLogoPartnerBean").validate().element('#edit_logoPartnerDesc');
});
$("#edit_logoJpmileDesc").on('blur', function(){
	$("#editLogoPartnerBean").validate().element('#edit_logoJpmileDesc');
});
$('#edit_prtDispOrd').on('keyup', function () {
	var prtDispOrd = parseInt($("#edit_prtDispOrd").val());
	var prtDispOrdHidden = parseInt($("#edit_prtDispOrdHidden").val());
	logoPrtDispOrder = false;
	var prtType="";
	if($("#offLinePartner").is(':checked')){
		prtType = "offline";
	}
	if($("#edit_readpartner").is(':checked')){
		prtType = "read";
	}
	if($("#edit_talkpartner").is(':checked')){
		prtType = "talk";
	}
	if(prtDispOrd != prtDispOrdHidden && prtDispOrd != 0){
		$.get("${contextPath}/admin/checkLogoPrtDispOrd?prtPos="+prtDispOrd+"&prtType="+prtType, function(data,status) {
			if(status=="success" && data!=''){
				logoPrtDispOrder=true;	
			}
			else{
				logoPrtDispOrder=false;
			}
			
			$("#editPartnerBean").validate().element('#edit_prtDispOrd');				
		});
	}
	else{
		$("#editPartnerBean").validate().element('#edit_prtDispOrd');
	}
});

$('#edit_logoPrtDispOrd').on('keyup', function () {
	var logoPrtDispOrd = parseInt($("#edit_logoPrtDispOrd").val());
	var logoPrtDispOrdHidden = parseInt($("#edit_logoPrtDispOrdHidden").val());
	logoPrtDispOrder = false;
	var prtType="";
	if($("#edit_offlinepartner").is(':checked')){
		prtType = "offline";
	}
	if($("#edit_readpartner").is(':checked')){
		prtType = "read";
	}
	if($("#edit_talkpartner").is(':checked')){
		prtType = "talk";
	}
	if(logoPrtDispOrd != logoPrtDispOrdHidden && logoPrtDispOrd != 0){
		$.get("${contextPath}/admin/checkLogoPrtDispOrd?prtPos="+logoPrtDispOrd+"&prtType="+prtType, function(data,status) {
			if(status=="success" && data!=''){
				logoPrtDispOrder=true;	
			}
			else{
				logoPrtDispOrder=false;
			}
			
			$("#editLogoPartnerBean").validate().element('#edit_logoPrtDispOrd');				
		});
	}
	else{
		$("#editLogoPartnerBean").validate().element('#edit_logoPrtDispOrd');
	}
});

$('#edit_logoPrtTilePos').on('keyup', function () {
	var logoPrtTilePosition = parseInt($("#edit_logoPrtTilePos").val());
	var logoPrtTilePositionHidden = parseInt($("#edit_logoPrtTilePosHidden").val());
	logoPrtTilePos = false;
	var prtType="";
	if($("#edit_offlinepartner").is(':checked')){
		prtType = "offline";
	}
	if(logoPrtTilePosition != logoPrtTilePositionHidden && logoPrtTilePosition != 0){
		$.get("${contextPath}/admin/checkLogoPrtTilePos?prtPos="+logoPrtTilePosition+"&prtType="+prtType, function(data,status) {
			if(status=="success" && data!=''){
				logoPrtTilePos=true;	
			}
			else{
				logoPrtTilePos=false;
			}
			
			$("#editLogoPartnerBean").validate().element('#edit_logoPrtTilePos');
		});
	}
	else{
		$("#editLogoPartnerBean").validate().element('#edit_logoPrtTilePos');
	}
});

//jagjeet tileposition
$('#edit_talkReadTilPos').on('keyup', function () {
	var logoPrtTilePosition = parseInt($("#edit_talkReadTilPos").val());
	var logoPrtTilePositionHidden = parseInt($("#edit_talkReadTilPosHidden").val());
	logoPrtTilePos = false;
	var prtType=""; //  edit_talkpartner edit_readpartner
	if($("#edit_talkpartner").is(':checked')){
		prtType = "talk";
	}
	if($("#edit_readpartner").is(':checked')){
		prtType = "read";
	}
	if($("#edit_defaultpartner").is(':checked')){
		prtType = "default";
	}
	if(logoPrtTilePosition != logoPrtTilePositionHidden && logoPrtTilePosition != 0){
		$.get("${contextPath}/admin/checkLogoPrtTilePos?prtPos="+logoPrtTilePosition+"&prtType="+prtType, function(data,status) {
			if(status=="success" && data!=''){
				logoPrtTilePos=true;	
			}
			else{
				logoPrtTilePos=false;
			}
			
			$("#editPartnerBean").validate().element('#edit_talkReadTilPos');
		});
	}
	else{
		$("#editPartnerBean").validate().element('#edit_talkReadTilPos');
	}
});


$('#datepicker11_').on('change', function () {
	$("#editLogoPartnerBean").validate().element('#datepicker11_');
	var endDate = getFormattedDate($('#datepicker12_').val());
	
	if(endDate != 'Invalid Date'){
		$("#editLogoPartnerBean").validate().element('#datepicker12_');
	}
});

$('#datepicker12_').on('change', function () {
	$("#editLogoPartnerBean").validate().element('#datepicker11_'); 
	$("#editLogoPartnerBean").validate().element('#datepicker12_'); 
});


$('#editPartnerImageBtn').on('change', function() {
	$('#edit_partnerimageurl_').val($(this).val());
});
$('#editPartnerPopupImageBtn').on('change', function() {
	$('#edit_partnerpopupimageurl_').val($(this).val());
});
$('#editPartnerIconBtn').on('change', function() {
	$('#edit_partnericonurl_').val($(this).val());
});

$.validator.addMethod('displayOrderNotZero_', function(value, element) {
	var isValid = false;
	var logoPrtDisplayOrder = parseInt($("#edit_logoPrtDispOrd").val());
	
	if(logoPrtDisplayOrder == 0){
		isValid = true;
	}
	return !isValid; 
});

$.validator.addMethod('tilePosNotZero_', function(value, element) {
	var isValid = false;
	var logoPrtTilePos = parseInt($("#edit_logoPrtTilePos").val());
	
	if(logoPrtTilePos == 0){
		isValid = true;
	}
	
	return !isValid; 
});

$.validator.addMethod("startDate_", function(value, element) {
	var startDate = getFormattedDate($('#datepicker11_').val());
	var endDate = getFormattedDate($('#datepicker12_').val());
	if(endDate=='Invalid Date')
		return true;
	if (startDate <= endDate)
		return true;
	if (startDate >= endDate)
		return false;
});

$.validator.addMethod("endDate_", function(value, element) {
	var startDate = getFormattedDate($('#datepicker11_').val());
	var endDate = getFormattedDate($('#datepicker12_').val());
	if(endDate=='Invalid Date')
		return true;
	if (endDate >= startDate)
		return true;
	if (endDate <= startDate)
		return false;
});

</script>
<script type="text/javascript">
$(document).ready(function () {
	<c:if test="${mappingKeyWordTab==true}">
		$('#viewPartnerTab').removeClass('active');
		$('#sectionA').removeClass('in active');
		$('#mapKeywordTab').addClass('active');
		$('#logoPartnerConfigurationTab').removeClass('active');
		
		$('#sectionC').addClass('in active');
		var logoPrtNo=$('#hiddenLogoPrtNo').val();
		 
		var prtType = $('#hiddenPrtType').val();
		 
		$("#prtType").val(prtType);
		$("#prtType").trigger("selectmenuchange", ["refresh", logoPrtNo]);
		//$("#prtType").selectmenu("refresh");
		 
		//setTimeout(function () {
		//	$('#logoPrtNo').val(logoPrtNo);
		//	$('#logoPrtNo').selectmenu("refresh");
		//}, 1);
		
	</c:if>
	<c:if test="${logoPartnerConfigurationTab==true}">
	$('#viewPartnerTab').removeClass('active');
	$('#sectionA').removeClass('in active');
	$('#mapKeywordTab').removeClass('active');
	$('#logoPartnerConfigurationTab').addClass('active');
	$('#sectionC').removeClass('active');
	$('#sectionD').addClass('in active');
	
	var logoPrtNo=$('#hiddenLogoPrtNo').val();
	var prtType = $('#hiddenPrtType').val();
	 
	$("#prtType").val(prtType);
	$("#prtType").trigger("selectmenuchange", ["refresh", logoPrtNo]);
	
	//$("#prtType").selectmenu("refresh");
	 
	//setTimeout(function () {
	//	$('#logoPrtNo').val(logoPrtNo);
	//	$('#logoPrtNo').selectmenu("refresh");
	//}, 1);
	</c:if>
	//CategoryTextTab
	<c:if test="${CategoryTextTab==true}">
	$('#viewPartnerTab').removeClass('active');
	$('#sectionA').removeClass('in active');
	$('#mapKeywordTab').removeClass('active');
	$('#logoPartnerConfigurationTab').removeClass('active');
	$('#categoryMappingTab').addClass('active');
	$('#sectionE').addClass('in active');
	

	</c:if>
	<c:if test="${CategoryTextTab1==true}">
	$('#viewPartnerTab').removeClass('active');
	$('#sectionA').removeClass('in active');
	$('#mapKeywordTab').removeClass('active');
	$('#logoPartnerConfigurationTab').removeClass('active');
	$('#categoryMappingTab').removeClass('active');
	$('#sectionE').removeClass('in active');
	$('#partnerMappingList').addClass('active');
	$('#sectionG').addClass('in active');
	

	</c:if>
	
	
	$("#uploadCSVForm").validate({
		ignore:[],	
		rules: {
			prtType:{
				required: true
			},
			logoPrtNo:{
				required: true
			},
			uploadCSVFile:{
				required: true,
				extension: "csv"
			}
 		},
		messages: {
			prtType:{
				required: "Please select a partner"
			},
			logoPrtNo:{
				required: "Please select a partner name"
			},
			uploadCSVFile:{
				required: "Please upload csv file",
				extension: "File format shoud be .csv"
			}
		},
 		errorElement: "div",
 		errorPlacement: function(error, element) {
			if ($(element).hasClass("uploadBtn2")){
		    	error.insertAfter($(element).closest("div"));
			}
			else {
		    	error.insertAfter(element);
			}
 		},
		submitHandler: function (form) {
			form.submit();
		},
	});
	
});

$('#logoPrtNo').on('selectmenuchange', function() {
	var logoprtNo=$('#logoPrtNo').val() ;
	var prttype=$('#prtType').val() ;

	$.redirect("${contextPath}/admin/getKeywordFileName", {'logoprtNo': logoprtNo, 'prttype': prttype});				
});

 $('span#deleteFile').on('click', function () { 
	 var id = $(this).closest('tr').data('id_');
	var filepath= $(this).closest('tr').data('filepath');
	$('#deleteFileId').text(filepath+" "+ " file will be deleted. Do you want to continue?");
	$('#deleteFileModal').data('id', id).modal('show');
	/* $(param).parents('tr').remove(); */
	if($( ".uploadedFiles_table tbody" ).children('tr').length < 1){
		$(".uploadedFiles_table").css('display','none');
	}
});

$('#btnFile').click(function () {
	var id = $('#logoPrtNo').val();
	var prttype = $('#prtType').val();
	 
	$.get("${contextPath}/admin/deleteFile?prtNo="+id , function(status) {		
		if(status=="success" ){
			$('[data-id=' + id + ']').remove();
			$('#deleteFileModal').modal('hide');
		  window.location.href="${contextPath}/admin/partner?deleteFileMsg="+status+"&logoprtNo="+id+"&prttype="+prttype;

		}
		if(status=="failure" ){
			$('#deleteFileModal').data('id', id).modal('show');
			 window.location.href="${contextPath}/admin/partner?deleteFileMsg="+status+"&logoprtNo="+id+"&prttype="+prttype;

		}
	});
	
});


$('#uploadCSVFile').on('change', function() {
	var filename = $(this).val();
	$('#uploadFile2').val($(this).val());
	$('#filename').val($("#logoPrtNo option:selected").text());
	/* $('#filename').val($(this).val()); */
	$("#uploadCSVForm").validate().element('#uploadCSVFile');

});

/* $("#uploadBtn2").change(function () {
    var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
    if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
    }
}); */

$("#prtType").on( "selectmenuchange", function(event, type, logoPrt){
	var obj=null;
	var prtType = $("#prtType").val();
	 
	$.get("${contextPath}/admin/getOffLinePartner?prtType="+prtType, function(data){
		var obj = jQuery.parseJSON(data);
		$('#logoPrtNo').empty();
		$('#logoPrtNo').append("<option value=''>-Select-</option>");
		
		for(var i = 0; i < obj.length; i++){
			var value = obj[i];
			var newOption = $("<option value="+value.prtNo+">"+value.partnerName+"</option>");
			$('#logoPrtNo').append(newOption);	       
		}
		
		
		if(type == "refresh"){
			$("#prtType").selectmenu("refresh");
			$('#logoPrtNo').val(logoPrt);
			$("#logoPrtNo").selectmenu("refresh");
		}else{
			$( "#logoPrtNo" ).selectmenu( "refresh" );
		}
	});
	
	
	 
});


$("#prtTypeCatMapping").on( "selectmenuchange", function(event, type, mapCat){
//        alert("echeck");
	var obj=null;
	var prtType = $("#prtTypeCatMapping").val();
	$.get("${contextPath}/admin/getCategoryMappingPartner?prtType="+prtType, function(data){
		var obj = jQuery.parseJSON(data);
		$('#prtNameCatMapping').empty();
		$('#prtNameCatMapping').append("<option value=''>-Select-</option>");
		
		for(var i = 0; i < obj.length; i++){
			var value = obj[i];
			var newOption = $("<option value="+value.prtNo+">"+value.partnerName+"</option>");
			$('#prtNameCatMapping').append(newOption);	       
		}
		$( "#prtTypeCatMapping" ).selectmenu( "refresh" );
		
		 if(type == "refresh"){
			$("#prtTypeCatMapping").selectmenu("refresh");
			$('#prtNameCatMapping').val(mapCat);
		}else{
			$("#prtNameCatMapping").selectmenu( "refresh" );
			$("#textCatMapping").val('');
		} 
	});
	 
});

$('#fetchLink').on('click', function(event, type, fetch) {
	var obj=null;
	var prtTypeCatMapping = $("#prtTypeCatMapping").val();
	var prtNameCatMapping = $("#prtNameCatMapping").val();
	
	$.get("${contextPath}/admin/getPartnerByCategory?prtNameCatMapping="+prtNameCatMapping, function(data){
		$('#textCatMapping').val(data);
		$("#partnerCatMapping").validate().element('#textCatMapping')
      });
	
	 /* $("#prtNameCatMapping").on('selectmenuchange', function(event, type, mapCatGet {
	    $('#textCatMapping').val('');
		var prtTypeCatMapping = $("#prtTypeCatMapping").val();
		var prtNameCatMapping = $("#prtNameCatMapping").val();
		
		$.get("${contextPath}/admin/getPartnerByCategory?prtNameCatMapping="+prtNameCatMapping, function(data){
			$('#textCatMapping').val(data);
	      });
		
		
	    });  */

});


//current
 $("#prtNameCatMapping").on( "selectmenuchange", function(event, type, mapCatGet){
	
	var prtTypeCatMapping = $("#prtTypeCatMapping").val();
	var prtNameCatMapping = $("#prtNameCatMapping").val();
	$.get("${contextPath}/admin/getCategoryTextMapped?prtNameCatMapping="+prtNameCatMapping, function(data){
		$('#textCatMapping').val(data);
      });
}); 

</script>	


<script type="text/javascript">
$(document).ready(function () {
	
	$("#logoPartnerConfigForm").validate({
		ignore:[],	
		rules: {
			configTileNo:{
				required: true,
				digits:true,
				conFigTile:true
			},
			configTotalLogoPartner:{
				//required: true,
				digits:true
			}
 		},
		messages: {
			configTileNo:{
				required: "Please enter tile number",
				digits:"Please enter numeric data",
				conFigTile:"Tile number should be between 0 to 12"
			},
			configTotalLogoPartner:{
				required: "Please enter number of logo partner",
				digits:"Please enter numeric data",
			}
		},
 		errorElement: "div",
 		errorPlacement: function(error, element) {
	    	error.insertAfter(element);
 		},
		submitHandler: function (form) {
			form.submit();
		},
	});
	
});


/*  Pagination OffersDeal */
var table1;
var table2; 
 $(document).ready(function() {
  var partnerType = $('#partnerType').val();
//  alert("partner type ----> "+partnerType);
  if(partnerType == 'host' ||partnerType == 'default' || partnerType == 'read' ||partnerType == 'talk' ){
  table1= $('#table').DataTable( {
						      "iDisplayLength" : 10,
						      "ordering": false,
						      "bPaginate": true,
						      "bProcessing": true,
						       "searching": false,
						       "scrollX": true, 
						      "serverSide": true,
						  "ajax":"${contextPath}/admin/partnerServerPagination?partnerType="+partnerType,
						  "type": 'GET',
						  "columns": [
							  
							     {"data":"prtNo"},
							     {"data":"jpmileDesc"},
							     {"data":"partnerDesc"},
							     {"data":"termsAndCondition"},
							     { "data": "partnerId"},
							    
							     {"data":"jpmileReconPeriod"},
							     {"data":"displayPrtPopup"},
							     {"data":"paramFour"},
							     {"data":"paramFourName"},
							     { "data": "sourceOfInfo"},
							     {"data":"allowJpnumber"},
							     
							     {"data":"hostPartnerCode"},
							     {"data":"defaultPartnerCode"},
							     {"data":"readPartnerCode"},
							     {"data":"talkPartnerCode"},
							     
							     { "data": "partnerId"},
						        { "data": "activityId"},
						        { "data": "partnerName" }, 
						        { "data": "partnerImageUrl" },
						        { "data": "partnerIconUrl" },
						        { "data": "partnerLandingUrl" },
						        { "data": "logoPrtDisplayOrder" },
						        { "data": "activationDate" },
						        { "data": "deactivationDate" },
						        { "data": "partnerType" },
						        { "data": "ratePerMile" },
						        { "data": "partnerStatus" },
						        {
					                "data": "null",
					                className: "center",
					                defaultContent: '<a class="view_details" data-target="#myModal2"><img class="edit_wraper" src="${contextPath}/static/img/View_details_icon1.png"></a> '
					                +'<a class="editpartner" data-target="#myModal1"><img src="${contextPath}/static/img/edit_icon.png"></img></a>'
					                +'<a class="delete btnDelete" data-target="#myModala" id="deletePartner" ><img src="${contextPath}/static/img/delete_icon1.png" class="fa fa-trash fa-lg"></img></a>'
					            }  ,
					            { "data": "aggregateId" },
						        {"data":"referenceTokenName"},
						        
						  
						    ]  ,
						  "columnDefs": [
							   {
					                "targets": [ 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,28,29],
					                "visible": false,
					                "searchable": false
					            }, 
					            
					          
							  { "defaultContent": "-", "targets": "_all" }
							  ],
							  "createdRow": function( row, data, dataIndex){
								  	//$(row).attr('id', data.partnerId);
								  	
		                            if(data.partnerStatus ==='Active' ){
		                            
		                            	$('td', row).eq(11).css('color', 'Green');
		                            }
		                            else {
		                            	$('td', row).eq(11).css('color', 'Red');
		                            }     
		                          }
						});

  }else if(partnerType == 'logo'){
 
  table2= $('#table-1').DataTable( {
						      "iDisplayLength" : 10,
						      "ordering": false,
						      "bPaginate": true,
						      "bProcessing": true,
						       "searching": false,
						       "scrollX": true, 
						      "serverSide": true,
						      "autoWidth": true,
						  "ajax":"${contextPath}/admin/partnerServerPagination?partnerType="+partnerType,
						  "type": 'GET',
						  "columns": [
							  
							     {"data":"prtNo"},
							    { "data": "partnerId"},
						        { "data": "activityId"},
						        { "data": "partnerName" }, 
						        { "data": "partnerImageUrl" },
						        { "data": "partnerIconUrl" },
						        { "data": "partnerLandingUrl" },
						        {"data":"logoPartnerSearchUrl"},
						        { "data": "logoPrtDisplayOrder" },
						        { "data": "logoPartnerTilePosition" },
						        { "data": "activationDate" },
						        { "data": "deactivationDate" },
						        { "data": "partnerType" },
						        {data: "ratePerMile",
						            "searchable": false,
						            "orderable":false,
						            "render": function (data, type, row) {
						            if (partnerType === 'logo') {
						                return row.ratePerMile;}
						                else {
						        return '-';
						    }
						            }
						     
						        },
						        { "data": "partnerStatus" },
						     {
					                "data": "null",
					                 className: "center",
					                defaultContent: '<a class="view_details_" data-target="#myModal2"><img class="edit_wraper" src="${contextPath}/static/img/View_details_icon1.png"></a> '
					                +'<a class="editpartner_" data-target="#myModal1"><img src="${contextPath}/static/img/edit_icon.png"></img></a>'
					                +'<a class="delete btnDelete" data-target="#myModala" id="deletePartner" ><img src="${contextPath}/static/img/delete_icon1.png" class="fa fa-trash fa-lg"></img></a>'
					            },  
						        { "data": "aggregateId" },
						        {"data":"referenceTokenName"},
						  
						    ]  ,
						  "columnDefs": [
							   {
					                "targets": [ 0,16,17],
					                "visible": false,
					                "searchable": false
					            }, 
					          
							  { "defaultContent": "-", "targets": "_all" }
							  ],
							  "createdRow": function( row, data, dataIndex){
								  $(row).attr('id', data.prtNo);
		                            if(data.partnerStatus ==='Active' ){
		                            
		                            	$('td', row).eq(13).css('color', 'Green');
		                            }
		                            else {
		                            	$('td', row).eq(13).css('color', 'Red');
		                            }     }
						});
  }
    else{
	  
	  table2= $('#table-1').DataTable( {
							      "iDisplayLength" : 10,
							      "ordering": false,
							      "bPaginate": true,
							      "bProcessing": true,
							       "searching": false,
							       "scrollX": true, 
							       "scrollY": true, 
							      "serverSide": true,
							      "autoWidth": true,
							  "ajax":"${contextPath}/admin/partnerServerPagination?partnerType="+partnerType,
							  "type": 'GET',
							  "columns": [
								  
								     {"data":"prtNo"},
								    { "data": "partnerId"},
							        { "data": "activityId"},
							        { "data": "partnerName" }, 
							        { "data": "partnerImageUrl" },
							        { "data": "partnerIconUrl" },
							        { "data": "partnerLandingUrl" },
							        {"data":"logoPartnerSearchUrl"},
							        { "data": "logoPrtDisplayOrder" },
							        { "data": "logoPartnerTilePosition" },
							        { "data": "activationDate" },
							        { "data": "deactivationDate" },
							        { "data": "partnerType" },
							         
							        { "data": "partnerStatus" },
							    {
						                "data": "null",
						                 className: "center",
						                defaultContent: '<a class="view_details_" data-target="#myModal2"><img class="edit_wraper" src="${contextPath}/static/img/View_details_icon1.png"></a> '
						                +'<a class="editpartner_" data-target="#myModal1"><img src="${contextPath}/static/img/edit_icon.png"></img></a>'
						                +'<a class="delete btnDelete" data-target="#myModala" id="deletePartner" ><img src="${contextPath}/static/img/delete_icon1.png" class="fa fa-trash fa-lg"></img></a>'
						            },
							        { "data": "aggregateId" },
						            {"data":"referenceTokenName"},
							  
							    ]  ,
							  "columnDefs": [
								   {
						                "targets": [ 0,15,16],
						                "visible": false,
						                "searchable": false
						            }, 
						          
								  { "defaultContent": "-", "targets": "_all" }
								  ],
								  "createdRow": function( row, data, dataIndex){
			                            if(data.partnerStatus ==='Active' ){
			                            
			                            	$('td', row).eq(12).css('color', 'Green');
			                            }
			                            else {
			                            	$('td', row).eq(12).css('color', 'Red');
			                            }     }
							});
	  }
 
 });

 
 
 $('#createPartnerSave').click(function () {
        var allowLandingUrl=$('#allowLandingUrl').val();
	    var partType=    $("#partnerTypeMapping option:selected").val();
		var partNumber = $("#partName option:selected").val();
		var createMappingPartnerName = $('#createMappingPartnerName').val();
		var createpartcatstatus = 1;
		//statusforcat
		var json={};
		json["partType"]=partType;
		json["partNumber"]=partNumber;
		json["createMappingPartnerName"]=createMappingPartnerName;
		json["createpartcatstatus"] = createpartcatstatus;
		json["allowLandingUrl"] = allowLandingUrl;
		var formData = new FormData();
		formData.append('json',JSON.stringify(json));
		
		 if($("#createPartnerMappingForm").validate().element('#partnerTypeMapping') &&
				 $("#createPartnerMappingForm").validate().element('#partName') &&
				 $("#createPartnerMappingForm").validate().element('#createMappingPartnerName')
				) {
		$.ajax({      
			  url: "${contextPath}/admin/createPartnerMapping",
			  data: formData,
			  dataType: 'text',
			  processData: false,
			  contentType: false,
			  type: 'POST',
			  success: function (response) {
			  window.location.href="${contextPath}/admin/partner?patCreateMessage="+response;
			  },
			  error: function (response) {
				  window.location.href="${contextPath}/admin/partner?patCreateMessage=failure";
	      	 }
			 });
		}
	});
	$("#partnerTypeMapping").on( "selectmenuchange", function(event, type, mapCat){
		var obj=null;
		var prtType = $("#partnerTypeMapping").val();
		$.get("${contextPath}/admin/getCategoryMappingPartner?prtType="+prtType, function(data){
			var obj = jQuery.parseJSON(data);
			$('#partName').empty();
			$('#partName').append("<option value=''>-Select-</option>");
			
			for(var i = 0; i < obj.length; i++){
				var value = obj[i];
				var newOption = $("<option value="+value.prtNo+">"+value.partnerName+"</option>");
				$('#partName').append(newOption);	       
			}
			$( "#partnerTypeMapping" ).selectmenu( "refresh" );
			
			 if(type == "refresh"){
				$("#partnerTypeMapping").selectmenu("refresh");
				$('#partName').val(mapCat);
			}else{
				$("#partName").selectmenu( "refresh" );
				$("#createMappingPartnerName").val('');
			} 
		});
		 
	});
	
	$('#cancelCreatePartCat').click(function () {
		$("#partnerTypeMapping").trigger("selectmenuchange", "refresh");
		$("#partnerTypeMapping").selectmenu("refresh");
		$("#partnerTypeMapping").val('');
		$("#partName").val('');

		 $("#partName").trigger("selectmenuchange", "refresh");
		$("#partName").selectmenu("refresh"); 
		
		$("#createMappingPartnerName").val('');
	});
	
	
	$('span.editPartnerMappingBean').on('click', function () {
		 
	 	var id = $(this).closest('tr').data('id');
		var PartnerType = $(this).closest('tr').data('partnertype');
	 	var PartnerName = $(this).closest('tr').data('prtname');
	 	var partnerMappingName= $(this).closest('tr').data('partnermappingname');
	 	var Prtno =  $(this).closest('tr').data('prtno');
	 	var allowLandingUrl=$(this).closest('tr').data('allowlandingurl');
	 	
	 	
	 	if(PartnerType=="host"){
			$("#edit_PartnerTypeMapping").val('host').selectmenu("refresh");
		}
		if(PartnerType=="default"){
			$("#edit_PartnerTypeMapping").val('default').selectmenu("refresh");
		}
		if(PartnerType=="logo"){
			$("#edit_PartnerTypeMapping").val('logo').selectmenu("refresh");
		}
		if(PartnerType=="offline"){
			$("#edit_PartnerTypeMapping").val('offline').selectmenu("refresh");
		}
		if(PartnerType=="talk"){
			$("#edit_PartnerTypeMapping").val('talk').selectmenu("refresh");
		}
		if(PartnerType=="read"){
			$("#edit_PartnerTypeMapping").val('read').selectmenu("refresh");
		}
		if(allowLandingUrl == "0"){
			$("#edit_allowLandingUrl").prop("checked", false);
		}
		else{
			$("#edit_allowLandingUrl").prop("checked", true);
		}
	 	$('#id').val(id);
		$("#edit_PartnerTypeMapping").trigger("selectmenuchange", ["refresh", Prtno]);
		$('#edit_partnermappingname').val(partnerMappingName).selectmenu("refresh");
		$("#edit_partnername1").val(Prtno).selectmenu("refresh");
		
		$('#edit_partnerhiddenmappingname').val(partnerMappingName);
		$("#edit_extPartNoHidden").val(Prtno);
		
		$('#editPartnerMapping').data('id',id).modal('show'); 
	});
	
	$("#edit_PartnerTypeMapping").on( "selectmenuchange", function(event, type, mapCat){
		var obj=null;
		var prtType = $("#edit_PartnerTypeMapping").val();
		$.get("${contextPath}/admin/getCategoryMappingPartner?prtType="+prtType, function(data){
			var obj = jQuery.parseJSON(data);
			$('#edit_partnername1').empty();
			$('#edit_partnername1').append("<option value=''>-Select-</option>");
			for(var i = 0; i < obj.length; i++){
				var value = obj[i];
				var newOption = $("<option value="+value.prtNo+">"+value.partnerName+"</option>");
				$('#edit_partnername1').append(newOption);	       
			}
			  if(type == "refresh"){
				$('#edit_partnername1').val(mapCat);
			}
			  $("#edit_partnername1").selectmenu("refresh");
		});
		 
	});
	
	
	$('#updatePartnerMapping').click(function () {
		var id = $('#editPartnerMapping').data('id');
		var json={};
		json["id"]=id;
		json["partNumber"]= $("#edit_partnername1 option:selected").val();
		json["createMappingPartnerName"]=$('#edit_partnermappingname').val();
		json["allowlandingurl"]=$('#edit_allowLandingUrl').val();
		var formData = new FormData();
		formData.append('json',JSON.stringify(json));
		 if($("#editPartnerMappingBean").validate().element('#edit_PartnerTypeMapping') &&
				 $("#editPartnerMappingBean").validate().element('#edit_partnername1') &&
				 $("#editPartnerMappingBean").validate().element('#edit_partnermappingname')){
		$.ajax({
			  url: "${contextPath}/admin/updatePartnerMapping",
			  data: formData,
			  dataType: 'text',
			  processData: false,
			  contentType: false,
			  type: 'POST',
			  success: function (response) {
				  $('#editPartnerMapping').modal('hide');
				  window.location.href="${contextPath}/admin/partner?patUpdateMessage="+response;
			  },
			  error: function (response) {
				  $('#editPartnerMapping').modal('hide');
				  window.location.href="${contextPath}/admin/partner?patUpdateMessage=failure";
	     	  }
		});
		}
	});
	
/* 	Delete */
 $('span.deleteMapping').on('click',function() {
						var id = $(this).closest('tr').data('id');
						 var createMappingPartnerName = $(this).closest('tr').data('partnermappingname'); 
						$('#deleteMappingPartner1').text(createMappingPartnerName+ " "
												+ " Partner Mapping will be deleted. Do you want to continue?");
						$('#myModal').data('id', id).modal('show');
					});
	
	
 $('#btnDelete1').click(function() {
				var id = $('#myModal').data('id');
				$.get(
					   "${contextPath}/admin/deletePartnerMapping?id="+ id,
								function(status) {
									if (status == "success") {
										$('[data-id=' + id + ']')
												.remove();
										$('#myModal').modal('hide');
										window.location.href = "${contextPath}/admin/partner?prtdeletePrtMsg="+ status;
									}
									if (status == "failure") {
										$('[data-id=' + id + ']').remove();
										$('#myModal').modal('hide');
										window.location.href = "${contextPath}/admin/partner?prtdeletePrtMsg="
												+ status;
									}
								});
			});
 
 $('#createMappingPartnerName,#partName').on('selectmenuchange', function() {
		var createMappingPartnerName = $('#createMappingPartnerName').val();
		var partNumber = $("#partName option:selected").val();
		//var prtNo = $('#partName').val();
		partnerMappingNameExist = false;
			$.get("${contextPath}/admin/checkCreateMappingDuplicate?createMappingPartnerName=" + createMappingPartnerName+"&partNumber="+partNumber,
					function(data, status) {
				if (status == "success" && data != '') {
					partnerMappingNameExist = true;
				} else {
					partnerMappingNameExist = false;
				}
				$("#createPartnerMappingForm").validate().element('#createMappingPartnerName');
				$("#createPartnerMappingForm").validate().element('#partName');
			});
		
	});
 
 $('#edit_partnermappingname,#edit_partnername1').on('selectmenuchange', function() {
		var editPartnerName = $('#edit_partnermappingname').val();
		var editPartNumber = $("#edit_partnername1 option:selected").val();
		var editPartnerNameHidden = $('#edit_partnerhiddenmappingname').val();
		var editPartNumberHidden = $("#edit_extPartNoHidden").val();
		partnerMappingNameExist = false;				
		if (editPartnerName == editPartnerNameHidden && editPartNumber == editPartNumberHidden) {
			partnerMappingNameExist=false;
			$("#editPartnerMappingBean").validate().element('#edit_partnermappingname');
			$("#editPartnerMappingBean").validate().element('#edit_partnername1');
		}
		else{
			$.get("${contextPath}/admin/checkCreateMappingDuplicate?createMappingPartnerName=" +editPartnerName+"&partNumber="+editPartNumber,
					function(data, status) {
				if (status == "success" && data != '') {
					partnerMappingNameExist = true;
				} else {
					partnerMappingNameExist = false;
				}
				$("#editPartnerMappingBean").validate().element('#edit_partnermappingname');
				$("#editPartnerMappingBean").validate().element('#edit_partnername1');
			});
		}
	}); 
 
 $.validator.addMethod('duplicateMappingPartnerName', function(value, element) {
		if (partnerMappingNameExist) {
			return false;
		} else {
			return true;
		}
	});
 
 $("#allowLandingUrl").on('change', function() {
	    if($(this).is(":checked")) {
	    	$('#allowLandingUrl').val(1);
	    }
	    else{
	    	$('#allowLandingUrl').val(0);
	    	
	    }
	});
 
 $("#edit_allowLandingUrl").on('change', function() {
	    if($(this).is(":checked")) {
	    	$('#edit_allowLandingUrl').val(1);
	    }
	    else{
	    	$('#edit_allowLandingUrl').val(0);
	    }
	});
 
 $('#cancelbtnDAmzon').click(function() {
		$.get("${contextPath}/admin/addPartnerMappingCancel", function(data, status) {
			if (status == "success") {
				window.location.replace("${contextPath}/" + data);
			}
		});
	});
</script>
