<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<section>
	<div id="breadcrumb">
		<ul>
			<!--- breadcrumb--->
			<li>Home</li>
			<li>Configuration</li>
			<li><strong>Seller Text</strong></li>
		</ul>
	</div>
	<c:if test="${fn:length(message) gt 0}">
	             <!--  <script>changeURL();</script> -->
	                <div class="alertMessage" id="divChanged" style="color:green;font-weight: bold;"  ><c:out value="${message}"></c:out></div> </c:if>
	  			<c:if test="${fn:length(errormessage) gt 0}">
	  			   <script>changeURL();</script>
	       		<div class="alertMessage" style="color:red;font-weight: bold;"  ><c:out value="${errormessage}"></c:out></div> </c:if>
	<h5>Seller Text</h5>
		<div class="tab-content">
		<div id="section" class="tab-pane fade in active" >
								<form id="sellerTextForm" name="sellerTextForm" method="POST" action="sellerText">
									<div class="row pad_top">
										<div class="col-md-4">
											<!-- <label>Seller Text</label><span class="mandatory_star">*</span> -->
											<label>Seller Text</label><span class="mandatory_star">*</span><span style="padding-left: 10px;"><img class="seo_edit" src="${contextPath}/static/img/edit_icon_new.png"></span>
											<input id="sellerText"  name="sellerText"  type="text" class="form-control" value="${sellerText}"  readonly="true" />	
										</div>
									</div>
									<br/>
									<p class="col-md-4" id="common-error" style="color:#F43835"></p>
									<div class="row btn_save pull-right">
										<a  href="${contextPath}/admin/sellerText"><input class="bg_g_button" type="button" value="Cancel" id="cancelbtnD" /></a>
										<button type="button" class="bg_g_button" id="saveButton"  onClick="onsubmit()" >Save</button>
									</div>
								</form>
							</div>
						</div>	
</section>

<script>
var valid=true;
$('#sellerText').on('input',function () {
	
	var sellerText = $('#sellerText').val();
	//var subString= sellerText.replace(sellerText.substring(sellerText.indexOf('{'), sellerText.indexOf('}')+1),''); 
	if(sellerText.length <17){
		valid=true;
		$("#common-error").hide();
	}else{
		valid=false;
		$("#common-error").show();
		$("#common-error").text("Character limit excluding {x} is 13 ");
	}
});
$('#saveButton').click(function(){
	if(valid){
		$(this).closest('form').submit();
	}
});


$('#cancelbtnD').click(function () {
	 onClickCloseBtn();
	});
$(".seo_edit").on('click', function(){
	$(this).parent().next('.form-control').removeAttr("readonly");
});
	
function onClickCloseBtn (){
	$.get("${contextPath}/admin/sellerText",function(data,status) {		
		window.location.href="${contextPath}/admin/sellerText";
	});
 }	
 
$(document).ready(function () {

	$("#sellerTextForm").validate({

	    ignore:[],	
		rules: {
			sellerText:{
				required:true,
				noSpace:true
			}	
	 },
	 
	 messages: {
		 sellerText:{
				required:"Seller Text field should not be empty",
				noSpace : "Please enter a valid seller text"
		 }		
	 },
	 errorElement: "div",
	 errorPlacement: function(error, element) {
	         error.insertAfter(element);
	 },
	 submitHandler: function (form) {
		form.submit();
	},
	});
	});

$.validator.addMethod("noSpace", function(value, element) { 
     return value == '' || value.trim().length != 0;  
   });
    
   
   
</script>