<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class='container'>
	<section class="affliates_management">
		<div id ="breadcrumb">
			<ul>
				<li>Home</li>
				<li>Content Management</li>
				<li>Popup Content</li>
			</ul>
		</div>  <!---breadcrumb--->
		
		<div class="section_heading">
			<c:if test="${fn:length(message) gt 0}"><div class="alertMessage" style="color:green;font-weight: bold;"  ><c:out value="${message}"></c:out></div> </c:if>
			<c:if test="${fn:length(errorMessage) gt 0}"><div class="alertMessage" style="color:red;font-weight: bold;"><c:out value="${errorMessage}"></c:out></div> </c:if>
			<h5>Partner Popup Content</h5> 
		</div><!-- TITLE -->
		
		<form:form autocomplete="off" name="partnerPopupContent" id="partnerPopupContent" commandName="partnerPopupContent" >
			<div class="row pad_top">
				<div class="col-md-4">
					<label for="prtNo">Select Partner</label><span class="mandatory_star">*</span>
					<form:select path="prtNo" name="prtNo" id="prtNo" class="select_option">
						<form:option value="">--Select Partner--</form:option>
						<c:forEach items="${prtList}" var="prt">
							<c:choose>								
								<c:when test="${selectedPartner eq prt.prtNo}">
									<li><form:option selected="true" value="${prt.prtNo}" data-point1="${prt.point1}" data-point2="${prt.point2}" data-point3="${prt.point3}" data-point4="${prt.point4}" data-point5="${prt.point5}">${prt.partnerName}</form:option></li>
								 
								</c:when>
								<c:otherwise>
									<li><form:option value="${prt.prtNo}" data-point1="${prt.point1}" data-point2="${prt.point2}" data-point3="${prt.point3}" data-point5="${prt.point5}" data-point4="${prt.point4}" >${prt.partnerName}</form:option></li>
								</c:otherwise>
							</c:choose>
							
						</c:forEach>
					</form:select>
				</div>
			</div>	<!-- row -->
			
			<div class="row pad_top">
				<div class="col-md-6">
				<div class='text_editor'>
					<label>You Must Know</label><span class="mandatory_star">*</span><span style="padding-left: 10px;"></span>
					<%-- <form:input path="point1" name="point1" id="point1" class="form-control" readonly="true" maxlength="2001" /> --%>
				    <form:textarea id="point1" name="point1" path="point1" cols="40" class="form-control text" readonly="true" maxlength="2001"/>
				</div>
				</div>
			</div>	<!-- row -->
			
			<div class="row pad_top">
				<div class="col-md-6">
					<label>Point 1</label><span class="mandatory_star">*</span><span style="padding-left: 10px;"></span>
					<form:textarea path="point2" name="point2" id="point2" class="form-control" readonly="true" maxlength="2001" />
					<%-- <form:textarea id="point2" name="point2" path="point2" cols="40" class="form-control" readonly="true" maxlength="2001" /> --%>
				</div>
			</div>	<!-- row -->
			
			<div class="row pad_top">
				<div class="col-md-6">
					<label>Point 2</label><span style="padding-left: 10px;"></span>
					<%-- <form:textarea path="point3" name="point3" id="point3" class="form-control" readonly="true" maxlength="2001"/> --%>
					<form:textarea id="point3" name="point3" path="point3" cols="40" class="form-control" readonly="true" maxlength="2001" />
				</div>
			</div>	<!-- row -->
			
			<div class="row pad_top">
				<div class="col-md-6">
					<label>Point 3</label><span style="padding-left: 10px;"></span>
					<%-- <form:textarea path="point4" name="point4" id="point4" class="form-control" readonly="true" maxlength="2001"/> --%>
					<form:textarea id="point4" name="point4" path="point4" cols="40" class="form-control"  readonly="true" maxlength="2001" />
				</div>
			</div>	<!-- row -->
			
			<div class="row pad_top">
				<div class="col-md-6">
					<label>Point 4</label><span style="padding-left: 10px;"></span>
					<%-- <form:textarea path="point5" name="point5" id="point5" class="form-control" readonly="true" maxlength="2001"/> --%>
					<form:textarea id="point5" name="point5" path="point5" cols="40" class="form-control" readonly="true" maxlength="2001" />
				</div>
			</div>	<!-- row -->
			
			<div class="row btn_save pull-right pad_bottom">
				<input class="bg_g_button" type="button" id="cancelbtnD" value="Cancel" />
				<button  type="submit" class="bg_g_button" id="saveTiny" >Save</button>
			</div>
		</form:form>
	</section>
</div>





<!-- ********************JS Start******************** -->
<script src='https://cdn.tinymce.com/4/tinymce.min.js'></script>
<script type="text/javascript">
var point1, point2, point3, point4, point5;


$(document).ready(function () {
	$('#prtNo').val("");
	$('#prtNo').selectmenu("refresh");
	
	 tinymce.init({
		selector: 'textarea#point1',
		height: '100px',
		theme: 'modern',
	
		plugins: [
			'advlist autolink lists link image charmap print preview hr anchor pagebreak',
			'searchreplace wordcount visualblocks visualchars code fullscreen',
			'insertdatetime media nonbreaking save table contextmenu directionality',
			'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
		],
		toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image forecolor backcolor | fontsizeselect',
		image_advtab: true,
		templates: [
			{ title: 'Test template 1', content: 'Test 1' },
			{ title: 'Test template 2', content: 'Test 2' }
		],
		content_css: [
			'//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
			'//www.tinymce.com/css/codepen.min.css'
		],
		force_br_newlines : true,
		force_p_newlines : false,
		forced_root_block : '',
		fontsize_formats: "8px 10px 12px 14px 18px 24px 36px",
		setup : function(ed){
			ed.on("NodeChange", function(e){
				jQuery("textarea[id='point1']").val(ed.getContent());
				
			});
			
		}
	}); 
	
	
	 tinymce.init({
		selector: 'textarea#point2',
		height: '100px',
		theme: 'modern',
		
		plugins: [
			'advlist autolink lists link image charmap print preview hr anchor pagebreak',
			'searchreplace wordcount visualblocks visualchars code fullscreen',
			'insertdatetime media nonbreaking save table contextmenu directionality',
			'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
		],
		toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image forecolor backcolor | fontsizeselect',
		image_advtab: true,
		templates: [
			{ title: 'Test template 1', content: 'Test 1' },
			{ title: 'Test template 2', content: 'Test 2' }
		],
		content_css: [
			'//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
			'//www.tinymce.com/css/codepen.min.css'
		],
		force_br_newlines : true,
		force_p_newlines : false,
		forced_root_block : '',
		fontsize_formats: "8px 10px 12px 14px 18px 24px 36px",
		setup : function(ed){
			ed.on("NodeChange", function(e){
				jQuery("textarea[id='point2']").val(ed.getContent());
			});
		}
	});
	
	
	tinymce.init({
		selector: 'textarea#point3',
		height: '100px',
		theme: 'modern',
		
		plugins: [
			'advlist autolink lists link image charmap print preview hr anchor pagebreak',
			'searchreplace wordcount visualblocks visualchars code fullscreen',
			'insertdatetime media nonbreaking save table contextmenu directionality',
			'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
		],
		toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image forecolor backcolor | fontsizeselect',
		image_advtab: true,
		templates: [
			{ title: 'Test template 1', content: 'Test 1' },
			{ title: 'Test template 2', content: 'Test 2' }
		],
		content_css: [
			'//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
			'//www.tinymce.com/css/codepen.min.css'
		],
		force_br_newlines : true,
		force_p_newlines : false,
		forced_root_block : '',
		fontsize_formats: "8px 10px 12px 14px 18px 24px 36px",
		setup : function(ed){
			ed.on("NodeChange", function(e){
				jQuery("textarea[id='point3']").val(ed.getContent());
			});
		}
	});

	
	tinymce.init({
		selector: 'textarea#point4',
		height: '100px',
		theme: 'modern',
		
		plugins: [
			'advlist autolink lists link image charmap print preview hr anchor pagebreak',
			'searchreplace wordcount visualblocks visualchars code fullscreen',
			'insertdatetime media nonbreaking save table contextmenu directionality',
			'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
		],
		toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image forecolor backcolor | fontsizeselect',
		image_advtab: true,
		templates: [
			{ title: 'Test template 1', content: 'Test 1' },
			{ title: 'Test template 2', content: 'Test 2' }
		],
		content_css: [
			'//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
			'//www.tinymce.com/css/codepen.min.css'
		],
		force_br_newlines : true,
		force_p_newlines : false,
		forced_root_block : '',
		fontsize_formats: "8px 10px 12px 14px 18px 24px 36px",
		setup : function(ed){
			ed.on("NodeChange", function(e){
				jQuery("textarea[id='point4']").val(ed.getContent());
				
			});
		}
	});
	
	tinymce.init({
		selector: 'textarea#point5',
		height: '100px',
		theme: 'modern',
		
		plugins: [
			'advlist autolink lists link image charmap print preview hr anchor pagebreak',
			'searchreplace wordcount visualblocks visualchars code fullscreen',
			'insertdatetime media nonbreaking save table contextmenu directionality',
			'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
		],
		toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image forecolor backcolor | fontsizeselect',
		image_advtab: true,
		templates: [
			{ title: 'Test template 1', content: 'Test 1' },
			{ title: 'Test template 2', content: 'Test 2' }
		],
		content_css: [
			'//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
			'//www.tinymce.com/css/codepen.min.css'
		],
		force_br_newlines : true,
		force_p_newlines : false,
		forced_root_block : '',
		fontsize_formats: "8px 10px 12px 14px 18px 24px 36px",
		setup : function(ed){
			ed.on("NodeChange", function(e){
				jQuery("textarea[id='point5']").val(ed.getContent());
			});
		}
	}); 
	
	
	$("#partnerPopupContent").validate({
		onkeyup: true,
		ignore:[],
		rules: {	
			prtNo:{
				required: true
			},
			point1:{
				required: true,
				rangelength:[1,2000]
			},
			
			point2:{
				required: true,
				rangelength:[1,2000]
			},
			point3:{
				rangelength:[1,2000]
			},
			point4:{
				rangelength:[1,2000]
			},
			point5:{
				rangelength:[1,2000]
			}
		},
		messages: {
			prtNo:{
				required: "Please select a Partner."
			},
			point1:{
				required: "You must know cannot be left blank",
				rangelength:"Maximum 2000 characters permitted",
			},
			
			point2:{
				required: "Point 1 cannot be left blank",
				rangelength:"Maximum 2000 characters permitted",
			},
			point3:{
				rangelength:"Maximum 2000 characters permitted",
			},
			point4:{
				rangelength:"Maximum 2000 characters permitted",
			},
			point5:{
				rangelength:"Maximum 2000 characters permitted",
			}
		},
		errorElement: "div",
		errorPlacement: function(error, element) {
			error.insertAfter(element);
		},
		submitHandler: function (form) {
			form.submit();
		}
	});
	
	
	
	
	
	
});


$("#cancelbtnD").on('click', function(){
	
	
	
	$('#point1').val("");
    $('#point2').val("");
	$('#point3').val("");
	$('#point4').val("");
	$('#point5').val("");
	
	//tinymce.activeEditor.setContent('point1');
	tinymce.get('point1').setContent("");
	tinymce.get('point2').setContent("");
	tinymce.get('point3').setContent("");
	tinymce.get('point4').setContent("");
	tinymce.get('point5').setContent("");
	tinymce.triggerSave();
	$('#prtNo').val("");
	$('#prtNo').selectmenu("refresh");
	$('#partnerPopupContent').validate().resetForm();
});

/* $(".point_edit").on('click', function(){
	
	tinyMCE.get('point1').getBody().setAttribute('contenteditable', true);
	tinyMCE.get('point2').getBody().setAttribute('contenteditable', true);
	tinyMCE.get('point3').getBody().setAttribute('contenteditable', true);
	tinyMCE.get('point4').getBody().setAttribute('contenteditable', true);
	tinyMCE.get('point5').getBody().setAttribute('contenteditable', true);

}); */

$('#prtNo').on('selectmenuchange', function() {
	
	$('#partnerPopupContent').validate().resetForm();
	setPopupPoints();
	$("#partnerPopupContent").validate().element("#prtNo");

	
});

/* $("#point1").on('keyup', function(){
	alert("just");
	$("#partnerPopupContent").validate().element("#point1");
});

$("#point2").on('blur', function(){
	$("#partnerPopupContent").validate().element("#point2");
});

$("#point3").on('blur', function(){
	$("#partnerPopupContent").validate().element("#point3");
});

$("#point4").on('blur', function(){
	$("#partnerPopupContent").validate().element("#point4");
});
$("#point5").on('blur', function(){
	$("#partnerPopupContent").validate().element("#point4");
});
$("#point1").on('change', function(){
	$("#partnerPopupContent").validate().element("#point1");
});
 */
function setPopupPoints(){
	if($('#prtNo').val()>0){
	//point1 = $('#prtNo :selected').data('point1');
	point1 = ($('#prtNo :selected').data('point1')!= undefined) ? $('#prtNo :selected').data('point1') : "";
	point2 = ($('#prtNo :selected').data('point2')!= undefined) ? $('#prtNo :selected').data('point2') : "";
	point3 = ($('#prtNo :selected').data('point3')!= undefined) ? $('#prtNo :selected').data('point3') : "";
	point4 = ($('#prtNo :selected').data('point4')!= undefined) ? $('#prtNo :selected').data('point4') : "";
	point5 = ($('#prtNo :selected').data('point5')!= undefined) ? $('#prtNo :selected').data('point5') : "";
	
	point1 = decodeURIComponent(point1).split('+').join(" ");
	point2 = decodeURIComponent(point2).split('+').join(" ");
	point3 = decodeURIComponent(point3).split('+').join(" ");
	point4 = decodeURIComponent(point4).split('+').join(" ");
	point5 = decodeURIComponent(point5).split('+').join(" ");
	
	$('#point1').val(point1);
    $('#point2').val(point2);
	$('#point3').val(point3);
	$('#point4').val(point4);
	$('#point5').val(point5);
	
	//tinymce.activeEditor.setContent('point1');
	tinymce.get('point1').setContent(point1);
	tinymce.get('point2').setContent(point2);
	tinymce.get('point3').setContent(point3);
	tinymce.get('point4').setContent(point4);
	tinymce.get('point5').setContent(point5);
	tinymce.triggerSave();	
	}	
	else{
		
		$('#point1').val("");
	    $('#point2').val("");
		$('#point3').val("");
		$('#point4').val("");
		$('#point5').val("");
		
		tinymce.get('point1').setContent("");
		tinymce.get('point2').setContent("");
		tinymce.get('point3').setContent("");
		tinymce.get('point4').setContent("");
		tinymce.get('point5').setContent("");
		tinymce.triggerSave();	
	}
	
}
 
</script>