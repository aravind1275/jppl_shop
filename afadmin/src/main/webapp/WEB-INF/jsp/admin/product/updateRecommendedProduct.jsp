<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
	<div class='container'>           
            <section class="insurance_management">
                <div id ="breadcrumb">
                    <ul>
                        <li>Home</li>
						<li>Content Management</li>
						<li>Recommended Products</li>
                    </ul>
                </div>  <!---breadcrumb--->
				<div class="section_heading">
					<h5>Recommended Products</h5>
				</div><!--- section heading ---->
				<div class="management"><!---User Management--->
                    <ul class="nav nav-tabs">
                        <li><a  href="${contextPath}/admin/recommendedProducts">Recommended Products</a></li>
                        <li class="active"><a data-toggle="tab" href="#sectionB">Update Recommendation</a></li>
                    </ul>
                    <form:form autocomplete="off" commandName="recommendedProductBean" enctype="multipart/form-data">
                        <div id="sectionB" class="tab-pane fade submenu  in active" ><!-----Define Recommended starts here---->
                            <div class="row pad_top">
								<%-- <div class="col-md-4">
									<label>Select A Partner</label><span class="mandatory_star">*</span>
									<fieldset class="infield">
										<form:select path="prtNo" id="prtNo" class="select_option">             		
		             					<form:option value="">-Select Partner-</form:option>
		             	 					 <c:forEach items="${partnerList}" var="partner">
			             					      <form:option value="${partner.prtNo}" label="${partner.partnerName}" />
			             					</c:forEach> 
			             				</form:select>
									</fieldset>
									<input type="hidden" name="prtNo_" id="prtNo_"/>	
								</div>
								<div class="col-md-4"><span class="mandatory_star">*</span>
									<label>Partner Category</label>
									<fieldset class="infield">
										<form:select path="prtCatNo" id="prtCatNo" class="select_option">
		             					<form:option value="">-Partner Category-</form:option>
			             				<c:forEach items="${partnerCatList}" var="partnerCategory">
			             					 <form:option value="${partnerCategory.prtCatNo}" label="${partnerCategory.partnercategoryName}" />
			             				</c:forEach> 
			             				</form:select>
									</fieldset>	
								</div> --%>
                                <div class="col-md-4">
									<label>Recommended Product Name</label><span class="mandatory_star">*</span>
									<form:input type='text'  id="rpName" path="rpName" placeholder=" What's your Wish?" class="form-control" readonly="true"/>
									<!-- <input type="hidden" name="rpName_" id="rpName_"/> -->
									<form:input type="hidden" path="prtPrdNo" id="prtPrdNo"/>
									<!-- <input type="hidden" name="prtPrdNo_" id="prtPrdNo_"/> -->
								</div>
                            </div><!--- row --->
                            <div class="row pad_top">
								<div class="col-md-4">
									<label>Position</label><!-- <span class="mandatory_star">*</span> -->
									<form:input type='text' name="position" id="position" path="position" class="form-control" maxlength="3"/>
									<input type="hidden" name="positionHidden" id="positionHidden"/>
								</div>
								<div class="col-md-4">
									<label>Product Start Date</label><span class="mandatory_star">*</span>
									<div class="pick_date">
										<form:input  class="form-control startDate" id='datepicker1' path="activationDate" readonly="true"/>
									</div>
								</div><!--- datepicker --->
								<div class="col-md-4">
									<label>Product End Date</label><!-- <span class="mandatory_star">*</span> -->
									<div class="pick_date">
										<form:input  class="form-control endDate" id='datepicker2' path="deactivationDate"/>
									</div>
								</div><!--- datepicker --->
							</div> <!------  row  ------>
							<div class="row pad_top pad_bottom">
								<div class="col-md-4">
									<label>Recommended Product Status</label><span class="mandatory_star">*</span>
									<div class="half-col last">
										<ul class="list-inline">
											<c:forEach items="${rpStatus}" var="status">
												<li>
												<label>
												<form:radiobutton path="rpStatus" class="css-checkbox" id ="${status.key}" value="${status.key}" />
												<label for="${status.key}" class="css-label radGroup2">${status.value}</label>
												</label>
												</li>
											</c:forEach>
										</ul>
									</div>
                                </div><!--- radio button --->
							</div> <!------  row  ------>
                            <div class="row btn_save pull-right">
								<a  href="${contextPath}/admin/recommendedProducts"><button type="button" class="bg_g_button">Cancel</button></a>
								<button type="button" class="bg_g_button" onClick="$(this).closest('form').submit();">Update</button>
							</div><!------ button ------>
                        </div><!----section B---->
                     </form:form>
                </div>	<!---- user managemnt----->
            </section><!-----section---->            
        </div><!-----container---->


<!-- Variables -->
<script type="text/javascript">
var rpNameExist=false;
var rpNameInvalid=false;
var rpPositionExist=false;
</script>

<!-- From Validation -->
<script type="text/javascript">
$(document).ready(function () {
	$("#positionHidden").val($("#position").val());
	
	if(parseInt($("#position").val()) == 0){
		$("#position").val('');
	}
	
	$("#recommendedProductBean").validate({
		ignore:[],
		rules: { 		
			prtNo:{
				required: true,	 			
			},
			prtCatNo:{
				required: true,	 			
			},
			rpName:{
				required: true,
				rangelength:[1,250],
				rpNameInvalid:true,
				duplicaterpName:true,
			},
			position:{
				//required: true,	
				digits:true,
				positionNotZero:true,
				duplicaterpPosition:true,
				positionRange: true
			},	 		
			activationDate:{
				required: true,
				startDate:true
			},
			deactivationDate:{
				//required:true,
				endDate:true
			},
		},
		messages: {
			prtNo:{
				required:"Please select the Partner",	 		
			},
			prtCatNo:{
				required:"Please select the Partner Category",	 		
			},
			rpName:{
				required:"Please enter Recommended Product Name",
				rangelength:"Maximum 100 characters permitted",
				duplicaterpName:"Recommended Product already exists",
				rpNameInvalid:"Invalid Product, Please select from dropdown"
			},
			position:{
				//required: "Please enter Position",
				digits:"Please enter numeric data",
				positionNotZero:"Position cannot be 0",
				duplicaterpPosition:"Recommended Product position already exists",
				positionRange: "Position should be less than 100"
			},	 	
			activationDate:{
				required: "Please select Product Start Date",
				startDate: "* Start Date must be less than or equal to End date"		
			},
			deactivationDate:{
				//required: "Please select Product End Date",
				endDate:"* End Date must be greater than or equal to Start date"		
			},		 		 	
		},
		errorElement: "div",
		errorPlacement: function(error, element) {
			if ($(element).hasClass("form-control startDate")) {
				error.insertAfter($(element).closest("div"));
			}
			else if ($(element).hasClass("form-control endDate")){
				error.insertAfter($(element).closest("div"));
			}
			else {
				error.insertAfter(element);
			}
		},
		submitHandler: function (form) {
			
			if($("#position").val() == ""){
				$("#position").val('0');
			}
			
			form.submit();
		},
	});
		 
	/* for submit the form on key press enter*/
	$(document).keypress(function(event){
		if(event.keyCode == 13){
			$("#recommendedProductBean").submit();
		}
	});
});	
</script>

<!-- Events -->
<script type="text/javascript">
$('#sectionA').addClass('in active');
$('#sectionB').addClass('active');

//-----Default Auto Complete starts
var prtCatNo = $("#prtCatNo option:selected").val(); 
$("#prtCatNo_").val(prtCatNo);
$("#rpName_").val($("#rpName").val());
$("#prtPrdNo_").val($("#prtPrdNo").val());

$( "#rpName" ).autocomplete({
	minLength: 1,
	source: function(request, response) {   	  
		$.ajax({
			dataType: "json",
			type: "GET",
			xhrFields: {
				withCredentials: true
			},
			crossDomain: true,
			cache: true,
			data: {
				keyword: request.term
			}, 
			url: "${contextPath}/admin/getPartnerProduct?prtCatNo="+prtCatNo,
			success: function(data){            	  
				var array= $.map( data, function(key,value) {            	 
					return {                    	  
						label:key,
						value:value    
					}
				});            	  
				response($.ui.autocomplete.filter(array, request.term));
			},
			error: function (data) {
			}
		})
	},
	focus: function( event, ui ) {
		event.preventDefault();
	},
	select: function(event, ui) {    	  
		$("#prtPrdNo").val(ui.item.value); 
		$("#rpName").val(ui.item.label);
		rpNameInvalid = false;
		$("#recommendedProductBean").validate().element('#rpName');
		return false;
	}
});
//-----Default Auto Complete ends

/* $('#rpName').on('blur', function () {
	var prtNo=$("#prtNo").val();
	var prtPrdNo=$("#prtPrdNo").val();
	var prtPrdNo_=$("#prtPrdNo_").val();
	
	rpNameInvalid=false;
	rpNameExist=false;
	
	if(prtPrdNo==''){				
		rpNameInvalid=true;
		$("#recommendedProductBean").validate().element('#rpName');
	}
	else{ 
		if(prtPrdNo!=prtPrdNo_){
			$.get("${contextPath}/admin/checkRecommendedProduct?prtNo="+prtNo+"&prtPrdNo="+prtPrdNo, function(data,status) {
				if(status=="success" && data!=''){
					rpNameExist=true;	
				}
				else{
					rpNameExist=false;
				}
				rpNameInvalid=false;	
				$("#recommendedProductBean").validate().element('#rpName');	
			});
		}
		else{				
			$("#rpName").val($("#rpName_").val());
			rpNameExist=false;
			$("#recommendedProductBean").validate().element('#rpName');
		}
	}
}); */

$('#position').on('keyup', function () {
	var prtPrdPos = parseInt($("#position").val());
	var prtPrdPosHidden = parseInt($("#positionHidden").val());
	rpPositionExist = false;
	
	if(prtPrdPos != prtPrdPosHidden && prtPrdPos != 0){
		$.get("${contextPath}/admin/checkRecommendedProductPosition?prtPrdPos="+prtPrdPos, function(data,status) {
			if(status=="success" && data!=''){
				rpPositionExist=true;	
			}
			else{
				rpPositionExist=false;
			}
			$("#recommendedProductBean").validate().element('#position');
		});
	}
	else{
		$("#recommendedProductBean").validate().element('#position');
	}
});

$("#prtNo").on( "selectmenuchange", function(){
	var obj=null;		
	var prtNo = $("#prtNo option:selected").val();	
	$("#rpName").val('');
	
	$.get("${contextPath}/admin/getPartnerCategories?prtNo="+prtNo, function(data){
		var obj = jQuery.parseJSON(data);
		$('#prtCatNo').empty();
		$('#prtCatNo').append("<option value=''>-Partner Category-</option>"); 
		
		for(var i = 0; i < obj.length; i++){
			var value = obj[i];
			var newOption = $("<option value="+value.prtCatNo+">"+value.partnercategoryName+"</option>");
			$('#prtCatNo').append(newOption);	       
		}
		$( "#prtCatNo" ).selectmenu( "refresh" );
		$("#recommendedProductBean").validate().element('#prtNo');
	});
}); 

$("#prtCatNo").on( "selectmenuchange", function(){	
	var prtCatNo = $("#prtCatNo option:selected").val();
	$("#rpName").val('');
	var prtCatNo_ = $('#prtCatNo_').val();
	var rpName = $('#rpName').val();
	
	$( "#rpName" ).autocomplete({
		minLength: 2,
		source: function(request, response) {    	  
			$.ajax({
				dataType: "json",
				type: "GET",
				xhrFields: {
					withCredentials: true
				},
				crossDomain: true,
				cache: true,
				data: {
					keyword: request.term
				}, 
				url: "${contextPath}/admin/getPartnerProduct?prtCatNo="+prtCatNo,
				success: function(data){            	  
					var array= $.map( data, function(key,value) {
						return {                    	  
							label:key,
							value:value    
						}
					});            	  
					response($.ui.autocomplete.filter(array, request.term));
				},	
				error: function (data) {
				}
			})
		},
		select: function(event, ui) {    	  
			$("#prtPrdNo").val(ui.item.value); 
			$("#rpName").val(ui.item.label);
			rpNameInvalid = false;
			$("#recommendedProductBean").validate().element('#rpName');
			return false;
		}
	});
	$("#recommendedProductBean").validate().element('#prtCatNo');
});

$( "#prtCatNo" ).on( "selectmenuselect", function(){
	var prtNo = $("#prtNo option:selected").val();
	
		if(prtNo==''){
		alert("Please select the partner");
		$("#rpName").val('');
		$( "#prtNo-button" ).focus();
		return false;
	}			 
});

/* $( "#rpName" ).click(function() {
	var prtCatNo = $("#prtCatNo option:selected").val();
	$("#prtPrdNo").val('');
	$("#rpName").val('');
	
	if(prtCatNo==''){
		alert("Please select the partner category");
		$("#rpName").val('');
		$( "#prtCatNo-button" ).focus();
		return false;
	}			 
}) */;

$('#rpName').focusout(function () {
	var prtCatNo = $("#prtCatNo option:selected").val();
	var prtCatNo_ = $('#prtCatNo_').val();
	var rpName = $('#rpName').val();
	
	if ((rpName == '') && (prtCatNo == prtCatNo_)) {
		$("#rpName").val($("#rpName_").val());
		$("#prtPrdNo").val($("#prtPrdNo_").val());
		rpNameInvalid=false;
		$("#recommendedProductBean").validate().element('#rpName');       
	}
	else
		$("#recommendedProductBean").validate().element('#rpName');
});

$( "#datepicker1" ).on('change', function () {
	$("#recommendedProductBean").validate().element('#datepicker1');
	var endDate = getFormattedDate($('#datepicker2').val());
	
	if(endDate != 'Invalid Date'){
		$("#recommendedProductBean").validate().element('#datepicker2');
	}
});  

$( "#datepicker2" ).on('change', function () {
	$("#recommendedProductBean").validate().element('#datepicker2');
	$("#recommendedProductBean").validate().element('#datepicker1');
});  
</script>

<!-- Validation Methods -->
<script type="text/javascript">
$.validator.addMethod('duplicaterpName', function(value, element) {
	return !rpNameExist;
});

$.validator.addMethod('rpNameInvalid', function(value, element) {
	return !rpNameInvalid;
});
	 
$.validator.addMethod('duplicaterpPosition', function(value, element) {
	return !rpPositionExist;
});

$.validator.addMethod('positionNotZero', function(value, element) {
	var isValid = true;
	var rpPosition = parseInt($("#position").val());
	
	if(rpPosition == 0){
		isValid = false;
	}
	
	return isValid; 
});

$.validator.addMethod("endDate", function(value, element) {
	var startDate = getFormattedDate($('#datepicker1').val());
	var endDate = getFormattedDate($('#datepicker2').val());
	
	if(endDate=='Invalid Date')
		return true;
	if (endDate >= startDate)
		return true;
	if (endDate <= startDate)
		return false;
});

$.validator.addMethod("startDate", function(value, element) {
	var startDate = getFormattedDate($('#datepicker1').val());
	var endDate = getFormattedDate($('#datepicker2').val());
	
	if(endDate=='Invalid Date')
		return true;
	if (startDate <= endDate)
		return true;
	if (startDate >= endDate)
		return false;
});

$.validator.addMethod('positionRange', function(value, element) {
	var positionRange = parseInt($("#position").val());
	var isValid = true;
	
	if(positionRange > 100){
		isValid = false;
	}

	return isValid;
});
</script>

<!-- Methods -->
<script type="text/javascript">
function getFormattedDate(date){
	var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
	return new Date(date.replace(pattern,'$3-$2-$1'));
}
</script>


