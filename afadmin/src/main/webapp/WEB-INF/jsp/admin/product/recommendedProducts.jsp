<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<script>
	function changeURL(){
                	history.pushState({}, '',"${contextPath}/admin/recommendedProducts");
        }
</script>
<!-- Modal contet for delete-->
<div id="myModalDelete" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Are You Sure?</h4>
			</div>
			<!--header-->
			<div class="modal-body">
				<p>
					<img src="${contextPath}/static/img/info_icon1.png"></img>
					<span id="deleterpId"></span> 
				</p>
			</div>
			<!---body--->
			<div class="modal-footer">
				<button id="btnD" class="bg_g_button" data-dismiss="modal">Delete</button>
			</div>
		</div>
		<!---modal content--->
	</div>
</div>

<div class='container'>

	<section class="insurance_management">
		<div id="breadcrumb">
			<ul>
				<li>Home</li>
				<li>Content Management</li>
				<li>Recommended Products</li>
			</ul>
		</div>
		<!---breadcrumb--->
		<div class="section_heading">
 		
			<c:if test="${fn:length(message) gt 0}">
			 <script>changeURL();</script>
				<div class="alertMessage" style="color: green; font-weight: bold;">
					<c:out value="${message}"></c:out>
				</div>
			</c:if>
			<c:if test="${fn:length(errormessage) gt 0}">
			 <script>changeURL();</script>
				<div class="alertMessage" style="color: red; font-weight: bold;">
					<c:out value="${errormessage}"></c:out>
				</div>
			</c:if>

			<h5>Recommended Products</h5>
		</div>
		<!--- section heading ---->
		<div class="management">
			<!---User Management--->
			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#sectionA">Recommended Products</a></li>
				<li><a data-toggle="tab" href="#sectionB">Define Recommendation</a></li>
			</ul>

			<div class="tab-content">

				<div id="sectionA" class="tab-pane fade in active">
					<!----section A---->
					<nav class="navbar navbar-default" role="navigation" id="submenu">
						<%-- <form:form autocomplete="off" action="recommendedProductsFilter" commandName="recommendedProductBean_" enctype="multipart/form-data">
							<!-----Recommended Products starts here---->
							<div class="row pad_top">
								<div class="col-md-4">
									<label>Select A Partner</label>
									<fieldset class="infield">
										<form:select name="speed" path="prtNo" id="prtNo_" class="select_option">
											<form:option value="">-Select Partner-</form:option>
											<c:forEach items="${partnerList}" var="partner">
												<form:option value="${partner.prtNo}" label="${partner.partnerName}" />
											</c:forEach>
										</form:select>
									</fieldset>
								</div>
							</div>
							<!--- row --->
						</form:form> --%>
						<c:if test="${empty rpList}">
							<div class="alertMessage" style="padding-left:15px; color:green;font-weight: bold;"  >No Recommended Products Found</div> 
						</c:if>
						<c:if test="${not empty rpList}">
							<table class="partner_info_table table-bordered" cellspacing="0" cellpadding="0" width="98%">
								<thead>
									<tr>
										<th class="name">Sl No.</th>
										<!-- <th>Product Id</th> -->
										<th>Product Name</th>
										<!-- <th>Category Name</th> -->
										<th>Position</th>
										<th>Product Start Date</th>
										<th>Product End Date</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<!----thead--->
								<tbody>
									<c:forEach items="${rpList}" var="rproduct" varStatus="i">
										<tr class="admin" data-id="${rproduct.rpNo}"
											<%-- data-rpartnername="${rproduct.prtName}"
										 --%>	<%-- data-rproductid="${rproduct.rpId}" --%>
											data-rproductname="${rproduct.rpName}"
											data-rproductpcname="${rproduct.pcName}"
											data-rproductstatus="${rproduct.rpStatus}"
											data-rproductstatuscode="${rproduct.statusCode}"
											data-rproductactivationdate="${rproduct.activationDate}"
											data-rproductdeactivationdate="${rproduct.deactivationDate}"
											data-rproductstatusclass="${rproduct.statusClass}">
											<td class="slno_width">${i.count}</td>
											<%-- <td class="td_width">${rproduct.prtName}</td>
											<td class="td_width">${rproduct.rpId}</td>
											 --%>
											 <td class="td_width">${rproduct.rpName}</td>
											<%-- <td class="td_width">${rproduct.pcName}</td> --%>
											
											<c:if test="${rproduct.position eq 0}">
												<td class="td_width"></td>
											</c:if>
											<c:if test="${rproduct.position ne 0}">
												<td class="td_width">${rproduct.position}</td>
											</c:if>
											
											<td class="td_width">${rproduct.activationDate}</td>
											<td class="td_width">${rproduct.deactivationDate}</td>
											<td class="${rproduct.statusClass} green td_width">${rproduct.rpStatus}</td>
											<td class="td_width">
												<a href="${contextPath}/admin/updateRecommendedProduct?rpNo=${rproduct.rpNo}">
													<img src="${contextPath}/static/img/edit_icon.png" class="edit_wraper"></img>
												</a> 
												 <span class="delete btnDelete" id="deleteRecommendedProduct" data-target="#myModalDelete">
													<img src="${contextPath}/static/img/delete_icon1.png" class="fa fa-trash fa-lg"></img>
												</span> 
											</td>
										</tr>
									</c:forEach>
								</tbody>
								<!---tbody--->
							</table>
							<!---table-->
						</c:if>
					</nav>
				</div>
				<!----section A---->

				<div id="sectionB" class="tab-pane fade submenu">
					<!-----Define Recommended starts here---->
					<form:form autocomplete="off" action="recommendedProducts" commandName="recommendedProductBean" enctype="multipart/form-data">
						<div class="row pad_top">
							<div class="col-md-4">
								<label>Select A Partner</label><span class="mandatory_star">*</span>
								<fieldset class="infield">
									<form:select name="speed" path="prtNo" id="prtNo" class="select_option">
										<form:option value="">-Select Partner-</form:option>
											<c:forEach items="${partnerList}" var="partner">
												<form:option value="${partner.prtNo}" label="${partner.partnerName}" />
											</c:forEach>
									</form:select>
								</fieldset>
							</div>

							<div class="col-md-4">
								<label>Partner Category</label> <span class="mandatory_star">*</span> 
								<fieldset class="infield">
									<form:select name="speed" path="prtCatNo" id="prtCatNo"	class="select_option">
										<form:option value="">-Partner Category-</form:option>
									</form:select>
								</fieldset>
							</div>

							<div class="col-md-4">
								<label>Recommended Product Name</label><span
									class="mandatory_star">*</span>
								<input type='text' id="rpName" name="rpName" placeholder=" What's your Wish?" class="form-control" />
								<form:input type="hidden" path="prtPrdNo" id="prtPrdNo" />
							</div>
						</div>
						<!--- row --->

						<div class="row pad_top">
							<div class="col-md-4">
								<label>Position</label><!-- <span class="mandatory_star">*</span> -->
								<form:input type='text' name="position" id="position" path="position"
									class="form-control" maxlength="3"/>
							</div>
							<div class="col-md-4">
								<label>Product Start Date</label><span class="mandatory_star">*</span>
								<div class="pick_date">
									<form:input class="form-control startDate" id='datepicker1' path="activationDate" readonly="true" />
								</div>
							</div>
							<!--- datepicker --->
							<div class="col-md-4">
								<label>Product End Date</label><!-- <span class="mandatory_star">*</span> -->
								<div class="pick_date">
									<form:input class="form-control endDate" id='datepicker2' path="deactivationDate" />
								</div>
							</div>
							<!--- datepicker --->
						</div>
						<!------  row  ------>
						<div class="row pad_top pad_bottom">
							<div class="col-md-4">
								<label>Recommended Product Status</label><span
									class="mandatory_star">*</span>
								<div class="half-col last">
										<ul class="list-inline">
										 <c:forEach items="${rpStatus}" var="status">
											<li>
												<label>
													<form:radiobutton name="radiog_dark" path="statusCode" class="css-checkbox" id="${status.key}" value="${status.key}" /> 
													<label for="${status.key}" class="css-label radGroup2">${status.value}</label>
												</label>
											</li>
										</c:forEach>
									</ul>
								</div>
							</div>
							<!--- radio button --->
						</div>
						<!------  row  ------>
						<div class="row btn_save pull-right">
							<a  href="${contextPath}/admin/recommendedProducts"><input class="bg_g_button" type="button" value="Cancel" /></a>
							<button type="button" class="bg_g_button" onClick="$(this).closest('form').submit();">Save</button>
						</div>
						<!------ button ------>
					</form:form>
				</div>
				<!----section B---->
			</div>
			<!-----tab-content---->

		</div>
		<!---- user managemnt----->
	</section>
	<!-----section---->

</div>
<!-----container---->


<!-- Variables -->
<script type="text/javascript">
var rpNameExist=false;
var rpNameInvalid=false;
var rpPositionExist=false;
</script>

<!-- Validation Rules and Messages -->
<script type="text/javascript">
$(document).ready(function () {

jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "non-empty-string-asc": function (str1, str2) {
    	str1 = Number($.trim(str1));
    	str2 = Number($.trim(str2));
        if(str1 == null || str1 == "")
            return 1;
        if(str1 == null || str2 == "")
            return -1;
        return ((str1 < str2) ? -1 : ((str1 > str2) ? 1 : 0));
    },
    "non-empty-string-desc": function (str1, str2) {
    	str1 = Number($.trim(str1));
    	str2 = Number($.trim(str2));
        if(str1 == null || str1 == "")
            return 1;
        if(str1 == null || str2 == "")
            return -1;
        return ((str1 < str2) ? 1 : ((str1 > str2) ? -1 : 0));
    }
} );

$('.partner_info_table').DataTable( {
     "paging": true,
     "searching": false,
     "lengthChange": false,
     "info":false,
     "iDisplayLength" : 100,
     "order": [[2, "asc" ]],
     columnDefs: [
          {type: 'non-empty-string', targets: 2} // define 'name' column as non-empty-string type
     ],
 });

/*$('.partner_info_table').DataTable( {
        "paging": true,
         "searching": false,
         "lengthChange": false,
         "info":false,
         "order": [[ 2, "asc" ]]
     });*/
	$("#position").val('');
	
	$("#recommendedProductBean").validate({
		ignore:[],
		rules: {
			prtNo:{
				required: true
			},
			prtCatNo:{
				required: true
			},
			rpName:{
				required: true,
				rangelength:[1,250],
				rpNameInvalid:true,
				duplicaterpName:true
			},
			position:{
				//required: true,
				digits:true,
				positionNotZero:true,
				duplicaterpPosition:true,
				positionRange: true
			},
			activationDate:{
				required: true,
				startDate: true		
			},
			deactivationDate:{
				//required: true,
				endDate:true
			}
		},
		messages: {
			prtNo:{
				required:"Please select the Partner"
			},
			prtCatNo:{
				required:"Please select the Partner Category"
			},
			rpName:{
				required:"Please enter Recommended Product Name",
				rangelength:"Maximum 100 characters permitted",
				duplicaterpName:"Recommended Product already exists",
				rpNameInvalid:"Invalid Product, Please select from dropdown"
			},
			position:{
				//required: "Please enter Position",
				digits:"Please enter numeric data",
				positionNotZero:"Position cannot be 0",
				duplicaterpPosition:"Recommended Product position already exists",
				positionRange: "Position should be less than 100"
			},	 	
			activationDate:{
				required: "Please select Product Start Date",
				startDate: "* Start Date must be less than or equal to End date"
			},
			deactivationDate:{
				//required: "Please select Product End Date",
				endDate:"* End Date must be greater than or equal to Start date"
			}	 		 	
		},
		errorElement: "div",
		errorPlacement: function(error, element) {
			if ($(element).hasClass("form-control startDate")) {
				error.insertAfter($(element).closest("div"));
			}
			else if ($(element).hasClass("form-control endDate")){
				error.insertAfter($(element).closest("div"));
			}
			else {
				error.insertAfter(element);
			}
		},
		submitHandler: function (form) {
			$('input[type="submit"]').prop('disabled', true);
			
			if($("#position").val() == ""){
				$("#position").val('0');
			}
			
			form.submit();
		},
		invalidHandler: function() {
			$('input[type="submit"]').prop('disabled', false);
		}
	});
});	
</script>

<!-- Events -->
<script type="text/javascript">
$('span#deleteRecommendedProduct').on('click', function () {
	var id = $(this).closest('tr').data('id');
	var rpid= $(this).closest('tr').data('rproductid');
	var rpname= $(this).closest('tr').data('rproductname');
	
	$('#deleterpId').text(rpname+" "+ " Recommended Product will be deleted. Do you want to continue?");
	$('#myModalDelete').data('id', id).modal('show');
});

$('#btnD').click(function () {
	var prtNo=$("#prtNo_").val();
	var id = $('#myModalDelete').data('id');	
	$.get("${contextPath}/admin/deleteRecommendedProduct?rpNo="+id , function(status) {		
		$('[data-id=' + id + ']').remove();
		$('#myModalDelete').modal('hide');
		window.location.href="${contextPath}/admin/recommendedProducts?deleteMsg="+status+"&prtNo="+prtNo;
	});
});

$('#rpName').on('blur', function () {	
	var prtNo=$("#prtNo").val();
	var prtPrdNo=$("#prtPrdNo").val();
	rpNameInvalid=false;
	
	if(prtPrdNo==''){				
		rpNameInvalid=true;
		$("#recommendedProductBean").validate().element('#rpName');
	}
	else{			
		$.get("${contextPath}/admin/checkRecommendedProduct?prtPrdNo="+prtPrdNo, function(data,status) {
			if(status=="success" && data!=''){
				rpNameExist=true;	
			}
			else{
				rpNameExist=false;
			}
			$("#recommendedProductBean").validate().element('#rpName');				
		});
	}
});

$('#position').on('keyup', function () {
	var prtPrdPos = parseInt($("#position").val());
	rpPositionExist = false;
	
	if(prtPrdPos == 0){
		$("#recommendedProductBean").validate().element('#position');
	}
	else{
		$.get("${contextPath}/admin/checkRecommendedProductPosition?prtPrdPos="+prtPrdPos, function(data,status) {
			if(status=="success" && data!=''){
				rpPositionExist=true;	
			}
			else{
				rpPositionExist=false;
			}
			$("#recommendedProductBean").validate().element('#position');				
		});
	}
});

$("#prtNo").on( "selectmenuchange", function(){
	var obj=null;
	var prtNo = $("#prtNo option:selected").val();
	$("#rpName").val('');
	
	$.get("${contextPath}/admin/getPartnerCategories?prtNo="+prtNo, function(data){
		var obj = jQuery.parseJSON(data);
		$('#prtCatNo').empty();
		$('#prtCatNo').append("<option value=''>-Partner Category-</option>");
		
		for(var i = 0; i < obj.length; i++){
			var value = obj[i];
			var newOption = $("<option value="+value.prtCatNo+">"+value.partnercategoryName+"</option>");
			$('#prtCatNo').append(newOption);	       
		}
		$( "#prtCatNo" ).selectmenu( "refresh" );
		$("#recommendedProductBean").validate().element('#prtNo');
	});
});

$("#prtNo_").on( "selectmenuchange", function(){
	if ($('#prtNo_').val() != ''){
		$(this).closest('form').trigger('submit');
	}
});  

$("#prtCatNo").on( "selectmenuchange", function(){
	var prtCatNo = $("#prtCatNo option:selected").val();
	$("#rpName").val('');
	$("#prtPrdNo").val('');
	
	$( "#rpName" ).autocomplete({
		minLength: 1,
		source: function(request, response) {    	  
			$.ajax({
				dataType: "json",
				type: "GET",
				xhrFields: {
					withCredentials: true
				},
				crossDomain: true,
				cache: true,
				data: {
					keyword: request.term
				}, 
				url: "${contextPath}/admin/getPartnerProduct?prtCatNo="+prtCatNo,
				success: function(data){ 
					var array= $.map( data, function(key,value) {
						return {                    	  
							label:key,
							value:value
						}
					});
					response($.ui.autocomplete.filter(array, request.term));
				},
				error: function (data) {
				}
			})
		},
		focus: function( event, ui ) {
			event.preventDefault();
		},
		select: function(event, ui) {
			$("#prtPrdNo").val(ui.item.value); 
			$("#rpName").val(ui.item.label);
			rpNameInvalid = false;
			$("#recommendedProductBean").validate().element('#rpName');
			return false;
		}
	});
	$("#recommendedProductBean").validate().element('#prtCatNo');
});

/*$.ui.autocomplete.prototype._renderItem = function (ul, item) {        
    var t = String(item.value).replace(
            new RegExp(this.term, "gi"),
            "<span class='ui-state-highlight'>$&</span>");
    return $("<li></li>")
        .data("item.autocomplete", item)
        .append("<a>" + t + "</a>")
        .appendTo(ul);
};*/

$( "#prtCatNo" ).on( "selectmenuselect", function(){
	var prtNo = $("#prtNo option:selected").val();
	$("#rpName").val('');
	$("#prtPrdNo").val('');
	
	if(prtNo==''){
		alert("Please select the Partner");
		$("#rpName").val('');
		$( "#prtNo-button" ).focus();
		return false;
	}			 
});

$( "#rpName" ).click(function() {
	var prtCatNo = $("#prtCatNo option:selected").val();
	$("#rpName").val('');
	$("#prtPrdNo").val('');
	
	if(prtCatNo==''){
		alert("Please select the Partner Category");
		$("#rpName").val('');
		$( "#prtCatNo-button" ).focus();
		return false;
	}			 
});

$( "#datepicker1" ).on('change', function () {
	$("#recommendedProductBean").validate().element('#datepicker1');
	var endDate = getFormattedDate($('#datepicker2').val());
	
	if(endDate != 'Invalid Date'){
		$("#recommendedProductBean").validate().element('#datepicker2');
	}
});  

$( "#datepicker2" ).on('change', function () {
	$("#recommendedProductBean").validate().element('#datepicker2');
	$("#recommendedProductBean").validate().element('#datepicker1');
});
</script>

<!-- Validation Method -->
<script type="text/javascript">
$.validator.addMethod('duplicaterpName', function(value, element) {
	return !rpNameExist;
});

$.validator.addMethod('duplicaterpPosition', function(value, element) {
	return !rpPositionExist;
});

$.validator.addMethod('rpNameInvalid', function(value, element) {
	return !rpNameInvalid;
});

$.validator.addMethod('positionRange', function(value, element) {
	var positionRange = parseInt($("#position").val());
	var isValid = true;
	
	if(positionRange > 100){
		isValid = false;
	}

	return isValid;
});

$.validator.addMethod("startDate", function(value, element) {
	var startDate = getFormattedDate($('#datepicker1').val());
	var endDate = getFormattedDate($('#datepicker2').val());
	if(endDate=='Invalid Date')
		return true;
	if (startDate <= endDate)
		return true;
	if (startDate >= endDate)
		return false;
});

$.validator.addMethod("endDate", function(value, element) {
	var startDate = getFormattedDate($('#datepicker1').val());
	var endDate = getFormattedDate($('#datepicker2').val());
	if(endDate=='Invalid Date')
		return true;
	if (endDate >= startDate)
		return true;
	if (endDate <= startDate)
		return false;
});

$.validator.addMethod('positionNotZero', function(value, element) {
	var isValid = true;
	var rpPosition = parseInt($("#position").val());
	
	if(rpPosition == 0){
		isValid = false;
	}
	return isValid; 
});
</script>

<!-- Method -->
<script type="text/javascript">
function getFormattedDate(date){
	var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
	return new Date(date.replace(pattern,'$3-$2-$1'));
}
</script>
